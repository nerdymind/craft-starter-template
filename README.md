## Craft starter templates
These starter templates include Bootstrap Vue and some javascript libraries to expedite the Craft template setup procedure.
Use the dev.sql to set up all of the blocks for the site. 

# Install instructions
1. Copy the **templates** folder into the **templates** folder, overwriting the existing folder with the same name.
2. Copy the **redactor** folder into the **config** folder.
3. At the root run:
    1. `npm install`
    2. `compser install`
