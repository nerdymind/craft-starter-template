/*
 * Laravel Mix - A wrapper for webpack
 * More info: https://laravel-mix.com/docs/2.1/basic-example
 */

let mix = require('laravel-mix');
// let webpack = require('webpack'); // Require if using custom Plugins, etc.

mix.setPublicPath('web/assets') // important: make sure to set this to base "build" path and use relative paths for methods
    .js('templates/_assets/js/main.js', 'js')
    .sass('templates/_assets/sass/main.scss', 'css')
    .copy('templates/_assets/images/', 'web/assets/images/')
    .copy('templates/_assets/images/favicons', 'web/assets/images/favicons')
    .webpackConfig({
        resolve: {
            modules: [
                'node_modules'
            ],
            alias: {
                // Add aliases, e.g. for Vue:
                /* 'vue$': mix.inProduction() ? 'vue/dist/vue.min' : 'vue/dist/vue.js' */
            },
        },
    })
    .browserSync({
        // Browser Sync API, more info: https://browsersync.io/docs/api
        files: [
            'web/assets/css/{*,**/*}.css',
            'web/assets/js/{*,**/*}.js',
            'templates/{*,**/*}.twig'
        ],
        proxy: 'http://test.test'
    }, {reload: true})
//.version() // use to enable hashing the asset names for cache breaking
