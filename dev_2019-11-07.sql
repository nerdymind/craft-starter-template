# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.26)
# Database: pc_dev
# Generation Time: 2019-11-07 16:53:55 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table craft_assetindexdata
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assetindexdata`;

CREATE TABLE `craft_assetindexdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sessionId` varchar(36) NOT NULL DEFAULT '',
  `volumeId` int(11) NOT NULL,
  `uri` text,
  `size` bigint(20) unsigned DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  `recordId` int(11) DEFAULT NULL,
  `inProgress` tinyint(1) DEFAULT '0',
  `completed` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_assetindexdata_sessionId_volumeId_idx` (`sessionId`,`volumeId`),
  KEY `craft_assetindexdata_volumeId_idx` (`volumeId`),
  CONSTRAINT `craft_assetindexdata_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `craft_volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_assets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assets`;

CREATE TABLE `craft_assets` (
  `id` int(11) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `folderId` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `kind` varchar(50) NOT NULL DEFAULT 'unknown',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `size` bigint(20) unsigned DEFAULT NULL,
  `focalPoint` varchar(13) DEFAULT NULL,
  `deletedWithVolume` tinyint(1) DEFAULT NULL,
  `keptFile` tinyint(1) DEFAULT NULL,
  `dateModified` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_assets_folderId_idx` (`folderId`),
  KEY `craft_assets_volumeId_idx` (`volumeId`),
  CONSTRAINT `craft_assets_folderId_fk` FOREIGN KEY (`folderId`) REFERENCES `craft_volumefolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_assets_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_assets_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `craft_volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_assets` WRITE;
/*!40000 ALTER TABLE `craft_assets` DISABLE KEYS */;

INSERT INTO `craft_assets` (`id`, `volumeId`, `folderId`, `filename`, `kind`, `width`, `height`, `size`, `focalPoint`, `deletedWithVolume`, `keptFile`, `dateModified`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(11,1,1,'Text-Banner.jpg','image',1440,154,5871,NULL,NULL,NULL,'2019-01-30 21:35:19','2019-01-30 21:35:19','2019-01-30 21:35:19','4cedbed9-a64f-4927-9565-ec9140bccaf7'),
	(12,1,1,'Place-holder-Video.jpg','image',506,352,165311,NULL,NULL,NULL,'2019-01-30 21:35:20','2019-01-30 21:35:20','2019-01-30 21:35:20','a004fa36-8c75-4d35-8de5-961dc2f65194'),
	(13,1,1,'Brochure-Background.jpg','image',1440,400,14195,NULL,NULL,NULL,'2019-01-30 21:38:03','2019-01-30 21:38:03','2019-02-07 22:31:30','d8caa664-bd08-44f4-8974-fe541c6825ff'),
	(14,1,1,'Brochure-Image.png','image',587,398,365384,NULL,NULL,NULL,'2019-01-30 21:38:05','2019-01-30 21:38:05','2019-01-30 21:38:05','c1164bdc-5859-40f6-ab24-fc980362f69d'),
	(15,1,1,'Kid-Smiling.jpg','image',583,527,58472,NULL,NULL,NULL,'2019-01-30 21:38:06','2019-01-30 21:38:06','2019-01-30 21:38:06','85240596-24ba-4e1d-9f9f-6a03eb7745c8'),
	(16,1,1,'Map-Placeholder.jpg','image',524,281,30431,NULL,NULL,NULL,'2019-01-30 21:38:16','2019-01-30 21:38:16','2019-01-30 21:38:16','10cdeaf5-2cb9-4c29-8b70-a18e03a3e74b'),
	(17,1,1,'Black-Board-Texture.jpg','image',1440,583,176945,NULL,NULL,NULL,'2019-01-30 21:38:32','2019-01-30 21:38:32','2019-01-30 21:38:32','67ed51e3-f809-4643-86d7-21723a9fbb81'),
	(18,1,1,'Hero-Image.jpg','image',2300,1937,634350,NULL,NULL,NULL,'2019-01-30 21:38:40','2019-01-30 21:38:40','2019-01-30 21:38:40','b9cc47b7-1ae0-4516-831b-d5e7eafaa930'),
	(19,1,1,'Story3-Image.jpg','image',371,257,33340,NULL,NULL,NULL,'2019-01-30 21:38:56','2019-01-30 21:38:56','2019-01-30 21:38:56','492d03be-332a-4390-af7e-0f6ea56df827'),
	(20,1,1,'Story2-Image.jpg','image',376,262,25184,NULL,NULL,NULL,'2019-01-30 21:38:57','2019-01-30 21:38:57','2019-01-30 21:38:57','765b9588-e8c3-4b6f-87af-e33c50162941'),
	(21,1,1,'Story1-Image.jpg','image',366,262,39030,NULL,NULL,NULL,'2019-01-30 21:38:58','2019-01-30 21:38:58','2019-01-30 21:38:58','ae1a5bd2-ac8e-48a9-a43f-e41994cc45b1'),
	(40,1,1,'logo.svg','image',246,44,4522,NULL,NULL,NULL,'2019-01-30 22:51:51','2019-01-30 22:51:51','2019-01-30 22:51:51','245469d9-f410-4598-8bb7-e2d6c1088df5'),
	(41,1,1,'banner.jpg','image',2000,1416,727636,NULL,NULL,NULL,'2019-01-30 23:06:44','2019-01-30 23:06:45','2019-01-30 23:06:45','896a0726-405d-4c95-be47-2496e14b4398'),
	(42,1,1,'GrayHeart.png','image',414,463,20314,NULL,NULL,NULL,'2019-01-30 23:10:27','2019-01-30 23:10:27','2019-01-30 23:10:27','cc4c527c-6de3-43b8-aab7-5a0e49e934f3'),
	(46,1,1,'red-heart.svg','image',699,603,2267,NULL,NULL,NULL,'2019-01-30 23:17:15','2019-01-30 23:17:15','2019-01-30 23:17:15','3cf56713-9069-4060-a193-88c4e4dc7a94'),
	(56,1,1,'school-group.jpg','image',2048,1365,320315,NULL,NULL,NULL,'2019-01-30 23:43:47','2019-01-30 23:43:47','2019-01-30 23:43:47','a738fe4c-37d6-4649-ad04-820eaa9a1294'),
	(57,1,1,'school.jpg','image',2048,1365,263527,NULL,NULL,NULL,'2019-01-30 23:43:49','2019-01-30 23:43:49','2019-01-30 23:43:49','c044bdc3-a0b1-44ad-b120-619101a5f941'),
	(58,1,1,'chirstmas-tree.jpg','image',1080,1080,196643,NULL,NULL,NULL,'2019-01-30 23:43:50','2019-01-30 23:43:50','2019-01-30 23:43:50','6a9f87dd-e72b-40f6-9acf-72815432c996'),
	(87,1,1,'boy-1205255_1920.jpg','image',1920,1315,506218,NULL,NULL,NULL,'2019-02-07 20:58:43','2019-02-07 20:58:43','2019-02-07 20:58:43','85ce6318-b25f-4371-9a9a-0f17d534f3ea'),
	(88,1,1,'card.jpg','image',1920,1280,417437,NULL,NULL,NULL,'2019-02-07 20:58:44','2019-02-07 20:58:44','2019-02-07 20:58:44','8b1d83f7-76d7-451f-8520-fb5fc39d248a'),
	(89,1,1,'child-865116_1920.jpg','image',1920,1280,635123,NULL,NULL,NULL,'2019-02-07 20:58:46','2019-02-07 20:58:46','2019-02-07 20:58:46','aa8f635a-3f86-4b81-89f9-b909a78f9f54'),
	(90,1,1,'donate.jpg','image',1024,682,578140,NULL,NULL,NULL,'2019-02-07 20:58:47','2019-02-07 20:58:47','2019-02-07 20:58:47','90f1e49b-e859-49cb-a06c-86820ba8c960'),
	(91,1,1,'giving.jpg','image',1920,1523,681208,NULL,NULL,NULL,'2019-02-07 20:58:48','2019-02-07 20:58:48','2019-02-07 20:58:48','13877693-f99b-4a22-b4f5-69e3fe054574'),
	(92,1,1,'hearthands.jpg','image',1920,1311,335442,NULL,NULL,NULL,'2019-02-07 20:58:48','2019-02-07 20:58:48','2019-02-07 20:58:48','da7a1d8d-4e1e-4bc4-b64b-604995949ef3'),
	(93,1,1,'kindness.jpg','image',1920,1440,1611431,NULL,NULL,NULL,'2019-02-07 20:58:50','2019-02-07 20:58:51','2019-02-07 20:58:51','8a1bd5cc-a127-4524-853e-ab03304f2c3c'),
	(94,1,1,'notebook-2247351_1920.jpg','image',1920,1290,263929,NULL,NULL,NULL,'2019-02-07 20:58:52','2019-02-07 20:58:52','2019-02-07 20:58:52','248af36e-2ec0-4bfc-a453-ab998b729a03'),
	(95,1,1,'school-1974369_1920.jpg','image',1920,1280,238021,NULL,NULL,NULL,'2019-02-07 20:58:52','2019-02-07 20:58:52','2019-02-07 20:58:52','da6f3dd3-3ed6-470f-855d-dce6c6a9dd7e'),
	(96,1,1,'shutterstock_162276314_FILTER.jpg','image',3154,2099,1196455,NULL,NULL,NULL,'2019-02-07 20:58:54','2019-02-07 20:58:54','2019-02-07 20:58:54','b6451cb6-7bae-4e56-8963-e7161aa666c4'),
	(97,1,1,'shutterstock_194717693-2.jpg','image',2506,1795,1244333,NULL,NULL,NULL,'2019-02-07 20:58:55','2019-02-07 20:58:55','2019-02-07 20:58:55','79576716-7562-4758-ae25-3a0135db0da7'),
	(133,1,1,'1B4A7189_FILTER.jpg','image',1200,800,547645,NULL,NULL,NULL,'2019-02-07 22:17:58','2019-02-07 22:17:58','2019-02-07 22:17:58','a5ae7778-349e-41f8-b091-fc5af0cbfdbc'),
	(134,1,1,'1B4A7199_BleachSmooth.jpg','image',954,800,140780,NULL,NULL,NULL,'2019-02-07 22:26:50','2019-02-07 22:17:59','2019-02-07 22:26:50','052fba37-6e6c-44ea-b1a2-5fd6f22edc12'),
	(135,1,1,'1B4A7252_modify.jpg','image',1200,800,737513,NULL,NULL,NULL,'2019-02-07 22:18:00','2019-02-07 22:18:00','2019-02-07 22:18:00','392b39c0-fa48-47aa-8076-5a1d2d081e3f'),
	(136,1,1,'1B4A7455-Bleach.jpg','image',1200,800,699264,NULL,NULL,NULL,'2019-02-07 22:18:01','2019-02-07 22:18:01','2019-02-07 22:18:01','c0db1ddb-c82c-4835-9592-61e23645559d'),
	(137,1,1,'1B4A7481_BleachSmooth.jpg','image',1200,800,557406,NULL,NULL,NULL,'2019-02-07 22:18:02','2019-02-07 22:18:02','2019-02-07 22:18:02','7780a80c-25a6-4f9a-a4c1-307812c93cce'),
	(138,1,1,'1B4A7611_BleachSmooth.jpg','image',1200,800,565744,'0.4483;0.5738',NULL,NULL,'2019-02-07 22:18:03','2019-02-07 22:18:03','2019-02-07 22:24:47','150b1693-ff7b-486a-8ae9-74184dbc9621'),
	(151,1,1,'39265722_1663716650421197_7196011610442301440_o.jpg','image',2048,1365,267753,NULL,NULL,NULL,'2019-02-07 22:52:44','2019-02-07 22:52:45','2019-02-07 22:52:45','766c369e-bd3b-44fa-9566-dd8fdf606ad0'),
	(162,1,1,'Gray-Background.jpg','image',1440,400,20282,NULL,NULL,NULL,'2019-02-08 16:41:02','2019-02-08 16:41:02','2019-02-08 16:41:02','96971809-3030-41fb-8a0a-00a7feb3839f'),
	(167,1,1,'Gray-Heart-Background.jpg','image',1248,400,19082,NULL,NULL,NULL,'2019-02-08 16:48:25','2019-02-08 16:48:25','2019-02-08 16:48:25','00027bbf-1a05-46be-8388-68592a537fd5'),
	(168,1,1,'GrayBackgroundFP.jpg','image',1248,400,19887,NULL,NULL,NULL,'2019-02-08 16:52:58','2019-02-08 16:52:58','2019-02-08 16:52:58','4a57bbc0-9535-47ac-ad46-2c0c0b9bc1b3');

/*!40000 ALTER TABLE `craft_assets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_assettransformindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assettransformindex`;

CREATE TABLE `craft_assettransformindex` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetId` int(11) NOT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `location` varchar(255) NOT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `fileExists` tinyint(1) NOT NULL DEFAULT '0',
  `inProgress` tinyint(1) NOT NULL DEFAULT '0',
  `dateIndexed` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_assettransformindex_volumeId_assetId_location_idx` (`volumeId`,`assetId`,`location`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_assettransformindex` WRITE;
/*!40000 ALTER TABLE `craft_assettransformindex` DISABLE KEYS */;

INSERT INTO `craft_assettransformindex` (`id`, `assetId`, `filename`, `format`, `location`, `volumeId`, `fileExists`, `inProgress`, `dateIndexed`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,41,'banner.jpg',NULL,'_banner',1,1,0,'2019-02-05 23:42:37','2019-02-05 23:42:37','2019-02-05 23:42:40','f9fef5cc-7074-42b8-b7b6-1f12f2421716'),
	(2,41,'banner.jpg',NULL,'_mediumSquare',1,1,0,'2019-02-05 23:42:37','2019-02-05 23:42:37','2019-02-05 23:42:40','d32e216e-f1be-4f14-94d0-53f4e9afd2bf'),
	(3,15,'Kid-Smiling.jpg',NULL,'_mediumSquare',1,1,0,'2019-02-05 23:42:37','2019-02-05 23:42:37','2019-02-05 23:42:40','29398b46-c9de-4bef-af3b-d90c466ca844'),
	(4,57,'school.jpg',NULL,'_thumbnail',1,1,0,'2019-02-05 23:42:37','2019-02-05 23:42:37','2019-02-05 23:42:40','b2d8c56e-b657-4bb5-a6ec-f0da1f012a05'),
	(5,41,'banner.jpg',NULL,'_thumbnail',1,1,0,'2019-02-05 23:42:37','2019-02-05 23:42:37','2019-02-05 23:42:40','8cce7d5e-9317-4b55-9a41-f7d0919b45dd'),
	(6,21,'Story1-Image.jpg',NULL,'_thumbnail',1,1,0,'2019-02-05 23:42:37','2019-02-05 23:42:37','2019-02-05 23:42:41','7b1597b3-db9e-4fd5-9968-9b5110f64546'),
	(7,58,'chirstmas-tree.jpg',NULL,'_thumbnail',1,1,0,'2019-02-05 23:42:38','2019-02-05 23:42:38','2019-02-05 23:42:41','87601457-4529-49d4-8bee-2c7867c58214'),
	(8,56,'school-group.jpg',NULL,'_thumbnail',1,1,0,'2019-02-05 23:42:38','2019-02-05 23:42:38','2019-02-05 23:42:41','ccad3db9-9c0d-4392-b593-7dc2670991e0'),
	(9,14,'Brochure-Image.png',NULL,'_thumbnail',1,1,0,'2019-02-05 23:42:38','2019-02-05 23:42:38','2019-02-05 23:42:41','059672c0-1587-4991-9b51-edc04bc8c56e'),
	(10,18,'Hero-Image.jpg',NULL,'_thumbnail',1,1,0,'2019-02-05 23:42:38','2019-02-05 23:42:38','2019-02-05 23:42:41','39c78d58-e8e5-4752-b180-264feb6f0ea2'),
	(11,15,'Kid-Smiling.jpg',NULL,'_thumbnail',1,1,0,'2019-02-05 23:42:38','2019-02-05 23:42:38','2019-02-05 23:42:41','2c0752e1-0834-4000-bcbc-ff192dd17ae2'),
	(12,16,'Map-Placeholder.jpg',NULL,'_thumbnail',1,1,0,'2019-02-05 23:42:38','2019-02-05 23:42:38','2019-02-05 23:42:42','1c60b789-11bc-4807-a20a-aff6512c3d3f'),
	(13,12,'Place-holder-Video.jpg',NULL,'_thumbnail',1,1,0,'2019-02-05 23:42:38','2019-02-05 23:42:38','2019-02-05 23:42:42','259653fa-f620-42c8-9864-50496e0ad312'),
	(14,20,'Story2-Image.jpg',NULL,'_thumbnail',1,1,0,'2019-02-05 23:42:38','2019-02-05 23:42:38','2019-02-05 23:42:42','44eee86e-c4e6-4b65-a101-1ac988f2d42d'),
	(15,91,'giving.jpg',NULL,'_thumbnail',1,1,0,'2019-02-07 21:46:40','2019-02-07 21:46:40','2019-02-07 21:46:41','cc6f2792-f127-4095-91fe-3b9cf1d72a73'),
	(16,89,'child-865116_1920.jpg',NULL,'_thumbnail',1,1,0,'2019-02-07 21:53:52','2019-02-07 21:53:52','2019-02-07 21:53:53','35e7cd07-5c51-4caf-97e7-09ba9864b711'),
	(17,95,'school-1974369_1920.jpg',NULL,'_thumbnail',1,1,0,'2019-02-07 22:18:36','2019-02-07 22:18:36','2019-02-07 22:18:37','7a5abd87-b8f1-4be0-8c0e-b47d005d59d6'),
	(19,136,'1B4A7455-Bleach.jpg',NULL,'_thumbnail',1,1,0,'2019-02-07 22:18:36','2019-02-07 22:18:36','2019-02-07 22:18:37','6bf40168-97e8-4359-9a3c-758df4f92a83'),
	(21,133,'1B4A7189_FILTER.jpg',NULL,'_thumbnail',1,1,0,'2019-02-07 22:22:30','2019-02-07 22:22:30','2019-02-07 22:22:31','f650239c-609d-4af3-8695-af958b4a7dee'),
	(22,93,'kindness.jpg',NULL,'_thumbnail',1,1,0,'2019-02-07 22:22:37','2019-02-07 22:22:37','2019-02-07 22:22:38','7c2b635d-3a0c-404f-8542-03a8f9c93fda'),
	(23,138,'1B4A7611_BleachSmooth.jpg',NULL,'_thumbnail',1,1,0,'2019-02-07 22:24:43','2019-02-07 22:24:43','2019-02-07 22:24:44','ea1b7de4-8de3-47a6-9320-a31549935040'),
	(24,96,'shutterstock_162276314_FILTER.jpg',NULL,'_mediumSquare',1,1,0,'2019-02-07 22:47:30','2019-02-07 22:47:30','2019-02-07 22:47:32','bef9eb0a-e2e5-4dd5-8850-d7f3af78c493'),
	(25,138,'1B4A7611_BleachSmooth.jpg',NULL,'_mediumSquare',1,1,0,'2019-02-07 22:47:30','2019-02-07 22:47:30','2019-02-07 22:47:32','15d5ec01-3395-4b70-91a6-dd42513681e0'),
	(26,137,'1B4A7481_BleachSmooth.jpg',NULL,'_mediumSquare',1,1,0,'2019-02-07 22:47:30','2019-02-07 22:47:30','2019-02-07 22:47:33','9da19b30-a9a6-4c9f-9227-e171a13522aa'),
	(27,136,'1B4A7455-Bleach.jpg',NULL,'_mediumSquare',1,1,0,'2019-02-07 22:51:08','2019-02-07 22:51:08','2019-02-07 22:51:09','ddaebb22-04be-4f44-8369-3dfd0fa19e1c'),
	(28,151,'39265722_1663716650421197_7196011610442301440_o.jpg',NULL,'_thumbnail',1,1,0,'2019-02-07 22:52:57','2019-02-07 22:52:57','2019-02-07 22:52:59','3b9d8885-e54c-4217-9514-97db6f0437eb'),
	(29,137,'1B4A7481_BleachSmooth.jpg',NULL,'_thumbnail',1,1,0,'2019-02-07 23:42:52','2019-02-07 23:42:52','2019-02-07 23:42:53','c095a891-6da9-46b6-8e40-5f3887c96806'),
	(30,134,'1B4A7199_BleachSmooth.jpg',NULL,'_thumbnail',1,1,0,'2019-02-12 20:43:26','2019-02-12 20:43:26','2019-02-12 20:43:26','6ddd60bd-6c8c-49ca-9999-578ab8ce36f7'),
	(31,97,'shutterstock_194717693-2.jpg',NULL,'_thumbnail',1,1,0,'2019-02-12 20:48:38','2019-02-12 20:48:38','2019-02-12 20:48:39','97e270c8-d3ff-44b5-a3d5-a59e2833ea4c');

/*!40000 ALTER TABLE `craft_assettransformindex` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_assettransforms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_assettransforms`;

CREATE TABLE `craft_assettransforms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `mode` enum('stretch','fit','crop') NOT NULL DEFAULT 'crop',
  `position` enum('top-left','top-center','top-right','center-left','center-center','center-right','bottom-left','bottom-center','bottom-right') NOT NULL DEFAULT 'center-center',
  `width` int(11) unsigned DEFAULT NULL,
  `height` int(11) unsigned DEFAULT NULL,
  `format` varchar(255) DEFAULT NULL,
  `quality` int(11) DEFAULT NULL,
  `interlace` enum('none','line','plane','partition') NOT NULL DEFAULT 'none',
  `dimensionChangeTime` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_assettransforms_name_unq_idx` (`name`),
  UNIQUE KEY `craft_assettransforms_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_assettransforms` WRITE;
/*!40000 ALTER TABLE `craft_assettransforms` DISABLE KEYS */;

INSERT INTO `craft_assettransforms` (`id`, `name`, `handle`, `mode`, `position`, `width`, `height`, `format`, `quality`, `interlace`, `dimensionChangeTime`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Thumbnail','thumbnail','crop','center-center',350,350,NULL,NULL,'none','2019-01-31 16:07:03','2019-01-31 16:07:03','2019-01-31 16:07:03','b38cff1b-146d-442e-ab43-2d60a7fd3986'),
	(2,'Banner','banner','crop','center-center',1400,NULL,NULL,NULL,'none','2019-01-31 16:07:47','2019-01-31 16:07:47','2019-01-31 16:07:47','43d844ad-22c6-47ee-b7b4-c2b3a9e0c48c'),
	(3,'Medium Square','mediumSquare','crop','center-center',580,580,NULL,NULL,'none','2019-02-01 20:57:46','2019-02-01 20:57:46','2019-02-01 20:57:46','1f849e9e-ba75-4cf0-b79a-25ba0a277be1');

/*!40000 ALTER TABLE `craft_assettransforms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_categories`;

CREATE TABLE `craft_categories` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_categories_groupId_idx` (`groupId`),
  KEY `craft_categories_parentId_fk` (`parentId`),
  CONSTRAINT `craft_categories_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_categories_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_categories_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `craft_categories` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_categories` WRITE;
/*!40000 ALTER TABLE `craft_categories` DISABLE KEYS */;

INSERT INTO `craft_categories` (`id`, `groupId`, `parentId`, `deletedWithGroup`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(8,1,NULL,0,'2019-01-30 21:25:57','2019-01-30 21:26:44','9f7fdfeb-ea18-4b97-8842-5c7126af0731'),
	(9,1,NULL,0,'2019-01-30 21:26:31','2019-01-30 21:26:31','9a5d550b-ca15-4534-b661-8ca437bdb17a'),
	(51,2,NULL,NULL,'2019-01-30 23:35:14','2019-01-30 23:35:14','c1613632-b2bf-4c17-98ca-1f17faa05dc5'),
	(52,2,NULL,NULL,'2019-01-30 23:35:24','2019-01-30 23:35:24','cb38f5b8-5975-4e80-890d-4615dc60a194'),
	(53,1,NULL,NULL,'2019-01-30 23:35:41','2019-01-30 23:35:41','623c4096-88d7-4c70-80b1-a4280bc236c1'),
	(54,1,NULL,NULL,'2019-01-30 23:36:36','2019-01-30 23:36:36','3bfb5ad3-551e-4b37-8512-0bef7edca517'),
	(55,1,NULL,NULL,'2019-01-30 23:36:48','2019-01-30 23:36:48','07b2666b-20df-4f32-9aae-63f1caa4f492'),
	(145,2,NULL,NULL,'2019-02-07 22:41:16','2019-02-07 22:41:16','b7eb7cd1-89a1-49d6-902e-ffb12d87408d'),
	(148,2,NULL,NULL,'2019-02-07 22:44:08','2019-02-07 22:44:08','74d7cfef-1038-4b13-b17c-babb38cd2c35'),
	(177,2,NULL,0,'2019-02-14 22:31:33','2019-02-14 22:31:33','b47d976c-bbcb-4053-9cf8-9fb867327177');

/*!40000 ALTER TABLE `craft_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_categorygroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_categorygroups`;

CREATE TABLE `craft_categorygroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_categorygroups_name_idx` (`name`),
  KEY `craft_categorygroups_handle_idx` (`handle`),
  KEY `craft_categorygroups_structureId_idx` (`structureId`),
  KEY `craft_categorygroups_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `craft_categorygroups_dateDeleted_idx` (`dateDeleted`),
  CONSTRAINT `craft_categorygroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_categorygroups_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_categorygroups` WRITE;
/*!40000 ALTER TABLE `craft_categorygroups` DISABLE KEYS */;

INSERT INTO `craft_categorygroups` (`id`, `structureId`, `fieldLayoutId`, `name`, `handle`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,2,NULL,'Blog','blog','2019-01-30 21:25:15','2019-01-30 21:25:15',NULL,'ddd31c2a-2ae9-45cc-8ee2-d43795a7198a'),
	(2,8,NULL,'Quote','quote','2019-01-30 23:34:45','2019-01-30 23:34:45',NULL,'ae11201c-d8b4-4575-a034-14d73922010c');

/*!40000 ALTER TABLE `craft_categorygroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_categorygroups_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_categorygroups_sites`;

CREATE TABLE `craft_categorygroups_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_categorygroups_sites_groupId_siteId_unq_idx` (`groupId`,`siteId`),
  KEY `craft_categorygroups_sites_siteId_idx` (`siteId`),
  CONSTRAINT `craft_categorygroups_sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_categorygroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_categorygroups_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_categorygroups_sites` WRITE;
/*!40000 ALTER TABLE `craft_categorygroups_sites` DISABLE KEYS */;

INSERT INTO `craft_categorygroups_sites` (`id`, `groupId`, `siteId`, `hasUrls`, `uriFormat`, `template`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,1,1,'blog/{slug}','blog/_categories.twig','2019-01-30 21:25:15','2019-01-30 21:25:15','7cab9134-cdd4-44a9-8150-f62376a0d8d8'),
	(2,2,1,1,'quote/{slug}','quote/categories.twig','2019-01-30 23:34:45','2019-01-30 23:34:45','6074f27c-bdbd-47d3-b971-d7f88661bfb5');

/*!40000 ALTER TABLE `craft_categorygroups_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_content`;

CREATE TABLE `craft_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_backgroundColor` varchar(7) DEFAULT NULL,
  `field_customBackground` varchar(255) DEFAULT NULL,
  `field_margin` text,
  `field_padding` text,
  `field_form` int(11) DEFAULT NULL,
  `field_blockContent` text,
  `field_blockTitle` text,
  `field_entryType` varchar(255) DEFAULT NULL,
  `field_headingOverride` text,
  `field_globalPhoneNumber` text,
  `field_video` text,
  `field_position` varchar(255) DEFAULT NULL,
  `field_width` varchar(255) DEFAULT NULL,
  `field_quoteText` text,
  `field_quoteAttribution` text,
  `field_shortDescription` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_content_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `craft_content_siteId_idx` (`siteId`),
  KEY `craft_content_title_idx` (`title`),
  CONSTRAINT `craft_content_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_content_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_content` WRITE;
/*!40000 ALTER TABLE `craft_content` DISABLE KEYS */;

INSERT INTO `craft_content` (`id`, `elementId`, `siteId`, `title`, `dateCreated`, `dateUpdated`, `uid`, `field_backgroundColor`, `field_customBackground`, `field_margin`, `field_padding`, `field_form`, `field_blockContent`, `field_blockTitle`, `field_entryType`, `field_headingOverride`, `field_globalPhoneNumber`, `field_video`, `field_position`, `field_width`, `field_quoteText`, `field_quoteAttribution`, `field_shortDescription`)
VALUES
	(1,1,1,NULL,'2019-01-21 23:30:44','2019-01-21 23:30:44','0786dcd6-2433-4133-b34d-9f68c2c02ffe',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(2,2,1,'Homepage','2019-01-21 23:39:18','2019-11-07 16:37:10','a0fd3245-7a82-4c1c-93f4-8465172c0829',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(3,3,1,NULL,'2019-01-21 23:41:01','2019-01-30 21:46:51','9ee5480a-c785-4b03-b406-6218b6beab64',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(4,4,1,NULL,'2019-01-21 23:41:12','2019-11-07 16:41:53','328988f3-b069-40d1-aa61-1b236f7e9a7f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(5,5,1,'Test Blog','2019-01-30 21:22:22','2019-02-05 20:28:28','9d32b2f9-88e2-4e17-a261-15914d5985cc',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'This is the text that shows up on the blog module!'),
	(6,6,1,'Blocks','2019-01-30 21:22:51','2019-11-07 16:38:02','e90b5c7d-5f01-4df3-8d37-baff6a89a1bd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(7,7,1,'Blog','2019-01-30 21:23:35','2019-02-05 19:05:20','803f87d0-a62c-4a75-ba82-f83196210c44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'blog',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(8,8,1,'Elementary School','2019-01-30 21:25:57','2019-01-30 21:26:44','8d95c386-a848-4617-86e0-9dee7b67fe3b',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(9,9,1,'High School','2019-01-30 21:26:31','2019-01-30 21:26:31','445e2604-a023-4568-bfd1-245a87c8bddd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(10,10,1,'Test','2019-01-30 21:32:20','2019-02-07 22:48:51','9b8dbc1e-fa38-460e-bf2e-73a08caaac13',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>','Test Name Last Name',NULL),
	(11,11,1,'Text-Banner','2019-01-30 21:35:19','2019-01-30 21:35:19','935730a0-49bf-4c1e-9855-dd0ad109456b',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(12,12,1,'Place-holder-Video','2019-01-30 21:35:20','2019-01-30 21:35:20','6a00ed91-7306-4274-9445-d8f4e99e3225',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(13,13,1,'Brochure-Background','2019-01-30 21:38:03','2019-02-07 22:31:30','9abb9ffc-d282-46b0-b082-5807d3466db0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(14,14,1,'Brochure-Image','2019-01-30 21:38:04','2019-01-30 21:38:04','984b72b2-82cf-4581-b432-61986e56e77b',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(15,15,1,'Kid-Smiling','2019-01-30 21:38:06','2019-01-30 21:38:06','e5b37434-ad20-45bc-8faa-4b00e928d447',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(16,16,1,'Map-Placeholder','2019-01-30 21:38:16','2019-01-30 21:38:16','ddcb10b0-c03f-4ecc-918a-0fcdda63aa14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(17,17,1,'Black-Board-Texture','2019-01-30 21:38:32','2019-01-30 21:38:32','47a9ae77-5581-467d-a79c-2b23d233f411',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(18,18,1,'Hero-Image','2019-01-30 21:38:39','2019-01-30 21:38:39','cccaae72-f840-4b39-8280-b0f9db5c10d5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(19,19,1,'Story3-Image','2019-01-30 21:38:56','2019-01-30 21:38:56','c87a581d-f26b-469c-aaed-b0380031d5b2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(20,20,1,'Story2-Image','2019-01-30 21:38:57','2019-01-30 21:38:57','ce566565-4029-4220-8c8b-c2a3d559bcd9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(21,21,1,'Story1-Image','2019-01-30 21:38:58','2019-01-30 21:38:58','5533a032-150a-49a2-a628-2a8c6124e88c',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(22,26,1,'About','2019-01-30 21:49:22','2019-02-08 17:36:56','8afb56c7-b782-42bc-a2c0-1ac0b8fc2d7d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Inspire Your Students to Change the World',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(23,27,1,'Project Heart For Elementary School','2019-01-30 21:49:45','2019-02-12 20:50:49','d442e2a6-dcc2-425a-9def-43feb222eeca',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(24,28,1,'Middle School','2019-01-30 21:49:56','2019-02-12 20:52:21','f89c8bd9-d313-451c-b3d7-1ee9fa1f1f70',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Project Heart For Middle School',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(25,29,1,'Project Heart For High School','2019-01-30 21:50:11','2019-02-12 20:53:06','503d4d80-6429-4f91-8aba-d0d6753c06c8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(26,30,1,'Home','2019-01-30 21:51:59','2019-11-07 16:37:10','09a88600-7190-4457-a3f4-f8951df37625',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(27,31,1,'About','2019-01-30 21:52:09','2019-02-08 17:36:55','cf27ccfe-4994-4339-8563-58583415c540',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(28,32,1,'Blog','2019-01-30 21:52:09','2019-02-05 19:05:20','f02551c1-f766-4b17-94b2-e594d53a9d7a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(29,33,1,'Middle School','2019-01-30 21:52:10','2019-02-12 20:52:21','d5242d82-519b-4e92-a013-7cdb10f97ae5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(30,34,1,'Elementary School','2019-01-30 21:52:10','2019-02-12 20:50:49','692063fc-8853-4689-96df-3b7856a52865',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(31,35,1,'High School','2019-01-30 21:52:10','2019-02-12 20:53:06','389e3a99-debf-4dff-8a64-3ce4a1658c6c',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(32,36,1,'Home','2019-01-30 21:54:06','2019-11-07 16:37:10','1ce1a265-057f-4047-a979-96f7dcfd0874',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(33,37,1,'Blog','2019-01-30 21:54:11','2019-02-05 19:05:19','92e48ebb-2517-45e6-a8cd-725377792d7b',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(34,38,1,'Privacy Policy','2019-01-30 21:55:00','2019-01-30 21:55:00','2ee13ac2-124e-44cb-9b5b-19ff1a01fbcc',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(35,39,1,'Blocks','2019-01-30 22:21:05','2019-02-08 16:29:36','cf323b39-ec80-4003-a421-b2de3a55dd8c',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(36,40,1,'Logo','2019-01-30 22:51:51','2019-01-30 22:51:51','04171cc7-e7d4-4a0a-9a21-224aae670343',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(37,41,1,'Banner','2019-01-30 23:06:44','2019-01-30 23:06:44','9cc345a1-82aa-4226-9b94-38bd8c01fc2a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(38,42,1,'Gray Heart','2019-01-30 23:10:27','2019-01-30 23:10:27','07873892-068d-44b1-8638-105667c9b141',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(39,43,1,NULL,'2019-01-30 23:11:17','2019-02-27 21:20:40','3e9a615f-6cca-4ad3-9254-a603724567e4',NULL,NULL,NULL,NULL,NULL,'<h1>Ottercares<br>philanthropy<br>program</h1>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(40,44,1,NULL,'2019-01-30 23:11:17','2019-02-27 21:20:40','f244a8b1-64e7-449b-b5ca-abe657bd90ff',NULL,NULL,NULL,NULL,NULL,'<h1>Headline</h1>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<h2>Header</h2>\r\n<h3>Header</h3>\r\n<h4>Header</h4>\r\n<h5>Header</h5>\r\n<p></p>\r\n<p><a href=\"{entry:2:url}\" class=\"btn\">\r\nButton</a></p>',NULL,NULL,NULL,NULL,'https://www.youtube.com/watch?v=ScMzIvxBSi4&t=4s','left','6',NULL,NULL,NULL),
	(41,45,1,NULL,'2019-01-30 23:11:17','2019-02-27 21:20:40','65be016f-aa9e-4951-b7f6-180edc3fdcb5',NULL,NULL,NULL,NULL,NULL,'<h2 style=\"text-align: center;\">Color Text and then non color</h2>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(42,46,1,'Red-heart','2019-01-30 23:17:15','2019-01-30 23:17:15','6507d0e7-a134-4327-a447-b2652375bd27',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(43,47,1,NULL,'2019-01-30 23:18:12','2019-02-27 21:20:40','81366e85-85fd-4991-9bab-d04292f2875f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(44,51,1,'High School','2019-01-30 23:35:14','2019-01-30 23:35:14','f4e73f57-e7f0-411c-8807-9c420851d632',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(45,52,1,'General','2019-01-30 23:35:24','2019-01-30 23:35:24','de503a72-aeda-48fb-833f-945a85823cfa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(46,53,1,'News','2019-01-30 23:35:41','2019-01-30 23:35:41','0b6f9db6-6fb3-4978-b332-c49b20c14054',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(47,54,1,'Stories','2019-01-30 23:36:36','2019-01-30 23:36:36','2bba4cd1-482e-4a1a-a750-cf6c09f8aa52',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(48,55,1,'General','2019-01-30 23:36:48','2019-01-30 23:36:48','44851d1c-4d5f-410a-8d43-295ca866f44d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(49,56,1,'School-group','2019-01-30 23:43:46','2019-01-30 23:43:46','548a87f8-beb7-417d-953d-27f832dc4f54',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(50,57,1,'School','2019-01-30 23:43:48','2019-01-30 23:43:48','c18527c2-0794-4f67-931f-3b43e19fd361',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(51,58,1,'Chirstmas-tree','2019-01-30 23:43:50','2019-01-30 23:43:50','e5b4990a-8b7b-45b0-8fd7-ebf519bfc6fc',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(52,59,1,NULL,'2019-01-30 23:50:25','2019-02-27 21:20:40','273269f0-4f11-4121-ace4-becee2c869ae',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(53,60,1,NULL,'2019-01-30 23:50:25','2019-02-27 21:20:40','77855d74-47b2-4533-a24a-fa7fb1fe9da3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(54,61,1,NULL,'2019-01-30 23:50:25','2019-02-27 21:20:40','0bc1c19b-7938-49fc-a5e4-410a4d2cb1a8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(55,62,1,NULL,'2019-01-30 23:50:26','2019-02-27 21:20:40','f2406cfc-6e7e-4158-aaaf-9b502eb4a4c2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(56,67,1,'Contact','2019-01-30 23:51:10','2019-02-05 18:47:10','92a9848e-b134-4377-aad7-eff153b7f79e',NULL,NULL,NULL,NULL,1,NULL,'Share Your Projects and Stories',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(57,68,1,NULL,'2019-01-31 16:04:34','2019-02-27 21:20:40','10935da7-02ab-48e9-92bf-46ad9870a274',NULL,'image',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(58,71,1,NULL,'2019-01-31 16:06:10','2019-02-27 21:20:40','cbade2b7-50ce-4c96-97b1-c3566c70becb',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(59,72,1,NULL,'2019-01-31 20:57:39','2019-02-08 18:05:31','619e4d8d-4855-4c6e-91d0-e549c65018f3',NULL,NULL,NULL,NULL,NULL,'<h1>Ottercares<br>Philanthropy <br>Program</h1>',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(60,73,1,'Test2','2019-02-01 20:56:00','2019-02-07 22:48:43','f1549a61-6e13-4d4e-9107-4ebf9d464a94',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>','- John Smith, Student',NULL),
	(61,74,1,NULL,'2019-02-05 17:49:55','2019-02-27 21:20:40','bec0e858-c049-457d-934d-5a6ef733b061',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(62,76,1,NULL,'2019-02-05 18:52:38','2019-02-05 20:28:28','b3c9aba2-5b1b-4f9a-91fd-f6c1ad2e8f48',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(63,79,1,'Test Blog2','2019-02-05 20:29:19','2019-02-05 20:29:19','b904a428-98d6-4f8e-93b2-a6a5787da4fd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
	(64,80,1,NULL,'2019-02-05 20:29:19','2019-02-05 20:29:19','7209e0ce-ec11-4bc1-b1b9-3e1b81b657d6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(65,82,1,'Test Blog 3','2019-02-05 20:30:08','2019-02-05 20:30:08','e62ad211-4cf9-4eb0-b2e0-3eabedeab236',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'),
	(66,83,1,NULL,'2019-02-05 20:30:08','2019-02-05 20:30:08','c4983383-10dd-4d4c-b258-5493e0a70258',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(67,85,1,NULL,'2019-02-07 20:26:26','2019-02-07 20:27:16','daa79cdf-7544-45a4-8029-b313435ca282',NULL,NULL,NULL,NULL,NULL,'<p>Changing the world is a big goal and we can’t do it alone. We want to ignite an entire generation of thinkers, makers, doers and givers who have passion and resource to tackle the world’s biggest problems. That is why we created Project Heart, our philanthropy education curriculum. Project Heart taps into every child’s innate desire to give, while providing them with the tools and framework they need to be tomorrow’s world-changers, today.</p>',NULL,NULL,NULL,NULL,NULL,'left','3',NULL,NULL,NULL),
	(68,86,1,NULL,'2019-02-07 20:33:23','2019-02-07 20:33:23','f10e8519-5b12-444f-b332-5dfd8656f523',NULL,NULL,NULL,NULL,NULL,'<p>Changing the world is a big goal and we can’t do it alone. We want to ignite an entire generation of thinkers, makers, doers and givers who have passion and resource to tackle the world’s biggest problems. That is why we created Project Heart, our philanthropy education curriculum. Project Heart taps into every child’s innate desire to give, while providing them with the tools and framework they need to be tomorrow’s world-changers, today.</p>',NULL,NULL,NULL,NULL,'<iframe src=\"https://player.vimeo.com/video/230004158\" width=\"640\" height=\"270\" frameborder=\"0\" allowfullscreen></iframe>','left','3',NULL,NULL,NULL),
	(69,87,1,'Boy-1205255_1920','2019-02-07 20:58:43','2019-02-07 20:58:43','5fc60601-80d1-4f3d-bc88-b0187b2a9f17',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(70,88,1,'Card','2019-02-07 20:58:44','2019-02-07 20:58:44','b910f0c5-19fe-486d-834b-88574dc1188a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(71,89,1,'Child-865116_1920','2019-02-07 20:58:45','2019-02-07 20:58:45','5231da84-a80a-4189-aa15-c7f89b8cc99f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(72,90,1,'Donate','2019-02-07 20:58:47','2019-02-07 20:58:47','5c53c5ec-5e3b-458d-a0c9-13a0a5cdd6d0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(73,91,1,'Giving','2019-02-07 20:58:47','2019-02-07 20:58:47','ea93e119-3b6a-4b16-9485-5b5fdae902d0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(74,92,1,'Hearthands','2019-02-07 20:58:48','2019-02-07 20:58:48','c280c411-93dd-4422-93c9-ff1875229989',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(75,93,1,'Kindness','2019-02-07 20:58:50','2019-02-07 20:58:50','c86b8638-a5fc-46e9-92c9-bdf52c624ad2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(76,94,1,'Notebook-2247351_1920','2019-02-07 20:58:51','2019-02-07 20:58:51','464bdafe-2bbc-4bc7-8955-a4e7796b311c',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(77,95,1,'School-1974369_1920','2019-02-07 20:58:52','2019-02-07 20:58:52','34ae30ed-3835-4464-b0cd-02e7af806af2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(78,96,1,'Shutterstock_162276314_FILTER','2019-02-07 20:58:53','2019-02-07 20:58:53','c23b9b5f-bd40-4616-aa94-1f1f2ec4f26b',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(79,97,1,'Shutterstock_194717693-2','2019-02-07 20:58:55','2019-02-07 20:58:55','e476c14f-7c5e-40e1-86f2-acccf4971ec8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(80,98,1,NULL,'2019-02-07 21:02:36','2019-02-08 17:36:56','35e12e99-aef0-451c-840a-675c599a51f2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(81,101,1,NULL,'2019-02-07 21:02:36','2019-02-08 17:36:56','30c2539f-11d3-4a7b-9cb4-7286ec2c1f70',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(82,102,1,NULL,'2019-02-07 21:16:26','2019-02-08 17:16:37','d8ce8bfc-ba89-4f64-a899-bb3c0f912be8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(83,104,1,NULL,'2019-02-07 21:16:26','2019-02-07 21:16:26','ede5d8b3-4ea9-4038-bc5c-57934dde1523',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(84,105,1,NULL,'2019-02-07 21:17:08','2019-02-12 20:39:11','4ae88113-7b75-4721-8f00-89480cd6e54f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(85,106,1,NULL,'2019-02-07 21:18:38','2019-02-08 17:15:21','cc2f8b24-fe68-48a1-baab-773177f19b76',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(86,108,1,NULL,'2019-02-07 21:18:38','2019-02-12 20:43:47','32157c24-e31d-4352-9fac-f9c2fb2c1d00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(87,109,1,NULL,'2019-02-07 21:20:38','2019-02-08 17:44:24','90d2c62c-bb4d-46e3-a8b7-06ec9c03f5b7',NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(88,112,1,NULL,'2019-02-07 21:23:41','2019-02-12 20:49:00','c8e505b6-e45e-486c-b36d-4a14192d832f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(89,113,1,NULL,'2019-02-07 21:30:45','2019-02-08 18:05:31','acdba800-20bf-42ea-8dba-0cdeb1577d45',NULL,'image',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(90,115,1,NULL,'2019-02-07 21:37:48','2019-02-08 18:05:31','8b4f3b06-d3e9-4b03-b00f-84dfb55c893b',NULL,'image',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(91,119,1,NULL,'2019-02-07 21:44:10','2019-02-08 18:05:31','fb4c9c5b-2b5a-4574-b670-0a86c42e4bea',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(92,120,1,NULL,'2019-02-07 21:44:10','2019-02-08 18:05:31','35c6d182-84b8-4e8d-8b87-16873f8afa65',NULL,'image',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(93,123,1,NULL,'2019-02-07 21:44:10','2019-02-08 18:05:32','5d59c634-17e3-4399-8908-4b206eb03fbb',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(94,126,1,NULL,'2019-02-07 22:07:10','2019-02-08 18:05:32','9ef64175-9c46-4940-8c04-2cd8fc032db6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(95,127,1,NULL,'2019-02-07 22:07:10','2019-02-08 18:05:32','7967e2ff-24c1-45fc-aa76-3f150470927b',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(96,128,1,NULL,'2019-02-07 22:07:10','2019-02-08 18:05:32','75cdf3ff-c499-48ce-a0d3-505c88e28674',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(97,133,1,'1B4A7189_FILTER','2019-02-07 22:17:58','2019-02-07 22:17:58','a8dc30a5-2b83-4808-bc55-5e3c7b7b06b4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(98,134,1,'1B4A7199_Bleach Smooth','2019-02-07 22:17:59','2019-02-07 22:26:50','c9dc882a-7367-4cf1-9dd2-8c9ac4be3e30',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(99,135,1,'1B4A7252_modify','2019-02-07 22:18:00','2019-02-07 22:18:00','311def11-baeb-4fa9-b808-4538ad01c3a7',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(100,136,1,'1B4A7455-Bleach','2019-02-07 22:18:01','2019-02-07 22:18:01','56f6fb65-63bc-4395-a9a0-748557f43fdb',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(101,137,1,'1B4A7481_Bleach Smooth','2019-02-07 22:18:02','2019-02-07 22:18:02','577d9bda-f677-4d9d-bfde-9277c27bc243',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(102,138,1,'1B4A7611_Bleach Smooth','2019-02-07 22:18:03','2019-02-07 22:24:47','e08014cd-2241-4875-907c-ff1d92f10ce5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(103,140,1,NULL,'2019-02-07 22:19:35','2019-02-12 20:52:21','f457e776-5c0b-4675-87aa-b71ccc323e8f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(104,142,1,NULL,'2019-02-07 22:22:58','2019-02-12 20:50:49','73fef888-b56c-4a54-9077-43e8970d7a20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(105,143,1,NULL,'2019-02-07 22:24:54','2019-02-12 20:53:06','30bbcc0d-1028-4124-81f1-4768d8040e05',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(106,144,1,'Elementary 1','2019-02-07 22:39:59','2019-02-08 17:07:53','7933da22-603d-428d-82a4-64cbaa79f3ac',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>Having Project Heart…connects you with the students on a deeper level and you all understand that you’re working together to make the world a better</p>','4th grade teacher at Shepardson Elementary',NULL),
	(107,145,1,'Middle School','2019-02-07 22:41:16','2019-02-07 22:41:16','52e4009d-24b0-4d6f-aeab-6313f3f2214d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(108,146,1,'Middle School 1','2019-02-07 22:41:24','2019-02-08 17:09:37','4489f53a-24d9-4b38-aabf-96620f294457',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>The kids know that the world needs them now. They don’t need to wait</p>','6th/8th grade teacher',NULL),
	(109,147,1,'High School 1','2019-02-07 22:42:53','2019-02-08 17:11:34','34782cdb-0989-40b2-9021-aed6d314d14c',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>Anyone can be a philanthropist; you just have to figure out that every little thing you do, every 30 minutes, 20 minutes that you spend your time and using YOUR talents to make the world you a better place that makes you a philanthropist. Anyone can be one; you just have to apply yourself</p>','Student, 16',NULL),
	(110,148,1,'Elementary','2019-02-07 22:44:08','2019-02-07 22:44:08','d3eb6165-10fa-4cd8-b588-ed71ef871a39',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(111,149,1,'Elementary 2','2019-02-07 22:44:14','2019-02-08 17:10:47','986d8335-6a11-43d7-8003-22a5e90c5485',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>One thing that I am walking away with from Project Heart is knowing that I helped someone and knowing that I impacted their life</p>','5th grade student at Cache La Poudre Elementary',NULL),
	(112,150,1,'Elementary 3','2019-02-07 22:45:07','2019-02-08 17:11:02','27af1e58-7425-417b-845e-2ca3e8c72713',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>Many parents, community members, and colleagues were impressed that we were helping kids develop the habit of using their time, talent, or fundraising skills to give back to others. Kids innately want to help others, and this project was an easy and useful way to tap into this desire</p>','4/5th grade teacher at Cache la Poudre Elementary School',NULL),
	(113,151,1,'39265722_1663716650421197_7196011610442301440_o','2019-02-07 22:52:44','2019-02-07 22:52:44','028226de-d230-41c6-aa93-df5b338e6594',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(114,152,1,NULL,'2019-02-08 15:47:39','2019-02-08 18:05:31','811df72b-2558-4fe6-9033-0b5fd6c0e1b5',NULL,'image',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(115,154,1,NULL,'2019-02-08 15:52:54','2019-02-08 18:05:31','a5c6c405-1888-4fc3-a633-1d0fa9772e72',NULL,'image',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(116,156,1,NULL,'2019-02-08 15:52:54','2019-02-08 18:05:32','5349f8aa-6f53-4463-9ada-1875e7355cb7',NULL,'image',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(117,158,1,NULL,'2019-02-08 15:52:55','2019-02-08 18:05:32','47240378-e289-4e5a-8428-80b3282f2765',NULL,'image',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(118,160,1,NULL,'2019-02-08 15:54:32','2019-02-08 18:05:32','20fa0e2a-9511-4f89-a1d3-04617a6586d7',NULL,'image',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(119,162,1,'Gray-Background','2019-02-08 16:41:02','2019-02-08 16:41:02','6e5fe961-7f04-4aa6-9758-c6a07c8384ab',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(120,163,1,NULL,'2019-02-08 16:42:15','2019-02-08 16:42:15','2a33488b-53d5-45f7-84cf-d7e6adf19e92',NULL,'image',NULL,NULL,NULL,'<h3>We’re on a mission to change the world, one classroom at a time.</h3><p>Changing the world is a big goal and we can’t do it alone. We want to ignite an entire generation of thinkers, makers, doers, and givers who have passion and resource to tackle the world’s biggest problems. That is why we created Project Heart, our philanthropy education curriculum. Project Heart taps into every child’s innate desire to give, while providing them with the tools and framework they need to be tomorrow’s world-changers, today. <br><br></p>',NULL,NULL,NULL,NULL,'<iframe src=\"https://player.vimeo.com/video/230004158\" width=\"640\" height=\"270\" frameborder=\"0\" allowfullscreen></iframe>','left','4',NULL,NULL,NULL),
	(121,164,1,NULL,'2019-02-08 16:45:20','2019-02-08 18:05:31','7beb5d62-3ce2-4dfa-8250-054c730be39a',NULL,'image',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(122,167,1,'Gray-Heart-Background','2019-02-08 16:48:25','2019-02-08 16:48:25','60f32dac-cf9c-48fe-843a-6f8de7557546',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(123,168,1,'Gray Background FP','2019-02-08 16:52:58','2019-02-08 16:52:58','dc539ff0-1cd4-46c2-a7b4-7ca7431df83a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(124,169,1,NULL,'2019-02-08 17:05:33','2019-02-08 17:36:56','302a5d82-1e72-4a40-8933-8e47940f39bb',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(125,170,1,NULL,'2019-02-08 17:07:08','2019-02-12 20:50:49','43ed7591-b8ac-4058-984a-57006202bf5c',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(126,171,1,NULL,'2019-02-08 17:11:44','2019-02-12 20:52:21','0235e80a-cb8c-4588-95ff-b2c0d0a2791b',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(127,172,1,NULL,'2019-02-08 17:41:19','2019-02-12 20:53:06','24126ca2-3810-4c79-bbc0-1e8e29468514',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(128,173,1,NULL,'2019-02-08 20:04:39','2019-02-27 21:20:40','087dccda-b511-442c-9907-f116da313f3b',NULL,NULL,NULL,NULL,1,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',NULL,NULL,NULL,NULL,NULL,'left',NULL,NULL,NULL,NULL),
	(129,174,1,NULL,'2019-02-12 20:39:11','2019-02-12 20:50:49','a7444167-bfc8-4b36-94e8-f1864a470bdc',NULL,NULL,NULL,NULL,1,'<p>Project Heart for 4th & 5th grade challenges your students to find the philanthropist inside and address the needs they see in the world around them as a class. In these 9 lessons, students are identifying personal passions, writing mission statements, learning about philanthropists, and completing a class service project. Each lesson averages 35 minutes, aligns with the common core state standards, and utilizes multiple best-practice teaching strategies. Students learn that by using their time, talent, and/or treasure they can be change agents who make a huge philanthropic impact.</p>',NULL,NULL,NULL,NULL,NULL,'left',NULL,NULL,NULL,NULL),
	(130,175,1,NULL,'2019-02-12 20:43:47','2019-02-12 20:52:21','e9563e8f-e5d1-47cd-bf0b-d4775e7acbe7',NULL,NULL,NULL,NULL,1,'<p>Project Heart for Middle School engages your students to think about how they can personally make a difference with their time, talents, and/ or treasure.</p>\r\n<p>In these 10 lessons, students create measurable goals for executing their plans for philanthropic impact. Each lesson averages 20-25 minutes, aligns with the common core state standards, and utilizes multiple best-practice teaching strategies. Discovering what philanthropy means to them, students learn how their personal passions will drive their philanthropic mission. Students work in small groups or individually for their student-led projects while implementing ways to affect change on a local, national, and global level.</p>',NULL,NULL,NULL,NULL,NULL,'left',NULL,NULL,NULL,NULL),
	(131,176,1,NULL,'2019-02-12 20:49:00','2019-02-12 20:53:06','a5217261-4951-4e87-a7cd-804ec3a7f320',NULL,NULL,NULL,NULL,1,'<p>Project Heart for High School provides a platform for students to explore how their passions and career goals can align with their philanthropic mission.</p>\r\n<p>In these 10 lessons, students create measurable goals for executing their plans for philanthropic impact. Each lesson averages 20-25 minutes, aligns with the common core state standards, and utilizes multiple best-practice teaching strategies. Discovering what philanthropy means to them, students learn how their personal passions will drive their philanthropic mission. Students work in small groups or individually for their student-led projects while implementing ways to affect change on a local, national, and global level.</p>',NULL,NULL,NULL,NULL,NULL,'left',NULL,NULL,NULL,NULL),
	(132,177,1,'test','2019-02-14 22:31:33','2019-02-14 22:31:33','0e0d148d-ad57-4ad3-aa72-29e7a00eeea5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(133,178,1,'Test','2019-02-14 22:37:25','2019-02-14 22:38:11','c7ab0a57-f3e0-411c-b001-fdad14331fb8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(134,179,1,NULL,'2019-02-14 22:43:46','2019-02-18 15:02:36','3ec64c6c-f736-4c2b-9641-0cecfd5a7c84',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(135,180,1,NULL,'2019-02-27 21:20:40','2019-02-27 21:20:40','e70857d5-745d-4c6d-91e4-be01fe2890af',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(136,181,1,'Blocks','2019-11-07 15:48:53','2019-11-07 15:48:53','c39ce4d8-47bc-42f5-9ff7-215af3fca380',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(137,182,1,'Blocks','2019-11-07 15:48:53','2019-11-07 15:48:53','6fb1e49c-e7de-4210-a03d-ba1fe0f14507',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(138,183,1,'Project Heart For High School','2019-11-07 15:48:53','2019-11-07 15:48:53','8894e6fd-47ab-4e11-a219-e1e060e24972',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(139,184,1,'Middle School','2019-11-07 15:48:53','2019-11-07 15:48:53','ce15e62c-fa00-4c61-a13a-d1a1a13fca0d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Project Heart For Middle School',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(140,185,1,'Project Heart For Elementary School','2019-11-07 15:48:53','2019-11-07 15:48:53','64cebec1-1709-4428-ae27-0d8f2fc4b09d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(141,186,1,'Project Heart For High School','2019-11-07 15:48:53','2019-11-07 15:48:53','fcf9c81d-8c44-452f-b9d8-9d1aa3bd792a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(142,187,1,'Middle School','2019-11-07 15:48:53','2019-11-07 15:48:53','f1baa9d7-8557-4b40-b0b6-8c6448a4797f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Project Heart For Middle School',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(143,188,1,'Project Heart For Elementary School','2019-11-07 15:48:53','2019-11-07 15:48:53','4e0b6600-2e8e-499e-8a04-aa9cff203c53',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(144,189,1,'Blocks','2019-11-07 15:48:53','2019-11-07 15:48:53','b4c85b97-0ac2-4bac-b70e-1aa3864c4b22',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(145,190,1,'Blocks','2019-11-07 15:48:53','2019-11-07 15:48:53','579356bd-35b6-430c-a040-49217886341b',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(146,191,1,'Homepage','2019-11-07 15:48:53','2019-11-07 15:48:53','aea3ee01-9318-4fe1-ad49-dd96f21a2d84',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(147,192,1,'Project Heart For High School','2019-11-07 15:48:53','2019-11-07 15:48:53','b3802ebe-641c-4960-8acc-a87b9f3968a0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(148,193,1,'Project Heart For High School','2019-11-07 15:48:53','2019-11-07 15:48:53','f6492157-f5db-4408-9527-33652823fcf5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(149,194,1,'About','2019-11-07 15:48:53','2019-11-07 15:48:53','df9f69d4-2727-4a69-b823-2cc5ae80aaad',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Inspire Your Students to Change the World',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(150,195,1,'About','2019-11-07 15:48:53','2019-11-07 15:48:53','c1baa7f5-79c0-4ccd-83cf-6e1cae5444f8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Inspire Your Students to Change the World',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(151,196,1,'Project Heart For High School','2019-11-07 15:48:53','2019-11-07 15:48:53','9ff7ae0c-56f0-4a92-b0d4-6df3f1f0c94b',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(152,197,1,'Project Heart For Elementary School','2019-11-07 15:48:53','2019-11-07 15:48:53','a1f940f6-2e55-4813-b573-3df913d62a85',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(153,198,1,'Middle School','2019-11-07 15:48:53','2019-11-07 15:48:53','c0330a36-e818-427f-a7d3-4e9db2c0b05e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Project Heart For Middle School',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(154,199,1,'Middle School','2019-11-07 15:48:53','2019-11-07 15:48:54','e7d7365f-ca12-409c-9a44-e2d591e93bb7',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Project Heart For Middle School',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(155,200,1,'High School 1','2019-11-07 15:48:54','2019-11-07 15:48:54','2ac3de95-8390-4896-a699-342bb6b12b16',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Student, 16',NULL),
	(156,201,1,'Elementary 3','2019-11-07 15:48:54','2019-11-07 15:48:54','97af0ccd-dbbb-4b37-ab5c-112446941f95',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'4/5th grade teacher at Cache la Poudre Elementary School',NULL),
	(157,202,1,'Elementary 2','2019-11-07 15:48:54','2019-11-07 15:48:54','9e29818f-1a4e-4a2b-bf2b-d77253bb3b6f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'5th grade student at Cache La Poudre Elementary',NULL),
	(158,203,1,'Middle School 1','2019-11-07 15:48:54','2019-11-07 15:48:54','90014bb3-e2b3-4e38-81f1-6af86a4a4de4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'6th/8th grade teacher',NULL),
	(159,204,1,'Elementary 1','2019-11-07 15:48:54','2019-11-07 15:48:54','ea7aa015-799b-4c3f-89d8-7914c537b2bd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'4th grade teacher at Shepardson Elementary',NULL),
	(160,205,1,'Elementary School','2019-11-07 15:48:54','2019-11-07 15:48:54','8cf329f5-3273-428c-a4f7-47f1077924c5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(161,206,1,'About','2019-11-07 15:48:54','2019-11-07 15:48:54','4e1d63aa-c726-409a-ab91-cf9d1d711759',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Inspire Your Students to Change the World',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(162,207,1,'Homepage','2019-11-07 15:48:54','2019-11-07 15:48:54','b0c90bcb-cddd-4de4-8fd3-a3f31683b35a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(163,208,1,'Homepage','2019-11-07 15:48:54','2019-11-07 15:48:54','49192418-cabe-4a52-a716-d95d89d68fda',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(164,209,1,'Homepage','2019-11-07 15:48:54','2019-11-07 15:48:54','3aa1e765-bfa9-4db3-8ec1-4cc6e706af3e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(165,210,1,'Homepage','2019-11-07 15:48:54','2019-11-07 15:48:54','ed24a4ea-4f22-4ea5-b99a-c58783e6564a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(166,211,1,'Homepage','2019-11-07 15:48:54','2019-11-07 15:48:54','ea77a41a-b86f-4310-a03e-751558c7e85c',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(167,212,1,'Homepage','2019-11-07 15:48:54','2019-11-07 15:48:54','e6b0993f-c587-4607-ac5f-ef3d7218743d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(168,213,1,'Homepage','2019-11-07 15:48:54','2019-11-07 15:48:54','d5b94842-69a8-491b-a821-6d54ae12c838',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(169,214,1,'Homepage','2019-11-07 15:48:54','2019-11-07 15:48:54','492edada-5fb2-4bd3-a27e-39aaf35b02ea',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(170,215,1,'Homepage','2019-11-07 15:48:54','2019-11-07 15:48:54','1fa8771a-c8b4-46ce-8ebe-d1d8f16141f1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(171,216,1,'Homepage','2019-11-07 15:48:54','2019-11-07 15:48:54','60e68b3d-8658-45fe-9da5-f1853af3a722',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(172,217,1,'Homepage','2019-11-07 15:48:54','2019-11-07 15:48:54','8b13beb3-e295-4a39-b4ae-83492c5d161f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(173,218,1,'Homepage','2019-11-07 15:48:54','2019-11-07 15:48:54','65d9e006-e8e6-4558-802e-ac7a2fda16a4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(174,219,1,'Blocks','2019-11-07 15:48:54','2019-11-07 15:48:54','79706a65-8434-4af8-8dab-39a78d3409c5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(175,220,1,'Homepage','2019-11-07 15:48:54','2019-11-07 15:48:54','bd1e816e-ec96-476c-b352-efd51b4af0fb',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(176,221,1,'Middle School','2019-11-07 15:48:54','2019-11-07 15:48:54','c6652c54-f213-4d55-9638-fb791cafeffc',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Project Heart For Middle School',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(177,222,1,'Middle School 1','2019-11-07 15:48:54','2019-11-07 15:48:54','3cbe6167-2ac1-442f-bd15-ba6f0f856d92',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'6th/8th grade teacher',NULL),
	(178,223,1,'Elementary 1','2019-11-07 15:48:54','2019-11-07 15:48:54','3a089bac-203a-4a20-a53c-975fb32b8258',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'4th grade teacher at Shepardson Elementary',NULL),
	(179,224,1,'Test','2019-11-07 15:48:54','2019-11-07 15:48:54','e3fb5bb2-60d1-400c-b6be-290c5f284b4f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Test Name Last Name',NULL),
	(180,225,1,'Test2','2019-11-07 15:48:54','2019-11-07 15:48:54','bab9a661-2cde-4232-b807-bf940fabb26c',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'- John Smith, Student',NULL),
	(181,226,1,'Elementary 1','2019-11-07 15:48:54','2019-11-07 15:48:54','0cdb3709-1c5f-4b3f-aebf-001d1b5d7498',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'4th grade teacher at Shepardson Elementary',NULL),
	(182,227,1,'Middle School 1','2019-11-07 15:48:54','2019-11-07 15:48:54','074e0188-2add-4d0f-944c-35cb711b465d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'6th/8th grade teacher',NULL),
	(183,228,1,'High School 1','2019-11-07 15:48:54','2019-11-07 15:48:54','042a7b82-3e49-475a-84e7-46b1da5c625f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Student, 16',NULL),
	(184,229,1,'Elementary 2','2019-11-07 15:48:54','2019-11-07 15:48:54','948309dd-8893-4bcc-bd43-5590e80eae91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'5th grade student at Cache La Poudre Elementary',NULL),
	(185,230,1,'Elementary 3','2019-11-07 15:48:54','2019-11-07 15:48:54','88eb5000-b590-4a7b-a65f-6ef14ee6c547',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'4/5th grade teacher at Cache la Poudre Elementary School',NULL),
	(186,231,1,'Elementary 3','2019-11-07 15:48:54','2019-11-07 15:48:54','b157c1e2-c354-4622-bb57-b48ee53ae49a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'4/5th grade teacher at Cache la Poudre Elementary School',NULL),
	(187,232,1,'Elementary 2','2019-11-07 15:48:55','2019-11-07 15:48:55','ec61d364-2979-4082-ae01-ea4ffc24daa7',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'5th grade student at Cache La Poudre Elementary',NULL),
	(188,233,1,'High School 1','2019-11-07 15:48:55','2019-11-07 15:48:55','3c7605b7-fd27-4283-8074-f63651224be4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Student, 16',NULL),
	(189,234,1,'Middle School 1','2019-11-07 15:48:55','2019-11-07 15:48:55','c14b8f38-7650-4327-bc07-80f360a5c596',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'6th/8th grade teacher',NULL),
	(190,235,1,'Middle School 1','2019-11-07 15:48:55','2019-11-07 15:48:55','8a85403f-0b66-437f-a963-d99ceacf8c3d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'6th/8th grade teacher',NULL),
	(191,236,1,'Elementary 1','2019-11-07 15:48:55','2019-11-07 15:48:55','13ab78af-4dd3-4cf8-b9d2-8771a092ab29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'4th grade teacher at Shepardson Elementary',NULL),
	(192,237,1,'Homepage','2019-11-07 15:48:55','2019-11-07 15:48:55','d5ba2e88-0b0d-4be3-a75f-1d84921e8d6b',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(193,238,1,'High School','2019-11-07 15:48:55','2019-11-07 15:48:55','82a26bfc-7844-4432-a45e-7f7b5a616d5f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(194,239,1,'Elementary School','2019-11-07 15:48:55','2019-11-07 15:48:55','bdcf48d0-aaa2-444d-84db-4d0c1d7c5d19',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(195,240,1,'Middle School','2019-11-07 15:48:55','2019-11-07 15:48:55','27e4fb49-84ff-4ca7-ade6-97af5424325a',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Project Heart For Middle School',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(196,241,1,'Homepage','2019-11-07 15:48:55','2019-11-07 15:48:55','29b97e1f-2a1c-4daa-b04c-651708eb3595',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(197,242,1,'Homepage','2019-11-07 15:48:55','2019-11-07 15:48:55','9e9a272b-f93d-4192-9951-be40a8ad52b8',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(198,243,1,'Homepage','2019-11-07 15:48:55','2019-11-07 15:48:55','2fce8dc4-3d3d-4442-869a-a75859f28e25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(199,244,1,'Homepage','2019-11-07 15:48:55','2019-11-07 15:48:55','00d88bdf-8cc2-4ad0-ac52-941d22e53906',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(200,245,1,'Homepage','2019-11-07 15:48:55','2019-11-07 15:48:55','3e718382-a2a1-4721-8d51-04105a620833',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(201,246,1,'High School','2019-11-07 15:48:55','2019-11-07 15:48:55','fc03eb3b-925f-41d6-8e24-e87e5982976b',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(202,247,1,'High School','2019-11-07 15:48:55','2019-11-07 15:48:55','cafc0059-4aa9-4d87-8293-26e6ba871a18',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(203,248,1,'Middle School','2019-11-07 15:48:55','2019-11-07 15:48:55','36e9d5e3-4d38-4124-97e8-c5ecfa16474e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Project Heart For Middle School',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(204,249,1,'Elementary School','2019-11-07 15:48:55','2019-11-07 15:48:55','1922d455-d5d6-47ee-8b65-7e06dddc7242',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(205,250,1,'Elementary School','2019-11-07 15:48:55','2019-11-07 15:48:55','dcd370a6-41c4-4088-9744-d8f614b92c62',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(206,251,1,'About','2019-11-07 15:48:55','2019-11-07 15:48:55','4ba0930f-32e6-44a9-b74e-753d65ce5b16',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Inspire Your Students to Change the World',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(207,252,1,'About','2019-11-07 15:48:55','2019-11-07 15:48:55','d3ea8e8d-912b-4792-81ac-ed75eb34c192',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Inspire Your Students to Change the World',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(208,253,1,'Inspire Your Students to Change the World','2019-11-07 15:48:55','2019-11-07 15:48:55','a1fcc697-90bf-4148-9366-27859b391ab3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Inspire Your Students to Change the World',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(209,254,1,'Homepage','2019-11-07 15:48:55','2019-11-07 15:48:55','e66a884b-ce62-4fa8-8f10-3cc2ba819af2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(210,255,1,'Homepage','2019-11-07 15:48:55','2019-11-07 15:48:55','ee1fdcef-e0cf-4b55-926e-513e5ad8e721',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(211,256,1,'Homepage','2019-11-07 15:48:55','2019-11-07 15:48:55','70704ac9-72a3-4f2e-854b-fb241f8f7876',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(212,257,1,'Test Blog 3','2019-11-07 15:48:55','2019-11-07 15:48:55','c794f2fe-eb88-49e3-941d-dc227171f9fc',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua'),
	(213,258,1,'Test Blog2','2019-11-07 15:48:55','2019-11-07 15:48:55','a03e651d-4226-45c2-8582-6424ec9ae7e7',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
	(214,259,1,'Test Blog','2019-11-07 15:48:55','2019-11-07 15:48:55','311ddd4f-54d3-49d5-8d35-6c8b57d86944',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'This is the text that shows up on the blog module!'),
	(215,260,1,'Contact','2019-11-07 15:48:55','2019-11-07 15:48:55','d2a35759-b99e-4857-b46b-d08bdef868c9',NULL,NULL,NULL,NULL,NULL,NULL,'Share Your Projects and Stories',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(216,261,1,'Blocks','2019-11-07 15:48:55','2019-11-07 15:48:55','8b7899ac-da13-4ac2-8385-357c598e9d62',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(217,262,1,'Test2','2019-11-07 15:48:55','2019-11-07 15:48:55','33e1b103-2b45-4f82-b869-88ba6ca745b0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'- John Smith, Student',NULL),
	(218,263,1,'Blocks','2019-11-07 15:48:55','2019-11-07 15:48:55','f371514a-8021-4560-a760-59638c628c49',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(219,264,1,'Blocks','2019-11-07 15:48:56','2019-11-07 15:48:56','4382e2ad-7c9f-4079-b3c0-d49a43c1f47b',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(220,265,1,'Blocks','2019-11-07 15:48:56','2019-11-07 15:48:56','437fd187-a4d4-42d2-97f6-98f6a93e131d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(221,266,1,'Homepage','2019-11-07 15:48:56','2019-11-07 15:48:56','6aaf6201-351e-4971-8989-5befd71acdd0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(222,267,1,'Homepage','2019-11-07 15:48:56','2019-11-07 15:48:56','3cd7a583-7d36-4378-b30e-357d48004fd4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(223,268,1,'Blocks','2019-11-07 15:48:56','2019-11-07 15:48:56','6deadf3a-24dc-4637-a177-2b1baf83994f',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(224,269,1,'Test Blog','2019-11-07 15:48:56','2019-11-07 15:48:56','55678a5b-1957-41eb-8727-a0606f8eed4d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'This is the text that shows up on the blog module!'),
	(225,270,1,'Test Blog','2019-11-07 15:48:56','2019-11-07 15:48:56','26b1507a-0ddc-436e-a262-126db7637296',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'This is the text that shows up on the blog module!'),
	(226,271,1,'Test','2019-11-07 15:48:56','2019-11-07 15:48:56','fa0e4d2a-8e29-4d5a-8a14-ba4df89216b5',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Test Name Last Name',NULL),
	(227,272,1,'Test','2019-11-07 15:48:56','2019-11-07 15:48:56','4a3ab851-aaba-4dc3-b1ed-6effd3a39864',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Test Name Last Name',NULL),
	(228,273,1,'Blocks','2019-11-07 15:48:56','2019-11-07 15:48:56','224aab16-b9b2-40aa-88c0-67cfbf0e8ff6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(229,274,1,'Blocks','2019-11-07 15:48:56','2019-11-07 15:48:56','303f751a-d448-469e-bd5e-0a1b35d40a99',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(230,275,1,'Contact','2019-11-07 15:48:56','2019-11-07 15:48:56','28230da8-3107-4669-98e4-db12466dd873',NULL,NULL,NULL,NULL,NULL,NULL,'Share Your Projects and Stories',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(231,276,1,'Blocks','2019-11-07 15:48:56','2019-11-07 15:48:56','e3c79e89-b054-482c-80c5-3db3cc543565',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(232,277,1,'Contact','2019-11-07 15:48:56','2019-11-07 15:48:56','974b3670-0df9-4d1d-b1b6-313b1473e224',NULL,NULL,NULL,NULL,NULL,NULL,'Share Your Projects and Stories',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(233,278,1,'Blocks','2019-11-07 15:48:56','2019-11-07 15:48:56','c32ce684-33e8-4a86-878f-c190fa86bcc3',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(234,279,1,'Blocks','2019-11-07 15:48:56','2019-11-07 15:48:56','b8a66766-d7fd-4e32-baf1-26a097eed872',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(235,280,1,'Blocks','2019-11-07 15:48:56','2019-11-07 15:48:56','45f4b6c3-6a41-4db6-b2ea-e66c78f2f9b2',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(236,281,1,'Blocks','2019-11-07 15:48:56','2019-11-07 15:48:56','a4297a4a-0e74-4d3d-99ad-4bb4fe9f9f57',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(237,282,1,'Privacy Policy','2019-11-07 15:48:56','2019-11-07 15:48:56','51e32595-c59e-4065-9018-4ea993a68c14',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(238,283,1,'High School','2019-11-07 15:48:56','2019-11-07 15:48:56','8ba46ba3-6154-4f54-8f97-2ef9f0ac5f91',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(239,284,1,'Middle School','2019-11-07 15:48:56','2019-11-07 15:48:56','d3696a56-c0e2-431b-b78b-20983466693d',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Project Heart For Middle School',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(240,285,1,'Elementary School','2019-11-07 15:48:56','2019-11-07 15:48:56','98003b11-989c-42ab-b180-220234657185',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(241,286,1,'About','2019-11-07 15:48:56','2019-11-07 15:48:56','edde2f72-94e6-4f37-b732-b1862ea68b59',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none','Inspire Your Students to Change the World',NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(242,287,1,'Test','2019-11-07 15:48:56','2019-11-07 15:48:56','b9734248-a30f-48fa-8153-f650e3b88675',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Test Name Last Name',NULL),
	(243,288,1,'Blog','2019-11-07 15:48:56','2019-11-07 15:48:56','e624b5e4-9f46-48a8-bdab-886820be40a9',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'blog',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(244,289,1,'Blocks','2019-11-07 15:48:56','2019-11-07 15:48:56','243687b9-fa55-4746-b2c5-96844b43cbd1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(245,290,1,'Test Blog','2019-11-07 15:48:57','2019-11-07 15:48:57','d3487d6e-b62f-4c35-85c9-1647bbb0f256',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'This is the text that shows up on the blog module!'),
	(246,291,1,'Homepage','2019-11-07 16:37:11','2019-11-07 16:37:11','e45bbd6c-47de-4aec-8fe5-708a72f7fcfd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),
	(247,292,1,'Blocks','2019-11-07 16:38:03','2019-11-07 16:38:03','4baaadf3-011c-4be1-96af-2677aee2221b',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'none',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `craft_content` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_craftidtokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_craftidtokens`;

CREATE TABLE `craft_craftidtokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `accessToken` text NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_craftidtokens_userId_fk` (`userId`),
  CONSTRAINT `craft_craftidtokens_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_craftidtokens` WRITE;
/*!40000 ALTER TABLE `craft_craftidtokens` DISABLE KEYS */;

INSERT INTO `craft_craftidtokens` (`id`, `userId`, `accessToken`, `expiryDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImE5NDE2MTFmZDE4NzBhMTkyMmIzMWZmM2Y3ZjMxOTgyNDdlOTY1OTM4OWM4ODg3OTQwZGNmM2JiODVkNTUxNGZjN2NlZGNhZjA3Zjg3ZjJiIn0.eyJhdWQiOiI2RHZFcmE3ZXFSS0xZaWM5Zm92eUQyRldGall4UndabiIsImp0aSI6ImE5NDE2MTFmZDE4NzBhMTkyMmIzMWZmM2Y3ZjMxOTgyNDdlOTY1OTM4OWM4ODg3OTQwZGNmM2JiODVkNTUxNGZjN2NlZGNhZjA3Zjg3ZjJiIiwiaWF0IjoxNTQ5NDA5NDg1LCJuYmYiOjE1NDk0MDk0ODUsImV4cCI6MTU1MjAwMTQ4NSwic3ViIjoiMTk0OTEiLCJzY29wZXMiOlsicHVyY2hhc2VQbHVnaW5zIiwiZXhpc3RpbmdQbHVnaW5zIiwidHJhbnNmZXJQbHVnaW5MaWNlbnNlIiwiZGVhc3NvY2lhdGVQbHVnaW5MaWNlbnNlIl19.Zf_Or5kQLRsgnS3HWU6WXov1hVfyBcJyqSn08VKNva-RsWkovxwCLWrh0DzQKaCpW9fhsnV1ZKCemuFfD2uGS0R62PrOvqHIRULZsvhQww98Z-FrlnqR5gmzK4y9vcG7h1Yshrh6I5JuL1A_-ZMqY2-zC1xfdKyZog9IlDespGE','2019-03-07 23:31:27','2019-02-05 23:31:27','2019-02-05 23:31:27','c8282b18-87ce-478c-82e6-a60e5a740abe');

/*!40000 ALTER TABLE `craft_craftidtokens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_deprecationerrors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_deprecationerrors`;

CREATE TABLE `craft_deprecationerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `fingerprint` varchar(255) NOT NULL,
  `lastOccurrence` datetime NOT NULL,
  `file` varchar(255) NOT NULL,
  `line` smallint(6) unsigned DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `traces` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_deprecationerrors_key_fingerprint_unq_idx` (`key`,`fingerprint`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_deprecationerrors` WRITE;
/*!40000 ALTER TABLE `craft_deprecationerrors` DISABLE KEYS */;

INSERT INTO `craft_deprecationerrors` (`id`, `key`, `fingerprint`, `lastOccurrence`, `file`, `line`, `message`, `traces`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1898,'ElementQuery::getIterator()','/home/projectheart/templates/macros/_nav_nodes.twig:16','2019-02-14 22:38:17','/home/projectheart/templates/macros/_nav_nodes.twig',16,'Looping through element queries directly has been deprecated. Use the all() function to fetch the query results before looping over them.','[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/elements/db/ElementQuery.php\",\"line\":464,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::getIterator()\\\", \\\"Looping through element queries directly has been deprecated. Us...\\\"\"},{\"objectClass\":\"verbb\\\\navigation\\\\elements\\\\db\\\\NodeQuery\",\"file\":\"/home/projectheart/storage/runtime/compiled_templates/9b/9bb8c7d1711528b488764069e9fead5652def3c0784013c0308eeb0d16f84f74.php\",\"line\":102,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"getIterator\",\"args\":null},{\"objectClass\":\"__TwigTemplate_ebd8db7aaa1f97e481e816d421c3cb8339dfed1622a2844f08e41ed0ab9bd841\",\"file\":\"/home/projectheart/storage/runtime/compiled_templates/f2/f2ead5b9d86dbd8108eedd735d1a83077df93c6362784c359cb27fdccf523758.php\",\"line\":120,\"class\":\"__TwigTemplate_ebd8db7aaa1f97e481e816d421c3cb8339dfed1622a2844f08e41ed0ab9bd841\",\"method\":\"macro_renderNode\",\"args\":\"verbb\\\\navigation\\\\elements\\\\Node\"},{\"objectClass\":\"__TwigTemplate_1555db5a64e4fa9c99be05351330023b840e670ea15be169609f9f9e7a22afbf\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Template.php\",\"line\":386,\"class\":\"__TwigTemplate_1555db5a64e4fa9c99be05351330023b840e670ea15be169609f9f9e7a22afbf\",\"method\":\"doDisplay\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...], []\"},{\"objectClass\":\"__TwigTemplate_1555db5a64e4fa9c99be05351330023b840e670ea15be169609f9f9e7a22afbf\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/twig/Template.php\",\"line\":49,\"class\":\"Twig_Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...], []\"},{\"objectClass\":\"__TwigTemplate_1555db5a64e4fa9c99be05351330023b840e670ea15be169609f9f9e7a22afbf\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Template.php\",\"line\":363,\"class\":\"craft\\\\web\\\\twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...], []\"},{\"objectClass\":\"__TwigTemplate_1555db5a64e4fa9c99be05351330023b840e670ea15be169609f9f9e7a22afbf\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/twig/Template.php\",\"line\":31,\"class\":\"Twig_Template\",\"method\":\"display\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...], []\"},{\"objectClass\":\"__TwigTemplate_1555db5a64e4fa9c99be05351330023b840e670ea15be169609f9f9e7a22afbf\",\"file\":\"/home/projectheart/storage/runtime/compiled_templates/0e/0e183ce318d0084a4ea72b24e603e3cb92e621eccc65dc4c07bfd3db55ba90f5.php\",\"line\":86,\"class\":\"craft\\\\web\\\\twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...]\"},{\"objectClass\":\"__TwigTemplate_66b98e44a87893fea17db5d0c3aba481f036f19b0d34665fad78faf40202e8be\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Template.php\",\"line\":386,\"class\":\"__TwigTemplate_66b98e44a87893fea17db5d0c3aba481f036f19b0d34665fad78faf40202e8be\",\"method\":\"doDisplay\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...], [\\\"content\\\" => [__TwigTemplate_e279e9087a38cc8e2409b40377dc3f08162cdcbfacac10f08cef9c4002aef267, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_66b98e44a87893fea17db5d0c3aba481f036f19b0d34665fad78faf40202e8be\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/twig/Template.php\",\"line\":49,\"class\":\"Twig_Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...], [\\\"content\\\" => [__TwigTemplate_e279e9087a38cc8e2409b40377dc3f08162cdcbfacac10f08cef9c4002aef267, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_66b98e44a87893fea17db5d0c3aba481f036f19b0d34665fad78faf40202e8be\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Template.php\",\"line\":363,\"class\":\"craft\\\\web\\\\twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...], [\\\"content\\\" => [__TwigTemplate_e279e9087a38cc8e2409b40377dc3f08162cdcbfacac10f08cef9c4002aef267, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_66b98e44a87893fea17db5d0c3aba481f036f19b0d34665fad78faf40202e8be\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/twig/Template.php\",\"line\":31,\"class\":\"Twig_Template\",\"method\":\"display\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...], [\\\"content\\\" => [__TwigTemplate_e279e9087a38cc8e2409b40377dc3f08162cdcbfacac10f08cef9c4002aef267, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_66b98e44a87893fea17db5d0c3aba481f036f19b0d34665fad78faf40202e8be\",\"file\":\"/home/projectheart/storage/runtime/compiled_templates/3f/3f254ccef67c617fe28270fec409fd97eef8c07960a24d220a2d21d84788e32f.php\",\"line\":31,\"class\":\"craft\\\\web\\\\twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...], [\\\"content\\\" => [__TwigTemplate_e279e9087a38cc8e2409b40377dc3f08162cdcbfacac10f08cef9c4002aef267, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_e279e9087a38cc8e2409b40377dc3f08162cdcbfacac10f08cef9c4002aef267\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Template.php\",\"line\":386,\"class\":\"__TwigTemplate_e279e9087a38cc8e2409b40377dc3f08162cdcbfacac10f08cef9c4002aef267\",\"method\":\"doDisplay\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...], [\\\"content\\\" => [__TwigTemplate_e279e9087a38cc8e2409b40377dc3f08162cdcbfacac10f08cef9c4002aef267, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_e279e9087a38cc8e2409b40377dc3f08162cdcbfacac10f08cef9c4002aef267\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/twig/Template.php\",\"line\":49,\"class\":\"Twig_Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...], [\\\"content\\\" => [__TwigTemplate_e279e9087a38cc8e2409b40377dc3f08162cdcbfacac10f08cef9c4002aef267, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_e279e9087a38cc8e2409b40377dc3f08162cdcbfacac10f08cef9c4002aef267\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Template.php\",\"line\":363,\"class\":\"craft\\\\web\\\\twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...], [\\\"content\\\" => [__TwigTemplate_e279e9087a38cc8e2409b40377dc3f08162cdcbfacac10f08cef9c4002aef267, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_e279e9087a38cc8e2409b40377dc3f08162cdcbfacac10f08cef9c4002aef267\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/twig/Template.php\",\"line\":31,\"class\":\"Twig_Template\",\"method\":\"display\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...], []\"},{\"objectClass\":\"__TwigTemplate_e279e9087a38cc8e2409b40377dc3f08162cdcbfacac10f08cef9c4002aef267\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Template.php\",\"line\":371,\"class\":\"craft\\\\web\\\\twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...]\"},{\"objectClass\":\"__TwigTemplate_e279e9087a38cc8e2409b40377dc3f08162cdcbfacac10f08cef9c4002aef267\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Environment.php\",\"line\":289,\"class\":\"Twig_Template\",\"method\":\"render\",\"args\":\"[\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/View.php\",\"line\":337,\"class\":\"Twig_Environment\",\"method\":\"render\",\"args\":\"\\\"404\\\", [\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/View.php\",\"line\":384,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"404\\\", [\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/Controller.php\",\"line\":161,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"404\\\", [\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/controllers/TemplatesController.php\",\"line\":229,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"404\\\", [\\\"message\\\" => \\\"Template not found: fonts/vendor/@fortawesome/fontawesome-free/w...\\\", \\\"code\\\" => 0, \\\"file\\\" => \\\"/home/projectheart/vendor/craftcms/cms/src/controllers/Templates...\\\", \\\"line\\\" => 70, ...]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRenderError\",\"args\":null},{\"objectClass\":null,\"file\":\"/home/projectheart/vendor/yiisoft/yii2/base/InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRenderError\\\"], []\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"/home/projectheart/vendor/yiisoft/yii2/base/Controller.php\",\"line\":157,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/Controller.php\",\"line\":109,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render-error\\\", []\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/projectheart/vendor/yiisoft/yii2/base/Module.php\",\"line\":528,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render-error\\\", []\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/Application.php\",\"line\":297,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render-error\\\", []\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/projectheart/vendor/yiisoft/yii2/web/ErrorHandler.php\",\"line\":108,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render-error\\\"\"},{\"objectClass\":\"craft\\\\web\\\\ErrorHandler\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/ErrorHandler.php\",\"line\":132,\"class\":\"yii\\\\web\\\\ErrorHandler\",\"method\":\"renderException\",\"args\":\"yii\\\\web\\\\NotFoundHttpException\"},{\"objectClass\":\"craft\\\\web\\\\ErrorHandler\",\"file\":\"/home/projectheart/vendor/yiisoft/yii2/base/ErrorHandler.php\",\"line\":111,\"class\":\"craft\\\\web\\\\ErrorHandler\",\"method\":\"renderException\",\"args\":\"yii\\\\web\\\\NotFoundHttpException\"},{\"objectClass\":\"craft\\\\web\\\\ErrorHandler\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/ErrorHandler.php\",\"line\":63,\"class\":\"yii\\\\base\\\\ErrorHandler\",\"method\":\"handleException\",\"args\":\"yii\\\\web\\\\NotFoundHttpException\"},{\"objectClass\":\"craft\\\\web\\\\ErrorHandler\",\"file\":null,\"line\":null,\"class\":\"craft\\\\web\\\\ErrorHandler\",\"method\":\"handleException\",\"args\":\"yii\\\\web\\\\NotFoundHttpException\"}]','2019-02-14 22:38:17','2019-02-14 22:38:17','77290d81-9af0-4f31-a6ba-d79c344a185c'),
	(3208,'ElementQuery::first()','/home/projectheart/templates/blocks/__block_loop.twig:15','2019-11-06 17:45:10','/home/projectheart/templates/blocks/__block_loop.twig',15,'The first() function used to query for elements is now deprecated. Use one() instead.','[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/elements/db/ElementQuery.php\",\"line\":1429,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::first()\\\", \\\"The first() function used to query for elements is now deprecate...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\AssetQuery\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Extension/Core.php\",\"line\":1626,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"first\",\"args\":null},{\"objectClass\":null,\"file\":\"/home/projectheart/vendor/craftcms/cms/src/helpers/Template.php\",\"line\":73,\"class\":null,\"method\":\"twig_get_attribute\",\"args\":\"craft\\\\web\\\\twig\\\\Environment, Twig_Source, craft\\\\elements\\\\db\\\\AssetQuery, \\\"first\\\", ...\"},{\"objectClass\":null,\"file\":\"/home/projectheart/storage/runtime/compiled_templates/c6/c676bb52a2a35c157e180d09548ecb0c3ea48ddba8ea117c2f36875706acf434.php\",\"line\":78,\"class\":\"craft\\\\helpers\\\\Template\",\"method\":\"attribute\",\"args\":\"craft\\\\web\\\\twig\\\\Environment, Twig_Source, craft\\\\elements\\\\db\\\\AssetQuery, \\\"first\\\", ...\"},{\"objectClass\":\"__TwigTemplate_4caa887185c8dcf135cd11679b82a89f68235ec5aefb61fa067be28668dff475\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Template.php\",\"line\":386,\"class\":\"__TwigTemplate_4caa887185c8dcf135cd11679b82a89f68235ec5aefb61fa067be28668dff475\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"SORT_ASC\\\" => 4, ...], []\"},{\"objectClass\":\"__TwigTemplate_4caa887185c8dcf135cd11679b82a89f68235ec5aefb61fa067be28668dff475\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/twig/Template.php\",\"line\":49,\"class\":\"Twig_Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"SORT_ASC\\\" => 4, ...], []\"},{\"objectClass\":\"__TwigTemplate_4caa887185c8dcf135cd11679b82a89f68235ec5aefb61fa067be28668dff475\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Template.php\",\"line\":363,\"class\":\"craft\\\\web\\\\twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"SORT_ASC\\\" => 4, ...], []\"},{\"objectClass\":\"__TwigTemplate_4caa887185c8dcf135cd11679b82a89f68235ec5aefb61fa067be28668dff475\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/twig/Template.php\",\"line\":31,\"class\":\"Twig_Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"SORT_ASC\\\" => 4, ...], []\"},{\"objectClass\":\"__TwigTemplate_4caa887185c8dcf135cd11679b82a89f68235ec5aefb61fa067be28668dff475\",\"file\":\"/home/projectheart/storage/runtime/compiled_templates/10/1098f9d776f7792dbfd067135594a54a5723cfff38ada8e2847a71b387a2cd5f.php\",\"line\":41,\"class\":\"craft\\\\web\\\\twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"SORT_ASC\\\" => 4, ...]\"},{\"objectClass\":\"__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Template.php\",\"line\":189,\"class\":\"__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d\",\"method\":\"block_content\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"SORT_ASC\\\" => 4, ...], [\\\"content\\\" => [__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_66b98e44a87893fea17db5d0c3aba481f036f19b0d34665fad78faf40202e8be\",\"file\":\"/home/projectheart/storage/runtime/compiled_templates/0e/0e183ce318d0084a4ea72b24e603e3cb92e621eccc65dc4c07bfd3db55ba90f5.php\",\"line\":92,\"class\":\"Twig_Template\",\"method\":\"displayBlock\",\"args\":\"\\\"content\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"SORT_ASC\\\" => 4, ...], [\\\"content\\\" => [__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_66b98e44a87893fea17db5d0c3aba481f036f19b0d34665fad78faf40202e8be\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Template.php\",\"line\":386,\"class\":\"__TwigTemplate_66b98e44a87893fea17db5d0c3aba481f036f19b0d34665fad78faf40202e8be\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"SORT_ASC\\\" => 4, ...], [\\\"content\\\" => [__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_66b98e44a87893fea17db5d0c3aba481f036f19b0d34665fad78faf40202e8be\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/twig/Template.php\",\"line\":49,\"class\":\"Twig_Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"SORT_ASC\\\" => 4, ...], [\\\"content\\\" => [__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_66b98e44a87893fea17db5d0c3aba481f036f19b0d34665fad78faf40202e8be\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Template.php\",\"line\":363,\"class\":\"craft\\\\web\\\\twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"SORT_ASC\\\" => 4, ...], [\\\"content\\\" => [__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_66b98e44a87893fea17db5d0c3aba481f036f19b0d34665fad78faf40202e8be\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/twig/Template.php\",\"line\":31,\"class\":\"Twig_Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"SORT_ASC\\\" => 4, ...], [\\\"content\\\" => [__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_66b98e44a87893fea17db5d0c3aba481f036f19b0d34665fad78faf40202e8be\",\"file\":\"/home/projectheart/storage/runtime/compiled_templates/10/1098f9d776f7792dbfd067135594a54a5723cfff38ada8e2847a71b387a2cd5f.php\",\"line\":28,\"class\":\"craft\\\\web\\\\twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"SORT_ASC\\\" => 4, ...], [\\\"content\\\" => [__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Template.php\",\"line\":386,\"class\":\"__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"SORT_ASC\\\" => 4, ...], [\\\"content\\\" => [__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/twig/Template.php\",\"line\":49,\"class\":\"Twig_Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"SORT_ASC\\\" => 4, ...], [\\\"content\\\" => [__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Template.php\",\"line\":363,\"class\":\"craft\\\\web\\\\twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"SORT_ASC\\\" => 4, ...], [\\\"content\\\" => [__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/twig/Template.php\",\"line\":31,\"class\":\"Twig_Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Template.php\",\"line\":371,\"class\":\"craft\\\\web\\\\twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_d274fdb4453c038aaf613707605449eb0a9567aede03cc41dc3974ae2da5150d\",\"file\":\"/home/projectheart/vendor/twig/twig/lib/Twig/Environment.php\",\"line\":289,\"class\":\"Twig_Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/View.php\",\"line\":337,\"class\":\"Twig_Environment\",\"method\":\"render\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/View.php\",\"line\":384,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/Controller.php\",\"line\":161,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/controllers/TemplatesController.php\",\"line\":78,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"/home/projectheart/vendor/yiisoft/yii2/base/InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"/home/projectheart/vendor/yiisoft/yii2/base/Controller.php\",\"line\":157,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/Controller.php\",\"line\":109,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/home/projectheart/vendor/yiisoft/yii2/base/Module.php\",\"line\":528,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/Application.php\",\"line\":297,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/projectheart/vendor/yiisoft/yii2/web/Application.php\",\"line\":103,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/projectheart/vendor/craftcms/cms/src/web/Application.php\",\"line\":286,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/projectheart/vendor/yiisoft/yii2/base/Application.php\",\"line\":386,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/home/projectheart/public_html/index.php\",\"line\":21,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]','2019-11-06 17:45:10','2019-11-06 17:45:10','307ab471-6b88-4f13-8548-c93c106c608a'),
	(3209,'ElementQuery::first()','/Users/nathanschmidt/Sites/premier-charters-2019/templates/blocks/__block_loop.twig:15','2019-11-07 16:33:54','/Users/nathanschmidt/Sites/premier-charters-2019/templates/blocks/__block_loop.twig',15,'The first() function used to query for elements is now deprecated. Use one() instead.','[{\"objectClass\":\"craft\\\\services\\\\Deprecator\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/craftcms/cms/src/elements/db/ElementQuery.php\",\"line\":1838,\"class\":\"craft\\\\services\\\\Deprecator\",\"method\":\"log\",\"args\":\"\\\"ElementQuery::first()\\\", \\\"The first() function used to query for elements is now deprecate...\\\"\"},{\"objectClass\":\"craft\\\\elements\\\\db\\\\AssetQuery\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/twig/twig/src/Extension/CoreExtension.php\",\"line\":1495,\"class\":\"craft\\\\elements\\\\db\\\\ElementQuery\",\"method\":\"first\",\"args\":null},{\"objectClass\":null,\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/craftcms/cms/src/helpers/Template.php\",\"line\":105,\"class\":null,\"method\":\"twig_get_attribute\",\"args\":\"craft\\\\web\\\\twig\\\\Environment, Twig\\\\Source, craft\\\\elements\\\\db\\\\AssetQuery, \\\"first\\\", ...\"},{\"objectClass\":null,\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/storage/runtime/compiled_templates/7b/7bd25cc04fc41258865e0d282ca35b4c1c1c3f48e0d19bb9fcd62f22abcded56.php\",\"line\":93,\"class\":\"craft\\\\helpers\\\\Template\",\"method\":\"attribute\",\"args\":\"craft\\\\web\\\\twig\\\\Environment, Twig\\\\Source, craft\\\\elements\\\\db\\\\AssetQuery, \\\"first\\\", ...\"},{\"objectClass\":\"__TwigTemplate_1a9b84be9b0b28b2033531ca1dbd58aff9c5053ef6aaa427e352c64f0389c8eb\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/twig/twig/src/Template.php\",\"line\":407,\"class\":\"__TwigTemplate_1a9b84be9b0b28b2033531ca1dbd58aff9c5053ef6aaa427e352c64f0389c8eb\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_1a9b84be9b0b28b2033531ca1dbd58aff9c5053ef6aaa427e352c64f0389c8eb\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/twig/twig/src/Template.php\",\"line\":380,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], []\"},{\"objectClass\":\"__TwigTemplate_1a9b84be9b0b28b2033531ca1dbd58aff9c5053ef6aaa427e352c64f0389c8eb\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/storage/runtime/compiled_templates/8c/8c3f5dfa2369d640a6b4f753d579a06ae07c66fb20677ac1dfc9c662a9b51517.php\",\"line\":63,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...]\"},{\"objectClass\":\"__TwigTemplate_6906ac91cf49ff1dc239423ef5bc29af337f7c828facc04b4a702ceb1e791b75\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/twig/twig/src/Template.php\",\"line\":184,\"class\":\"__TwigTemplate_6906ac91cf49ff1dc239423ef5bc29af337f7c828facc04b4a702ceb1e791b75\",\"method\":\"block_content\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"content\\\" => [__TwigTemplate_6906ac91cf49ff1dc239423ef5bc29af337f7c828facc04b4a702ceb1e791b75, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_9b5c68928a0ff404e6b7badd7fc8e3ae7db18ff83af37824c54211118980a8c5\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/storage/runtime/compiled_templates/8e/8e8670736ca4354731a7e55b82f716a15258073b102cab7f2857d1a17d460d1c.php\",\"line\":107,\"class\":\"Twig\\\\Template\",\"method\":\"displayBlock\",\"args\":\"\\\"content\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"content\\\" => [__TwigTemplate_6906ac91cf49ff1dc239423ef5bc29af337f7c828facc04b4a702ceb1e791b75, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_9b5c68928a0ff404e6b7badd7fc8e3ae7db18ff83af37824c54211118980a8c5\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/twig/twig/src/Template.php\",\"line\":407,\"class\":\"__TwigTemplate_9b5c68928a0ff404e6b7badd7fc8e3ae7db18ff83af37824c54211118980a8c5\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"content\\\" => [__TwigTemplate_6906ac91cf49ff1dc239423ef5bc29af337f7c828facc04b4a702ceb1e791b75, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_9b5c68928a0ff404e6b7badd7fc8e3ae7db18ff83af37824c54211118980a8c5\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/twig/twig/src/Template.php\",\"line\":380,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"content\\\" => [__TwigTemplate_6906ac91cf49ff1dc239423ef5bc29af337f7c828facc04b4a702ceb1e791b75, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_9b5c68928a0ff404e6b7badd7fc8e3ae7db18ff83af37824c54211118980a8c5\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/storage/runtime/compiled_templates/8c/8c3f5dfa2369d640a6b4f753d579a06ae07c66fb20677ac1dfc9c662a9b51517.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"content\\\" => [__TwigTemplate_6906ac91cf49ff1dc239423ef5bc29af337f7c828facc04b4a702ceb1e791b75, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_6906ac91cf49ff1dc239423ef5bc29af337f7c828facc04b4a702ceb1e791b75\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/twig/twig/src/Template.php\",\"line\":407,\"class\":\"__TwigTemplate_6906ac91cf49ff1dc239423ef5bc29af337f7c828facc04b4a702ceb1e791b75\",\"method\":\"doDisplay\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"content\\\" => [__TwigTemplate_6906ac91cf49ff1dc239423ef5bc29af337f7c828facc04b4a702ceb1e791b75, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_6906ac91cf49ff1dc239423ef5bc29af337f7c828facc04b4a702ceb1e791b75\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/twig/twig/src/Template.php\",\"line\":380,\"class\":\"Twig\\\\Template\",\"method\":\"displayWithErrorHandling\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry], \\\"view\\\" => craft\\\\web\\\\View, \\\"devMode\\\" => true, ...], [\\\"content\\\" => [__TwigTemplate_6906ac91cf49ff1dc239423ef5bc29af337f7c828facc04b4a702ceb1e791b75, \\\"block_content\\\"]]\"},{\"objectClass\":\"__TwigTemplate_6906ac91cf49ff1dc239423ef5bc29af337f7c828facc04b4a702ceb1e791b75\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/twig/twig/src/Template.php\",\"line\":392,\"class\":\"Twig\\\\Template\",\"method\":\"display\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"__TwigTemplate_6906ac91cf49ff1dc239423ef5bc29af337f7c828facc04b4a702ceb1e791b75\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/twig/twig/src/TemplateWrapper.php\",\"line\":45,\"class\":\"Twig\\\\Template\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]], []\"},{\"objectClass\":\"Twig\\\\TemplateWrapper\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/twig/twig/src/Environment.php\",\"line\":318,\"class\":\"Twig\\\\TemplateWrapper\",\"method\":\"render\",\"args\":\"[\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\twig\\\\Environment\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/craftcms/cms/src/web/View.php\",\"line\":344,\"class\":\"Twig\\\\Environment\",\"method\":\"render\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/craftcms/cms/src/web/View.php\",\"line\":393,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\View\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/craftcms/cms/src/web/Controller.php\",\"line\":243,\"class\":\"craft\\\\web\\\\View\",\"method\":\"renderPageTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/craftcms/cms/src/controllers/TemplatesController.php\",\"line\":101,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"renderTemplate\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":null,\"line\":null,\"class\":\"craft\\\\controllers\\\\TemplatesController\",\"method\":\"actionRender\",\"args\":\"\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry, \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":null,\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/yiisoft/yii2/base/InlineAction.php\",\"line\":57,\"class\":null,\"method\":\"call_user_func_array\",\"args\":\"[craft\\\\controllers\\\\TemplatesController, \\\"actionRender\\\"], [\\\"\\\", [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"yii\\\\base\\\\InlineAction\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/yiisoft/yii2/base/Controller.php\",\"line\":157,\"class\":\"yii\\\\base\\\\InlineAction\",\"method\":\"runWithParams\",\"args\":\"[\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/craftcms/cms/src/web/Controller.php\",\"line\":187,\"class\":\"yii\\\\base\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\controllers\\\\TemplatesController\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/yiisoft/yii2/base/Module.php\",\"line\":528,\"class\":\"craft\\\\web\\\\Controller\",\"method\":\"runAction\",\"args\":\"\\\"render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/craftcms/cms/src/web/Application.php\",\"line\":299,\"class\":\"yii\\\\base\\\\Module\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/yiisoft/yii2/web/Application.php\",\"line\":103,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"runAction\",\"args\":\"\\\"templates/render\\\", [\\\"template\\\" => \\\"\\\", \\\"variables\\\" => [\\\"entry\\\" => craft\\\\elements\\\\Entry]]\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/craftcms/cms/src/web/Application.php\",\"line\":284,\"class\":\"yii\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/vendor/yiisoft/yii2/base/Application.php\",\"line\":386,\"class\":\"craft\\\\web\\\\Application\",\"method\":\"handleRequest\",\"args\":\"craft\\\\web\\\\Request\"},{\"objectClass\":\"craft\\\\web\\\\Application\",\"file\":\"/Users/nathanschmidt/Sites/premier-charters-2019/web/index.php\",\"line\":21,\"class\":\"yii\\\\base\\\\Application\",\"method\":\"run\",\"args\":null}]','2019-11-07 16:33:54','2019-11-07 16:33:54','f21bb2ea-eefe-4c0b-b4af-8a43b03f6701');

/*!40000 ALTER TABLE `craft_deprecationerrors` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_drafts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_drafts`;

CREATE TABLE `craft_drafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sourceId` int(11) DEFAULT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `craft_drafts_sourceId_fk` (`sourceId`),
  KEY `craft_drafts_creatorId_fk` (`creatorId`),
  CONSTRAINT `craft_drafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_drafts_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_elementindexsettings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_elementindexsettings`;

CREATE TABLE `craft_elementindexsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_elementindexsettings_type_unq_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_elements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_elements`;

CREATE TABLE `craft_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `draftId` int(11) DEFAULT NULL,
  `revisionId` int(11) DEFAULT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_elements_dateDeleted_idx` (`dateDeleted`),
  KEY `craft_elements_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `craft_elements_type_idx` (`type`),
  KEY `craft_elements_enabled_idx` (`enabled`),
  KEY `craft_elements_archived_dateCreated_idx` (`archived`,`dateCreated`),
  KEY `craft_elements_draftId_fk` (`draftId`),
  KEY `craft_elements_revisionId_fk` (`revisionId`),
  CONSTRAINT `craft_elements_draftId_fk` FOREIGN KEY (`draftId`) REFERENCES `craft_drafts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_elements_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_elements_revisionId_fk` FOREIGN KEY (`revisionId`) REFERENCES `craft_revisions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_elements` WRITE;
/*!40000 ALTER TABLE `craft_elements` DISABLE KEYS */;

INSERT INTO `craft_elements` (`id`, `draftId`, `revisionId`, `fieldLayoutId`, `type`, `enabled`, `archived`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,NULL,NULL,NULL,'craft\\elements\\User',1,0,'2019-01-21 23:30:44','2019-01-21 23:30:44',NULL,'40fcf449-c318-4689-bcda-a33e0d2a42e9'),
	(2,NULL,NULL,110,'craft\\elements\\Entry',1,0,'2019-01-21 23:39:18','2019-11-07 16:37:10',NULL,'ecadc3f5-4950-437b-abc3-dfb4dbe783d2'),
	(3,NULL,NULL,81,'craft\\elements\\GlobalSet',1,0,'2019-01-21 23:41:01','2019-01-30 21:46:51',NULL,'bcb6ba1d-7abf-40b3-8322-e482a6607c3b'),
	(4,NULL,NULL,82,'craft\\elements\\GlobalSet',1,0,'2019-01-21 23:41:12','2019-11-07 16:41:53',NULL,'fc8ed7ef-6fba-4982-a813-e4fcdb3c9f10'),
	(5,NULL,NULL,84,'craft\\elements\\Entry',1,0,'2019-01-30 21:22:22','2019-02-05 20:28:28',NULL,'ed5bd6e6-3780-4045-8b76-28273eba4aee'),
	(6,NULL,NULL,83,'craft\\elements\\Entry',1,0,'2019-01-30 21:22:51','2019-11-07 16:38:02',NULL,'48d432c0-8d2d-470a-9fda-50cff2adcc5f'),
	(7,NULL,NULL,83,'craft\\elements\\Entry',1,0,'2019-01-30 21:23:35','2019-02-05 19:05:20','2019-11-07 15:49:26','137b1e2f-4de6-4200-af2f-1386d179a61a'),
	(8,NULL,NULL,NULL,'craft\\elements\\Category',1,0,'2019-01-30 21:25:57','2019-01-30 21:26:44','2019-01-30 23:35:35','b7348516-7866-4a30-8520-f87b461965fe'),
	(9,NULL,NULL,NULL,'craft\\elements\\Category',1,0,'2019-01-30 21:26:31','2019-01-30 21:26:31','2019-01-30 23:35:35','82051f70-20e7-4717-a074-e407885ed10b'),
	(10,NULL,NULL,85,'craft\\elements\\Entry',1,0,'2019-01-30 21:32:20','2019-02-07 22:48:51','2019-11-07 15:49:33','2fbe355a-79d1-468e-a741-0735eac92957'),
	(11,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 21:35:19','2019-01-30 21:35:19',NULL,'850f26fe-4db0-4bbe-b3da-34356224976f'),
	(12,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 21:35:20','2019-01-30 21:35:20',NULL,'851012ef-d6aa-4c43-82fd-aa1d7bbd8e7f'),
	(13,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 21:38:03','2019-02-07 22:31:30',NULL,'f4cb250b-0359-4da8-ab7a-a1bad756c490'),
	(14,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 21:38:04','2019-01-30 21:38:04',NULL,'6ee1d376-e98c-4c0e-88ed-894d90eca3f5'),
	(15,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 21:38:06','2019-01-30 21:38:06',NULL,'3c327c8b-0d45-43a6-9c84-142441ea993a'),
	(16,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 21:38:16','2019-01-30 21:38:16',NULL,'e0cafb35-0432-426f-80fe-3806b0a40256'),
	(17,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 21:38:32','2019-01-30 21:38:32',NULL,'9a654fee-7052-4416-986b-7e4268870fc2'),
	(18,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 21:38:39','2019-01-30 21:38:39',NULL,'fc725157-d4cb-472c-96a9-4633ec187377'),
	(19,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 21:38:56','2019-01-30 21:38:56',NULL,'86db79e9-da64-41a0-9e00-3623053eafe3'),
	(20,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 21:38:57','2019-01-30 21:38:57',NULL,'7c64df11-aeab-42ec-8eb6-c27d298475f6'),
	(21,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 21:38:58','2019-01-30 21:38:58',NULL,'d17d8838-6f21-4b74-8fae-6443eb66a464'),
	(22,NULL,NULL,86,'craft\\elements\\MatrixBlock',1,0,'2019-01-30 21:46:51','2019-01-30 21:46:51',NULL,'56044769-22c7-4ad8-8bee-10dd400e2c9a'),
	(23,NULL,NULL,86,'craft\\elements\\MatrixBlock',1,0,'2019-01-30 21:46:51','2019-01-30 21:46:51',NULL,'ede93ac6-8758-4426-845c-e25231035514'),
	(24,NULL,NULL,86,'craft\\elements\\MatrixBlock',1,0,'2019-01-30 21:46:51','2019-01-30 21:46:51',NULL,'97bbab48-a5b5-42cc-ad1e-cb0c1a96ea86'),
	(25,NULL,NULL,86,'craft\\elements\\MatrixBlock',1,0,'2019-01-30 21:46:51','2019-01-30 21:46:51',NULL,'82b91ee3-5d69-4e64-a1c9-3f36751f6a94'),
	(26,NULL,NULL,83,'craft\\elements\\Entry',1,0,'2019-01-30 21:49:22','2019-02-08 17:36:56','2019-11-07 15:49:26','d830a671-25f7-47b2-b327-2542598d0f2e'),
	(27,NULL,NULL,83,'craft\\elements\\Entry',1,0,'2019-01-30 21:49:45','2019-02-12 20:50:49','2019-11-07 15:49:26','ab27c513-3851-4936-8768-c415a8c06554'),
	(28,NULL,NULL,83,'craft\\elements\\Entry',1,0,'2019-01-30 21:49:56','2019-02-12 20:52:21','2019-11-07 15:49:26','98a54b54-cab7-49c2-843a-285616a7f82c'),
	(29,NULL,NULL,83,'craft\\elements\\Entry',1,0,'2019-01-30 21:50:11','2019-02-12 20:53:06','2019-11-07 15:49:26','06557c99-30c0-433c-bd60-ac9259a3c082'),
	(30,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-01-30 21:51:59','2019-11-07 16:37:10',NULL,'dba25ee0-73de-4982-b070-a6fca0ca8da0'),
	(31,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-01-30 21:52:09','2019-02-08 17:36:55','2019-11-07 16:39:19','f437c234-03cd-401b-ba99-7890285765e6'),
	(32,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-01-30 21:52:09','2019-02-05 19:05:20','2019-11-07 16:39:23','79cb9b70-fa31-4f75-b518-bb82550c12b6'),
	(33,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-01-30 21:52:10','2019-02-12 20:52:21','2019-11-07 16:39:17','f9731748-243a-466f-97b6-9f224eeff463'),
	(34,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-01-30 21:52:10','2019-02-12 20:50:49','2019-11-07 16:39:14','e090b798-5cb6-4611-ac7c-0ca942b349ac'),
	(35,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-01-30 21:52:10','2019-02-12 20:53:06','2019-11-07 16:39:21','b82f42b4-1038-4747-8e96-77e61e59c1ab'),
	(36,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-01-30 21:54:06','2019-11-07 16:37:10',NULL,'fc267f25-02f1-4505-8e3f-c146ba1004cb'),
	(37,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-01-30 21:54:11','2019-02-05 19:05:19','2019-11-07 16:39:30','e7e26e7a-6505-464c-8766-39f3023eef8a'),
	(38,NULL,NULL,83,'craft\\elements\\Entry',1,0,'2019-01-30 21:55:00','2019-01-30 21:55:00','2019-11-07 15:49:26','dd18eab7-bc07-4754-98de-e5ca210fb561'),
	(39,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',0,0,'2019-01-30 22:21:05','2019-02-08 16:29:36','2019-02-08 17:32:50','32b03dcf-5814-4aa9-b88e-d117829a1910'),
	(40,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 22:51:51','2019-01-30 22:51:51',NULL,'f2bec663-9739-46e0-85a7-90a134077c8e'),
	(41,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 23:06:44','2019-01-30 23:06:44',NULL,'bc35d38a-3ed3-4a89-8a53-d660d6a7c12e'),
	(42,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 23:10:27','2019-01-30 23:10:27',NULL,'6f6f75d8-d14d-40c0-b38b-375c55a1ce43'),
	(43,NULL,NULL,73,'benf\\neo\\elements\\Block',1,0,'2019-01-30 23:11:17','2019-02-27 21:20:40','2019-11-07 16:38:03','52cc6c6d-4842-49d7-84c3-0238cc72e7fe'),
	(44,NULL,NULL,71,'benf\\neo\\elements\\Block',1,0,'2019-01-30 23:11:17','2019-02-27 21:20:40','2019-11-07 16:38:03','260cba5d-c728-44e8-827e-a1da13e11695'),
	(45,NULL,NULL,72,'benf\\neo\\elements\\Block',1,0,'2019-01-30 23:11:17','2019-02-27 21:20:40','2019-11-07 16:38:03','c173ba55-0911-4f6c-97b7-ab7ea0218411'),
	(46,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 23:17:15','2019-01-30 23:17:15',NULL,'0d254dad-9fd9-4056-84d4-574a66b0fbf1'),
	(47,NULL,NULL,75,'benf\\neo\\elements\\Block',1,0,'2019-01-30 23:18:12','2019-02-27 21:20:40','2019-11-07 16:38:02','7f28cfdd-f260-4445-8887-9a3f02fa24c8'),
	(48,NULL,NULL,11,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-01-30 23:18:12','2019-02-27 21:20:40','2019-11-07 16:38:02','1a1855e6-9f55-454b-a905-46a9b5f7b498'),
	(49,NULL,NULL,11,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-01-30 23:18:12','2019-02-27 21:20:40','2019-11-07 16:38:02','121e7852-3ea3-4d3a-a2b2-3332e6955414'),
	(50,NULL,NULL,11,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-01-30 23:18:13','2019-02-27 21:20:40','2019-11-07 16:38:02','e1c52dc8-1e0b-481a-aa81-152b30eff6ef'),
	(51,NULL,NULL,NULL,'craft\\elements\\Category',1,0,'2019-01-30 23:35:14','2019-01-30 23:35:14',NULL,'b2b01339-37f3-4638-b262-cdac4c60674b'),
	(52,NULL,NULL,NULL,'craft\\elements\\Category',1,0,'2019-01-30 23:35:24','2019-01-30 23:35:24',NULL,'45a67c85-486b-4b51-b3ff-c3a42ef385a2'),
	(53,NULL,NULL,NULL,'craft\\elements\\Category',1,0,'2019-01-30 23:35:41','2019-01-30 23:35:41',NULL,'30259fa6-b472-41b7-819c-20ff1aa885e2'),
	(54,NULL,NULL,NULL,'craft\\elements\\Category',1,0,'2019-01-30 23:36:36','2019-01-30 23:36:36',NULL,'5a93425c-8bf4-4ba1-ac9a-fe48c6bf0d79'),
	(55,NULL,NULL,NULL,'craft\\elements\\Category',1,0,'2019-01-30 23:36:48','2019-01-30 23:36:48',NULL,'a80840d7-497f-4338-b08b-f59b756f2e90'),
	(56,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 23:43:46','2019-01-30 23:43:46',NULL,'02b8031c-3451-4243-ac2e-d203bfcaef1c'),
	(57,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 23:43:48','2019-01-30 23:43:48',NULL,'49df07f8-5270-4a86-959c-a28cf8acd407'),
	(58,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-01-30 23:43:50','2019-01-30 23:43:50',NULL,'b029755c-fb64-4846-972a-e54ec9f20d4f'),
	(59,NULL,NULL,94,'benf\\neo\\elements\\Block',1,0,'2019-01-30 23:50:25','2019-02-27 21:20:40','2019-11-07 16:38:02','58364340-de92-449e-b35d-4c0c37ad1dff'),
	(60,NULL,NULL,95,'benf\\neo\\elements\\Block',1,0,'2019-01-30 23:50:25','2019-02-27 21:20:40','2019-11-07 16:38:02','275371ba-561b-42b2-b683-73be97448d2c'),
	(61,NULL,NULL,96,'benf\\neo\\elements\\Block',1,0,'2019-01-30 23:50:25','2019-02-27 21:20:40','2019-11-07 16:38:02','cf461f41-3730-4d13-ae2d-957e5942fac3'),
	(62,NULL,NULL,97,'benf\\neo\\elements\\Block',1,0,'2019-01-30 23:50:25','2019-02-27 21:20:40','2019-11-07 16:38:02','91532edb-2018-4478-834d-453ca8695ae5'),
	(63,NULL,NULL,69,'craft\\elements\\MatrixBlock',1,0,'2019-01-30 23:50:26','2019-02-27 21:20:40','2019-11-07 16:38:02','2203b768-3f88-4ccb-9676-24a0ca08b806'),
	(64,NULL,NULL,69,'craft\\elements\\MatrixBlock',1,0,'2019-01-30 23:50:26','2019-02-27 21:20:40','2019-11-07 16:38:02','6933908d-f110-4618-b6ab-b2b67d8fb354'),
	(65,NULL,NULL,69,'craft\\elements\\MatrixBlock',1,0,'2019-01-30 23:50:26','2019-02-27 21:20:40','2019-11-07 16:38:02','d1926376-76f7-4644-9dda-a0bc993c4c1d'),
	(66,NULL,NULL,69,'craft\\elements\\MatrixBlock',1,0,'2019-01-30 23:50:26','2019-02-27 21:20:40','2019-11-07 16:38:02','f7605186-d051-4489-8927-213f209326db'),
	(67,NULL,NULL,98,'craft\\elements\\Entry',1,0,'2019-01-30 23:51:10','2019-02-05 18:47:10',NULL,'c4f3f18b-2323-4958-9460-fbf334558653'),
	(68,NULL,NULL,87,'benf\\neo\\elements\\Block',1,0,'2019-01-31 16:04:34','2019-02-27 21:20:40','2019-11-07 16:38:02','29057fc5-ab9c-4dc5-9eda-ab15236c6df8'),
	(69,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-01-31 16:04:34','2019-02-27 21:20:40','2019-11-07 16:38:02','80c3eebc-2982-45c2-9d5e-cabd1ba66a29'),
	(70,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-01-31 16:04:34','2019-02-27 21:20:40','2019-11-07 16:38:02','1ce0e64e-f26e-40ea-a296-9350386e45a4'),
	(71,NULL,NULL,91,'benf\\neo\\elements\\Block',1,0,'2019-01-31 16:06:10','2019-02-27 21:20:40','2019-11-07 16:38:02','5162ede0-d390-4560-a507-a19b646c002f'),
	(72,NULL,NULL,99,'benf\\neo\\elements\\Block',1,0,'2019-01-31 20:57:39','2019-02-08 18:05:31','2019-11-07 16:37:11','8af04ee8-8610-49d3-8640-0659ae4047bf'),
	(73,NULL,NULL,85,'craft\\elements\\Entry',1,0,'2019-02-01 20:56:00','2019-02-07 22:48:43','2019-11-07 15:49:33','dcdd86fd-d065-45c9-92da-bbff122f7e05'),
	(74,NULL,NULL,100,'benf\\neo\\elements\\Block',1,0,'2019-02-05 17:49:55','2019-02-27 21:20:40','2019-11-07 16:38:02','d14066d0-291f-4770-b702-102fcbbeded8'),
	(75,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-05 17:49:55','2019-02-27 21:20:40','2019-11-07 16:38:02','959e86f1-f129-4807-b193-e64c3ee380cb'),
	(76,NULL,NULL,100,'benf\\neo\\elements\\Block',1,0,'2019-02-05 18:52:38','2019-02-05 20:28:28',NULL,'bbbfc4ad-300c-400a-a344-f7066d96e356'),
	(77,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-05 18:52:38','2019-02-05 20:28:28',NULL,'7efbc364-55d6-4af6-9c0c-a7ea5435c503'),
	(78,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-05 18:52:38','2019-02-05 20:28:28',NULL,'92efd57f-3c43-4a6b-85ae-b114613057db'),
	(79,NULL,NULL,84,'craft\\elements\\Entry',1,0,'2019-02-05 20:29:19','2019-02-05 20:29:19',NULL,'3e789033-8e1e-4618-9b84-7bf5dc95c3fa'),
	(80,NULL,NULL,100,'benf\\neo\\elements\\Block',1,0,'2019-02-05 20:29:19','2019-02-05 20:29:19',NULL,'94e3617f-fe57-4967-8356-c5ad3d498497'),
	(81,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-05 20:29:19','2019-02-05 20:29:19',NULL,'3afbce73-3205-4002-b537-cc6f4264d032'),
	(82,NULL,NULL,84,'craft\\elements\\Entry',1,0,'2019-02-05 20:30:08','2019-02-05 20:30:08',NULL,'6bf81670-a1ee-4ada-9466-603ce7f3ef74'),
	(83,NULL,NULL,100,'benf\\neo\\elements\\Block',1,0,'2019-02-05 20:30:08','2019-02-05 20:30:08',NULL,'c299df5e-25bd-4ba3-a532-0beeb39e2cb0'),
	(84,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-05 20:30:08','2019-02-05 20:30:08',NULL,'5667951c-99f9-4483-bb19-0ae93836c97c'),
	(85,NULL,NULL,101,'benf\\neo\\elements\\Block',1,0,'2019-02-07 20:26:26','2019-02-07 20:27:16','2019-02-07 20:33:23','05ab6777-c648-4786-9997-1bb7716aa77c'),
	(86,NULL,NULL,101,'benf\\neo\\elements\\Block',1,0,'2019-02-07 20:33:23','2019-02-07 20:33:23','2019-02-07 20:37:50','905ec2e4-dff0-40fe-b5b3-b8201710c7bd'),
	(87,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 20:58:43','2019-02-07 20:58:43',NULL,'5dddb80d-489b-43a7-83ce-a266a6ba1b10'),
	(88,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 20:58:44','2019-02-07 20:58:44',NULL,'628586c6-cefb-4ddd-8dae-28414d13c494'),
	(89,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 20:58:45','2019-02-07 20:58:45',NULL,'4bfb219b-d91f-42ee-8914-797ac7081445'),
	(90,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 20:58:47','2019-02-07 20:58:47',NULL,'0f8c88cc-650f-4778-bd39-3e817860e9ed'),
	(91,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 20:58:47','2019-02-07 20:58:47',NULL,'d14c8337-7a88-4643-94ee-bc7f17da128e'),
	(92,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 20:58:48','2019-02-07 20:58:48',NULL,'df137d3b-640d-4cca-9566-f0969d693e89'),
	(93,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 20:58:50','2019-02-07 20:58:50',NULL,'d4fad406-7adb-46ea-aa4f-73c90c353ef6'),
	(94,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 20:58:51','2019-02-07 20:58:51',NULL,'dc521fdf-8774-4b06-ab75-1bb1ff3d376b'),
	(95,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 20:58:52','2019-02-07 20:58:52',NULL,'c304deb5-d336-41a9-8b0a-1c1317e78358'),
	(96,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 20:58:53','2019-02-07 20:58:53',NULL,'18344c69-2b0b-4df2-8c16-221945b012cb'),
	(97,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 20:58:55','2019-02-07 20:58:55',NULL,'c21947b1-c7b8-4583-971d-e9d82d70f02a'),
	(98,NULL,NULL,123,'benf\\neo\\elements\\Block',1,0,'2019-02-07 21:02:36','2019-02-08 17:36:56',NULL,'231ae6ab-e4e9-4f26-aff6-92b63cf00d29'),
	(99,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 21:02:36','2019-02-08 17:36:56',NULL,'13e3eba3-13dc-4a92-9376-e4c34892a9b4'),
	(100,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 21:02:36','2019-02-08 17:36:56',NULL,'41d1a5bd-07a5-4699-a174-68620784894a'),
	(101,NULL,NULL,129,'benf\\neo\\elements\\Block',1,0,'2019-02-07 21:02:36','2019-02-08 17:36:56',NULL,'1f1f3eec-ee2b-4a73-baa1-ce51623ddf24'),
	(102,NULL,NULL,123,'benf\\neo\\elements\\Block',1,0,'2019-02-07 21:16:26','2019-02-08 17:16:37','2019-02-12 20:39:11','a4f61d47-07b7-4241-9618-7678033c31f5'),
	(103,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 21:16:26','2019-02-08 17:16:37','2019-02-12 20:39:11','057788e4-8f94-49de-8c20-ce2833e3c7f4'),
	(104,NULL,NULL,128,'benf\\neo\\elements\\Block',1,0,'2019-02-07 21:16:26','2019-02-07 21:16:26','2019-02-07 21:17:08','3d750529-ec67-442f-a0d2-49ba84cc981c'),
	(105,NULL,NULL,131,'benf\\neo\\elements\\Block',1,0,'2019-02-07 21:17:08','2019-02-12 20:39:11','2019-02-12 20:50:49','cd43524d-55ec-4d85-854c-c500f587ef49'),
	(106,NULL,NULL,123,'benf\\neo\\elements\\Block',1,0,'2019-02-07 21:18:38','2019-02-08 17:15:21','2019-02-12 20:43:47','111cecaf-cd77-450e-822e-a55d119be0f2'),
	(107,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 21:18:38','2019-02-08 17:15:21','2019-02-12 20:43:47','3c146970-8d1e-4712-a4f1-674cff1bf413'),
	(108,NULL,NULL,131,'benf\\neo\\elements\\Block',1,0,'2019-02-07 21:18:38','2019-02-12 20:43:47','2019-02-12 20:52:21','c707ff78-7b3f-4391-a6c9-acb5bed3e51e'),
	(109,NULL,NULL,123,'benf\\neo\\elements\\Block',1,0,'2019-02-07 21:20:38','2019-02-08 17:44:24','2019-02-12 20:49:00','f805f129-42a0-4a60-9c14-a5f68fdb44c3'),
	(110,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 21:20:38','2019-02-08 17:44:24','2019-02-12 20:49:00','46c04fde-36b6-48eb-8084-f9a72fee9062'),
	(111,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 21:20:38','2019-02-08 17:44:24','2019-02-12 20:49:00','6f3aa3de-efde-4bc1-b6a5-f1594012cfc1'),
	(112,NULL,NULL,131,'benf\\neo\\elements\\Block',1,0,'2019-02-07 21:23:41','2019-02-12 20:49:00','2019-02-12 20:53:06','fbd967e9-94bf-4fb3-9e94-4b58ec0d9c4b'),
	(113,NULL,NULL,123,'benf\\neo\\elements\\Block',1,0,'2019-02-07 21:30:45','2019-02-08 18:05:31','2019-11-07 16:37:11','d13986dd-8acd-4fc4-94e2-dab8ee5113a5'),
	(114,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 21:30:45','2019-02-08 18:05:31','2019-11-07 16:37:10','6f672aa5-1d27-41c8-a618-c4a8a5277c45'),
	(115,NULL,NULL,127,'benf\\neo\\elements\\Block',1,0,'2019-02-07 21:37:48','2019-02-08 18:05:31','2019-11-07 16:37:10','5905b07a-32a7-4b77-a62b-bd4f3a188f6b'),
	(116,NULL,NULL,11,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 21:37:48','2019-02-08 18:05:31','2019-11-07 16:37:10','e75b82ef-0114-4ea5-b525-eaab795d8787'),
	(117,NULL,NULL,11,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 21:37:48','2019-02-08 18:05:31','2019-11-07 16:37:10','c7f6486f-b1d9-431c-ba99-56bc545ad2ee'),
	(118,NULL,NULL,11,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 21:37:48','2019-02-08 18:05:31','2019-11-07 16:37:10','1b0e5e42-e551-42f9-8ef4-d5637ac99333'),
	(119,NULL,NULL,129,'benf\\neo\\elements\\Block',1,0,'2019-02-07 21:44:10','2019-02-08 18:05:31','2019-11-07 16:37:10','8a0b9f6c-fef7-4453-bd68-d5f11dd73457'),
	(120,NULL,NULL,123,'benf\\neo\\elements\\Block',1,0,'2019-02-07 21:44:10','2019-02-08 18:05:31','2019-11-07 16:37:10','dd158925-92c8-47d9-9626-9d1687832298'),
	(121,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 21:44:10','2019-02-08 18:05:31','2019-11-07 16:37:10','3a63eeda-fa3a-4927-8e58-45cb8a3fa922'),
	(122,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 21:44:10','2019-02-08 18:05:31','2019-11-07 16:37:10','f081a7fa-b305-4808-9dca-1f45b46a988e'),
	(123,NULL,NULL,123,'benf\\neo\\elements\\Block',1,0,'2019-02-07 21:44:10','2019-02-08 18:05:32','2019-11-07 16:37:10','b3512425-83cc-42f5-ae87-057610002b5e'),
	(124,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 21:44:10','2019-02-08 18:05:32','2019-11-07 16:37:10','427ee6b3-79f5-45f6-af6a-81e723175e18'),
	(125,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 21:44:10','2019-02-08 18:05:32','2019-11-07 16:37:10','d3aa8234-575b-4212-97cc-58be89a3d118'),
	(126,NULL,NULL,130,'benf\\neo\\elements\\Block',1,0,'2019-02-07 22:07:10','2019-02-08 18:05:32','2019-11-07 16:37:10','553475d8-5bcb-4551-841e-66074b8d0a1f'),
	(127,NULL,NULL,131,'benf\\neo\\elements\\Block',1,0,'2019-02-07 22:07:10','2019-02-08 18:05:32','2019-11-07 16:37:10','f90ca60e-8db1-4e57-9084-4b019539f831'),
	(128,NULL,NULL,132,'benf\\neo\\elements\\Block',1,0,'2019-02-07 22:07:10','2019-02-08 18:05:32','2019-11-07 16:37:10','f2ec1396-d16e-4074-a690-c03c475dab67'),
	(129,NULL,NULL,69,'craft\\elements\\MatrixBlock',1,0,'2019-02-07 22:07:10','2019-02-08 18:05:32','2019-11-07 16:37:10','b4bfca8d-101e-417b-ae04-fd6feaf6a109'),
	(130,NULL,NULL,69,'craft\\elements\\MatrixBlock',1,0,'2019-02-07 22:07:10','2019-02-08 18:05:32','2019-11-07 16:37:10','c6f47391-3645-43d0-aeb6-8ecfd3b7f4dc'),
	(131,NULL,NULL,69,'craft\\elements\\MatrixBlock',1,0,'2019-02-07 22:07:10','2019-02-08 18:05:32','2019-11-07 16:37:10','37c41d27-1f05-4b6a-83eb-41414adade4e'),
	(132,NULL,NULL,69,'craft\\elements\\MatrixBlock',1,0,'2019-02-07 22:07:10','2019-02-08 18:05:32','2019-11-07 16:37:10','df684993-9d05-4334-adc4-1b29e0e3b69c'),
	(133,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 22:17:58','2019-02-07 22:17:58',NULL,'debd39f5-d12c-4af2-a8f8-95d0b7033643'),
	(134,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 22:17:59','2019-02-07 22:26:50',NULL,'542e5eb5-a6a4-47d8-a374-41bf06300fd6'),
	(135,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 22:18:00','2019-02-07 22:18:00',NULL,'88af6df1-c5a5-4481-8b90-86b3934fdb19'),
	(136,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 22:18:01','2019-02-07 22:18:01',NULL,'5ae69369-28b0-47c1-9b4f-430c11dbc6f0'),
	(137,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 22:18:02','2019-02-07 22:18:02',NULL,'9ee6f270-03c5-4b7d-a76f-0353273c5f23'),
	(138,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 22:18:03','2019-02-07 22:24:47',NULL,'045b8f8b-7316-4716-a4d5-253aaf7b21d1'),
	(139,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 22:19:35','2019-02-08 17:15:21','2019-02-12 20:43:47','2f2d352c-5874-402b-be38-6e4797f6f8b9'),
	(140,NULL,NULL,126,'benf\\neo\\elements\\Block',1,0,'2019-02-07 22:19:35','2019-02-12 20:52:21',NULL,'2de96d70-2a11-41ec-9d15-24f4b3b9438d'),
	(141,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-07 22:22:58','2019-02-08 17:16:37','2019-02-12 20:39:11','c5b81ce4-5bb3-4442-a85a-6b133a53db02'),
	(142,NULL,NULL,126,'benf\\neo\\elements\\Block',1,0,'2019-02-07 22:22:58','2019-02-12 20:50:49',NULL,'69e70580-8ce4-4bc6-9523-acd631996265'),
	(143,NULL,NULL,126,'benf\\neo\\elements\\Block',1,0,'2019-02-07 22:24:54','2019-02-12 20:53:06',NULL,'f410af74-3b50-4eed-8b01-f6336a1502ff'),
	(144,NULL,NULL,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:39:59','2019-02-08 17:07:53','2019-11-07 15:49:33','64a2daab-1ceb-458c-a4c6-fb38c4e41931'),
	(145,NULL,NULL,NULL,'craft\\elements\\Category',1,0,'2019-02-07 22:41:16','2019-02-07 22:41:16',NULL,'33edb6eb-84c4-4def-a733-02602ee28379'),
	(146,NULL,NULL,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:41:24','2019-02-08 17:09:37','2019-11-07 15:49:33','cbe236c0-36f6-4b0c-b01c-e6f75e3f06e5'),
	(147,NULL,NULL,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:42:53','2019-02-08 17:11:34','2019-11-07 15:49:33','f5e98771-75b2-4e4d-a6a5-35b74837318f'),
	(148,NULL,NULL,NULL,'craft\\elements\\Category',1,0,'2019-02-07 22:44:08','2019-02-07 22:44:08',NULL,'a265697c-fb90-46bc-9d37-5e685e01891a'),
	(149,NULL,NULL,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:44:14','2019-02-08 17:10:47','2019-11-07 15:49:33','416c2b62-aba6-4d1b-b055-32fd9b380009'),
	(150,NULL,NULL,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:45:07','2019-02-08 17:11:02','2019-11-07 15:49:33','cc048d72-6161-48f8-ba8c-99bf4069a46a'),
	(151,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-07 22:52:44','2019-02-07 22:52:44',NULL,'388da44b-1609-4349-95ac-b2dc78cef32f'),
	(152,NULL,NULL,134,'benf\\neo\\elements\\Block',1,0,'2019-02-08 15:47:39','2019-02-08 18:05:31','2019-11-07 16:37:10','d650b85f-0532-47fb-a91f-bef7cccebf8f'),
	(153,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-08 15:47:39','2019-02-08 18:05:31','2019-11-07 16:37:10','3e7b283e-55e0-4c4b-b951-6d40cba567f6'),
	(154,NULL,NULL,134,'benf\\neo\\elements\\Block',1,0,'2019-02-08 15:52:54','2019-02-08 18:05:31','2019-11-07 16:37:10','cbb603dc-9674-40c0-90b8-ff8ba4c4ead0'),
	(155,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-08 15:52:54','2019-02-08 18:05:31','2019-11-07 16:37:10','d8f525f1-533b-433e-86e2-c043d538d033'),
	(156,NULL,NULL,134,'benf\\neo\\elements\\Block',1,0,'2019-02-08 15:52:54','2019-02-08 18:05:32','2019-11-07 16:37:10','0516015b-b8d2-4257-8f8e-db05a5585462'),
	(157,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-08 15:52:54','2019-02-08 18:05:32','2019-11-07 16:37:10','4f1fa049-30d1-4998-b8f3-89d5af7c2756'),
	(158,NULL,NULL,134,'benf\\neo\\elements\\Block',1,0,'2019-02-08 15:52:55','2019-02-08 18:05:32','2019-11-07 16:37:10','b31bc08d-ef98-4967-be68-98da287c51fc'),
	(159,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-08 15:52:55','2019-02-08 18:05:32','2019-11-07 16:37:10','0e8fd74d-b2d0-49f6-901a-7e410020c9d1'),
	(160,NULL,NULL,134,'benf\\neo\\elements\\Block',1,0,'2019-02-08 15:54:32','2019-02-08 18:05:32','2019-11-07 16:37:10','ca53008a-91f2-48e1-bbb9-f5a37c1e667d'),
	(161,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-08 15:54:32','2019-02-08 18:05:32','2019-11-07 16:37:10','e4535940-8085-4a0a-8539-df6211f22ea6'),
	(162,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-08 16:41:02','2019-02-08 16:41:02',NULL,'cae89203-e7b7-4405-b9b2-d4ba67f891e9'),
	(163,NULL,NULL,135,'benf\\neo\\elements\\Block',1,0,'2019-02-08 16:42:15','2019-02-08 16:42:15','2019-02-08 16:45:21','a506281a-1d34-409c-9906-a56c6e6edea8'),
	(164,NULL,NULL,134,'benf\\neo\\elements\\Block',1,0,'2019-02-08 16:45:20','2019-02-08 18:05:31','2019-11-07 16:37:11','84101954-17cd-4d16-b399-4660d2a5e119'),
	(165,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-08 16:45:20','2019-02-08 18:05:31','2019-11-07 16:37:11','9040caaf-3cef-4675-b8a4-e0fd778f80ab'),
	(166,NULL,NULL,10,'verbb\\supertable\\elements\\SuperTableBlockElement',1,0,'2019-02-08 16:45:20','2019-02-08 18:05:31','2019-11-07 16:37:11','cfa7d13b-f6b3-42ac-b2bd-40b9e2e16810'),
	(167,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-08 16:48:25','2019-02-08 16:48:25',NULL,'24d6db0b-40e4-48af-beaa-34e3f61cb9b4'),
	(168,NULL,NULL,NULL,'craft\\elements\\Asset',1,0,'2019-02-08 16:52:58','2019-02-08 16:52:58',NULL,'c386cf53-ff9b-413e-ac9e-e1cdd4dd09ef'),
	(169,NULL,NULL,142,'benf\\neo\\elements\\Block',1,0,'2019-02-08 17:05:33','2019-02-08 17:36:56',NULL,'8ee62335-2bd0-4ad7-9d75-61877a3d6ca7'),
	(170,NULL,NULL,140,'benf\\neo\\elements\\Block',1,0,'2019-02-08 17:07:08','2019-02-12 20:50:49',NULL,'0f20ba9c-120e-4419-92f9-4bf2be35f4ca'),
	(171,NULL,NULL,140,'benf\\neo\\elements\\Block',1,0,'2019-02-08 17:11:44','2019-02-12 20:52:21',NULL,'3731e52c-55fc-479a-b6ea-30dad8e287ed'),
	(172,NULL,NULL,140,'benf\\neo\\elements\\Block',1,0,'2019-02-08 17:41:19','2019-02-12 20:53:06',NULL,'731b14b6-1fcd-4f4d-bcfc-1d5a73ef716a'),
	(173,NULL,NULL,155,'benf\\neo\\elements\\Block',1,0,'2019-02-08 20:04:39','2019-02-27 21:20:40','2019-11-07 16:38:03','1c2ae5a5-9269-438e-a6bc-c0608136c78d'),
	(174,NULL,NULL,155,'benf\\neo\\elements\\Block',1,0,'2019-02-12 20:39:11','2019-02-12 20:50:49',NULL,'45a80569-221c-4c4d-b319-8812b9876d48'),
	(175,NULL,NULL,155,'benf\\neo\\elements\\Block',1,0,'2019-02-12 20:43:47','2019-02-12 20:52:21',NULL,'ede3e359-6676-4ac2-ab51-59f1fae74839'),
	(176,NULL,NULL,155,'benf\\neo\\elements\\Block',1,0,'2019-02-12 20:49:00','2019-02-12 20:53:06',NULL,'9e22f345-82a7-4bf2-86c4-452f99e02bd9'),
	(177,NULL,NULL,NULL,'craft\\elements\\Category',1,0,'2019-02-14 22:31:33','2019-02-14 22:31:33','2019-02-14 22:31:51','f003553e-523e-4d00-8606-802044e02b08'),
	(178,NULL,NULL,NULL,'verbb\\navigation\\elements\\Node',1,0,'2019-02-14 22:37:25','2019-02-14 22:38:11','2019-02-14 22:38:24','96856c33-fafe-43e2-b566-da76cb5430de'),
	(179,NULL,NULL,NULL,'craft\\elements\\User',1,0,'2019-02-14 22:43:46','2019-02-18 15:02:36','2019-11-07 16:42:10','28d41a54-dc93-471e-b530-11253bd38b14'),
	(180,NULL,NULL,151,'benf\\neo\\elements\\Block',1,0,'2019-02-27 21:20:40','2019-02-27 21:20:40','2019-11-07 16:38:02','9b623e04-29bc-4f3f-b1e5-6a9691e1dac0'),
	(181,NULL,1,83,'craft\\elements\\Entry',1,0,'2019-02-27 21:20:40','2019-11-07 15:48:53',NULL,'99125c7c-acfc-4613-99f3-9bbebd60e5b4'),
	(182,NULL,2,83,'craft\\elements\\Entry',1,0,'2019-02-14 22:28:47','2019-11-07 15:48:53',NULL,'cc80590f-abae-415c-9ce0-8fdcdc460834'),
	(183,NULL,3,83,'craft\\elements\\Entry',1,0,'2019-02-12 20:53:06','2019-11-07 15:48:53','2019-11-07 15:49:26','4dc5138e-325c-4a37-8d0e-75a48e02171c'),
	(184,NULL,4,83,'craft\\elements\\Entry',1,0,'2019-02-12 20:52:21','2019-11-07 15:48:53','2019-11-07 15:49:26','40c093da-8fb5-4fcf-bcb6-2ab623becb32'),
	(185,NULL,5,83,'craft\\elements\\Entry',1,0,'2019-02-12 20:50:50','2019-11-07 15:48:53','2019-11-07 15:49:26','d60f60b2-3281-4591-a43a-4fc62d4c35e2'),
	(186,NULL,6,83,'craft\\elements\\Entry',1,0,'2019-02-12 20:49:00','2019-11-07 15:48:53','2019-11-07 15:49:26','ad27a342-f19c-4d0d-9549-270eb480f1c6'),
	(187,NULL,7,83,'craft\\elements\\Entry',1,0,'2019-02-12 20:43:47','2019-11-07 15:48:53','2019-11-07 15:49:26','65acc3d4-528e-43cf-b3c7-ef36b1005fbb'),
	(188,NULL,8,83,'craft\\elements\\Entry',1,0,'2019-02-12 20:39:11','2019-11-07 15:48:53','2019-11-07 15:49:26','e2746331-c448-4ea9-9d94-d83a2c79a6ef'),
	(189,NULL,9,83,'craft\\elements\\Entry',1,0,'2019-02-08 20:13:05','2019-11-07 15:48:53',NULL,'e6a2cca5-8f5c-429b-aab0-316f11196d38'),
	(190,NULL,10,83,'craft\\elements\\Entry',1,0,'2019-02-08 20:04:40','2019-11-07 15:48:53',NULL,'d4225907-fa3a-4de3-9c98-e33e70e9b445'),
	(191,NULL,11,110,'craft\\elements\\Entry',1,0,'2019-02-08 18:05:32','2019-11-07 15:48:53',NULL,'4bd2ce74-79b4-42a1-ab32-ab3d6d87f044'),
	(192,NULL,12,83,'craft\\elements\\Entry',1,0,'2019-02-08 17:44:24','2019-11-07 15:48:53','2019-11-07 15:49:26','b08031e0-da1e-49a8-aa0e-6dbc8b8fb31f'),
	(193,NULL,13,83,'craft\\elements\\Entry',1,0,'2019-02-08 17:41:19','2019-11-07 15:48:53','2019-11-07 15:49:26','e3426232-b2b4-4711-a845-b6118f0efb9d'),
	(194,NULL,14,83,'craft\\elements\\Entry',1,0,'2019-02-08 17:36:56','2019-11-07 15:48:53','2019-11-07 15:49:26','e1d6f605-ab00-4f21-bee4-b1a8d9482e62'),
	(195,NULL,15,83,'craft\\elements\\Entry',1,0,'2019-02-08 17:36:28','2019-11-07 15:48:53','2019-11-07 15:49:26','53818d95-1ebc-464e-97c7-f292778a575b'),
	(196,NULL,16,83,'craft\\elements\\Entry',1,0,'2019-02-08 17:17:43','2019-11-07 15:48:53','2019-11-07 15:49:26','da76354c-e1b1-4c55-b373-adaae0623226'),
	(197,NULL,17,83,'craft\\elements\\Entry',1,0,'2019-02-08 17:16:37','2019-11-07 15:48:53','2019-11-07 15:49:26','48d2f6ef-ab7c-4cfc-a4f4-783f5f7d035d'),
	(198,NULL,18,83,'craft\\elements\\Entry',1,0,'2019-02-08 17:15:21','2019-11-07 15:48:53','2019-11-07 15:49:26','239a5d06-0061-4b41-bf2a-dc094f6ccb6c'),
	(199,NULL,19,83,'craft\\elements\\Entry',1,0,'2019-02-08 17:11:44','2019-11-07 15:48:54','2019-11-07 15:49:26','a19166fd-962b-42f2-980f-02ce9c1685d6'),
	(200,NULL,20,85,'craft\\elements\\Entry',1,0,'2019-02-08 17:11:34','2019-11-07 15:48:54','2019-11-07 15:49:33','9458d6a7-d9e8-4c3c-8e6e-a207821400e2'),
	(201,NULL,21,85,'craft\\elements\\Entry',1,0,'2019-02-08 17:11:02','2019-11-07 15:48:54','2019-11-07 15:49:33','f1550912-1620-4d98-8683-7b38205019f6'),
	(202,NULL,22,85,'craft\\elements\\Entry',1,0,'2019-02-08 17:10:47','2019-11-07 15:48:54','2019-11-07 15:49:33','deb6063d-d462-4320-b1de-cbdd1cc139bf'),
	(203,NULL,23,85,'craft\\elements\\Entry',1,0,'2019-02-08 17:09:37','2019-11-07 15:48:54','2019-11-07 15:49:33','31c39153-06bc-4492-bc25-7b20beb37808'),
	(204,NULL,24,85,'craft\\elements\\Entry',1,0,'2019-02-08 17:07:53','2019-11-07 15:48:54','2019-11-07 15:49:33','c9cd9edf-1cdd-47b1-bb9b-e1ef0b30b7b8'),
	(205,NULL,25,83,'craft\\elements\\Entry',1,0,'2019-02-08 17:07:08','2019-11-07 15:48:54','2019-11-07 15:49:26','3534b289-dcd6-491d-a65c-a0c5d4c83bfd'),
	(206,NULL,26,83,'craft\\elements\\Entry',1,0,'2019-02-08 17:05:33','2019-11-07 15:48:54','2019-11-07 15:49:26','3247356c-4070-4d71-8a7b-3ae07591e763'),
	(207,NULL,27,110,'craft\\elements\\Entry',1,0,'2019-02-08 16:58:07','2019-11-07 15:48:54',NULL,'5c8a9dd8-ce7b-4aef-94ae-2397007141a9'),
	(208,NULL,28,110,'craft\\elements\\Entry',1,0,'2019-02-08 16:53:48','2019-11-07 15:48:54',NULL,'891f8b64-eeae-4d5d-8325-2ec1a678af46'),
	(209,NULL,29,110,'craft\\elements\\Entry',1,0,'2019-02-08 16:48:46','2019-11-07 15:48:54',NULL,'d2f4fa93-0070-4454-af49-7c840c5df07a'),
	(210,NULL,30,110,'craft\\elements\\Entry',1,0,'2019-02-08 16:47:00','2019-11-07 15:48:54',NULL,'cd71f448-7de7-4972-8454-6e20a51d141b'),
	(211,NULL,31,110,'craft\\elements\\Entry',1,0,'2019-02-08 16:45:21','2019-11-07 15:48:54',NULL,'301ae185-1968-44ec-98a5-335fb571d2c9'),
	(212,NULL,32,110,'craft\\elements\\Entry',1,0,'2019-02-08 16:42:16','2019-11-07 15:48:54',NULL,'67a81f9c-f982-4964-ad2e-8a82e3ca40e9'),
	(213,NULL,33,110,'craft\\elements\\Entry',1,0,'2019-02-08 15:59:58','2019-11-07 15:48:54',NULL,'d16df6a1-aa6c-4756-9aa7-f50d6379e915'),
	(214,NULL,34,110,'craft\\elements\\Entry',1,0,'2019-02-08 15:57:41','2019-11-07 15:48:54',NULL,'3b7742e4-9692-4b8e-90c5-e89ef2343ebc'),
	(215,NULL,35,110,'craft\\elements\\Entry',1,0,'2019-02-08 15:54:32','2019-11-07 15:48:54',NULL,'5db6e63b-2b94-4400-9c54-ecbd6d2aacdd'),
	(216,NULL,36,110,'craft\\elements\\Entry',1,0,'2019-02-08 15:52:55','2019-11-07 15:48:54',NULL,'821ea881-36f9-4c2c-85a1-50aee0d59c77'),
	(217,NULL,37,110,'craft\\elements\\Entry',1,0,'2019-02-08 15:47:40','2019-11-07 15:48:54',NULL,'f1ca628f-9d17-4e22-9466-1b674012bd0e'),
	(218,NULL,38,110,'craft\\elements\\Entry',1,0,'2019-02-08 15:45:05','2019-11-07 15:48:54',NULL,'8906b548-4e0f-4b58-9fa3-fb68bb9ad117'),
	(219,NULL,39,83,'craft\\elements\\Entry',1,0,'2019-02-07 23:55:39','2019-11-07 15:48:54',NULL,'d25ea6ee-7f17-4917-a40a-e730cfaa1d9a'),
	(220,NULL,40,110,'craft\\elements\\Entry',1,0,'2019-02-07 22:52:52','2019-11-07 15:48:54',NULL,'eb13a57d-d5c7-42f7-acd4-723992970d7a'),
	(221,NULL,41,83,'craft\\elements\\Entry',1,0,'2019-02-07 22:51:47','2019-11-07 15:48:54','2019-11-07 15:49:26','f97993e3-e23f-43ed-90eb-afa052732008'),
	(222,NULL,42,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:51:03','2019-11-07 15:48:54','2019-11-07 15:49:33','31c55ea2-4041-4098-a4ad-d9832ba9c0dd'),
	(223,NULL,43,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:50:45','2019-11-07 15:48:54','2019-11-07 15:49:33','deb48049-96de-4a24-add6-6c26017f86c8'),
	(224,NULL,44,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:48:51','2019-11-07 15:48:54','2019-11-07 15:49:33','eaf432bb-cb7d-420d-afca-da335e5e96d5'),
	(225,NULL,45,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:48:43','2019-11-07 15:48:54','2019-11-07 15:49:33','5ee792d7-ea2f-42de-ab1a-a6eba6119d24'),
	(226,NULL,46,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:47:19','2019-11-07 15:48:54','2019-11-07 15:49:33','9c912e0f-fbcc-4ebb-bf21-54f423907798'),
	(227,NULL,47,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:47:05','2019-11-07 15:48:54','2019-11-07 15:49:33','9931749f-c7a5-466d-b839-4b69bdcc68ef'),
	(228,NULL,48,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:46:42','2019-11-07 15:48:54','2019-11-07 15:49:33','fe6ef76f-74f0-43a5-b85d-b90762c4758f'),
	(229,NULL,49,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:46:26','2019-11-07 15:48:54','2019-11-07 15:49:33','fc8df1e1-a666-49a7-bead-3f26cc675780'),
	(230,NULL,50,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:46:13','2019-11-07 15:48:54','2019-11-07 15:49:33','a52cb724-86f9-4e2f-8d19-f9ab53d87a33'),
	(231,NULL,51,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:45:08','2019-11-07 15:48:54','2019-11-07 15:49:33','eb9dbfde-d791-4ff6-86b8-80540b6df6c7'),
	(232,NULL,52,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:44:15','2019-11-07 15:48:55','2019-11-07 15:49:33','7d9b7118-c82a-4b16-a0d3-f17464b98155'),
	(233,NULL,53,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:42:54','2019-11-07 15:48:55','2019-11-07 15:49:33','8563d83e-5192-4593-947c-be7ecb7c8b7b'),
	(234,NULL,54,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:41:45','2019-11-07 15:48:55','2019-11-07 15:49:33','8e23c578-6a84-4f1b-8ab9-5e3e0a0f1979'),
	(235,NULL,55,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:41:24','2019-11-07 15:48:55','2019-11-07 15:49:33','876bc9d1-f661-4efd-829e-239ec2855038'),
	(236,NULL,56,85,'craft\\elements\\Entry',1,0,'2019-02-07 22:40:00','2019-11-07 15:48:55','2019-11-07 15:49:33','172f3594-74f4-4f31-bbdb-01ec52eb6635'),
	(237,NULL,57,110,'craft\\elements\\Entry',1,0,'2019-02-07 22:31:37','2019-11-07 15:48:55',NULL,'64e9e1c7-6d38-4482-a01c-40e822672b45'),
	(238,NULL,58,83,'craft\\elements\\Entry',1,0,'2019-02-07 22:24:55','2019-11-07 15:48:55','2019-11-07 15:49:26','fddcfd8d-56f1-44e0-a834-b156840b3910'),
	(239,NULL,59,83,'craft\\elements\\Entry',1,0,'2019-02-07 22:22:58','2019-11-07 15:48:55','2019-11-07 15:49:26','68faeef3-25bc-4c65-b2d8-4a6dbe33c801'),
	(240,NULL,60,83,'craft\\elements\\Entry',1,0,'2019-02-07 22:19:35','2019-11-07 15:48:55','2019-11-07 15:49:26','1dc0e0fc-3f22-4f84-9a50-4ee4f36430f1'),
	(241,NULL,61,110,'craft\\elements\\Entry',1,0,'2019-02-07 22:07:10','2019-11-07 15:48:55',NULL,'d4f6d661-8ab1-44ca-80bf-4a1b6c90b8c4'),
	(242,NULL,62,110,'craft\\elements\\Entry',1,0,'2019-02-07 21:44:11','2019-11-07 15:48:55',NULL,'f7014969-b91c-4dd9-bad7-2436d2cd1277'),
	(243,NULL,63,110,'craft\\elements\\Entry',1,0,'2019-02-07 21:37:48','2019-11-07 15:48:55',NULL,'489d11bd-2dda-4c86-9fef-4541bf38eb08'),
	(244,NULL,64,110,'craft\\elements\\Entry',1,0,'2019-02-07 21:35:36','2019-11-07 15:48:55',NULL,'f710e520-7cbd-4f3a-b38d-1c05736fb690'),
	(245,NULL,65,110,'craft\\elements\\Entry',1,0,'2019-02-07 21:30:45','2019-11-07 15:48:55',NULL,'db9a0ec6-f0f2-41c2-99c0-d392db2a4f48'),
	(246,NULL,66,83,'craft\\elements\\Entry',1,0,'2019-02-07 21:23:41','2019-11-07 15:48:55','2019-11-07 15:49:26','c4f64ca1-e833-4241-9050-62f5510b8445'),
	(247,NULL,67,83,'craft\\elements\\Entry',1,0,'2019-02-07 21:20:38','2019-11-07 15:48:55','2019-11-07 15:49:26','33b3d132-2e6d-4381-b42d-fb4987446ce0'),
	(248,NULL,68,83,'craft\\elements\\Entry',1,0,'2019-02-07 21:18:38','2019-11-07 15:48:55','2019-11-07 15:49:26','4c3d5a7c-6be0-497c-9693-31b77675873e'),
	(249,NULL,69,83,'craft\\elements\\Entry',1,0,'2019-02-07 21:17:08','2019-11-07 15:48:55','2019-11-07 15:49:26','5db267f7-ff19-4d1f-9518-2ca80b3b155f'),
	(250,NULL,70,83,'craft\\elements\\Entry',1,0,'2019-02-07 21:16:26','2019-11-07 15:48:55','2019-11-07 15:49:26','20ccd872-6203-4c2a-92fe-b3c5ca6dbc87'),
	(251,NULL,71,83,'craft\\elements\\Entry',1,0,'2019-02-07 21:06:44','2019-11-07 15:48:55','2019-11-07 15:49:26','bc9e388b-0f5d-48a4-b5e9-3d8c8cf93076'),
	(252,NULL,72,83,'craft\\elements\\Entry',1,0,'2019-02-07 21:03:22','2019-11-07 15:48:55','2019-11-07 15:49:26','8b8217c2-0d7f-40db-b7ae-4035800bc128'),
	(253,NULL,73,83,'craft\\elements\\Entry',1,0,'2019-02-07 21:02:36','2019-11-07 15:48:55','2019-11-07 15:49:26','5d8899ef-eadb-4fc1-907a-c778202c8cbc'),
	(254,NULL,74,110,'craft\\elements\\Entry',1,0,'2019-02-07 20:37:50','2019-11-07 15:48:55',NULL,'c2ea32c2-aeb6-4b36-a817-f81a204a9e75'),
	(255,NULL,75,110,'craft\\elements\\Entry',1,0,'2019-02-07 20:33:23','2019-11-07 15:48:55',NULL,'752833aa-8f9c-4e70-8a29-7b81e92ff00b'),
	(256,NULL,76,110,'craft\\elements\\Entry',1,0,'2019-02-07 20:26:26','2019-11-07 15:48:55',NULL,'41b21d92-ea52-4337-ac99-49c4cb3642a0'),
	(257,NULL,77,84,'craft\\elements\\Entry',1,0,'2019-02-05 20:30:09','2019-11-07 15:48:55',NULL,'2f1f5587-8534-45d8-8b2e-51e61e332b0b'),
	(258,NULL,78,84,'craft\\elements\\Entry',1,0,'2019-02-05 20:29:19','2019-11-07 15:48:55',NULL,'6553ffdb-4410-430c-beae-2f80b09b97f7'),
	(259,NULL,79,84,'craft\\elements\\Entry',1,0,'2019-02-05 18:52:39','2019-11-07 15:48:55',NULL,'60bd712e-2acf-4006-9efd-1448b31f5823'),
	(260,NULL,80,98,'craft\\elements\\Entry',1,0,'2019-02-05 18:47:10','2019-11-07 15:48:55',NULL,'e7b8ef74-c092-43ac-bc06-2b554242effd'),
	(261,NULL,81,83,'craft\\elements\\Entry',1,0,'2019-02-05 17:49:56','2019-11-07 15:48:55',NULL,'ce2c7192-5931-46cd-9071-10ae628aa2bb'),
	(262,NULL,82,85,'craft\\elements\\Entry',1,0,'2019-02-01 20:56:00','2019-11-07 15:48:55','2019-11-07 15:49:33','f5280e82-4108-48dd-a792-3777a517b067'),
	(263,NULL,83,83,'craft\\elements\\Entry',1,0,'2019-01-31 23:40:42','2019-11-07 15:48:55',NULL,'fa257776-6540-4608-a1c5-60585df8e2d0'),
	(264,NULL,84,83,'craft\\elements\\Entry',1,0,'2019-01-31 23:26:59','2019-11-07 15:48:56',NULL,'731f916e-63f5-43e3-98e2-02c5220857ee'),
	(265,NULL,85,83,'craft\\elements\\Entry',1,0,'2019-01-31 22:50:32','2019-11-07 15:48:56',NULL,'ef502682-f35a-4ffc-be74-56bf5b29c72d'),
	(266,NULL,86,110,'craft\\elements\\Entry',1,0,'2019-01-31 20:57:39','2019-11-07 15:48:56',NULL,'6ffcba7f-5094-41ea-a85c-f611b2787710'),
	(267,NULL,87,110,'craft\\elements\\Entry',1,0,'2019-01-31 20:57:38','2019-11-07 15:48:56',NULL,'22e9f823-5103-497b-9745-97633dbc1738'),
	(268,NULL,88,83,'craft\\elements\\Entry',1,0,'2019-01-31 20:27:09','2019-11-07 15:48:56',NULL,'1e6bb62f-654a-4090-adc0-b79b50cbe00f'),
	(269,NULL,89,84,'craft\\elements\\Entry',1,0,'2019-01-31 16:43:44','2019-11-07 15:48:56',NULL,'bdea39c6-8a89-4731-a08e-3b6001e4f718'),
	(270,NULL,90,84,'craft\\elements\\Entry',1,0,'2019-01-31 16:41:39','2019-11-07 15:48:56',NULL,'904a1a96-a0d7-4b06-b6a7-aef0967c91c1'),
	(271,NULL,91,85,'craft\\elements\\Entry',1,0,'2019-01-31 16:30:08','2019-11-07 15:48:56','2019-11-07 15:49:33','47222ab9-b770-4477-884e-0b4c0d40c14f'),
	(272,NULL,92,85,'craft\\elements\\Entry',1,0,'2019-01-31 16:29:57','2019-11-07 15:48:56','2019-11-07 15:49:33','1624fb55-8e07-46b7-9a8f-b16d9ca600b1'),
	(273,NULL,93,83,'craft\\elements\\Entry',1,0,'2019-01-31 16:06:11','2019-11-07 15:48:56',NULL,'2fbeae84-b8c6-451b-8cc4-058acdd38c17'),
	(274,NULL,94,83,'craft\\elements\\Entry',1,0,'2019-01-31 16:04:36','2019-11-07 15:48:56',NULL,'631b9c8c-ad4d-4e35-8fff-2e02874c1d75'),
	(275,NULL,95,98,'craft\\elements\\Entry',1,0,'2019-01-31 15:43:51','2019-11-07 15:48:56',NULL,'3b6e3356-1fd0-4aed-bd0e-238420d6b715'),
	(276,NULL,96,83,'craft\\elements\\Entry',1,0,'2019-01-31 15:16:55','2019-11-07 15:48:56',NULL,'fcc3bd36-facd-4b1d-8145-b339dffe8be6'),
	(277,NULL,97,98,'craft\\elements\\Entry',1,0,'2019-01-30 23:51:10','2019-11-07 15:48:56',NULL,'97dba471-cdf6-4c2f-be35-bf889ed0f618'),
	(278,NULL,98,83,'craft\\elements\\Entry',1,0,'2019-01-30 23:50:27','2019-11-07 15:48:56',NULL,'df774273-6f89-46c5-9427-a322d61ff89b'),
	(279,NULL,99,83,'craft\\elements\\Entry',1,0,'2019-01-30 23:18:13','2019-11-07 15:48:56',NULL,'38cc4741-bef1-46ee-b2a9-7b0294e87234'),
	(280,NULL,100,83,'craft\\elements\\Entry',1,0,'2019-01-30 23:12:53','2019-11-07 15:48:56',NULL,'3d5f49de-3795-4a9e-9aca-f6c964f6835f'),
	(281,NULL,101,83,'craft\\elements\\Entry',1,0,'2019-01-30 23:11:18','2019-11-07 15:48:56',NULL,'326d2bc9-08d2-4078-8ba5-0552eda5526b'),
	(282,NULL,102,83,'craft\\elements\\Entry',1,0,'2019-01-30 21:55:00','2019-11-07 15:48:56','2019-11-07 15:49:26','1e2cc1fe-c1a2-4896-8ccc-f135c944aa6c'),
	(283,NULL,103,83,'craft\\elements\\Entry',1,0,'2019-01-30 21:50:11','2019-11-07 15:48:56','2019-11-07 15:49:26','3ec502c2-1b66-48b1-886e-97dc892b0776'),
	(284,NULL,104,83,'craft\\elements\\Entry',1,0,'2019-01-30 21:49:57','2019-11-07 15:48:56','2019-11-07 15:49:26','964590b0-0f52-48ed-9da0-667cf3fd76d1'),
	(285,NULL,105,83,'craft\\elements\\Entry',1,0,'2019-01-30 21:49:45','2019-11-07 15:48:56','2019-11-07 15:49:26','8d95652c-b4b8-4a2f-a781-1bebd20a6534'),
	(286,NULL,106,83,'craft\\elements\\Entry',1,0,'2019-01-30 21:49:22','2019-11-07 15:48:56','2019-11-07 15:49:26','a8a7250e-eedb-411e-b26b-a7d5a6d57b98'),
	(287,NULL,107,85,'craft\\elements\\Entry',1,0,'2019-01-30 21:32:20','2019-11-07 15:48:56','2019-11-07 15:49:33','1dc1cf5c-6203-4a71-82e2-7e3a93334f6c'),
	(288,NULL,108,83,'craft\\elements\\Entry',1,0,'2019-01-30 21:23:35','2019-11-07 15:48:56','2019-11-07 15:49:26','f11f5a3f-6ba4-4c0e-9c71-cf0eb523f1d1'),
	(289,NULL,109,83,'craft\\elements\\Entry',1,0,'2019-01-30 21:22:51','2019-11-07 15:48:56',NULL,'72b78a78-bf6b-4b6e-9a41-c4efd38cb64a'),
	(290,NULL,110,84,'craft\\elements\\Entry',1,0,'2019-01-30 21:22:23','2019-11-07 15:48:57',NULL,'a76b5ec2-37fd-4757-a840-0ca42ec32259'),
	(291,NULL,111,110,'craft\\elements\\Entry',1,0,'2019-11-07 16:37:10','2019-11-07 16:37:10',NULL,'087a6340-0867-4ca1-8356-ad72fa29bc9b'),
	(292,NULL,112,83,'craft\\elements\\Entry',1,0,'2019-11-07 16:38:02','2019-11-07 16:38:02',NULL,'8c48b7c0-3f22-4b91-a714-19a1eb35f457');

/*!40000 ALTER TABLE `craft_elements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_elements_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_elements_sites`;

CREATE TABLE `craft_elements_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_elements_sites_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `craft_elements_sites_siteId_idx` (`siteId`),
  KEY `craft_elements_sites_slug_siteId_idx` (`slug`,`siteId`),
  KEY `craft_elements_sites_enabled_idx` (`enabled`),
  KEY `craft_elements_sites_uri_siteId_idx` (`uri`,`siteId`),
  CONSTRAINT `craft_elements_sites_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_elements_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_elements_sites` WRITE;
/*!40000 ALTER TABLE `craft_elements_sites` DISABLE KEYS */;

INSERT INTO `craft_elements_sites` (`id`, `elementId`, `siteId`, `slug`, `uri`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,1,NULL,NULL,1,'2019-01-21 23:30:44','2019-01-21 23:30:44','4b38d56c-b2e8-4330-9e3a-78d04923042a'),
	(2,2,1,'homepage','__home__',1,'2019-01-21 23:39:18','2019-02-08 18:05:31','7882afb0-6f4f-464a-86b8-009d354a17b5'),
	(3,3,1,NULL,NULL,1,'2019-01-21 23:41:01','2019-01-30 21:46:51','a6772940-8c32-44f2-9425-da483956c8db'),
	(4,4,1,NULL,NULL,1,'2019-01-21 23:41:12','2019-01-30 22:52:01','6f5616dc-e857-4b49-912d-1c1908966554'),
	(5,5,1,'test-blog','blog/test-blog',1,'2019-01-30 21:22:22','2019-02-05 20:28:28','b595366c-8c74-445c-97e5-d781a8b37068'),
	(6,6,1,'blocks','blocks',1,'2019-01-30 21:22:51','2019-02-27 21:20:40','ef125823-d6c9-4b7f-9aa1-94d2d9db3cec'),
	(7,7,1,'blog','blog',1,'2019-01-30 21:23:35','2019-02-05 19:05:20','621ff6c1-e3ce-47f3-8760-a54844862261'),
	(8,8,1,'elementary','blog/elementary',1,'2019-01-30 21:25:57','2019-01-30 21:26:44','85267e01-dd8f-42c2-a3ca-3a27d4e99f9f'),
	(9,9,1,'high-school','blog/high-school',1,'2019-01-30 21:26:31','2019-01-30 21:26:33','39d99ea0-5164-4d0b-86b2-180c12974ee0'),
	(10,10,1,'test','quotes/test',1,'2019-01-30 21:32:20','2019-02-07 22:48:51','1344ecd9-59e6-4695-b489-3d3f5d729fb8'),
	(11,11,1,NULL,NULL,1,'2019-01-30 21:35:19','2019-01-30 21:35:19','941c6b83-3a94-4b48-9ede-81546eeeac6d'),
	(12,12,1,NULL,NULL,1,'2019-01-30 21:35:20','2019-01-30 21:35:20','1496860a-102e-4939-aeb4-e0b4a6bad9aa'),
	(13,13,1,NULL,NULL,1,'2019-01-30 21:38:03','2019-02-07 22:31:30','352cfa8b-3c84-4012-a9e5-7a8013eb3cc4'),
	(14,14,1,NULL,NULL,1,'2019-01-30 21:38:04','2019-01-30 21:38:04','cd3508f5-81f1-4b0e-a1bf-f4ae48107b9b'),
	(15,15,1,NULL,NULL,1,'2019-01-30 21:38:06','2019-01-30 21:38:06','a667937f-409e-438c-a93f-02020a20728e'),
	(16,16,1,NULL,NULL,1,'2019-01-30 21:38:16','2019-01-30 21:38:16','b03dda59-8cf7-4ffc-983e-0a76a50c491e'),
	(17,17,1,NULL,NULL,1,'2019-01-30 21:38:32','2019-01-30 21:38:32','dc911474-0a43-453e-ba29-30376238f442'),
	(18,18,1,NULL,NULL,1,'2019-01-30 21:38:39','2019-01-30 21:38:39','31c23869-eede-4420-a57f-c75e5324c934'),
	(19,19,1,NULL,NULL,1,'2019-01-30 21:38:56','2019-01-30 21:38:56','9ed34e68-2e44-49c2-918e-56c3d43eefa2'),
	(20,20,1,NULL,NULL,1,'2019-01-30 21:38:57','2019-01-30 21:38:57','6caeaff4-d6f6-4d8f-a9ff-03089a1bb3b2'),
	(21,21,1,NULL,NULL,1,'2019-01-30 21:38:58','2019-01-30 21:38:58','15add453-42be-4465-a1ed-23ea884c2a75'),
	(22,22,1,NULL,NULL,1,'2019-01-30 21:46:51','2019-01-30 21:46:51','615bb5b7-a552-4e7a-aaca-6d144aeaca8a'),
	(23,23,1,NULL,NULL,1,'2019-01-30 21:46:51','2019-01-30 21:46:51','33e2cced-cc1f-40ca-bc2d-5b9f9584c482'),
	(24,24,1,NULL,NULL,1,'2019-01-30 21:46:51','2019-01-30 21:46:51','33b2e8c1-aafc-43b2-a2f6-6a3e08f1c3d2'),
	(25,25,1,NULL,NULL,1,'2019-01-30 21:46:51','2019-01-30 21:46:51','c61cf14c-ab61-424c-8d9b-fbe42852b599'),
	(26,26,1,'about','about',1,'2019-01-30 21:49:22','2019-02-08 17:36:56','aab4c2c2-0bd9-4ac7-9be6-231c4740890b'),
	(27,27,1,'elementary-school','elementary-school',1,'2019-01-30 21:49:45','2019-02-12 20:50:49','920d1ab8-f37e-4ac1-b2f3-818cf92cf360'),
	(28,28,1,'middle-school','middle-school',1,'2019-01-30 21:49:56','2019-02-12 20:52:21','d318e327-7a9a-472b-bc02-ac9aae396d42'),
	(29,29,1,'high-school','high-school',1,'2019-01-30 21:50:11','2019-02-12 20:53:06','2302a321-cf25-460c-ae1f-0c99c14c0e13'),
	(30,30,1,NULL,NULL,1,'2019-01-30 21:51:59','2019-02-08 18:05:31','38d97f60-7bc1-4302-9d57-26eca2fc1122'),
	(31,31,1,NULL,NULL,1,'2019-01-30 21:52:09','2019-02-08 17:36:55','1890e134-b91d-4db4-9c5e-4f82239d2c2e'),
	(32,32,1,NULL,NULL,1,'2019-01-30 21:52:09','2019-02-05 19:05:20','1bf3ab3e-d90b-4b05-a347-2778c374a173'),
	(33,33,1,NULL,NULL,1,'2019-01-30 21:52:10','2019-02-12 20:52:21','5c966ccc-e44a-492a-ad3c-01b178ddcc55'),
	(34,34,1,NULL,NULL,1,'2019-01-30 21:52:10','2019-02-12 20:50:49','5b52e7be-0d89-4537-aa0d-54461cf42a81'),
	(35,35,1,NULL,NULL,1,'2019-01-30 21:52:10','2019-02-12 20:53:06','6860ada1-01cf-4be7-a052-62d468586e19'),
	(36,36,1,NULL,NULL,1,'2019-01-30 21:54:06','2019-02-08 18:05:31','950e9ec5-32e9-4e44-8e53-dd783fe06037'),
	(37,37,1,NULL,NULL,1,'2019-01-30 21:54:11','2019-02-05 19:05:19','33104ab7-92cf-48b2-b981-7ad473426a85'),
	(38,38,1,'privacy-policy','privacy-policy',1,'2019-01-30 21:55:00','2019-01-30 21:55:03','a53722bf-24c1-4590-b5b3-7b852825cb77'),
	(39,39,1,NULL,NULL,1,'2019-01-30 22:21:05','2019-02-08 16:29:36','9b5a7c2f-21d8-4911-8e62-12639aa9bf61'),
	(40,40,1,NULL,NULL,1,'2019-01-30 22:51:51','2019-01-30 22:51:51','d687e991-889a-4dfd-a623-f2578a9def41'),
	(41,41,1,NULL,NULL,1,'2019-01-30 23:06:44','2019-01-30 23:06:44','f9d440df-f36a-4de9-a13e-08b399b67c9f'),
	(42,42,1,NULL,NULL,1,'2019-01-30 23:10:27','2019-01-30 23:10:27','f96706f1-af92-4558-a999-be663befce95'),
	(43,43,1,NULL,NULL,1,'2019-01-30 23:11:17','2019-02-27 21:20:40','11c135d6-aaff-47d7-9051-755874a58f38'),
	(44,44,1,NULL,NULL,1,'2019-01-30 23:11:17','2019-02-27 21:20:40','4760f20a-805c-4f22-84b1-e09c8cd88783'),
	(45,45,1,NULL,NULL,1,'2019-01-30 23:11:17','2019-02-27 21:20:40','91ee4317-e42c-4280-a52f-92e190aa1749'),
	(46,46,1,NULL,NULL,1,'2019-01-30 23:17:15','2019-01-30 23:17:15','8034332c-e82f-46e4-97dd-c0579d493b71'),
	(47,47,1,NULL,NULL,1,'2019-01-30 23:18:12','2019-02-27 21:20:40','c8dd64e0-fd39-4259-98c1-442af8490939'),
	(48,48,1,NULL,NULL,1,'2019-01-30 23:18:12','2019-02-27 21:20:40','de49817c-a6db-4dda-a00a-11974b5baa77'),
	(49,49,1,NULL,NULL,1,'2019-01-30 23:18:12','2019-02-27 21:20:40','dafeaaa9-2ccc-4e1e-8777-b52d0c8e3f4d'),
	(50,50,1,NULL,NULL,1,'2019-01-30 23:18:13','2019-02-27 21:20:40','07ebd41b-f5b9-4521-80ce-7dbc274d20a5'),
	(51,51,1,'high-school','quote/high-school',1,'2019-01-30 23:35:14','2019-01-30 23:35:17','e9910283-5e68-4e6e-a33b-a62a51b5e99e'),
	(52,52,1,'general','quote/general',1,'2019-01-30 23:35:24','2019-01-30 23:35:26','df093363-82d1-45ce-9e75-0abab28768dc'),
	(53,53,1,'news','blog/news',1,'2019-01-30 23:35:41','2019-01-30 23:35:44','d9a4c58d-cd3f-4229-ab8f-0ec6cbc3850e'),
	(54,54,1,'stories','blog/stories',1,'2019-01-30 23:36:36','2019-01-30 23:36:39','0a938bac-ab31-4019-8c6e-01297672be76'),
	(55,55,1,'general','blog/general',1,'2019-01-30 23:36:48','2019-01-30 23:36:50','f5a62c74-82b3-410b-8d31-dc4828cd2042'),
	(56,56,1,NULL,NULL,1,'2019-01-30 23:43:46','2019-01-30 23:43:46','a1f40ca1-19ed-4c5c-b51c-b02ae4168782'),
	(57,57,1,NULL,NULL,1,'2019-01-30 23:43:48','2019-01-30 23:43:48','c7a144b2-9aba-435f-8fd9-83363ea83ebf'),
	(58,58,1,NULL,NULL,1,'2019-01-30 23:43:50','2019-01-30 23:43:50','2b7b9a3f-bad7-4772-bf0a-54c0950886cd'),
	(59,59,1,NULL,NULL,1,'2019-01-30 23:50:25','2019-02-27 21:20:40','e12105e3-92cc-45fa-a7f9-cf6db940b4f3'),
	(60,60,1,NULL,NULL,1,'2019-01-30 23:50:25','2019-02-27 21:20:40','614e11da-f840-42c3-8b21-eada8059dc8b'),
	(61,61,1,NULL,NULL,1,'2019-01-30 23:50:25','2019-02-27 21:20:40','bb08df2a-30dc-4fa8-8f9c-50959dda7299'),
	(62,62,1,NULL,NULL,1,'2019-01-30 23:50:25','2019-02-27 21:20:40','bc3dc691-31c5-4e58-8b54-17dcc54b7586'),
	(63,63,1,NULL,NULL,1,'2019-01-30 23:50:26','2019-02-27 21:20:40','d5f52ebe-c9f3-455d-8530-29dcd2fb7a81'),
	(64,64,1,NULL,NULL,1,'2019-01-30 23:50:26','2019-02-27 21:20:40','83387c08-fbac-4d85-9d6d-791818fea847'),
	(65,65,1,NULL,NULL,1,'2019-01-30 23:50:26','2019-02-27 21:20:40','39a14639-a2e9-48a9-bb17-939e004132f4'),
	(66,66,1,NULL,NULL,1,'2019-01-30 23:50:26','2019-02-27 21:20:40','0cbd84c5-b222-4fad-a027-06aba055be30'),
	(67,67,1,'contact',NULL,1,'2019-01-30 23:51:10','2019-02-05 18:47:10','ae0e974e-24c2-43fd-9f31-42feb80e0e5f'),
	(68,68,1,NULL,NULL,1,'2019-01-31 16:04:34','2019-02-27 21:20:40','b1536303-49b3-49d0-977e-1f17acc5d0e1'),
	(69,69,1,NULL,NULL,1,'2019-01-31 16:04:34','2019-02-27 21:20:40','a5d9741f-0d15-412b-8da8-3e4595297295'),
	(70,70,1,NULL,NULL,1,'2019-01-31 16:04:34','2019-02-27 21:20:40','df4d8dee-247f-44e7-a8a9-4ff19bfa4d72'),
	(71,71,1,NULL,NULL,1,'2019-01-31 16:06:10','2019-02-27 21:20:40','601c8f53-77f0-47db-9cdb-698a734f9ee3'),
	(72,72,1,NULL,NULL,1,'2019-01-31 20:57:39','2019-02-08 18:05:31','c8a06743-b83c-4c55-814c-bae3940637d7'),
	(73,73,1,'test2','quotes/test2',1,'2019-02-01 20:56:00','2019-02-07 22:48:43','8017422d-9eb2-460e-bc65-76a869518b66'),
	(74,74,1,NULL,NULL,1,'2019-02-05 17:49:55','2019-02-27 21:20:40','867f6094-576c-4edc-8e57-fb56a7ef3f2c'),
	(75,75,1,NULL,NULL,1,'2019-02-05 17:49:55','2019-02-27 21:20:40','1bd1905c-a32d-4a0e-8974-05c30fcaa970'),
	(76,76,1,NULL,NULL,1,'2019-02-05 18:52:38','2019-02-05 20:28:28','fc850a6f-60a3-48b3-a60e-bc7de5023232'),
	(77,77,1,NULL,NULL,1,'2019-02-05 18:52:38','2019-02-05 20:28:28','00e6c8d1-a4aa-4cf0-a076-90f9cc66adc0'),
	(78,78,1,NULL,NULL,1,'2019-02-05 18:52:38','2019-02-05 20:28:28','f96fbd8d-9225-43e2-98d2-b38f22ec13eb'),
	(79,79,1,'test-blog2','blog/test-blog2',1,'2019-02-05 20:29:19','2019-02-05 20:29:19','e5fd9ebd-73f4-4839-a5ef-dfcd2650c717'),
	(80,80,1,NULL,NULL,1,'2019-02-05 20:29:19','2019-02-05 20:29:19','0c6f00c7-5a80-49e0-8fe8-3a1ccfb9b34a'),
	(81,81,1,NULL,NULL,1,'2019-02-05 20:29:19','2019-02-05 20:29:19','5ff0ae1e-1c9b-4ee1-b8e5-cc7bb2db6455'),
	(82,82,1,'test-blog-3','blog/test-blog-3',1,'2019-02-05 20:30:08','2019-02-05 20:30:08','cb08945f-b4e5-48c2-b1ad-38cf1867cccb'),
	(83,83,1,NULL,NULL,1,'2019-02-05 20:30:08','2019-02-05 20:30:08','7614eaf0-3b7d-4a63-aca3-fdba9c400c14'),
	(84,84,1,NULL,NULL,1,'2019-02-05 20:30:08','2019-02-05 20:30:08','430387ed-9fe6-4211-954d-cdc96b409a89'),
	(85,85,1,NULL,NULL,1,'2019-02-07 20:26:26','2019-02-07 20:27:16','90344185-c7a0-4b9f-be45-1f6a7b0b97f4'),
	(86,86,1,NULL,NULL,1,'2019-02-07 20:33:23','2019-02-07 20:33:23','364d4a42-dff5-4087-a747-df14e4a4a683'),
	(87,87,1,NULL,NULL,1,'2019-02-07 20:58:43','2019-02-07 20:58:43','65d364c9-0bd4-4e8e-ada7-ca905e5dd7ec'),
	(88,88,1,NULL,NULL,1,'2019-02-07 20:58:44','2019-02-07 20:58:44','ed55d919-e880-43d3-ae09-9f7f43302cdf'),
	(89,89,1,NULL,NULL,1,'2019-02-07 20:58:45','2019-02-07 20:58:45','2b88d2eb-999d-4646-899d-79ffbb28fd20'),
	(90,90,1,NULL,NULL,1,'2019-02-07 20:58:47','2019-02-07 20:58:47','0aa6f4a5-a471-44b4-968e-c1b35e92892b'),
	(91,91,1,NULL,NULL,1,'2019-02-07 20:58:47','2019-02-07 20:58:47','01d9fa93-3cca-433d-ba11-e3d1530097b0'),
	(92,92,1,NULL,NULL,1,'2019-02-07 20:58:48','2019-02-07 20:58:48','0b57c5b3-27e3-4901-b5a2-ad51e2c72fda'),
	(93,93,1,NULL,NULL,1,'2019-02-07 20:58:50','2019-02-07 20:58:50','502da8b2-ffe8-41aa-bf5b-380848878c9e'),
	(94,94,1,NULL,NULL,1,'2019-02-07 20:58:51','2019-02-07 20:58:51','0fd5487f-095a-4b88-836d-fd50d760f7f3'),
	(95,95,1,NULL,NULL,1,'2019-02-07 20:58:52','2019-02-07 20:58:52','03251328-c1af-4293-b896-92f59c04b9ba'),
	(96,96,1,NULL,NULL,1,'2019-02-07 20:58:53','2019-02-07 20:58:53','508434b2-9c76-4c0b-9f1c-bbf040b7ce42'),
	(97,97,1,NULL,NULL,1,'2019-02-07 20:58:55','2019-02-07 20:58:55','049018b0-73ff-4a47-b8ea-3722e8e7a8ba'),
	(98,98,1,NULL,NULL,1,'2019-02-07 21:02:36','2019-02-08 17:36:56','8f3891ff-c1c3-40b7-98c9-4adfce3c5019'),
	(99,99,1,NULL,NULL,1,'2019-02-07 21:02:36','2019-02-08 17:36:56','92bf387d-7e34-4225-9f92-571ea9502fb8'),
	(100,100,1,NULL,NULL,1,'2019-02-07 21:02:36','2019-02-08 17:36:56','756ebfa1-d272-4034-9ea3-dca2bb5dab50'),
	(101,101,1,NULL,NULL,1,'2019-02-07 21:02:36','2019-02-08 17:36:56','5a9b2d86-98e5-4495-af1b-c46c42febe1a'),
	(102,102,1,NULL,NULL,1,'2019-02-07 21:16:26','2019-02-08 17:16:37','4f0b8ca3-e746-4e72-9f28-05a2b27ef6e0'),
	(103,103,1,NULL,NULL,1,'2019-02-07 21:16:26','2019-02-08 17:16:37','7a48203a-658c-4f8a-b54d-52bb23c556ed'),
	(104,104,1,NULL,NULL,1,'2019-02-07 21:16:26','2019-02-07 21:16:26','c5acceb8-4007-4f37-804f-cc7ffaffe500'),
	(105,105,1,NULL,NULL,1,'2019-02-07 21:17:08','2019-02-12 20:39:11','b82a162a-4b8d-4637-88cb-470c7fb1d6ba'),
	(106,106,1,NULL,NULL,1,'2019-02-07 21:18:38','2019-02-08 17:15:21','2e519f2c-e5d4-4fc6-b18a-43635b7741b4'),
	(107,107,1,NULL,NULL,1,'2019-02-07 21:18:38','2019-02-08 17:15:21','03521b6e-25b9-4eba-9ae2-573ab84ce828'),
	(108,108,1,NULL,NULL,1,'2019-02-07 21:18:38','2019-02-12 20:43:47','7d537555-349d-4dd3-8469-f7057c708ed6'),
	(109,109,1,NULL,NULL,1,'2019-02-07 21:20:38','2019-02-08 17:44:24','36ab39eb-dcb8-4211-88f6-5bbbd594586b'),
	(110,110,1,NULL,NULL,1,'2019-02-07 21:20:38','2019-02-08 17:44:24','8bd30b87-3483-45a9-b1c7-8e59210b5a98'),
	(111,111,1,NULL,NULL,1,'2019-02-07 21:20:38','2019-02-08 17:44:24','5a98c791-e225-44b1-8db4-ae547ee2919a'),
	(112,112,1,NULL,NULL,1,'2019-02-07 21:23:41','2019-02-12 20:49:00','b1f36d59-71b3-4e88-8798-a5e6aa4c0b64'),
	(113,113,1,NULL,NULL,1,'2019-02-07 21:30:45','2019-02-08 18:05:31','271e3e0b-c5ca-4b2d-9579-4a466f546a3e'),
	(114,114,1,NULL,NULL,1,'2019-02-07 21:30:45','2019-02-08 18:05:31','693bcb17-bdad-4f6a-b1ca-50854856eb31'),
	(115,115,1,NULL,NULL,1,'2019-02-07 21:37:48','2019-02-08 18:05:31','af4001d5-6263-4706-9568-2f22f55ff0f2'),
	(116,116,1,NULL,NULL,1,'2019-02-07 21:37:48','2019-02-08 18:05:31','42928204-8c86-4fd1-8eed-c817c88e5c2a'),
	(117,117,1,NULL,NULL,1,'2019-02-07 21:37:48','2019-02-08 18:05:31','3d867cb8-f04d-4274-852a-0a6df1c10116'),
	(118,118,1,NULL,NULL,1,'2019-02-07 21:37:48','2019-02-08 18:05:31','2959f10e-3a02-45b6-8088-c197a6d117f8'),
	(119,119,1,NULL,NULL,1,'2019-02-07 21:44:10','2019-02-08 18:05:31','7d07fba9-af73-4615-830b-29728bf8bba7'),
	(120,120,1,NULL,NULL,1,'2019-02-07 21:44:10','2019-02-08 18:05:31','46b89d85-1a2b-4425-a69a-da48521b6bc8'),
	(121,121,1,NULL,NULL,1,'2019-02-07 21:44:10','2019-02-08 18:05:31','cc641e49-c827-494b-89b3-7ce52918d322'),
	(122,122,1,NULL,NULL,1,'2019-02-07 21:44:10','2019-02-08 18:05:31','a32033d7-e17c-48e3-9d34-901fab99d295'),
	(123,123,1,NULL,NULL,1,'2019-02-07 21:44:10','2019-02-08 18:05:32','44bb19b7-1fdf-4a1e-92a5-f81f528bb2be'),
	(124,124,1,NULL,NULL,1,'2019-02-07 21:44:10','2019-02-08 18:05:32','da13a5c4-8767-419c-84a4-54ebd2a23578'),
	(125,125,1,NULL,NULL,1,'2019-02-07 21:44:10','2019-02-08 18:05:32','0eac5a56-8f56-4170-af1e-18c339f594f3'),
	(126,126,1,NULL,NULL,1,'2019-02-07 22:07:10','2019-02-08 18:05:32','d464be90-2756-4b7d-b184-b2f1a2e978df'),
	(127,127,1,NULL,NULL,1,'2019-02-07 22:07:10','2019-02-08 18:05:32','7ecbd6b5-fac5-428d-bb7f-b354569e2ed5'),
	(128,128,1,NULL,NULL,1,'2019-02-07 22:07:10','2019-02-08 18:05:32','2bb7d50a-a019-4b2a-85a2-8a38900202cc'),
	(129,129,1,NULL,NULL,1,'2019-02-07 22:07:10','2019-02-08 18:05:32','7a0c8c82-3b5e-4f89-bc20-f85ca92f4cc3'),
	(130,130,1,NULL,NULL,1,'2019-02-07 22:07:10','2019-02-08 18:05:32','eb29494d-133a-436f-b206-aea06527f348'),
	(131,131,1,NULL,NULL,1,'2019-02-07 22:07:10','2019-02-08 18:05:32','54512283-cdc7-4fe3-999a-eefdd67c2a01'),
	(132,132,1,NULL,NULL,1,'2019-02-07 22:07:10','2019-02-08 18:05:32','11ddcecf-5586-4abc-895d-759c76700de7'),
	(133,133,1,NULL,NULL,1,'2019-02-07 22:17:58','2019-02-07 22:17:58','5dbed25a-30d9-4a05-a291-786060698efa'),
	(134,134,1,NULL,NULL,1,'2019-02-07 22:17:59','2019-02-07 22:26:50','3b414a0d-72df-41b1-bd9e-dba227795cf5'),
	(135,135,1,NULL,NULL,1,'2019-02-07 22:18:00','2019-02-07 22:18:00','c64f078e-8cdf-412b-90e0-656610f6f0e8'),
	(136,136,1,NULL,NULL,1,'2019-02-07 22:18:01','2019-02-07 22:18:01','29950bf2-dca6-4eb3-b6bd-a6d119ee85f7'),
	(137,137,1,NULL,NULL,1,'2019-02-07 22:18:02','2019-02-07 22:18:02','4a5c5751-2596-49a1-aedb-62fc33f69080'),
	(138,138,1,NULL,NULL,1,'2019-02-07 22:18:03','2019-02-07 22:24:47','68d7d861-02ca-4a40-99b0-d9861c6533e5'),
	(139,139,1,NULL,NULL,1,'2019-02-07 22:19:35','2019-02-08 17:15:21','e58b8a5f-cb2d-41b2-89f1-7d86381b3a8c'),
	(140,140,1,NULL,NULL,1,'2019-02-07 22:19:35','2019-02-12 20:52:21','22321ee1-e069-4eb3-a4db-4eb0de71e431'),
	(141,141,1,NULL,NULL,1,'2019-02-07 22:22:58','2019-02-08 17:16:37','092ca4d5-31ba-4e96-9f53-4f8806e8334f'),
	(142,142,1,NULL,NULL,1,'2019-02-07 22:22:58','2019-02-12 20:50:49','16ccea41-bebc-414d-b859-5fa3578f8e3c'),
	(143,143,1,NULL,NULL,1,'2019-02-07 22:24:54','2019-02-12 20:53:06','db738ae3-7f89-4131-b6b8-f26104fec9bc'),
	(144,144,1,'elementary-1','quotes/elementary-1',1,'2019-02-07 22:39:59','2019-02-08 17:07:53','0961adac-30e0-4542-a722-a36b90b889e9'),
	(145,145,1,'middle-school','quote/middle-school',1,'2019-02-07 22:41:16','2019-02-07 22:41:25','5020b5c8-c54e-4778-9a10-72d2ac0827e2'),
	(146,146,1,'middle-school-1','quotes/middle-school-1',1,'2019-02-07 22:41:24','2019-02-08 17:09:37','36a8c403-48c1-4b8a-b553-b712e26073d9'),
	(147,147,1,'high-school-1','quotes/high-school-1',1,'2019-02-07 22:42:53','2019-02-08 17:11:34','3f11860f-6cf4-4dd6-ba08-cba44ad28a83'),
	(148,148,1,'elementary','quote/elementary',1,'2019-02-07 22:44:08','2019-02-07 22:44:16','8232a446-ff4d-4fb2-b30f-353c353839fd'),
	(149,149,1,'elementary-2','quotes/elementary-2',1,'2019-02-07 22:44:14','2019-02-08 17:10:47','a2547ea1-96af-47f0-89c8-3761b9aa05aa'),
	(150,150,1,'elementary-3','quotes/elementary-3',1,'2019-02-07 22:45:07','2019-02-08 17:11:02','fd4f6ea6-f055-4313-88f0-3a4af1ce57e1'),
	(151,151,1,NULL,NULL,1,'2019-02-07 22:52:44','2019-02-07 22:52:44','ff6a678a-e271-4dc0-98f6-e4b81f9835d4'),
	(152,152,1,NULL,NULL,1,'2019-02-08 15:47:39','2019-02-08 18:05:31','8c0916cf-10cd-4e08-8d23-02037594999d'),
	(153,153,1,NULL,NULL,1,'2019-02-08 15:47:39','2019-02-08 18:05:31','5da32a78-3c92-4d50-9e1d-91941781b2de'),
	(154,154,1,NULL,NULL,1,'2019-02-08 15:52:54','2019-02-08 18:05:31','c7f6217c-fa6f-4248-aebd-40229abe5889'),
	(155,155,1,NULL,NULL,1,'2019-02-08 15:52:54','2019-02-08 18:05:31','07b30776-2f0c-4075-a820-3947edd9ef5d'),
	(156,156,1,NULL,NULL,1,'2019-02-08 15:52:54','2019-02-08 18:05:32','5002a193-d807-44b0-b870-560efd7acab4'),
	(157,157,1,NULL,NULL,1,'2019-02-08 15:52:54','2019-02-08 18:05:32','70ede475-0cbb-4c79-8552-30a8bce55113'),
	(158,158,1,NULL,NULL,1,'2019-02-08 15:52:55','2019-02-08 18:05:32','50360996-2d46-4909-8ecd-019482cac329'),
	(159,159,1,NULL,NULL,1,'2019-02-08 15:52:55','2019-02-08 18:05:32','32381fe8-12be-4740-9093-05bf6b270bca'),
	(160,160,1,NULL,NULL,1,'2019-02-08 15:54:32','2019-02-08 18:05:32','44e21dd7-f0b1-45a2-a928-492244eb4e50'),
	(161,161,1,NULL,NULL,1,'2019-02-08 15:54:32','2019-02-08 18:05:32','183e4a9a-60f8-414c-9c07-daad53e22af3'),
	(162,162,1,NULL,NULL,1,'2019-02-08 16:41:02','2019-02-08 16:41:02','c6fcb55c-f982-4158-bce4-d480412e16a1'),
	(163,163,1,NULL,NULL,1,'2019-02-08 16:42:15','2019-02-08 16:42:15','b3bfc4f2-db09-48d1-993f-f21653d6ff3c'),
	(164,164,1,NULL,NULL,1,'2019-02-08 16:45:20','2019-02-08 18:05:31','9abdf741-aae1-495e-bcb6-014cd2f447fd'),
	(165,165,1,NULL,NULL,1,'2019-02-08 16:45:20','2019-02-08 18:05:31','cc3d24c4-a86d-4b43-962b-b8111a8a861a'),
	(166,166,1,NULL,NULL,1,'2019-02-08 16:45:20','2019-02-08 18:05:31','b8c11552-6008-4870-8568-fb44ca90750a'),
	(167,167,1,NULL,NULL,1,'2019-02-08 16:48:25','2019-02-08 16:48:25','2dba8ca3-ac82-421d-88fd-21476540fff7'),
	(168,168,1,NULL,NULL,1,'2019-02-08 16:52:58','2019-02-08 16:52:58','e9c0d54a-2f4c-4765-b583-4b51a143821c'),
	(169,169,1,NULL,NULL,1,'2019-02-08 17:05:33','2019-02-08 17:36:56','640116aa-bc7d-4ed8-b8f7-b40c92d37bf8'),
	(170,170,1,NULL,NULL,1,'2019-02-08 17:07:08','2019-02-12 20:50:49','1eb332cb-f458-4229-8e0a-1400cfdb3586'),
	(171,171,1,NULL,NULL,1,'2019-02-08 17:11:44','2019-02-12 20:52:21','a3b7d849-0b61-4cc8-82ff-3abb15bda52e'),
	(172,172,1,NULL,NULL,1,'2019-02-08 17:41:19','2019-02-12 20:53:06','5e2217e0-4898-455a-9bda-a41b2af60188'),
	(173,173,1,NULL,NULL,1,'2019-02-08 20:04:39','2019-02-27 21:20:40','34f717bb-f4a0-46ba-9e76-6d60179de451'),
	(174,174,1,NULL,NULL,1,'2019-02-12 20:39:11','2019-02-12 20:50:49','bd55d27e-9ae9-461e-85e1-2b76d74d55dd'),
	(175,175,1,NULL,NULL,1,'2019-02-12 20:43:47','2019-02-12 20:52:21','543c20bd-3923-45fe-9288-80df264b8e2a'),
	(176,176,1,NULL,NULL,1,'2019-02-12 20:49:00','2019-02-12 20:53:06','5259ec55-3b8d-4588-bc3c-c3286a4c8b94'),
	(177,177,1,'test','quote/test',1,'2019-02-14 22:31:33','2019-02-14 22:31:37','35708dd9-9975-4a13-93df-a0767c7e3533'),
	(178,178,1,NULL,NULL,1,'2019-02-14 22:37:25','2019-02-14 22:38:11','1341c0e2-273c-47b8-839f-67452243c8a0'),
	(179,179,1,NULL,NULL,1,'2019-02-14 22:43:46','2019-02-18 15:02:36','2fc0b750-53d7-4b36-91ad-d8f3ee6ae123'),
	(180,180,1,NULL,NULL,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','1fc08484-cb55-495f-8f08-9337e07cf288'),
	(181,181,1,'blocks','blocks',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','a13d93ad-dd77-4c96-a845-8b4c0143cf2a'),
	(182,182,1,'blocks','blocks',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','54e6be1c-2eb6-446a-8d4a-cac5e91c4afa'),
	(183,183,1,'high-school','high-school',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','415cabdb-bdf0-4e9c-a762-dd7fe4ff5838'),
	(184,184,1,'middle-school','middle-school',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','9cbb8978-306d-4f9f-9ddb-b6e63bee2fc5'),
	(185,185,1,'elementary-school','elementary-school',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','f8a06cb1-4cfe-4930-b4fc-80f2f2a518a4'),
	(186,186,1,'high-school','high-school',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','149cd5eb-9579-44cd-83b6-0d326ab0a35f'),
	(187,187,1,'middle-school','middle-school',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','57961ae4-0c24-4958-8450-563b615bea3d'),
	(188,188,1,'elementary-school','elementary-school',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','845f038b-1abf-41d2-b261-9d869354f9b4'),
	(189,189,1,'blocks','blocks',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','f9b67350-bed5-4cd4-b8c7-1bb986dbdf7a'),
	(190,190,1,'blocks','blocks',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','ce3e2a01-f023-4b36-899f-582b060f0a54'),
	(191,191,1,'homepage','__home__',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','8b4fc847-03be-4bb2-8e3f-d2b9f59bde31'),
	(192,192,1,'high-school','high-school',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','c1ee3d89-5504-4b56-aa7b-818c6f9c7db7'),
	(193,193,1,'high-school','high-school',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','0838b3e5-acd5-4145-8a70-ac984f1f6308'),
	(194,194,1,'about','about',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','43cd491d-31da-40c7-84c9-4556f11ece87'),
	(195,195,1,'about','about',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','e954846f-b701-43a3-b824-4565db440438'),
	(196,196,1,'high-school','high-school',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','6639d1f2-7e96-45bb-9018-38966394b375'),
	(197,197,1,'elementary-school','elementary-school',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','6e6f4f91-bb29-4f8c-9c9a-e11774c8e7b3'),
	(198,198,1,'middle-school','middle-school',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','b4cb0328-0ce1-499f-8ccf-78aa883065ad'),
	(199,199,1,'middle-school','middle-school',1,'2019-11-07 15:48:53','2019-11-07 15:48:53','a293eecc-8528-4cf7-b666-9c1dbf98a4a9'),
	(200,200,1,'high-school-1','quotes/high-school-1',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','09092199-056f-4227-b8a8-22718a2199ad'),
	(201,201,1,'elementary-3','quotes/elementary-3',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','e0b3d517-3032-45a9-a141-522052d3ef34'),
	(202,202,1,'elementary-2','quotes/elementary-2',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','299561bf-0b98-4f22-96fb-f1044dfd2aa5'),
	(203,203,1,'middle-school-1','quotes/middle-school-1',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','1442fdfb-5011-496d-b267-444d28eb2991'),
	(204,204,1,'elementary-1','quotes/elementary-1',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','58136627-ace1-4fd4-8fbe-7b545bb764e8'),
	(205,205,1,'elementary-school','elementary-school',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','f4d6f575-6281-42e2-8bcc-b44c940b800a'),
	(206,206,1,'about','about',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','e994b427-7d2f-4a29-b20f-6cbebb810f28'),
	(207,207,1,'homepage','__home__',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','ada09a3e-18ee-4d3b-a5ef-052ba469106b'),
	(208,208,1,'homepage','__home__',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','36ff3326-78f2-40c9-9638-6fe9ca7b1b09'),
	(209,209,1,'homepage','__home__',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','0890e884-7338-47f2-9f18-10014fdd5f1d'),
	(210,210,1,'homepage','__home__',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','d26fb0c3-9b1e-46f7-b08e-8dd1e4d94dd3'),
	(211,211,1,'homepage','__home__',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','b1d1edd0-f0b5-4101-bb85-1b01be980bee'),
	(212,212,1,'homepage','__home__',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','4012503a-43ed-4833-91b6-f3ec2fe45ffa'),
	(213,213,1,'homepage','__home__',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','3717519f-f649-4367-a976-11876dbddc12'),
	(214,214,1,'homepage','__home__',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','1db71550-c745-41d4-a6d5-48dd233452ca'),
	(215,215,1,'homepage','__home__',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','99992835-1de4-4dab-9012-00a3e8db11fe'),
	(216,216,1,'homepage','__home__',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','ec5c0712-f7ea-4b40-9e8e-73e0d72dba37'),
	(217,217,1,'homepage','__home__',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','4a9efa36-0c87-429f-9b8f-5b8d655b0218'),
	(218,218,1,'homepage','__home__',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','cbd7df87-6859-4d8f-9e7c-a1932ff4cd67'),
	(219,219,1,'blocks','blocks',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','54718978-c8d3-4cd9-a4dc-a289b46572b2'),
	(220,220,1,'homepage','__home__',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','900dd149-1c9a-4745-b451-855aa39c029f'),
	(221,221,1,'middle-school','middle-school',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','f41a08cf-5ed7-482e-9bba-d5db39db2559'),
	(222,222,1,'middle-school-1','quotes/middle-school-1',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','1f5fb3ba-9861-4b3f-b22b-4752f8ef6540'),
	(223,223,1,'elementary-1','quotes/elementary-1',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','61f8edd4-74cb-40e9-be8e-4a5e844e0d09'),
	(224,224,1,'test','quotes/test',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','f98d7356-bbc5-4f7e-a187-154749f9669d'),
	(225,225,1,'test2','quotes/test2',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','59937179-4a27-4817-873a-ad52193227ef'),
	(226,226,1,'elementary-1','quotes/elementary-1',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','719d65d7-6bb6-4806-bdad-b1a3e57bd5ca'),
	(227,227,1,'middle-school-1','quotes/middle-school-1',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','4be3e4b9-c7fe-4362-985f-143bfa8ada8d'),
	(228,228,1,'high-school-1','quotes/high-school-1',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','d5ed6a0c-b31e-498b-b60a-3cdc0d99d3de'),
	(229,229,1,'elementary-2','quotes/elementary-2',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','79cdc1c2-8533-4786-8e11-d53f35c291c5'),
	(230,230,1,'elementary-3','quotes/elementary-3',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','5428456a-bc04-4fd1-a3e8-9cdad8bac066'),
	(231,231,1,'elementary-3','quotes/elementary-3',1,'2019-11-07 15:48:54','2019-11-07 15:48:54','c6290942-a12d-46e0-a9a1-388d8a6aa717'),
	(232,232,1,'elementary-2','quotes/elementary-2',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','fab65bf7-6425-4d40-9888-995f4b3efe6e'),
	(233,233,1,'high-school-1','quotes/high-school-1',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','30d2b75c-3fe8-486d-a10c-9ab6c452309d'),
	(234,234,1,'middle-school-1','quotes/middle-school-1',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','640f07d8-c14c-48a9-8da3-65466812fb5d'),
	(235,235,1,'middle-school-1','quotes/middle-school-1',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','36198f3b-cf1a-40b3-9f58-ed25786f12a8'),
	(236,236,1,'elementary-1','quotes/elementary-1',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','161527f8-6b14-43c5-a763-9ed11650f977'),
	(237,237,1,'homepage','__home__',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','a2ea7c44-40a8-4cb0-8e91-89ba95f20a60'),
	(238,238,1,'high-school','high-school',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','05109cde-4547-46db-8f8c-276ec228c999'),
	(239,239,1,'elementary-school','elementary-school',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','95182e46-03e4-43d4-85e8-fa19fb46da4b'),
	(240,240,1,'middle-school','middle-school',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','d793e377-6e07-45a2-a571-6679ca860861'),
	(241,241,1,'homepage','__home__',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','edc2eba6-08ed-4740-b8cc-4f811d8f5b9b'),
	(242,242,1,'homepage','__home__',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','15f2e886-ffd3-460e-91ec-f2f0b7c3d092'),
	(243,243,1,'homepage','__home__',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','65d1d5e8-59a8-4b69-b4a9-6d0249230b33'),
	(244,244,1,'homepage','__home__',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','bddcc45f-874f-440c-b98d-aba48c67ecd8'),
	(245,245,1,'homepage','__home__',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','341189cf-3db3-4991-9e2d-518602aede7f'),
	(246,246,1,'high-school','high-school',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','830f078f-ad04-4449-bc6f-0019220c302f'),
	(247,247,1,'high-school','high-school',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','df064c8c-15cc-4741-97dc-f87cddc03119'),
	(248,248,1,'middle-school','middle-school',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','98cb1ac6-e8b3-4b52-b938-67a1226b11cc'),
	(249,249,1,'elementary-school','elementary-school',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','63f7af27-bc34-4b01-826b-776d0d153022'),
	(250,250,1,'elementary-school','elementary-school',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','36a0a827-88e8-4663-803a-295a7db3f42d'),
	(251,251,1,'about','about',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','faee29ba-beec-46e5-8a92-7e8437aa52e7'),
	(252,252,1,'about','about',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','17ca0525-b75c-4012-885d-5fcec5719524'),
	(253,253,1,'about','about',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','a5578383-ea82-4c99-bcbf-9cab2293e18e'),
	(254,254,1,'homepage','__home__',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','762d4661-2960-4faa-9305-1b5df0186b5b'),
	(255,255,1,'homepage','__home__',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','82b86d4b-ac7e-4986-94fb-57effaf3ab49'),
	(256,256,1,'homepage','__home__',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','e651597c-6bd6-4293-ac2b-601fd1367b2a'),
	(257,257,1,'test-blog-3','blog/test-blog-3',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','04d1dcbb-3d1c-41ae-848d-0630fc530849'),
	(258,258,1,'test-blog2','blog/test-blog2',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','b2470542-5891-402a-97ac-a9e1f9e1672a'),
	(259,259,1,'test-blog','blog/test-blog',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','ad851edd-6fe2-40cd-968d-9ef854134d64'),
	(260,260,1,'contact',NULL,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','f74bac3e-1159-4b6d-abbe-dbeeac3d036f'),
	(261,261,1,'blocks','blocks',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','924265f0-04a5-416c-aff8-e63ef6b8f511'),
	(262,262,1,'test2','quotes/test2',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','ca245eb3-449a-4417-bc5e-991f54f0260f'),
	(263,263,1,'blocks','blocks',1,'2019-11-07 15:48:55','2019-11-07 15:48:55','39937bc0-7a29-4b06-8ea4-2cba1f4147bf'),
	(264,264,1,'blocks','blocks',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','f5c74fe8-9eb9-4255-aaa1-da2756ab392c'),
	(265,265,1,'blocks','blocks',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','ebe2469b-276b-4c63-973b-450413aeacb6'),
	(266,266,1,'homepage','__home__',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','976c5bf6-7f81-42a4-8c34-7c998dee96a0'),
	(267,267,1,'homepage','__home__',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','d370ec35-633b-496d-bd62-30f124422534'),
	(268,268,1,'blocks','blocks',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','3050323e-a323-49d8-801a-f5f796bdae1a'),
	(269,269,1,'test-blog','blog/test-blog',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','0ed08ae7-dc62-4b7c-a920-35d1b37c0adb'),
	(270,270,1,'test-blog','blog/test-blog',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','bb26822f-2ef4-42fb-9292-78b87bba6a5c'),
	(271,271,1,'test','quotes/test',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','dc7e6d9d-696d-4d75-b52d-df1c9448ebd6'),
	(272,272,1,'test','quotes/test',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','678b776a-9f37-4787-85fd-44f7443e0833'),
	(273,273,1,'blocks','blocks',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','94e13006-8e60-402d-9200-cf431226398b'),
	(274,274,1,'blocks','blocks',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','a7b1a1d7-5edf-4e55-8318-1a2d5b4e1bf4'),
	(275,275,1,'contact',NULL,1,'2019-11-07 15:48:56','2019-11-07 15:48:56','f2081464-6dd8-47dd-9161-c1a14baf20b6'),
	(276,276,1,'blocks','blocks',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','7eadf89e-5a81-4810-a56d-98b003a9779f'),
	(277,277,1,'contact',NULL,1,'2019-11-07 15:48:56','2019-11-07 15:48:56','358c6489-5cc9-410a-b22b-a1e851ce6bd0'),
	(278,278,1,'blocks','blocks',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','fa36b354-b254-423f-adc6-2a601c9ce7aa'),
	(279,279,1,'blocks','blocks',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','73f6154f-bd73-401a-a74c-3defc30df676'),
	(280,280,1,'blocks','blocks',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','a5495f0a-b7ab-4a7e-a870-5aa1358962dd'),
	(281,281,1,'blocks','blocks',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','534c4d8b-45ff-4503-ae97-8580ebfce9aa'),
	(282,282,1,'privacy-policy','privacy-policy',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','3cd34fa8-360d-4373-a189-fda220bb804d'),
	(283,283,1,'high-school','high-school',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','2931b4d3-b883-454b-a2a9-91d07f875b5e'),
	(284,284,1,'middle-school','middle-school',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','2b25143e-e298-4ff2-9c5d-f1e887c832c9'),
	(285,285,1,'elementary-school','elementary-school',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','c15b6bb8-8787-4620-80ba-a2fef19f5ac2'),
	(286,286,1,'about','about',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','fb805f1c-87f0-4999-ad08-186dfc193886'),
	(287,287,1,'test','quotes/test',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','76dde372-9ebc-4f63-8df4-5f6b32cc7dd2'),
	(288,288,1,'blog','blog',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','e3dddb79-78ba-4e5b-b81d-8d1d10c53158'),
	(289,289,1,'blocks','blocks',1,'2019-11-07 15:48:56','2019-11-07 15:48:56','ea7bf282-d57b-4c3a-9379-4984f80ec349'),
	(290,290,1,'test-blog','blog/test-blog',1,'2019-11-07 15:48:57','2019-11-07 15:48:57','24aede33-7297-4f2b-b299-2e8534ec79c6'),
	(291,291,1,'homepage','__home__',1,'2019-11-07 16:37:11','2019-11-07 16:37:11','b590a56c-167d-48d4-8088-829f4d9859bd'),
	(292,292,1,'blocks','blocks',1,'2019-11-07 16:38:03','2019-11-07 16:38:03','3fb2c8c6-e5b1-45bc-a0ad-720d8c6331f6');

/*!40000 ALTER TABLE `craft_elements_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_entries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entries`;

CREATE TABLE `craft_entries` (
  `id` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `parentId` int(11) DEFAULT NULL,
  `typeId` int(11) NOT NULL,
  `authorId` int(11) DEFAULT NULL,
  `postDate` datetime DEFAULT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `deletedWithEntryType` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entries_postDate_idx` (`postDate`),
  KEY `craft_entries_expiryDate_idx` (`expiryDate`),
  KEY `craft_entries_authorId_idx` (`authorId`),
  KEY `craft_entries_sectionId_idx` (`sectionId`),
  KEY `craft_entries_typeId_idx` (`typeId`),
  KEY `craft_entries_parentId_fk` (`parentId`),
  CONSTRAINT `craft_entries_authorId_fk` FOREIGN KEY (`authorId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entries_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entries_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `craft_entries` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_entries_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entries_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `craft_entrytypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_entries` WRITE;
/*!40000 ALTER TABLE `craft_entries` DISABLE KEYS */;

INSERT INTO `craft_entries` (`id`, `sectionId`, `parentId`, `typeId`, `authorId`, `postDate`, `expiryDate`, `deletedWithEntryType`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(2,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-01-21 23:39:18','2019-02-08 18:05:31','b4296746-aa86-4bf0-89fd-fda79e4d3aa6'),
	(5,1,NULL,1,1,'2019-01-30 21:22:00',NULL,NULL,'2019-01-30 21:22:22','2019-02-05 20:28:28','19c1293c-ed0e-474c-8b2e-12242821c255'),
	(6,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-01-30 21:22:51','2019-02-27 21:20:40','1814ef2a-f805-4304-aaeb-9ec31eb60349'),
	(7,4,NULL,4,1,'2019-01-30 21:23:00',NULL,0,'2019-01-30 21:23:35','2019-02-05 19:05:20','6a92b9a4-db7a-41a9-8273-69d085ae9f15'),
	(10,5,NULL,5,1,'2019-01-30 21:32:00',NULL,0,'2019-01-30 21:32:20','2019-02-07 22:48:51','2ebe59cd-a22d-4fae-8f89-e2c8afe21c7b'),
	(26,4,NULL,4,1,'2019-01-30 21:49:00',NULL,0,'2019-01-30 21:49:22','2019-02-08 17:36:56','c0d20394-e2a3-4c2c-be60-a955501cdd0d'),
	(27,4,NULL,4,1,'2019-01-30 21:49:00',NULL,0,'2019-01-30 21:49:45','2019-02-12 20:50:49','74e2aa7d-827e-4820-a5e1-23cc9acaf494'),
	(28,4,NULL,4,1,'2019-01-30 21:49:00',NULL,0,'2019-01-30 21:49:56','2019-02-12 20:52:21','c2401890-a983-403e-894f-a0231847c1c5'),
	(29,4,NULL,4,1,'2019-01-30 21:50:00',NULL,0,'2019-01-30 21:50:11','2019-02-12 20:53:06','32898004-733e-4a7b-82ad-b346e6ccb0c7'),
	(38,4,NULL,4,1,'2019-01-30 21:54:00',NULL,0,'2019-01-30 21:55:00','2019-01-30 21:55:00','ad347b88-593a-43ff-8471-6917d71498fc'),
	(67,2,NULL,2,1,'2019-01-30 23:51:00',NULL,NULL,'2019-01-30 23:51:10','2019-02-05 18:47:10','f53afb98-6888-45e6-92d6-1166ff098f3a'),
	(73,5,NULL,5,1,'2019-02-01 20:55:00',NULL,0,'2019-02-01 20:56:00','2019-02-07 22:48:43','eea0a6fd-35cb-45cd-8e84-9af19303f97b'),
	(79,1,NULL,1,1,'2019-02-05 20:29:00',NULL,NULL,'2019-02-05 20:29:19','2019-02-05 20:29:19','05efbfca-3a31-4b0e-8b2a-414f435cfe05'),
	(82,1,NULL,1,1,'2019-02-05 20:30:00',NULL,NULL,'2019-02-05 20:30:08','2019-02-05 20:30:08','b461112f-0f4d-4b45-b04b-ff4afa7aa53a'),
	(144,5,NULL,5,1,'2019-02-07 22:39:00',NULL,0,'2019-02-07 22:39:59','2019-02-08 17:07:53','971677d0-0432-4f3e-95cf-1e165d3bf169'),
	(146,5,NULL,5,1,'2019-02-07 22:41:00',NULL,0,'2019-02-07 22:41:24','2019-02-08 17:09:37','e9ac51f2-385b-4628-8784-64d6d590143f'),
	(147,5,NULL,5,1,'2019-02-07 22:42:00',NULL,0,'2019-02-07 22:42:53','2019-02-08 17:11:34','d27932a8-793a-4509-bc89-d3089ae5da06'),
	(149,5,NULL,5,1,'2019-02-07 22:44:00',NULL,0,'2019-02-07 22:44:14','2019-02-08 17:10:47','ee8c2308-70e7-48e6-b622-855ba367844f'),
	(150,5,NULL,5,1,'2019-02-07 22:45:00',NULL,0,'2019-02-07 22:45:07','2019-02-08 17:11:02','bffa0eb0-748a-4b8b-8709-dccc407e8f78'),
	(181,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','9291cd0b-bdeb-4522-94e0-d300c1cea24c'),
	(182,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','11f90d8d-3d25-4a79-86d3-0eaa9613834e'),
	(183,4,NULL,4,1,'2019-01-30 21:50:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','16f37a91-8eec-4a4d-9d87-c087f08ec252'),
	(184,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','e8d2f492-796b-4e83-b472-97bf0fe002d8'),
	(185,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','63b5866b-1ea3-4409-b0a8-04dd699c5d86'),
	(186,4,NULL,4,1,'2019-01-30 21:50:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','8e3b4ac1-2ca6-42c3-8579-4795643fb5f3'),
	(187,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','445d7253-463b-4360-8def-bea14aa81d91'),
	(188,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','47982faa-7121-4799-baf5-01c7d0e0d1bd'),
	(189,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','dbd9c678-f77b-492f-bfe2-3734fa1bbabd'),
	(190,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','b274235b-beef-4c4a-9a08-93ae033c512d'),
	(191,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','258a7a03-3ba2-436f-ab38-c3cef4466277'),
	(192,4,NULL,4,1,'2019-01-30 21:50:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','6e5d82ff-87e9-4560-bd87-ec6ffb2dabc1'),
	(193,4,NULL,4,1,'2019-01-30 21:50:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','738ce3bd-70d9-4140-a7b4-df5b7510891e'),
	(194,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','073a588e-3dc3-4f29-bb88-fd846fa84cdb'),
	(195,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','ef1d6e19-b731-4710-9f76-7bc0a6922ae9'),
	(196,4,NULL,4,1,'2019-01-30 21:50:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','7d253bf5-4868-48a6-9298-8a917c2e9068'),
	(197,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','bbbafaa7-5b2b-448b-96d8-29539d641f05'),
	(198,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','268f75f8-509c-463d-9147-76e529e80abc'),
	(199,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:53','2019-11-07 15:48:53','514f9547-db98-43f4-9df3-3b4c134dd094'),
	(200,5,NULL,5,1,'2019-02-07 22:42:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','1eea9e24-a148-4b29-a9fe-51b630f77ba1'),
	(201,5,NULL,5,1,'2019-02-07 22:45:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','de7e5e50-bd5a-4d2a-8729-b977d31e8096'),
	(202,5,NULL,5,1,'2019-02-07 22:44:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','48240080-ccdf-49b6-a779-a7e1849c8f4e'),
	(203,5,NULL,5,1,'2019-02-07 22:41:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','97a59b84-526d-4aa4-9015-51448c0fd79b'),
	(204,5,NULL,5,1,'2019-02-07 22:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','f2dab7e4-49dc-4737-a53a-ffa5325993c1'),
	(205,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','734c9ba4-d0a3-4a41-93b7-60bdcaa32cff'),
	(206,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','60093316-b9f1-47e2-b534-56b3428a6696'),
	(207,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','d51ae892-dc69-4492-9515-aceae9c619ba'),
	(208,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','28543098-388f-49ff-ac50-aab2dfe6ea89'),
	(209,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','464fe2a7-0710-4e53-a9be-17684137a3e0'),
	(210,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','4275ac59-de5a-491a-8897-27828e74edc2'),
	(211,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','2bda71d1-d37a-4611-879e-748706029ae5'),
	(212,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','1d7d519b-7418-4539-9294-d31fedaabfb4'),
	(213,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','0b9c0f9f-a397-4474-a824-086aeab7886f'),
	(214,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','0672612b-cf51-48ae-b38c-789d34718416'),
	(215,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','90bb78ff-2612-412f-aae6-d05e343216f6'),
	(216,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','c8ba9650-8773-431b-9d2d-e28533eeffff'),
	(217,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','6727873e-9f5d-49e1-b98a-bf30aa570186'),
	(218,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','2e05a5d3-1879-4e11-8817-32100b10713a'),
	(219,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','9333085c-cfd1-432b-b47a-d442c1f57ffb'),
	(220,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','25d1b1d7-8b6c-4d13-b4cb-17a1b2bead13'),
	(221,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','86e2989d-cc5a-4e09-88bf-3ac3f4c9e721'),
	(222,5,NULL,5,1,'2019-02-07 22:41:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','fbc9df90-33a9-471c-a085-6f4315cfec1b'),
	(223,5,NULL,5,1,'2019-02-07 22:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','e58243a2-a89b-4aa3-9f42-7d91edef4da4'),
	(224,5,NULL,5,1,'2019-01-30 21:32:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','f8788913-4677-4f7d-be56-259f48fcafef'),
	(225,5,NULL,5,1,'2019-02-01 20:55:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','911183ff-1337-4f1d-b0ea-8d4bbb410ec6'),
	(226,5,NULL,5,1,'2019-02-07 22:39:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','a47ca8f8-da9b-4249-a78e-44d6bb29a839'),
	(227,5,NULL,5,1,'2019-02-07 22:41:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','2ff7d076-cd91-4a0c-8731-ac27136bacba'),
	(228,5,NULL,5,1,'2019-02-07 22:42:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','b4e92c61-da5d-4d79-9689-7ea94c291f50'),
	(229,5,NULL,5,1,'2019-02-07 22:44:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','247efa4f-e2a2-4c03-a10b-f21d86c66269'),
	(230,5,NULL,5,1,'2019-02-07 22:45:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','e64e63a0-8e2c-4b3a-b521-3097530364fd'),
	(231,5,NULL,5,1,'2019-02-07 22:45:00',NULL,NULL,'2019-11-07 15:48:54','2019-11-07 15:48:54','f47e53ad-3b35-4581-b533-b29214c89bde'),
	(232,5,NULL,5,1,'2019-02-07 22:44:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','2ba270ad-896e-4e65-aacb-b19597d6082d'),
	(233,5,NULL,5,1,'2019-02-07 22:42:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','6ad33493-5974-4503-b6ea-262d848fa178'),
	(234,5,NULL,5,1,'2019-02-07 22:41:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','45ef7987-2dcd-4f78-90ea-03499dfe3616'),
	(235,5,NULL,5,1,'2019-02-07 22:41:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','e092340f-898e-4e1f-91c4-6a302ed54b55'),
	(236,5,NULL,5,1,'2019-02-07 22:39:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','6b1a63e7-221a-4231-a5c5-cd42dd01bf6c'),
	(237,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','4fdf9b4a-1f57-47bc-8371-2c5d2048e7db'),
	(238,4,NULL,4,1,'2019-01-30 21:50:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','298463e1-5cfb-478d-97b3-f6936fdb7bb2'),
	(239,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','9cef37e2-6f16-47ca-96e9-32fabea7c65a'),
	(240,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','9dc4ee6d-41b5-4527-8ea0-ad90e51025fb'),
	(241,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','7fec7fe4-4fb4-4397-90ce-f6f242b0956e'),
	(242,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','71efac35-78de-448b-a6ce-f680c58cc58c'),
	(243,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','65e32d6a-d963-4fca-a53a-3e0b9a553553'),
	(244,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','f3d55516-d921-4e6c-9484-defa240d215d'),
	(245,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','59e733dd-5e25-43a3-9c17-a3268dd838d7'),
	(246,4,NULL,4,1,'2019-01-30 21:50:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','0cb6289e-36d4-4c30-9e85-c834e9afe4f5'),
	(247,4,NULL,4,1,'2019-01-30 21:50:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','b270e9d5-95c6-4bb2-8b95-f0266dba293a'),
	(248,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','fe1cd406-7181-4108-8892-576fe7e891aa'),
	(249,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','99bc5fd2-bec9-46c0-8e6c-75743609f4e8'),
	(250,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','640b0b17-7d63-45c1-b608-8e9fbe2c49bb'),
	(251,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','a2acff9a-b3b9-41eb-b0dd-f47cfffb819a'),
	(252,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','65be8690-879b-47f8-a110-20cbd49a8b39'),
	(253,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','0c102943-b9bd-40bf-b3c6-378b851d2e60'),
	(254,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','a00a0480-ffb8-497d-8a6a-7006eaab8c24'),
	(255,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','9daa96b7-890f-4c2f-9d5a-1ae5df5f0315'),
	(256,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','2ecbfd97-2efe-4664-b248-c511b85cb46b'),
	(257,1,NULL,1,1,'2019-02-05 20:30:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','97b4e23d-33e4-4589-a7c4-1ddfd80525dc'),
	(258,1,NULL,1,1,'2019-02-05 20:29:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','b25975c3-1086-4279-9ebf-09cc17b91d2e'),
	(259,1,NULL,1,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','8b25c8e1-9df1-4223-aee1-b572a4228146'),
	(260,2,NULL,2,1,'2019-01-30 23:51:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','a07a36d8-0de2-4c1f-9064-a6bcaa6ef62f'),
	(261,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','2bcd267f-1eaf-4407-8e02-547075dcad34'),
	(262,5,NULL,5,1,'2019-02-01 20:55:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','bf9b73e0-75a3-4a9b-b2d8-a4242f9712df'),
	(263,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:55','2019-11-07 15:48:55','9dfbfc0b-a822-4d3f-8c93-b53caec9ba94'),
	(264,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','479eb2d1-7f2d-4cbe-9799-26a983d24cf8'),
	(265,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','503b3bfe-ce0e-4ee4-9371-9716846817ac'),
	(266,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','1a9539d4-c313-4d4a-a4b3-10660b49e693'),
	(267,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','feae0191-9be3-43a7-b5cf-3666022c9294'),
	(268,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','3b50aa0c-3b7a-409a-9c87-aea8187e4307'),
	(269,1,NULL,1,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','fcd22e52-f8a2-4938-889a-960aa221b091'),
	(270,1,NULL,1,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','ce2a1598-c27d-4dad-84e0-b2c09ca80fd2'),
	(271,5,NULL,5,1,'2019-01-30 21:32:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','751610a9-15e4-4e1a-bc5f-9cdcd873a50a'),
	(272,5,NULL,5,1,'2019-01-30 21:32:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','5178fe9e-343b-4187-b281-a2227f91ebb3'),
	(273,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','56351cce-9a81-4745-8c58-8a040ff9dc34'),
	(274,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','d14651ab-cc48-4b0c-878f-8c7a6b958003'),
	(275,2,NULL,2,1,'2019-01-30 23:51:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','6a0e731a-12d2-494b-b843-17483dabfb85'),
	(276,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','8e1d10a0-cb2a-4eb9-ae36-4a786cf08c88'),
	(277,2,NULL,2,1,'2019-01-30 23:51:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','d1bea850-5916-4028-9071-24be396e71cc'),
	(278,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','69179b0b-c059-459c-9391-7e969079f8fd'),
	(279,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','1390127f-8494-4b1a-b5bb-51e8465887b9'),
	(280,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','1d964908-3afd-44fa-bc9b-83308857e246'),
	(281,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','f25b1b7d-6717-408a-93e5-3444a01d02e9'),
	(282,4,NULL,4,1,'2019-01-30 21:54:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','d74227e0-b8d2-4035-8a25-2c392ffb0b49'),
	(283,4,NULL,4,1,'2019-01-30 21:50:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','da9a4814-9b4e-48ed-a241-aadce3aa0162'),
	(284,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','db783254-a156-4a9e-8a47-e464a57965c7'),
	(285,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','e48a82d5-4364-481e-8e0a-2cf1ea39dbb2'),
	(286,4,NULL,4,1,'2019-01-30 21:49:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','d0f9aa4a-44c4-4382-9508-843c2aab6e01'),
	(287,5,NULL,5,1,'2019-01-30 21:32:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','f43f92ec-09bb-47b3-a5d5-a5be33b31018'),
	(288,4,NULL,4,1,'2019-01-30 21:23:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','75335619-0dcb-47c5-a28b-c6fa653e4e09'),
	(289,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:56','2019-11-07 15:48:56','1928854b-a549-4845-af06-d3f53d1acb84'),
	(290,1,NULL,1,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 15:48:57','2019-11-07 15:48:57','37ae8318-5745-40ad-9c9d-fbf75e633905'),
	(291,3,NULL,3,NULL,'2019-01-21 23:39:00',NULL,NULL,'2019-11-07 16:37:11','2019-11-07 16:37:11','d8bece52-5ee7-4374-b615-e9bf59454777'),
	(292,4,NULL,4,1,'2019-01-30 21:22:00',NULL,NULL,'2019-11-07 16:38:03','2019-11-07 16:38:03','062c8a56-f9a8-4e0b-8aa5-a7aa5107f7db');

/*!40000 ALTER TABLE `craft_entries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_entrydrafterrors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entrydrafterrors`;

CREATE TABLE `craft_entrydrafterrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `draftId` int(11) DEFAULT NULL,
  `error` text,
  PRIMARY KEY (`id`),
  KEY `craft_entrydrafterrors_draftId_fk` (`draftId`),
  CONSTRAINT `craft_entrydrafterrors_draftId_fk` FOREIGN KEY (`draftId`) REFERENCES `craft_entrydrafts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_entrydrafts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entrydrafts`;

CREATE TABLE `craft_entrydrafts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `notes` text,
  `data` mediumtext NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entrydrafts_sectionId_idx` (`sectionId`),
  KEY `craft_entrydrafts_entryId_siteId_idx` (`entryId`,`siteId`),
  KEY `craft_entrydrafts_siteId_idx` (`siteId`),
  KEY `craft_entrydrafts_creatorId_idx` (`creatorId`),
  CONSTRAINT `craft_entrydrafts_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entrydrafts_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `craft_entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entrydrafts_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entrydrafts_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_entrytypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entrytypes`;

CREATE TABLE `craft_entrytypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `hasTitleField` tinyint(1) NOT NULL DEFAULT '1',
  `titleLabel` varchar(255) DEFAULT 'Title',
  `titleFormat` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entrytypes_name_sectionId_idx` (`name`,`sectionId`),
  KEY `craft_entrytypes_handle_sectionId_idx` (`handle`,`sectionId`),
  KEY `craft_entrytypes_sectionId_idx` (`sectionId`),
  KEY `craft_entrytypes_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `craft_entrytypes_dateDeleted_idx` (`dateDeleted`),
  CONSTRAINT `craft_entrytypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_entrytypes_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_entrytypes` WRITE;
/*!40000 ALTER TABLE `craft_entrytypes` DISABLE KEYS */;

INSERT INTO `craft_entrytypes` (`id`, `sectionId`, `fieldLayoutId`, `name`, `handle`, `hasTitleField`, `titleLabel`, `titleFormat`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,1,84,'Blog','blog',1,'Title','',1,'2019-01-21 23:38:24','2019-01-31 16:43:11',NULL,'bf493d22-2155-483d-8cc0-b48adee39630'),
	(2,2,98,'Call to Action','callToAction',1,'Title','',1,'2019-01-21 23:38:48','2019-02-05 18:46:06',NULL,'8d3494c2-1513-45e9-a7f0-87fe59b20d1b'),
	(3,3,110,'Homepage','homepage',0,'','{section.name|raw}',1,'2019-01-21 23:39:17','2019-01-31 20:56:19',NULL,'cb6bdd31-3298-4c0e-9676-069e9f37f4af'),
	(4,4,83,'Page','page',1,'Title','',1,'2019-01-21 23:39:47','2019-01-22 16:11:58',NULL,'0165d7fd-bf82-487d-82e3-53911dc66c54'),
	(5,5,85,'Quotes','quotes',1,'Title','',1,'2019-01-22 16:10:41','2019-01-30 23:37:21',NULL,'a74dd557-d92a-4a60-8880-e6d1a7cf5101');

/*!40000 ALTER TABLE `craft_entrytypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_entryversionerrors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entryversionerrors`;

CREATE TABLE `craft_entryversionerrors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `versionId` int(11) DEFAULT NULL,
  `error` text,
  PRIMARY KEY (`id`),
  KEY `craft_entryversionerrors_versionId_fk` (`versionId`),
  CONSTRAINT `craft_entryversionerrors_versionId_fk` FOREIGN KEY (`versionId`) REFERENCES `craft_entryversions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_entryversions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_entryversions`;

CREATE TABLE `craft_entryversions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryId` int(11) NOT NULL,
  `sectionId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `siteId` int(11) NOT NULL,
  `num` smallint(6) unsigned NOT NULL,
  `notes` text,
  `data` mediumtext NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_entryversions_sectionId_idx` (`sectionId`),
  KEY `craft_entryversions_entryId_siteId_idx` (`entryId`,`siteId`),
  KEY `craft_entryversions_siteId_idx` (`siteId`),
  KEY `craft_entryversions_creatorId_idx` (`creatorId`),
  CONSTRAINT `craft_entryversions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_entryversions_entryId_fk` FOREIGN KEY (`entryId`) REFERENCES `craft_entries` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entryversions_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_entryversions_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_fieldgroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldgroups`;

CREATE TABLE `craft_fieldgroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_fieldgroups_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_fieldgroups` WRITE;
/*!40000 ALTER TABLE `craft_fieldgroups` DISABLE KEYS */;

INSERT INTO `craft_fieldgroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Common','2019-01-21 23:30:43','2019-01-21 23:30:43','cd825c6f-1d25-4823-a212-ce4079c6e4e7'),
	(2,'Block Options','2019-01-21 23:45:09','2019-01-21 23:45:09','b7db88d8-8d51-4759-86c6-d83b1fc1ea65'),
	(3,'Blocks','2019-01-21 23:45:18','2019-01-21 23:45:18','46c1ca72-5d1d-49c5-aeb8-3aa4c7ab1a9f'),
	(4,'Blog','2019-01-21 23:45:23','2019-01-21 23:45:23','024ef342-aed2-46fe-8110-f248b4ca01e2'),
	(5,'Call to Action','2019-01-21 23:45:34','2019-01-21 23:45:34','52840786-f113-4e5f-94a3-eded8dabafbb'),
	(6,'General Fields','2019-01-21 23:45:45','2019-01-21 23:45:45','688ea26c-2c9a-4765-b3e7-ba03c09c5043'),
	(7,'Page Options','2019-01-21 23:45:57','2019-01-21 23:45:57','c3ef8c2d-34fe-4922-8fcd-492e567df532'),
	(8,'Theme Options','2019-01-21 23:46:07','2019-01-21 23:46:07','a7068d61-4021-48c1-a6dc-fdd50446baba'),
	(9,'Quote','2019-01-30 21:29:13','2019-01-30 21:29:13','5206e00d-9d76-4558-b13d-48c8882111e2');

/*!40000 ALTER TABLE `craft_fieldgroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fieldlayoutfields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldlayoutfields`;

CREATE TABLE `craft_fieldlayoutfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `tabId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_fieldlayoutfields_layoutId_fieldId_unq_idx` (`layoutId`,`fieldId`),
  KEY `craft_fieldlayoutfields_sortOrder_idx` (`sortOrder`),
  KEY `craft_fieldlayoutfields_tabId_idx` (`tabId`),
  KEY `craft_fieldlayoutfields_fieldId_idx` (`fieldId`),
  CONSTRAINT `craft_fieldlayoutfields_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fieldlayoutfields_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_fieldlayoutfields_tabId_fk` FOREIGN KEY (`tabId`) REFERENCES `craft_fieldlayouttabs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_fieldlayoutfields` WRITE;
/*!40000 ALTER TABLE `craft_fieldlayoutfields` DISABLE KEYS */;

INSERT INTO `craft_fieldlayoutfields` (`id`, `layoutId`, `tabId`, `fieldId`, `required`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(39,13,13,38,0,1,'2019-01-22 15:42:57','2019-01-22 15:42:57','f8963c77-9ba7-4c2d-81d5-cc90a8685f5c'),
	(40,13,14,1,0,1,'2019-01-22 15:42:57','2019-01-22 15:42:57','c14a6437-d99c-468b-bbd1-4e79e261a939'),
	(41,13,14,2,0,2,'2019-01-22 15:42:57','2019-01-22 15:42:57','62701a19-4006-4d10-a007-8c6e49e7d803'),
	(42,13,14,4,0,3,'2019-01-22 15:42:57','2019-01-22 15:42:57','f2678717-d53d-406d-b025-e4fea6b3aa35'),
	(43,13,14,5,0,4,'2019-01-22 15:42:57','2019-01-22 15:42:57','d44844c1-1883-4257-8436-d5d329d1293e'),
	(44,14,15,51,0,1,'2019-01-22 15:42:57','2019-01-22 15:42:57','077e1148-5f2c-4c61-b345-b351707ecaa7'),
	(45,14,15,49,0,2,'2019-01-22 15:42:57','2019-01-22 15:42:57','0cfec636-3edf-4481-a759-540c873a3f60'),
	(46,14,15,2,0,3,'2019-01-22 15:42:57','2019-01-22 15:42:57','7f5db88d-e623-44dd-bae7-6c2aa4aae440'),
	(47,15,16,52,0,1,'2019-01-22 15:42:57','2019-01-22 15:42:57','5a55ed32-17cf-4275-a6e5-1ba3163eea06'),
	(48,16,17,41,0,1,'2019-01-22 15:42:57','2019-01-22 15:42:57','51fabcb6-e999-41a7-afc9-69efb642a44a'),
	(49,16,18,1,0,1,'2019-01-22 15:42:57','2019-01-22 15:42:57','4e8998b7-f9d2-4db1-aba9-7f48a22e8f59'),
	(50,16,18,2,0,2,'2019-01-22 15:42:57','2019-01-22 15:42:57','332fa4c9-baa8-4d94-a47d-b3a7f6f390d7'),
	(51,16,18,4,0,3,'2019-01-22 15:42:57','2019-01-22 15:42:57','6b78aa47-d3df-4535-9d8c-a807c5a0ee3a'),
	(52,16,18,5,0,4,'2019-01-22 15:42:57','2019-01-22 15:42:57','aac1e4cd-f8f5-4d07-baa5-a5a150867662'),
	(53,17,19,37,0,1,'2019-01-22 15:42:57','2019-01-22 15:42:57','737cab45-0e23-46e4-bf34-e1c10b2c2bc8'),
	(54,17,20,1,0,1,'2019-01-22 15:42:57','2019-01-22 15:42:57','f460a3c8-31ed-4a3d-b4a8-fff23b00840b'),
	(55,17,20,2,0,2,'2019-01-22 15:42:57','2019-01-22 15:42:57','7236d7a1-699b-4bfd-b9e6-ed098eddeb64'),
	(56,17,20,4,0,3,'2019-01-22 15:42:57','2019-01-22 15:42:57','da50694e-1578-4754-ab05-af04afa4b278'),
	(57,17,20,5,0,4,'2019-01-22 15:42:57','2019-01-22 15:42:57','3753e63e-3864-4e45-952d-a4e411550d55'),
	(58,18,21,38,0,1,'2019-01-22 15:47:51','2019-01-22 15:47:51','8e4ccd76-8c25-4e5e-a7a4-bb68dae5960e'),
	(59,18,22,1,0,1,'2019-01-22 15:47:51','2019-01-22 15:47:51','62d0f1d5-9823-45f7-9dd0-75d748edbab6'),
	(60,18,22,2,0,2,'2019-01-22 15:47:51','2019-01-22 15:47:51','be6c4441-e44c-44ff-b798-2c441b2526b0'),
	(61,18,22,4,0,3,'2019-01-22 15:47:51','2019-01-22 15:47:51','694dda9f-16fa-48d0-8e99-ca61ed6f2b8f'),
	(62,18,22,5,0,4,'2019-01-22 15:47:51','2019-01-22 15:47:51','25ba99a7-f824-4e10-816c-9db6901864a2'),
	(63,19,24,1,0,1,'2019-01-22 15:47:51','2019-01-22 15:47:51','bab01195-4d2f-4a24-8b1d-e1314bcf1f9e'),
	(64,19,24,2,0,2,'2019-01-22 15:47:51','2019-01-22 15:47:51','6ec3f80f-0849-4e38-9ff1-cdfcb42c1ef1'),
	(65,19,24,4,0,3,'2019-01-22 15:47:51','2019-01-22 15:47:51','eb615f74-11a4-4e4e-b139-6847de460f70'),
	(66,19,24,5,0,4,'2019-01-22 15:47:51','2019-01-22 15:47:51','1e244b5c-eec1-450b-8412-94eb3875d96c'),
	(67,20,25,49,0,1,'2019-01-22 15:47:51','2019-01-22 15:47:51','41bc0ecd-7aff-4976-95c9-6dbb9a85c5ed'),
	(68,21,26,51,0,1,'2019-01-22 15:47:51','2019-01-22 15:47:51','9acdc666-e530-453e-b0af-08a5f3cef000'),
	(69,21,26,49,0,2,'2019-01-22 15:47:51','2019-01-22 15:47:51','ea90bbbe-ba9b-4ab2-9ad7-43d8b5aa8ae4'),
	(70,21,26,2,0,3,'2019-01-22 15:47:51','2019-01-22 15:47:51','39adfaf8-2e5b-4329-8bd0-db44a32f1a3f'),
	(71,22,27,52,0,1,'2019-01-22 15:47:52','2019-01-22 15:47:52','8b4a9ee9-b6f5-43a2-a670-781550b13967'),
	(72,23,28,41,0,1,'2019-01-22 15:47:52','2019-01-22 15:47:52','2d4e5ce3-c256-4734-8032-c99c3b838adf'),
	(73,23,29,1,0,1,'2019-01-22 15:47:52','2019-01-22 15:47:52','a6e8917a-a276-4a35-98b5-45903050c8cf'),
	(74,23,29,2,0,2,'2019-01-22 15:47:52','2019-01-22 15:47:52','d5c05335-cb2d-4dba-84dc-c3ff6ae28a6c'),
	(75,23,29,4,0,3,'2019-01-22 15:47:52','2019-01-22 15:47:52','3e2cf249-58eb-4bbc-b614-b47b97b45d0f'),
	(76,23,29,5,0,4,'2019-01-22 15:47:52','2019-01-22 15:47:52','45cccf3e-aa68-4f4d-b87f-8136fe6ae3e6'),
	(77,24,30,37,0,1,'2019-01-22 15:47:52','2019-01-22 15:47:52','8d87073f-9909-4d0d-b952-9cfeab49258c'),
	(78,24,31,1,0,1,'2019-01-22 15:47:52','2019-01-22 15:47:52','4c5ee4a9-b8df-4ebd-91b9-40d01bce0e32'),
	(79,24,31,2,0,2,'2019-01-22 15:47:52','2019-01-22 15:47:52','e5a17edf-76a9-4334-8d29-b00931bd7b6a'),
	(80,24,31,4,0,3,'2019-01-22 15:47:52','2019-01-22 15:47:52','8a4568b8-0d12-45ab-8f8f-defd0089ed88'),
	(81,24,31,5,0,4,'2019-01-22 15:47:52','2019-01-22 15:47:52','a37df315-43a3-41a1-8ecf-42cafddc19f3'),
	(111,25,43,38,0,1,'2019-01-22 15:55:26','2019-01-22 15:55:26','3213a0a7-7a63-4bc3-852c-51dd843a007a'),
	(112,25,44,1,0,1,'2019-01-22 15:55:26','2019-01-22 15:55:26','e2d945ca-7112-481b-832a-fc60e02411f4'),
	(113,25,44,2,0,2,'2019-01-22 15:55:26','2019-01-22 15:55:26','f19db0cb-fd44-470d-99b8-7fd8e4358ca4'),
	(114,25,44,4,0,3,'2019-01-22 15:55:26','2019-01-22 15:55:26','251f1c95-90f0-4afd-8f28-d45ab4e741eb'),
	(115,25,44,5,0,4,'2019-01-22 15:55:26','2019-01-22 15:55:26','fe744ecb-9897-4b4b-acda-c1749543c7ef'),
	(116,26,45,51,0,1,'2019-01-22 15:55:26','2019-01-22 15:55:26','761d5ff1-4ea2-4b06-b567-d7b67707e30d'),
	(117,26,45,62,0,2,'2019-01-22 15:55:26','2019-01-22 15:55:26','6e31a618-cfb4-45f5-8cf6-7ba6817061e8'),
	(118,26,45,63,0,3,'2019-01-22 15:55:26','2019-01-22 15:55:26','9e65ab6e-845d-4a3c-a7bd-18a4283d2844'),
	(119,26,45,64,0,4,'2019-01-22 15:55:26','2019-01-22 15:55:26','b967035f-e0c8-4675-878e-ecbfb3bb2f73'),
	(120,26,45,49,0,5,'2019-01-22 15:55:26','2019-01-22 15:55:26','26d44e28-111a-4e94-9a51-0decd86ba624'),
	(121,26,46,1,0,1,'2019-01-22 15:55:26','2019-01-22 15:55:26','49ba34a9-1737-4743-848c-d53e612d6036'),
	(122,26,46,2,0,2,'2019-01-22 15:55:26','2019-01-22 15:55:26','298b7b4e-7a02-4384-a9aa-b3ba467e701f'),
	(123,26,46,4,0,3,'2019-01-22 15:55:26','2019-01-22 15:55:26','e941622b-157c-467b-8b16-f1572e5f639b'),
	(124,26,46,5,0,4,'2019-01-22 15:55:26','2019-01-22 15:55:26','2758fb5b-c2cb-4ace-b229-83319d130930'),
	(125,27,47,49,0,1,'2019-01-22 15:55:27','2019-01-22 15:55:27','95f4e0e1-dbc3-4ab0-9e32-284b9635b09b'),
	(126,28,48,51,0,1,'2019-01-22 15:55:27','2019-01-22 15:55:27','74fe205a-1d0f-411b-9b78-5bb9d3ea259c'),
	(127,28,48,49,0,2,'2019-01-22 15:55:27','2019-01-22 15:55:27','502ab314-54af-4e3e-9ae6-805336288289'),
	(128,28,48,2,0,3,'2019-01-22 15:55:27','2019-01-22 15:55:27','8968b609-c7ce-41cb-96f5-4d60eaf6555b'),
	(129,29,49,52,0,1,'2019-01-22 15:55:27','2019-01-22 15:55:27','e4be9ffc-32f2-4628-b429-fa4b5e8572e0'),
	(130,30,50,41,0,1,'2019-01-22 15:55:27','2019-01-22 15:55:27','6b6d3b84-58cf-4fa4-9d61-7d1aa7d508d6'),
	(131,30,51,1,0,1,'2019-01-22 15:55:27','2019-01-22 15:55:27','6270d583-38d2-4f13-b857-4c16e587a337'),
	(132,30,51,2,0,2,'2019-01-22 15:55:27','2019-01-22 15:55:27','e1caa533-86f2-40e5-9637-524bc165c03d'),
	(133,30,51,4,0,3,'2019-01-22 15:55:27','2019-01-22 15:55:27','271b2f96-d9ec-4780-b525-85e12393784c'),
	(134,30,51,5,0,4,'2019-01-22 15:55:27','2019-01-22 15:55:27','d674278b-847d-404b-bf03-9c069c4d7567'),
	(135,31,52,37,0,1,'2019-01-22 15:55:27','2019-01-22 15:55:27','555ad877-28c9-457f-b97d-bbe1e7501df7'),
	(136,31,53,1,0,1,'2019-01-22 15:55:27','2019-01-22 15:55:27','7ebbfe16-eab4-4246-8715-e0ca29c46022'),
	(137,31,53,2,0,2,'2019-01-22 15:55:27','2019-01-22 15:55:27','34b870ec-c61f-4579-9cc3-0c551ce0f384'),
	(138,31,53,4,0,3,'2019-01-22 15:55:27','2019-01-22 15:55:27','b6dd2510-4c8c-4273-b43a-c0b6d45b22f7'),
	(139,31,53,5,0,4,'2019-01-22 15:55:27','2019-01-22 15:55:27','c88b3059-1892-4a5d-a2a4-27a09ab1a85b'),
	(143,32,55,38,0,1,'2019-01-22 15:56:26','2019-01-22 15:56:26','c2f4c38d-d89d-4ace-b190-ec76aac3e647'),
	(144,32,56,1,0,1,'2019-01-22 15:56:26','2019-01-22 15:56:26','716795d8-2910-4eef-8620-f44c57703fc6'),
	(145,32,56,2,0,2,'2019-01-22 15:56:26','2019-01-22 15:56:26','d78b2e5c-de02-4aae-9d7c-d6d58b3ed337'),
	(146,32,56,4,0,3,'2019-01-22 15:56:26','2019-01-22 15:56:26','fefb7ea6-c3f1-4fa8-9c47-0b1f5bd79cf4'),
	(147,32,56,5,0,4,'2019-01-22 15:56:26','2019-01-22 15:56:26','126fa2d3-6986-4cc1-a024-b1a1ebd70217'),
	(148,33,57,51,0,1,'2019-01-22 15:56:26','2019-01-22 15:56:26','080fb070-6b00-4cb7-a80d-991d159a68c9'),
	(149,33,57,62,0,2,'2019-01-22 15:56:26','2019-01-22 15:56:26','57795037-8568-4f5b-8c28-e5b5ffa92519'),
	(150,33,57,63,0,3,'2019-01-22 15:56:26','2019-01-22 15:56:26','d81dbe82-2ff5-43c4-96ec-3df0d0ca111a'),
	(151,33,57,64,0,4,'2019-01-22 15:56:26','2019-01-22 15:56:26','2118dcc1-9d9f-465d-97a5-39fd51ccb18f'),
	(152,33,57,49,0,5,'2019-01-22 15:56:26','2019-01-22 15:56:26','accc02c3-86ef-40fa-bad3-d7537d8c72a4'),
	(153,33,58,1,0,1,'2019-01-22 15:56:26','2019-01-22 15:56:26','7edb8668-a153-4307-b2e5-cf3011aabf93'),
	(154,33,58,2,0,2,'2019-01-22 15:56:26','2019-01-22 15:56:26','2ce0d7db-af5f-4ed3-96d6-283166230f72'),
	(155,33,58,4,0,3,'2019-01-22 15:56:26','2019-01-22 15:56:26','7b27518a-3227-4e07-9084-0ce46554f346'),
	(156,33,58,5,0,4,'2019-01-22 15:56:26','2019-01-22 15:56:26','fab9e43b-5d6c-4791-81d2-44d96d196ae9'),
	(157,34,59,49,0,1,'2019-01-22 15:56:26','2019-01-22 15:56:26','c5da8974-d45f-40a5-9aee-083c77904c7f'),
	(158,35,60,51,0,1,'2019-01-22 15:56:26','2019-01-22 15:56:26','ec21ae1a-6641-4234-83c4-4d0b2b5ff3db'),
	(159,35,60,49,0,2,'2019-01-22 15:56:26','2019-01-22 15:56:26','8392e76f-e835-4c10-aebc-280515b6623c'),
	(160,35,60,2,0,3,'2019-01-22 15:56:26','2019-01-22 15:56:26','87bec4e1-7293-4d33-a286-e9d7563ad53f'),
	(161,36,61,52,0,1,'2019-01-22 15:56:26','2019-01-22 15:56:26','64c4f6fc-99fe-4ace-bd31-e9dcb2c12eee'),
	(162,37,62,41,0,1,'2019-01-22 15:56:26','2019-01-22 15:56:26','74aa58e9-0612-4c82-9e95-b33fb75bd228'),
	(163,37,63,1,0,1,'2019-01-22 15:56:26','2019-01-22 15:56:26','ec9349a7-2460-42bf-a0c9-ffd242feaa1f'),
	(164,37,63,2,0,2,'2019-01-22 15:56:26','2019-01-22 15:56:26','117a0853-26f0-471b-ae3b-9888a382567d'),
	(165,37,63,4,0,3,'2019-01-22 15:56:26','2019-01-22 15:56:26','f4c8d728-ff4f-4c73-ae6d-986f7bdf80b8'),
	(166,37,63,5,0,4,'2019-01-22 15:56:26','2019-01-22 15:56:26','8b515a7f-dce2-4e02-be0f-39263964a114'),
	(167,38,64,37,0,1,'2019-01-22 15:56:27','2019-01-22 15:56:27','da56ee5b-7915-46b4-af80-e550ef6aad85'),
	(168,38,65,1,0,1,'2019-01-22 15:56:27','2019-01-22 15:56:27','33104543-90a7-4014-91b1-eb4df3ad3f04'),
	(169,38,65,2,0,2,'2019-01-22 15:56:27','2019-01-22 15:56:27','bf3a820e-5ac0-4922-a01b-f2ffaa1df354'),
	(170,38,65,4,0,3,'2019-01-22 15:56:27','2019-01-22 15:56:27','35b6acaa-8561-445c-9a7a-501035c721d2'),
	(171,38,65,5,0,4,'2019-01-22 15:56:27','2019-01-22 15:56:27','805979e2-82b5-4056-9c0b-dc733ffda9ef'),
	(172,40,67,38,0,1,'2019-01-22 15:59:52','2019-01-22 15:59:52','61b45bdb-a1e8-4777-8fe2-c687da6f142f'),
	(173,40,68,1,0,1,'2019-01-22 15:59:52','2019-01-22 15:59:52','8448193f-e1a1-4830-a8d4-330921fc5f24'),
	(174,40,68,2,0,2,'2019-01-22 15:59:52','2019-01-22 15:59:52','b12290e0-dbd1-4e0d-8f77-d253953c14d5'),
	(175,40,68,4,0,3,'2019-01-22 15:59:52','2019-01-22 15:59:52','857516e3-6f12-4e32-ade0-e557dd53abf5'),
	(176,40,68,5,0,4,'2019-01-22 15:59:52','2019-01-22 15:59:52','0df5c5b4-dda1-438a-80ab-f4ec528beeea'),
	(177,41,69,51,0,1,'2019-01-22 15:59:52','2019-01-22 15:59:52','e8aaecc9-5a6a-4bed-867b-81e8ce11a1db'),
	(178,41,69,62,0,2,'2019-01-22 15:59:52','2019-01-22 15:59:52','e0eb01d0-12bc-4f67-aac8-1b678564c114'),
	(179,41,69,63,0,3,'2019-01-22 15:59:52','2019-01-22 15:59:52','1280ab1f-fee8-4815-8815-dfd638927490'),
	(180,41,69,64,0,4,'2019-01-22 15:59:52','2019-01-22 15:59:52','c1cd099e-1b3a-4bb3-ad16-66f2abfc046e'),
	(181,41,69,49,0,5,'2019-01-22 15:59:52','2019-01-22 15:59:52','9ccd339f-0b93-46c7-be7c-9e208339e28d'),
	(182,41,70,1,0,1,'2019-01-22 15:59:52','2019-01-22 15:59:52','3b894d17-1b8c-45b1-85f5-93d91161b2d3'),
	(183,41,70,2,0,2,'2019-01-22 15:59:52','2019-01-22 15:59:52','90fe3647-4c69-4c39-a732-8e9f645c2ee0'),
	(184,41,70,4,0,3,'2019-01-22 15:59:52','2019-01-22 15:59:52','a2879635-9104-4a24-bff7-fa1c087434ed'),
	(185,41,70,5,0,4,'2019-01-22 15:59:52','2019-01-22 15:59:52','68c1e5da-14de-44a5-8a04-a134cd207749'),
	(186,42,71,49,0,1,'2019-01-22 15:59:52','2019-01-22 15:59:52','e787ba63-ceb2-422c-a5f6-b7cf41a56d3b'),
	(187,43,72,51,0,1,'2019-01-22 15:59:52','2019-01-22 15:59:52','cbf68e9e-d3ff-423f-9915-c596ca3c02b7'),
	(188,43,72,49,0,2,'2019-01-22 15:59:52','2019-01-22 15:59:52','f54de12a-c738-426b-966c-687a8ddf75c0'),
	(189,43,72,2,0,3,'2019-01-22 15:59:52','2019-01-22 15:59:52','29c6d5c8-b3e1-4007-9e95-a1da1ccaa036'),
	(190,44,73,52,0,1,'2019-01-22 15:59:52','2019-01-22 15:59:52','777ba524-d72b-40a3-9e75-a2142cc313f7'),
	(191,45,74,41,0,1,'2019-01-22 15:59:52','2019-01-22 15:59:52','3ea47d40-2f1e-4774-89b9-2cf0d599afaa'),
	(192,45,75,1,0,1,'2019-01-22 15:59:52','2019-01-22 15:59:52','dba3ec37-cbd3-4729-9c45-03e0010ddbfe'),
	(193,45,75,2,0,2,'2019-01-22 15:59:52','2019-01-22 15:59:52','30cc98f1-958d-40e8-99fd-136e5e467f10'),
	(194,45,75,4,0,3,'2019-01-22 15:59:52','2019-01-22 15:59:52','f4e8cdd2-96b0-4b85-a6d6-b1d4c010b87a'),
	(195,45,75,5,0,4,'2019-01-22 15:59:52','2019-01-22 15:59:52','58123809-1cfd-452d-9b0e-264deaadd00c'),
	(196,46,76,37,0,1,'2019-01-22 15:59:52','2019-01-22 15:59:52','c76d0ed5-20a4-4601-a4b1-30900c34b14e'),
	(197,46,77,1,0,1,'2019-01-22 15:59:52','2019-01-22 15:59:52','3cdf28b7-422e-4c25-ba98-9bb33656523d'),
	(198,46,77,2,0,2,'2019-01-22 15:59:52','2019-01-22 15:59:52','5fc401e7-d5fd-4f45-8986-57e38843813d'),
	(199,46,77,4,0,3,'2019-01-22 15:59:52','2019-01-22 15:59:52','689b63a0-03d1-4a1c-99ed-c7f73e89fd3e'),
	(200,46,77,5,0,4,'2019-01-22 15:59:52','2019-01-22 15:59:52','962c31ce-05e6-40c4-9acc-ca4729c59baa'),
	(201,47,78,47,0,1,'2019-01-22 15:59:52','2019-01-22 15:59:52','c800224e-6de9-4266-9e38-bf7883380e25'),
	(202,48,79,47,0,1,'2019-01-22 15:59:52','2019-01-22 15:59:52','2cab884f-5d9d-42ef-91eb-c45e34538eac'),
	(203,49,80,38,0,1,'2019-01-22 16:00:55','2019-01-22 16:00:55','152f1720-bf9e-44f1-b624-2a6494875503'),
	(204,49,81,1,0,1,'2019-01-22 16:00:55','2019-01-22 16:00:55','43b9a2c1-51e9-42fa-b6c7-31d950d6ff17'),
	(205,49,81,2,0,2,'2019-01-22 16:00:55','2019-01-22 16:00:55','b5ab03db-9e51-49d3-80eb-e824582cd88e'),
	(206,49,81,4,0,3,'2019-01-22 16:00:55','2019-01-22 16:00:55','36ab8731-e7bb-49c8-b34a-f58a2b184d72'),
	(207,49,81,5,0,4,'2019-01-22 16:00:55','2019-01-22 16:00:55','da91d40a-8492-4584-911c-78377bd315ec'),
	(208,50,82,51,0,1,'2019-01-22 16:00:55','2019-01-22 16:00:55','49752da5-316e-44ab-bb64-728684d46d53'),
	(209,50,82,62,0,2,'2019-01-22 16:00:55','2019-01-22 16:00:55','89133157-0b13-4e97-becb-1df4a41255e2'),
	(210,50,82,63,0,3,'2019-01-22 16:00:55','2019-01-22 16:00:55','4a2887b6-45b9-44a2-a8e6-2f0a8bf49015'),
	(211,50,82,64,0,4,'2019-01-22 16:00:55','2019-01-22 16:00:55','6c082ddc-3192-4891-ab00-9ece099aa20d'),
	(212,50,82,49,0,5,'2019-01-22 16:00:55','2019-01-22 16:00:55','f59a28a9-7a44-4ed4-9b24-b22fa01dd272'),
	(213,50,83,1,0,1,'2019-01-22 16:00:55','2019-01-22 16:00:55','36f56241-943e-49ac-a99c-5c05bd4371c0'),
	(214,50,83,2,0,2,'2019-01-22 16:00:55','2019-01-22 16:00:55','17be0ac6-9db9-4120-bb7a-ed8930306d90'),
	(215,50,83,4,0,3,'2019-01-22 16:00:55','2019-01-22 16:00:55','35116981-f1f6-471a-b916-9081320582cb'),
	(216,50,83,5,0,4,'2019-01-22 16:00:55','2019-01-22 16:00:55','959f8524-191e-4ff6-9964-9c6ba7e302a0'),
	(217,51,84,49,0,1,'2019-01-22 16:00:55','2019-01-22 16:00:55','c0116989-7e54-40a2-9888-7cc39772c2f3'),
	(218,52,85,51,0,1,'2019-01-22 16:00:56','2019-01-22 16:00:56','4918cd9b-e100-4235-b4a3-628c7736335f'),
	(219,52,85,49,0,2,'2019-01-22 16:00:56','2019-01-22 16:00:56','8a287d70-388e-476f-a5c2-3f15588546d8'),
	(220,52,85,2,0,3,'2019-01-22 16:00:56','2019-01-22 16:00:56','3c9215a3-3397-496c-90f2-d272d351b3c1'),
	(221,53,86,52,0,1,'2019-01-22 16:00:56','2019-01-22 16:00:56','5f9f1797-40ad-451d-a116-26ac3c28c4c1'),
	(222,54,87,41,0,1,'2019-01-22 16:00:56','2019-01-22 16:00:56','58d5a8a5-ea64-49c3-b8cc-b42406e2da1e'),
	(223,54,88,1,0,1,'2019-01-22 16:00:56','2019-01-22 16:00:56','e7952558-b959-46ce-a127-86e483473790'),
	(224,54,88,2,0,2,'2019-01-22 16:00:56','2019-01-22 16:00:56','d71fc2a9-d9dd-43d7-a7d0-92e65da7a0c4'),
	(225,54,88,4,0,3,'2019-01-22 16:00:56','2019-01-22 16:00:56','29759374-eb90-430b-b0f3-440839ede9b3'),
	(226,54,88,5,0,4,'2019-01-22 16:00:56','2019-01-22 16:00:56','89818224-d5c5-4560-94ac-56509563f7a5'),
	(227,55,89,37,0,1,'2019-01-22 16:00:56','2019-01-22 16:00:56','be0a59eb-3c85-4408-be55-b81e5a133c40'),
	(228,55,90,1,0,1,'2019-01-22 16:00:56','2019-01-22 16:00:56','7eea7337-db37-41da-bcbb-6a638f61ce98'),
	(229,55,90,2,0,2,'2019-01-22 16:00:56','2019-01-22 16:00:56','c9767f6d-bd39-488a-a224-ff19a613c6c7'),
	(230,55,90,4,0,3,'2019-01-22 16:00:56','2019-01-22 16:00:56','c62a3681-3bab-428d-b923-5dcadd62c374'),
	(231,55,90,5,0,4,'2019-01-22 16:00:56','2019-01-22 16:00:56','8ba4a8ff-d3c3-42ef-91ec-ea59d118151c'),
	(232,56,91,47,0,1,'2019-01-22 16:00:56','2019-01-22 16:00:56','fc633026-0169-4d98-9164-a09bec8706c5'),
	(233,57,92,47,0,1,'2019-01-22 16:00:56','2019-01-22 16:00:56','c9e8babe-932b-475b-9673-5a1cc9123fe4'),
	(266,58,108,38,0,1,'2019-01-22 16:06:18','2019-01-22 16:06:18','b837d23b-4571-4d62-9166-a34ce3fa0e12'),
	(267,58,109,1,0,1,'2019-01-22 16:06:18','2019-01-22 16:06:18','8c8903e6-cd09-40db-8ce8-1467130926db'),
	(268,58,109,2,0,2,'2019-01-22 16:06:18','2019-01-22 16:06:18','75fc3c81-0e43-45e8-bdf8-8356cea6e0a5'),
	(269,58,109,4,0,3,'2019-01-22 16:06:18','2019-01-22 16:06:18','e51b2e76-1906-41fe-a502-fe4ba31893d4'),
	(270,58,109,5,0,4,'2019-01-22 16:06:18','2019-01-22 16:06:18','e50d414a-acc5-404b-bb71-9c3bfadb68cf'),
	(271,59,110,51,0,1,'2019-01-22 16:06:18','2019-01-22 16:06:18','e74d9aa8-d917-4fb3-b7a3-15f8fe720961'),
	(272,59,110,62,0,2,'2019-01-22 16:06:18','2019-01-22 16:06:18','26fa6427-e3da-4c2d-9379-4182e49f7ff0'),
	(273,59,110,63,0,3,'2019-01-22 16:06:18','2019-01-22 16:06:18','82cf2b65-5134-460a-921f-9bc86f030351'),
	(274,59,110,64,0,4,'2019-01-22 16:06:18','2019-01-22 16:06:18','818a2ee7-6f5b-46a8-ae43-cb3015df26e4'),
	(275,59,110,49,0,5,'2019-01-22 16:06:18','2019-01-22 16:06:18','175acd13-838e-4da2-ae5c-672f3dbbf91b'),
	(276,59,111,1,0,1,'2019-01-22 16:06:18','2019-01-22 16:06:18','136ba6cb-0217-4544-8290-0106c4cb97b8'),
	(277,59,111,2,0,2,'2019-01-22 16:06:18','2019-01-22 16:06:18','354dc6ee-3d7e-4680-ace6-b549e94b2c89'),
	(278,59,111,4,0,3,'2019-01-22 16:06:18','2019-01-22 16:06:18','875986e2-21da-493f-abf7-856e3b4e87df'),
	(279,59,111,5,0,4,'2019-01-22 16:06:18','2019-01-22 16:06:18','9fa38e8f-7bee-464f-9290-d0f9e91255a7'),
	(280,60,112,49,0,1,'2019-01-22 16:06:19','2019-01-22 16:06:19','9ccf5270-1f3c-4558-987f-f13c0a9a1286'),
	(281,61,113,51,0,1,'2019-01-22 16:06:19','2019-01-22 16:06:19','38d81d04-369d-4d0e-997b-25abb7d59a92'),
	(282,61,113,49,0,2,'2019-01-22 16:06:19','2019-01-22 16:06:19','69bb42ef-7668-4caf-980b-0d8ecb877e4f'),
	(283,61,113,2,0,3,'2019-01-22 16:06:19','2019-01-22 16:06:19','be63647a-8291-476a-8f52-dd6d9f9aa00a'),
	(284,62,114,52,0,1,'2019-01-22 16:06:19','2019-01-22 16:06:19','db85467a-266e-4cfd-8380-57e5c2328e9b'),
	(285,63,115,41,0,1,'2019-01-22 16:06:19','2019-01-22 16:06:19','90fae44a-6f0f-451b-ac2c-2a65e0a449b7'),
	(286,63,116,1,0,1,'2019-01-22 16:06:19','2019-01-22 16:06:19','34f03315-1af0-47d1-8e66-209ed7cf8c56'),
	(287,63,116,2,0,2,'2019-01-22 16:06:19','2019-01-22 16:06:19','42efff1f-3990-4456-95af-98f04d2b10f8'),
	(288,63,116,4,0,3,'2019-01-22 16:06:19','2019-01-22 16:06:19','d6841802-f1cd-4ce5-bd2d-a1cda684e2dc'),
	(289,63,116,5,0,4,'2019-01-22 16:06:19','2019-01-22 16:06:19','16c81da6-531f-4df0-81d7-e65792029cd8'),
	(290,64,117,37,0,1,'2019-01-22 16:06:19','2019-01-22 16:06:19','e8096ab9-b0b8-4b76-964c-cf5606d14fc3'),
	(291,64,118,1,0,1,'2019-01-22 16:06:19','2019-01-22 16:06:19','eb4580f3-abeb-4c7e-a92e-08fc3684f06f'),
	(292,64,118,2,0,2,'2019-01-22 16:06:19','2019-01-22 16:06:19','d1a0926f-dd7e-4dab-834d-fb5473e802cf'),
	(293,64,118,4,0,3,'2019-01-22 16:06:19','2019-01-22 16:06:19','4bac11f2-0baf-4c3c-958a-76fdbe489f85'),
	(294,64,118,5,0,4,'2019-01-22 16:06:19','2019-01-22 16:06:19','f52189e1-1add-46f2-b6aa-8f193aaa2b3d'),
	(295,65,119,47,0,1,'2019-01-22 16:06:19','2019-01-22 16:06:19','c26c883c-734b-4502-a72d-4b7147ce2bd2'),
	(296,66,120,47,0,1,'2019-01-22 16:06:19','2019-01-22 16:06:19','c3553290-1203-4f1b-b259-406cf0a0d5e8'),
	(297,67,121,37,0,1,'2019-01-22 16:06:19','2019-01-22 16:06:19','e254c612-28d1-44ba-bf83-a465255d4fc7'),
	(298,69,123,66,0,1,'2019-01-22 16:06:20','2019-01-22 16:06:20','bdb24050-75cc-47ab-a954-f889b5f509b3'),
	(299,69,123,67,0,2,'2019-01-22 16:06:20','2019-01-22 16:06:20','13b6d571-c077-4834-a007-dacca3b29cc8'),
	(300,69,123,68,0,3,'2019-01-22 16:06:20','2019-01-22 16:06:20','0fcc93df-1ac7-456d-8c80-97435536df0f'),
	(338,70,140,38,0,1,'2019-01-22 16:09:22','2019-01-22 16:09:22','d8ac281a-f3b0-4beb-83ce-8127166750ad'),
	(339,70,141,1,0,1,'2019-01-22 16:09:22','2019-01-22 16:09:22','fbff9371-b3ad-46a0-b31a-dc84e09db0c5'),
	(340,70,141,2,0,2,'2019-01-22 16:09:22','2019-01-22 16:09:22','6984c636-8e21-4fb4-bc82-490c7c89da5e'),
	(341,70,141,4,0,3,'2019-01-22 16:09:22','2019-01-22 16:09:22','9f661fcd-458f-45d5-9937-d43bf0bd9a5e'),
	(342,70,141,5,0,4,'2019-01-22 16:09:22','2019-01-22 16:09:22','8ca2ebac-3895-4769-8046-3440015b8d60'),
	(343,71,142,51,0,1,'2019-01-22 16:09:23','2019-01-22 16:09:23','68fb25bd-5de8-4eab-b47f-85c7c0937d0f'),
	(344,71,142,62,0,2,'2019-01-22 16:09:23','2019-01-22 16:09:23','9397a799-59e9-4d97-9450-dea6b12ba31f'),
	(345,71,142,63,0,3,'2019-01-22 16:09:23','2019-01-22 16:09:23','ba8c6c88-fb70-4cd7-86d5-e10bb72fc6c7'),
	(346,71,142,64,0,4,'2019-01-22 16:09:23','2019-01-22 16:09:23','dfd1422f-c053-41f1-89ae-263b3115bae4'),
	(347,71,142,49,0,5,'2019-01-22 16:09:23','2019-01-22 16:09:23','ca988d6e-7f5c-46ab-97d3-40e9611e8095'),
	(348,71,143,1,0,1,'2019-01-22 16:09:23','2019-01-22 16:09:23','5dfde21a-0762-4dbe-b1a7-504b92b4d675'),
	(349,71,143,2,0,2,'2019-01-22 16:09:23','2019-01-22 16:09:23','ceacbd63-8f3b-4514-8acb-810010a32a20'),
	(350,71,143,4,0,3,'2019-01-22 16:09:23','2019-01-22 16:09:23','ec4c9455-9b5e-4fb9-bfab-488685382c72'),
	(351,71,143,5,0,4,'2019-01-22 16:09:23','2019-01-22 16:09:23','7197cdcb-1259-4736-95d7-b5f259deff06'),
	(352,72,144,49,0,1,'2019-01-22 16:09:23','2019-01-22 16:09:23','532d28f4-b07e-4869-b7d0-f5fc0a350acb'),
	(353,73,145,51,0,1,'2019-01-22 16:09:23','2019-01-22 16:09:23','95659038-126d-4ab0-8d6d-192f556e6749'),
	(354,73,145,49,0,2,'2019-01-22 16:09:23','2019-01-22 16:09:23','fb9391d4-817f-4266-aca5-6b0b6f4f0585'),
	(355,73,145,2,0,3,'2019-01-22 16:09:23','2019-01-22 16:09:23','82f40040-5687-4990-8965-4c3fdae19e4d'),
	(356,74,146,52,0,1,'2019-01-22 16:09:23','2019-01-22 16:09:23','3a4ee1d9-f33d-4f14-8308-b8da4d830afe'),
	(357,75,147,41,0,1,'2019-01-22 16:09:23','2019-01-22 16:09:23','44bcaeec-dc62-433c-987a-edaef94b4f83'),
	(358,75,148,1,0,1,'2019-01-22 16:09:23','2019-01-22 16:09:23','54fb3ae6-4ac4-4d53-b730-d3fe948662c4'),
	(359,75,148,2,0,2,'2019-01-22 16:09:23','2019-01-22 16:09:23','a7c67912-7f18-427d-baad-32f62fa6d03b'),
	(360,75,148,4,0,3,'2019-01-22 16:09:23','2019-01-22 16:09:23','8cf6288f-234f-4237-900d-a434e49b8c4d'),
	(361,75,148,5,0,4,'2019-01-22 16:09:23','2019-01-22 16:09:23','e75d683d-21f7-47bf-854c-27074bb37bce'),
	(362,76,149,37,0,1,'2019-01-22 16:09:23','2019-01-22 16:09:23','d259711d-768d-4125-98c2-f002cc093af6'),
	(363,76,150,1,0,1,'2019-01-22 16:09:23','2019-01-22 16:09:23','e078011b-8e8b-4ced-9719-df1dfd2f0ec8'),
	(364,76,150,2,0,2,'2019-01-22 16:09:23','2019-01-22 16:09:23','a770a090-2d3e-4f89-bdd3-882831e03235'),
	(365,76,150,4,0,3,'2019-01-22 16:09:23','2019-01-22 16:09:23','f7c30ab0-4f8e-4477-bc92-cf6fde4ea77d'),
	(366,76,150,5,0,4,'2019-01-22 16:09:23','2019-01-22 16:09:23','ca40469f-9557-49b8-a07f-72a43a989af0'),
	(367,77,151,47,0,1,'2019-01-22 16:09:23','2019-01-22 16:09:23','7895939c-fbdd-4e95-a1fa-151cefb42b90'),
	(368,78,152,47,0,1,'2019-01-22 16:09:23','2019-01-22 16:09:23','a1d634a0-7e58-4cdf-b865-360c5efdf2fd'),
	(369,79,153,37,0,1,'2019-01-22 16:09:23','2019-01-22 16:09:23','f85ebabd-46c6-4679-9075-9d5e48180748'),
	(370,80,154,65,0,1,'2019-01-22 16:09:23','2019-01-22 16:09:23','666fe3da-3b17-4097-92b9-d21cd0c38867'),
	(371,80,155,1,0,1,'2019-01-22 16:09:23','2019-01-22 16:09:23','101fdc1e-b95f-4414-9b5d-af9e2f95a021'),
	(372,80,155,2,0,2,'2019-01-22 16:09:23','2019-01-22 16:09:23','41b129d1-4796-4127-8a2f-7454667ba3cf'),
	(373,80,155,4,0,3,'2019-01-22 16:09:23','2019-01-22 16:09:23','59b6e407-a2af-47ff-b288-36f9e786d4df'),
	(374,80,155,5,0,4,'2019-01-22 16:09:23','2019-01-22 16:09:23','28dcd289-b39c-45ff-960f-467ef5fe6a60'),
	(376,82,157,57,0,1,'2019-01-22 16:10:08','2019-01-22 16:10:08','89963459-a81d-4ab0-ad31-500ff93f3f18'),
	(377,83,158,61,0,1,'2019-01-22 16:11:58','2019-01-22 16:11:58','dea51ff0-956d-4b3f-9707-3d02af1200ce'),
	(378,83,159,54,0,1,'2019-01-22 16:11:58','2019-01-22 16:11:58','d29215cf-0056-4862-9441-dfde52a12e54'),
	(379,83,159,55,0,2,'2019-01-22 16:11:58','2019-01-22 16:11:58','7a6d291b-f9ac-4b15-89d1-a4f6427ff809'),
	(388,86,163,72,0,1,'2019-01-30 21:44:19','2019-01-30 21:44:19','c3808935-3d77-43c3-b709-ea6d25019494'),
	(389,86,163,73,0,2,'2019-01-30 21:44:19','2019-01-30 21:44:19','5660e093-b3cc-4585-aea2-5cf30620bd58'),
	(390,81,164,71,0,1,'2019-01-30 21:44:19','2019-01-30 21:44:19','ee523543-1bba-42a7-b367-a5981b07e73e'),
	(391,11,165,42,0,1,'2019-01-30 23:31:54','2019-01-30 23:31:54','efb65db4-dab1-4463-939c-12feb0081b89'),
	(392,11,165,43,0,2,'2019-01-30 23:31:54','2019-01-30 23:31:54','6044d795-d630-4129-8824-52e5ae3c2077'),
	(393,11,165,45,0,3,'2019-01-30 23:31:54','2019-01-30 23:31:54','4d85dec7-1800-468f-b7cd-6878e7a0c818'),
	(394,85,166,51,0,1,'2019-01-30 23:37:21','2019-01-30 23:37:21','13538f20-9f87-46fc-bc52-1530de245cc8'),
	(395,85,166,69,0,2,'2019-01-30 23:37:21','2019-01-30 23:37:21','81d4881b-b7f9-4001-9e24-54155f816544'),
	(396,85,166,70,0,3,'2019-01-30 23:37:21','2019-01-30 23:37:21','139e688a-6b68-47de-a6b3-14c00c4cf21f'),
	(397,85,166,74,0,4,'2019-01-30 23:37:21','2019-01-30 23:37:21','3b0a4497-44eb-4f7d-89e0-943fa41d90f1'),
	(401,87,168,38,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','903a026d-187a-4d7b-8036-7f8ac5719be3'),
	(402,87,169,1,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','e3332f90-82e8-4fa9-b1dd-54e73e3ca8c2'),
	(403,87,169,2,0,2,'2019-01-30 23:39:28','2019-01-30 23:39:28','cdb3b69a-6bc3-444b-8007-8968e45fe7dc'),
	(404,87,169,4,0,3,'2019-01-30 23:39:28','2019-01-30 23:39:28','d05f17a7-5990-4c96-b1f2-1d723f2941c0'),
	(405,87,169,5,0,4,'2019-01-30 23:39:28','2019-01-30 23:39:28','9caa12b8-e10b-47f0-97f0-eed503326bd8'),
	(406,88,170,51,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','8453242a-4f0e-48b4-b0e6-adf8bdf22573'),
	(407,88,170,62,0,2,'2019-01-30 23:39:28','2019-01-30 23:39:28','1d4e77f8-049b-4864-bb3d-e700ac6410f3'),
	(408,88,170,63,0,3,'2019-01-30 23:39:28','2019-01-30 23:39:28','9575a243-8429-4db2-9ac7-e44829e8994c'),
	(409,88,170,64,0,4,'2019-01-30 23:39:28','2019-01-30 23:39:28','9bae5528-a4aa-4a5a-b99f-6ca906e2a503'),
	(410,88,170,49,0,5,'2019-01-30 23:39:28','2019-01-30 23:39:28','cfbfd719-67b4-4976-b2b1-d9d470ee6e4c'),
	(411,88,171,1,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','869c28cd-809a-4c6a-ba72-d6a10b6c5eac'),
	(412,88,171,2,0,2,'2019-01-30 23:39:28','2019-01-30 23:39:28','57994f0c-1f46-45c2-b449-245a80fa7aa4'),
	(413,88,171,4,0,3,'2019-01-30 23:39:28','2019-01-30 23:39:28','2f2aac56-e74d-4bfc-aab9-5c177181fb0a'),
	(414,88,171,5,0,4,'2019-01-30 23:39:28','2019-01-30 23:39:28','8f90d5d3-3bb6-4b52-8349-210db1f841e6'),
	(415,89,172,49,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','21c7afac-3727-42da-b29d-1f125cbdedde'),
	(416,90,173,51,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','76a10b25-a609-4ca6-a59a-16b9f29d198b'),
	(417,90,173,49,0,2,'2019-01-30 23:39:28','2019-01-30 23:39:28','dfb41fc1-14ae-44e7-8a11-c87f2ab09a64'),
	(418,90,173,2,0,3,'2019-01-30 23:39:28','2019-01-30 23:39:28','71b1a07c-e727-4417-a2d8-20ff9e1ade36'),
	(419,91,174,52,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','dafdac5c-d413-49d9-9b52-8b3c8a06ad3a'),
	(420,92,175,41,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','d60582c5-6087-4e00-837e-736e815d93f3'),
	(421,92,176,1,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','3ac8a6c6-eda4-49c1-8927-e495f82eb7dc'),
	(422,92,176,2,0,2,'2019-01-30 23:39:28','2019-01-30 23:39:28','d5fc339e-3004-4e0e-bb9b-dae0893f5f3c'),
	(423,92,176,4,0,3,'2019-01-30 23:39:28','2019-01-30 23:39:28','55fb0ed8-6885-41d1-afc6-c2d6e984373c'),
	(424,92,176,5,0,4,'2019-01-30 23:39:28','2019-01-30 23:39:28','e9b35ca3-7657-4c10-a036-c9e03d1bacbe'),
	(425,93,177,37,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','4f07b15a-015e-43ac-af53-59096188c587'),
	(426,93,178,1,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','496a17a7-d34b-4249-8fb3-b2031e9c416d'),
	(427,93,178,2,0,2,'2019-01-30 23:39:28','2019-01-30 23:39:28','4b10e225-3edb-4270-ab0d-edbb1c0e93bf'),
	(428,93,178,4,0,3,'2019-01-30 23:39:28','2019-01-30 23:39:28','5182721f-0822-4153-ad7c-c92826248ba4'),
	(429,93,178,5,0,4,'2019-01-30 23:39:28','2019-01-30 23:39:28','1364c11d-96bf-4bac-b9ed-6bbcc766be0d'),
	(430,94,179,74,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','cc36a4ad-6dbd-458b-84e2-26b76b33b483'),
	(431,95,180,47,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','881196af-9298-430c-ab6e-446070cdb26e'),
	(432,96,181,37,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','08732a7e-3893-45ae-980f-8c690b2b6f0d'),
	(433,97,182,65,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','c820577a-f55b-4b23-a5fe-cddf5d424d73'),
	(434,97,183,1,0,1,'2019-01-30 23:39:28','2019-01-30 23:39:28','e4059c5b-295d-4297-b41d-1457dbdc5c1e'),
	(435,97,183,2,0,2,'2019-01-30 23:39:28','2019-01-30 23:39:28','d28bc871-ab95-43dd-a744-0ec3ee50645f'),
	(436,97,183,4,0,3,'2019-01-30 23:39:28','2019-01-30 23:39:28','67cdaf17-5423-40f7-acd8-9f05b1b8cfa6'),
	(437,97,183,5,0,4,'2019-01-30 23:39:28','2019-01-30 23:39:28','2b329718-7f93-4214-8ff9-566e4e175175'),
	(439,10,185,39,0,1,'2019-01-31 16:04:10','2019-01-31 16:04:10','9a75683b-3e46-4e26-b7a4-4bd02d322e74'),
	(440,10,185,40,0,2,'2019-01-31 16:04:10','2019-01-31 16:04:10','3741c86f-1477-4e0a-a211-2bf8368b0abf'),
	(441,99,186,51,0,1,'2019-01-31 16:40:23','2019-01-31 16:40:23','f873537b-44ad-4377-8a70-baaa7e741047'),
	(442,99,186,49,0,2,'2019-01-31 16:40:23','2019-01-31 16:40:23','74afc81c-212e-4743-81e3-6c17e9d5c173'),
	(443,99,186,2,0,3,'2019-01-31 16:40:23','2019-01-31 16:40:23','f97189f9-9717-4583-960a-817ddad5479f'),
	(444,100,187,38,0,1,'2019-01-31 16:40:23','2019-01-31 16:40:23','1f995e51-1709-4c85-897d-e4cf335ae510'),
	(445,100,188,1,0,1,'2019-01-31 16:40:23','2019-01-31 16:40:23','119cc6b8-7aac-4f45-8217-2661fe57e393'),
	(446,100,188,2,0,2,'2019-01-31 16:40:23','2019-01-31 16:40:23','47ecffbb-5bce-44d5-a317-1d6c9448b3b7'),
	(447,100,188,4,0,3,'2019-01-31 16:40:23','2019-01-31 16:40:23','4d2b88c9-2d4a-48b3-a05b-379a55449971'),
	(448,100,188,5,0,4,'2019-01-31 16:40:23','2019-01-31 16:40:23','168b52f1-f3dc-4f96-b30c-de2b9570b594'),
	(449,101,189,51,0,1,'2019-01-31 16:40:23','2019-01-31 16:40:23','aa48f6fd-66a8-48ab-9ad0-6af8aae892e4'),
	(450,101,189,62,0,2,'2019-01-31 16:40:23','2019-01-31 16:40:23','24589b17-e3f0-47b5-a9bc-c7b2430dff2b'),
	(451,101,189,63,0,3,'2019-01-31 16:40:23','2019-01-31 16:40:23','30e4ed1e-e889-4888-b243-a4ec1fe0dd22'),
	(452,101,189,64,0,4,'2019-01-31 16:40:23','2019-01-31 16:40:23','631191ce-74ca-49e9-9a45-b73abd77b691'),
	(453,101,189,49,0,5,'2019-01-31 16:40:23','2019-01-31 16:40:23','48cf98b0-7477-45c1-99c6-7545c6b6a1db'),
	(454,101,190,1,0,1,'2019-01-31 16:40:23','2019-01-31 16:40:23','7300d373-284a-4133-ad79-41864c33031c'),
	(455,101,190,2,0,2,'2019-01-31 16:40:23','2019-01-31 16:40:23','5218db26-51bd-4549-b64e-b156500d92cc'),
	(456,101,190,4,0,3,'2019-01-31 16:40:23','2019-01-31 16:40:23','def54660-d2bd-49ba-ba82-8ee24f488cb1'),
	(457,101,190,5,0,4,'2019-01-31 16:40:23','2019-01-31 16:40:23','5c24d63c-bfff-41ba-ba4f-60f8eb9b4931'),
	(458,102,191,49,0,1,'2019-01-31 16:40:23','2019-01-31 16:40:23','271fbc6f-f801-47f9-8723-a04364ab3797'),
	(459,103,192,52,0,1,'2019-01-31 16:40:23','2019-01-31 16:40:23','233d4fe8-8ac9-440d-ac45-00b4b0158f26'),
	(460,104,193,41,0,1,'2019-01-31 16:40:23','2019-01-31 16:40:23','cc6dffa7-5d9c-4b7b-8ce8-43b3df934992'),
	(461,104,194,1,0,1,'2019-01-31 16:40:23','2019-01-31 16:40:23','ab1abe60-0309-481f-ad31-97c3a9c9e625'),
	(462,104,194,2,0,2,'2019-01-31 16:40:23','2019-01-31 16:40:23','849e2540-6d12-4797-9b86-75083aea39d3'),
	(463,104,194,4,0,3,'2019-01-31 16:40:23','2019-01-31 16:40:23','ff0826d5-acf9-4974-9cdd-158da591999d'),
	(464,104,194,5,0,4,'2019-01-31 16:40:23','2019-01-31 16:40:23','c5f488bf-705c-4ec2-9163-5814d215cc6f'),
	(465,105,195,37,0,1,'2019-01-31 16:40:23','2019-01-31 16:40:23','b045dcb7-e027-4b20-9b5a-0ef0bf766dbc'),
	(466,105,196,1,0,1,'2019-01-31 16:40:23','2019-01-31 16:40:23','c45ffdea-52b7-4cda-9428-e1ed2db1500f'),
	(467,105,196,2,0,2,'2019-01-31 16:40:23','2019-01-31 16:40:23','b347bdcb-2e0a-4e18-9a1e-2c83e5904920'),
	(468,105,196,4,0,3,'2019-01-31 16:40:23','2019-01-31 16:40:23','f38ba469-460d-4405-ae54-d7f63a2a8cfb'),
	(469,105,196,5,0,4,'2019-01-31 16:40:23','2019-01-31 16:40:23','d3f61510-2307-4830-9396-1839b5c4d01c'),
	(470,106,197,74,0,1,'2019-01-31 16:40:23','2019-01-31 16:40:23','75af52ce-83ae-4b1e-a444-a6aed1a88c27'),
	(471,108,199,37,0,1,'2019-01-31 16:40:23','2019-01-31 16:40:23','88c4b243-7a6f-43f3-b9db-85a27c2e4df9'),
	(472,109,200,65,0,1,'2019-01-31 16:40:23','2019-01-31 16:40:23','7fb8b1ef-1bc6-44aa-924a-c81e57508c21'),
	(473,109,201,1,0,1,'2019-01-31 16:40:23','2019-01-31 16:40:23','641f843e-edfa-444f-9dba-12cd6d1fb6fd'),
	(474,109,201,2,0,2,'2019-01-31 16:40:23','2019-01-31 16:40:23','73f899c1-aecc-489a-aafc-0a66761b754d'),
	(475,109,201,4,0,3,'2019-01-31 16:40:23','2019-01-31 16:40:23','a7202b0b-9e73-4489-95da-9067bde38234'),
	(476,109,201,5,0,4,'2019-01-31 16:40:23','2019-01-31 16:40:23','91edcbe0-7fa4-4d67-9009-723dc78248f6'),
	(477,84,202,75,0,1,'2019-01-31 16:43:11','2019-01-31 16:43:11','a3a81996-0a1f-4120-9de6-2c562ec9dd56'),
	(478,84,202,46,0,2,'2019-01-31 16:43:11','2019-01-31 16:43:11','e1bfec8a-44b3-4409-b2f3-c6a738bbca13'),
	(479,84,202,47,0,3,'2019-01-31 16:43:11','2019-01-31 16:43:11','a76b1d09-2010-4e07-9980-8fccd861a7e6'),
	(480,84,202,61,0,4,'2019-01-31 16:43:11','2019-01-31 16:43:11','3ecce916-9b90-488c-8a0e-bbfdd315fd51'),
	(481,110,203,61,0,1,'2019-01-31 20:56:19','2019-01-31 20:56:19','87af819c-1055-4703-bbf5-ac7bbb4c6068'),
	(482,98,204,53,0,1,'2019-02-05 18:46:06','2019-02-05 18:46:06','78fdd4d1-bf04-4678-a019-386632e72fed'),
	(483,98,204,48,0,2,'2019-02-05 18:46:06','2019-02-05 18:46:06','df4ac47d-66d7-4dfd-9b99-153018b4984a'),
	(484,111,205,51,0,1,'2019-02-07 20:46:38','2019-02-07 20:46:38','1bf4c477-2100-4aaa-bd9e-0cccf766220b'),
	(485,111,205,49,0,2,'2019-02-07 20:46:38','2019-02-07 20:46:38','a602ba65-8cf8-4694-87e4-ece25b01e67f'),
	(486,111,205,2,0,3,'2019-02-07 20:46:38','2019-02-07 20:46:38','729acced-75db-47dd-bcf6-30a4d33cc896'),
	(487,112,206,38,0,1,'2019-02-07 20:46:38','2019-02-07 20:46:38','d17944d7-ea5a-4b67-a9ba-3df6d40d060e'),
	(488,112,207,1,0,1,'2019-02-07 20:46:38','2019-02-07 20:46:38','c6ddf664-9db0-47e1-b13c-29d1c1efa66c'),
	(489,112,207,2,0,2,'2019-02-07 20:46:38','2019-02-07 20:46:38','2d710adb-7acc-4186-96f8-4fc5e9e80591'),
	(490,112,207,4,0,3,'2019-02-07 20:46:38','2019-02-07 20:46:38','0f2323cb-c30c-4ea0-bffe-4a9952d6ebcd'),
	(491,112,207,5,0,4,'2019-02-07 20:46:38','2019-02-07 20:46:38','67526efa-a5d7-4906-9a1b-ae2bdc8d1f2b'),
	(492,113,208,62,0,1,'2019-02-07 20:46:38','2019-02-07 20:46:38','5c49fd6d-c5e4-4ab4-94d9-2c7d36be2f1d'),
	(493,113,208,63,0,2,'2019-02-07 20:46:38','2019-02-07 20:46:38','b1e113da-d5fc-4d6a-8394-11454dae4247'),
	(494,113,208,64,0,3,'2019-02-07 20:46:38','2019-02-07 20:46:38','7ee09074-08ed-4173-af66-d3e135c65bfd'),
	(495,113,208,49,0,4,'2019-02-07 20:46:38','2019-02-07 20:46:38','b5fa14a4-daa2-4a1e-822b-85acfe03025e'),
	(496,113,209,1,0,1,'2019-02-07 20:46:38','2019-02-07 20:46:38','3a904a9e-847d-42d0-9326-22406e1f52ca'),
	(497,113,209,2,0,2,'2019-02-07 20:46:38','2019-02-07 20:46:38','0c5bda90-a549-470c-b80c-80d9e3d9f293'),
	(498,113,209,4,0,3,'2019-02-07 20:46:38','2019-02-07 20:46:38','a3dc50af-9e80-4460-a367-3ce2937ebd53'),
	(499,113,209,5,0,4,'2019-02-07 20:46:38','2019-02-07 20:46:38','4dd5556d-3444-4dc3-a905-e31836635d6c'),
	(500,114,210,49,0,1,'2019-02-07 20:46:38','2019-02-07 20:46:38','4515b93b-c986-4e3c-a0f4-f5d6f18f312e'),
	(501,115,211,52,0,1,'2019-02-07 20:46:38','2019-02-07 20:46:38','4982677d-b0c6-48b0-9b10-fc0c07a9cfb7'),
	(502,116,212,41,0,1,'2019-02-07 20:46:38','2019-02-07 20:46:38','3def7711-3b11-451b-a3d7-2b1e4ea9331b'),
	(503,116,213,1,0,1,'2019-02-07 20:46:38','2019-02-07 20:46:38','147a4dd6-08d5-4ab5-93bf-bde3c543b8df'),
	(504,116,213,2,0,2,'2019-02-07 20:46:38','2019-02-07 20:46:38','72c1c516-097c-45a8-a989-13e74a1c82cc'),
	(505,116,213,4,0,3,'2019-02-07 20:46:38','2019-02-07 20:46:38','0e649215-a23d-4f08-8ced-bc69e071d731'),
	(506,116,213,5,0,4,'2019-02-07 20:46:38','2019-02-07 20:46:38','6ca956b7-63a0-4caa-98a1-2e0f231d7246'),
	(507,117,214,37,0,1,'2019-02-07 20:46:38','2019-02-07 20:46:38','8a076583-27d7-45f0-9c11-6cdeb19895a5'),
	(508,117,215,1,0,1,'2019-02-07 20:46:38','2019-02-07 20:46:38','97c76af2-4b24-4028-9ea9-a30f6d04a863'),
	(509,117,215,2,0,2,'2019-02-07 20:46:38','2019-02-07 20:46:38','0af54297-86c1-4eda-9ec0-b138d02c9790'),
	(510,117,215,4,0,3,'2019-02-07 20:46:38','2019-02-07 20:46:38','51392d73-3f1a-4965-9bc3-25c89ee2d0b1'),
	(511,117,215,5,0,4,'2019-02-07 20:46:38','2019-02-07 20:46:38','cd550361-67a0-4397-9e46-5b572a8644d7'),
	(512,118,216,74,0,1,'2019-02-07 20:46:38','2019-02-07 20:46:38','c1c9ead5-d122-4478-94a5-82ceda7ed264'),
	(513,120,218,37,0,1,'2019-02-07 20:46:38','2019-02-07 20:46:38','dd1f549d-7d31-4da2-8a0d-9682be4a16c0'),
	(514,121,219,65,0,1,'2019-02-07 20:46:38','2019-02-07 20:46:38','16b9cb11-fdae-4c69-b2de-cd94bd4cd5f8'),
	(515,121,220,1,0,1,'2019-02-07 20:46:38','2019-02-07 20:46:38','606c54db-0078-48d0-9b33-ac60db56a602'),
	(516,121,220,2,0,2,'2019-02-07 20:46:38','2019-02-07 20:46:38','fad48180-58d2-4e22-a02a-c01553218bc7'),
	(517,121,220,4,0,3,'2019-02-07 20:46:38','2019-02-07 20:46:38','67de457f-e750-417b-961f-92224ec2f7a5'),
	(518,121,220,5,0,4,'2019-02-07 20:46:38','2019-02-07 20:46:38','ab6d3d85-7f9e-445b-a329-8dcf734678ad'),
	(519,122,221,51,0,1,'2019-02-07 20:47:25','2019-02-07 20:47:25','6c47b14e-7373-4703-88ac-4384c651f392'),
	(520,122,221,49,0,2,'2019-02-07 20:47:25','2019-02-07 20:47:25','c7f477f9-f834-4ff7-975f-0662ad0c7ada'),
	(521,122,221,2,0,3,'2019-02-07 20:47:25','2019-02-07 20:47:25','00360613-a030-476a-9579-6b3941954b68'),
	(522,123,222,38,0,1,'2019-02-07 20:47:25','2019-02-07 20:47:25','339c33f2-27d4-4a2c-92eb-6a052c95964d'),
	(523,123,223,1,0,1,'2019-02-07 20:47:25','2019-02-07 20:47:25','54c6a981-0855-4775-a28e-5ef70be96be5'),
	(524,123,223,2,0,2,'2019-02-07 20:47:25','2019-02-07 20:47:25','2d2a435f-66a6-4091-be81-b84d263395af'),
	(525,123,223,4,0,3,'2019-02-07 20:47:25','2019-02-07 20:47:25','9e6ce870-1472-44f4-a99e-3e9bcd87b01d'),
	(526,123,223,5,0,4,'2019-02-07 20:47:25','2019-02-07 20:47:25','12a07100-6b9b-46bc-b065-0c0a5bc8f0ef'),
	(527,124,224,51,0,1,'2019-02-07 20:47:25','2019-02-07 20:47:25','d1566419-7ccf-4936-8808-9a7468601a0b'),
	(528,124,224,62,0,2,'2019-02-07 20:47:25','2019-02-07 20:47:25','8165d8bc-e30c-4cf3-ac30-4dc341aa97aa'),
	(529,124,224,63,0,3,'2019-02-07 20:47:25','2019-02-07 20:47:25','dd220863-d195-4e1e-9323-616a5bf6bf1c'),
	(530,124,224,64,0,4,'2019-02-07 20:47:25','2019-02-07 20:47:25','b0023bc6-cf9c-416d-836e-6249c1bcabec'),
	(531,124,224,49,0,5,'2019-02-07 20:47:25','2019-02-07 20:47:25','3c7aca8d-15d1-4503-9990-3c71efb96eeb'),
	(532,124,225,1,0,1,'2019-02-07 20:47:25','2019-02-07 20:47:25','a8f57f96-2002-4732-aa80-aed21317ca4d'),
	(533,124,225,2,0,2,'2019-02-07 20:47:25','2019-02-07 20:47:25','1f2c4b47-88c9-4274-a165-f0862317a237'),
	(534,124,225,4,0,3,'2019-02-07 20:47:25','2019-02-07 20:47:25','632da377-9239-4002-9049-00ce3b31fab1'),
	(535,124,225,5,0,4,'2019-02-07 20:47:25','2019-02-07 20:47:25','b82c9cbc-d641-4701-8afb-596d0bb1e86c'),
	(536,125,226,49,0,1,'2019-02-07 20:47:25','2019-02-07 20:47:25','05ac4676-e191-4eb6-89b2-1ee793d9ffce'),
	(537,126,227,52,0,1,'2019-02-07 20:47:25','2019-02-07 20:47:25','01281d59-87d9-4787-a14c-a68f1d4bac72'),
	(538,127,228,41,0,1,'2019-02-07 20:47:25','2019-02-07 20:47:25','2b866499-8e10-4bc2-a119-46bedf0c6042'),
	(539,127,229,1,0,1,'2019-02-07 20:47:25','2019-02-07 20:47:25','af0ce292-e924-4887-8ad9-163d0bb5667b'),
	(540,127,229,2,0,2,'2019-02-07 20:47:25','2019-02-07 20:47:25','254fda80-73a8-4b00-8882-ee016db5fd3d'),
	(541,127,229,4,0,3,'2019-02-07 20:47:25','2019-02-07 20:47:25','a63b63cc-8d65-42f5-868c-892c0f4ecd72'),
	(542,127,229,5,0,4,'2019-02-07 20:47:25','2019-02-07 20:47:25','d47cacfc-fd6a-41c8-b2a8-dd130f87ee38'),
	(543,128,230,37,0,1,'2019-02-07 20:47:25','2019-02-07 20:47:25','25ed04fb-f9e5-4f51-a5ec-bcc325725a1c'),
	(544,128,231,1,0,1,'2019-02-07 20:47:25','2019-02-07 20:47:25','f18fcd33-d1ff-413c-978b-658530505353'),
	(545,128,231,2,0,2,'2019-02-07 20:47:25','2019-02-07 20:47:25','68fc831b-a7b2-49f8-ae01-33d2ac2c5068'),
	(546,128,231,4,0,3,'2019-02-07 20:47:25','2019-02-07 20:47:25','61c2c170-9111-4874-b269-7baac68bfdfd'),
	(547,128,231,5,0,4,'2019-02-07 20:47:25','2019-02-07 20:47:25','075fa7c2-7407-47bf-91c2-d66a34fae76c'),
	(548,129,232,74,0,1,'2019-02-07 20:47:25','2019-02-07 20:47:25','3c35671f-4890-4b15-be7c-e3b97b420d80'),
	(549,131,234,37,0,1,'2019-02-07 20:47:25','2019-02-07 20:47:25','43d968ca-9646-4cc6-8592-e21fb1ba0dd3'),
	(550,132,235,65,0,1,'2019-02-07 20:47:25','2019-02-07 20:47:25','25777ee0-9b2b-4d9f-891b-c3a62620dbe1'),
	(551,132,236,1,0,1,'2019-02-07 20:47:25','2019-02-07 20:47:25','b73c2132-a978-4c8c-9fb6-a370b9140927'),
	(552,132,236,2,0,2,'2019-02-07 20:47:25','2019-02-07 20:47:25','e41eda20-7143-46c1-ad98-3779c8c4100e'),
	(553,132,236,4,0,3,'2019-02-07 20:47:25','2019-02-07 20:47:25','aabe5990-7ac8-4f81-be7e-11f1f46c1b8f'),
	(554,132,236,5,0,4,'2019-02-07 20:47:25','2019-02-07 20:47:25','f868e8c0-8ded-4118-8e22-655491957240'),
	(555,133,237,51,0,1,'2019-02-07 23:54:58','2019-02-07 23:54:58','d8c67443-a388-46b1-8f40-0d7302b52306'),
	(556,133,237,49,0,2,'2019-02-07 23:54:58','2019-02-07 23:54:58','270adb56-9646-4dc8-a8e7-cea1c8c6acc8'),
	(557,133,237,2,0,3,'2019-02-07 23:54:58','2019-02-07 23:54:58','29a9e0b0-850f-410a-94f0-5ddb4a3eae26'),
	(558,134,238,38,0,1,'2019-02-07 23:54:58','2019-02-07 23:54:58','b8815419-9826-4a88-9e0c-269a92677891'),
	(559,134,239,3,0,1,'2019-02-07 23:54:58','2019-02-07 23:54:58','708707bd-2619-41bc-a5fe-75e218423c55'),
	(560,134,239,1,0,2,'2019-02-07 23:54:58','2019-02-07 23:54:58','ecb59bfc-eb4a-4266-b986-e8198f1fe37d'),
	(561,134,239,2,0,3,'2019-02-07 23:54:58','2019-02-07 23:54:58','65d80279-3f25-4860-876a-4d1b3ea9faf0'),
	(562,134,239,4,0,4,'2019-02-07 23:54:58','2019-02-07 23:54:58','92e099b0-9391-40e7-81db-9618db0a178e'),
	(563,134,239,5,0,5,'2019-02-07 23:54:58','2019-02-07 23:54:58','ceee5618-2488-4c24-a2d2-db54b4694e06'),
	(564,135,240,51,0,1,'2019-02-07 23:54:58','2019-02-07 23:54:58','04833e04-7d18-4e0f-bc73-93374d3c1a5d'),
	(565,135,240,62,0,2,'2019-02-07 23:54:58','2019-02-07 23:54:58','83cbb9f3-d91f-4f15-a8fa-cd6db87f21e4'),
	(566,135,240,63,0,3,'2019-02-07 23:54:58','2019-02-07 23:54:58','69c82cb2-0dad-4ca1-b66f-8d01ce7687b9'),
	(567,135,240,64,0,4,'2019-02-07 23:54:58','2019-02-07 23:54:58','f7fc6b5e-5b0a-43f1-85c9-0a3862322862'),
	(568,135,240,49,0,5,'2019-02-07 23:54:58','2019-02-07 23:54:58','3c1ad81d-863b-48e2-931f-419a820a6e72'),
	(569,135,241,3,0,1,'2019-02-07 23:54:58','2019-02-07 23:54:58','2bbc95fd-574c-4f18-ac12-998293cc576f'),
	(570,135,241,1,0,2,'2019-02-07 23:54:58','2019-02-07 23:54:58','fc2b6430-399c-4f17-9360-fb0945371241'),
	(571,135,241,2,0,3,'2019-02-07 23:54:58','2019-02-07 23:54:58','f61969b5-ca34-4084-bd35-514f6b8ee6d9'),
	(572,135,241,4,0,4,'2019-02-07 23:54:58','2019-02-07 23:54:58','34eabce3-77c4-4f40-94c1-24bbf752445b'),
	(573,135,241,5,0,5,'2019-02-07 23:54:58','2019-02-07 23:54:58','fd2e953f-17cd-4f9f-a6c4-124e7a8abec8'),
	(574,136,242,49,0,1,'2019-02-07 23:54:58','2019-02-07 23:54:58','fd8c86eb-70b8-4a4d-850b-f28257b31383'),
	(575,137,243,52,0,1,'2019-02-07 23:54:58','2019-02-07 23:54:58','ac646198-87c3-43d5-be0f-6a78aafe5c41'),
	(576,138,244,41,0,1,'2019-02-07 23:54:58','2019-02-07 23:54:58','22e784b1-4764-4e4d-a120-10aaec4cbd31'),
	(577,138,245,3,0,1,'2019-02-07 23:54:58','2019-02-07 23:54:58','abb3e398-e86d-49b9-ad3d-21883e828aa2'),
	(578,138,245,1,0,2,'2019-02-07 23:54:58','2019-02-07 23:54:58','07a00403-f9dc-4607-b86b-4dbab8ecbddb'),
	(579,138,245,2,0,3,'2019-02-07 23:54:58','2019-02-07 23:54:58','f064da22-1ede-47dd-82da-5a1400ab0bd6'),
	(580,138,245,4,0,4,'2019-02-07 23:54:58','2019-02-07 23:54:58','37b1b245-b05b-4d27-b5f2-d12b51e4693d'),
	(581,138,245,5,0,5,'2019-02-07 23:54:58','2019-02-07 23:54:58','51ffa736-1937-4f20-ae49-d0d821ec74bd'),
	(582,139,246,37,0,1,'2019-02-07 23:54:58','2019-02-07 23:54:58','86244303-831d-4979-86f5-b491affa5c0f'),
	(583,139,247,3,0,1,'2019-02-07 23:54:58','2019-02-07 23:54:58','c6bd12fe-d2aa-4a19-b2a4-92f0727ec3cb'),
	(584,139,247,1,0,2,'2019-02-07 23:54:58','2019-02-07 23:54:58','087a9433-fc23-4998-bb25-ae7a7b41839a'),
	(585,139,247,2,0,3,'2019-02-07 23:54:58','2019-02-07 23:54:58','fa0bb11d-a87c-4a38-b0ea-9d9e2d65d525'),
	(586,139,247,4,0,4,'2019-02-07 23:54:58','2019-02-07 23:54:58','ad9be34e-2a2a-404b-a66a-e119447cb114'),
	(587,139,247,5,0,5,'2019-02-07 23:54:58','2019-02-07 23:54:58','7bbafe34-2552-4bae-961f-deb46ea4f7b5'),
	(588,140,248,74,0,1,'2019-02-07 23:54:58','2019-02-07 23:54:58','aa4f399f-b51a-45bb-8b11-9a06137348f0'),
	(589,142,250,37,0,1,'2019-02-07 23:54:58','2019-02-07 23:54:58','8731d530-f51f-4318-b5fb-0751b295c1ba'),
	(590,143,251,65,0,1,'2019-02-07 23:54:58','2019-02-07 23:54:58','9b547e02-1faa-4d48-9006-dbf5f8acf7eb'),
	(591,143,252,3,0,1,'2019-02-07 23:54:58','2019-02-07 23:54:58','2cb88041-330e-4561-adcd-b49efac7c31d'),
	(592,143,252,1,0,2,'2019-02-07 23:54:58','2019-02-07 23:54:58','db840832-f4de-427b-9f06-3ff186727590'),
	(593,143,252,2,0,3,'2019-02-07 23:54:58','2019-02-07 23:54:58','cb0c1f1a-300d-41ab-9c10-3abb860654a3'),
	(594,143,252,4,0,4,'2019-02-07 23:54:58','2019-02-07 23:54:58','a7fdaed9-6a1a-47ed-b5d8-42e74e7c9bb2'),
	(595,143,252,5,0,5,'2019-02-07 23:54:58','2019-02-07 23:54:58','13f65762-7910-4f30-80c1-e15c10988f27'),
	(596,144,253,51,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','deda7d2c-35db-42b4-8731-7c6f4d0fabda'),
	(597,144,253,49,0,2,'2019-02-08 20:03:35','2019-02-08 20:03:35','b2e8828c-bdb7-4039-a6e1-79b382f3ecab'),
	(598,144,253,2,0,3,'2019-02-08 20:03:35','2019-02-08 20:03:35','fdf8c995-0411-4bc3-8789-e98079bf73e6'),
	(599,145,254,38,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','32838c12-1fa6-44a5-8b40-bcbb7179b7ba'),
	(600,145,255,3,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','da5e508b-a478-4170-b78d-908a40b944ba'),
	(601,145,255,1,0,2,'2019-02-08 20:03:35','2019-02-08 20:03:35','b9994651-1fc2-4903-88c6-26851bb7c0af'),
	(602,145,255,2,0,3,'2019-02-08 20:03:35','2019-02-08 20:03:35','27e5e0e7-408d-405d-84a2-b6545edac294'),
	(603,145,255,4,0,4,'2019-02-08 20:03:35','2019-02-08 20:03:35','10e5a10d-ba91-46de-bffd-d511f6280f7e'),
	(604,145,255,5,0,5,'2019-02-08 20:03:35','2019-02-08 20:03:35','f6574df0-e37b-426e-bb11-f16d498686a1'),
	(605,146,256,51,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','cba06188-858d-4d38-988f-f0a90563f8ea'),
	(606,146,256,62,0,2,'2019-02-08 20:03:35','2019-02-08 20:03:35','4ff94c41-276b-45f7-88d6-d66af0a830d3'),
	(607,146,256,63,0,3,'2019-02-08 20:03:35','2019-02-08 20:03:35','000cdf76-f9ba-4528-9d60-f1cff63bb6fa'),
	(608,146,256,64,0,4,'2019-02-08 20:03:35','2019-02-08 20:03:35','40503b3b-2026-4b92-ba9a-bf66c06258eb'),
	(609,146,256,49,0,5,'2019-02-08 20:03:35','2019-02-08 20:03:35','2a7cd102-9a84-40f5-86bc-b36d042b868a'),
	(610,146,257,3,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','a6a88107-ed7e-4d01-b0ad-a7f4497dcd58'),
	(611,146,257,1,0,2,'2019-02-08 20:03:35','2019-02-08 20:03:35','5d23caec-96b2-4a8e-a8ea-c20e4380ca8d'),
	(612,146,257,2,0,3,'2019-02-08 20:03:35','2019-02-08 20:03:35','57b7faff-06cd-438f-b28e-844ac2c8312c'),
	(613,146,257,4,0,4,'2019-02-08 20:03:35','2019-02-08 20:03:35','6bd9553f-9fd6-45ff-87b6-14a0adb9c383'),
	(614,146,257,5,0,5,'2019-02-08 20:03:35','2019-02-08 20:03:35','27bf0cd8-435e-4eea-96f4-ed86cebce75d'),
	(615,147,258,49,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','7b31df27-7696-43a4-b69c-f7fb9e0646a5'),
	(616,148,259,52,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','bccbb2b1-5138-4b97-a6e7-a044ef050b47'),
	(617,149,260,41,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','f3409db5-027e-4ef8-a847-24c0135da968'),
	(618,149,261,3,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','c2b9c7c7-6382-4f13-999f-0d193da548eb'),
	(619,149,261,1,0,2,'2019-02-08 20:03:35','2019-02-08 20:03:35','eb9cfe1c-4a44-4640-82f5-0460cf3299f8'),
	(620,149,261,2,0,3,'2019-02-08 20:03:35','2019-02-08 20:03:35','66c3db19-264a-44ec-9dea-c166cf884945'),
	(621,149,261,4,0,4,'2019-02-08 20:03:35','2019-02-08 20:03:35','80921725-2f45-49e4-a5d2-e4faf49d6839'),
	(622,149,261,5,0,5,'2019-02-08 20:03:35','2019-02-08 20:03:35','d59afe03-1fd0-4129-9cd1-b0037c1740b4'),
	(623,150,262,37,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','658de374-efb6-48d5-9a0e-467ad06bc10b'),
	(624,150,263,3,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','20bf6cb9-c7cd-4bbe-b1a3-1c3d070fd1a2'),
	(625,150,263,1,0,2,'2019-02-08 20:03:35','2019-02-08 20:03:35','eb44bc1a-1d7d-495c-b2e5-cfa5483fc9c7'),
	(626,150,263,2,0,3,'2019-02-08 20:03:35','2019-02-08 20:03:35','a209453b-50f1-4441-b300-5befa9dbb7f2'),
	(627,150,263,4,0,4,'2019-02-08 20:03:35','2019-02-08 20:03:35','3b6800a6-858a-4148-9eda-2572e5b67b8e'),
	(628,150,263,5,0,5,'2019-02-08 20:03:35','2019-02-08 20:03:35','82e78747-ea07-4f25-9e12-d1315155e280'),
	(629,151,264,74,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','2bc24a56-971a-45d2-8acf-fc9cb2142315'),
	(630,153,266,37,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','41765b50-821d-4aeb-87a6-32958747a241'),
	(631,154,267,65,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','089a4919-9f14-4807-8e10-272148b61d19'),
	(632,154,268,3,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','4907145f-ffee-4292-9bdb-668ea983298e'),
	(633,154,268,1,0,2,'2019-02-08 20:03:35','2019-02-08 20:03:35','e7e04f18-68cf-496e-a34b-18d6cca8bfbd'),
	(634,154,268,2,0,3,'2019-02-08 20:03:35','2019-02-08 20:03:35','b6d23fa5-edc1-4b01-90ce-11f13a9342df'),
	(635,154,268,4,0,4,'2019-02-08 20:03:35','2019-02-08 20:03:35','a984c50a-6d10-47a4-b3d6-8067faacb278'),
	(636,154,268,5,0,5,'2019-02-08 20:03:35','2019-02-08 20:03:35','031a7724-aa32-42d7-8b23-4115f8e123b4'),
	(637,155,269,49,0,1,'2019-02-08 20:03:35','2019-02-08 20:03:35','07500a43-8fd5-4505-a96f-a57e50c214b2'),
	(638,155,269,63,0,2,'2019-02-08 20:03:35','2019-02-08 20:03:35','aed3bb24-0660-462e-b9f8-c2d821c9452c'),
	(639,155,269,48,0,3,'2019-02-08 20:03:35','2019-02-08 20:03:35','a13b23a1-9f51-48ae-9a8d-b7bb4db01156');

/*!40000 ALTER TABLE `craft_fieldlayoutfields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fieldlayouts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldlayouts`;

CREATE TABLE `craft_fieldlayouts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_fieldlayouts_dateDeleted_idx` (`dateDeleted`),
  KEY `craft_fieldlayouts_type_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_fieldlayouts` WRITE;
/*!40000 ALTER TABLE `craft_fieldlayouts` DISABLE KEYS */;

INSERT INTO `craft_fieldlayouts` (`id`, `type`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,'craft\\elements\\MatrixBlock','2019-01-21 23:48:56','2019-01-21 23:48:56','2019-01-22 15:32:44','2a6f22f6-c82c-4a52-a3ad-a846087ab7c3'),
	(2,'verbb\\supertable\\elements\\SuperTableBlockElement','2019-01-21 23:48:57','2019-01-21 23:48:57','2019-01-22 15:32:46','fb8e4666-b47a-4745-b788-e77e29514551'),
	(3,'craft\\elements\\MatrixBlock','2019-01-21 23:48:58','2019-01-21 23:48:58','2019-01-22 15:32:47','41d012fc-1788-4c1c-aa28-31b4d1d30432'),
	(4,'craft\\elements\\MatrixBlock','2019-01-21 23:49:01','2019-01-21 23:49:01','2019-01-22 15:32:49','bebc7103-691d-479f-8cca-bf6fe822e531'),
	(5,'craft\\elements\\MatrixBlock','2019-01-21 23:49:02','2019-01-21 23:49:02','2019-01-22 15:32:52','c7c490e9-db98-419b-a6f7-233fd98abc15'),
	(6,'craft\\elements\\MatrixBlock','2019-01-21 23:49:03','2019-01-21 23:49:03','2019-01-22 15:32:52','7cd52b70-18fc-4206-a2ee-74e44c14d654'),
	(7,'verbb\\supertable\\elements\\SuperTableBlockElement','2019-01-21 23:49:05','2019-01-21 23:49:05','2019-01-22 15:32:53','49d26ca7-63db-40d9-8d9a-383740502337'),
	(8,'craft\\elements\\MatrixBlock','2019-01-21 23:49:06','2019-01-21 23:49:06','2019-01-22 15:32:53','ab9218b3-f6aa-45bd-9fcd-972351806f5f'),
	(9,'craft\\elements\\MatrixBlock','2019-01-21 23:49:06','2019-01-21 23:49:06','2019-01-22 15:32:54','a445497d-8448-444f-ae0f-ef079430dd1c'),
	(10,'verbb\\supertable\\elements\\SuperTableBlockElement','2019-01-21 23:49:08','2019-01-31 16:04:10',NULL,'5a83b54c-ff15-4d23-9c94-e0977905d3ae'),
	(11,'verbb\\supertable\\elements\\SuperTableBlockElement','2019-01-21 23:49:10','2019-01-30 23:31:54',NULL,'a80581a7-0ff2-4616-ba93-e1f10a8e29ed'),
	(12,'verbb\\supertable\\elements\\SuperTableBlockElement','2019-01-21 23:49:13','2019-01-21 23:49:13','2019-01-30 21:40:57','9fd20304-2e62-4aec-861a-9e2f64afa425'),
	(13,'benf\\neo\\elements\\Block','2019-01-22 15:42:57','2019-01-22 15:42:57','2019-01-22 15:47:51','16789b11-0251-450d-94ba-c95bb1ed508a'),
	(14,'benf\\neo\\elements\\Block','2019-01-22 15:42:57','2019-01-22 15:42:57','2019-01-22 15:47:51','46c8cef0-6e84-4e41-af9c-4924ff8daeb1'),
	(15,'benf\\neo\\elements\\Block','2019-01-22 15:42:57','2019-01-22 15:42:57','2019-01-22 15:47:52','ad8bacec-f4bb-4f98-82de-43b1dc4b08d0'),
	(16,'benf\\neo\\elements\\Block','2019-01-22 15:42:57','2019-01-22 15:42:57','2019-01-22 15:47:52','f80fcc5f-e17a-4302-ad4c-532091ece97f'),
	(17,'benf\\neo\\elements\\Block','2019-01-22 15:42:57','2019-01-22 15:42:57','2019-01-22 15:47:52','9f28e0c6-3c86-4680-9603-341dc6ef5d9c'),
	(18,'benf\\neo\\elements\\Block','2019-01-22 15:47:51','2019-01-22 15:47:51','2019-01-22 15:52:56','a95989cd-0256-4a7a-9ef8-e4731f46606b'),
	(19,'benf\\neo\\elements\\Block','2019-01-22 15:47:51','2019-01-22 15:47:51','2019-01-22 15:52:57','3fe6b32d-d1b2-4acd-a678-c186911b4db8'),
	(20,'benf\\neo\\elements\\Block','2019-01-22 15:47:51','2019-01-22 15:47:51','2019-01-22 15:52:57','dd3d248e-1165-432c-973b-33a2bc2bcb6e'),
	(21,'benf\\neo\\elements\\Block','2019-01-22 15:47:51','2019-01-22 15:47:51','2019-01-22 15:52:57','65084ca9-ddd3-461b-85e4-009f591e4a8b'),
	(22,'benf\\neo\\elements\\Block','2019-01-22 15:47:52','2019-01-22 15:47:52','2019-01-22 15:52:57','a38e8d3f-0108-4123-ace7-45c0d77c3693'),
	(23,'benf\\neo\\elements\\Block','2019-01-22 15:47:52','2019-01-22 15:47:52','2019-01-22 15:52:57','9bb77590-94fb-4ecd-94ce-8e3ba7a7e21e'),
	(24,'benf\\neo\\elements\\Block','2019-01-22 15:47:52','2019-01-22 15:47:52','2019-01-22 15:52:57','5691ad4f-afa9-40ab-b30e-fff1195ab7d0'),
	(25,'benf\\neo\\elements\\Block','2019-01-22 15:52:56','2019-01-22 15:55:26','2019-01-22 15:56:26','79c7f612-5015-47e2-b75b-cee6053177e6'),
	(26,'benf\\neo\\elements\\Block','2019-01-22 15:52:57','2019-01-22 15:55:26','2019-01-22 15:56:26','14b79429-603a-4929-b564-6386b3671e61'),
	(27,'benf\\neo\\elements\\Block','2019-01-22 15:52:57','2019-01-22 15:55:26','2019-01-22 15:56:26','bb481c7b-e2a4-43f9-981b-9fa7b24cc839'),
	(28,'benf\\neo\\elements\\Block','2019-01-22 15:52:57','2019-01-22 15:55:27','2019-01-22 15:56:26','8fdd7074-ed83-4681-b8f5-7e9ddd847881'),
	(29,'benf\\neo\\elements\\Block','2019-01-22 15:52:57','2019-01-22 15:55:27','2019-01-22 15:56:26','d31e3aaa-22c2-466b-8d2f-fdd38cf995fa'),
	(30,'benf\\neo\\elements\\Block','2019-01-22 15:52:57','2019-01-22 15:55:27','2019-01-22 15:56:26','01c60177-2fd7-4971-97d3-23363f16c07e'),
	(31,'benf\\neo\\elements\\Block','2019-01-22 15:52:57','2019-01-22 15:55:27','2019-01-22 15:56:26','d4e0bc76-8189-4d93-9175-ddc1df05aea9'),
	(32,'benf\\neo\\elements\\Block','2019-01-22 15:56:26','2019-01-22 15:56:26','2019-01-22 15:59:52','bf63a996-40c3-44c4-ae74-575a8ae9e0d8'),
	(33,'benf\\neo\\elements\\Block','2019-01-22 15:56:26','2019-01-22 15:56:26','2019-01-22 15:59:52','f734a4cc-8b6f-4cf5-bb48-b79d8c480316'),
	(34,'benf\\neo\\elements\\Block','2019-01-22 15:56:26','2019-01-22 15:56:26','2019-01-22 15:59:52','3945dc64-8313-4119-8623-f4c822d85e97'),
	(35,'benf\\neo\\elements\\Block','2019-01-22 15:56:26','2019-01-22 15:56:26','2019-01-22 15:59:52','7c0cca02-7588-4b92-b5b6-0bcc69acfe8d'),
	(36,'benf\\neo\\elements\\Block','2019-01-22 15:56:26','2019-01-22 15:56:26','2019-01-22 15:59:52','081db510-d877-496d-89b8-2d531bf0f631'),
	(37,'benf\\neo\\elements\\Block','2019-01-22 15:56:26','2019-01-22 15:56:26','2019-01-22 15:59:52','03f554de-8ef9-47be-8fe8-15aa4070acfc'),
	(38,'benf\\neo\\elements\\Block','2019-01-22 15:56:26','2019-01-22 15:56:26','2019-01-22 15:59:52','5da6f28c-4fa1-4721-ab3c-72f07381df41'),
	(39,'benf\\neo\\elements\\Block','2019-01-22 15:56:27','2019-01-22 15:56:27','2019-01-22 15:59:52','2cf11fec-2ea3-4329-9738-1be0d935ec5a'),
	(40,'benf\\neo\\elements\\Block','2019-01-22 15:59:52','2019-01-22 15:59:52','2019-01-22 16:00:55','2ccaef55-c380-4975-b5d3-2785aac1f837'),
	(41,'benf\\neo\\elements\\Block','2019-01-22 15:59:52','2019-01-22 15:59:52','2019-01-22 16:00:55','04d16eb9-5db1-4f4e-a8ef-fe76c1dd1a68'),
	(42,'benf\\neo\\elements\\Block','2019-01-22 15:59:52','2019-01-22 15:59:52','2019-01-22 16:00:55','adfeab08-557d-4033-be85-74caaf5e0646'),
	(43,'benf\\neo\\elements\\Block','2019-01-22 15:59:52','2019-01-22 15:59:52','2019-01-22 16:00:56','5ce9e387-d8e8-46f1-b5d2-865067c30f15'),
	(44,'benf\\neo\\elements\\Block','2019-01-22 15:59:52','2019-01-22 15:59:52','2019-01-22 16:00:56','135e136e-7be3-4c21-84f7-9d6dcbbe7b86'),
	(45,'benf\\neo\\elements\\Block','2019-01-22 15:59:52','2019-01-22 15:59:52','2019-01-22 16:00:56','e07bade8-7a9d-473b-a120-728e53ab397f'),
	(46,'benf\\neo\\elements\\Block','2019-01-22 15:59:52','2019-01-22 15:59:52','2019-01-22 16:00:56','7635fdd3-9ed9-4a8e-a89a-cb79170c8061'),
	(47,'benf\\neo\\elements\\Block','2019-01-22 15:59:52','2019-01-22 15:59:52','2019-01-22 16:00:56','f47f4e7b-36eb-4f7c-9310-2e690f1917c6'),
	(48,'benf\\neo\\elements\\Block','2019-01-22 15:59:52','2019-01-22 15:59:52','2019-01-22 16:00:56','9a5c5b29-fc73-49b1-8d3f-d4dc7d5340cc'),
	(49,'benf\\neo\\elements\\Block','2019-01-22 16:00:55','2019-01-22 16:00:55','2019-01-22 16:03:50','03ad506c-4651-4ab5-8e67-ada05cc5cb0f'),
	(50,'benf\\neo\\elements\\Block','2019-01-22 16:00:55','2019-01-22 16:00:55','2019-01-22 16:03:50','7402ff12-a8a1-4aea-bfc3-b3b377172b2c'),
	(51,'benf\\neo\\elements\\Block','2019-01-22 16:00:55','2019-01-22 16:00:55','2019-01-22 16:03:50','737caf17-8361-40d4-b6b2-a3e32f270aa8'),
	(52,'benf\\neo\\elements\\Block','2019-01-22 16:00:56','2019-01-22 16:00:56','2019-01-22 16:03:50','29602a55-1dff-491d-adea-0e4d10ab6bb9'),
	(53,'benf\\neo\\elements\\Block','2019-01-22 16:00:56','2019-01-22 16:00:56','2019-01-22 16:03:50','8fcc88b5-0b4e-49ee-a1c0-0685d395c095'),
	(54,'benf\\neo\\elements\\Block','2019-01-22 16:00:56','2019-01-22 16:00:56','2019-01-22 16:03:50','b2dbc6d2-8a02-4523-a21d-2b1640fe5543'),
	(55,'benf\\neo\\elements\\Block','2019-01-22 16:00:56','2019-01-22 16:00:56','2019-01-22 16:03:50','1a674dc6-969e-4dfc-ab6e-cda4806f8463'),
	(56,'benf\\neo\\elements\\Block','2019-01-22 16:00:56','2019-01-22 16:00:56','2019-01-22 16:03:50','e6b636ad-f98f-4d66-9103-832be0abed59'),
	(57,'benf\\neo\\elements\\Block','2019-01-22 16:00:56','2019-01-22 16:00:56','2019-01-22 16:03:50','91a32a31-2ad8-4f36-8b5c-0827dbe9b7e9'),
	(58,'benf\\neo\\elements\\Block','2019-01-22 16:03:50','2019-01-22 16:06:18','2019-01-22 16:08:34','e1a65105-9a9e-4998-aeac-4253237bda70'),
	(59,'benf\\neo\\elements\\Block','2019-01-22 16:03:50','2019-01-22 16:06:18','2019-01-22 16:08:34','8f2c6b82-fbde-4778-a2d2-fb23a212cd06'),
	(60,'benf\\neo\\elements\\Block','2019-01-22 16:03:50','2019-01-22 16:06:19','2019-01-22 16:08:34','5fa24ab7-45d6-4432-8045-bb703099f638'),
	(61,'benf\\neo\\elements\\Block','2019-01-22 16:03:50','2019-01-22 16:06:19','2019-01-22 16:08:34','eddc6e25-e0ef-4464-85b6-c9ee62a482e2'),
	(62,'benf\\neo\\elements\\Block','2019-01-22 16:03:50','2019-01-22 16:06:19','2019-01-22 16:08:34','0f1381cf-78f5-40f4-adb0-8d8ec7e37c82'),
	(63,'benf\\neo\\elements\\Block','2019-01-22 16:03:50','2019-01-22 16:06:19','2019-01-22 16:08:35','d6fc0cb5-373e-4b62-8967-860bc19c55cd'),
	(64,'benf\\neo\\elements\\Block','2019-01-22 16:03:50','2019-01-22 16:06:19','2019-01-22 16:08:35','de9ab0a2-8a43-493a-b4dd-6ceee501c185'),
	(65,'benf\\neo\\elements\\Block','2019-01-22 16:03:50','2019-01-22 16:06:19','2019-01-22 16:08:35','795cfe2b-c4e3-4c8e-9fa1-4364deb421bc'),
	(66,'benf\\neo\\elements\\Block','2019-01-22 16:03:50','2019-01-22 16:06:19','2019-01-22 16:08:35','154053f2-f4c6-4407-9de0-b1cc7a4e102a'),
	(67,'benf\\neo\\elements\\Block','2019-01-22 16:03:50','2019-01-22 16:06:19','2019-01-22 16:08:35','c92ba4fa-8011-4e70-afa2-e22687f055bf'),
	(68,'benf\\neo\\elements\\Block','2019-01-22 16:03:50','2019-01-22 16:06:19','2019-01-22 16:08:35','c256365d-3d2a-4144-836e-c23f3ff91874'),
	(69,'craft\\elements\\MatrixBlock','2019-01-22 16:06:20','2019-01-22 16:06:20',NULL,'4e75fa4e-7c81-44ae-a7b1-4a0db1b53ff6'),
	(70,'benf\\neo\\elements\\Block','2019-01-22 16:08:34','2019-01-22 16:09:22','2019-01-30 23:39:28','249c7217-f831-48df-bdb3-5ee00c2e65a1'),
	(71,'benf\\neo\\elements\\Block','2019-01-22 16:08:34','2019-01-22 16:09:23','2019-01-30 23:39:28','27be1f1a-1d16-4932-959a-f43bb8336ef0'),
	(72,'benf\\neo\\elements\\Block','2019-01-22 16:08:34','2019-01-22 16:09:23','2019-01-30 23:39:28','9b65dd53-0b95-4e35-a415-58a5ffce1b48'),
	(73,'benf\\neo\\elements\\Block','2019-01-22 16:08:34','2019-01-22 16:09:23','2019-01-30 23:39:28','686388a2-8104-4ed5-863d-95a011955d8e'),
	(74,'benf\\neo\\elements\\Block','2019-01-22 16:08:34','2019-01-22 16:09:23','2019-01-30 23:39:28','9cd0ea28-49ab-4ba3-8e26-4af2a7b4633a'),
	(75,'benf\\neo\\elements\\Block','2019-01-22 16:08:35','2019-01-22 16:09:23','2019-01-30 23:39:28','cc278bbc-77ac-4b2b-acb8-46f07f3bacf5'),
	(76,'benf\\neo\\elements\\Block','2019-01-22 16:08:35','2019-01-22 16:09:23','2019-01-30 23:39:28','e3039fe0-5812-455c-9678-9d57bd5a0ff1'),
	(77,'benf\\neo\\elements\\Block','2019-01-22 16:08:35','2019-01-22 16:09:23','2019-01-30 23:39:28','5854af67-0ba5-4337-b1a9-7e8607317c6f'),
	(78,'benf\\neo\\elements\\Block','2019-01-22 16:08:35','2019-01-22 16:09:23','2019-01-30 23:39:28','55407783-7dba-4458-acf3-a85f3ba793e7'),
	(79,'benf\\neo\\elements\\Block','2019-01-22 16:08:35','2019-01-22 16:09:23','2019-01-30 23:39:28','10e082ab-e2fd-4fd9-88cd-48a1d3252318'),
	(80,'benf\\neo\\elements\\Block','2019-01-22 16:08:35','2019-01-22 16:09:23','2019-01-30 23:39:28','5568e2e1-8ea0-4ae3-ae85-6996c540f12a'),
	(81,'craft\\elements\\GlobalSet','2019-01-22 16:09:23','2019-01-30 21:44:19',NULL,'610ee66f-738a-44ef-8d83-dcbc043f9d58'),
	(82,'craft\\elements\\GlobalSet','2019-01-22 16:10:08','2019-01-22 16:10:08',NULL,'6c438554-e1ad-483d-a7d7-403b8a0ea3a4'),
	(83,'craft\\elements\\Entry','2019-01-22 16:11:58','2019-01-22 16:11:58',NULL,'b995cdd6-6eea-435e-8828-4896a3608a34'),
	(84,'craft\\elements\\Entry','2019-01-30 21:27:55','2019-01-31 16:43:11',NULL,'b593a223-8bc3-4d82-b815-5b1545fcdab1'),
	(85,'craft\\elements\\Entry','2019-01-30 21:31:52','2019-01-30 23:37:21',NULL,'f9f61668-5913-4aaa-8b2c-2150b2e6f12f'),
	(86,'craft\\elements\\MatrixBlock','2019-01-30 21:43:52','2019-01-30 21:44:19',NULL,'ccf8f545-4938-4bd5-a07d-1216a65e0472'),
	(87,'benf\\neo\\elements\\Block','2019-01-30 23:39:28','2019-01-30 23:39:28','2019-01-31 16:40:23','e150e29c-2e8a-4ebb-8d69-fa578b4dce84'),
	(88,'benf\\neo\\elements\\Block','2019-01-30 23:39:28','2019-01-30 23:39:28','2019-01-31 16:40:23','7d442be4-1f37-4fc1-a156-e91432b19f48'),
	(89,'benf\\neo\\elements\\Block','2019-01-30 23:39:28','2019-01-30 23:39:28','2019-01-31 16:40:23','d48d7eaf-3be0-4c6f-ae4a-bbcf8dc9848a'),
	(90,'benf\\neo\\elements\\Block','2019-01-30 23:39:28','2019-01-30 23:39:28','2019-01-31 16:40:23','dde7cd51-72b9-4dc0-ac78-3d6302bf2d9b'),
	(91,'benf\\neo\\elements\\Block','2019-01-30 23:39:28','2019-01-30 23:39:28','2019-01-31 16:40:23','94839f77-aa1f-4b82-9163-06dfa7317232'),
	(92,'benf\\neo\\elements\\Block','2019-01-30 23:39:28','2019-01-30 23:39:28','2019-01-31 16:40:23','1a70608e-4d91-4db8-83e0-0bd2586b411d'),
	(93,'benf\\neo\\elements\\Block','2019-01-30 23:39:28','2019-01-30 23:39:28','2019-01-31 16:40:23','d58c19ae-c164-45bc-9589-c053648dbb1e'),
	(94,'benf\\neo\\elements\\Block','2019-01-30 23:39:28','2019-01-30 23:39:28','2019-01-31 16:40:23','0716dab6-9869-46ca-baa6-a560a5da6b2e'),
	(95,'benf\\neo\\elements\\Block','2019-01-30 23:39:28','2019-01-30 23:39:28','2019-01-31 16:40:23','c1fe5d71-7813-4a8d-b041-d5de5d5d3033'),
	(96,'benf\\neo\\elements\\Block','2019-01-30 23:39:28','2019-01-30 23:39:28','2019-01-31 16:40:23','f8a33409-14f8-4dbe-84f3-086c36d3d506'),
	(97,'benf\\neo\\elements\\Block','2019-01-30 23:39:28','2019-01-30 23:39:28','2019-01-31 16:40:23','16ac1e1d-4eb6-46b5-9d55-2105fbd13a65'),
	(98,'craft\\elements\\Entry','2019-01-31 15:32:38','2019-02-05 18:46:06',NULL,'451aca94-daf3-4f9b-a199-188df903f943'),
	(99,'benf\\neo\\elements\\Block','2019-01-31 16:40:23','2019-01-31 16:40:23','2019-02-07 20:46:38','b501b3cd-737f-4c87-8d12-42f00cac1397'),
	(100,'benf\\neo\\elements\\Block','2019-01-31 16:40:23','2019-01-31 16:40:23','2019-02-07 20:46:38','b953bae5-5037-4eb3-baf9-99ce5307dbe0'),
	(101,'benf\\neo\\elements\\Block','2019-01-31 16:40:23','2019-01-31 16:40:23','2019-02-07 20:46:38','7afa26c3-fb41-4b63-9939-2ee4c33eddca'),
	(102,'benf\\neo\\elements\\Block','2019-01-31 16:40:23','2019-01-31 16:40:23','2019-02-07 20:46:38','1ae3b65e-18d7-4392-a161-0287730cdbde'),
	(103,'benf\\neo\\elements\\Block','2019-01-31 16:40:23','2019-01-31 16:40:23','2019-02-07 20:46:38','1d639aeb-79a2-4455-8f35-4d7406512155'),
	(104,'benf\\neo\\elements\\Block','2019-01-31 16:40:23','2019-01-31 16:40:23','2019-02-07 20:46:38','ec515c11-3d34-4bfc-b80b-5a8f56de8c82'),
	(105,'benf\\neo\\elements\\Block','2019-01-31 16:40:23','2019-01-31 16:40:23','2019-02-07 20:46:38','8d20d334-b887-4b53-bf15-c1386ee9bdd6'),
	(106,'benf\\neo\\elements\\Block','2019-01-31 16:40:23','2019-01-31 16:40:23','2019-02-07 20:46:38','0ef6fc85-30f7-4aea-8d8e-d5b6d3225069'),
	(107,'benf\\neo\\elements\\Block','2019-01-31 16:40:23','2019-01-31 16:40:23','2019-02-07 20:46:38','533af013-2dcf-4640-aa93-4ce21d4e96c8'),
	(108,'benf\\neo\\elements\\Block','2019-01-31 16:40:23','2019-01-31 16:40:23','2019-02-07 20:46:38','ee281538-69ec-406d-b358-45f0f38b570a'),
	(109,'benf\\neo\\elements\\Block','2019-01-31 16:40:23','2019-01-31 16:40:23','2019-02-07 20:46:38','bed2e013-3b23-430e-8a77-ffef7a538fe0'),
	(110,'craft\\elements\\Entry','2019-01-31 20:56:19','2019-01-31 20:56:19',NULL,'c6180b17-2363-4a81-8ad8-b98460c0bb63'),
	(111,'benf\\neo\\elements\\Block','2019-02-07 20:46:38','2019-02-07 20:46:38','2019-02-07 20:47:25','df2c49b8-ef3b-4e22-a356-62bb8422d933'),
	(112,'benf\\neo\\elements\\Block','2019-02-07 20:46:38','2019-02-07 20:46:38','2019-02-07 20:47:25','cb54c24d-1e50-4d4f-ac18-3f4d8b0439ba'),
	(113,'benf\\neo\\elements\\Block','2019-02-07 20:46:38','2019-02-07 20:46:38','2019-02-07 20:47:25','a433bd0c-2165-436e-8d63-a006871e6262'),
	(114,'benf\\neo\\elements\\Block','2019-02-07 20:46:38','2019-02-07 20:46:38','2019-02-07 20:47:25','492981e8-511b-4b78-a743-54fdd89ad7d1'),
	(115,'benf\\neo\\elements\\Block','2019-02-07 20:46:38','2019-02-07 20:46:38','2019-02-07 20:47:25','fdef8899-f664-4f82-8e37-23584e642885'),
	(116,'benf\\neo\\elements\\Block','2019-02-07 20:46:38','2019-02-07 20:46:38','2019-02-07 20:47:25','f8492338-d154-42d4-ae41-5087b5283bae'),
	(117,'benf\\neo\\elements\\Block','2019-02-07 20:46:38','2019-02-07 20:46:38','2019-02-07 20:47:25','2f9f3404-ff2a-4bda-b281-4c263bdd2500'),
	(118,'benf\\neo\\elements\\Block','2019-02-07 20:46:38','2019-02-07 20:46:38','2019-02-07 20:47:25','dcf5841c-e532-4f87-bb32-8f18ec29303d'),
	(119,'benf\\neo\\elements\\Block','2019-02-07 20:46:38','2019-02-07 20:46:38','2019-02-07 20:47:25','5c009f3d-e246-45ef-b5af-cb68d3f63da9'),
	(120,'benf\\neo\\elements\\Block','2019-02-07 20:46:38','2019-02-07 20:46:38','2019-02-07 20:47:25','7b537d55-feb6-4700-a4de-896b051def7a'),
	(121,'benf\\neo\\elements\\Block','2019-02-07 20:46:38','2019-02-07 20:46:38','2019-02-07 20:47:25','b376ac76-7e99-4a09-8335-82105238963b'),
	(122,'benf\\neo\\elements\\Block','2019-02-07 20:47:25','2019-02-07 20:47:25','2019-02-07 23:54:58','33f40345-d5bf-462f-9c75-b115ec3f6040'),
	(123,'benf\\neo\\elements\\Block','2019-02-07 20:47:25','2019-02-07 20:47:25','2019-02-07 23:54:58','74fc5deb-70fa-4e21-8a55-f2050402fac5'),
	(124,'benf\\neo\\elements\\Block','2019-02-07 20:47:25','2019-02-07 20:47:25','2019-02-07 23:54:58','d873512b-1c78-4e55-a9fe-1fc65174e93c'),
	(125,'benf\\neo\\elements\\Block','2019-02-07 20:47:25','2019-02-07 20:47:25','2019-02-07 23:54:58','90bb586c-bba4-49ea-99d1-909b386a3fab'),
	(126,'benf\\neo\\elements\\Block','2019-02-07 20:47:25','2019-02-07 20:47:25','2019-02-07 23:54:58','8d9b638f-7186-4e8e-947a-b1b5fbda2ca8'),
	(127,'benf\\neo\\elements\\Block','2019-02-07 20:47:25','2019-02-07 20:47:25','2019-02-07 23:54:58','74b15059-1f5f-4e57-9d36-5460e8d5f2da'),
	(128,'benf\\neo\\elements\\Block','2019-02-07 20:47:25','2019-02-07 20:47:25','2019-02-07 23:54:58','8ebfbf76-0718-4515-afa4-72e283d35a9b'),
	(129,'benf\\neo\\elements\\Block','2019-02-07 20:47:25','2019-02-07 20:47:25','2019-02-07 23:54:58','d93f8454-3626-4555-919c-3221c78419fe'),
	(130,'benf\\neo\\elements\\Block','2019-02-07 20:47:25','2019-02-07 20:47:25','2019-02-07 23:54:58','daae633c-92ac-481c-b5ba-921a65442ece'),
	(131,'benf\\neo\\elements\\Block','2019-02-07 20:47:25','2019-02-07 20:47:25','2019-02-07 23:54:58','551c4823-522d-4ad7-8342-5c2cb5a68f2e'),
	(132,'benf\\neo\\elements\\Block','2019-02-07 20:47:25','2019-02-07 20:47:25','2019-02-07 23:54:58','98c911d7-d928-4c78-a519-af7e4a635706'),
	(133,'benf\\neo\\elements\\Block','2019-02-07 23:54:58','2019-02-07 23:54:58','2019-02-08 20:03:35','61883344-5124-4a5f-9f7f-66f2391457ae'),
	(134,'benf\\neo\\elements\\Block','2019-02-07 23:54:58','2019-02-07 23:54:58','2019-02-08 20:03:35','0d1f4eea-f3eb-42f3-a107-c3b9b4ac6b96'),
	(135,'benf\\neo\\elements\\Block','2019-02-07 23:54:58','2019-02-07 23:54:58','2019-02-08 20:03:35','02a691e5-98f9-4474-b18b-0c9beaae8520'),
	(136,'benf\\neo\\elements\\Block','2019-02-07 23:54:58','2019-02-07 23:54:58','2019-02-08 20:03:35','32fa6214-5a51-45e2-a33a-77fbfa768483'),
	(137,'benf\\neo\\elements\\Block','2019-02-07 23:54:58','2019-02-07 23:54:58','2019-02-08 20:03:35','3e58caa2-2835-48e5-acfb-be8b300f1e7e'),
	(138,'benf\\neo\\elements\\Block','2019-02-07 23:54:58','2019-02-07 23:54:58','2019-02-08 20:03:35','ec016599-e809-4d83-8be0-8416d1e39a12'),
	(139,'benf\\neo\\elements\\Block','2019-02-07 23:54:58','2019-02-07 23:54:58','2019-02-08 20:03:35','f722482b-758f-4bd4-80c8-b77acc4e4e5b'),
	(140,'benf\\neo\\elements\\Block','2019-02-07 23:54:58','2019-02-07 23:54:58','2019-02-08 20:03:35','bcee62d4-ee53-4b99-afd0-a25e820f7a0f'),
	(141,'benf\\neo\\elements\\Block','2019-02-07 23:54:58','2019-02-07 23:54:58','2019-02-08 20:03:35','bdcc51e5-aa68-467d-b50f-d9443c0cf8d8'),
	(142,'benf\\neo\\elements\\Block','2019-02-07 23:54:58','2019-02-07 23:54:58','2019-02-08 20:03:35','143a2221-f2d4-46f7-8a2b-ad039ed3e7a9'),
	(143,'benf\\neo\\elements\\Block','2019-02-07 23:54:58','2019-02-07 23:54:58','2019-02-08 20:03:35','eeb93900-1e30-4b1d-8fef-b684796d6d64'),
	(144,'benf\\neo\\elements\\Block','2019-02-08 20:03:35','2019-02-08 20:03:35',NULL,'8f5eee48-0ea6-491e-a124-cea6bf2bfa6a'),
	(145,'benf\\neo\\elements\\Block','2019-02-08 20:03:35','2019-02-08 20:03:35',NULL,'b6331323-6108-4dc4-8cf8-245ea2f50ffe'),
	(146,'benf\\neo\\elements\\Block','2019-02-08 20:03:35','2019-02-08 20:03:35',NULL,'7f34c642-f6b0-4a99-a817-83fcd9346c13'),
	(147,'benf\\neo\\elements\\Block','2019-02-08 20:03:35','2019-02-08 20:03:35',NULL,'83227d32-e041-4c32-acf9-886423874b66'),
	(148,'benf\\neo\\elements\\Block','2019-02-08 20:03:35','2019-02-08 20:03:35',NULL,'c3457c11-e07d-4619-aad5-98c971f7cd97'),
	(149,'benf\\neo\\elements\\Block','2019-02-08 20:03:35','2019-02-08 20:03:35',NULL,'4fd997cb-2fea-4d36-a97f-6a98c2ed29a9'),
	(150,'benf\\neo\\elements\\Block','2019-02-08 20:03:35','2019-02-08 20:03:35',NULL,'702443db-c1a2-4bba-8c1a-c97d7df66982'),
	(151,'benf\\neo\\elements\\Block','2019-02-08 20:03:35','2019-02-08 20:03:35',NULL,'d63c6765-25fd-4cd8-b0c7-1362c807f8be'),
	(152,'benf\\neo\\elements\\Block','2019-02-08 20:03:35','2019-02-08 20:03:35',NULL,'10a05eb3-d758-4a84-881d-39942c411a82'),
	(153,'benf\\neo\\elements\\Block','2019-02-08 20:03:35','2019-02-08 20:03:35',NULL,'548b7cc1-ee68-4735-a486-9e6036251d1d'),
	(154,'benf\\neo\\elements\\Block','2019-02-08 20:03:35','2019-02-08 20:03:35',NULL,'7b615efd-bdf4-4cad-85e7-5c7e0c225fee'),
	(155,'benf\\neo\\elements\\Block','2019-02-08 20:03:35','2019-02-08 20:03:35',NULL,'05d7706e-eccb-442f-9f4f-ff2ab6789996');

/*!40000 ALTER TABLE `craft_fieldlayouts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fieldlayouttabs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fieldlayouttabs`;

CREATE TABLE `craft_fieldlayouttabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `layoutId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_fieldlayouttabs_sortOrder_idx` (`sortOrder`),
  KEY `craft_fieldlayouttabs_layoutId_idx` (`layoutId`),
  CONSTRAINT `craft_fieldlayouttabs_layoutId_fk` FOREIGN KEY (`layoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_fieldlayouttabs` WRITE;
/*!40000 ALTER TABLE `craft_fieldlayouttabs` DISABLE KEYS */;

INSERT INTO `craft_fieldlayouttabs` (`id`, `layoutId`, `name`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'Content',1,'2019-01-21 23:48:56','2019-01-21 23:48:56','351bee8d-804b-40e0-8ed4-9418312ead74'),
	(2,2,'Content',1,'2019-01-21 23:48:57','2019-01-21 23:48:57','929e2d58-394e-42b3-8b85-e4f402b006e6'),
	(3,3,'Content',1,'2019-01-21 23:48:58','2019-01-21 23:48:58','e9f74029-93d5-4535-ba28-e3442c766bfe'),
	(4,4,'Content',1,'2019-01-21 23:49:01','2019-01-21 23:49:01','1294dce3-181c-4d3e-98b2-6720f0e06d8e'),
	(5,5,'Content',1,'2019-01-21 23:49:02','2019-01-21 23:49:02','42ed3cb8-dc7c-419b-8f5b-d03a360b9739'),
	(6,6,'Content',1,'2019-01-21 23:49:03','2019-01-21 23:49:03','f8a15aee-83c3-461a-8864-beb580ecf1fe'),
	(7,7,'Content',1,'2019-01-21 23:49:05','2019-01-21 23:49:05','83c35661-2c8e-4d8a-8e9d-1d6a1d3ce0dc'),
	(8,8,'Content',1,'2019-01-21 23:49:06','2019-01-21 23:49:06','9c8aae06-2f49-46ae-a16b-bfd40da901ae'),
	(9,9,'Content',1,'2019-01-21 23:49:06','2019-01-21 23:49:06','06307adf-c919-421f-ae74-8ced6516b667'),
	(12,12,'Content',1,'2019-01-21 23:49:13','2019-01-21 23:49:13','881c5f15-3420-4db8-aafe-fed494af05ad'),
	(13,13,'Content',1,'2019-01-22 15:42:57','2019-01-22 15:42:57','44b3f29c-b5d9-4afe-8187-0f6dd17dd94c'),
	(14,13,'Block Options',2,'2019-01-22 15:42:57','2019-01-22 15:42:57','e6457383-8794-441d-b67b-c6fd2f94f1ef'),
	(15,14,'Banner',1,'2019-01-22 15:42:57','2019-01-22 15:42:57','a3ddb6c9-2f98-43ae-bcff-1f294c949808'),
	(16,15,'Content',1,'2019-01-22 15:42:57','2019-01-22 15:42:57','07277ec4-01bb-464a-96d8-fa299f3bc86a'),
	(17,16,'Content',1,'2019-01-22 15:42:57','2019-01-22 15:42:57','7f69af6a-be72-4503-8279-cebfe5416103'),
	(18,16,'Block Options',2,'2019-01-22 15:42:57','2019-01-22 15:42:57','cea4428b-6763-4abe-bf20-d07606027f36'),
	(19,17,'Content',1,'2019-01-22 15:42:57','2019-01-22 15:42:57','f03061dc-5d44-4821-a1cb-c6eb708b79b9'),
	(20,17,'Block Options',2,'2019-01-22 15:42:57','2019-01-22 15:42:57','169fbf30-052c-4361-80f7-2588dfa66d85'),
	(21,18,'Content',1,'2019-01-22 15:47:51','2019-01-22 15:47:51','0d69f237-699e-42d6-9db7-88c4a7df4118'),
	(22,18,'Block Options',2,'2019-01-22 15:47:51','2019-01-22 15:47:51','e02cae74-b915-4aa9-88bd-e8f8d7b67c24'),
	(23,19,'Content',1,'2019-01-22 15:47:51','2019-01-22 15:47:51','0905cf12-d10b-4529-88ac-00fd25753be9'),
	(24,19,'Block Options',2,'2019-01-22 15:47:51','2019-01-22 15:47:51','04fbcad4-de48-4c7f-90e3-4cca02088a7a'),
	(25,20,'Content',1,'2019-01-22 15:47:51','2019-01-22 15:47:51','e6788b92-f902-442f-bc1e-8b10d54bcfdb'),
	(26,21,'Banner',1,'2019-01-22 15:47:51','2019-01-22 15:47:51','188ae4da-6898-40a6-a586-ebc32c213466'),
	(27,22,'Content',1,'2019-01-22 15:47:52','2019-01-22 15:47:52','85a1babb-3f64-4435-aa8f-04459fa5bac2'),
	(28,23,'Content',1,'2019-01-22 15:47:52','2019-01-22 15:47:52','ea71a837-db61-48b5-b7df-d7226a8e1f56'),
	(29,23,'Block Options',2,'2019-01-22 15:47:52','2019-01-22 15:47:52','cf064ed7-93f5-4a97-adaa-0cddebc0ab6a'),
	(30,24,'Content',1,'2019-01-22 15:47:52','2019-01-22 15:47:52','99596aac-f607-473c-a8ef-0dd40362d220'),
	(31,24,'Block Options',2,'2019-01-22 15:47:52','2019-01-22 15:47:52','4ec585b3-aa42-4d7b-b2cb-084241db8e73'),
	(43,25,'Content',1,'2019-01-22 15:55:26','2019-01-22 15:55:26','3e5eab33-9054-481a-b280-4690a10debd0'),
	(44,25,'Block Options',2,'2019-01-22 15:55:26','2019-01-22 15:55:26','8b58132c-71f4-4498-ae39-b5eb0647a1b3'),
	(45,26,'Content',1,'2019-01-22 15:55:26','2019-01-22 15:55:26','43ad7233-3e56-472f-9fd1-ca9f19863335'),
	(46,26,'Block Options',2,'2019-01-22 15:55:26','2019-01-22 15:55:26','18ec2866-c082-4485-8311-fa4f5c9c86e1'),
	(47,27,'Content',1,'2019-01-22 15:55:26','2019-01-22 15:55:26','68bbb234-5f41-455d-bfda-c7b647346c71'),
	(48,28,'Banner',1,'2019-01-22 15:55:27','2019-01-22 15:55:27','2defc5fa-f890-4512-b0bf-db2ea35cdef6'),
	(49,29,'Content',1,'2019-01-22 15:55:27','2019-01-22 15:55:27','09dee9b8-1bc7-4557-a679-3df5dbe319b4'),
	(50,30,'Content',1,'2019-01-22 15:55:27','2019-01-22 15:55:27','5ea73ed8-28a3-4a46-b1e9-f73bdf6ec765'),
	(51,30,'Block Options',2,'2019-01-22 15:55:27','2019-01-22 15:55:27','7f1e9641-5390-4cdd-9089-b7dfa7ea0eb7'),
	(52,31,'Content',1,'2019-01-22 15:55:27','2019-01-22 15:55:27','aa83423e-12c3-4e99-9d61-a9fd82eb7374'),
	(53,31,'Block Options',2,'2019-01-22 15:55:27','2019-01-22 15:55:27','343880e9-c05a-4c09-bb80-2bd04fe2a8f7'),
	(55,32,'Content',1,'2019-01-22 15:56:26','2019-01-22 15:56:26','bc1718f4-b21b-4fcd-8178-10bf3aaf0ed6'),
	(56,32,'Block Options',2,'2019-01-22 15:56:26','2019-01-22 15:56:26','498256c4-8583-4d61-8995-b1952c2b0515'),
	(57,33,'Content',1,'2019-01-22 15:56:26','2019-01-22 15:56:26','d231751b-7f98-45fc-bbf8-661ce1041218'),
	(58,33,'Block Options',2,'2019-01-22 15:56:26','2019-01-22 15:56:26','cea5d586-c73b-46c9-b8f3-4a9738bbf163'),
	(59,34,'Content',1,'2019-01-22 15:56:26','2019-01-22 15:56:26','915b6df6-72d0-4bee-8fb0-a7a83e10aa99'),
	(60,35,'Banner',1,'2019-01-22 15:56:26','2019-01-22 15:56:26','a9fdfe67-a0a0-4688-9381-ddc203b2f75a'),
	(61,36,'Content',1,'2019-01-22 15:56:26','2019-01-22 15:56:26','4f267cc6-e6a9-4fbc-8d36-912cb0425aa2'),
	(62,37,'Content',1,'2019-01-22 15:56:26','2019-01-22 15:56:26','afcd04a1-f832-4f80-ab14-1bc35bbf0a81'),
	(63,37,'Block Options',2,'2019-01-22 15:56:26','2019-01-22 15:56:26','7841156c-b65a-4fb6-a603-5a253a747fb8'),
	(64,38,'Content',1,'2019-01-22 15:56:27','2019-01-22 15:56:27','2c49803e-f9f1-4075-bb07-950b79c163cc'),
	(65,38,'Block Options',2,'2019-01-22 15:56:27','2019-01-22 15:56:27','9441112f-d9aa-4c54-9510-5d31c182ac88'),
	(66,39,'Content',1,'2019-01-22 15:56:27','2019-01-22 15:56:27','f7a48b26-03cd-4fb6-b6d5-453970e6614d'),
	(67,40,'Content',1,'2019-01-22 15:59:52','2019-01-22 15:59:52','7c6496f7-2178-47bd-a168-da371a60416d'),
	(68,40,'Block Options',2,'2019-01-22 15:59:52','2019-01-22 15:59:52','47659e0b-2e81-45df-b2ae-b457eba8b812'),
	(69,41,'Content',1,'2019-01-22 15:59:52','2019-01-22 15:59:52','b12709b2-37ce-467d-aa80-9934d9f77d61'),
	(70,41,'Block Options',2,'2019-01-22 15:59:52','2019-01-22 15:59:52','b819c765-e3bc-4c0e-a44e-51a3e414fe5e'),
	(71,42,'Content',1,'2019-01-22 15:59:52','2019-01-22 15:59:52','78633cc4-9ed3-472e-80fd-7e991ea78eb4'),
	(72,43,'Banner',1,'2019-01-22 15:59:52','2019-01-22 15:59:52','2ec9f42b-29ca-452c-a746-338d572ab0c2'),
	(73,44,'Content',1,'2019-01-22 15:59:52','2019-01-22 15:59:52','2037ae39-00df-4a2d-9e79-a3216d87ebef'),
	(74,45,'Content',1,'2019-01-22 15:59:52','2019-01-22 15:59:52','9c7db0dc-eeb9-4ee9-845e-ff466aa1cce8'),
	(75,45,'Block Options',2,'2019-01-22 15:59:52','2019-01-22 15:59:52','2d9a8d03-48bc-42c6-9435-5e99c41c7db4'),
	(76,46,'Content',1,'2019-01-22 15:59:52','2019-01-22 15:59:52','69f4030e-11e9-442d-8ddd-d7ea775c01d1'),
	(77,46,'Block Options',2,'2019-01-22 15:59:52','2019-01-22 15:59:52','ac4fb109-673a-4529-b7a8-a24e92fff161'),
	(78,47,'Content',1,'2019-01-22 15:59:52','2019-01-22 15:59:52','206c1548-3413-40bb-96b0-4dfa1e0d167b'),
	(79,48,'Blogs',1,'2019-01-22 15:59:52','2019-01-22 15:59:52','f8e28169-4235-4550-a377-2c1525a4a915'),
	(80,49,'Content',1,'2019-01-22 16:00:55','2019-01-22 16:00:55','f5784bd5-102b-4163-b9c7-19a51729ae97'),
	(81,49,'Block Options',2,'2019-01-22 16:00:55','2019-01-22 16:00:55','d7a799da-5c90-4999-8a5a-b8ae6d0efbe4'),
	(82,50,'Content',1,'2019-01-22 16:00:55','2019-01-22 16:00:55','a4944f81-c364-46dc-bfaa-b0a435b02c90'),
	(83,50,'Block Options',2,'2019-01-22 16:00:55','2019-01-22 16:00:55','2f80162b-0478-4396-bab2-550d100edb97'),
	(84,51,'Content',1,'2019-01-22 16:00:55','2019-01-22 16:00:55','bcee6c8f-0b4e-4fe7-9e35-377c1633e559'),
	(85,52,'Banner',1,'2019-01-22 16:00:56','2019-01-22 16:00:56','427ad1c3-2878-468c-aabd-4d6f2236065f'),
	(86,53,'Content',1,'2019-01-22 16:00:56','2019-01-22 16:00:56','4f20a534-a781-4739-8852-579f1b7f77aa'),
	(87,54,'Content',1,'2019-01-22 16:00:56','2019-01-22 16:00:56','93769fd7-a4a9-4940-a949-688ac99190b7'),
	(88,54,'Block Options',2,'2019-01-22 16:00:56','2019-01-22 16:00:56','669a8b4c-baf2-4c19-b680-abcc7c3a282e'),
	(89,55,'Content',1,'2019-01-22 16:00:56','2019-01-22 16:00:56','1f35445b-ac7c-4ca7-9da0-80f9e80f6189'),
	(90,55,'Block Options',2,'2019-01-22 16:00:56','2019-01-22 16:00:56','ea01c39f-1339-42d1-8ab2-ff3ab40a8e29'),
	(91,56,'Content',1,'2019-01-22 16:00:56','2019-01-22 16:00:56','5683fe84-5615-4a14-a73e-6fb3fee48771'),
	(92,57,'Blogs',1,'2019-01-22 16:00:56','2019-01-22 16:00:56','fe4ed858-3316-4267-8f6f-ff8e586e63b5'),
	(108,58,'Content',1,'2019-01-22 16:06:18','2019-01-22 16:06:18','1cfba32c-3067-4877-9069-462afdd02c81'),
	(109,58,'Block Options',2,'2019-01-22 16:06:18','2019-01-22 16:06:18','144bbf42-2ff3-4df0-ad8d-930f55f76aa5'),
	(110,59,'Content',1,'2019-01-22 16:06:18','2019-01-22 16:06:18','c2615add-48b7-4cec-9207-1dcfe14806a1'),
	(111,59,'Block Options',2,'2019-01-22 16:06:18','2019-01-22 16:06:18','84fb5682-d368-4d98-9f63-aa2f021f6708'),
	(112,60,'Content',1,'2019-01-22 16:06:19','2019-01-22 16:06:19','5239fda8-01c9-422b-8c3a-1406e76b58a6'),
	(113,61,'Banner',1,'2019-01-22 16:06:19','2019-01-22 16:06:19','666a8d4a-2f8e-43ef-9088-4b4846b44a21'),
	(114,62,'Content',1,'2019-01-22 16:06:19','2019-01-22 16:06:19','f16ecfe0-9484-45b1-bb57-b391c9453a69'),
	(115,63,'Content',1,'2019-01-22 16:06:19','2019-01-22 16:06:19','f5d357ef-6e33-4f7f-acb9-de639d560a37'),
	(116,63,'Block Options',2,'2019-01-22 16:06:19','2019-01-22 16:06:19','d7c9b320-1bc1-4a20-9d5a-26a39a62054a'),
	(117,64,'Content',1,'2019-01-22 16:06:19','2019-01-22 16:06:19','be919727-37df-41b3-9edc-5f8b14709d83'),
	(118,64,'Block Options',2,'2019-01-22 16:06:19','2019-01-22 16:06:19','32d03c03-09a1-4716-b28e-7ea9f68c95b0'),
	(119,65,'Content',1,'2019-01-22 16:06:19','2019-01-22 16:06:19','819f7969-b0bb-4a60-9966-bde5106b67ff'),
	(120,66,'Blogs',1,'2019-01-22 16:06:19','2019-01-22 16:06:19','cbc9e3e4-1b13-4132-bdd3-4a5837fdbf5f'),
	(121,67,'Content',1,'2019-01-22 16:06:19','2019-01-22 16:06:19','0d74ff05-9641-4c03-a7c4-dc4cae92031e'),
	(122,68,'Content',1,'2019-01-22 16:06:19','2019-01-22 16:06:19','1c6130d9-2353-4e62-b3c0-6557bbb720dc'),
	(123,69,'Content',1,'2019-01-22 16:06:20','2019-01-22 16:06:20','f2ba8aef-7c21-46d3-887a-f78c08d6fc14'),
	(140,70,'Content',1,'2019-01-22 16:09:22','2019-01-22 16:09:22','c9a7e54e-b81f-4519-8953-e4af4abb9fe3'),
	(141,70,'Block Options',2,'2019-01-22 16:09:22','2019-01-22 16:09:22','212ecf36-0a63-46a8-8014-b267dcf49ee6'),
	(142,71,'Content',1,'2019-01-22 16:09:23','2019-01-22 16:09:23','d18339ea-ed41-40d1-94bf-418c99335007'),
	(143,71,'Block Options',2,'2019-01-22 16:09:23','2019-01-22 16:09:23','76e852de-cdb1-4613-a55f-6f0104f71a53'),
	(144,72,'Content',1,'2019-01-22 16:09:23','2019-01-22 16:09:23','6d2cf00f-4909-4628-8fee-d3ff0341f8bf'),
	(145,73,'Banner',1,'2019-01-22 16:09:23','2019-01-22 16:09:23','d43a79b2-72a6-479b-afe4-368e98f5062e'),
	(146,74,'Content',1,'2019-01-22 16:09:23','2019-01-22 16:09:23','e4534ffe-caf9-4611-a3ef-639a6b7a8ea5'),
	(147,75,'Content',1,'2019-01-22 16:09:23','2019-01-22 16:09:23','9ecf8339-fd51-447d-b5b1-8dac861f8be1'),
	(148,75,'Block Options',2,'2019-01-22 16:09:23','2019-01-22 16:09:23','1b562779-7dcf-462e-b18b-17154552008d'),
	(149,76,'Content',1,'2019-01-22 16:09:23','2019-01-22 16:09:23','d87cdaea-6230-48f3-a22d-8abae111fc6d'),
	(150,76,'Block Options',2,'2019-01-22 16:09:23','2019-01-22 16:09:23','45138911-44dd-4cc4-a4fd-3eb8a166fa1f'),
	(151,77,'Content',1,'2019-01-22 16:09:23','2019-01-22 16:09:23','8c571814-f4d0-44bf-a3c0-22e14dd40af7'),
	(152,78,'Blogs',1,'2019-01-22 16:09:23','2019-01-22 16:09:23','5cf45de7-ddfa-4325-ac13-8c51de287e54'),
	(153,79,'Content',1,'2019-01-22 16:09:23','2019-01-22 16:09:23','fd3e7874-0b42-4c1a-822d-32af8dcb2cbb'),
	(154,80,'Content',1,'2019-01-22 16:09:23','2019-01-22 16:09:23','f5e407fd-16ed-4205-8417-94f9dd446968'),
	(155,80,'Block Options',2,'2019-01-22 16:09:23','2019-01-22 16:09:23','7957374e-2c7d-4a5e-940a-62c6f5591c5e'),
	(157,82,'Content',1,'2019-01-22 16:10:08','2019-01-22 16:10:08','6e841a90-2a27-4725-8651-3f7def2b7c19'),
	(158,83,'Common',1,'2019-01-22 16:11:58','2019-01-22 16:11:58','68c6dc6d-c943-4407-8317-7effcdeee22f'),
	(159,83,'Page Options',2,'2019-01-22 16:11:58','2019-01-22 16:11:58','051bd061-a303-4e6c-b3a1-af4e55b6d990'),
	(163,86,'Content',1,'2019-01-30 21:44:19','2019-01-30 21:44:19','920b1882-4ea0-4bbb-b8c4-a46061209df5'),
	(164,81,'Social',1,'2019-01-30 21:44:19','2019-01-30 21:44:19','49b4d118-b28f-4d69-b750-b12a8189e2b8'),
	(165,11,'Content',1,'2019-01-30 23:31:54','2019-01-30 23:31:54','a4004a5d-bc83-4cc4-aae6-65d3986e5ea1'),
	(166,85,'Content',1,'2019-01-30 23:37:21','2019-01-30 23:37:21','ba68cca3-30a7-412a-9066-b09fa355b7fa'),
	(168,87,'Content',1,'2019-01-30 23:39:28','2019-01-30 23:39:28','1ad0a684-63b9-488d-b87d-79115368beec'),
	(169,87,'Block Options',2,'2019-01-30 23:39:28','2019-01-30 23:39:28','f02fdff2-74ac-4751-8dbf-e1decfcc0b30'),
	(170,88,'Content',1,'2019-01-30 23:39:28','2019-01-30 23:39:28','d1201e56-c442-4235-8754-06b8755efb64'),
	(171,88,'Block Options',2,'2019-01-30 23:39:28','2019-01-30 23:39:28','f16c743f-1601-4998-a575-99d5439e5126'),
	(172,89,'Content',1,'2019-01-30 23:39:28','2019-01-30 23:39:28','358f83dd-847b-431d-b532-03d7b89fcf21'),
	(173,90,'Banner',1,'2019-01-30 23:39:28','2019-01-30 23:39:28','489c8f9e-ba89-4545-9946-ee96b37bf0f1'),
	(174,91,'Content',1,'2019-01-30 23:39:28','2019-01-30 23:39:28','27302527-1e6c-484a-a455-fb0d85e04185'),
	(175,92,'Content',1,'2019-01-30 23:39:28','2019-01-30 23:39:28','ad10b32a-5a3a-4951-a61e-f84704f4d292'),
	(176,92,'Block Options',2,'2019-01-30 23:39:28','2019-01-30 23:39:28','65d22169-920e-4dcf-b1e8-e7d75d57279a'),
	(177,93,'Content',1,'2019-01-30 23:39:28','2019-01-30 23:39:28','10adfd36-e5f7-4567-ba63-1bf3a217ca26'),
	(178,93,'Block Options',2,'2019-01-30 23:39:28','2019-01-30 23:39:28','dfe2a2e7-0c68-4d93-a6e2-672e3397e217'),
	(179,94,'Content',1,'2019-01-30 23:39:28','2019-01-30 23:39:28','fd8b6178-26f5-4ed5-845b-43e783efeb1f'),
	(180,95,'Blogs',1,'2019-01-30 23:39:28','2019-01-30 23:39:28','0971c22c-ce6d-4e66-8fc7-5281bdef4eb7'),
	(181,96,'Content',1,'2019-01-30 23:39:28','2019-01-30 23:39:28','8ae253c3-4f63-4898-b8f7-dda02cf65c04'),
	(182,97,'Content',1,'2019-01-30 23:39:28','2019-01-30 23:39:28','b3500614-7e5d-4e9a-ad04-1c6f32829ddb'),
	(183,97,'Block Options',2,'2019-01-30 23:39:28','2019-01-30 23:39:28','0fb60198-ea6d-4719-98f4-bb855804d26e'),
	(185,10,'Content',1,'2019-01-31 16:04:10','2019-01-31 16:04:10','21a99669-23e1-42a9-a16e-a0b4c8335da5'),
	(186,99,'Banner',1,'2019-01-31 16:40:23','2019-01-31 16:40:23','00bf3edd-621b-42bc-9641-4f11a458c3f4'),
	(187,100,'Content',1,'2019-01-31 16:40:23','2019-01-31 16:40:23','2bd97ce1-c66a-4cf5-8ff7-6bc309cf4c7c'),
	(188,100,'Block Options',2,'2019-01-31 16:40:23','2019-01-31 16:40:23','2300227d-ce4d-4ee2-9b51-1a9111d125da'),
	(189,101,'Content',1,'2019-01-31 16:40:23','2019-01-31 16:40:23','6d41db38-98a3-47a4-8b15-0929797cd1b6'),
	(190,101,'Block Options',2,'2019-01-31 16:40:23','2019-01-31 16:40:23','b4ac336b-2e0e-41a8-9a4c-5b0bef1ea72c'),
	(191,102,'Content',1,'2019-01-31 16:40:23','2019-01-31 16:40:23','bc1bdc44-8fd5-4c4f-ad21-5b8adb980da1'),
	(192,103,'Content',1,'2019-01-31 16:40:23','2019-01-31 16:40:23','aa6dfa99-b2c4-4dab-91f1-4888aad1211f'),
	(193,104,'Content',1,'2019-01-31 16:40:23','2019-01-31 16:40:23','ce1fe019-045d-4dd7-b12a-67f446b2301f'),
	(194,104,'Block Options',2,'2019-01-31 16:40:23','2019-01-31 16:40:23','090f9177-68a4-4e01-9d18-8fc7fc5715ac'),
	(195,105,'Content',1,'2019-01-31 16:40:23','2019-01-31 16:40:23','1465457b-cb77-4883-b1be-7abfd38f9930'),
	(196,105,'Block Options',2,'2019-01-31 16:40:23','2019-01-31 16:40:23','87b8c077-1edb-4fed-be32-3f5ee3632c6b'),
	(197,106,'Content',1,'2019-01-31 16:40:23','2019-01-31 16:40:23','ea380e62-b528-44bc-826f-5d36e7cba63c'),
	(198,107,'Blogs',1,'2019-01-31 16:40:23','2019-01-31 16:40:23','3961c0fc-614f-4ffd-9692-b6651fc55018'),
	(199,108,'Content',1,'2019-01-31 16:40:23','2019-01-31 16:40:23','b464f240-73d8-4366-9748-e82ab9bfb642'),
	(200,109,'Content',1,'2019-01-31 16:40:23','2019-01-31 16:40:23','b55faa66-a071-48ad-a90f-e51f93c7483b'),
	(201,109,'Block Options',2,'2019-01-31 16:40:23','2019-01-31 16:40:23','7c0875ba-a301-44ed-9ad2-553168914dfa'),
	(202,84,'Content',1,'2019-01-31 16:43:11','2019-01-31 16:43:11','7f0cd8f7-d6ee-4f1f-8488-1db77811cf9f'),
	(203,110,'Content',1,'2019-01-31 20:56:19','2019-01-31 20:56:19','6317760d-7594-4939-bc23-3be587e7133b'),
	(204,98,'Contact',1,'2019-02-05 18:46:06','2019-02-05 18:46:06','93a15b32-752c-44c6-8a8b-a4692ad71f68'),
	(205,111,'Banner',1,'2019-02-07 20:46:38','2019-02-07 20:46:38','c21d74d8-2a7d-4d95-9a6b-430a278452c5'),
	(206,112,'Content',1,'2019-02-07 20:46:38','2019-02-07 20:46:38','5bde657d-d20e-4d15-add4-dd0546d541d2'),
	(207,112,'Block Options',2,'2019-02-07 20:46:38','2019-02-07 20:46:38','a7711d2c-5005-4f26-9686-daf2614ffb9f'),
	(208,113,'Content',1,'2019-02-07 20:46:38','2019-02-07 20:46:38','34244581-1d43-420e-8e86-1ed116ad7bc5'),
	(209,113,'Block Options',2,'2019-02-07 20:46:38','2019-02-07 20:46:38','9865439c-fdb1-48aa-af21-0f286b27a6b2'),
	(210,114,'Content',1,'2019-02-07 20:46:38','2019-02-07 20:46:38','d27d8ceb-181b-49b6-83fb-4b20b45de310'),
	(211,115,'Content',1,'2019-02-07 20:46:38','2019-02-07 20:46:38','50e93f67-4357-4892-a6f3-ca987a6d4a4f'),
	(212,116,'Content',1,'2019-02-07 20:46:38','2019-02-07 20:46:38','06ff1fbd-0610-40f5-a8cf-738df754e27d'),
	(213,116,'Block Options',2,'2019-02-07 20:46:38','2019-02-07 20:46:38','10768ab8-34f0-4caf-b59c-afb4b86d62a2'),
	(214,117,'Content',1,'2019-02-07 20:46:38','2019-02-07 20:46:38','d5c077a8-2bf7-4c92-a2b1-9e56ca9da949'),
	(215,117,'Block Options',2,'2019-02-07 20:46:38','2019-02-07 20:46:38','9fe9974b-24e6-479c-bc0c-e071ee479431'),
	(216,118,'Content',1,'2019-02-07 20:46:38','2019-02-07 20:46:38','5b08006e-4d07-4ba3-9f03-c165a82476cd'),
	(217,119,'Blogs',1,'2019-02-07 20:46:38','2019-02-07 20:46:38','d20b64b5-8c13-4a17-ab27-04d5b37dee2d'),
	(218,120,'Content',1,'2019-02-07 20:46:38','2019-02-07 20:46:38','3c2bb949-0b94-4b0c-b678-541287aeb538'),
	(219,121,'Content',1,'2019-02-07 20:46:38','2019-02-07 20:46:38','2ed70501-c5f3-464d-a1f1-6f972ef355e2'),
	(220,121,'Block Options',2,'2019-02-07 20:46:38','2019-02-07 20:46:38','d1cc9f3f-a192-471b-aa90-e6571c2429b1'),
	(221,122,'Banner',1,'2019-02-07 20:47:25','2019-02-07 20:47:25','043b4511-58b2-4e7b-b2a0-39e01bfae846'),
	(222,123,'Content',1,'2019-02-07 20:47:25','2019-02-07 20:47:25','cf759c3a-118e-430a-846d-391703691f64'),
	(223,123,'Block Options',2,'2019-02-07 20:47:25','2019-02-07 20:47:25','13be053b-acdb-4fca-9afc-71052c29fb42'),
	(224,124,'Content',1,'2019-02-07 20:47:25','2019-02-07 20:47:25','d099083a-9336-470d-aea9-8288eb8c9eb8'),
	(225,124,'Block Options',2,'2019-02-07 20:47:25','2019-02-07 20:47:25','efc7832c-e1c6-4273-a155-e55e7d17d756'),
	(226,125,'Content',1,'2019-02-07 20:47:25','2019-02-07 20:47:25','6164c8db-602e-4a23-bc56-7d3dc20c83ab'),
	(227,126,'Content',1,'2019-02-07 20:47:25','2019-02-07 20:47:25','bb9fb9a6-bf78-4e02-88c9-56f1754da5d2'),
	(228,127,'Content',1,'2019-02-07 20:47:25','2019-02-07 20:47:25','b63e5e23-fdb9-441b-94a3-1ee94d71d621'),
	(229,127,'Block Options',2,'2019-02-07 20:47:25','2019-02-07 20:47:25','92c0a4f1-fb27-4c85-8c53-49a92c5877ef'),
	(230,128,'Content',1,'2019-02-07 20:47:25','2019-02-07 20:47:25','7ef29844-dfd9-4f1f-9252-5539a0c228d9'),
	(231,128,'Block Options',2,'2019-02-07 20:47:25','2019-02-07 20:47:25','109fb96d-9cdb-4d44-8b96-50518df3ee1a'),
	(232,129,'Content',1,'2019-02-07 20:47:25','2019-02-07 20:47:25','5ac2a6f3-74a2-4bf9-9799-dc6d4f330662'),
	(233,130,'Blogs',1,'2019-02-07 20:47:25','2019-02-07 20:47:25','f98ed901-f274-4667-9517-486333568b5e'),
	(234,131,'Content',1,'2019-02-07 20:47:25','2019-02-07 20:47:25','d485d363-e2ba-48ef-abff-ac650f0a009a'),
	(235,132,'Content',1,'2019-02-07 20:47:25','2019-02-07 20:47:25','b89dae3f-4261-4655-aa75-0dfaf47d9c9b'),
	(236,132,'Block Options',2,'2019-02-07 20:47:25','2019-02-07 20:47:25','70911ced-b939-4007-93a3-260244586cd7'),
	(237,133,'Banner',1,'2019-02-07 23:54:58','2019-02-07 23:54:58','d2b1ca48-4604-4d2b-a31b-00aa3410b060'),
	(238,134,'Content',1,'2019-02-07 23:54:58','2019-02-07 23:54:58','87964f07-0b86-4669-8f01-fcaee1cf65e0'),
	(239,134,'Block Options',2,'2019-02-07 23:54:58','2019-02-07 23:54:58','f6d948a1-8843-43a3-baf7-f792be039aa7'),
	(240,135,'Content',1,'2019-02-07 23:54:58','2019-02-07 23:54:58','0c0af440-b1f1-4746-b83a-7c7bddeea7d1'),
	(241,135,'Block Options',2,'2019-02-07 23:54:58','2019-02-07 23:54:58','ebd2ae61-3141-4c88-9a26-cfeec42e8246'),
	(242,136,'Content',1,'2019-02-07 23:54:58','2019-02-07 23:54:58','f13cd280-5028-4fcf-bebb-96bfae7dd939'),
	(243,137,'Content',1,'2019-02-07 23:54:58','2019-02-07 23:54:58','9757e38f-66f8-495e-be73-5f97c4f14463'),
	(244,138,'Content',1,'2019-02-07 23:54:58','2019-02-07 23:54:58','4064bdc6-043e-44a9-bad9-0b0b8799e957'),
	(245,138,'Block Options',2,'2019-02-07 23:54:58','2019-02-07 23:54:58','8e935c68-35b0-4c2e-b496-b2cc0e5dcb91'),
	(246,139,'Content',1,'2019-02-07 23:54:58','2019-02-07 23:54:58','17c524ba-666a-471c-b036-5354aee19237'),
	(247,139,'Block Options',2,'2019-02-07 23:54:58','2019-02-07 23:54:58','fcecfcee-ff4d-4737-bcdb-2ccdebbf0291'),
	(248,140,'Content',1,'2019-02-07 23:54:58','2019-02-07 23:54:58','5c920887-a3ad-4352-a2bb-5be6e3ddb69a'),
	(249,141,'Blogs',1,'2019-02-07 23:54:58','2019-02-07 23:54:58','5ffaaa05-9f2d-4e29-9e94-88ffe19bc1c5'),
	(250,142,'Content',1,'2019-02-07 23:54:58','2019-02-07 23:54:58','d1e14951-d5c0-4cc7-b375-cdcc6e649e19'),
	(251,143,'Content',1,'2019-02-07 23:54:58','2019-02-07 23:54:58','a1932c5e-7f89-4203-a5bd-d5d67b069869'),
	(252,143,'Block Options',2,'2019-02-07 23:54:58','2019-02-07 23:54:58','a671e3d6-31c5-4cd4-b2cf-6e8ff5d62842'),
	(253,144,'Banner',1,'2019-02-08 20:03:35','2019-02-08 20:03:35','b4b00efd-b455-4d3c-a34b-15baedc6cbb0'),
	(254,145,'Content',1,'2019-02-08 20:03:35','2019-02-08 20:03:35','f4418850-c1cd-4368-8d0d-2f9e31fec4f2'),
	(255,145,'Block Options',2,'2019-02-08 20:03:35','2019-02-08 20:03:35','a75ac66d-e971-4a49-b012-0df569b0eba1'),
	(256,146,'Content',1,'2019-02-08 20:03:35','2019-02-08 20:03:35','d819f9a9-7200-4414-a265-73cc239b7ab2'),
	(257,146,'Block Options',2,'2019-02-08 20:03:35','2019-02-08 20:03:35','762081ac-27da-4330-8446-0bb076fb2cb3'),
	(258,147,'Content',1,'2019-02-08 20:03:35','2019-02-08 20:03:35','51c81f39-97be-40c3-82c3-dfc24de46ba4'),
	(259,148,'Content',1,'2019-02-08 20:03:35','2019-02-08 20:03:35','c594c21f-594b-497d-a626-92dfb5ea962c'),
	(260,149,'Content',1,'2019-02-08 20:03:35','2019-02-08 20:03:35','bc9f88a8-1e99-45a0-81c5-b8181c430b14'),
	(261,149,'Block Options',2,'2019-02-08 20:03:35','2019-02-08 20:03:35','37abd5bd-deb6-43ae-ac09-d26023198c7c'),
	(262,150,'Content',1,'2019-02-08 20:03:35','2019-02-08 20:03:35','82d9430b-1e1f-4ae3-871a-43de4f0a230d'),
	(263,150,'Block Options',2,'2019-02-08 20:03:35','2019-02-08 20:03:35','e055dde4-0fe3-43e9-a0d2-a873c3a91e22'),
	(264,151,'Content',1,'2019-02-08 20:03:35','2019-02-08 20:03:35','520201df-e518-4d0c-bf87-04775f5e6392'),
	(265,152,'Blogs',1,'2019-02-08 20:03:35','2019-02-08 20:03:35','8bc46790-533b-4a18-a9cd-029393a2f94c'),
	(266,153,'Content',1,'2019-02-08 20:03:35','2019-02-08 20:03:35','f5b410ca-c63a-4d17-ab2d-9427da6d2efa'),
	(267,154,'Content',1,'2019-02-08 20:03:35','2019-02-08 20:03:35','4fe2c74e-31e5-4c34-bbe6-a8b171aee7c4'),
	(268,154,'Block Options',2,'2019-02-08 20:03:35','2019-02-08 20:03:35','4ee0925f-297c-4c40-a0cd-4408b09bf083'),
	(269,155,'Content',1,'2019-02-08 20:03:35','2019-02-08 20:03:35','3f2f8d14-8b56-4cdd-99ce-541839141afc');

/*!40000 ALTER TABLE `craft_fieldlayouttabs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_fields`;

CREATE TABLE `craft_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(64) NOT NULL,
  `context` varchar(255) NOT NULL DEFAULT 'global',
  `instructions` text,
  `searchable` tinyint(1) NOT NULL DEFAULT '1',
  `translationMethod` varchar(255) NOT NULL DEFAULT 'none',
  `translationKeyFormat` text,
  `type` varchar(255) NOT NULL,
  `settings` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_fields_handle_context_unq_idx` (`handle`,`context`),
  KEY `craft_fields_groupId_idx` (`groupId`),
  KEY `craft_fields_context_idx` (`context`),
  CONSTRAINT `craft_fields_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_fieldgroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_fields` WRITE;
/*!40000 ALTER TABLE `craft_fields` DISABLE KEYS */;

INSERT INTO `craft_fields` (`id`, `groupId`, `name`, `handle`, `context`, `instructions`, `searchable`, `translationMethod`, `translationKeyFormat`, `type`, `settings`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,2,'Background Color','backgroundColor','global','',1,'none',NULL,'craft\\fields\\Color','{\"defaultColor\":\"\"}','2019-01-21 23:48:53','2019-01-21 23:48:53','505d4149-2cbb-4eb1-a6c3-e6dff168aca1'),
	(2,2,'Background Image','backgroundImage','global','',1,'site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"allowedKinds\":null,\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2019-01-21 23:48:53','2019-01-30 21:33:12','e660e5f6-d471-4459-adae-180958bb4dc9'),
	(3,2,'Custom Background','customBackground','global','',1,'none',NULL,'craft\\fields\\RadioButtons','{\"options\":[{\"label\":\"None\",\"value\":\"none\",\"default\":\"\"},{\"label\":\"Color\",\"value\":\"color\",\"default\":\"\"},{\"label\":\"Image\",\"value\":\"image\",\"default\":\"\"}]}','2019-01-21 23:48:53','2019-01-21 23:48:53','c5c4a6a7-7c25-4595-b93a-b084faf2da7c'),
	(4,2,'Margin','margin','global','Standard CSS margin',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"0 5% 0 5%\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"}','2019-01-21 23:48:53','2019-01-21 23:48:53','c36e4ec1-b15f-416b-8688-ede105b9abe0'),
	(5,2,'Padding','padding','global','Standard CSS padding',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"0 5% 0 5%\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"}','2019-01-21 23:48:53','2019-01-21 23:48:53','93751aed-0596-4e0a-ba61-1784d50d4193'),
	(37,3,'Call to Action','callToAction','global','',1,'site',NULL,'craft\\fields\\Entries','{\"limit\":\"1\",\"localizeRelations\":false,\"selectionLabel\":\"\",\"source\":null,\"sources\":[\"section:c56e2508-9559-4291-82c0-546311d3961b\"],\"targetSiteId\":null,\"viewMode\":null}','2019-01-21 23:49:07','2019-01-31 15:32:38','067e4034-f577-41cc-99a6-bea1b7742890'),
	(38,3,'Columns','columns','global','',1,'site',NULL,'verbb\\supertable\\fields\\SuperTableField','{\"minRows\":\"\",\"maxRows\":\"\",\"contentTable\":\"{{%stc_columns}}\",\"localizeBlocks\":false,\"staticField\":\"\",\"columns\":{\"39\":{\"width\":\"\"},\"40\":{\"width\":\"\"}},\"fieldLayout\":\"matrix\",\"selectionLabel\":\"\"}','2019-01-21 23:49:07','2019-01-31 16:04:08','fe88044f-6d76-40a1-b67c-4d1f7a38f62b'),
	(39,NULL,'Column Width','columnWidth','superTableBlockType:6201b01a-ad18-4b80-b177-da1103bc45f6','',1,'none',NULL,'craft\\fields\\Dropdown','{\"options\":[{\"label\":\"17%\",\"value\":\"2\",\"default\":\"\"},{\"label\":\"25%\",\"value\":\"3\",\"default\":\"\"},{\"label\":\"33%\",\"value\":\"4\",\"default\":\"\"},{\"label\":\"50%\",\"value\":\"6\",\"default\":\"1\"},{\"label\":\"66%\",\"value\":\"8\",\"default\":\"\"},{\"label\":\"75%\",\"value\":\"9\",\"default\":\"\"},{\"label\":\"100%\",\"value\":\"12\",\"default\":\"\"}]}','2019-01-21 23:49:08','2019-01-31 16:04:10','63bc51eb-c0c2-47c0-b4b3-e6c1763bde93'),
	(40,NULL,'Content','columnContent','superTableBlockType:6201b01a-ad18-4b80-b177-da1103bc45f6','',1,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Standard.json\",\"purifierConfig\":\"\",\"cleanupHtml\":\"\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"}','2019-01-21 23:49:08','2019-01-31 16:04:10','37924b31-cc82-4c32-a8b8-45cd0cd69c22'),
	(41,3,'Forward Path Grid','forwardPathGrid','global','',1,'site',NULL,'verbb\\supertable\\fields\\SuperTableField','{\"minRows\":\"\",\"maxRows\":\"\",\"contentTable\":\"{{%stc_forwardpathgrid}}\",\"localizeBlocks\":false,\"staticField\":\"\",\"columns\":{\"42\":{\"width\":\"\"},\"43\":{\"width\":\"\"},\"45\":{\"width\":\"\"}},\"fieldLayout\":\"matrix\",\"selectionLabel\":\"\"}','2019-01-21 23:49:08','2019-01-30 23:31:54','36cbdbd4-5e01-40b8-be3a-7568707136d8'),
	(42,NULL,'Image','image','superTableBlockType:9f710e28-f3c7-45c0-b11c-ae72aa1d379b','Please use an SVG image if possible',1,'site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"allowedKinds\":null,\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"large\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2019-01-21 23:49:09','2019-01-30 23:31:54','fe446013-c722-49fc-82a5-c9709a9b7e53'),
	(43,NULL,'Title','fpTitle','superTableBlockType:9f710e28-f3c7-45c0-b11c-ae72aa1d379b','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-01-21 23:49:09','2019-01-30 23:31:54','2ff7f484-0f9a-4725-b5da-ff4242273322'),
	(45,NULL,'Page Link','pageLink','superTableBlockType:9f710e28-f3c7-45c0-b11c-ae72aa1d379b','',1,'site',NULL,'craft\\fields\\Entries','{\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":null,\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2019-01-21 23:49:09','2019-01-30 23:31:54','7fa0e542-86ff-44bb-8d4b-059a8be9dc20'),
	(46,4,'Banner Image','bannerImage','global','',1,'site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"allowedKinds\":null,\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"large\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2019-01-21 23:49:10','2019-01-30 21:33:10','c0656cd9-0e0c-4efd-9970-f91a5d729834'),
	(47,4,'Blog Category ','blogCategory','global','',1,'site',NULL,'craft\\fields\\Categories','{\"branchLimit\":\"\",\"sources\":\"*\",\"source\":\"group:ddd31c2a-2ae9-45cc-8ee2-d43795a7198a\",\"targetSiteId\":null,\"viewMode\":null,\"limit\":null,\"selectionLabel\":\"\",\"localizeRelations\":false}','2019-01-21 23:49:10','2019-01-30 23:33:14','1e4308ab-e0a5-4a36-b76d-4d5d0c1f59af'),
	(48,5,'Form','form','global','',1,'none',NULL,'Solspace\\Freeform\\FieldTypes\\FormFieldType',NULL,'2019-01-21 23:49:10','2019-01-31 16:04:10','d386a8dc-011e-4918-b93b-f21e15c8d4b6'),
	(49,6,'Content','blockContent','global','',1,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Standard.json\",\"purifierConfig\":\"\",\"cleanupHtml\":\"\",\"purifyHtml\":\"\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"}','2019-01-21 23:49:11','2019-01-21 23:49:11','678cb4b8-2b51-42c7-bf90-05465e52f4e8'),
	(50,6,'Entries Link','entriesLink','global','',1,'site',NULL,'craft\\fields\\Entries','{\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":null,\"limit\":\"\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2019-01-21 23:49:11','2019-01-21 23:49:11','2a0cd4a0-dafb-4f02-8c75-23400cc88ecb'),
	(51,6,'Image','blockImage','global','',1,'site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"allowedKinds\":null,\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"large\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2019-01-21 23:49:11','2019-01-30 21:33:14','244270e3-5f4b-4777-a5bc-a8cc022cb5e7'),
	(52,6,'Multiple Images','blockMultipleImages','global','',1,'site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"allowedKinds\":null,\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"list\",\"limit\":\"\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2019-01-21 23:49:11','2019-01-30 21:33:18','c843ca00-85a2-4fa7-9d67-8fc17efd75da'),
	(53,6,'Title','blockTitle','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"}','2019-01-21 23:49:11','2019-01-21 23:49:11','a42c7639-d2aa-4b0a-9975-d06886a1cf83'),
	(54,7,'Entry Type','entryType','global','',1,'none',NULL,'craft\\fields\\Dropdown','{\"options\":[{\"label\":\"None\",\"value\":\"none\",\"default\":\"\"},{\"label\":\"Blog\",\"value\":\"blog\",\"default\":\"\"}]}','2019-01-21 23:49:11','2019-01-21 23:49:11','751efea8-484b-4333-85f1-49adeaacebb1'),
	(55,7,'Heading Override','headingOverride','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"}','2019-01-21 23:49:12','2019-01-21 23:49:12','aac7c6ae-fdd1-4e8d-997d-5fb8a3c64c4c'),
	(56,8,'Global Phone Number','globalPhoneNumber','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":4,\"charLimit\":\"\",\"columnType\":\"text\"}','2019-01-21 23:49:12','2019-01-21 23:49:12','4e1ea51e-1b6b-437e-88e2-7ae2183ee09b'),
	(57,8,'Logo','logo','global','',1,'site',NULL,'craft\\fields\\Assets','{\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"singleUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"useSingleFolder\":\"\",\"viewMode\":\"large\"}','2019-01-21 23:49:12','2019-01-30 23:31:54','ee804c75-2927-4dc7-bb09-f537c1e68e3f'),
	(61,1,'Blocks','blocks','global','',1,'site',NULL,'benf\\neo\\Field','{\"localizeBlocks\":false,\"minBlocks\":\"\",\"maxBlocks\":\"\"}','2019-01-22 15:33:39','2019-02-08 20:03:35','b6fc1e42-0137-49b8-9a62-ea002ced47a5'),
	(62,6,'Video','video','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-01-22 15:49:14','2019-01-22 15:49:14','4072c709-5067-4886-ab01-e102f8fbbb62'),
	(63,6,'Position','position','global','',1,'none',NULL,'rias\\positionfieldtype\\fields\\Position','{\"options\":{\"left\":\"1\",\"center\":\"\",\"right\":\"1\",\"full\":\"\",\"drop-left\":\"\",\"drop-right\":\"\"},\"default\":\"left\"}','2019-01-22 15:50:08','2019-01-22 15:50:08','31977304-450c-4dc5-b381-1f380c154903'),
	(64,6,'Width','width','global','',1,'none',NULL,'craft\\fields\\Dropdown','{\"options\":[{\"label\":\"25%\",\"value\":\"3\",\"default\":\"\"},{\"label\":\"33%\",\"value\":\"4\",\"default\":\"\"},{\"label\":\"50%\",\"value\":\"6\",\"default\":\"\"},{\"label\":\"75%\",\"value\":\"8\",\"default\":\"\"}]}','2019-01-22 15:51:46','2019-01-22 15:51:46','a55919c9-4fbb-4498-8251-ace7864f95d0'),
	(65,3,'Social Media Blocks','socialMediaBlocks','global','',1,'site',NULL,'craft\\fields\\Matrix','{\"minBlocks\":\"\",\"maxBlocks\":\"\",\"contentTable\":\"{{%matrixcontent_socialmediablocks}}\",\"localizeBlocks\":false}','2019-01-22 16:06:16','2019-01-22 16:06:16','e583e0e9-0032-42bc-98ea-ade86a7d4bb4'),
	(66,NULL,'Media Icon','mediaIcon','matrixBlockType:7cca57cb-0702-4a0d-ae12-39dbd320cb8e','Place the class of the icon from Font Awesome',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-01-22 16:06:20','2019-01-22 16:06:20','7a4250f0-87b9-42f2-8406-abccf1ded9a1'),
	(67,NULL,'Media Image','mediaImage','matrixBlockType:7cca57cb-0702-4a0d-ae12-39dbd320cb8e','',1,'site',NULL,'craft\\fields\\Assets','{\"useSingleFolder\":\"\",\"defaultUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"defaultUploadLocationSubpath\":\"\",\"singleUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"singleUploadLocationSubpath\":\"\",\"restrictFiles\":\"\",\"allowedKinds\":null,\"sources\":\"*\",\"source\":null,\"targetSiteId\":null,\"viewMode\":\"large\",\"limit\":\"1\",\"selectionLabel\":\"\",\"localizeRelations\":false}','2019-01-22 16:06:20','2019-01-22 16:06:20','39673fbf-03e6-4ceb-bba9-7ce291a23c9b'),
	(68,NULL,'Media Link','mediaLink','matrixBlockType:7cca57cb-0702-4a0d-ae12-39dbd320cb8e','',1,'none',NULL,'craft\\fields\\Url','{\"placeholder\":\"\"}','2019-01-22 16:06:20','2019-01-22 16:06:20','8a46e9b3-393e-4be5-80ed-041c1fbfa309'),
	(69,9,'Quote Text','quoteText','global','',1,'none',NULL,'craft\\redactor\\Field','{\"redactorConfig\":\"Simple.json\",\"purifierConfig\":\"\",\"cleanupHtml\":\"1\",\"purifyHtml\":\"1\",\"columnType\":\"text\",\"availableVolumes\":\"*\",\"availableTransforms\":\"*\"}','2019-01-30 21:29:46','2019-01-30 21:29:46','5868ff9f-7ef8-4c35-ae86-6d360290afce'),
	(70,9,'Quote Attribution','quoteAttribution','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"charLimit\":\"\",\"code\":\"\",\"columnType\":\"text\",\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\"}','2019-01-30 21:30:46','2019-01-30 21:31:52','dea7bb72-cedf-434c-8f52-66b7674ac5a7'),
	(71,8,'Social Networks','socialNetworks','global','',1,'site',NULL,'craft\\fields\\Matrix','{\"contentTable\":\"{{%matrixcontent_socialnetworks}}\",\"localizeBlocks\":false,\"maxBlocks\":\"\",\"minBlocks\":\"\"}','2019-01-30 21:43:51','2019-01-30 21:44:19','21f5aedd-1af2-4ff2-96ec-143a010f62ce'),
	(72,NULL,'Icon html','iconhtml','matrixBlockType:56b3fc31-3cd2-4791-83b8-677d9de46e79','',1,'none',NULL,'craft\\fields\\PlainText','{\"placeholder\":\"\",\"code\":\"\",\"multiline\":\"\",\"initialRows\":\"4\",\"charLimit\":\"\",\"columnType\":\"text\"}','2019-01-30 21:43:52','2019-01-30 21:44:19','c134d797-760a-4c81-8b07-1333a762c3f8'),
	(73,NULL,'Social Link','socialLink','matrixBlockType:56b3fc31-3cd2-4791-83b8-677d9de46e79','',1,'none',NULL,'craft\\fields\\Url','{\"placeholder\":\"\"}','2019-01-30 21:43:52','2019-01-30 21:44:19','5ef1ea73-ce08-4adb-889d-0cc51b5329ce'),
	(74,9,'Quote Category','quoteCategory','global','',1,'site',NULL,'craft\\fields\\Categories','{\"branchLimit\":\"\",\"sources\":\"*\",\"source\":\"group:ae11201c-d8b4-4575-a034-14d73922010c\",\"targetSiteId\":null,\"viewMode\":null,\"limit\":null,\"selectionLabel\":\"\",\"localizeRelations\":false}','2019-01-30 23:33:38','2019-01-30 23:40:38','75340a1c-b9ff-4c2b-9a66-b3fb23a01d68'),
	(75,4,'Short Description','shortDescription','global','',1,'none',NULL,'craft\\fields\\PlainText','{\"charLimit\":\"\",\"code\":\"\",\"columnType\":\"text\",\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\"}','2019-01-31 16:42:23','2019-01-31 16:43:10','980e947d-aa5f-40f3-bbf5-7ab15371e271');

/*!40000 ALTER TABLE `craft_fields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_freeform_crm_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_freeform_crm_fields`;

CREATE TABLE `craft_freeform_crm_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integrationId` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `required` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `freeform_crm_fields_type_idx` (`type`),
  KEY `freeform_crm_fields_integrationId_fk` (`integrationId`),
  CONSTRAINT `freeform_crm_fields_integrationId_fk` FOREIGN KEY (`integrationId`) REFERENCES `craft_freeform_integrations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_freeform_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_freeform_fields`;

CREATE TABLE `craft_freeform_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(50) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `required` tinyint(1) DEFAULT '0',
  `instructions` text,
  `metaProperties` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `handle` (`handle`),
  KEY `freeform_fields_type_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_freeform_fields` WRITE;
/*!40000 ALTER TABLE `craft_freeform_fields` DISABLE KEYS */;

INSERT INTO `craft_freeform_fields` (`id`, `type`, `handle`, `label`, `required`, `instructions`, `metaProperties`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'text','firstName','First Name',0,NULL,NULL,'2019-01-30 21:13:43','2019-01-30 21:13:43','15d967f6-f3e5-452b-85a1-2b44be3c635e'),
	(2,'text','lastName','Last Name',0,NULL,NULL,'2019-01-30 21:13:43','2019-01-30 21:13:43','18337418-db42-4bbc-8794-d574939c3fd9'),
	(3,'email','email','Email',0,NULL,NULL,'2019-01-30 21:13:43','2019-01-30 21:13:43','682cc3c8-194c-45cf-9322-17f810ca0817'),
	(4,'text','website','Website',0,NULL,NULL,'2019-01-30 21:13:43','2019-01-30 21:13:43','fac87538-5216-499b-abff-6e495ad226b0'),
	(5,'text','cellPhone','Cell Phone',0,NULL,NULL,'2019-01-30 21:13:43','2019-01-30 21:13:43','d599d332-a392-4ab5-9a32-b3374c875e3c'),
	(6,'text','homePhone','Home Phone',0,NULL,NULL,'2019-01-30 21:13:43','2019-01-30 21:13:43','a5012588-8009-4cfb-87eb-7f42330c1295'),
	(7,'text','companyName','Company Name',0,NULL,NULL,'2019-01-30 21:13:43','2019-01-30 21:13:43','e99564a1-e579-42c0-99b4-fc878b83ba68'),
	(8,'textarea','address','Address',0,NULL,'{\"rows\":2}','2019-01-30 21:13:43','2019-01-30 21:13:43','6eb65024-25ab-469b-b867-80d05c39f286'),
	(9,'text','city','City',0,NULL,NULL,'2019-01-30 21:13:43','2019-01-30 21:13:43','84f3939c-d17e-4fd5-98eb-a673a348ba35'),
	(10,'select','state','State',0,NULL,'{\"options\":[{\"value\":\"\",\"label\":\"Select a State\"},{\"value\":\"AL\",\"label\":\"Alabama\"},{\"value\":\"AK\",\"label\":\"Alaska\"},{\"value\":\"AZ\",\"label\":\"Arizona\"},{\"value\":\"AR\",\"label\":\"Arkansas\"},{\"value\":\"CA\",\"label\":\"California\"},{\"value\":\"CO\",\"label\":\"Colorado\"},{\"value\":\"CT\",\"label\":\"Connecticut\"},{\"value\":\"DE\",\"label\":\"Delaware\"},{\"value\":\"DC\",\"label\":\"District of Columbia\"},{\"value\":\"FL\",\"label\":\"Florida\"},{\"value\":\"GA\",\"label\":\"Georgia\"},{\"value\":\"HI\",\"label\":\"Hawaii\"},{\"value\":\"ID\",\"label\":\"Idaho\"},{\"value\":\"IL\",\"label\":\"Illinois\"},{\"value\":\"IN\",\"label\":\"Indiana\"},{\"value\":\"IA\",\"label\":\"Iowa\"},{\"value\":\"KS\",\"label\":\"Kansas\"},{\"value\":\"KY\",\"label\":\"Kentucky\"},{\"value\":\"LA\",\"label\":\"Louisiana\"},{\"value\":\"ME\",\"label\":\"Maine\"},{\"value\":\"MD\",\"label\":\"Maryland\"},{\"value\":\"MA\",\"label\":\"Massachusetts\"},{\"value\":\"MI\",\"label\":\"Michigan\"},{\"value\":\"MN\",\"label\":\"Minnesota\"},{\"value\":\"MS\",\"label\":\"Mississippi\"},{\"value\":\"MO\",\"label\":\"Missouri\"},{\"value\":\"MT\",\"label\":\"Montana\"},{\"value\":\"NE\",\"label\":\"Nebraska\"},{\"value\":\"NV\",\"label\":\"Nevada\"},{\"value\":\"NH\",\"label\":\"New Hampshire\"},{\"value\":\"NJ\",\"label\":\"New Jersey\"},{\"value\":\"NM\",\"label\":\"New Mexico\"},{\"value\":\"NY\",\"label\":\"New York\"},{\"value\":\"NC\",\"label\":\"North Carolina\"},{\"value\":\"ND\",\"label\":\"North Dakota\"},{\"value\":\"OH\",\"label\":\"Ohio\"},{\"value\":\"OK\",\"label\":\"Oklahoma\"},{\"value\":\"OR\",\"label\":\"Oregon\"},{\"value\":\"PA\",\"label\":\"Pennsylvania\"},{\"value\":\"RI\",\"label\":\"Rhode Island\"},{\"value\":\"SC\",\"label\":\"South Carolina\"},{\"value\":\"SD\",\"label\":\"South Dakota\"},{\"value\":\"TN\",\"label\":\"Tennessee\"},{\"value\":\"TX\",\"label\":\"Texas\"},{\"value\":\"UT\",\"label\":\"Utah\"},{\"value\":\"VT\",\"label\":\"Vermont\"},{\"value\":\"VA\",\"label\":\"Virginia\"},{\"value\":\"WA\",\"label\":\"Washington\"},{\"value\":\"WV\",\"label\":\"West Virginia\"},{\"value\":\"WI\",\"label\":\"Wisconsin\"},{\"value\":\"WY\",\"label\":\"Wyoming\"}]}','2019-01-30 21:13:43','2019-01-30 21:13:43','5baa3512-db37-401d-a938-ba42e615cda2'),
	(11,'text','zipCode','Zip Code',0,NULL,NULL,'2019-01-30 21:13:43','2019-01-30 21:13:43','1d81d73b-11e8-4960-98da-5841cb49db19'),
	(12,'textarea','message','Message',0,NULL,'{\"rows\":5}','2019-01-30 21:13:43','2019-01-30 21:13:43','9f3ace53-07f9-4ef0-a063-d970bd9ec411'),
	(13,'number','number','Number',0,NULL,NULL,'2019-01-30 21:13:43','2019-01-30 21:13:43','2582febb-4675-4e51-bd25-d0213c497b0a');

/*!40000 ALTER TABLE `craft_freeform_fields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_freeform_forms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_freeform_forms`;

CREATE TABLE `craft_freeform_forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `handle` varchar(100) NOT NULL,
  `spamBlockCount` int(11) unsigned NOT NULL DEFAULT '0',
  `submissionTitleFormat` varchar(255) NOT NULL,
  `description` text,
  `layoutJson` mediumtext,
  `returnUrl` varchar(255) DEFAULT NULL,
  `defaultStatus` int(11) unsigned DEFAULT NULL,
  `formTemplateId` int(11) unsigned DEFAULT NULL,
  `color` varchar(10) DEFAULT NULL,
  `optInDataStorageTargetHash` varchar(20) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `handle` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_freeform_forms` WRITE;
/*!40000 ALTER TABLE `craft_freeform_forms` DISABLE KEYS */;

INSERT INTO `craft_freeform_forms` (`id`, `name`, `handle`, `spamBlockCount`, `submissionTitleFormat`, `description`, `layoutJson`, `returnUrl`, `defaultStatus`, `formTemplateId`, `color`, `optInDataStorageTargetHash`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Contact','contact',0,'{{ dateCreated|date(\"Y-m-d H:i:s\") }}','','{\"composer\":{\"layout\":[[{\"id\":\"6E6zQY1pQ\",\"columns\":[\"8oWe9jZpP\",\"5KM0OK02p\"]},{\"id\":\"zE7rKyNlE\",\"columns\":[\"d1EeG8Z68\"]},{\"id\":\"P21yYxXdd\",\"columns\":[\"QBOZ7q06y\"]},{\"id\":\"15rzkGy7P\",\"columns\":[\"2JozW31ox\"]}]],\"properties\":{\"page0\":{\"type\":\"page\",\"label\":\"Page 1\"},\"form\":{\"type\":\"form\",\"name\":\"Contact\",\"handle\":\"contact\",\"color\":\"#b7f801\",\"submissionTitleFormat\":\"{{ dateCreated|date(\\\"Y-m-d H:i:s\\\") }}\",\"description\":\"\",\"formTemplate\":\"bootstrap-4.html\",\"returnUrl\":\"\",\"storeData\":true,\"defaultStatus\":2,\"ajaxEnabled\":true},\"integration\":{\"type\":\"integration\",\"integrationId\":0,\"mapping\":[]},\"connections\":{\"type\":\"connections\",\"list\":null},\"rules\":{\"type\":\"rules\",\"list\":[]},\"admin_notifications\":{\"type\":\"admin_notifications\",\"notificationId\":1,\"recipients\":\"dev@nerdymind.com\"},\"payment\":{\"type\":\"payment\",\"integrationId\":0,\"mapping\":[]},\"2JozW31ox\":{\"type\":\"submit\",\"label\":\"Submit\",\"labelNext\":\"Submit\",\"labelPrev\":\"Previous\",\"disablePrev\":false,\"position\":\"center\"},\"d1EeG8Z68\":{\"hash\":\"d1EeG8Z68\",\"id\":3,\"type\":\"email\",\"handle\":\"email\",\"label\":\"\",\"required\":false,\"instructions\":\"\",\"notificationId\":0,\"values\":[],\"placeholder\":\"Email\"},\"8oWe9jZpP\":{\"hash\":\"8oWe9jZpP\",\"id\":1,\"type\":\"text\",\"handle\":\"firstName\",\"label\":\"\",\"required\":false,\"instructions\":\"\",\"value\":\"\",\"placeholder\":\"First Name\"},\"5KM0OK02p\":{\"hash\":\"5KM0OK02p\",\"id\":2,\"type\":\"text\",\"handle\":\"lastName\",\"label\":\"\",\"required\":false,\"instructions\":\"\",\"value\":\"\",\"placeholder\":\"Last Name\"},\"QBOZ7q06y\":{\"hash\":\"QBOZ7q06y\",\"id\":12,\"type\":\"textarea\",\"handle\":\"message\",\"label\":\"\",\"required\":false,\"instructions\":\"\",\"value\":\"\",\"placeholder\":\"Message\",\"rows\":5}}},\"context\":{\"page\":0,\"hash\":\"QBOZ7q06y\"}}','',2,NULL,'#b7f801',NULL,'2019-01-31 15:41:13','2019-02-08 20:10:15','58d0c2f6-f836-454c-98df-016688013688');

/*!40000 ALTER TABLE `craft_freeform_forms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_freeform_integrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_freeform_integrations`;

CREATE TABLE `craft_freeform_integrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `class` varchar(255) DEFAULT NULL,
  `accessToken` varchar(255) DEFAULT NULL,
  `settings` text,
  `forceUpdate` tinyint(1) DEFAULT '0',
  `lastUpdate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `handle` (`handle`),
  KEY `freeform_integrations_type_idx` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_freeform_integrations_queue
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_freeform_integrations_queue`;

CREATE TABLE `craft_freeform_integrations_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `submissionId` int(11) NOT NULL,
  `integrationType` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `fieldHash` varchar(20) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `freeform_integrations_queue_status_unq_idx` (`status`),
  KEY `freeform_integrations_queue_submissionId_fk` (`submissionId`),
  CONSTRAINT `freeform_integrations_queue_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_freeform_mailing_list_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `freeform_integrations_queue_submissionId_fk` FOREIGN KEY (`submissionId`) REFERENCES `craft_freeform_submissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_freeform_mailing_list_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_freeform_mailing_list_fields`;

CREATE TABLE `craft_freeform_mailing_list_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mailingListId` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `required` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `freeform_mailing_list_fields_type_idx` (`type`),
  KEY `freeform_mailing_list_fields_mailingListId_fk` (`mailingListId`),
  CONSTRAINT `freeform_mailing_list_fields_mailingListId_fk` FOREIGN KEY (`mailingListId`) REFERENCES `craft_freeform_mailing_lists` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_freeform_mailing_lists
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_freeform_mailing_lists`;

CREATE TABLE `craft_freeform_mailing_lists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integrationId` int(11) NOT NULL,
  `resourceId` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `memberCount` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `freeform_mailing_lists_integrationId_resourceId_unq_idx` (`integrationId`,`resourceId`),
  CONSTRAINT `freeform_mailing_lists_integrationId_fk` FOREIGN KEY (`integrationId`) REFERENCES `craft_freeform_integrations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_freeform_notifications
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_freeform_notifications`;

CREATE TABLE `craft_freeform_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `description` text,
  `fromName` varchar(255) NOT NULL,
  `fromEmail` varchar(255) NOT NULL,
  `replyToEmail` varchar(255) DEFAULT NULL,
  `bodyHtml` text,
  `bodyText` text,
  `includeAttachments` tinyint(1) DEFAULT '1',
  `sortOrder` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `handle` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_freeform_notifications` WRITE;
/*!40000 ALTER TABLE `craft_freeform_notifications` DISABLE KEYS */;

INSERT INTO `craft_freeform_notifications` (`id`, `name`, `handle`, `subject`, `description`, `fromName`, `fromEmail`, `replyToEmail`, `bodyHtml`, `bodyText`, `includeAttachments`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Default','default','New submission from {{ form.name }}',NULL,'Mapp','dev@nerdymind.com',NULL,'<p>Submitted on: {{ dateCreated|date(\'Y-m-d H:i:s\') }}</p>\n<ul>\n{% for field in allFields %}\n    <li>{{ field.label }}: {{ field.getValueAsString() }}</li>\n{% endfor %}\n</ul>','Submitted on: {{ dateCreated|date(\'Y-m-d H:i:s\') }}\n\n{% for field in allFields %} \n\n  * {{ field.label }}: {{ field.getValueAsString() }} {% endfor %}',1,NULL,'2019-01-31 15:41:08','2019-01-31 15:41:08','fd4277eb-4310-4d2c-9e0b-376a43cd58de');

/*!40000 ALTER TABLE `craft_freeform_notifications` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_freeform_payment_gateway_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_freeform_payment_gateway_fields`;

CREATE TABLE `craft_freeform_payment_gateway_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `integrationId` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(50) NOT NULL,
  `required` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `freeform_payment_gateway_fields_type_idx` (`type`),
  KEY `freeform_payment_gateway_fields_integrationId_fk` (`integrationId`),
  CONSTRAINT `freeform_payment_gateway_fields_integrationId_fk` FOREIGN KEY (`integrationId`) REFERENCES `craft_freeform_integrations` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_freeform_statuses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_freeform_statuses`;

CREATE TABLE `craft_freeform_statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `color` varchar(30) DEFAULT NULL,
  `isDefault` tinyint(1) DEFAULT NULL,
  `sortOrder` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `handle` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_freeform_statuses` WRITE;
/*!40000 ALTER TABLE `craft_freeform_statuses` DISABLE KEYS */;

INSERT INTO `craft_freeform_statuses` (`id`, `name`, `handle`, `color`, `isDefault`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Pending','pending','light',NULL,1,'2019-01-30 21:13:43','2019-01-30 21:13:43','4ef4b8d6-94ab-4ccb-a7f5-8b6ad99f7032'),
	(2,'Open','open','green',1,2,'2019-01-30 21:13:43','2019-01-30 21:13:43','825a49c6-8006-4aa9-aa2f-669358c2fc28'),
	(3,'Closed','closed','grey',NULL,3,'2019-01-30 21:13:43','2019-01-30 21:13:43','6deec97a-e5bb-474e-851a-14b2337419b6');

/*!40000 ALTER TABLE `craft_freeform_statuses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_freeform_submissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_freeform_submissions`;

CREATE TABLE `craft_freeform_submissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `incrementalId` int(11) NOT NULL,
  `statusId` int(11) DEFAULT NULL,
  `formId` int(11) NOT NULL,
  `token` varchar(100) NOT NULL,
  `ip` varchar(46) DEFAULT NULL,
  `isSpam` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_1` varchar(100) DEFAULT NULL,
  `field_2` varchar(100) DEFAULT NULL,
  `field_3` text,
  `field_4` varchar(100) DEFAULT NULL,
  `field_5` varchar(100) DEFAULT NULL,
  `field_6` varchar(100) DEFAULT NULL,
  `field_7` varchar(100) DEFAULT NULL,
  `field_8` text,
  `field_9` varchar(100) DEFAULT NULL,
  `field_10` varchar(100) DEFAULT NULL,
  `field_11` varchar(100) DEFAULT NULL,
  `field_12` text,
  `field_13` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `freeform_submissions_incrementalId_unq_idx` (`incrementalId`),
  UNIQUE KEY `freeform_submissions_token_unq_idx` (`token`),
  KEY `freeform_submissions_formId_fk` (`formId`),
  KEY `freeform_submissions_statusId_fk` (`statusId`),
  CONSTRAINT `freeform_submissions_formId_fk` FOREIGN KEY (`formId`) REFERENCES `craft_freeform_forms` (`id`) ON DELETE CASCADE,
  CONSTRAINT `freeform_submissions_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `freeform_submissions_statusId_fk` FOREIGN KEY (`statusId`) REFERENCES `craft_freeform_statuses` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_freeform_unfinalized_files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_freeform_unfinalized_files`;

CREATE TABLE `craft_freeform_unfinalized_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assetId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_globalsets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_globalsets`;

CREATE TABLE `craft_globalsets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_globalsets_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `craft_globalsets_name_idx` (`name`),
  KEY `craft_globalsets_handle_idx` (`handle`),
  CONSTRAINT `craft_globalsets_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_globalsets_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_globalsets` WRITE;
/*!40000 ALTER TABLE `craft_globalsets` DISABLE KEYS */;

INSERT INTO `craft_globalsets` (`id`, `name`, `handle`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(3,'Social Media','socialMedia',81,'2019-01-21 23:41:02','2019-01-30 21:44:19','26a7e0c4-aeb8-4365-b08e-5a58a5fcca24'),
	(4,'Theme Options','themeOptions',82,'2019-01-21 23:41:12','2019-01-22 16:10:09','fc8ed7ef-6fba-4982-a813-e4fcdb3c9f10');

/*!40000 ALTER TABLE `craft_globalsets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_gqlschemas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_gqlschemas`;

CREATE TABLE `craft_gqlschemas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `accessToken` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `expiryDate` datetime DEFAULT NULL,
  `lastUsed` datetime DEFAULT NULL,
  `scope` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_gqlschemas_accessToken_unq_idx` (`accessToken`),
  UNIQUE KEY `craft_gqlschemas_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_gqlschemas` WRITE;
/*!40000 ALTER TABLE `craft_gqlschemas` DISABLE KEYS */;

INSERT INTO `craft_gqlschemas` (`id`, `name`, `accessToken`, `enabled`, `expiryDate`, `lastUsed`, `scope`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'Public Schema','__PUBLIC__',1,NULL,'2019-11-07 15:50:33','[]','2019-11-07 15:49:51','2019-11-07 15:50:33','ca27e47f-1122-402c-a8f3-3bbceaaa3f44');

/*!40000 ALTER TABLE `craft_gqlschemas` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_info
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_info`;

CREATE TABLE `craft_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` varchar(50) NOT NULL,
  `schemaVersion` varchar(15) NOT NULL,
  `maintenance` tinyint(1) NOT NULL DEFAULT '0',
  `config` mediumtext,
  `configMap` mediumtext,
  `fieldVersion` char(12) NOT NULL DEFAULT '000000000000',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_info` WRITE;
/*!40000 ALTER TABLE `craft_info` DISABLE KEYS */;

INSERT INTO `craft_info` (`id`, `version`, `schemaVersion`, `maintenance`, `config`, `configMap`, `fieldVersion`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'3.3.15','3.3.3',0,'{\"categoryGroups\":{\"ae11201c-d8b4-4575-a034-14d73922010c\":{\"handle\":\"quote\",\"name\":\"Quote\",\"siteSettings\":{\"4e0fc612-e53b-4ed7-8b23-d723a0c987e8\":{\"hasUrls\":true,\"template\":\"quote/categories.twig\",\"uriFormat\":\"quote/{slug}\"}},\"structure\":{\"maxLevels\":null,\"uid\":\"15e92219-8361-44e4-8d09-0a3b849060bb\"}},\"ddd31c2a-2ae9-45cc-8ee2-d43795a7198a\":{\"handle\":\"blog\",\"name\":\"Blog\",\"siteSettings\":{\"4e0fc612-e53b-4ed7-8b23-d723a0c987e8\":{\"hasUrls\":true,\"template\":\"blog/_categories.twig\",\"uriFormat\":\"blog/{slug}\"}},\"structure\":{\"maxLevels\":null,\"uid\":\"489bb1cd-412e-41f1-aa73-12797a2e91ee\"}}},\"dateModified\":1573145020,\"email\":{\"fromEmail\":\"$DEFAULT_EMAIL\",\"fromName\":\"$SITE_NAME\",\"template\":\"\",\"transportSettings\":null,\"transportType\":\"craft\\\\mail\\\\transportadapters\\\\Sendmail\"},\"fieldGroups\":{\"024ef342-aed2-46fe-8110-f248b4ca01e2\":{\"name\":\"Blog\"},\"46c1ca72-5d1d-49c5-aeb8-3aa4c7ab1a9f\":{\"name\":\"Blocks\"},\"5206e00d-9d76-4558-b13d-48c8882111e2\":{\"name\":\"Quote\"},\"52840786-f113-4e5f-94a3-eded8dabafbb\":{\"name\":\"Call to Action\"},\"688ea26c-2c9a-4765-b3e7-ba03c09c5043\":{\"name\":\"General Fields\"},\"a7068d61-4021-48c1-a6dc-fdd50446baba\":{\"name\":\"Theme Options\"},\"b7db88d8-8d51-4759-86c6-d83b1fc1ea65\":{\"name\":\"Block Options\"},\"c3ef8c2d-34fe-4922-8fcd-492e567df532\":{\"name\":\"Page Options\"},\"cd825c6f-1d25-4823-a212-ce4079c6e4e7\":{\"name\":\"Common\"}},\"fields\":{\"067e4034-f577-41cc-99a6-bea1b7742890\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"46c1ca72-5d1d-49c5-aeb8-3aa4c7ab1a9f\",\"handle\":\"callToAction\",\"instructions\":\"\",\"name\":\"Call to Action\",\"searchable\":true,\"settings\":{\"limit\":\"1\",\"localizeRelations\":false,\"selectionLabel\":\"\",\"source\":null,\"sources\":[\"section:c56e2508-9559-4291-82c0-546311d3961b\"],\"targetSiteId\":null,\"viewMode\":null},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"craft\\\\fields\\\\Entries\"},\"1e4308ab-e0a5-4a36-b76d-4d5d0c1f59af\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"024ef342-aed2-46fe-8110-f248b4ca01e2\",\"handle\":\"blogCategory\",\"instructions\":\"\",\"name\":\"Blog Category \",\"searchable\":true,\"settings\":{\"branchLimit\":\"\",\"limit\":null,\"localizeRelations\":false,\"selectionLabel\":\"\",\"source\":\"group:ddd31c2a-2ae9-45cc-8ee2-d43795a7198a\",\"sources\":\"*\",\"targetSiteId\":null,\"viewMode\":null},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"craft\\\\fields\\\\Categories\"},\"21f5aedd-1af2-4ff2-96ec-143a010f62ce\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"a7068d61-4021-48c1-a6dc-fdd50446baba\",\"handle\":\"socialNetworks\",\"instructions\":\"\",\"name\":\"Social Networks\",\"searchable\":true,\"settings\":{\"contentTable\":\"{{%matrixcontent_socialnetworks}}\",\"localizeBlocks\":false,\"maxBlocks\":\"\",\"minBlocks\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"craft\\\\fields\\\\Matrix\"},\"244270e3-5f4b-4777-a5bc-a8cc022cb5e7\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"688ea26c-2c9a-4765-b3e7-ba03c09c5043\",\"handle\":\"blockImage\",\"instructions\":\"\",\"name\":\"Image\",\"searchable\":true,\"settings\":{\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"singleUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"useSingleFolder\":\"\",\"viewMode\":\"large\"},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"craft\\\\fields\\\\Assets\"},\"2a0cd4a0-dafb-4f02-8c75-23400cc88ecb\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"688ea26c-2c9a-4765-b3e7-ba03c09c5043\",\"handle\":\"entriesLink\",\"instructions\":\"\",\"name\":\"Entries Link\",\"searchable\":true,\"settings\":{\"limit\":\"\",\"localizeRelations\":false,\"selectionLabel\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"viewMode\":null},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"craft\\\\fields\\\\Entries\"},\"31977304-450c-4dc5-b381-1f380c154903\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"688ea26c-2c9a-4765-b3e7-ba03c09c5043\",\"handle\":\"position\",\"instructions\":\"\",\"name\":\"Position\",\"searchable\":true,\"settings\":{\"default\":\"left\",\"options\":{\"center\":\"\",\"drop-left\":\"\",\"drop-right\":\"\",\"full\":\"\",\"left\":\"1\",\"right\":\"1\"}},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"rias\\\\positionfieldtype\\\\fields\\\\Position\"},\"36cbdbd4-5e01-40b8-be3a-7568707136d8\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"46c1ca72-5d1d-49c5-aeb8-3aa4c7ab1a9f\",\"handle\":\"forwardPathGrid\",\"instructions\":\"\",\"name\":\"Forward Path Grid\",\"searchable\":true,\"settings\":{\"columns\":{\"42\":{\"width\":\"\"},\"43\":{\"width\":\"\"},\"45\":{\"width\":\"\"}},\"contentTable\":\"{{%stc_forwardpathgrid}}\",\"fieldLayout\":\"matrix\",\"localizeBlocks\":false,\"maxRows\":\"\",\"minRows\":\"\",\"selectionLabel\":\"\",\"staticField\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"verbb\\\\supertable\\\\fields\\\\SuperTableField\"},\"4072c709-5067-4886-ab01-e102f8fbbb62\":{\"contentColumnType\":\"text\",\"fieldGroup\":\"688ea26c-2c9a-4765-b3e7-ba03c09c5043\",\"handle\":\"video\",\"instructions\":\"\",\"name\":\"Video\",\"searchable\":true,\"settings\":{\"charLimit\":\"\",\"code\":\"\",\"columnType\":\"text\",\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\PlainText\"},\"4e1ea51e-1b6b-437e-88e2-7ae2183ee09b\":{\"contentColumnType\":\"text\",\"fieldGroup\":\"a7068d61-4021-48c1-a6dc-fdd50446baba\",\"handle\":\"globalPhoneNumber\",\"instructions\":\"\",\"name\":\"Global Phone Number\",\"searchable\":true,\"settings\":{\"charLimit\":\"\",\"code\":\"\",\"columnType\":\"text\",\"initialRows\":4,\"multiline\":\"\",\"placeholder\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\PlainText\"},\"505d4149-2cbb-4eb1-a6c3-e6dff168aca1\":{\"contentColumnType\":\"string(7)\",\"fieldGroup\":\"b7db88d8-8d51-4759-86c6-d83b1fc1ea65\",\"handle\":\"backgroundColor\",\"instructions\":\"\",\"name\":\"Background Color\",\"searchable\":true,\"settings\":{\"defaultColor\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\Color\"},\"5868ff9f-7ef8-4c35-ae86-6d360290afce\":{\"contentColumnType\":\"text\",\"fieldGroup\":\"5206e00d-9d76-4558-b13d-48c8882111e2\",\"handle\":\"quoteText\",\"instructions\":\"\",\"name\":\"Quote Text\",\"searchable\":true,\"settings\":{\"availableTransforms\":\"*\",\"availableVolumes\":\"*\",\"cleanupHtml\":\"1\",\"columnType\":\"text\",\"purifierConfig\":\"\",\"purifyHtml\":\"1\",\"redactorConfig\":\"Simple.json\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\redactor\\\\Field\"},\"678cb4b8-2b51-42c7-bf90-05465e52f4e8\":{\"contentColumnType\":\"text\",\"fieldGroup\":\"688ea26c-2c9a-4765-b3e7-ba03c09c5043\",\"handle\":\"blockContent\",\"instructions\":\"\",\"name\":\"Content\",\"searchable\":true,\"settings\":{\"availableTransforms\":\"*\",\"availableVolumes\":\"*\",\"cleanupHtml\":\"\",\"columnType\":\"text\",\"purifierConfig\":\"\",\"purifyHtml\":\"\",\"redactorConfig\":\"Standard.json\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\redactor\\\\Field\"},\"751efea8-484b-4333-85f1-49adeaacebb1\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"c3ef8c2d-34fe-4922-8fcd-492e567df532\",\"handle\":\"entryType\",\"instructions\":\"\",\"name\":\"Entry Type\",\"searchable\":true,\"settings\":{\"options\":[{\"default\":\"\",\"label\":\"None\",\"value\":\"none\"},{\"default\":\"\",\"label\":\"Blog\",\"value\":\"blog\"}]},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\Dropdown\"},\"75340a1c-b9ff-4c2b-9a66-b3fb23a01d68\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"5206e00d-9d76-4558-b13d-48c8882111e2\",\"handle\":\"quoteCategory\",\"instructions\":\"\",\"name\":\"Quote Category\",\"searchable\":true,\"settings\":{\"branchLimit\":\"\",\"limit\":null,\"localizeRelations\":false,\"selectionLabel\":\"\",\"source\":\"group:ae11201c-d8b4-4575-a034-14d73922010c\",\"sources\":\"*\",\"targetSiteId\":null,\"viewMode\":null},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"craft\\\\fields\\\\Categories\"},\"93751aed-0596-4e0a-ba61-1784d50d4193\":{\"contentColumnType\":\"text\",\"fieldGroup\":\"b7db88d8-8d51-4759-86c6-d83b1fc1ea65\",\"handle\":\"padding\",\"instructions\":\"Standard CSS padding\",\"name\":\"Padding\",\"searchable\":true,\"settings\":{\"charLimit\":\"\",\"code\":\"\",\"columnType\":\"text\",\"initialRows\":4,\"multiline\":\"\",\"placeholder\":\"0 5% 0 5%\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\PlainText\"},\"980e947d-aa5f-40f3-bbf5-7ab15371e271\":{\"contentColumnType\":\"text\",\"fieldGroup\":\"024ef342-aed2-46fe-8110-f248b4ca01e2\",\"handle\":\"shortDescription\",\"instructions\":\"\",\"name\":\"Short Description\",\"searchable\":true,\"settings\":{\"charLimit\":\"\",\"code\":\"\",\"columnType\":\"text\",\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\PlainText\"},\"a42c7639-d2aa-4b0a-9975-d06886a1cf83\":{\"contentColumnType\":\"text\",\"fieldGroup\":\"688ea26c-2c9a-4765-b3e7-ba03c09c5043\",\"handle\":\"blockTitle\",\"instructions\":\"\",\"name\":\"Title\",\"searchable\":true,\"settings\":{\"charLimit\":\"\",\"code\":\"\",\"columnType\":\"text\",\"initialRows\":4,\"multiline\":\"\",\"placeholder\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\PlainText\"},\"a55919c9-4fbb-4498-8251-ace7864f95d0\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"688ea26c-2c9a-4765-b3e7-ba03c09c5043\",\"handle\":\"width\",\"instructions\":\"\",\"name\":\"Width\",\"searchable\":true,\"settings\":{\"options\":[{\"default\":\"\",\"label\":\"25%\",\"value\":\"3\"},{\"default\":\"\",\"label\":\"33%\",\"value\":\"4\"},{\"default\":\"\",\"label\":\"50%\",\"value\":\"6\"},{\"default\":\"\",\"label\":\"75%\",\"value\":\"8\"}]},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\Dropdown\"},\"aac7c6ae-fdd1-4e8d-997d-5fb8a3c64c4c\":{\"contentColumnType\":\"text\",\"fieldGroup\":\"c3ef8c2d-34fe-4922-8fcd-492e567df532\",\"handle\":\"headingOverride\",\"instructions\":\"\",\"name\":\"Heading Override\",\"searchable\":true,\"settings\":{\"charLimit\":\"\",\"code\":\"\",\"columnType\":\"text\",\"initialRows\":4,\"multiline\":\"\",\"placeholder\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\PlainText\"},\"b6fc1e42-0137-49b8-9a62-ea002ced47a5\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"cd825c6f-1d25-4823-a212-ce4079c6e4e7\",\"handle\":\"blocks\",\"instructions\":\"\",\"name\":\"Blocks\",\"searchable\":true,\"settings\":{\"localizeBlocks\":false,\"maxBlocks\":\"\",\"minBlocks\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"benf\\\\neo\\\\Field\"},\"c0656cd9-0e0c-4efd-9970-f91a5d729834\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"024ef342-aed2-46fe-8110-f248b4ca01e2\",\"handle\":\"bannerImage\",\"instructions\":\"\",\"name\":\"Banner Image\",\"searchable\":true,\"settings\":{\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"singleUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"useSingleFolder\":\"\",\"viewMode\":\"large\"},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"craft\\\\fields\\\\Assets\"},\"c36e4ec1-b15f-416b-8688-ede105b9abe0\":{\"contentColumnType\":\"text\",\"fieldGroup\":\"b7db88d8-8d51-4759-86c6-d83b1fc1ea65\",\"handle\":\"margin\",\"instructions\":\"Standard CSS margin\",\"name\":\"Margin\",\"searchable\":true,\"settings\":{\"charLimit\":\"\",\"code\":\"\",\"columnType\":\"text\",\"initialRows\":4,\"multiline\":\"\",\"placeholder\":\"0 5% 0 5%\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\PlainText\"},\"c5c4a6a7-7c25-4595-b93a-b084faf2da7c\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"b7db88d8-8d51-4759-86c6-d83b1fc1ea65\",\"handle\":\"customBackground\",\"instructions\":\"\",\"name\":\"Custom Background\",\"searchable\":true,\"settings\":{\"options\":[{\"default\":\"\",\"label\":\"None\",\"value\":\"none\"},{\"default\":\"\",\"label\":\"Color\",\"value\":\"color\"},{\"default\":\"\",\"label\":\"Image\",\"value\":\"image\"}]},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\RadioButtons\"},\"c843ca00-85a2-4fa7-9d67-8fc17efd75da\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"688ea26c-2c9a-4765-b3e7-ba03c09c5043\",\"handle\":\"blockMultipleImages\",\"instructions\":\"\",\"name\":\"Multiple Images\",\"searchable\":true,\"settings\":{\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"\",\"localizeRelations\":false,\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"singleUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"useSingleFolder\":\"\",\"viewMode\":\"list\"},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"craft\\\\fields\\\\Assets\"},\"d386a8dc-011e-4918-b93b-f21e15c8d4b6\":{\"contentColumnType\":\"integer\",\"fieldGroup\":\"52840786-f113-4e5f-94a3-eded8dabafbb\",\"handle\":\"form\",\"instructions\":\"\",\"name\":\"Form\",\"searchable\":true,\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"Solspace\\\\Freeform\\\\FieldTypes\\\\FormFieldType\"},\"dea7bb72-cedf-434c-8f52-66b7674ac5a7\":{\"contentColumnType\":\"text\",\"fieldGroup\":\"5206e00d-9d76-4558-b13d-48c8882111e2\",\"handle\":\"quoteAttribution\",\"instructions\":\"\",\"name\":\"Quote Attribution\",\"searchable\":true,\"settings\":{\"charLimit\":\"\",\"code\":\"\",\"columnType\":\"text\",\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\PlainText\"},\"e583e0e9-0032-42bc-98ea-ade86a7d4bb4\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"46c1ca72-5d1d-49c5-aeb8-3aa4c7ab1a9f\",\"handle\":\"socialMediaBlocks\",\"instructions\":\"\",\"name\":\"Social Media Blocks\",\"searchable\":true,\"settings\":{\"contentTable\":\"{{%matrixcontent_socialmediablocks}}\",\"localizeBlocks\":false,\"maxBlocks\":\"\",\"minBlocks\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"craft\\\\fields\\\\Matrix\"},\"e660e5f6-d471-4459-adae-180958bb4dc9\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"b7db88d8-8d51-4759-86c6-d83b1fc1ea65\",\"handle\":\"backgroundImage\",\"instructions\":\"\",\"name\":\"Background Image\",\"searchable\":true,\"settings\":{\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"\",\"localizeRelations\":false,\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"singleUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"useSingleFolder\":\"\",\"viewMode\":\"list\"},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"craft\\\\fields\\\\Assets\"},\"ee804c75-2927-4dc7-bb09-f537c1e68e3f\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"a7068d61-4021-48c1-a6dc-fdd50446baba\",\"handle\":\"logo\",\"instructions\":\"\",\"name\":\"Logo\",\"searchable\":true,\"settings\":{\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"singleUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"useSingleFolder\":\"\",\"viewMode\":\"large\"},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"craft\\\\fields\\\\Assets\"},\"fe88044f-6d76-40a1-b67c-4d1f7a38f62b\":{\"contentColumnType\":\"string\",\"fieldGroup\":\"46c1ca72-5d1d-49c5-aeb8-3aa4c7ab1a9f\",\"handle\":\"columns\",\"instructions\":\"\",\"name\":\"Columns\",\"searchable\":true,\"settings\":{\"columns\":{\"39\":{\"width\":\"\"},\"40\":{\"width\":\"\"}},\"contentTable\":\"{{%stc_columns}}\",\"fieldLayout\":\"matrix\",\"localizeBlocks\":false,\"maxRows\":\"\",\"minRows\":\"\",\"selectionLabel\":\"\",\"staticField\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"verbb\\\\supertable\\\\fields\\\\SuperTableField\"}},\"globalSets\":{\"26a7e0c4-aeb8-4365-b08e-5a58a5fcca24\":{\"fieldLayouts\":{\"610ee66f-738a-44ef-8d83-dcbc043f9d58\":{\"tabs\":[{\"fields\":{\"21f5aedd-1af2-4ff2-96ec-143a010f62ce\":{\"required\":false,\"sortOrder\":1}},\"name\":\"Social\",\"sortOrder\":1}]}},\"handle\":\"socialMedia\",\"name\":\"Social Media\"},\"fc8ed7ef-6fba-4982-a813-e4fcdb3c9f10\":{\"fieldLayouts\":{\"6c438554-e1ad-483d-a7d7-403b8a0ea3a4\":{\"tabs\":[{\"fields\":{\"ee804c75-2927-4dc7-bb09-f537c1e68e3f\":{\"required\":false,\"sortOrder\":1}},\"name\":\"Content\",\"sortOrder\":1}]}},\"handle\":\"themeOptions\",\"name\":\"Theme Options\"}},\"imageTransforms\":{\"1f849e9e-ba75-4cf0-b79a-25ba0a277be1\":{\"format\":null,\"handle\":\"mediumSquare\",\"height\":\"580\",\"interlace\":\"none\",\"mode\":\"crop\",\"name\":\"Medium Square\",\"position\":\"center-center\",\"quality\":null,\"width\":\"580\"},\"43d844ad-22c6-47ee-b7b4-c2b3a9e0c48c\":{\"format\":null,\"handle\":\"banner\",\"height\":\"\",\"interlace\":\"none\",\"mode\":\"crop\",\"name\":\"Banner\",\"position\":\"center-center\",\"quality\":null,\"width\":\"1400\"},\"b38cff1b-146d-442e-ab43-2d60a7fd3986\":{\"format\":null,\"handle\":\"thumbnail\",\"height\":\"350\",\"interlace\":\"none\",\"mode\":\"crop\",\"name\":\"Thumbnail\",\"position\":\"center-center\",\"quality\":null,\"width\":\"350\"}},\"matrixBlockTypes\":{\"56b3fc31-3cd2-4791-83b8-677d9de46e79\":{\"field\":\"21f5aedd-1af2-4ff2-96ec-143a010f62ce\",\"fieldLayouts\":{\"ccf8f545-4938-4bd5-a07d-1216a65e0472\":{\"tabs\":[{\"fields\":{\"5ef1ea73-ce08-4adb-889d-0cc51b5329ce\":{\"required\":\"0\",\"sortOrder\":2},\"c134d797-760a-4c81-8b07-1333a762c3f8\":{\"required\":\"0\",\"sortOrder\":1}},\"name\":\"Content\",\"sortOrder\":1}]}},\"fields\":{\"5ef1ea73-ce08-4adb-889d-0cc51b5329ce\":{\"contentColumnType\":\"string\",\"fieldGroup\":null,\"handle\":\"socialLink\",\"instructions\":\"\",\"name\":\"Social Link\",\"searchable\":\"1\",\"settings\":{\"placeholder\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\Url\"},\"c134d797-760a-4c81-8b07-1333a762c3f8\":{\"contentColumnType\":\"text\",\"fieldGroup\":null,\"handle\":\"iconhtml\",\"instructions\":\"\",\"name\":\"Icon html\",\"searchable\":\"1\",\"settings\":{\"charLimit\":\"\",\"code\":\"\",\"columnType\":\"text\",\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\PlainText\"}},\"handle\":\"socialMedia\",\"name\":\"Social Media\",\"sortOrder\":1},\"7cca57cb-0702-4a0d-ae12-39dbd320cb8e\":{\"field\":\"e583e0e9-0032-42bc-98ea-ade86a7d4bb4\",\"fieldLayouts\":{\"4e75fa4e-7c81-44ae-a7b1-4a0db1b53ff6\":{\"tabs\":[{\"fields\":{\"39673fbf-03e6-4ceb-bba9-7ce291a23c9b\":{\"required\":false,\"sortOrder\":2},\"7a4250f0-87b9-42f2-8406-abccf1ded9a1\":{\"required\":false,\"sortOrder\":1},\"8a46e9b3-393e-4be5-80ed-041c1fbfa309\":{\"required\":false,\"sortOrder\":3}},\"name\":\"Content\",\"sortOrder\":1}]}},\"fields\":{\"39673fbf-03e6-4ceb-bba9-7ce291a23c9b\":{\"contentColumnType\":\"string\",\"fieldGroup\":null,\"handle\":\"mediaImage\",\"instructions\":\"\",\"name\":\"Media Image\",\"searchable\":true,\"settings\":{\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"singleUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"useSingleFolder\":\"\",\"viewMode\":\"large\"},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"craft\\\\fields\\\\Assets\"},\"7a4250f0-87b9-42f2-8406-abccf1ded9a1\":{\"contentColumnType\":\"text\",\"fieldGroup\":null,\"handle\":\"mediaIcon\",\"instructions\":\"Place the class of the icon from Font Awesome\",\"name\":\"Media Icon\",\"searchable\":true,\"settings\":{\"charLimit\":\"\",\"code\":\"\",\"columnType\":\"text\",\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\PlainText\"},\"8a46e9b3-393e-4be5-80ed-041c1fbfa309\":{\"contentColumnType\":\"string\",\"fieldGroup\":null,\"handle\":\"mediaLink\",\"instructions\":\"\",\"name\":\"Media Link\",\"searchable\":true,\"settings\":{\"placeholder\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\Url\"}},\"handle\":\"socialMedia\",\"name\":\"Social Media\",\"sortOrder\":1}},\"plugins\":{\"admin-bar\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"3.1.0\"},\"field-manager\":{\"edition\":\"standard\",\"enabled\":false,\"schemaVersion\":\"1.0.0\"},\"freeform\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"2.1.3\",\"settings\":{\"pluginName\":\"Forms\",\"formTemplateDirectory\":null,\"emailTemplateDirectory\":null,\"emailTemplateStorage\":\"db\",\"defaultView\":\"dashboard\",\"fieldDisplayOrder\":\"name\",\"showTutorial\":false,\"removeNewlines\":\"\",\"defaultTemplates\":\"1\",\"footerScripts\":\"0\",\"formSubmitDisable\":\"\",\"freeformHoneypot\":true,\"spamProtectionBehaviour\":\"simulate_success\",\"submissionThrottlingCount\":null,\"submissionThrottlingTimeFrame\":null,\"blockedEmails\":null,\"blockedKeywords\":null,\"blockedKeywordsError\":\"Invalid Entry Data\",\"blockedEmailsError\":\"Invalid Email Address\",\"showErrorsForBlockedEmails\":false,\"showErrorsForBlockedKeywords\":false,\"blockedIpAddresses\":null,\"purgableSubmissionAgeInDays\":\"0\",\"purgableSpamAgeInDays\":null,\"salesforce_client_id\":null,\"salesforce_client_secret\":null,\"salesforce_username\":null,\"salesforce_password\":null,\"spamFolderEnabled\":false,\"recaptchaEnabled\":false,\"recaptchaKey\":null,\"recaptchaSecret\":null,\"renderFormHtmlInCpViews\":\"1\",\"autoScrollToErrors\":\"1\"}},\"mix\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"1.0.0\"},\"navigation\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"1.0.5\"},\"neo\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"2.0.0\"},\"position-fieldtype\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"1.0.0\"},\"redactor\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"2.2.1\"},\"seomatic\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"3.0.8\"},\"super-table\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"2.0.7\"},\"typogrify\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"1.0.0\"},\"video-embedder\":{\"edition\":\"standard\",\"enabled\":true,\"schemaVersion\":\"1.0.0\"}},\"sections\":{\"1e943d8b-5226-4d93-b9a1-030f88420b7e\":{\"enableVersioning\":true,\"entryTypes\":{\"0165d7fd-bf82-487d-82e3-53911dc66c54\":{\"fieldLayouts\":{\"b995cdd6-6eea-435e-8828-4896a3608a34\":{\"tabs\":[{\"fields\":{\"b6fc1e42-0137-49b8-9a62-ea002ced47a5\":{\"required\":false,\"sortOrder\":1}},\"name\":\"Common\",\"sortOrder\":1},{\"fields\":{\"751efea8-484b-4333-85f1-49adeaacebb1\":{\"required\":false,\"sortOrder\":1},\"aac7c6ae-fdd1-4e8d-997d-5fb8a3c64c4c\":{\"required\":false,\"sortOrder\":2}},\"name\":\"Page Options\",\"sortOrder\":2}]}},\"handle\":\"page\",\"hasTitleField\":true,\"name\":\"Page\",\"sortOrder\":1,\"titleFormat\":\"\",\"titleLabel\":\"Title\"}},\"handle\":\"page\",\"name\":\"Page\",\"siteSettings\":{\"4e0fc612-e53b-4ed7-8b23-d723a0c987e8\":{\"enabledByDefault\":true,\"hasUrls\":true,\"template\":\"page/_entry\",\"uriFormat\":\"{slug}\"}},\"structure\":{\"maxLevels\":\"\",\"uid\":\"28148cbe-8f04-486c-9acf-57a4379ee6f9\"},\"type\":\"structure\",\"propagationMethod\":\"all\"},\"626c6290-9346-4bc4-a02f-8da30695dd2d\":{\"enableVersioning\":true,\"entryTypes\":{\"bf493d22-2155-483d-8cc0-b48adee39630\":{\"fieldLayouts\":{\"b593a223-8bc3-4d82-b815-5b1545fcdab1\":{\"tabs\":[{\"fields\":{\"1e4308ab-e0a5-4a36-b76d-4d5d0c1f59af\":{\"required\":false,\"sortOrder\":3},\"980e947d-aa5f-40f3-bbf5-7ab15371e271\":{\"required\":false,\"sortOrder\":1},\"b6fc1e42-0137-49b8-9a62-ea002ced47a5\":{\"required\":false,\"sortOrder\":4},\"c0656cd9-0e0c-4efd-9970-f91a5d729834\":{\"required\":false,\"sortOrder\":2}},\"name\":\"Content\",\"sortOrder\":1}]}},\"handle\":\"blog\",\"hasTitleField\":true,\"name\":\"Blog\",\"sortOrder\":1,\"titleFormat\":\"\",\"titleLabel\":\"Title\"}},\"handle\":\"blog\",\"name\":\"Blog\",\"siteSettings\":{\"4e0fc612-e53b-4ed7-8b23-d723a0c987e8\":{\"enabledByDefault\":true,\"hasUrls\":true,\"template\":\"blog/_entry\",\"uriFormat\":\"blog/{slug}\"}},\"type\":\"channel\",\"propagationMethod\":\"all\"},\"9bcfd187-529c-4aca-83bc-0b68875375b7\":{\"enableVersioning\":true,\"entryTypes\":{\"cb6bdd31-3298-4c0e-9676-069e9f37f4af\":{\"fieldLayouts\":{\"c6180b17-2363-4a81-8ad8-b98460c0bb63\":{\"tabs\":[{\"fields\":{\"b6fc1e42-0137-49b8-9a62-ea002ced47a5\":{\"required\":false,\"sortOrder\":1}},\"name\":\"Content\",\"sortOrder\":1}]}},\"handle\":\"homepage\",\"hasTitleField\":false,\"name\":\"Homepage\",\"sortOrder\":1,\"titleFormat\":\"{section.name|raw}\",\"titleLabel\":\"\"}},\"handle\":\"homepage\",\"name\":\"Homepage\",\"siteSettings\":{\"4e0fc612-e53b-4ed7-8b23-d723a0c987e8\":{\"enabledByDefault\":true,\"hasUrls\":true,\"template\":\"\",\"uriFormat\":\"__home__\"}},\"type\":\"single\",\"propagationMethod\":\"all\"},\"c56e2508-9559-4291-82c0-546311d3961b\":{\"enableVersioning\":true,\"entryTypes\":{\"8d3494c2-1513-45e9-a7f0-87fe59b20d1b\":{\"fieldLayouts\":{\"451aca94-daf3-4f9b-a199-188df903f943\":{\"tabs\":[{\"fields\":{\"a42c7639-d2aa-4b0a-9975-d06886a1cf83\":{\"required\":false,\"sortOrder\":1},\"d386a8dc-011e-4918-b93b-f21e15c8d4b6\":{\"required\":false,\"sortOrder\":2}},\"name\":\"Contact\",\"sortOrder\":1}]}},\"handle\":\"callToAction\",\"hasTitleField\":true,\"name\":\"Call to Action\",\"sortOrder\":1,\"titleFormat\":\"\",\"titleLabel\":\"Title\"}},\"handle\":\"callToAction\",\"name\":\"Call to Action\",\"siteSettings\":{\"4e0fc612-e53b-4ed7-8b23-d723a0c987e8\":{\"enabledByDefault\":false,\"hasUrls\":false,\"template\":null,\"uriFormat\":null}},\"type\":\"channel\",\"propagationMethod\":\"all\"},\"efe0cc28-ce97-4a7e-9ab6-bad947159ab6\":{\"enableVersioning\":true,\"entryTypes\":{\"a74dd557-d92a-4a60-8880-e6d1a7cf5101\":{\"fieldLayouts\":{\"f9f61668-5913-4aaa-8b2c-2150b2e6f12f\":{\"tabs\":[{\"fields\":{\"244270e3-5f4b-4777-a5bc-a8cc022cb5e7\":{\"required\":false,\"sortOrder\":1},\"5868ff9f-7ef8-4c35-ae86-6d360290afce\":{\"required\":false,\"sortOrder\":2},\"75340a1c-b9ff-4c2b-9a66-b3fb23a01d68\":{\"required\":false,\"sortOrder\":4},\"dea7bb72-cedf-434c-8f52-66b7674ac5a7\":{\"required\":false,\"sortOrder\":3}},\"name\":\"Content\",\"sortOrder\":1}]}},\"handle\":\"quotes\",\"hasTitleField\":true,\"name\":\"Quotes\",\"sortOrder\":1,\"titleFormat\":\"\",\"titleLabel\":\"Title\"}},\"handle\":\"quotes\",\"name\":\"Quotes\",\"siteSettings\":{\"4e0fc612-e53b-4ed7-8b23-d723a0c987e8\":{\"enabledByDefault\":true,\"hasUrls\":true,\"template\":\"quotes/_entry.twig\",\"uriFormat\":\"quotes/{slug}\"}},\"type\":\"channel\",\"propagationMethod\":\"all\"}},\"siteGroups\":{\"81be598e-e609-4e22-a752-1250e799527a\":{\"name\":\"Craft CMS\"}},\"sites\":{\"4e0fc612-e53b-4ed7-8b23-d723a0c987e8\":{\"siteGroup\":\"81be598e-e609-4e22-a752-1250e799527a\",\"name\":\"Site Name\",\"handle\":\"default\",\"language\":\"en-US\",\"hasUrls\":true,\"baseUrl\":\"$DEFAULT_SITE_URL\",\"sortOrder\":1,\"primary\":true}},\"superTableBlockTypes\":{\"6201b01a-ad18-4b80-b177-da1103bc45f6\":{\"field\":\"fe88044f-6d76-40a1-b67c-4d1f7a38f62b\",\"fieldLayouts\":{\"5a83b54c-ff15-4d23-9c94-e0977905d3ae\":{\"tabs\":[{\"fields\":{\"37924b31-cc82-4c32-a8b8-45cd0cd69c22\":{\"required\":false,\"sortOrder\":2},\"63bc51eb-c0c2-47c0-b4b3-e6c1763bde93\":{\"required\":false,\"sortOrder\":1}},\"name\":\"Content\",\"sortOrder\":1}]}},\"fields\":{\"37924b31-cc82-4c32-a8b8-45cd0cd69c22\":{\"contentColumnType\":\"text\",\"fieldGroup\":null,\"handle\":\"columnContent\",\"instructions\":\"\",\"name\":\"Content\",\"searchable\":true,\"settings\":{\"availableTransforms\":\"*\",\"availableVolumes\":\"*\",\"cleanupHtml\":\"\",\"columnType\":\"text\",\"purifierConfig\":\"\",\"purifyHtml\":\"1\",\"redactorConfig\":\"Standard.json\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\redactor\\\\Field\"},\"63bc51eb-c0c2-47c0-b4b3-e6c1763bde93\":{\"contentColumnType\":\"string\",\"fieldGroup\":null,\"handle\":\"columnWidth\",\"instructions\":\"\",\"name\":\"Column Width\",\"searchable\":true,\"settings\":{\"options\":[{\"default\":\"\",\"label\":\"17%\",\"value\":\"2\"},{\"default\":\"\",\"label\":\"25%\",\"value\":\"3\"},{\"default\":\"\",\"label\":\"33%\",\"value\":\"4\"},{\"default\":\"1\",\"label\":\"50%\",\"value\":\"6\"},{\"default\":\"\",\"label\":\"66%\",\"value\":\"8\"},{\"default\":\"\",\"label\":\"75%\",\"value\":\"9\"},{\"default\":\"\",\"label\":\"100%\",\"value\":\"12\"}]},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\Dropdown\"}}},\"9f710e28-f3c7-45c0-b11c-ae72aa1d379b\":{\"field\":\"36cbdbd4-5e01-40b8-be3a-7568707136d8\",\"fieldLayouts\":{\"a80581a7-0ff2-4616-ba93-e1f10a8e29ed\":{\"tabs\":[{\"fields\":{\"2ff7f484-0f9a-4725-b5da-ff4242273322\":{\"required\":false,\"sortOrder\":2},\"7fa0e542-86ff-44bb-8d4b-059a8be9dc20\":{\"required\":false,\"sortOrder\":3},\"fe446013-c722-49fc-82a5-c9709a9b7e53\":{\"required\":false,\"sortOrder\":1}},\"name\":\"Content\",\"sortOrder\":1}]}},\"fields\":{\"2ff7f484-0f9a-4725-b5da-ff4242273322\":{\"contentColumnType\":\"text\",\"fieldGroup\":null,\"handle\":\"fpTitle\",\"instructions\":\"\",\"name\":\"Title\",\"searchable\":true,\"settings\":{\"charLimit\":\"\",\"code\":\"\",\"columnType\":\"text\",\"initialRows\":\"4\",\"multiline\":\"\",\"placeholder\":\"\"},\"translationKeyFormat\":null,\"translationMethod\":\"none\",\"type\":\"craft\\\\fields\\\\PlainText\"},\"7fa0e542-86ff-44bb-8d4b-059a8be9dc20\":{\"contentColumnType\":\"string\",\"fieldGroup\":null,\"handle\":\"pageLink\",\"instructions\":\"\",\"name\":\"Page Link\",\"searchable\":true,\"settings\":{\"limit\":\"1\",\"localizeRelations\":false,\"selectionLabel\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"viewMode\":null},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"craft\\\\fields\\\\Entries\"},\"fe446013-c722-49fc-82a5-c9709a9b7e53\":{\"contentColumnType\":\"string\",\"fieldGroup\":null,\"handle\":\"image\",\"instructions\":\"Please use an SVG image if possible\",\"name\":\"Image\",\"searchable\":true,\"settings\":{\"allowedKinds\":null,\"defaultUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"defaultUploadLocationSubpath\":\"\",\"limit\":\"1\",\"localizeRelations\":false,\"restrictFiles\":\"\",\"selectionLabel\":\"\",\"singleUploadLocationSource\":\"volume:4af88935-0c38-4783-9283-a0fa900cc2ce\",\"singleUploadLocationSubpath\":\"\",\"source\":null,\"sources\":\"*\",\"targetSiteId\":null,\"useSingleFolder\":\"\",\"viewMode\":\"large\"},\"translationKeyFormat\":null,\"translationMethod\":\"site\",\"type\":\"craft\\\\fields\\\\Assets\"}}}},\"system\":{\"edition\":\"pro\",\"live\":true,\"name\":\"$SITE_NAME\",\"schemaVersion\":\"3.3.3\",\"timeZone\":\"America/Los_Angeles\"},\"users\":{\"allowPublicRegistration\":false,\"defaultGroup\":null,\"photoSubpath\":\"\",\"photoVolumeUid\":null,\"requireEmailVerification\":true},\"volumes\":{\"4af88935-0c38-4783-9283-a0fa900cc2ce\":{\"handle\":\"default\",\"hasUrls\":true,\"name\":\"Default\",\"settings\":{\"path\":\"@webroot/images/default\"},\"sortOrder\":1,\"type\":\"craft\\\\volumes\\\\Local\",\"url\":\"/images/default\"}}}',NULL,'14EW6gCqStB2','2019-01-21 23:30:43','2019-02-18 19:24:13','a1ab8305-e3c0-476a-919a-3133bec83547');

/*!40000 ALTER TABLE `craft_info` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_matrixblocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_matrixblocks`;

CREATE TABLE `craft_matrixblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `deletedWithOwner` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_matrixblocks_ownerId_idx` (`ownerId`),
  KEY `craft_matrixblocks_fieldId_idx` (`fieldId`),
  KEY `craft_matrixblocks_typeId_idx` (`typeId`),
  KEY `craft_matrixblocks_sortOrder_idx` (`sortOrder`),
  CONSTRAINT `craft_matrixblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `craft_matrixblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_matrixblocks` WRITE;
/*!40000 ALTER TABLE `craft_matrixblocks` DISABLE KEYS */;

INSERT INTO `craft_matrixblocks` (`id`, `ownerId`, `fieldId`, `typeId`, `sortOrder`, `deletedWithOwner`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(22,3,71,9,1,NULL,'2019-01-30 21:46:51','2019-01-30 21:46:51','c23e5b17-9eb9-43c3-91ff-13e0b4c422c3'),
	(23,3,71,9,2,NULL,'2019-01-30 21:46:51','2019-01-30 21:46:51','7fedbdc0-cc44-4d49-a1eb-b42ea0d3113d'),
	(24,3,71,9,3,NULL,'2019-01-30 21:46:51','2019-01-30 21:46:51','beec9525-7379-42af-8694-f4087ad25098'),
	(25,3,71,9,4,NULL,'2019-01-30 21:46:51','2019-01-30 21:46:51','5c807b42-20a7-4ad1-8d10-7e368f23a88e'),
	(63,62,65,8,1,1,'2019-01-30 23:50:26','2019-02-27 21:20:40','06077939-d092-4c9f-8a86-072627399032'),
	(64,62,65,8,2,1,'2019-01-30 23:50:26','2019-02-27 21:20:40','14eaaa5a-adfc-447e-bf42-f766275c2e0b'),
	(65,62,65,8,3,1,'2019-01-30 23:50:26','2019-02-27 21:20:40','3cb1f67d-d01d-48c5-9659-12cf4166c937'),
	(66,62,65,8,4,1,'2019-01-30 23:50:26','2019-02-27 21:20:40','0c6cea72-9427-405b-9c51-3e45b0d08a9d'),
	(129,128,65,8,1,1,'2019-02-07 22:07:10','2019-02-08 18:05:32','f83667ee-e9c6-42c1-b4eb-7ef0554461f6'),
	(130,128,65,8,2,1,'2019-02-07 22:07:10','2019-02-08 18:05:32','f63de0e3-96e7-49f2-9bb7-46574dea2910'),
	(131,128,65,8,3,1,'2019-02-07 22:07:10','2019-02-08 18:05:32','74cbd3c0-7979-4132-9ce6-86cd8766a386'),
	(132,128,65,8,4,1,'2019-02-07 22:07:10','2019-02-08 18:05:32','9dd7dd61-8589-44df-b729-c7ba9e313cde');

/*!40000 ALTER TABLE `craft_matrixblocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_matrixblocktypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_matrixblocktypes`;

CREATE TABLE `craft_matrixblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_matrixblocktypes_name_fieldId_unq_idx` (`name`,`fieldId`),
  UNIQUE KEY `craft_matrixblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `craft_matrixblocktypes_fieldId_idx` (`fieldId`),
  KEY `craft_matrixblocktypes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `craft_matrixblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_matrixblocktypes` WRITE;
/*!40000 ALTER TABLE `craft_matrixblocktypes` DISABLE KEYS */;

INSERT INTO `craft_matrixblocktypes` (`id`, `fieldId`, `fieldLayoutId`, `name`, `handle`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(8,65,69,'Social Media','socialMedia',1,'2019-01-22 16:06:20','2019-01-22 16:06:20','7cca57cb-0702-4a0d-ae12-39dbd320cb8e'),
	(9,71,86,'Social Media','socialMedia',1,'2019-01-30 21:43:52','2019-01-30 21:44:19','56b3fc31-3cd2-4791-83b8-677d9de46e79');

/*!40000 ALTER TABLE `craft_matrixblocktypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_matrixcontent_socialmediablocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_matrixcontent_socialmediablocks`;

CREATE TABLE `craft_matrixcontent_socialmediablocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_socialMedia_mediaIcon` text,
  `field_socialMedia_mediaLink` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_matrixcontent_socialmediablocks_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `craft_matrixcontent_socialmediablocks_siteId_fk` (`siteId`),
  CONSTRAINT `craft_matrixcontent_socialmediablocks_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixcontent_socialmediablocks_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_matrixcontent_socialmediablocks` WRITE;
/*!40000 ALTER TABLE `craft_matrixcontent_socialmediablocks` DISABLE KEYS */;

INSERT INTO `craft_matrixcontent_socialmediablocks` (`id`, `elementId`, `siteId`, `dateCreated`, `dateUpdated`, `uid`, `field_socialMedia_mediaIcon`, `field_socialMedia_mediaLink`)
VALUES
	(1,63,1,'2019-01-30 23:50:26','2019-02-27 21:20:40','f410e48a-fbfe-45d0-a1a4-3a813ec24920','<i class=\"fab fa-facebook-f\"></i>','https://www.facebook.com/'),
	(2,64,1,'2019-01-30 23:50:26','2019-02-27 21:20:40','7aa5df19-b9eb-468a-8c25-85f573ae79e4','<i class=\"fab fa-twitter\"></i>','https://www.facebook.com/'),
	(3,65,1,'2019-01-30 23:50:26','2019-02-27 21:20:40','0a97a6cd-4c6b-4e8c-8fb4-ab2fffadfab6','<i class=\"fab fa-twitter\"></i>','https://www.facebook.com/'),
	(4,66,1,'2019-01-30 23:50:26','2019-02-27 21:20:40','d119f7fe-c265-4f1a-9736-2a483107d2c7','<i class=\"fab fa-instagram\"></i>','https://www.facebook.com/'),
	(5,129,1,'2019-02-07 22:07:10','2019-02-08 18:05:32','5f0d0b82-ea6b-4c78-919e-65d161f6aee6','<i class=\"fab fa-facebook-f\"></i>','https://www.facebook.com/groups/1931603617055357/?source_id=520600891399451'),
	(6,130,1,'2019-02-07 22:07:10','2019-02-08 18:05:32','5b9be4ec-5da0-4a75-b579-cef316ee96da','<i class=\"fab fa-twitter\"></i>','https://twitter.com/OtterCares'),
	(7,131,1,'2019-02-07 22:07:10','2019-02-08 18:05:32','0d7fd8ed-1722-416f-9c10-95bd499c2294','<i class=\"fab fa-instagram\"></i>','https://www.instagram.com/ottercares/'),
	(8,132,1,'2019-02-07 22:07:10','2019-02-08 18:05:32','722d4f60-4da6-4d4c-875c-5627b6d0c230','<i class=\"fab fa-youtube\"></i>','https://www.youtube.com/user/OtterCaresFoundation');

/*!40000 ALTER TABLE `craft_matrixcontent_socialmediablocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_matrixcontent_socialnetworks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_matrixcontent_socialnetworks`;

CREATE TABLE `craft_matrixcontent_socialnetworks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_socialMedia_iconhtml` text,
  `field_socialMedia_socialLink` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_matrixcontent_socialnetworks_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `craft_matrixcontent_socialnetworks_siteId_fk` (`siteId`),
  CONSTRAINT `craft_matrixcontent_socialnetworks_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_matrixcontent_socialnetworks_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_matrixcontent_socialnetworks` WRITE;
/*!40000 ALTER TABLE `craft_matrixcontent_socialnetworks` DISABLE KEYS */;

INSERT INTO `craft_matrixcontent_socialnetworks` (`id`, `elementId`, `siteId`, `dateCreated`, `dateUpdated`, `uid`, `field_socialMedia_iconhtml`, `field_socialMedia_socialLink`)
VALUES
	(1,22,1,'2019-01-30 21:46:51','2019-01-30 21:46:51','7659fbfd-b949-4a9a-9ca7-985d351c4376','<i class=\"fab fa-facebook-f\"></i>','https://www.facebook.com/'),
	(2,23,1,'2019-01-30 21:46:51','2019-01-30 21:46:51','d790abf7-a8d1-4377-bdef-e1089f2b48b7','<i class=\"fab fa-twitter\"></i>','https://twitter.com/'),
	(3,24,1,'2019-01-30 21:46:51','2019-01-30 21:46:51','c204a4f8-0f76-4235-b7cd-0c61a8c14973','<i class=\"fab fa-instagram\"></i>','https://www.instagram.com'),
	(4,25,1,'2019-01-30 21:46:51','2019-01-30 21:46:51','e52bcad7-4d5c-4caf-aa21-dff886a17a47','<i class=\"fab fa-youtube\"></i>','https://www.youtube.com/');

/*!40000 ALTER TABLE `craft_matrixcontent_socialnetworks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_migrations`;

CREATE TABLE `craft_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pluginId` int(11) DEFAULT NULL,
  `type` enum('app','plugin','content') NOT NULL DEFAULT 'app',
  `name` varchar(255) NOT NULL,
  `applyTime` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_migrations_pluginId_idx` (`pluginId`),
  KEY `craft_migrations_type_pluginId_idx` (`type`,`pluginId`),
  CONSTRAINT `craft_migrations_pluginId_fk` FOREIGN KEY (`pluginId`) REFERENCES `craft_plugins` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_migrations` WRITE;
/*!40000 ALTER TABLE `craft_migrations` DISABLE KEYS */;

INSERT INTO `craft_migrations` (`id`, `pluginId`, `type`, `name`, `applyTime`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,'app','Install','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','fca20b95-f8e8-4897-8c2e-66fe76341946'),
	(2,NULL,'app','m150403_183908_migrations_table_changes','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','590f85f3-c926-404d-bfa0-e4e405b6c689'),
	(3,NULL,'app','m150403_184247_plugins_table_changes','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','332e058b-e883-4fb2-90b8-4b2f69f6c925'),
	(4,NULL,'app','m150403_184533_field_version','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','ce3cdf8a-6e41-4278-bb49-75663a651881'),
	(5,NULL,'app','m150403_184729_type_columns','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','f74ec0b7-2f3e-491e-aae0-56fe8a4cde26'),
	(6,NULL,'app','m150403_185142_volumes','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','edfea6fc-46d0-449b-b5dd-aa99d8855ebf'),
	(7,NULL,'app','m150428_231346_userpreferences','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','b81cbb4c-be8d-4697-835c-bf60c82642c9'),
	(8,NULL,'app','m150519_150900_fieldversion_conversion','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','5871fcb8-6cf5-499f-b4f4-524cf4f11119'),
	(9,NULL,'app','m150617_213829_update_email_settings','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','6708bcd9-82a8-44c7-95cb-28a0db2b8bc0'),
	(10,NULL,'app','m150721_124739_templatecachequeries','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','4b2d2172-bc93-4809-a26e-5048375575a6'),
	(11,NULL,'app','m150724_140822_adjust_quality_settings','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','e706214b-6cf4-462c-bc87-2d9ba3cba84c'),
	(12,NULL,'app','m150815_133521_last_login_attempt_ip','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','adf127d6-ce22-407a-8f23-d1459740e004'),
	(13,NULL,'app','m151002_095935_volume_cache_settings','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','1a1b68df-a6fe-497a-85c4-65b63ebdb25c'),
	(14,NULL,'app','m151005_142750_volume_s3_storage_settings','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','f2cff33c-fcb6-468d-817f-81b969040204'),
	(15,NULL,'app','m151016_133600_delete_asset_thumbnails','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','bd1e0ac9-45e3-4ab3-9229-bc932d543ac7'),
	(16,NULL,'app','m151209_000000_move_logo','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','a7608b7e-a0f2-4625-85f8-c01248a89741'),
	(17,NULL,'app','m151211_000000_rename_fileId_to_assetId','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','d6912961-8531-40f0-93a5-c1bf663a443e'),
	(18,NULL,'app','m151215_000000_rename_asset_permissions','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','23404ec7-c180-4c29-bfca-3af471cd737f'),
	(19,NULL,'app','m160707_000001_rename_richtext_assetsource_setting','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','dc4e8237-853b-453e-abd1-6fb3715a39a0'),
	(20,NULL,'app','m160708_185142_volume_hasUrls_setting','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','d0cb8588-14f7-40c1-8e0c-41461f7ade1e'),
	(21,NULL,'app','m160714_000000_increase_max_asset_filesize','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','040ae57b-8516-4c22-a37f-88241ec93b4d'),
	(22,NULL,'app','m160727_194637_column_cleanup','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','be65da9f-99b4-41c2-bbf1-5de5783dc786'),
	(23,NULL,'app','m160804_110002_userphotos_to_assets','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','7f71a205-a618-424d-8efd-0cf414188afe'),
	(24,NULL,'app','m160807_144858_sites','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','ea42337c-52f2-428f-bd0e-e6a7c904e80d'),
	(25,NULL,'app','m160829_000000_pending_user_content_cleanup','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','39866369-f16b-4822-81b1-0c5db8e0d27d'),
	(26,NULL,'app','m160830_000000_asset_index_uri_increase','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','5e74cabf-2b4a-482e-850e-97910ed51f2b'),
	(27,NULL,'app','m160912_230520_require_entry_type_id','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','0d91d252-8f00-4628-9b5a-2462403d8b42'),
	(28,NULL,'app','m160913_134730_require_matrix_block_type_id','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','8b0c5485-6fd7-4a55-9396-1b40c40d9522'),
	(29,NULL,'app','m160920_174553_matrixblocks_owner_site_id_nullable','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','20166bee-6f46-4e0c-aa1a-9bc17dd9a911'),
	(30,NULL,'app','m160920_231045_usergroup_handle_title_unique','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','03f48277-ec7f-4d22-b16f-54793cf8c464'),
	(31,NULL,'app','m160925_113941_route_uri_parts','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','ec05022e-de4c-446a-aee6-9ce2ada6fecf'),
	(32,NULL,'app','m161006_205918_schemaVersion_not_null','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','8808f53a-ffa5-46d0-b495-66dbb62364d1'),
	(33,NULL,'app','m161007_130653_update_email_settings','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','b81fc06e-76de-40ce-be04-ff2c22d949c8'),
	(34,NULL,'app','m161013_175052_newParentId','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','57338a66-f3d7-4568-bf56-ac2610ac6261'),
	(35,NULL,'app','m161021_102916_fix_recent_entries_widgets','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','9ff5083f-ae65-4db3-b7c4-60ce55740c2c'),
	(36,NULL,'app','m161021_182140_rename_get_help_widget','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','18f774fc-3617-4b1f-956b-76a53b52c6b8'),
	(37,NULL,'app','m161025_000000_fix_char_columns','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','f7c3d1c7-6681-4388-8d72-35fd2fda7915'),
	(38,NULL,'app','m161029_124145_email_message_languages','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','78c1f920-b130-4a96-b7ad-dbf7ae71d8ff'),
	(39,NULL,'app','m161108_000000_new_version_format','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','84c11582-59e3-463f-b4d8-1ccbf9f98357'),
	(40,NULL,'app','m161109_000000_index_shuffle','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','1217bc94-eea4-4f16-8674-deb25309667e'),
	(41,NULL,'app','m161122_185500_no_craft_app','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','9e8e20b2-33f0-4f75-9a3f-7259cc373222'),
	(42,NULL,'app','m161125_150752_clear_urlmanager_cache','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','1607c37c-7c3c-40da-b112-4aa7572474ba'),
	(43,NULL,'app','m161220_000000_volumes_hasurl_notnull','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','b151fba8-7687-4bb6-874f-d2ddfe58d4d5'),
	(44,NULL,'app','m170114_161144_udates_permission','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','a15fb441-d41c-42d6-915b-1981e5ce40fb'),
	(45,NULL,'app','m170120_000000_schema_cleanup','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','af6644c4-7a38-4ee2-8778-9b6c9c77153e'),
	(46,NULL,'app','m170126_000000_assets_focal_point','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','9e1611af-a8b2-42d2-8348-49f77584c9e1'),
	(47,NULL,'app','m170206_142126_system_name','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','2d4e0d65-ef8d-4177-9404-98a92f8f1594'),
	(48,NULL,'app','m170217_044740_category_branch_limits','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','5fdb5bc0-ea72-4449-a8ab-35c4a0b1cf2f'),
	(49,NULL,'app','m170217_120224_asset_indexing_columns','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','ca1a1230-dae8-443d-a2d5-fd4239aee2ea'),
	(50,NULL,'app','m170223_224012_plain_text_settings','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','06a2a4fe-1ec7-475e-b362-0553b5150a51'),
	(51,NULL,'app','m170227_120814_focal_point_percentage','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','6ef739fd-3920-4d4c-81e6-a9041d74a8ca'),
	(52,NULL,'app','m170228_171113_system_messages','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','d0416cd3-adb2-4820-a377-1daf85ac2a87'),
	(53,NULL,'app','m170303_140500_asset_field_source_settings','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','e9a61271-20ca-48d3-b478-ee77fe801e21'),
	(54,NULL,'app','m170306_150500_asset_temporary_uploads','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','f08bfbd2-5da0-4bc0-9825-b447f480dcc5'),
	(55,NULL,'app','m170523_190652_element_field_layout_ids','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','5b735175-83fc-4910-b2f1-0f8543517787'),
	(56,NULL,'app','m170612_000000_route_index_shuffle','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','29bda7f6-9088-4da5-a69f-ded4cd7aa12b'),
	(57,NULL,'app','m170621_195237_format_plugin_handles','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','1e2a2c5d-e021-4352-82f8-411ab357bfc1'),
	(58,NULL,'app','m170630_161027_deprecation_line_nullable','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','276effc3-21a2-44ee-a800-69416edea629'),
	(59,NULL,'app','m170630_161028_deprecation_changes','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','5702d2dc-2f83-4394-b4a6-e9f8946951c4'),
	(60,NULL,'app','m170703_181539_plugins_table_tweaks','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','c2bf28b2-9171-476e-baa6-663d22e602f3'),
	(61,NULL,'app','m170704_134916_sites_tables','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','f11ca1cc-1532-4aa9-8cd5-8ac7ecfcab6a'),
	(62,NULL,'app','m170706_183216_rename_sequences','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','06bef79f-25f3-4e19-b0ae-8c55356dfcaf'),
	(63,NULL,'app','m170707_094758_delete_compiled_traits','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','3e5cc61b-9620-4811-9742-042557fa625b'),
	(64,NULL,'app','m170731_190138_drop_asset_packagist','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','7842b6cc-bad6-4463-b713-be1199159ae1'),
	(65,NULL,'app','m170810_201318_create_queue_table','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','310ac8ae-3efd-47cb-8185-f837d7590dec'),
	(66,NULL,'app','m170816_133741_delete_compiled_behaviors','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','f19b3bdd-8906-4f09-9d8b-5457fcbdf8a3'),
	(67,NULL,'app','m170903_192801_longblob_for_queue_jobs','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','83f2a4e8-aba1-4ae0-8216-690cbe129ea5'),
	(68,NULL,'app','m170914_204621_asset_cache_shuffle','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','b2fc41f5-6648-450a-b07b-1a86cf0a0647'),
	(69,NULL,'app','m171011_214115_site_groups','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','f9762041-767c-4f73-8fb9-d88d7e9892e6'),
	(70,NULL,'app','m171012_151440_primary_site','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','c3246ee5-a4f0-4d97-94d1-32e16c667065'),
	(71,NULL,'app','m171013_142500_transform_interlace','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','38487364-bf39-4477-a7ff-940cd2743f79'),
	(72,NULL,'app','m171016_092553_drop_position_select','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','514d1f85-ae26-4c86-a402-1d3858a6c891'),
	(73,NULL,'app','m171016_221244_less_strict_translation_method','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','a22db310-cb64-472e-84bd-b9e02f44811d'),
	(74,NULL,'app','m171107_000000_assign_group_permissions','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','dd7b1ab7-bdac-4e18-86a5-52740d45338e'),
	(75,NULL,'app','m171117_000001_templatecache_index_tune','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','8130890c-5879-40d4-b678-ad782ef9ccc9'),
	(76,NULL,'app','m171126_105927_disabled_plugins','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','2dccbdf1-10f1-49bc-bf7c-373d26ddadd1'),
	(77,NULL,'app','m171130_214407_craftidtokens_table','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','5dc85df2-2f0f-4399-b28f-58b79060e858'),
	(78,NULL,'app','m171202_004225_update_email_settings','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','8a9ffefb-bda0-458e-a644-6e7e2284cec1'),
	(79,NULL,'app','m171204_000001_templatecache_index_tune_deux','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','c897d60a-4c6e-4b1e-a4d6-6b68b94f6137'),
	(80,NULL,'app','m171205_130908_remove_craftidtokens_refreshtoken_column','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','94c78460-2c4f-4e67-857f-d42810fc4c9c'),
	(81,NULL,'app','m171218_143135_longtext_query_column','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','d757d34c-1144-4a4c-87ae-ed1f19383970'),
	(82,NULL,'app','m171231_055546_environment_variables_to_aliases','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','83593a71-c848-4a5a-b2b7-30acbafeb7b6'),
	(83,NULL,'app','m180113_153740_drop_users_archived_column','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','df25fa44-b2e6-464e-8f80-a4b6058d2f73'),
	(84,NULL,'app','m180122_213433_propagate_entries_setting','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','e9ea4a3b-dcf8-40aa-a36a-1e4b3d3ec0d6'),
	(85,NULL,'app','m180124_230459_fix_propagate_entries_values','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','85defdfa-6f9c-4983-8f49-1c1e309949da'),
	(86,NULL,'app','m180128_235202_set_tag_slugs','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','478e46c3-b2d3-443b-9ce4-38d70d7f2ca4'),
	(87,NULL,'app','m180202_185551_fix_focal_points','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','2b4d9ebf-e8d6-4067-947a-bf69fff3a724'),
	(88,NULL,'app','m180217_172123_tiny_ints','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','70834245-3649-4399-ab9c-db51f2c21c59'),
	(89,NULL,'app','m180321_233505_small_ints','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','3742e77a-ae56-4a29-8176-4f26aaba68c8'),
	(90,NULL,'app','m180328_115523_new_license_key_statuses','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','59025588-e4fe-4c13-9a8f-b4310738a44d'),
	(91,NULL,'app','m180404_182320_edition_changes','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','19f4cb92-f24b-421a-b36c-353ec5824f11'),
	(92,NULL,'app','m180411_102218_fix_db_routes','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','6827c941-26cc-433f-a15b-1b3d72f8de71'),
	(93,NULL,'app','m180416_205628_resourcepaths_table','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','d9ffb076-f140-46bf-a14b-a06019010fa4'),
	(94,NULL,'app','m180418_205713_widget_cleanup','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','51244a01-0e22-4b50-971c-e39caaaf3238'),
	(95,NULL,'app','m180425_203349_searchable_fields','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','6df47c6b-104a-4d91-9b16-dc785ef3fef7'),
	(96,NULL,'app','m180516_153000_uids_in_field_settings','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','78d2528a-02cd-4cfa-b0d9-746cd7f34854'),
	(97,NULL,'app','m180517_173000_user_photo_volume_to_uid','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','0ecbd3c6-1095-4ff0-8432-59a54fe44d2d'),
	(98,NULL,'app','m180518_173000_permissions_to_uid','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','8699ec50-3b0f-4596-8612-a8848900c432'),
	(99,NULL,'app','m180520_173000_matrix_context_to_uids','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','b0a9d74c-a99f-4cc7-a524-95e9f1510619'),
	(100,NULL,'app','m180521_173000_initial_yml_and_snapshot','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','5fb09e09-6674-4180-a294-5ad2d61099b3'),
	(101,NULL,'app','m180731_162030_soft_delete_sites','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','94401b25-49f8-4c67-aeec-82fe64755129'),
	(102,NULL,'app','m180810_214427_soft_delete_field_layouts','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','f73be40b-7fa8-4fdc-b06a-18c002bff67d'),
	(103,NULL,'app','m180810_214439_soft_delete_elements','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','5f8a78f9-8d89-417d-926e-106241efd594'),
	(104,NULL,'app','m180824_193422_case_sensitivity_fixes','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','bcf37428-1862-40b9-be7e-a8b34fa617be'),
	(105,NULL,'app','m180901_151639_fix_matrixcontent_tables','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','a4583f0c-aa97-465f-8103-6205879b0bd2'),
	(106,NULL,'app','m180904_112109_permission_changes','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','e5b88e0f-8f02-4304-ab56-481df3d965a3'),
	(107,NULL,'app','m180910_142030_soft_delete_sitegroups','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','b6cb5b3f-460f-4b15-8c22-750b02316fe3'),
	(108,NULL,'app','m181011_160000_soft_delete_asset_support','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','b207be07-f806-49be-835d-a902f0444ecd'),
	(109,NULL,'app','m181016_183648_set_default_user_settings','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','138e5bf9-d527-457f-a5b0-1fb75f90d9d7'),
	(110,NULL,'app','m181017_225222_system_config_settings','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','3b2ed891-2653-4e8c-b467-af0eccc8d555'),
	(111,NULL,'app','m181018_222343_drop_userpermissions_from_config','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','9e659971-ada5-4e0b-8039-8a8053d70cea'),
	(112,NULL,'app','m181029_130000_add_transforms_routes_to_config','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','4865037f-5870-41fa-8b57-45a2f01949a4'),
	(113,NULL,'app','m181112_203955_sequences_table','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','bb8ee486-0ddd-4ac2-a07d-7de1c0af5e8b'),
	(114,NULL,'app','m181121_001712_cleanup_field_configs','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','a7cbf4c4-fc46-4793-aa98-685acbe9aedc'),
	(115,NULL,'app','m181128_193942_fix_project_config','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','95e48ee6-e8ac-4925-9ff1-2de3a3462aa6'),
	(116,NULL,'app','m181130_143040_fix_schema_version','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','715bbda1-2536-474e-899c-1359e81a5d96'),
	(117,NULL,'app','m181211_143040_fix_entry_type_uids','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','a5f53f73-80a6-4a1b-a07d-ab66d4827115'),
	(118,NULL,'app','m181213_102500_config_map_aliases','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','7c9f51c6-5361-4883-bd0b-834b52a5f8d1'),
	(119,NULL,'app','m181217_153000_fix_structure_uids','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','fc3ed2bb-53ac-400d-8792-9b6d033f026d'),
	(120,NULL,'app','m190104_152725_store_licensed_plugin_editions','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','9290df21-1256-4cce-b73e-b181f7e21bd4'),
	(121,NULL,'app','m190108_110000_cleanup_project_config','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','22dfd012-b9d4-42f3-bc4f-312572e2eaff'),
	(122,NULL,'app','m190108_113000_asset_field_setting_change','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','7e4cef30-7aea-4f6f-a7d1-40a9aa7d1ccc'),
	(123,NULL,'app','m190109_172845_fix_colspan','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','20387998-c828-4b25-a418-0d7ec165f776'),
	(124,NULL,'app','m190110_150000_prune_nonexisting_sites','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','24ada009-f1dd-466e-a08b-c65b37ba0a2c'),
	(125,NULL,'app','m190110_214819_soft_delete_volumes','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','bbcade09-ebe1-4791-86e3-3fdfd8f99c3e'),
	(126,NULL,'app','m190112_124737_fix_user_settings','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','c26a4658-309a-4174-93fb-41741f737a46'),
	(127,NULL,'app','m190112_131225_fix_field_layouts','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','bb9ec25a-4d6a-4284-b664-197db4e5711a'),
	(128,NULL,'app','m190112_201010_more_soft_deletes','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','2cd976b6-1b8d-4bd7-b9d8-03ff141cb4e6'),
	(129,NULL,'app','m190114_143000_more_asset_field_setting_changes','2019-01-21 23:30:49','2019-01-21 23:30:49','2019-01-21 23:30:49','657af74d-f5ba-4fd7-ad00-1450df5ede87'),
	(130,NULL,'app','m190121_120000_rich_text_config_setting','2019-01-21 23:30:50','2019-01-21 23:30:50','2019-01-21 23:30:50','4152dd57-0709-4f63-bc80-72185483996e'),
	(131,NULL,'app','m190218_143000_element_index_settings_uid','2019-01-21 23:30:50','2019-01-21 23:30:50','2019-01-21 23:30:50','1a43f035-8b73-47fe-9ba7-b8f1fb229949'),
	(132,1,'plugin','m180430_204710_remove_old_plugins','2019-01-21 23:33:32','2019-01-21 23:33:32','2019-01-21 23:33:32','2a36b182-78d3-4685-a9b7-797b4cecc245'),
	(133,1,'plugin','Install','2019-01-21 23:33:32','2019-01-21 23:33:32','2019-01-21 23:33:32','3175e9fb-7798-413d-8a21-e8adbcc769ee'),
	(134,1,'plugin','m181101_110000_ids_in_settings_to_uids','2019-01-21 23:33:32','2019-01-21 23:33:32','2019-01-21 23:33:32','5d48fe83-f730-40f6-8daa-4999e0aa6666'),
	(135,6,'plugin','Install','2019-01-21 23:35:44','2019-01-21 23:35:44','2019-01-21 23:35:44','5ad93b30-3694-409e-a1ce-c942a0c0cddb'),
	(136,6,'plugin','m180826_000000_propagate_nav_setting','2019-01-21 23:35:44','2019-01-21 23:35:44','2019-01-21 23:35:44','740b57ec-3c58-4b00-8738-c257c3dd62cb'),
	(137,6,'plugin','m180827_000000_propagate_nav_setting_additional','2019-01-21 23:35:44','2019-01-21 23:35:44','2019-01-21 23:35:44','f53a8d48-232e-4c8b-87f0-5e52443ee974'),
	(138,6,'plugin','m181110_000000_add_elementSiteId','2019-01-21 23:35:44','2019-01-21 23:35:44','2019-01-21 23:35:44','cdc54fea-18c2-4843-9ffd-cc62019feaa8'),
	(139,6,'plugin','m181123_000000_populate_elementSiteIds','2019-01-21 23:35:44','2019-01-21 23:35:44','2019-01-21 23:35:44','1b2ff310-fc41-438a-8936-199d119e2bd3'),
	(140,7,'plugin','Install','2019-01-21 23:35:55','2019-01-21 23:35:55','2019-01-21 23:35:55','2837ea11-07e3-446f-aff9-d7e171bf6997'),
	(141,7,'plugin','m181022_123749_craft3_upgrade','2019-01-21 23:35:55','2019-01-21 23:35:55','2019-01-21 23:35:55','68dd3b7b-a8ec-4385-aaca-668b2df435ba'),
	(142,8,'plugin','Install','2019-01-21 23:36:06','2019-01-21 23:36:06','2019-01-21 23:36:06','eeb6d001-e667-4963-8e6d-131bbbe8ab1d'),
	(143,8,'plugin','m180210_000000_migrate_content_tables','2019-01-21 23:36:06','2019-01-21 23:36:06','2019-01-21 23:36:06','d139790c-0237-4b62-be4d-acb7943ce4bb'),
	(144,8,'plugin','m180211_000000_type_columns','2019-01-21 23:36:06','2019-01-21 23:36:06','2019-01-21 23:36:06','4b431332-7cb4-4fa3-af11-13a1e1e95eb1'),
	(145,8,'plugin','m180219_000000_sites','2019-01-21 23:36:06','2019-01-21 23:36:06','2019-01-21 23:36:06','fd313b44-5e72-4340-936d-a257ba2780ea'),
	(146,8,'plugin','m180220_000000_fix_context','2019-01-21 23:36:06','2019-01-21 23:36:06','2019-01-21 23:36:06','3fec44a8-5363-4dc5-8ded-28903b3bb965'),
	(147,8,'plugin','m190117_000000_soft_deletes','2019-01-21 23:36:06','2019-01-21 23:36:06','2019-01-21 23:36:06','87f5c909-deea-4776-9b47-3aba4b626d8c'),
	(148,8,'plugin','m190117_000001_context_to_uids','2019-01-21 23:36:06','2019-01-21 23:36:06','2019-01-21 23:36:06','7077c99c-5f72-4ecd-ad6c-d5e5ec7e6bf0'),
	(149,8,'plugin','m190120_000000_fix_supertablecontent_tables','2019-01-21 23:36:06','2019-01-21 23:36:06','2019-01-21 23:36:06','4de14462-83d4-4723-8907-bb9c755721a8'),
	(150,NULL,'app','m190125_191628_fix_email_transport_password','2019-01-30 21:10:36','2019-01-30 21:10:36','2019-01-30 21:10:36','58bcbfe5-c12a-4612-b2e9-fdb7815138ff'),
	(151,NULL,'app','m190128_181422_cleanup_volume_folders','2019-01-30 21:10:36','2019-01-30 21:10:36','2019-01-30 21:10:36','6324da4c-feca-43a5-b5be-211fcc80a11b'),
	(152,10,'plugin','Install','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','61ecaf6c-7a6a-4437-93e6-08cdf019d73c'),
	(153,10,'plugin','m180120_140521_CraftUpgrade','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','b7e4e3ce-b05f-4ab0-9ca2-5bd8995d39f1'),
	(154,10,'plugin','m180125_124339_UpdateForeignKeyNames','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','dbaafd15-75cd-44f6-a3a5-e9e8f3a58357'),
	(155,10,'plugin','m180214_094247_AddUniqueTokenToSubmissionsAndForms','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','64c108c1-86cd-42a1-817a-c4febfd38e2f'),
	(156,10,'plugin','m180220_072652_ChangeFileUploadFieldColumnType','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','869969a8-8f75-4194-991d-2040ada38f7f'),
	(157,10,'plugin','m180326_094124_AddIsSpamToSubmissions','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','6d11214e-7ca2-4e23-b986-8e0db655504f'),
	(158,10,'plugin','m180405_101920_AddIpAddressToSubmissions','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','dfd6c14c-e0a3-49d8-bb77-dcc18686aa31'),
	(159,10,'plugin','m180410_131206_CreateIntegrationsQueue','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','ab17ff39-d1ff-4b2e-bad6-3be96214c13b'),
	(160,10,'plugin','m180417_134527_AddMultipleSelectTypeToFields','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','4a18c2c1-cfd1-43d2-8f3f-c2e2a0ed7714'),
	(161,10,'plugin','m180430_151626_PaymentGateways','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','ee8e883a-e60e-4b85-a164-22d00820243f'),
	(162,10,'plugin','m180508_095131_CreatePaymentGatewayFieldsTable','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','8d1bb8dd-f8f2-4502-9486-f1b45af726f4'),
	(163,10,'plugin','m180606_141402_AddConnectionsToFormProperties','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','2b0191ed-d116-421c-a513-1fc55f5e406f'),
	(164,10,'plugin','m180730_171628_AddCcDetailsFieldType','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','1c1b2a8a-73b4-4d9a-8239-d1da5449da78'),
	(165,10,'plugin','m180817_091801_AddRulesToFormProperties','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','0f65f824-0964-45d5-acba-79061486b1a1'),
	(166,10,'plugin','m181112_152751_ChangeTypeEnumColumnsToIndexedText','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','174b312d-61a5-4714-b84a-e6c30add8544'),
	(167,10,'plugin','m181129_083939_ChangeIntegrationFieldTypeColumnTypeToString','2019-01-30 21:13:43','2019-01-30 21:13:43','2019-01-30 21:13:43','252b3a6b-a6d5-4ec1-a300-6b0cf24e0bd6'),
	(168,11,'plugin','Install','2019-01-30 21:13:52','2019-01-30 21:13:52','2019-01-30 21:13:52','a94b4ef3-0723-439b-9b18-d8d51535d29f'),
	(169,11,'plugin','m180314_002755_field_type','2019-01-30 21:13:52','2019-01-30 21:13:52','2019-01-30 21:13:52','dfed7347-59c6-45b7-a591-dec5d04a6e9a'),
	(170,11,'plugin','m180314_002756_base_install','2019-01-30 21:13:52','2019-01-30 21:13:52','2019-01-30 21:13:52','f05b7314-4590-4dd0-821a-1c9dd8800099'),
	(171,11,'plugin','m180502_202319_remove_field_metabundles','2019-01-30 21:13:52','2019-01-30 21:13:52','2019-01-30 21:13:52','b5e7b49c-9756-4cdd-8f17-a37722e8d3f7'),
	(172,11,'plugin','m180711_024947_commerce_products','2019-01-30 21:13:52','2019-01-30 21:13:52','2019-01-30 21:13:52','2cd62a81-dcdf-4c0e-b723-e6db8bc96ded'),
	(173,NULL,'app','m190205_140000_fix_asset_soft_delete_index','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','daf575f1-3934-48c6-a337-3dc347d75e06'),
	(174,NULL,'app','m190208_140000_reset_project_config_mapping','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','723ae57e-7007-4abd-aab5-b88a20f36f4f'),
	(175,NULL,'app','m190312_152740_element_revisions','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','ecd37f57-3138-4f9b-956b-2d0b78fea3b6'),
	(176,NULL,'app','m190327_235137_propagation_method','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','73981fc3-a332-45e2-b776-7635fe29a11e'),
	(177,NULL,'app','m190401_223843_drop_old_indexes','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','4c0ca93d-b749-4aff-8e1c-671a1912cff7'),
	(178,NULL,'app','m190416_014525_drop_unique_global_indexes','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','060d6929-7f18-4996-a1cd-cd7c8bf7036d'),
	(179,NULL,'app','m190417_085010_add_image_editor_permissions','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','3f0f1b53-ded7-4b71-a521-39bdc3994244'),
	(180,NULL,'app','m190502_122019_store_default_user_group_uid','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','90076e19-996c-421d-9382-89b38582ec50'),
	(181,NULL,'app','m190504_150349_preview_targets','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','86528667-9107-461b-9831-5b0469b7810f'),
	(182,NULL,'app','m190516_184711_job_progress_label','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','a10bbf59-4dfa-443d-a26a-e71cd255d6fd'),
	(183,NULL,'app','m190523_190303_optional_revision_creators','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','c163b073-d921-4464-a353-c35b560aa312'),
	(184,NULL,'app','m190529_204501_fix_duplicate_uids','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','bae14ed0-ac56-4db1-98c7-b1f4eff49516'),
	(185,NULL,'app','m190605_223807_unsaved_drafts','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','333b5689-525e-44c7-959f-3f3a8b932b95'),
	(186,NULL,'app','m190607_230042_entry_revision_error_tables','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','7c0234d1-3dce-4b09-a706-35629646daf7'),
	(187,NULL,'app','m190608_033429_drop_elements_uid_idx','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','a30459f0-fece-40f3-8e02-062a07a0872b'),
	(188,NULL,'app','m190617_164400_add_gqlschemas_table','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','61df7ec9-fa53-4311-a4f9-3dbf2bfdae20'),
	(189,NULL,'app','m190624_234204_matrix_propagation_method','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','6296dbff-b17b-4ad3-ae17-e16e90610837'),
	(190,NULL,'app','m190711_153020_drop_snapshots','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','dd10f76c-4a6f-485a-bfc3-785970c6fc76'),
	(191,NULL,'app','m190712_195914_no_draft_revisions','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','a1d219ee-7b5b-43c0-a819-4866b6b549fc'),
	(192,NULL,'app','m190723_140314_fix_preview_targets_column','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','b7af48db-ccf6-4d1e-8a25-fb6b9a52fc69'),
	(193,NULL,'app','m190820_003519_flush_compiled_templates','2019-11-07 15:47:13','2019-11-07 15:47:13','2019-11-07 15:47:13','9f631235-62fa-4ad1-bea9-1e478413dea2'),
	(194,NULL,'app','m190823_020339_optional_draft_creators','2019-11-07 15:47:14','2019-11-07 15:47:14','2019-11-07 15:47:14','32f29e0d-f300-488c-9980-507928beaf87'),
	(195,11,'plugin','m190401_220828_longer_handles','2019-11-07 15:56:07','2019-11-07 15:56:07','2019-11-07 15:56:07','26deca09-1b89-4e22-93fe-4e6fbe191d42'),
	(196,11,'plugin','m190518_030221_calendar_events','2019-11-07 15:56:07','2019-11-07 15:56:07','2019-11-07 15:56:07','768f3e8c-e8ac-409d-a485-1e30139954ae');

/*!40000 ALTER TABLE `craft_migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_navigation_navs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_navigation_navs`;

CREATE TABLE `craft_navigation_navs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `propagateNodes` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_navigation_navs_handle_unq_idx` (`handle`),
  KEY `craft_navigation_navs_structureId_idx` (`structureId`),
  CONSTRAINT `craft_navigation_navs_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_navigation_navs` WRITE;
/*!40000 ALTER TABLE `craft_navigation_navs` DISABLE KEYS */;

INSERT INTO `craft_navigation_navs` (`id`, `structureId`, `name`, `handle`, `sortOrder`, `propagateNodes`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,3,'Main Navigation','mainNavigation',NULL,0,'2019-01-30 21:51:30','2019-01-30 21:51:30','1c6a3ee0-4b30-4d62-aee5-08faa43e3f8b'),
	(2,4,'Footer Navigation','footerNavigation',NULL,0,'2019-01-30 21:51:45','2019-01-30 21:54:38','b055f15e-44ce-4500-99e1-211d62b52fa4');

/*!40000 ALTER TABLE `craft_navigation_navs` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_navigation_nodes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_navigation_nodes`;

CREATE TABLE `craft_navigation_nodes` (
  `id` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `elementSiteId` int(11) DEFAULT NULL,
  `navId` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `classes` varchar(255) DEFAULT NULL,
  `newWindow` tinyint(1) DEFAULT '0',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_navigation_nodes_elementSiteId_idx` (`elementSiteId`),
  KEY `craft_navigation_nodes_navId_idx` (`navId`),
  KEY `craft_navigation_nodes_elementId_fk` (`elementId`),
  CONSTRAINT `craft_navigation_nodes_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_navigation_nodes_elementSiteId_fk` FOREIGN KEY (`elementSiteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_navigation_nodes_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_navigation_nodes_navId_fk` FOREIGN KEY (`navId`) REFERENCES `craft_navigation_navs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_navigation_nodes` WRITE;
/*!40000 ALTER TABLE `craft_navigation_nodes` DISABLE KEYS */;

INSERT INTO `craft_navigation_nodes` (`id`, `elementId`, `elementSiteId`, `navId`, `url`, `type`, `classes`, `newWindow`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(30,2,1,1,NULL,'craft\\elements\\Entry','',0,'2019-01-30 21:51:59','2019-11-07 16:37:10','32e46ff4-ab2e-4a90-9e5b-1a32c23ddb4f'),
	(31,26,1,1,NULL,'craft\\elements\\Entry','',0,'2019-01-30 21:52:09','2019-02-08 17:36:55','9dc8b06e-70bb-4ff3-a380-e4404200353d'),
	(32,7,1,1,NULL,'craft\\elements\\Entry',NULL,0,'2019-01-30 21:52:09','2019-02-05 19:05:20','577a86ab-0def-47ea-9c45-370798451ee5'),
	(33,28,1,1,NULL,'craft\\elements\\Entry',NULL,0,'2019-01-30 21:52:10','2019-02-12 20:52:21','adba4404-15f5-4cf0-bbbd-6d71f9f690c0'),
	(34,27,1,1,NULL,'craft\\elements\\Entry','',0,'2019-01-30 21:52:10','2019-02-12 20:50:49','9d42d6fc-c5a4-477f-8259-299c7f63aca3'),
	(35,29,1,1,NULL,'craft\\elements\\Entry','',0,'2019-01-30 21:52:10','2019-02-12 20:53:06','8c547484-bf8a-48a7-b1f4-f840ad968ce9'),
	(36,2,1,2,NULL,'craft\\elements\\Entry','',0,'2019-01-30 21:54:06','2019-11-07 16:37:10','14f9af7e-b8d3-4fbb-9aac-c4725dc08d45'),
	(37,7,1,2,NULL,'craft\\elements\\Entry',NULL,0,'2019-01-30 21:54:11','2019-02-05 19:05:19','82422d43-63c8-48fb-a766-00fc91f69e96'),
	(39,6,1,1,NULL,'craft\\elements\\Entry','',0,'2019-01-30 22:21:05','2019-02-08 16:29:36','110341e2-da80-4b71-b49b-5592fe1c5930'),
	(178,6,1,1,NULL,'craft\\elements\\Entry','',0,'2019-02-14 22:37:25','2019-02-14 22:38:11','97954ef4-de63-4acb-8e37-9126e96173d4');

/*!40000 ALTER TABLE `craft_navigation_nodes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_neoblocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_neoblocks`;

CREATE TABLE `craft_neoblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `ownerSiteId` int(11) DEFAULT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_neoblocks_ownerId_idx` (`ownerId`),
  KEY `craft_neoblocks_ownerSiteId_idx` (`ownerSiteId`),
  KEY `craft_neoblocks_fieldId_idx` (`fieldId`),
  KEY `craft_neoblocks_typeId_idx` (`typeId`),
  CONSTRAINT `craft_neoblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_neoblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_neoblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_neoblocks_ownerSiteId_fk` FOREIGN KEY (`ownerSiteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_neoblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `craft_neoblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_neoblocks` WRITE;
/*!40000 ALTER TABLE `craft_neoblocks` DISABLE KEYS */;

INSERT INTO `craft_neoblocks` (`id`, `ownerId`, `ownerSiteId`, `fieldId`, `typeId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(43,6,NULL,61,2,'2019-01-30 23:11:17','2019-02-27 21:20:40','ddc9f245-a7f8-4886-b836-35d96cb9d59e'),
	(44,6,NULL,61,6,'2019-01-30 23:11:17','2019-02-27 21:20:40','4b818ef9-6f56-4710-b910-2281f6c71c43'),
	(45,6,NULL,61,7,'2019-01-30 23:11:18','2019-02-27 21:20:40','e24206a6-ba2e-43d5-8017-4a0913cde146'),
	(47,6,NULL,61,4,'2019-01-30 23:18:12','2019-02-27 21:20:40','fb5de3da-dd22-4bf0-9118-43608f1389c8'),
	(59,6,NULL,61,8,'2019-01-30 23:50:25','2019-02-27 21:20:40','377aea03-7e27-43e2-923a-50c626ad2ded'),
	(60,6,NULL,61,9,'2019-01-30 23:50:25','2019-02-27 21:20:40','b7f6da46-0d06-43f2-9946-40f8a1ec0631'),
	(61,6,NULL,61,10,'2019-01-30 23:50:25','2019-02-27 21:20:40','903e2dca-c310-4826-a185-90bd07cf3de1'),
	(62,6,NULL,61,11,'2019-01-30 23:50:26','2019-02-27 21:20:40','1c73e3e1-f567-43ca-9ce3-582fcc81497e'),
	(68,6,NULL,61,1,'2019-01-31 16:04:34','2019-02-27 21:20:40','dda4ff19-d6cc-459b-bd9d-0031f7c2a2d0'),
	(71,6,NULL,61,3,'2019-01-31 16:06:10','2019-02-27 21:20:40','59e7e618-ae91-4db9-8645-3b503226bee8'),
	(72,2,NULL,61,2,'2019-01-31 20:57:39','2019-02-08 18:05:31','145e8c9a-66e6-4ee9-8fa7-8ee59041c5a7'),
	(74,6,NULL,61,1,'2019-02-05 17:49:55','2019-02-27 21:20:40','5a611f32-8670-4b57-bf04-6db63b053430'),
	(76,5,NULL,61,1,'2019-02-05 18:52:38','2019-02-05 20:28:28','7075bf62-fe9b-4c67-a7d5-1914122dfb91'),
	(80,79,NULL,61,1,'2019-02-05 20:29:19','2019-02-05 20:29:19','a0285dc9-f4d8-4ba7-bc6e-4fde1fc6a84c'),
	(83,82,NULL,61,1,'2019-02-05 20:30:08','2019-02-05 20:30:08','26becebe-e2c9-4de4-b5a7-aa3b48195c24'),
	(85,2,NULL,61,6,'2019-02-07 20:26:26','2019-02-07 20:27:16','2ea208d1-395b-4ff1-9832-8f0e357a1db3'),
	(86,2,NULL,61,6,'2019-02-07 20:33:23','2019-02-07 20:33:23','9cd9960a-f059-4ad1-a14e-0074cd0b1895'),
	(98,26,NULL,61,1,'2019-02-07 21:02:36','2019-02-08 17:36:56','ba1e9683-4103-49b2-a7c4-7c6c5cbe3dfb'),
	(101,26,NULL,61,8,'2019-02-07 21:02:36','2019-02-08 17:36:56','dda39419-6dc4-490e-9719-1e06c0bf5032'),
	(102,27,NULL,61,1,'2019-02-07 21:16:26','2019-02-08 17:16:37','bf4b2687-ab33-4406-9cf4-fa166c85ba30'),
	(104,27,NULL,61,5,'2019-02-07 21:16:26','2019-02-07 21:16:26','849577aa-0f60-4e5c-8852-ae4c62cb2e22'),
	(105,27,NULL,61,10,'2019-02-07 21:17:08','2019-02-12 20:39:11','7d31c196-89b6-4062-9b8d-62798b531d79'),
	(106,28,NULL,61,1,'2019-02-07 21:18:38','2019-02-08 17:15:21','e65106d7-367c-4585-b7ad-a06dca3fcab0'),
	(108,28,NULL,61,10,'2019-02-07 21:18:38','2019-02-12 20:43:47','0b314f24-e903-4a96-ad44-d045903f7cab'),
	(109,29,NULL,61,1,'2019-02-07 21:20:38','2019-02-08 17:44:24','8eb18072-cbef-47a1-8239-501e2639e889'),
	(112,29,NULL,61,10,'2019-02-07 21:23:41','2019-02-12 20:49:00','04070ca8-6bb4-4915-a502-fbec2d129138'),
	(113,2,NULL,61,1,'2019-02-07 21:30:45','2019-02-08 18:05:31','e738a0fe-b1e1-432b-8e39-3bf55028a505'),
	(115,2,NULL,61,4,'2019-02-07 21:37:48','2019-02-08 18:05:31','d52ddc7d-2802-4576-86d7-7681a225e355'),
	(119,2,NULL,61,8,'2019-02-07 21:44:10','2019-02-08 18:05:31','5e1c1357-1d49-4eab-a7ae-d424f67c38dc'),
	(120,2,NULL,61,1,'2019-02-07 21:44:10','2019-02-08 18:05:31','cef89521-dde8-451d-85a1-f47affa95481'),
	(123,2,NULL,61,1,'2019-02-07 21:44:10','2019-02-08 18:05:32','2971c66d-47bc-46b7-8d82-32a1d6e2f9ab'),
	(126,2,NULL,61,9,'2019-02-07 22:07:10','2019-02-08 18:05:32','99d54234-10ff-409d-8574-09337a80c0d5'),
	(127,2,NULL,61,10,'2019-02-07 22:07:10','2019-02-08 18:05:32','0d1aa7f6-748a-492f-9fe3-83ff387a9605'),
	(128,2,NULL,61,11,'2019-02-07 22:07:10','2019-02-08 18:05:32','a0266b59-f016-4764-aff5-b2dbb60a950b'),
	(140,28,NULL,61,3,'2019-02-07 22:19:35','2019-02-12 20:52:21','7b1b9bff-66c0-4004-b4fb-9de4a93ba888'),
	(142,27,NULL,61,3,'2019-02-07 22:22:58','2019-02-12 20:50:49','5f5d6f76-00d4-49f8-bfbb-47d2a1726b97'),
	(143,29,NULL,61,3,'2019-02-07 22:24:54','2019-02-12 20:53:06','4bed2b7a-7c96-4970-9dfc-4d53a2eed23b'),
	(152,2,NULL,61,1,'2019-02-08 15:47:39','2019-02-08 18:05:31','9adbe885-45f9-44b3-8186-71a85f63be03'),
	(154,2,NULL,61,1,'2019-02-08 15:52:54','2019-02-08 18:05:31','111aab8b-d427-48e7-8bef-45238e47b25b'),
	(156,2,NULL,61,1,'2019-02-08 15:52:54','2019-02-08 18:05:32','1548805c-9d7d-43ad-881d-9392e0bd0cab'),
	(158,2,NULL,61,1,'2019-02-08 15:52:55','2019-02-08 18:05:32','7880d997-93e2-4b56-ab35-0d5125b6e1c9'),
	(160,2,NULL,61,1,'2019-02-08 15:54:32','2019-02-08 18:05:32','3808cc05-3c9e-4dde-8398-bfbb5cfac45b'),
	(163,2,NULL,61,6,'2019-02-08 16:42:15','2019-02-08 16:42:15','67397e53-c7aa-477b-918a-ebb5969ba0ad'),
	(164,2,NULL,61,1,'2019-02-08 16:45:20','2019-02-08 18:05:31','f0994404-4343-4086-825b-579e7a93e92b'),
	(169,26,NULL,61,10,'2019-02-08 17:05:33','2019-02-08 17:36:56','d68e9365-f7e1-4d98-9bf3-067cdc3dd060'),
	(170,27,NULL,61,8,'2019-02-08 17:07:08','2019-02-12 20:50:49','5ce6a496-f772-471a-b632-b49267cf7739'),
	(171,28,NULL,61,8,'2019-02-08 17:11:44','2019-02-12 20:52:21','0c45b2ee-fed5-45aa-9fe2-2789eac820da'),
	(172,29,NULL,61,8,'2019-02-08 17:41:19','2019-02-12 20:53:06','dfdfbf5a-c510-46a4-8e52-80fc5c6fa282'),
	(173,6,NULL,61,12,'2019-02-08 20:04:39','2019-02-27 21:20:40','4288cfd0-d7ea-4498-b6fd-53f87aebec65'),
	(174,27,NULL,61,12,'2019-02-12 20:39:11','2019-02-12 20:50:49','128fab45-c519-43f0-a08b-a4b4422a8962'),
	(175,28,NULL,61,12,'2019-02-12 20:43:47','2019-02-12 20:52:21','78ffcec6-dfa1-4b34-b5cb-d561ea720acf'),
	(176,29,NULL,61,12,'2019-02-12 20:49:00','2019-02-12 20:53:06','3b4ddc8d-ba4f-4bc0-8563-8646f2f894be'),
	(180,6,NULL,61,8,'2019-02-27 21:20:40','2019-02-27 21:20:40','066788ef-4473-4eac-81fb-1c4a8e00c124');

/*!40000 ALTER TABLE `craft_neoblocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_neoblockstructures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_neoblockstructures`;

CREATE TABLE `craft_neoblockstructures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `ownerSiteId` int(11) DEFAULT NULL,
  `fieldId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_neoblockstructures_structureId_idx` (`structureId`),
  KEY `craft_neoblockstructures_ownerId_idx` (`ownerId`),
  KEY `craft_neoblockstructures_ownerSiteId_idx` (`ownerSiteId`),
  KEY `craft_neoblockstructures_fieldId_idx` (`fieldId`),
  CONSTRAINT `craft_neoblockstructures_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_neoblockstructures_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_neoblockstructures_ownerSiteId_fk` FOREIGN KEY (`ownerSiteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_neoblockstructures_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_neoblockstructures` WRITE;
/*!40000 ALTER TABLE `craft_neoblockstructures` DISABLE KEYS */;

INSERT INTO `craft_neoblockstructures` (`id`, `structureId`, `ownerId`, `ownerSiteId`, `fieldId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(17,22,5,NULL,61,'2019-02-05 20:28:29','2019-02-05 20:28:29','a120a7c6-6067-4a10-a2ba-ccef0bc03f4d'),
	(18,23,79,NULL,61,'2019-02-05 20:29:19','2019-02-05 20:29:19','2436cb3b-00d2-4cf2-b038-6d7cb0511c3d'),
	(19,24,82,NULL,61,'2019-02-05 20:30:08','2019-02-05 20:30:08','0fa40c37-e71d-47df-8cc2-da32af36bafe'),
	(68,73,26,NULL,61,'2019-02-08 17:36:56','2019-02-08 17:36:56','c9087142-8dda-4471-b9dc-f64661233c8e'),
	(77,82,27,NULL,61,'2019-02-12 20:50:49','2019-02-12 20:50:49','1627e239-07b7-4b54-9fef-4a6761c44f27'),
	(78,83,28,NULL,61,'2019-02-12 20:52:21','2019-02-12 20:52:21','3ee6d744-063b-425c-b94f-ba33bee9cdf5'),
	(79,84,29,NULL,61,'2019-02-12 20:53:06','2019-02-12 20:53:06','dbbafd5c-259c-45a3-aa70-4ebd00cfc7e3');

/*!40000 ALTER TABLE `craft_neoblockstructures` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_neoblocktypegroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_neoblocktypegroups`;

CREATE TABLE `craft_neoblocktypegroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_neoblocktypegroups_name_fieldId_idx` (`name`,`fieldId`),
  KEY `craft_neoblocktypegroups_fieldId_idx` (`fieldId`),
  CONSTRAINT `craft_neoblocktypegroups_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_neoblocktypegroups` WRITE;
/*!40000 ALTER TABLE `craft_neoblocktypegroups` DISABLE KEYS */;

INSERT INTO `craft_neoblocktypegroups` (`id`, `fieldId`, `name`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(11,61,'Content Blocks',1,'2019-02-08 20:03:35','2019-02-08 20:03:35','55603bfc-b52b-4a33-a4af-abcbe2a2bd24'),
	(12,61,'Special Blocks',6,'2019-02-08 20:03:35','2019-02-08 20:03:35','9ac03550-1733-4dfb-8eb6-cf2063a67401');

/*!40000 ALTER TABLE `craft_neoblocktypegroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_neoblocktypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_neoblocktypes`;

CREATE TABLE `craft_neoblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `maxBlocks` smallint(6) unsigned DEFAULT NULL,
  `maxChildBlocks` smallint(6) unsigned DEFAULT NULL,
  `childBlocks` text,
  `topLevel` tinyint(1) NOT NULL DEFAULT '1',
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_neoblocktypes_handle_fieldId_unq_idx` (`handle`,`fieldId`),
  KEY `craft_neoblocktypes_name_fieldId_idx` (`name`,`fieldId`),
  KEY `craft_neoblocktypes_fieldId_idx` (`fieldId`),
  KEY `craft_neoblocktypes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `craft_neoblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_neoblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_neoblocktypes` WRITE;
/*!40000 ALTER TABLE `craft_neoblocktypes` DISABLE KEYS */;

INSERT INTO `craft_neoblocktypes` (`id`, `fieldId`, `fieldLayoutId`, `name`, `handle`, `maxBlocks`, `maxChildBlocks`, `childBlocks`, `topLevel`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,61,145,'Content Grid','contentGrid',0,0,'',1,3,'2019-01-22 15:42:57','2019-02-08 20:03:35','e348f372-5d6e-4dea-b5d8-1b3b08ded7cf'),
	(2,61,144,'Banner with Content','bannerWithContent',0,0,'',1,2,'2019-01-22 15:42:57','2019-02-08 20:03:35','a0692df3-e543-4bcf-b3eb-2b9f18a1482e'),
	(3,61,148,'Photo Gallery','photoGallery',0,0,'',1,7,'2019-01-22 15:42:57','2019-02-08 20:03:35','20b1f3a8-0a05-4582-84e0-65a586a71c5e'),
	(4,61,149,'Forward Path','forwardPath',0,0,'',1,8,'2019-01-22 15:42:57','2019-02-08 20:03:35','48356792-be84-4415-bad1-56182997acca'),
	(5,61,150,'Call to Action','callToAction',0,0,'',1,9,'2019-01-22 15:42:57','2019-02-08 20:03:35','dfc3c7c4-8472-4cb8-942a-4ba8044ce73e'),
	(6,61,146,'Media Content','mediaContent',0,0,'',1,4,'2019-01-22 15:47:51','2019-02-08 20:03:35','d1cf4c6c-dab3-4120-8b47-e70836a1a202'),
	(7,61,147,'Title','titleBlock',0,0,'',1,5,'2019-01-22 15:47:51','2019-02-08 20:03:35','47a6fe63-16c9-411b-81fe-d0b893ae670e'),
	(8,61,151,'Quotes','quotes',0,0,'',1,10,'2019-01-22 15:56:27','2019-02-08 20:03:35','19821a6f-eea6-4645-b065-8b15350d98c1'),
	(9,61,152,'Blogs','blogs',0,0,'',1,11,'2019-01-22 15:59:52','2019-02-08 20:03:35','ec8131e6-896e-4e8c-8956-1fa674d631a3'),
	(10,61,153,'Call to Action','callToActionblock',0,0,'',1,12,'2019-01-22 16:03:50','2019-02-08 20:03:35','4b3b47be-a056-4e58-82be-0611b97362ce'),
	(11,61,154,'Social Media','socialMedia',0,0,'',1,13,'2019-01-22 16:03:50','2019-02-08 20:03:35','df119dd0-abac-43be-bb4e-d85e2fec2357'),
	(12,61,155,'Form','form',0,0,'',1,14,'2019-02-08 20:03:35','2019-02-08 20:03:35','bb49df09-7fb8-427d-ba2b-fb4f5c20d3b3');

/*!40000 ALTER TABLE `craft_neoblocktypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_plugins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_plugins`;

CREATE TABLE `craft_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `handle` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `schemaVersion` varchar(255) NOT NULL,
  `licenseKeyStatus` enum('valid','invalid','mismatched','astray','unknown') NOT NULL DEFAULT 'unknown',
  `licensedEdition` varchar(255) DEFAULT NULL,
  `installDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_plugins_handle_unq_idx` (`handle`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_plugins` WRITE;
/*!40000 ALTER TABLE `craft_plugins` DISABLE KEYS */;

INSERT INTO `craft_plugins` (`id`, `handle`, `version`, `schemaVersion`, `licenseKeyStatus`, `licensedEdition`, `installDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'redactor','2.2.1','2.2.1','unknown',NULL,'2019-01-21 23:33:32','2019-01-21 23:33:32','2019-11-07 16:44:08','e1910c36-1c43-412f-af7d-997d3672f233'),
	(2,'admin-bar','3.1.8.1','3.1.0','unknown',NULL,'2019-01-21 23:35:21','2019-01-21 23:35:21','2019-11-07 16:44:08','1aafb580-4dba-427b-b392-6aaea2dcf81a'),
	(4,'mix','1.5.2','1.0.0','unknown',NULL,'2019-01-21 23:35:33','2019-01-21 23:35:33','2019-11-07 16:44:08','a79d2eb3-e5e5-4613-a6c4-bace5fdd8afb'),
	(5,'field-manager','2.0.4','1.0.0','unknown',NULL,'2019-01-21 23:35:38','2019-01-21 23:35:38','2019-11-07 16:44:08','591c24dc-79e7-4232-80e6-0ce4244ea66d'),
	(6,'navigation','1.0.18','1.0.5','invalid',NULL,'2019-01-21 23:35:42','2019-01-21 23:35:42','2019-11-07 16:44:08','1a49e92b-4ad7-466e-a649-0621d2ad1bd9'),
	(7,'neo','2.1.5','2.0.0','invalid',NULL,'2019-01-21 23:35:51','2019-01-21 23:35:51','2019-11-07 16:44:08','3ec23577-2606-415f-a172-8f1ed5fdd587'),
	(8,'super-table','2.1.4.3','2.0.7','unknown',NULL,'2019-01-21 23:36:03','2019-01-21 23:36:03','2019-11-07 16:44:08','1d9dc814-56da-43db-a0ea-91434454e86b'),
	(9,'position-fieldtype','1.0.13','1.0.0','unknown',NULL,'2019-01-21 23:36:12','2019-01-21 23:36:12','2019-11-07 16:44:08','a9250698-587a-4c12-80ad-e5de53fae90e'),
	(10,'freeform','2.5.9','2.1.3','invalid',NULL,'2019-01-30 21:13:42','2019-01-30 21:13:42','2019-11-07 16:44:08','43f95acf-b5de-4ff1-9a0d-db93d37ea145'),
	(11,'seomatic','3.2.32','3.0.8','invalid',NULL,'2019-01-30 21:13:52','2019-01-30 21:13:52','2019-11-07 16:44:08','36b0793e-13c9-4cd5-bcbe-6c92be1ab771'),
	(12,'video-embedder','1.1.1','1.0.0','unknown',NULL,'2019-01-31 23:11:08','2019-01-31 23:11:08','2019-11-07 16:44:08','2a8906cd-0787-4823-84e2-f14561f1c331'),
	(13,'typogrify','1.1.16','1.0.0','unknown',NULL,'2019-02-05 19:08:46','2019-02-05 19:08:46','2019-11-07 16:44:08','4d3da0d8-a40b-4c71-beb3-27f3455df334');

/*!40000 ALTER TABLE `craft_plugins` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_queue
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_queue`;

CREATE TABLE `craft_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job` longblob NOT NULL,
  `description` text,
  `timePushed` int(11) NOT NULL,
  `ttr` int(11) NOT NULL,
  `delay` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) unsigned NOT NULL DEFAULT '1024',
  `dateReserved` datetime DEFAULT NULL,
  `timeUpdated` int(11) DEFAULT NULL,
  `progress` smallint(6) NOT NULL DEFAULT '0',
  `progressLabel` varchar(255) DEFAULT NULL,
  `attempt` int(11) DEFAULT NULL,
  `fail` tinyint(1) DEFAULT '0',
  `dateFailed` datetime DEFAULT NULL,
  `error` text,
  PRIMARY KEY (`id`),
  KEY `craft_queue_fail_timeUpdated_timePushed_idx` (`fail`,`timeUpdated`,`timePushed`),
  KEY `craft_queue_fail_timeUpdated_delay_idx` (`fail`,`timeUpdated`,`delay`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_relations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_relations`;

CREATE TABLE `craft_relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `sourceId` int(11) NOT NULL,
  `sourceSiteId` int(11) DEFAULT NULL,
  `targetId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_relations_fieldId_sourceId_sourceSiteId_targetId_unq_idx` (`fieldId`,`sourceId`,`sourceSiteId`,`targetId`),
  KEY `craft_relations_sourceId_idx` (`sourceId`),
  KEY `craft_relations_targetId_idx` (`targetId`),
  KEY `craft_relations_sourceSiteId_idx` (`sourceSiteId`),
  CONSTRAINT `craft_relations_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_relations_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_relations_sourceSiteId_fk` FOREIGN KEY (`sourceSiteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_relations_targetId_fk` FOREIGN KEY (`targetId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_relations` WRITE;
/*!40000 ALTER TABLE `craft_relations` DISABLE KEYS */;

INSERT INTO `craft_relations` (`id`, `fieldId`, `sourceId`, `sourceSiteId`, `targetId`, `sortOrder`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(277,46,5,NULL,21,1,'2019-02-05 20:28:28','2019-02-05 20:28:28','8c22e865-c257-4d7c-b44c-a60882e6c4b2'),
	(278,47,5,NULL,53,1,'2019-02-05 20:28:28','2019-02-05 20:28:28','7aa1b3b9-e0dd-4d25-996d-3d62bb7da7cb'),
	(279,46,79,NULL,41,1,'2019-02-05 20:29:19','2019-02-05 20:29:19','ea2d950d-29d6-4e92-b12d-0b02a37eba48'),
	(280,47,79,NULL,55,1,'2019-02-05 20:29:19','2019-02-05 20:29:19','43cb08bc-1018-436a-a54f-dada734f736d'),
	(281,46,82,NULL,57,1,'2019-02-05 20:30:08','2019-02-05 20:30:08','3ca0b936-cf06-48f0-9d19-6fbdfc2e58ff'),
	(282,47,82,NULL,53,1,'2019-02-05 20:30:08','2019-02-05 20:30:08','74777917-5be3-4e5b-8f95-9afa53b11fec'),
	(288,51,85,NULL,12,1,'2019-02-07 20:27:16','2019-02-07 20:27:16','1f8dc7ae-7686-413c-8dc7-6da5753c94bf'),
	(291,51,86,NULL,12,1,'2019-02-07 20:33:23','2019-02-07 20:33:23','8bbc09d5-ffa9-4e60-b31f-d55f10229bd0'),
	(297,37,104,NULL,67,1,'2019-02-07 21:16:26','2019-02-07 21:16:26','69500f08-8f92-4349-93fa-259c9aba968b'),
	(411,51,73,NULL,41,1,'2019-02-07 22:48:43','2019-02-07 22:48:43','f579192a-4d9f-42b9-b905-b5973d394ece'),
	(412,51,10,NULL,15,1,'2019-02-07 22:48:51','2019-02-07 22:48:51','fd704de3-4b23-4645-8ae2-3f13effac325'),
	(585,51,163,NULL,12,1,'2019-02-08 16:42:15','2019-02-08 16:42:15','b4a9be35-ef02-48f0-a574-3d403fd8849d'),
	(586,2,163,NULL,162,1,'2019-02-08 16:42:15','2019-02-08 16:42:15','41b301f3-84ad-424c-b39e-a52bbb55ea2a'),
	(725,51,144,NULL,136,1,'2019-02-08 17:07:53','2019-02-08 17:07:53','03917525-c078-4898-bb95-3926e6837f69'),
	(726,74,144,NULL,52,1,'2019-02-08 17:07:53','2019-02-08 17:07:53','691388c7-7fba-44a1-b1b3-dc21240836e5'),
	(727,74,144,NULL,148,2,'2019-02-08 17:07:53','2019-02-08 17:07:53','3db385ff-3a8b-4663-b822-baa449f53109'),
	(734,51,146,NULL,137,1,'2019-02-08 17:09:37','2019-02-08 17:09:37','fbd82463-b930-4fbd-8d84-552289fe2e48'),
	(735,74,146,NULL,52,1,'2019-02-08 17:09:37','2019-02-08 17:09:37','630cd90b-88c2-4193-b316-4b99787433fc'),
	(736,74,146,NULL,145,2,'2019-02-08 17:09:37','2019-02-08 17:09:37','1f12d94f-51dc-43ff-9d44-e7519f53f6d4'),
	(737,51,149,NULL,15,1,'2019-02-08 17:10:47','2019-02-08 17:10:47','a882b700-804c-4778-abda-be5cf74517ba'),
	(738,74,149,NULL,52,1,'2019-02-08 17:10:47','2019-02-08 17:10:47','eb525e3f-9494-437c-beef-ab31139215da'),
	(739,74,149,NULL,148,2,'2019-02-08 17:10:47','2019-02-08 17:10:47','0097bd04-122f-4579-8dd8-774a82c6e6dc'),
	(740,51,150,NULL,96,1,'2019-02-08 17:11:02','2019-02-08 17:11:02','36c0a1f3-2622-4c90-a77d-183c953401ab'),
	(741,74,150,NULL,52,1,'2019-02-08 17:11:02','2019-02-08 17:11:02','daf6db47-b0ce-4c77-8788-f7b5576a36b8'),
	(742,74,150,NULL,148,2,'2019-02-08 17:11:02','2019-02-08 17:11:02','c253e73c-3726-4c6b-b39e-4e3f656fd279'),
	(743,51,147,NULL,138,1,'2019-02-08 17:11:34','2019-02-08 17:11:34','15792610-4f90-4cd2-8d0c-5ceca6a7975c'),
	(744,74,147,NULL,51,1,'2019-02-08 17:11:34','2019-02-08 17:11:34','1d172f3e-d369-453c-a581-cdfc6a9549fd'),
	(745,74,147,NULL,52,2,'2019-02-08 17:11:34','2019-02-08 17:11:34','1f491cb0-a6e6-4a21-9a0d-0b92e97bd8af'),
	(771,74,101,NULL,52,1,'2019-02-08 17:36:56','2019-02-08 17:36:56','ad6693c7-4047-41d8-86fa-0895de768692'),
	(772,37,169,NULL,67,1,'2019-02-08 17:36:56','2019-02-08 17:36:56','c96fadbd-cfd1-4c53-a4d6-549a97dba4f6'),
	(779,2,109,NULL,167,1,'2019-02-08 17:44:24','2019-02-08 17:44:24','80e6993e-1183-4b3d-8f25-5548bd4e9b83'),
	(786,51,72,NULL,40,1,'2019-02-08 18:05:31','2019-02-08 18:05:31','79fd32fa-22fd-4ae8-b8eb-01c6bedaa129'),
	(787,2,72,NULL,41,1,'2019-02-08 18:05:31','2019-02-08 18:05:31','5e9351ae-f5ec-480b-a77d-163e0ad5bdf0'),
	(788,2,164,NULL,167,1,'2019-02-08 18:05:31','2019-02-08 18:05:31','800c7de9-82c6-4f65-9e19-111cdb039d17'),
	(789,2,113,NULL,11,1,'2019-02-08 18:05:31','2019-02-08 18:05:31','34c8b0fa-a344-48ac-9f37-87f7f48ee86f'),
	(790,42,116,NULL,46,1,'2019-02-08 18:05:31','2019-02-08 18:05:31','928547a7-e497-4dac-846b-9f2f963d2790'),
	(791,45,116,NULL,27,1,'2019-02-08 18:05:31','2019-02-08 18:05:31','31322cf3-e2d9-4bab-8f62-7e9bf351af05'),
	(792,42,117,NULL,46,1,'2019-02-08 18:05:31','2019-02-08 18:05:31','a4f425bc-4ae7-4ebf-88a2-82c052f36c1d'),
	(793,45,117,NULL,28,1,'2019-02-08 18:05:31','2019-02-08 18:05:31','1f3cf899-47a0-4b45-8f9a-40f83abc721e'),
	(794,42,118,NULL,46,1,'2019-02-08 18:05:31','2019-02-08 18:05:31','c329581d-35dc-4308-bbf1-93ddf335868d'),
	(795,45,118,NULL,29,1,'2019-02-08 18:05:31','2019-02-08 18:05:31','ef017ea8-7f7d-4f2e-8abd-b146985fd456'),
	(796,2,115,NULL,168,1,'2019-02-08 18:05:31','2019-02-08 18:05:31','c1044ff7-08ff-4ada-a6f2-ae5137d50d91'),
	(797,2,152,NULL,11,1,'2019-02-08 18:05:31','2019-02-08 18:05:31','fdf2fed9-a6e7-4d18-8eb5-fe30dee58ab7'),
	(798,74,119,NULL,52,1,'2019-02-08 18:05:31','2019-02-08 18:05:31','1e18fb98-c825-4303-9116-205874a4a0cc'),
	(799,2,154,NULL,11,1,'2019-02-08 18:05:31','2019-02-08 18:05:31','3011a099-291c-445b-9b30-b99e911bb42f'),
	(800,2,120,NULL,13,1,'2019-02-08 18:05:32','2019-02-08 18:05:32','be786f2b-b3b5-4a64-8e43-d2ea9c90dc08'),
	(801,2,156,NULL,11,1,'2019-02-08 18:05:32','2019-02-08 18:05:32','183978fb-3667-4f2f-b351-5693dc1b312d'),
	(802,2,158,NULL,11,1,'2019-02-08 18:05:32','2019-02-08 18:05:32','7e8154f9-a09a-4d41-bf10-7b5783c2b2d6'),
	(803,37,127,NULL,67,1,'2019-02-08 18:05:32','2019-02-08 18:05:32','e4fb485f-3684-4ab2-b9f9-46636d39126f'),
	(804,2,160,NULL,11,1,'2019-02-08 18:05:32','2019-02-08 18:05:32','27deeb55-277b-47cd-8989-276a8db34b6a'),
	(805,67,129,NULL,151,1,'2019-02-08 18:05:32','2019-02-08 18:05:32','d6ab077f-5f91-4ced-a4bf-501e80acb7af'),
	(806,67,130,NULL,89,1,'2019-02-08 18:05:32','2019-02-08 18:05:32','a5993301-4814-435d-8047-554f6f2d6f98'),
	(807,67,131,NULL,56,1,'2019-02-08 18:05:32','2019-02-08 18:05:32','846388ca-bd7f-48e0-8a8e-004c0cd4f5ff'),
	(808,67,132,NULL,58,1,'2019-02-08 18:05:32','2019-02-08 18:05:32','c3737eaa-7b7d-41de-9169-b84769c3bba0'),
	(870,37,105,NULL,67,1,'2019-02-12 20:39:11','2019-02-12 20:39:11','b50806e8-09bc-4e01-9d58-b3aa04d05103'),
	(876,37,108,NULL,67,1,'2019-02-12 20:43:47','2019-02-12 20:43:47','87918cc7-f09f-4e33-8477-47e3de51a6e8'),
	(882,37,112,NULL,67,1,'2019-02-12 20:49:00','2019-02-12 20:49:00','2fbd39f5-21f5-4752-aa01-89651f07c480'),
	(883,74,170,NULL,148,1,'2019-02-12 20:50:49','2019-02-12 20:50:49','11936920-1b64-4ece-9b4d-8566995b9677'),
	(884,52,142,NULL,12,1,'2019-02-12 20:50:49','2019-02-12 20:50:49','5a8f9bab-610f-463c-9d1d-bf9935a12f46'),
	(885,52,142,NULL,89,2,'2019-02-12 20:50:49','2019-02-12 20:50:49','9f7b7cfe-2768-497f-9a56-f3806266052c'),
	(886,52,142,NULL,133,3,'2019-02-12 20:50:49','2019-02-12 20:50:49','c505cc2d-dafc-4098-8247-d56bcbe8170a'),
	(887,52,142,NULL,93,4,'2019-02-12 20:50:49','2019-02-12 20:50:49','e38a6e0b-aceb-4e60-9ab0-80c0eef5a2d8'),
	(888,74,171,NULL,145,1,'2019-02-12 20:52:21','2019-02-12 20:52:21','9dd5dea4-ccd7-4822-a276-669186073338'),
	(889,52,140,NULL,95,1,'2019-02-12 20:52:21','2019-02-12 20:52:21','500677f6-b483-4ccc-a0d2-cef9050ab83f'),
	(890,52,140,NULL,56,2,'2019-02-12 20:52:21','2019-02-12 20:52:21','0e071714-b9f0-4a64-adc0-fd5ad8fc141f'),
	(891,52,140,NULL,136,3,'2019-02-12 20:52:21','2019-02-12 20:52:21','1af011de-517d-455a-88a5-d8d24e052d67'),
	(892,52,140,NULL,134,4,'2019-02-12 20:52:21','2019-02-12 20:52:21','bd61aba2-c203-4902-be54-1836a42fbe4a'),
	(893,74,172,NULL,51,1,'2019-02-12 20:53:06','2019-02-12 20:53:06','94c56082-2a2f-4dbe-b8df-a5f3f33a62a5'),
	(894,52,143,NULL,21,1,'2019-02-12 20:53:06','2019-02-12 20:53:06','84162fd9-9a9e-483b-b8d1-9267baf89497'),
	(895,52,143,NULL,95,2,'2019-02-12 20:53:06','2019-02-12 20:53:06','10b046af-a166-417c-9efc-f8a2ef0c895b'),
	(896,52,143,NULL,97,3,'2019-02-12 20:53:06','2019-02-12 20:53:06','353d1389-b9f0-4d93-95b6-609faed5dcb7'),
	(897,52,143,NULL,20,4,'2019-02-12 20:53:06','2019-02-12 20:53:06','6122836b-0525-4182-be8a-29243ba206cb'),
	(926,51,43,NULL,40,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','615be74c-16bf-41b7-829a-9e5dfee73b96'),
	(927,2,43,NULL,41,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','f64aee18-f5df-4a43-a518-ba25c3682b78'),
	(928,51,44,NULL,12,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','4b93e4a4-0b94-40ce-87a4-304481af1b4a'),
	(929,2,44,NULL,42,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','49faefaf-80c8-4005-a9ad-af98c61de132'),
	(930,2,68,NULL,11,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','d75fae55-865e-494d-a3fc-5eec1b96cf0d'),
	(931,42,48,NULL,46,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','2009a704-60ea-4892-812a-1374c2738c31'),
	(932,45,48,NULL,2,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','84cc289f-ce28-4af1-945b-1f7c0dabd9a4'),
	(933,42,49,NULL,42,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','2b608a6b-1f19-42a5-9c82-482cca1a83c0'),
	(934,45,49,NULL,2,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','eca1368e-28e5-489f-ba35-436ff86d0513'),
	(935,42,50,NULL,46,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','8522a892-1243-44ff-b402-5378d6db7128'),
	(936,45,50,NULL,2,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','f1f3eef1-752e-4197-800b-1884b62d4400'),
	(937,74,59,NULL,52,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','67e07c0b-22c5-4ef9-90b7-caaf0065af11'),
	(938,37,61,NULL,67,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','7947e115-ef1b-49c0-968b-d8e5742093a6'),
	(939,67,63,NULL,57,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','818ff4e4-f93d-4af5-af34-61ef884b4db6'),
	(940,67,64,NULL,58,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','8b909573-d4ca-4280-937f-1d32ba6cb485'),
	(941,67,65,NULL,56,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','a6a9417e-c55e-492a-9ea4-83ee2567c712'),
	(942,67,66,NULL,57,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','6fce47b4-f4fc-48aa-ba4d-e9c83f2006e5'),
	(943,52,71,NULL,41,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','807594d5-6214-4f22-a67e-3c41d10a61a7'),
	(944,52,71,NULL,57,2,'2019-02-27 21:20:40','2019-02-27 21:20:40','a6379afc-669e-4502-9676-60058d8c0a61'),
	(945,52,71,NULL,20,3,'2019-02-27 21:20:40','2019-02-27 21:20:40','cd62da02-03f6-49f0-8d16-a54e95d036f7'),
	(946,52,71,NULL,14,4,'2019-02-27 21:20:40','2019-02-27 21:20:40','b3631b34-d512-4902-b645-d7dd15d2c5a6'),
	(947,52,71,NULL,58,5,'2019-02-27 21:20:40','2019-02-27 21:20:40','ee613cc8-6824-4f6a-bcc8-b616cecbacd3'),
	(948,52,71,NULL,18,6,'2019-02-27 21:20:40','2019-02-27 21:20:40','72fa9693-b4df-41c2-a71b-d4925e24cbda'),
	(949,52,71,NULL,15,7,'2019-02-27 21:20:40','2019-02-27 21:20:40','c1116da1-1d0e-4f9d-b480-c3e4ee101f38'),
	(950,52,71,NULL,16,8,'2019-02-27 21:20:40','2019-02-27 21:20:40','a3152dd3-d9f5-4c96-985f-568b5dec6479'),
	(951,52,71,NULL,12,9,'2019-02-27 21:20:40','2019-02-27 21:20:40','21a4da70-df26-41fe-aee2-c84ec9e00850'),
	(952,52,71,NULL,56,10,'2019-02-27 21:20:40','2019-02-27 21:20:40','7ad70531-5275-465a-a546-4fff9ab3c58d'),
	(953,52,71,NULL,21,11,'2019-02-27 21:20:40','2019-02-27 21:20:40','e42df2b8-3aad-4c72-a8b7-2218467d27e2'),
	(954,74,180,NULL,52,1,'2019-02-27 21:20:40','2019-02-27 21:20:40','3994504f-59ba-4896-9396-bd501a0f6919'),
	(958,51,200,NULL,138,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','55b427e7-2635-48f9-9c69-34e715583398'),
	(959,74,200,NULL,51,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','5355b112-1e39-496c-8a39-3ec7d52edb75'),
	(960,74,200,NULL,52,2,'2019-11-07 15:48:54','2019-11-07 15:48:54','7d3a3348-91d8-4305-b022-da76ef25c6e9'),
	(964,51,201,NULL,96,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','a6795571-8477-41c8-a569-bc2265ab46ea'),
	(965,74,201,NULL,52,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','3a456219-7b2f-43cb-af8c-72ec76b91a35'),
	(966,74,201,NULL,148,2,'2019-11-07 15:48:54','2019-11-07 15:48:54','7ba8e587-e9f2-43c9-a1e7-9ef645450439'),
	(970,51,202,NULL,15,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','dfc4d9cf-cd2d-479c-b763-c68bd3958376'),
	(971,74,202,NULL,52,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','950039c0-b7d4-42b7-9e5e-677dc5d98fa7'),
	(972,74,202,NULL,148,2,'2019-11-07 15:48:54','2019-11-07 15:48:54','a3c3d79f-b4d2-4cc3-ae37-1bd51d464539'),
	(976,51,203,NULL,137,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','83941270-1084-4360-9c88-1a765f595850'),
	(977,74,203,NULL,52,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','c3536c05-fafb-4ba7-874f-20e77edf84f3'),
	(978,74,203,NULL,145,2,'2019-11-07 15:48:54','2019-11-07 15:48:54','2fde16e0-ea63-4207-9926-1b17e7da2c85'),
	(982,51,204,NULL,136,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','98fd5fd8-ec55-407d-8031-5f17dcf2a832'),
	(983,74,204,NULL,52,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','66391afd-3087-479c-9269-ea839403a1fb'),
	(984,74,204,NULL,148,2,'2019-11-07 15:48:54','2019-11-07 15:48:54','b5d2a5b5-9b6c-4fcd-9a53-9fde40669e87'),
	(988,51,222,NULL,137,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','2a502235-fc97-450d-99b6-2db9cb739bf4'),
	(989,74,222,NULL,52,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','654533eb-b484-4509-9d14-2d744b60abbc'),
	(993,51,223,NULL,136,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','2a6def85-cf98-4d31-b694-6b52b1524cb7'),
	(994,74,223,NULL,52,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','d59ce795-c5fd-407c-97f4-a95c7903203e'),
	(996,51,224,NULL,15,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','4fc717a0-1430-494e-b2c2-6ddb56abfaa3'),
	(998,51,225,NULL,41,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','d78e3463-98a1-4477-8760-c150f09f9cb4'),
	(1002,51,226,NULL,137,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','69e684b8-11b2-4dff-8a52-f71057d67d4b'),
	(1003,74,226,NULL,52,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','3e4d63b3-64bc-4d3b-b2df-fd244101cf0f'),
	(1007,51,227,NULL,41,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','966789d6-af7d-4473-a587-a984f75d21f6'),
	(1008,74,227,NULL,52,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','47467793-8b21-4457-8cb9-5a3864a01091'),
	(1012,51,228,NULL,138,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','288052ef-ea5f-4b03-b523-941874767955'),
	(1013,74,228,NULL,52,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','43acb320-76ee-4aa0-adac-78978c6e1a8e'),
	(1017,51,229,NULL,15,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','029befdf-4085-4424-84d1-6699418f2caa'),
	(1018,74,229,NULL,52,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','024261f6-b626-4d6d-9f40-9af68ea629dc'),
	(1022,51,230,NULL,96,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','f79f68c3-e1fe-43b4-a3ff-921815901adf'),
	(1023,74,230,NULL,52,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','53ea727a-48cc-45d0-9a5b-14d4fcc18640'),
	(1027,51,231,NULL,96,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','d91b3c7b-6cfd-4b93-a1d0-c4859526b596'),
	(1028,74,231,NULL,148,1,'2019-11-07 15:48:54','2019-11-07 15:48:54','6a2ce7ce-0f2b-45b0-89f7-f0ec36c1f52b'),
	(1032,51,232,NULL,15,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','66947e3a-dc77-44ff-8f45-2e1332436cfd'),
	(1033,74,232,NULL,148,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','e5398beb-29ca-4568-a262-eb67e1e90333'),
	(1037,51,233,NULL,138,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','5bc1026e-4f64-47bc-b662-1278c7b6b22d'),
	(1038,74,233,NULL,51,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','a503dced-95de-4e1d-9979-d7a439ed5917'),
	(1042,51,234,NULL,41,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','21a41bb2-d0d1-4678-beb3-4e92ca581a2f'),
	(1043,74,234,NULL,145,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','ab94a504-41bd-4e22-97b4-704d108c751c'),
	(1047,74,235,NULL,145,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','9a344cc6-e8ba-4128-b130-efa61f44999b'),
	(1051,51,236,NULL,137,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','5634cbb0-d1e4-43db-9f50-55a53fa33890'),
	(1054,46,257,NULL,57,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','17d5fb00-8174-44f1-811b-22dca1d46e53'),
	(1055,47,257,NULL,53,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','e8be4e2f-7f3b-470f-ba7d-d1eead2a665d'),
	(1058,46,258,NULL,41,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','c810a139-a320-4755-9da1-e89e336ae004'),
	(1059,47,258,NULL,55,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','461f3543-464c-4507-9fd7-c5416c0d9a7f'),
	(1062,46,259,NULL,21,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','06280005-d1b3-45f8-9cbc-0e7918f5c89c'),
	(1063,47,259,NULL,53,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','a3167f91-05bd-4402-a58d-f3e9208644d5'),
	(1065,51,262,NULL,41,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','6c95cc6a-b705-44a6-b491-052a850d83e6'),
	(1066,74,262,NULL,52,1,'2019-11-07 15:48:55','2019-11-07 15:48:55','764a4e56-12d6-4937-a800-d4c456fb6148'),
	(1069,46,269,NULL,21,1,'2019-11-07 15:48:56','2019-11-07 15:48:56','d3945ad1-1be0-4f6f-8759-55afe6c4b6cf'),
	(1070,47,269,NULL,53,1,'2019-11-07 15:48:56','2019-11-07 15:48:56','e107769a-5ba3-42f4-8f3f-fadf0d6d53f4'),
	(1073,46,270,NULL,21,1,'2019-11-07 15:48:56','2019-11-07 15:48:56','56af46bb-e807-4f0b-8ff3-6e34530d5b16'),
	(1074,47,270,NULL,53,1,'2019-11-07 15:48:56','2019-11-07 15:48:56','26a0c7a4-1ee3-4c04-949b-4f88b53cfbe6'),
	(1076,51,271,NULL,15,1,'2019-11-07 15:48:56','2019-11-07 15:48:56','884a5c49-3db0-4d22-888a-0cd96a79e37a'),
	(1077,74,271,NULL,52,1,'2019-11-07 15:48:56','2019-11-07 15:48:56','a3b5c83d-c79c-4e83-aa59-4d16a8516c49'),
	(1079,74,272,NULL,52,1,'2019-11-07 15:48:56','2019-11-07 15:48:56','f76ef261-5c53-424a-83e6-d805adc04279'),
	(1083,46,290,NULL,21,1,'2019-11-07 15:48:57','2019-11-07 15:48:57','045d74f1-89a5-400d-a695-d3d8d8b36cef'),
	(1084,47,290,NULL,53,1,'2019-11-07 15:48:57','2019-11-07 15:48:57','1fa03ea8-a647-44d7-931b-573746bfd2c8');

/*!40000 ALTER TABLE `craft_relations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_resourcepaths
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_resourcepaths`;

CREATE TABLE `craft_resourcepaths` (
  `hash` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_resourcepaths` WRITE;
/*!40000 ALTER TABLE `craft_resourcepaths` DISABLE KEYS */;

INSERT INTO `craft_resourcepaths` (`hash`, `path`)
VALUES
	('13a3549c','@app/web/assets/plugins/dist'),
	('145aa0a8','@lib'),
	('151dcc7d','@lib/selectize'),
	('15ca03d0','@craft/web/assets/recententries/dist'),
	('1e037718','@craft/web/assets/plugins/dist'),
	('1e2ed243','@lib'),
	('1e3c65b9','@vendor/craftcms/redactor/lib/redactor-plugins/fullscreen'),
	('1f7188f7','@lib/xregexp'),
	('20282dbd','@app/web/assets/pluginstore/dist'),
	('208ed082','@lib/timepicker'),
	('20f49bcd','@app/web/assets/utilities/dist'),
	('21d839c5','@lib/jquery.payment'),
	('23ddd1e6','@craft/web/assets/editentry/dist'),
	('260413bb','@bower/jquery/dist'),
	('2815a96e','@craft/web/assets/cp/dist'),
	('288414c6','@app/web/assets/plugins/dist'),
	('29699f5f','@vendor/craftcms/redactor/lib/redactor-plugins/video'),
	('29def592','@app/web/assets/tablesettings/dist'),
	('2b7041be','@craft/web/assets/updateswidget/dist'),
	('2bac4b2e','@lib/jquery.payment'),
	('2c706150','@bower/jquery/dist'),
	('2ea08b0c','@storage/rebrand/logo'),
	('319a0cbe','@config/redactor/plugins'),
	('378be4c6','@lib/velocity'),
	('37b13e6','@craft/web/assets/dashboard/dist'),
	('3875a852','@craft/web/assets/pluginstore/dist'),
	('39e6a95c','@app/web/assets/edituser/dist'),
	('3a78ce7','@craft/web/assets/feed/dist'),
	('3b106096','@app/web/assets/cp/dist'),
	('3b9492e2','@app/web/assets/feed/dist'),
	('3cb2b1a3','@craft/web/assets/craftsupport/dist'),
	('3dff962d','@lib/velocity'),
	('3e0caa5','@lib/element-resize-detector'),
	('3eee97c7','@wbrowar/adminbar/assetbundles/adminbar/dist'),
	('4163e1f9','@lib/picturefill'),
	('41b1a56a','@app/icons'),
	('428fd95c','@app/web/assets/cp/dist'),
	('431af5f9','@freeform/Resources'),
	('4441d7b2','@lib/prismjs'),
	('452d0869','@craft/web/assets/craftsupport/dist'),
	('45cbb9e1','@app/web/assets/matrix/dist'),
	('46b1cc22','@app/web/assets/login/dist'),
	('46c7dd3b','@lib/jquery-touch-events'),
	('4ae99bd5','@app/web/assets/editentry/dist'),
	('4d2bbfa4','@craft/web/assets/login/dist'),
	('4e145d0c','@lib/velocity'),
	('4fc6c1a8','@lib/d3'),
	('50414c58','@app/web/assets/tablesettings/dist'),
	('51193149','@vendor/craftcms/redactor/lib/redactor-plugins/video'),
	('5168198f','@app/web/assets/dashboard/dist'),
	('5233f2e4','@lib/jquery.payment'),
	('52df8ba7','@craft/web/assets/fields/dist'),
	('540bd1a','@app/web/assets/editcategory/dist'),
	('549aff94','@nystudio107/seomatic/assetbundles/seomatic/dist'),
	('560f37c1','@app/web/assets/fields/dist'),
	('578ee7bf','@app/web/assets/updates/dist'),
	('598be007','@lib/fileupload'),
	('59b79477','@app/web/assets/pluginstore/dist'),
	('5c82305b','@app/web/assets/sites/dist'),
	('5e96b55d','@wbrowar/adminbar/assetbundles/adminbar/dist'),
	('5f9baa71','@bower/jquery/dist'),
	('5fc2fed1','@app/web/assets/recententries/dist'),
	('6178bcbf','@app/web/assets/updateswidget/dist'),
	('63d60893','@craft/web/assets/tablesettings/dist'),
	('679cced2','@craft/web/assets/plugins/dist'),
	('67b16b89','@lib'),
	('68c4c79b','@nystudio107/seomatic/assetbundles/seomatic/dist'),
	('69014080','@config/redactor/plugins'),
	('693d0210','@lib/fabric'),
	('6a4f59d5','@app/web/assets/dashboard/dist'),
	('6aee3829','@benf/neo/resources'),
	('6bfca0e5','@craft/web/assets/utilities/dist'),
	('6fea107e','@app/web/assets/craftsupport/dist'),
	('700b0184','@lib/element-resize-detector'),
	('712dee80','@app/web/assets/updater/dist'),
	('71cedb8f','@app/web/assets/editentry/dist'),
	('73560d88','@lib/jquery-ui'),
	('73c17f47','@app/web/assets/matrixsettings/dist'),
	('749224ad','@verbb/supertable/resources/dist'),
	('7599d535','@freeform/Resources'),
	('75dcd0ae','@app/web/assets/generalsettings/dist'),
	('76fe64c3','@vendor/craftcms/redactor/lib/redactor-plugins/fontcolor'),
	('79227f63','@lib/jquery-ui'),
	('7a37d696','@craft/web/assets/edittransform/dist'),
	('7a38352d','@craft/web/assets/feed/dist'),
	('7a3c45dd','@craft/web/assets/matrix/dist'),
	('7ae4aa2c','@craft/web/assets/dashboard/dist'),
	('7c7bd62e','@lib/garnishjs'),
	('7ebe4140','@app/web/assets/sites/dist'),
	('7eecf9bb','@app/web/assets/matrix/dist'),
	('81609e3b','@craft/web/assets/cp/dist'),
	('81829710','@app/web/assets/dashboard/dist'),
	('81a724ca','@rias/positionfieldtype/assetbundles/positionfieldtype/dist'),
	('82f0ccd5','@lib/fabric'),
	('8376a63f','@storage/rebrand/icon'),
	('8427debb','@app/web/assets/craftsupport/dist'),
	('84bafc52','@rias/positionfieldtype/assetbundles/positionfieldtype/dist'),
	('861d244c','@storage/rebrand/logo'),
	('86e5b95e','@app/web/assets/fields/dist'),
	('874fbb72','@lib/selectize'),
	('881bae46','@craft/web/assets/pluginstore/dist'),
	('8884be3e','@lib/fabric'),
	('8ab5727a','@app/web/assets/updateswidget/dist'),
	('8cdc60f9','@verbb/fieldmanager/resources/dist'),
	('8d23fff8','@lib/xregexp'),
	('90462ba9','@app/web/assets/pluginstore/dist'),
	('926557c3','@app/web/assets/cp/dist'),
	('92efb1a6','@lib/jquery-ui'),
	('95c786f6','@craft/web/assets/craftsupport/dist'),
	('9633410f','@app/web/assets/recententries/dist'),
	('965b42bd','@app/web/assets/login/dist'),
	('97b618eb','@lib/garnishjs'),
	('985da4c3','@vendor/craftcms/redactor/lib/redactor-plugins/alignment'),
	('98abddcd','@app/web/assets/graphiql/dist'),
	('994b84e','@lib/element-resize-detector'),
	('9ae02045','@app/web/assets/updater/dist'),
	('9b1e47aa','@craft/web/assets/updateswidget/dist'),
	('9cb73614','@app/web/assets/installer/dist'),
	('9dc1313b','@craft/web/assets/login/dist'),
	('9dc26a00','@lib/garnishjs'),
	('9e111e6b','@app/web/assets/generalsettings/dist'),
	('9ffa229a','@craft/icons'),
	('a0da5dd7','@lib/picturefill'),
	('a1245510','@app/web/assets/editentry/dist'),
	('a3ecd8c9','@vendor/craftcms/redactor/lib/redactor'),
	('a40b0f6d','@lib/d3'),
	('a5365e31','@app/web/assets/generalsettings/dist'),
	('a5a405c4','@craft/web/assets/recententries/dist'),
	('a5ec68d','@app/web/assets/matrixsettings/dist'),
	('a5f86b9c','@lib/prismjs'),
	('a6c6eb5f','@craft/redactor/assets/field/dist'),
	('a77e6115','@lib/jquery-touch-events'),
	('a8890361','@app/web/assets/updateswidget/dist'),
	('a9421799','@app/web/assets/cp/dist'),
	('aaae2f3c','@lib/picturefill'),
	('aacb7bca','@craft/web/assets/clearcaches/dist'),
	('aad2bbb2','@craft/web/assets/feed/dist'),
	('aad6cb42','@craft/web/assets/matrix/dist'),
	('ac2f390d','@verbb/fieldmanager/resources/dist'),
	('ac67439b','@craft/web/assets/updater/dist'),
	('ad0a13fe','@lib/jquery-touch-events'),
	('ad7c02e7','@app/web/assets/login/dist'),
	('ae067724','@app/web/assets/matrix/dist'),
	('ae7f7d86','@lib/d3'),
	('af8c1977','@lib/prismjs'),
	('b202ec9d','@benf/neo/resources'),
	('b2462ec2','@lib/fileupload'),
	('b27a5ab2','@app/web/assets/pluginstore/dist'),
	('b2dca78d','@lib/timepicker'),
	('b40f3014','@app/web/assets/recententries/dist'),
	('b68a1a06','@craft/web/assets/pluginstoreoauth/dist'),
	('b74ffe9e','@app/web/assets/sites/dist'),
	('b776404d','@craft/web/assets/plugins/dist'),
	('b8325c29','@lib/fileupload'),
	('bad663c9','@app/web/assets/plugins/dist'),
	('bdc6a9','@lib/jquery-ui'),
	('bf009ee1','@app/web/assets/craftsupport/dist'),
	('c06185eb','@lib/jquery.payment'),
	('c11891ab','@verbb/navigation/resources/dist'),
	('c1376cac','@lib/timepicker'),
	('c349da03','@app/web/assets/plugins/dist'),
	('c8e594d2','@Solspace/Freeform/Resources'),
	('c93e43be','@verbb/supertable/resources/dist'),
	('cb395508','@app/web/assets/utilities/dist'),
	('cb431e47','@lib/timepicker'),
	('cbd99708','@lib/fileupload'),
	('cc5b7e15','@craft/web/assets/matrixsettings/dist'),
	('cdc9dd7e','@bower/jquery/dist'),
	('d0595c27','@app/web/assets/feed/dist'),
	('d21276ae','@app/web/assets/clearcaches/dist'),
	('d33196f6','@lib/picturefill'),
	('d3919d79','@craft/web/assets/dashboard/dist'),
	('d495aa34','@lib/jquery-touch-events'),
	('d5aa3385','@app/web/assets/editcategory/dist'),
	('d5f8fa51','@craft/web/assets/updater/dist'),
	('dab44812','@app/web/assets/matrixsettings/dist'),
	('db9c76df','@vendor/craftcms/redactor/lib/redactor'),
	('dc3bbc0e','@craft/web/assets/recententries/dist'),
	('dc462a03','@lib/velocity'),
	('dd94b6a7','@lib/d3'),
	('deb64549','@craft/redactor/assets/field/dist'),
	('dfaa8f63','@wbrowar/adminbar/assetbundles/adminbar/dist'),
	('e02d0ad5','@vendor/craftcms/redactor/lib/redactor-plugins/alignment'),
	('e02f4a4c','@app/web/assets/tablesettings/dist'),
	('e14ef6c5','@verbb/navigation/resources/dist'),
	('e259768b','@lib/element-resize-detector'),
	('e281fe60','@craft/web/assets/updateswidget/dist'),
	('e559ec9','@nystudio107/seomatic/assetbundles/seomatic/dist'),
	('e8ecad5','@vendor/craftcms/redactor/lib/redactor-plugins/fontcolor'),
	('e9052413','@app/web/assets/utilities/dist'),
	('eb7e1c7d','@app/web/assets/feed/dist'),
	('ec127385','@craft/web/assets/deprecationerrors/dist'),
	('ee29a121','@lib/garnishjs'),
	('f184178c','@craft/web/assets/pluginstore/dist'),
	('f3375f79','@craft/web/assets/editentry/dist'),
	('f4a47053','@lib/selectize'),
	('f4bc4632','@lib/xregexp'),
	('f5e31c86','@lib'),
	('f7e10c3a','@app/web/assets/userpermissions/dist'),
	('f8ff27f1','@craft/web/assets/cp/dist'),
	('f9ce9449','@app/web/assets/edittransform/dist'),
	('fb6f751f','@lib/fabric'),
	('fe66b41e','@Solspace/Freeform/Resources'),
	('fec834d9','@lib/xregexp'),
	('fed002b8','@lib/selectize'),
	('ff7a0094','@app/web/assets/fields/dist'),
	('ffece5e7','@app/web/assets/routes/dist');

/*!40000 ALTER TABLE `craft_resourcepaths` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_revisions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_revisions`;

CREATE TABLE `craft_revisions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sourceId` int(11) NOT NULL,
  `creatorId` int(11) DEFAULT NULL,
  `num` int(11) NOT NULL,
  `notes` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_revisions_sourceId_num_unq_idx` (`sourceId`,`num`),
  KEY `craft_revisions_creatorId_fk` (`creatorId`),
  CONSTRAINT `craft_revisions_creatorId_fk` FOREIGN KEY (`creatorId`) REFERENCES `craft_users` (`id`) ON DELETE SET NULL,
  CONSTRAINT `craft_revisions_sourceId_fk` FOREIGN KEY (`sourceId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_revisions` WRITE;
/*!40000 ALTER TABLE `craft_revisions` DISABLE KEYS */;

INSERT INTO `craft_revisions` (`id`, `sourceId`, `creatorId`, `num`, `notes`)
VALUES
	(1,6,1,18,NULL),
	(2,6,1,17,NULL),
	(3,29,1,9,NULL),
	(4,28,1,8,NULL),
	(5,27,1,8,NULL),
	(6,29,1,8,NULL),
	(7,28,1,7,NULL),
	(8,27,1,7,NULL),
	(9,6,1,16,NULL),
	(10,6,1,15,NULL),
	(11,2,1,25,NULL),
	(12,29,1,7,NULL),
	(13,29,1,6,NULL),
	(14,26,1,7,NULL),
	(15,26,1,6,NULL),
	(16,29,1,5,NULL),
	(17,27,1,6,NULL),
	(18,28,1,6,NULL),
	(19,28,1,5,NULL),
	(20,147,1,3,NULL),
	(21,150,1,3,NULL),
	(22,149,1,3,NULL),
	(23,146,1,5,NULL),
	(24,144,1,4,NULL),
	(25,27,1,5,NULL),
	(26,26,1,5,NULL),
	(27,2,1,24,NULL),
	(28,2,1,23,NULL),
	(29,2,1,22,NULL),
	(30,2,1,21,NULL),
	(31,2,1,20,NULL),
	(32,2,1,19,NULL),
	(33,2,1,18,NULL),
	(34,2,1,17,NULL),
	(35,2,1,16,NULL),
	(36,2,1,15,NULL),
	(37,2,1,14,NULL),
	(38,2,1,13,NULL),
	(39,6,1,14,NULL),
	(40,2,1,12,NULL),
	(41,28,1,4,NULL),
	(42,146,1,4,NULL),
	(43,144,1,3,NULL),
	(44,10,1,4,NULL),
	(45,73,1,2,NULL),
	(46,144,1,2,NULL),
	(47,146,1,3,NULL),
	(48,147,1,2,NULL),
	(49,149,1,2,NULL),
	(50,150,1,2,NULL),
	(51,150,1,1,NULL),
	(52,149,1,1,NULL),
	(53,147,1,1,NULL),
	(54,146,1,2,NULL),
	(55,146,1,1,NULL),
	(56,144,1,1,NULL),
	(57,2,1,11,NULL),
	(58,29,1,4,NULL),
	(59,27,1,4,NULL),
	(60,28,1,3,NULL),
	(61,2,1,10,NULL),
	(62,2,1,9,NULL),
	(63,2,1,8,NULL),
	(64,2,1,7,NULL),
	(65,2,1,6,NULL),
	(66,29,1,3,NULL),
	(67,29,1,2,NULL),
	(68,28,1,2,NULL),
	(69,27,1,3,NULL),
	(70,27,1,2,NULL),
	(71,26,1,4,NULL),
	(72,26,1,3,NULL),
	(73,26,1,2,NULL),
	(74,2,1,5,NULL),
	(75,2,1,4,NULL),
	(76,2,1,3,NULL),
	(77,82,1,1,NULL),
	(78,79,1,1,NULL),
	(79,5,1,4,NULL),
	(80,67,1,3,NULL),
	(81,6,1,13,NULL),
	(82,73,1,1,NULL),
	(83,6,1,12,NULL),
	(84,6,1,11,NULL),
	(85,6,1,10,NULL),
	(86,2,1,2,NULL),
	(87,2,1,1,'Revision from Jan 31, 2019, 12:56:23 PM'),
	(88,6,1,9,NULL),
	(89,5,1,3,NULL),
	(90,5,1,2,NULL),
	(91,10,1,3,NULL),
	(92,10,1,2,NULL),
	(93,6,1,8,NULL),
	(94,6,1,7,NULL),
	(95,67,1,2,NULL),
	(96,6,1,6,NULL),
	(97,67,1,1,NULL),
	(98,6,1,5,NULL),
	(99,6,1,4,NULL),
	(100,6,1,3,NULL),
	(101,6,1,2,NULL),
	(102,38,1,1,NULL),
	(103,29,1,1,NULL),
	(104,28,1,1,NULL),
	(105,27,1,1,NULL),
	(106,26,1,1,NULL),
	(107,10,1,1,NULL),
	(108,7,1,1,NULL),
	(109,6,1,1,NULL),
	(110,5,1,1,NULL),
	(111,2,1,26,NULL),
	(112,6,1,19,NULL);

/*!40000 ALTER TABLE `craft_revisions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_searchindex
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_searchindex`;

CREATE TABLE `craft_searchindex` (
  `elementId` int(11) NOT NULL,
  `attribute` varchar(25) NOT NULL,
  `fieldId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `keywords` text NOT NULL,
  PRIMARY KEY (`elementId`,`attribute`,`fieldId`,`siteId`),
  FULLTEXT KEY `craft_searchindex_keywords_idx` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `craft_searchindex` WRITE;
/*!40000 ALTER TABLE `craft_searchindex` DISABLE KEYS */;

INSERT INTO `craft_searchindex` (`elementId`, `attribute`, `fieldId`, `siteId`, `keywords`)
VALUES
	(1,'username',0,1,' nerdymind '),
	(1,'firstname',0,1,''),
	(1,'lastname',0,1,''),
	(1,'fullname',0,1,''),
	(1,'email',0,1,' dev nerdymind com '),
	(1,'slug',0,1,''),
	(2,'title',0,1,' homepage '),
	(2,'slug',0,1,' homepage '),
	(3,'slug',0,1,''),
	(4,'slug',0,1,''),
	(3,'field',58,1,''),
	(4,'field',57,1,''),
	(5,'slug',0,1,' test blog '),
	(5,'title',0,1,' test blog '),
	(6,'field',61,1,''),
	(6,'field',54,1,' none '),
	(6,'field',55,1,''),
	(6,'title',0,1,' blocks '),
	(6,'slug',0,1,' blocks '),
	(7,'field',61,1,''),
	(7,'field',54,1,' blog '),
	(7,'field',55,1,''),
	(7,'slug',0,1,' blog '),
	(7,'title',0,1,' blog '),
	(54,'title',0,1,' stories '),
	(54,'slug',0,1,' stories '),
	(53,'title',0,1,' news '),
	(53,'slug',0,1,' news '),
	(5,'field',46,1,' story1 image '),
	(5,'field',47,1,' news '),
	(5,'field',61,1,' 6 lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum 6 lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum '),
	(10,'field',51,1,' kid smiling '),
	(10,'field',69,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua '),
	(10,'field',70,1,' test name last name '),
	(10,'slug',0,1,' test '),
	(10,'title',0,1,' test '),
	(11,'filename',0,1,' text banner jpg '),
	(11,'extension',0,1,' jpg '),
	(11,'kind',0,1,' image '),
	(11,'slug',0,1,''),
	(11,'title',0,1,' text banner '),
	(12,'filename',0,1,' place holder video jpg '),
	(12,'extension',0,1,' jpg '),
	(12,'kind',0,1,' image '),
	(12,'slug',0,1,''),
	(12,'title',0,1,' place holder video '),
	(13,'filename',0,1,' brochure background jpg '),
	(13,'extension',0,1,' jpg '),
	(13,'kind',0,1,' image '),
	(13,'slug',0,1,''),
	(13,'title',0,1,' brochure background '),
	(14,'filename',0,1,' brochure image png '),
	(14,'extension',0,1,' png '),
	(14,'kind',0,1,' image '),
	(14,'slug',0,1,''),
	(14,'title',0,1,' brochure image '),
	(15,'filename',0,1,' kid smiling jpg '),
	(15,'extension',0,1,' jpg '),
	(15,'kind',0,1,' image '),
	(15,'slug',0,1,''),
	(15,'title',0,1,' kid smiling '),
	(16,'filename',0,1,' map placeholder jpg '),
	(16,'extension',0,1,' jpg '),
	(16,'kind',0,1,' image '),
	(16,'slug',0,1,''),
	(16,'title',0,1,' map placeholder '),
	(17,'filename',0,1,' black board texture jpg '),
	(17,'extension',0,1,' jpg '),
	(17,'kind',0,1,' image '),
	(17,'slug',0,1,''),
	(17,'title',0,1,' black board texture '),
	(18,'filename',0,1,' hero image jpg '),
	(18,'extension',0,1,' jpg '),
	(18,'kind',0,1,' image '),
	(18,'slug',0,1,''),
	(18,'title',0,1,' hero image '),
	(19,'filename',0,1,' story3 image jpg '),
	(19,'extension',0,1,' jpg '),
	(19,'kind',0,1,' image '),
	(19,'slug',0,1,''),
	(19,'title',0,1,' story3 image '),
	(20,'filename',0,1,' story2 image jpg '),
	(20,'extension',0,1,' jpg '),
	(20,'kind',0,1,' image '),
	(20,'slug',0,1,''),
	(20,'title',0,1,' story2 image '),
	(21,'filename',0,1,' story1 image jpg '),
	(21,'extension',0,1,' jpg '),
	(21,'kind',0,1,' image '),
	(21,'slug',0,1,''),
	(21,'title',0,1,' story1 image '),
	(3,'field',71,1,' https www facebook com https twitter com https www instagram com https www youtube com '),
	(22,'field',72,1,''),
	(22,'field',73,1,' https www facebook com '),
	(22,'slug',0,1,''),
	(23,'field',72,1,''),
	(23,'field',73,1,' https twitter com '),
	(23,'slug',0,1,''),
	(24,'field',72,1,''),
	(24,'field',73,1,' https www instagram com '),
	(24,'slug',0,1,''),
	(25,'field',72,1,''),
	(25,'field',73,1,' https www youtube com '),
	(25,'slug',0,1,''),
	(26,'field',61,1,' 6 6 we know that true transformation in our world begins with education that is why we created project heart our philanthropy education curriculum that teaches young people that they have the power and ability to create real lasting change in their communities and the world project heart our flagship educational curriculum teaches 4th 12th grade students and students the foundational principles of philanthropy while challenging young people to give their time talents and treasure to someone else in need designed by teachers for teachers the curriculum follows the understanding by design ubd framework and aligns with common core for easy adoption into the classroom all while focusing on the 3 t s of philanthropy time talent and treasure students learn that no matter their age background or ability when you use your 3 t s anyone can be a philanthropists and make a difference project heart is one way that we are investing in the future creating lasting and impactful change in our world through educating and inspiring young people to identify needs and work to solve the root of the issues in their communities we believe that one inspired mind can change the world and because of this we are striving to foster an entire generation of young philanthropists general contact '),
	(26,'field',54,1,' none '),
	(26,'field',55,1,' inspire your students to change the world '),
	(26,'slug',0,1,' about '),
	(26,'title',0,1,' about '),
	(27,'field',61,1,' project heart for 4th 5th grade challenges your students to find the philanthropist inside and address the needs they see in the world around them as a class in these 9 lessons students are identifying personal passions writing mission statements learning about philanthropists and completing a class service project each lesson averages 35 minutes aligns with the common core state standards and utilizes multiple best practice teaching strategies students learn that by using their time talent and or treasure they can be change agents who make a huge philanthropic impact contact left elementary place holder video child 865116_1920 1b4a7189_filter kindness '),
	(27,'field',54,1,' none '),
	(27,'field',55,1,''),
	(27,'slug',0,1,' elementary school '),
	(27,'title',0,1,' project heart for elementary school '),
	(28,'field',61,1,' project heart for middle school engages your students to think about how they can personally make a difference with their time talents and or treasure in these 10 lessons students create measurable goals for executing their plans for philanthropic impact each lesson averages 20 25 minutes aligns with the common core state standards and utilizes multiple best practice teaching strategies discovering what philanthropy means to them students learn how their personal passions will drive their philanthropic mission students work in small groups or individually for their student led projects while implementing ways to affect change on a local national and global level contact left middle school school 1974369_1920 school group 1b4a7455 bleach 1b4a7199_bleach smooth '),
	(28,'field',54,1,' none '),
	(28,'field',55,1,' project heart for middle school '),
	(28,'slug',0,1,' middle school '),
	(28,'title',0,1,' middle school '),
	(29,'field',61,1,' project heart for high school provides a platform for students to explore how their passions and career goals can align with their philanthropic mission in these 10 lessons students create measurable goals for executing their plans for philanthropic impact each lesson averages 20 25 minutes aligns with the common core state standards and utilizes multiple best practice teaching strategies discovering what philanthropy means to them students learn how their personal passions will drive their philanthropic mission students work in small groups or individually for their student led projects while implementing ways to affect change on a local national and global level contact left high school story1 image school 1974369_1920 shutterstock_194717693 2 story2 image '),
	(29,'field',54,1,' none '),
	(29,'field',55,1,''),
	(29,'slug',0,1,' high school '),
	(29,'title',0,1,' project heart for high school '),
	(30,'title',0,1,' home '),
	(30,'slug',0,1,''),
	(31,'slug',0,1,''),
	(31,'title',0,1,' about '),
	(32,'slug',0,1,''),
	(32,'title',0,1,' blog '),
	(33,'slug',0,1,''),
	(33,'title',0,1,' middle school '),
	(34,'slug',0,1,''),
	(34,'title',0,1,' elementary school '),
	(35,'slug',0,1,''),
	(35,'title',0,1,' high school '),
	(36,'title',0,1,' home '),
	(36,'slug',0,1,''),
	(37,'slug',0,1,''),
	(37,'title',0,1,' blog '),
	(38,'field',61,1,''),
	(38,'field',54,1,' none '),
	(38,'field',55,1,''),
	(38,'slug',0,1,' privacy policy '),
	(38,'title',0,1,' privacy policy '),
	(172,'slug',0,1,''),
	(40,'filename',0,1,' logo svg '),
	(40,'extension',0,1,' svg '),
	(40,'kind',0,1,' image '),
	(40,'slug',0,1,''),
	(40,'title',0,1,' logo '),
	(41,'filename',0,1,' banner jpg '),
	(41,'extension',0,1,' jpg '),
	(41,'kind',0,1,' image '),
	(41,'slug',0,1,''),
	(41,'title',0,1,' banner '),
	(42,'filename',0,1,' grayheart png '),
	(42,'extension',0,1,' png '),
	(42,'kind',0,1,' image '),
	(42,'slug',0,1,''),
	(42,'title',0,1,' gray heart '),
	(43,'field',51,1,' logo '),
	(43,'field',49,1,' ottercaresphilanthropyprogram '),
	(43,'field',2,1,' banner '),
	(43,'slug',0,1,''),
	(44,'field',51,1,' place holder video '),
	(44,'field',62,1,' https www youtube com watch v=scmzivxbsi4 t=4s '),
	(44,'field',63,1,' left '),
	(44,'field',64,1,' 6 '),
	(44,'field',49,1,' headline lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum header header header header button '),
	(44,'field',1,1,''),
	(44,'field',2,1,' gray heart '),
	(44,'field',4,1,''),
	(44,'field',5,1,''),
	(44,'slug',0,1,''),
	(45,'field',49,1,' color text and then non color '),
	(45,'slug',0,1,''),
	(46,'filename',0,1,' red heart svg '),
	(46,'extension',0,1,' svg '),
	(46,'kind',0,1,' image '),
	(46,'slug',0,1,''),
	(46,'title',0,1,' red heart '),
	(47,'field',41,1,' red heart homepage headline here with will wrap maybe gray heart homepage headline here with will wrap maybe red heart homepage headline here with will wrap maybe '),
	(47,'field',1,1,''),
	(47,'field',2,1,''),
	(47,'field',4,1,''),
	(47,'field',5,1,''),
	(48,'field',42,1,' red heart '),
	(48,'field',43,1,' headline here with will wrap maybe '),
	(48,'field',45,1,' homepage '),
	(48,'slug',0,1,''),
	(49,'field',42,1,' gray heart '),
	(49,'field',43,1,' headline here with will wrap maybe '),
	(49,'field',45,1,' homepage '),
	(49,'slug',0,1,''),
	(50,'field',42,1,' red heart '),
	(50,'field',43,1,' headline here with will wrap maybe '),
	(50,'field',45,1,' homepage '),
	(50,'slug',0,1,''),
	(47,'slug',0,1,''),
	(51,'slug',0,1,' high school '),
	(51,'title',0,1,' high school '),
	(52,'slug',0,1,' general '),
	(52,'title',0,1,' general '),
	(55,'slug',0,1,' general '),
	(55,'title',0,1,' general '),
	(10,'field',74,1,''),
	(56,'filename',0,1,' school group jpg '),
	(56,'extension',0,1,' jpg '),
	(56,'kind',0,1,' image '),
	(56,'slug',0,1,''),
	(56,'title',0,1,' school group '),
	(57,'filename',0,1,' school jpg '),
	(57,'extension',0,1,' jpg '),
	(57,'kind',0,1,' image '),
	(57,'slug',0,1,''),
	(57,'title',0,1,' school '),
	(58,'filename',0,1,' chirstmas tree jpg '),
	(58,'extension',0,1,' jpg '),
	(58,'kind',0,1,' image '),
	(58,'slug',0,1,''),
	(58,'title',0,1,' chirstmas tree '),
	(59,'field',74,1,' general '),
	(59,'slug',0,1,''),
	(60,'field',47,1,''),
	(60,'slug',0,1,''),
	(61,'field',37,1,' contact '),
	(61,'slug',0,1,''),
	(62,'field',65,1,' school https www facebook com chirstmas tree https www facebook com school group https www facebook com school https www facebook com '),
	(62,'field',1,1,''),
	(62,'field',2,1,''),
	(62,'field',4,1,''),
	(62,'field',5,1,''),
	(63,'field',66,1,''),
	(63,'field',67,1,' school '),
	(63,'field',68,1,' https www facebook com '),
	(63,'slug',0,1,''),
	(64,'field',66,1,''),
	(64,'field',67,1,' chirstmas tree '),
	(64,'field',68,1,' https www facebook com '),
	(64,'slug',0,1,''),
	(65,'field',66,1,''),
	(65,'field',67,1,' school group '),
	(65,'field',68,1,' https www facebook com '),
	(65,'slug',0,1,''),
	(66,'field',66,1,''),
	(66,'field',67,1,' school '),
	(66,'field',68,1,' https www facebook com '),
	(66,'slug',0,1,''),
	(62,'slug',0,1,''),
	(67,'slug',0,1,' contact '),
	(67,'title',0,1,' contact '),
	(67,'field',48,1,' contact '),
	(68,'field',38,1,' 6 lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum 6 lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum '),
	(68,'field',1,1,''),
	(68,'field',2,1,' text banner '),
	(68,'field',4,1,''),
	(68,'field',5,1,''),
	(69,'field',39,1,' 6 '),
	(69,'field',40,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum '),
	(69,'slug',0,1,''),
	(70,'field',39,1,' 6 '),
	(70,'field',40,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum '),
	(70,'slug',0,1,''),
	(68,'slug',0,1,''),
	(71,'field',52,1,' banner school story2 image brochure image chirstmas tree hero image kid smiling map placeholder place holder video school group story1 image '),
	(71,'slug',0,1,''),
	(5,'field',75,1,' this is the text that shows up on the blog module '),
	(2,'field',61,1,''),
	(72,'field',51,1,' logo '),
	(72,'field',49,1,' ottercaresphilanthropy program '),
	(72,'field',2,1,' banner '),
	(72,'slug',0,1,''),
	(73,'field',51,1,' banner '),
	(73,'field',69,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur '),
	(73,'field',70,1,' john smith student '),
	(73,'field',74,1,''),
	(73,'slug',0,1,' test2 '),
	(73,'title',0,1,' test2 '),
	(74,'field',38,1,' 12 itemlorem ipsum dolorlorem ipsum dolor sit ametlorem ipsum dolorlorem ipsum dolor sit ametincididunt ut labore et dolore magna aliqualorem ipsum dolorlorem ipsum dolor sit ametlorem ipsum dolorlorem ipsum dolor sit ametincididunt ut labore et dolore magna aliquaitem '),
	(74,'field',1,1,''),
	(74,'field',2,1,''),
	(74,'field',4,1,''),
	(74,'field',5,1,''),
	(75,'field',39,1,' 12 '),
	(75,'field',40,1,' itemlorem ipsum dolorlorem ipsum dolor sit ametlorem ipsum dolorlorem ipsum dolor sit ametincididunt ut labore et dolore magna aliqualorem ipsum dolorlorem ipsum dolor sit ametlorem ipsum dolorlorem ipsum dolor sit ametincididunt ut labore et dolore magna aliquaitem '),
	(75,'slug',0,1,''),
	(74,'slug',0,1,''),
	(67,'field',53,1,' share your projects and stories '),
	(76,'field',38,1,' 6 lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum 6 lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum '),
	(76,'field',1,1,''),
	(76,'field',2,1,''),
	(76,'field',4,1,''),
	(76,'field',5,1,''),
	(77,'field',39,1,' 6 '),
	(77,'field',40,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum '),
	(77,'slug',0,1,''),
	(78,'field',39,1,' 6 '),
	(78,'field',40,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum '),
	(78,'slug',0,1,''),
	(76,'slug',0,1,''),
	(79,'field',75,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua '),
	(79,'field',46,1,' banner '),
	(79,'field',47,1,' general '),
	(79,'field',61,1,' 12 lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum '),
	(80,'field',38,1,' 12 lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum '),
	(80,'field',1,1,''),
	(80,'field',2,1,''),
	(80,'field',4,1,''),
	(80,'field',5,1,''),
	(81,'field',39,1,' 12 '),
	(81,'field',40,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum '),
	(81,'slug',0,1,''),
	(80,'slug',0,1,''),
	(79,'slug',0,1,' test blog2 '),
	(79,'title',0,1,' test blog2 '),
	(82,'field',75,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua '),
	(82,'field',46,1,' school '),
	(82,'field',47,1,' news '),
	(82,'field',61,1,' 12 lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua '),
	(83,'field',38,1,' 12 lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua '),
	(83,'field',1,1,''),
	(83,'field',2,1,''),
	(83,'field',4,1,''),
	(83,'field',5,1,''),
	(84,'field',39,1,' 12 '),
	(84,'field',40,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua '),
	(84,'slug',0,1,''),
	(83,'slug',0,1,''),
	(82,'slug',0,1,' test blog 3 '),
	(82,'title',0,1,' test blog 3 '),
	(90,'filename',0,1,' donate jpg '),
	(90,'extension',0,1,' jpg '),
	(90,'kind',0,1,' image '),
	(90,'slug',0,1,''),
	(90,'title',0,1,' donate '),
	(91,'filename',0,1,' giving jpg '),
	(91,'extension',0,1,' jpg '),
	(91,'kind',0,1,' image '),
	(91,'slug',0,1,''),
	(91,'title',0,1,' giving '),
	(92,'filename',0,1,' hearthands jpg '),
	(92,'extension',0,1,' jpg '),
	(92,'kind',0,1,' image '),
	(92,'slug',0,1,''),
	(92,'title',0,1,' hearthands '),
	(93,'filename',0,1,' kindness jpg '),
	(93,'extension',0,1,' jpg '),
	(93,'kind',0,1,' image '),
	(93,'slug',0,1,''),
	(93,'title',0,1,' kindness '),
	(94,'filename',0,1,' notebook 2247351_1920 jpg '),
	(94,'extension',0,1,' jpg '),
	(94,'kind',0,1,' image '),
	(94,'slug',0,1,''),
	(94,'title',0,1,' notebook 2247351_1920 '),
	(95,'filename',0,1,' school 1974369_1920 jpg '),
	(89,'title',0,1,' child 865116_1920 '),
	(89,'slug',0,1,''),
	(87,'extension',0,1,' jpg '),
	(87,'kind',0,1,' image '),
	(87,'slug',0,1,''),
	(87,'title',0,1,' boy 1205255_1920 '),
	(88,'filename',0,1,' card jpg '),
	(88,'extension',0,1,' jpg '),
	(88,'kind',0,1,' image '),
	(88,'slug',0,1,''),
	(88,'title',0,1,' card '),
	(89,'filename',0,1,' child 865116_1920 jpg '),
	(89,'extension',0,1,' jpg '),
	(89,'kind',0,1,' image '),
	(87,'filename',0,1,' boy 1205255_1920 jpg '),
	(95,'extension',0,1,' jpg '),
	(95,'kind',0,1,' image '),
	(95,'slug',0,1,''),
	(95,'title',0,1,' school 1974369_1920 '),
	(96,'filename',0,1,' shutterstock_162276314_filter jpg '),
	(96,'extension',0,1,' jpg '),
	(96,'kind',0,1,' image '),
	(96,'slug',0,1,''),
	(96,'title',0,1,' shutterstock_162276314_filter '),
	(97,'filename',0,1,' shutterstock_194717693 2 jpg '),
	(97,'extension',0,1,' jpg '),
	(97,'kind',0,1,' image '),
	(97,'slug',0,1,''),
	(97,'title',0,1,' shutterstock_194717693 2 '),
	(98,'field',38,1,' 6 6 we know that true transformation in our world begins with education that is why we created project heart our philanthropy education curriculum that teaches young people that they have the power and ability to create real lasting change in their communities and the world project heart our flagship educational curriculum teaches 4th 12th grade students and students the foundational principles of philanthropy while challenging young people to give their time talents and treasure to someone else in need designed by teachers for teachers the curriculum follows the understanding by design ubd framework and aligns with common core for easy adoption into the classroom all while focusing on the 3 t s of philanthropy time talent and treasure students learn that no matter their age background or ability when you use your 3 t s anyone can be a philanthropists and make a difference project heart is one way that we are investing in the future creating lasting and impactful change in our world through educating and inspiring young people to identify needs and work to solve the root of the issues in their communities we believe that one inspired mind can change the world and because of this we are striving to foster an entire generation of young philanthropists '),
	(98,'field',1,1,''),
	(98,'field',2,1,''),
	(98,'field',4,1,''),
	(98,'field',5,1,''),
	(99,'field',39,1,' 6 '),
	(99,'field',40,1,' we know that true transformation in our world begins with education that is why we created project heart our philanthropy education curriculum that teaches young people that they have the power and ability to create real lasting change in their communities and the world project heart our flagship educational curriculum teaches 4th 12th grade students and students the foundational principles of philanthropy while challenging young people to give their time talents and treasure to someone else in need designed by teachers for teachers the curriculum follows the understanding by design ubd framework and aligns with common core for easy adoption into the classroom all while focusing on the 3 t s of philanthropy time talent and treasure students learn that no matter their age background or ability when you use your 3 t s anyone can be a philanthropists and make a difference project heart is one way that we are investing in the future creating lasting and impactful change in our world through educating and inspiring young people to identify needs and work to solve the root of the issues in their communities we believe that one inspired mind can change the world and because of this we are striving to foster an entire generation of young philanthropists '),
	(99,'slug',0,1,''),
	(100,'field',39,1,' 6 '),
	(100,'field',40,1,''),
	(100,'slug',0,1,''),
	(98,'slug',0,1,''),
	(101,'field',74,1,' general '),
	(101,'slug',0,1,''),
	(98,'field',3,1,''),
	(169,'field',37,1,' contact '),
	(175,'field',49,1,' project heart for middle school engages your students to think about how they can personally make a difference with their time talents and or treasure in these 10 lessons students create measurable goals for executing their plans for philanthropic impact each lesson averages 20 25 minutes aligns with the common core state standards and utilizes multiple best practice teaching strategies discovering what philanthropy means to them students learn how their personal passions will drive their philanthropic mission students work in small groups or individually for their student led projects while implementing ways to affect change on a local national and global level '),
	(175,'field',48,1,' contact '),
	(175,'slug',0,1,''),
	(175,'field',63,1,' left '),
	(176,'slug',0,1,''),
	(176,'field',48,1,' contact '),
	(176,'field',63,1,' left '),
	(176,'field',49,1,' project heart for high school provides a platform for students to explore how their passions and career goals can align with their philanthropic mission in these 10 lessons students create measurable goals for executing their plans for philanthropic impact each lesson averages 20 25 minutes aligns with the common core state standards and utilizes multiple best practice teaching strategies discovering what philanthropy means to them students learn how their personal passions will drive their philanthropic mission students work in small groups or individually for their student led projects while implementing ways to affect change on a local national and global level '),
	(179,'slug',0,1,''),
	(180,'field',74,1,' general '),
	(180,'slug',0,1,''),
	(113,'field',38,1,' 12 choose your grade '),
	(113,'field',1,1,''),
	(113,'field',2,1,' text banner '),
	(113,'field',4,1,''),
	(113,'field',5,1,''),
	(114,'field',39,1,' 12 '),
	(114,'field',40,1,' choose your grade '),
	(114,'slug',0,1,''),
	(113,'slug',0,1,''),
	(115,'field',41,1,' red heart project heart for elementary school elementary school curriculum red heart middle school middle school curriculum red heart project heart for high school high school curriculum '),
	(115,'field',1,1,''),
	(115,'field',2,1,' gray background fp '),
	(115,'field',4,1,''),
	(115,'field',5,1,''),
	(116,'field',42,1,' red heart '),
	(116,'field',43,1,' elementary school curriculum '),
	(116,'field',45,1,' project heart for elementary school '),
	(116,'slug',0,1,''),
	(117,'field',42,1,' red heart '),
	(117,'field',43,1,' middle school curriculum '),
	(117,'field',45,1,' middle school '),
	(117,'slug',0,1,''),
	(118,'field',42,1,' red heart '),
	(118,'field',43,1,' high school curriculum '),
	(118,'field',45,1,' project heart for high school '),
	(118,'slug',0,1,''),
	(115,'slug',0,1,''),
	(119,'field',74,1,' general '),
	(119,'slug',0,1,''),
	(120,'field',38,1,' 6 6 project heart at a glance learn more about what we do at project heart download '),
	(120,'field',1,1,''),
	(120,'field',2,1,' brochure background '),
	(120,'field',4,1,''),
	(120,'field',5,1,''),
	(121,'field',39,1,' 6 '),
	(121,'field',40,1,''),
	(121,'slug',0,1,''),
	(122,'field',39,1,' 6 '),
	(122,'field',40,1,' project heart at a glance learn more about what we do at project heart download '),
	(122,'slug',0,1,''),
	(120,'slug',0,1,''),
	(123,'field',38,1,' 6 6 we make speak different languages or have different cultures but one thing that unites us is our desire to create a better world from california to maine to vietnam to australia project heart is igniting the flame of philanthropy in students around the world inspiring them to be change makers today '),
	(123,'field',1,1,''),
	(123,'field',2,1,''),
	(123,'field',4,1,''),
	(123,'field',5,1,''),
	(124,'field',39,1,' 6 '),
	(124,'field',40,1,''),
	(124,'slug',0,1,''),
	(125,'field',39,1,' 6 '),
	(125,'field',40,1,' we make speak different languages or have different cultures but one thing that unites us is our desire to create a better world from california to maine to vietnam to australia project heart is igniting the flame of philanthropy in students around the world inspiring them to be change makers today '),
	(125,'slug',0,1,''),
	(123,'slug',0,1,''),
	(126,'slug',0,1,''),
	(127,'field',37,1,' contact '),
	(127,'slug',0,1,''),
	(128,'field',65,1,' 39265722_1663716650421197_7196011610442301440_o https www facebook com groups 1931603617055357 source_id=520600891399451 child 865116_1920 https twitter com ottercares school group https www instagram com ottercares chirstmas tree https www youtube com user ottercaresfoundation '),
	(128,'field',1,1,''),
	(128,'field',2,1,''),
	(128,'field',4,1,''),
	(128,'field',5,1,''),
	(129,'field',66,1,''),
	(129,'field',67,1,' 39265722_1663716650421197_7196011610442301440_o '),
	(129,'field',68,1,' https www facebook com groups 1931603617055357 source_id=520600891399451 '),
	(129,'slug',0,1,''),
	(130,'field',66,1,''),
	(130,'field',67,1,' child 865116_1920 '),
	(130,'field',68,1,' https twitter com ottercares '),
	(130,'slug',0,1,''),
	(131,'field',66,1,''),
	(131,'field',67,1,' school group '),
	(131,'field',68,1,' https www instagram com ottercares '),
	(131,'slug',0,1,''),
	(132,'field',66,1,''),
	(132,'field',67,1,' chirstmas tree '),
	(132,'field',68,1,' https www youtube com user ottercaresfoundation '),
	(132,'slug',0,1,''),
	(128,'slug',0,1,''),
	(133,'filename',0,1,' 1b4a7189_filter jpg '),
	(133,'extension',0,1,' jpg '),
	(133,'kind',0,1,' image '),
	(133,'slug',0,1,''),
	(133,'title',0,1,' 1b4a7189_filter '),
	(134,'filename',0,1,' 1b4a7199_bleachsmooth jpg '),
	(134,'extension',0,1,' jpg '),
	(134,'kind',0,1,' image '),
	(134,'slug',0,1,''),
	(134,'title',0,1,' 1b4a7199_bleach smooth '),
	(135,'filename',0,1,' 1b4a7252_modify jpg '),
	(135,'extension',0,1,' jpg '),
	(135,'kind',0,1,' image '),
	(135,'slug',0,1,''),
	(135,'title',0,1,' 1b4a7252_modify '),
	(136,'filename',0,1,' 1b4a7455 bleach jpg '),
	(136,'extension',0,1,' jpg '),
	(136,'kind',0,1,' image '),
	(136,'slug',0,1,''),
	(136,'title',0,1,' 1b4a7455 bleach '),
	(137,'filename',0,1,' 1b4a7481_bleachsmooth jpg '),
	(137,'extension',0,1,' jpg '),
	(137,'kind',0,1,' image '),
	(137,'slug',0,1,''),
	(137,'title',0,1,' 1b4a7481_bleach smooth '),
	(138,'filename',0,1,' 1b4a7611_bleachsmooth jpg '),
	(138,'extension',0,1,' jpg '),
	(138,'kind',0,1,' image '),
	(138,'slug',0,1,''),
	(138,'title',0,1,' 1b4a7611_bleach smooth '),
	(140,'field',52,1,' school 1974369_1920 school group 1b4a7455 bleach 1b4a7199_bleach smooth '),
	(140,'slug',0,1,''),
	(142,'field',52,1,' place holder video child 865116_1920 1b4a7189_filter kindness '),
	(142,'slug',0,1,''),
	(179,'fullname',0,1,''),
	(179,'email',0,1,' jessica dieken ottercares org '),
	(143,'field',52,1,' story1 image school 1974369_1920 shutterstock_194717693 2 story2 image '),
	(143,'slug',0,1,''),
	(144,'field',51,1,' 1b4a7455 bleach '),
	(144,'field',69,1,' having project heart connects you with the students on a deeper level and you all understand that you re working together to make the world a better '),
	(144,'field',70,1,' 4th grade teacher at shepardson elementary '),
	(144,'field',74,1,' general elementary '),
	(144,'slug',0,1,' elementary 1 '),
	(144,'title',0,1,' elementary 1 '),
	(145,'slug',0,1,' middle school '),
	(145,'title',0,1,' middle school '),
	(146,'field',51,1,' 1b4a7481_bleach smooth '),
	(146,'field',69,1,' the kids know that the world needs them now they don t need to wait '),
	(146,'field',70,1,' 6th 8th grade teacher '),
	(146,'field',74,1,' general middle school '),
	(146,'slug',0,1,' middle school 1 '),
	(146,'title',0,1,' middle school 1 '),
	(147,'field',51,1,' 1b4a7611_bleach smooth '),
	(147,'field',69,1,' anyone can be a philanthropist you just have to figure out that every little thing you do every 30 minutes 20 minutes that you spend your time and using your talents to make the world you a better place that makes you a philanthropist anyone can be one you just have to apply yourself '),
	(147,'field',70,1,' student 16 '),
	(147,'field',74,1,' high school general '),
	(147,'slug',0,1,' high school 1 '),
	(147,'title',0,1,' high school 1 '),
	(148,'slug',0,1,' elementary '),
	(148,'title',0,1,' elementary '),
	(149,'field',51,1,' kid smiling '),
	(149,'field',69,1,' one thing that i am walking away with from project heart is knowing that i helped someone and knowing that i impacted their life '),
	(149,'field',70,1,' 5th grade student at cache la poudre elementary '),
	(149,'field',74,1,' general elementary '),
	(149,'slug',0,1,' elementary 2 '),
	(149,'title',0,1,' elementary 2 '),
	(150,'field',51,1,' shutterstock_162276314_filter '),
	(150,'field',69,1,' many parents community members and colleagues were impressed that we were helping kids develop the habit of using their time talent or fundraising skills to give back to others kids innately want to help others and this project was an easy and useful way to tap into this desire '),
	(150,'field',70,1,' 4 5th grade teacher at cache la poudre elementary school '),
	(150,'field',74,1,' general elementary '),
	(150,'slug',0,1,' elementary 3 '),
	(150,'title',0,1,' elementary 3 '),
	(151,'filename',0,1,' 39265722_1663716650421197_7196011610442301440_o jpg '),
	(151,'extension',0,1,' jpg '),
	(151,'kind',0,1,' image '),
	(151,'slug',0,1,''),
	(151,'title',0,1,' 39265722_1663716650421197_7196011610442301440_o '),
	(44,'field',3,1,''),
	(68,'field',3,1,' image '),
	(47,'field',3,1,''),
	(62,'field',3,1,''),
	(74,'field',3,1,''),
	(113,'field',3,1,' image '),
	(115,'field',3,1,' image '),
	(120,'field',3,1,' image '),
	(123,'field',3,1,''),
	(128,'field',3,1,''),
	(152,'field',38,1,' 12 project heart in action '),
	(152,'field',3,1,' image '),
	(152,'field',1,1,''),
	(152,'field',2,1,' text banner '),
	(152,'field',4,1,''),
	(152,'field',5,1,''),
	(153,'field',39,1,' 12 '),
	(153,'field',40,1,' project heart in action '),
	(153,'slug',0,1,''),
	(152,'slug',0,1,''),
	(154,'field',38,1,' 12 download the project heart brochure '),
	(154,'field',3,1,' image '),
	(154,'field',1,1,''),
	(154,'field',2,1,' text banner '),
	(154,'field',4,1,''),
	(154,'field',5,1,''),
	(155,'field',39,1,' 12 '),
	(155,'field',40,1,' download the project heart brochure '),
	(155,'slug',0,1,''),
	(154,'slug',0,1,''),
	(156,'field',38,1,' 12 world changers in training '),
	(156,'field',3,1,' image '),
	(156,'field',1,1,''),
	(156,'field',2,1,' text banner '),
	(156,'field',4,1,''),
	(156,'field',5,1,''),
	(157,'field',39,1,' 12 '),
	(157,'field',40,1,' world changers in training '),
	(157,'slug',0,1,''),
	(156,'slug',0,1,''),
	(158,'field',38,1,' 12 our stories '),
	(158,'field',3,1,' image '),
	(158,'field',1,1,''),
	(158,'field',2,1,' text banner '),
	(158,'field',4,1,''),
	(158,'field',5,1,''),
	(159,'field',39,1,' 12 '),
	(159,'field',40,1,' our stories '),
	(159,'slug',0,1,''),
	(158,'slug',0,1,''),
	(160,'field',38,1,' 12 connect with ottercares '),
	(160,'field',3,1,' image '),
	(160,'field',1,1,''),
	(160,'field',2,1,' text banner '),
	(160,'field',4,1,''),
	(160,'field',5,1,''),
	(161,'field',39,1,' 12 '),
	(161,'field',40,1,' connect with ottercares '),
	(161,'slug',0,1,''),
	(160,'slug',0,1,''),
	(162,'filename',0,1,' gray background jpg '),
	(162,'extension',0,1,' jpg '),
	(162,'kind',0,1,' image '),
	(162,'slug',0,1,''),
	(162,'title',0,1,' gray background '),
	(167,'filename',0,1,' gray heart background jpg '),
	(167,'extension',0,1,' jpg '),
	(167,'kind',0,1,' image '),
	(167,'slug',0,1,''),
	(167,'title',0,1,' gray heart background '),
	(168,'filename',0,1,' graybackgroundfp jpg '),
	(168,'extension',0,1,' jpg '),
	(168,'kind',0,1,' image '),
	(168,'slug',0,1,''),
	(168,'title',0,1,' gray background fp '),
	(164,'field',38,1,' 6 6 we re on a mission to change the world one classroom at a time changing the world is a big goal and we can t do it alone we want to ignite an entire generation of thinkers makers doers and givers who have passion and resource to tackle the world s biggest problems that is why we created project heart our philanthropy education curriculum project heart taps into every child s innate desire to give while providing them with the tools and framework they need to be tomorrow s world changers today '),
	(164,'field',3,1,' image '),
	(164,'field',1,1,''),
	(164,'field',2,1,' gray heart background '),
	(164,'field',4,1,''),
	(164,'field',5,1,''),
	(165,'field',39,1,' 6 '),
	(165,'field',40,1,''),
	(165,'slug',0,1,''),
	(166,'field',39,1,' 6 '),
	(166,'field',40,1,' we re on a mission to change the world one classroom at a time changing the world is a big goal and we can t do it alone we want to ignite an entire generation of thinkers makers doers and givers who have passion and resource to tackle the world s biggest problems that is why we created project heart our philanthropy education curriculum project heart taps into every child s innate desire to give while providing them with the tools and framework they need to be tomorrow s world changers today '),
	(166,'slug',0,1,''),
	(164,'slug',0,1,''),
	(169,'slug',0,1,''),
	(170,'field',74,1,' elementary '),
	(170,'slug',0,1,''),
	(171,'field',74,1,' middle school '),
	(171,'slug',0,1,''),
	(172,'field',74,1,' high school '),
	(179,'lastname',0,1,''),
	(179,'username',0,1,' jessicadieken '),
	(179,'firstname',0,1,''),
	(173,'field',49,1,' lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur excepteur sint occaecat cupidatat non proident sunt in culpa qui officia deserunt mollit anim id est laborum '),
	(173,'field',63,1,' left '),
	(173,'field',48,1,' contact '),
	(173,'slug',0,1,''),
	(174,'field',49,1,' project heart for 4th 5th grade challenges your students to find the philanthropist inside and address the needs they see in the world around them as a class in these 9 lessons students are identifying personal passions writing mission statements learning about philanthropists and completing a class service project each lesson averages 35 minutes aligns with the common core state standards and utilizes multiple best practice teaching strategies students learn that by using their time talent and or treasure they can be change agents who make a huge philanthropic impact '),
	(174,'field',63,1,' left '),
	(174,'field',48,1,' contact '),
	(174,'slug',0,1,'');

/*!40000 ALTER TABLE `craft_searchindex` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_sections
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sections`;

CREATE TABLE `craft_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` enum('single','channel','structure') NOT NULL DEFAULT 'channel',
  `enableVersioning` tinyint(1) NOT NULL DEFAULT '0',
  `propagationMethod` varchar(255) NOT NULL DEFAULT 'all',
  `previewTargets` text,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_sections_handle_idx` (`handle`),
  KEY `craft_sections_name_idx` (`name`),
  KEY `craft_sections_structureId_idx` (`structureId`),
  KEY `craft_sections_dateDeleted_idx` (`dateDeleted`),
  CONSTRAINT `craft_sections_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_sections` WRITE;
/*!40000 ALTER TABLE `craft_sections` DISABLE KEYS */;

INSERT INTO `craft_sections` (`id`, `structureId`, `name`, `handle`, `type`, `enableVersioning`, `propagationMethod`, `previewTargets`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,NULL,'Blog','blog','channel',1,'all',NULL,'2019-01-21 23:38:23','2019-01-31 16:43:10',NULL,'626c6290-9346-4bc4-a02f-8da30695dd2d'),
	(2,NULL,'Call to Action','callToAction','channel',1,'all',NULL,'2019-01-21 23:38:48','2019-02-05 18:46:05',NULL,'c56e2508-9559-4291-82c0-546311d3961b'),
	(3,NULL,'Homepage','homepage','single',1,'all',NULL,'2019-01-21 23:39:16','2019-01-31 20:56:18',NULL,'9bcfd187-529c-4aca-83bc-0b68875375b7'),
	(4,1,'Page','page','structure',1,'all',NULL,'2019-01-21 23:39:46','2019-01-22 16:11:58',NULL,'1e943d8b-5226-4d93-b9a1-030f88420b7e'),
	(5,NULL,'Quotes','quotes','channel',1,'all',NULL,'2019-01-22 16:10:41','2019-01-30 23:37:21',NULL,'efe0cc28-ce97-4a7e-9ab6-bad947159ab6');

/*!40000 ALTER TABLE `craft_sections` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_sections_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sections_sites`;

CREATE TABLE `craft_sections_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sectionId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `uriFormat` text,
  `template` varchar(500) DEFAULT NULL,
  `enabledByDefault` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_sections_sites_sectionId_siteId_unq_idx` (`sectionId`,`siteId`),
  KEY `craft_sections_sites_siteId_idx` (`siteId`),
  CONSTRAINT `craft_sections_sites_sectionId_fk` FOREIGN KEY (`sectionId`) REFERENCES `craft_sections` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_sections_sites_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_sections_sites` WRITE;
/*!40000 ALTER TABLE `craft_sections_sites` DISABLE KEYS */;

INSERT INTO `craft_sections_sites` (`id`, `sectionId`, `siteId`, `hasUrls`, `uriFormat`, `template`, `enabledByDefault`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,1,1,'blog/{slug}','blog/_entry',1,'2019-01-21 23:38:23','2019-01-31 16:43:11','6a3946c0-f70e-4dcf-8a40-f7d6097e4890'),
	(2,2,1,0,NULL,NULL,0,'2019-01-21 23:38:48','2019-02-05 18:46:05','ba18bb05-8bba-4ffe-8cff-0684abd1a0d9'),
	(3,3,1,1,'__home__','',1,'2019-01-21 23:39:16','2019-01-31 20:56:18','dc8b75d0-2297-43a8-b62e-cc06feeb699c'),
	(4,4,1,1,'{slug}','page/_entry',1,'2019-01-21 23:39:46','2019-01-22 16:11:58','9eafc8f2-29b0-4214-b8d2-204b16e84218'),
	(5,5,1,1,'quotes/{slug}','quotes/_entry.twig',1,'2019-01-22 16:10:41','2019-01-30 23:37:21','3fee1571-85b2-4dfc-953a-5711628612c1');

/*!40000 ALTER TABLE `craft_sections_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_seomatic_metabundles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_seomatic_metabundles`;

CREATE TABLE `craft_seomatic_metabundles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `bundleVersion` varchar(255) NOT NULL DEFAULT '',
  `sourceBundleType` varchar(255) NOT NULL DEFAULT '',
  `sourceId` int(11) DEFAULT NULL,
  `sourceName` varchar(255) NOT NULL DEFAULT '',
  `sourceHandle` varchar(255) DEFAULT '',
  `sourceType` varchar(64) NOT NULL DEFAULT '',
  `sourceTemplate` varchar(500) DEFAULT '',
  `sourceSiteId` int(11) DEFAULT NULL,
  `sourceAltSiteSettings` text,
  `sourceDateUpdated` datetime NOT NULL,
  `metaGlobalVars` text,
  `metaSiteVars` text,
  `metaSitemapVars` text,
  `metaContainers` text,
  `redirectsContainer` text,
  `frontendTemplatesContainer` text,
  `metaBundleSettings` text,
  PRIMARY KEY (`id`),
  KEY `craft_seomatic_metabundles_sourceBundleType_idx` (`sourceBundleType`),
  KEY `craft_seomatic_metabundles_sourceId_idx` (`sourceId`),
  KEY `craft_seomatic_metabundles_sourceSiteId_idx` (`sourceSiteId`),
  KEY `craft_seomatic_metabundles_sourceHandle_idx` (`sourceHandle`),
  CONSTRAINT `craft_seomatic_metabundles_sourceSiteId_fk` FOREIGN KEY (`sourceSiteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_seomatic_metabundles` WRITE;
/*!40000 ALTER TABLE `craft_seomatic_metabundles` DISABLE KEYS */;

INSERT INTO `craft_seomatic_metabundles` (`id`, `dateCreated`, `dateUpdated`, `uid`, `bundleVersion`, `sourceBundleType`, `sourceId`, `sourceName`, `sourceHandle`, `sourceType`, `sourceTemplate`, `sourceSiteId`, `sourceAltSiteSettings`, `sourceDateUpdated`, `metaGlobalVars`, `metaSiteVars`, `metaSitemapVars`, `metaContainers`, `redirectsContainer`, `frontendTemplatesContainer`, `metaBundleSettings`)
VALUES
	(1,'2019-01-30 21:13:52','2019-11-07 16:39:57','60e15552-27dc-4dc6-862f-482000796af5','1.0.46','__GLOBAL_BUNDLE__',1,'__GLOBAL_BUNDLE__','__GLOBAL_BUNDLE__','__GLOBAL_BUNDLE__','',1,'[]','2019-11-07 16:39:57','{\"language\":null,\"mainEntityOfPage\":\"WebSite\",\"seoTitle\":\"\",\"siteNamePosition\":\"before\",\"seoDescription\":\"\",\"seoKeywords\":\"\",\"seoImage\":\"\",\"seoImageWidth\":\"\",\"seoImageHeight\":\"\",\"seoImageDescription\":\"\",\"canonicalUrl\":\"{seomatic.helper.safeCanonicalUrl()}\",\"robots\":\"all\",\"ogType\":\"website\",\"ogTitle\":\"{seomatic.meta.seoTitle}\",\"ogSiteNamePosition\":\"none\",\"ogDescription\":\"{seomatic.meta.seoDescription}\",\"ogImage\":\"{seomatic.meta.seoImage}\",\"ogImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"ogImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"ogImageDescription\":\"{seomatic.meta.seoImageDescription}\",\"twitterCard\":\"summary\",\"twitterCreator\":\"{seomatic.site.twitterHandle}\",\"twitterTitle\":\"{seomatic.meta.seoTitle}\",\"twitterSiteNamePosition\":\"none\",\"twitterDescription\":\"{seomatic.meta.seoDescription}\",\"twitterImage\":\"{seomatic.meta.seoImage}\",\"twitterImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"twitterImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"twitterImageDescription\":\"{seomatic.meta.seoImageDescription}\"}','{\"siteName\":\"Craft CMS\",\"identity\":{\"siteType\":\"Organization\",\"siteSubType\":\"LocalBusiness\",\"siteSpecificType\":\"none\",\"computedType\":\"LocalBusiness\",\"genericName\":\"\",\"genericAlternateName\":\"\",\"genericDescription\":\"\",\"genericUrl\":\"\",\"genericImage\":\"\",\"genericImageWidth\":\"\",\"genericImageHeight\":\"\",\"genericImageIds\":\"\",\"genericTelephone\":\"\",\"genericEmail\":\"\",\"genericStreetAddress\":\"\",\"genericAddressLocality\":\"\",\"genericAddressRegion\":\"\",\"genericPostalCode\":\"\",\"genericAddressCountry\":\"\",\"genericGeoLatitude\":\"\",\"genericGeoLongitude\":\"\",\"personGender\":\"Male\",\"personBirthPlace\":\"\",\"organizationDuns\":\"\",\"organizationFounder\":\"\",\"organizationFoundingDate\":\"\",\"organizationFoundingLocation\":\"\",\"organizationContactPoints\":\"\",\"corporationTickerSymbol\":\"\",\"localBusinessPriceRange\":\"$\",\"localBusinessOpeningHours\":[{\"open\":null,\"close\":null},{\"open\":null,\"close\":null},{\"open\":null,\"close\":null},{\"open\":null,\"close\":null},{\"open\":null,\"close\":null},{\"open\":null,\"close\":null},{\"open\":null,\"close\":null}],\"restaurantServesCuisine\":\"\",\"restaurantMenuUrl\":\"\",\"restaurantReservationsUrl\":\"\"},\"creator\":{\"siteType\":\"Organization\",\"siteSubType\":\"LocalBusiness\",\"siteSpecificType\":\"\",\"computedType\":\"Organization\",\"genericName\":\"\",\"genericAlternateName\":\"\",\"genericDescription\":\"\",\"genericUrl\":\"\",\"genericImage\":\"\",\"genericImageWidth\":\"\",\"genericImageHeight\":\"\",\"genericImageIds\":[],\"genericTelephone\":\"\",\"genericEmail\":\"\",\"genericStreetAddress\":\"\",\"genericAddressLocality\":\"\",\"genericAddressRegion\":\"\",\"genericPostalCode\":\"\",\"genericAddressCountry\":\"\",\"genericGeoLatitude\":\"\",\"genericGeoLongitude\":\"\",\"personGender\":\"\",\"personBirthPlace\":\"\",\"organizationDuns\":\"\",\"organizationFounder\":\"\",\"organizationFoundingDate\":\"\",\"organizationFoundingLocation\":\"\",\"organizationContactPoints\":[],\"corporationTickerSymbol\":\"\",\"localBusinessPriceRange\":\"\",\"localBusinessOpeningHours\":[],\"restaurantServesCuisine\":\"\",\"restaurantMenuUrl\":\"\",\"restaurantReservationsUrl\":\"\"},\"twitterHandle\":\"\",\"facebookProfileId\":\"\",\"facebookAppId\":\"\",\"googleSiteVerification\":\"\",\"bingSiteVerification\":\"\",\"pinterestSiteVerification\":\"\",\"sameAsLinks\":{\"twitter\":{\"siteName\":\"Twitter\",\"handle\":\"twitter\",\"url\":\"\"},\"facebook\":{\"siteName\":\"Facebook\",\"handle\":\"facebook\",\"url\":\"\"},\"wikipedia\":{\"siteName\":\"Wikipedia\",\"handle\":\"wikipedia\",\"url\":\"\"},\"linkedin\":{\"siteName\":\"LinkedIn\",\"handle\":\"linkedin\",\"url\":\"\"},\"googleplus\":{\"siteName\":\"Google+\",\"handle\":\"googleplus\",\"url\":\"\"},\"youtube\":{\"siteName\":\"YouTube\",\"handle\":\"youtube\",\"url\":\"\"},\"instagram\":{\"siteName\":\"Instagram\",\"handle\":\"instagram\",\"url\":\"\"},\"pinterest\":{\"siteName\":\"Pinterest\",\"handle\":\"pinterest\",\"url\":\"\"},\"github\":{\"siteName\":\"GitHub\",\"handle\":\"github\",\"url\":\"\"},\"vimeo\":{\"siteName\":\"Vimeo\",\"handle\":\"vimeo\",\"url\":\"\"}},\"siteLinksSearchTarget\":\"\",\"siteLinksQueryInput\":\"\",\"additionalSitemapUrls\":[],\"additionalSitemapUrlsDateUpdated\":null,\"additionalSitemaps\":[]}','{\"sitemapUrls\":true,\"sitemapAssets\":true,\"sitemapFiles\":true,\"sitemapAltLinks\":true,\"sitemapChangeFreq\":\"weekly\",\"sitemapPriority\":0.5,\"sitemapLimit\":null,\"structureDepth\":null,\"sitemapImageFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"caption\",\"field\":\"\"},{\"property\":\"geo_location\",\"field\":\"\"},{\"property\":\"license\",\"field\":\"\"}],\"sitemapVideoFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"description\",\"field\":\"\"},{\"property\":\"thumbnailLoc\",\"field\":\"\"},{\"property\":\"duration\",\"field\":\"\"},{\"property\":\"category\",\"field\":\"\"}]}','{\"MetaTagContainergeneral\":{\"data\":{\"generator\":{\"charset\":\"\",\"content\":\"SEOmatic\",\"httpEquiv\":\"\",\"name\":\"generator\",\"property\":null,\"include\":true,\"key\":\"generator\",\"environment\":null,\"dependencies\":{\"config\":[\"generatorEnabled\"]}},\"keywords\":{\"charset\":\"\",\"content\":\"{seomatic.meta.seoKeywords}\",\"httpEquiv\":\"\",\"name\":\"keywords\",\"property\":null,\"include\":true,\"key\":\"keywords\",\"environment\":null,\"dependencies\":null},\"description\":{\"charset\":\"\",\"content\":\"{seomatic.meta.seoDescription}\",\"httpEquiv\":\"\",\"name\":\"description\",\"property\":null,\"include\":true,\"key\":\"description\",\"environment\":null,\"dependencies\":null},\"referrer\":{\"charset\":\"\",\"content\":\"no-referrer-when-downgrade\",\"httpEquiv\":\"\",\"name\":\"referrer\",\"property\":null,\"include\":true,\"key\":\"referrer\",\"environment\":null,\"dependencies\":null},\"robots\":{\"charset\":\"\",\"content\":\"none\",\"httpEquiv\":\"\",\"name\":\"robots\",\"property\":null,\"include\":true,\"key\":\"robots\",\"environment\":{\"live\":{\"content\":\"{seomatic.meta.robots}\"},\"staging\":{\"content\":\"none\"},\"local\":{\"content\":\"none\"}},\"dependencies\":null}},\"name\":\"General\",\"description\":\"General Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContaineropengraph\":{\"data\":{\"fb:profile_id\":{\"charset\":\"\",\"content\":\"{seomatic.site.facebookProfileId}\",\"httpEquiv\":\"\",\"name\":\"\",\"property\":\"fb:profile_id\",\"include\":true,\"key\":\"fb:profile_id\",\"environment\":null,\"dependencies\":null},\"fb:app_id\":{\"charset\":\"\",\"content\":\"{seomatic.site.facebookAppId}\",\"httpEquiv\":\"\",\"name\":\"\",\"property\":\"fb:app_id\",\"include\":true,\"key\":\"fb:app_id\",\"environment\":null,\"dependencies\":null},\"og:locale\":{\"charset\":\"\",\"content\":\"{{ craft.app.language |replace({\\\"-\\\": \\\"_\\\"}) }}\",\"httpEquiv\":\"\",\"name\":\"\",\"property\":\"og:locale\",\"include\":true,\"key\":\"og:locale\",\"environment\":null,\"dependencies\":null},\"og:locale:alternate\":{\"charset\":\"\",\"content\":\"\",\"httpEquiv\":\"\",\"name\":\"\",\"property\":\"og:locale:alternate\",\"include\":true,\"key\":\"og:locale:alternate\",\"environment\":null,\"dependencies\":null},\"og:site_name\":{\"charset\":\"\",\"content\":\"{seomatic.site.siteName}\",\"httpEquiv\":\"\",\"name\":\"\",\"property\":\"og:site_name\",\"include\":true,\"key\":\"og:site_name\",\"environment\":null,\"dependencies\":null},\"og:type\":{\"charset\":\"\",\"content\":\"{seomatic.meta.ogType}\",\"httpEquiv\":\"\",\"name\":\"\",\"property\":\"og:type\",\"include\":true,\"key\":\"og:type\",\"environment\":null,\"dependencies\":null},\"og:url\":{\"charset\":\"\",\"content\":\"{seomatic.meta.canonicalUrl}\",\"httpEquiv\":\"\",\"name\":\"\",\"property\":\"og:url\",\"include\":true,\"key\":\"og:url\",\"environment\":null,\"dependencies\":null},\"og:title\":{\"siteName\":\"{seomatic.site.siteName}\",\"siteNamePosition\":\"{seomatic.meta.ogSiteNamePosition}\",\"separatorChar\":\"{seomatic.config.separatorChar}\",\"charset\":\"\",\"content\":\"{seomatic.meta.ogTitle}\",\"httpEquiv\":\"\",\"name\":\"\",\"property\":\"og:title\",\"include\":true,\"key\":\"og:title\",\"environment\":null,\"dependencies\":null},\"og:description\":{\"charset\":\"\",\"content\":\"{seomatic.meta.ogDescription}\",\"httpEquiv\":\"\",\"name\":\"\",\"property\":\"og:description\",\"include\":true,\"key\":\"og:description\",\"environment\":null,\"dependencies\":null},\"og:image\":{\"charset\":\"\",\"content\":\"{seomatic.meta.ogImage}\",\"httpEquiv\":\"\",\"name\":\"\",\"property\":\"og:image\",\"include\":true,\"key\":\"og:image\",\"environment\":null,\"dependencies\":null},\"og:image:width\":{\"charset\":\"\",\"content\":\"{seomatic.meta.ogImageWidth}\",\"httpEquiv\":\"\",\"name\":\"\",\"property\":\"og:image:width\",\"include\":true,\"key\":\"og:image:width\",\"environment\":null,\"dependencies\":{\"tag\":[\"og:image\"]}},\"og:image:height\":{\"charset\":\"\",\"content\":\"{seomatic.meta.ogImageHeight}\",\"httpEquiv\":\"\",\"name\":\"\",\"property\":\"og:image:height\",\"include\":true,\"key\":\"og:image:height\",\"environment\":null,\"dependencies\":{\"tag\":[\"og:image\"]}},\"og:image:alt\":{\"charset\":\"\",\"content\":\"{seomatic.meta.ogImageDescription}\",\"httpEquiv\":\"\",\"name\":\"\",\"property\":\"og:image:alt\",\"include\":true,\"key\":\"og:image:alt\",\"environment\":null,\"dependencies\":{\"tag\":[\"og:image\"]}},\"og:see_also\":{\"charset\":\"\",\"content\":\"\",\"httpEquiv\":\"\",\"name\":\"\",\"property\":\"og:see_also\",\"include\":true,\"key\":\"og:see_also\",\"environment\":null,\"dependencies\":null}},\"name\":\"Facebook\",\"description\":\"Facebook OpenGraph Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"opengraph\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContainertwitter\":{\"data\":{\"twitter:card\":{\"charset\":\"\",\"content\":\"{seomatic.meta.twitterCard}\",\"httpEquiv\":\"\",\"name\":\"twitter:card\",\"property\":null,\"include\":true,\"key\":\"twitter:card\",\"environment\":null,\"dependencies\":null},\"twitter:site\":{\"charset\":\"\",\"content\":\"@{seomatic.site.twitterHandle}\",\"httpEquiv\":\"\",\"name\":\"twitter:site\",\"property\":null,\"include\":true,\"key\":\"twitter:site\",\"environment\":null,\"dependencies\":{\"site\":[\"twitterHandle\"]}},\"twitter:creator\":{\"charset\":\"\",\"content\":\"@{seomatic.meta.twitterCreator}\",\"httpEquiv\":\"\",\"name\":\"twitter:creator\",\"property\":null,\"include\":true,\"key\":\"twitter:creator\",\"environment\":null,\"dependencies\":{\"meta\":[\"twitterCreator\"]}},\"twitter:title\":{\"siteName\":\"{seomatic.site.siteName}\",\"siteNamePosition\":\"{seomatic.meta.twitterSiteNamePosition}\",\"separatorChar\":\"{seomatic.config.separatorChar}\",\"charset\":\"\",\"content\":\"{seomatic.meta.twitterTitle}\",\"httpEquiv\":\"\",\"name\":\"twitter:title\",\"property\":null,\"include\":true,\"key\":\"twitter:title\",\"environment\":null,\"dependencies\":null},\"twitter:description\":{\"charset\":\"\",\"content\":\"{seomatic.meta.twitterDescription}\",\"httpEquiv\":\"\",\"name\":\"twitter:description\",\"property\":null,\"include\":true,\"key\":\"twitter:description\",\"environment\":null,\"dependencies\":null},\"twitter:image\":{\"charset\":\"\",\"content\":\"{seomatic.meta.twitterImage}\",\"httpEquiv\":\"\",\"name\":\"twitter:image\",\"property\":null,\"include\":true,\"key\":\"twitter:image\",\"environment\":null,\"dependencies\":null},\"twitter:image:width\":{\"charset\":\"\",\"content\":\"{seomatic.meta.twitterImageWidth}\",\"httpEquiv\":\"\",\"name\":\"twitter:image:width\",\"property\":null,\"include\":true,\"key\":\"twitter:image:width\",\"environment\":null,\"dependencies\":{\"tag\":[\"twitter:image\"]}},\"twitter:image:height\":{\"charset\":\"\",\"content\":\"{seomatic.meta.twitterImageHeight}\",\"httpEquiv\":\"\",\"name\":\"twitter:image:height\",\"property\":null,\"include\":true,\"key\":\"twitter:image:height\",\"environment\":null,\"dependencies\":{\"tag\":[\"twitter:image\"]}},\"twitter:image:alt\":{\"charset\":\"\",\"content\":\"{seomatic.meta.twitterImageDescription}\",\"httpEquiv\":\"\",\"name\":\"twitter:image:alt\",\"property\":null,\"include\":true,\"key\":\"twitter:image:alt\",\"environment\":null,\"dependencies\":{\"tag\":[\"twitter:image\"]}}},\"name\":\"Twitter\",\"description\":\"Twitter Card Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"twitter\",\"include\":true,\"dependencies\":{\"site\":[\"twitterHandle\"]},\"clearCache\":false},\"MetaTagContainermiscellaneous\":{\"data\":{\"google-site-verification\":{\"charset\":\"\",\"content\":\"{seomatic.site.googleSiteVerification}\",\"httpEquiv\":\"\",\"name\":\"google-site-verification\",\"property\":null,\"include\":true,\"key\":\"google-site-verification\",\"environment\":null,\"dependencies\":{\"site\":[\"googleSiteVerification\"]}},\"bing-site-verification\":{\"charset\":\"\",\"content\":\"{seomatic.site.bingSiteVerification}\",\"httpEquiv\":\"\",\"name\":\"msvalidate.01\",\"property\":null,\"include\":true,\"key\":\"bing-site-verification\",\"environment\":null,\"dependencies\":{\"site\":[\"bingSiteVerification\"]}},\"pinterest-site-verification\":{\"charset\":\"\",\"content\":\"{seomatic.site.pinterestSiteVerification}\",\"httpEquiv\":\"\",\"name\":\"p:domain_verify\",\"property\":null,\"include\":true,\"key\":\"pinterest-site-verification\",\"environment\":null,\"dependencies\":{\"site\":[\"pinterestSiteVerification\"]}}},\"name\":\"Miscellaneous\",\"description\":\"Miscellaneous Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"miscellaneous\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaLinkContainergeneral\":{\"data\":{\"canonical\":{\"crossorigin\":\"\",\"href\":\"{seomatic.meta.canonicalUrl}\",\"hreflang\":\"\",\"media\":\"\",\"rel\":\"canonical\",\"sizes\":\"\",\"type\":\"\",\"include\":true,\"key\":\"canonical\",\"environment\":null,\"dependencies\":null},\"home\":{\"crossorigin\":\"\",\"href\":\"{{ siteUrl }}\",\"hreflang\":\"\",\"media\":\"\",\"rel\":\"home\",\"sizes\":\"\",\"type\":\"\",\"include\":true,\"key\":\"home\",\"environment\":null,\"dependencies\":null},\"author\":{\"crossorigin\":\"\",\"href\":\"{{ url(\\\"/humans.txt\\\") }}\",\"hreflang\":\"\",\"media\":\"\",\"rel\":\"author\",\"sizes\":\"\",\"type\":\"text/plain\",\"include\":true,\"key\":\"author\",\"environment\":null,\"dependencies\":{\"frontend_template\":[\"humans\"]}},\"publisher\":{\"crossorigin\":\"\",\"href\":\"{seomatic.site.googlePublisherLink}\",\"hreflang\":\"\",\"media\":\"\",\"rel\":\"publisher\",\"sizes\":\"\",\"type\":\"\",\"include\":true,\"key\":\"publisher\",\"environment\":null,\"dependencies\":{\"site\":[\"googlePublisherLink\"]}}},\"name\":\"General\",\"description\":\"Link Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaLinkContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaScriptContainergeneral\":{\"data\":{\"googleAnalytics\":{\"name\":\"Google Analytics\",\"description\":\"Google Analytics gives you the digital analytics tools you need to analyze data from all touchpoints in one place, for a deeper understanding of the customer experience. You can then share the insights that matter with your whole organization. [Learn More](https://www.google.com/analytics/analytics/)\",\"templatePath\":\"_frontend/scripts/googleAnalytics.twig\",\"templateString\":\"{% if trackingId.value is defined and trackingId.value %}\\n(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\\n(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\\nm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\\n})(window,document,\'script\',\'{{ analyticsUrl.value }}\',\'ga\');\\nga(\'create\', \'{{ trackingId.value |raw }}\', \'auto\'{% if linker.value %}, {allowLinker: true}{% endif %});\\n{% if ipAnonymization.value %}\\nga(\'set\', \'anonymizeIp\', true);\\n{% endif %}\\n{% if displayFeatures.value %}\\nga(\'require\', \'displayfeatures\');\\n{% endif %}\\n{% if ecommerce.value %}\\nga(\'require\', \'ecommerce\');\\n{% endif %}\\n{% if enhancedEcommerce.value %}\\nga(\'require\', \'ec\');\\n{% endif %}\\n{% if enhancedLinkAttribution.value %}\\nga(\'require\', \'linkid\');\\n{% endif %}\\n{% if enhancedLinkAttribution.value %}\\nga(\'require\', \'linker\');\\n{% endif %}\\n{% set pageView = (sendPageView.value and not craft.app.request.isLivePreview) %}\\n{% if pageView %}\\nga(\'send\', \'pageview\');\\n{% endif %}\\n{% endif %}\\n\",\"position\":1,\"bodyTemplatePath\":null,\"bodyTemplateString\":null,\"bodyPosition\":2,\"vars\":{\"trackingId\":{\"title\":\"Google Analytics Tracking ID\",\"instructions\":\"Only enter the ID, e.g.: `UA-XXXXXX-XX`, not the entire script code. [Learn More](https://support.google.com/analytics/answer/1032385?hl=e)\",\"type\":\"string\",\"value\":\"\"},\"sendPageView\":{\"title\":\"Automatically send Google Analytics PageView\",\"instructions\":\"Controls whether the Google Analytics script automatically sends a PageView to Google Analytics when your pages are loaded.\",\"type\":\"bool\",\"value\":true},\"ipAnonymization\":{\"title\":\"Google Analytics IP Anonymization\",\"instructions\":\"When a customer of Analytics requests IP address anonymization, Analytics anonymizes the address as soon as technically feasible at the earliest possible stage of the collection network.\",\"type\":\"bool\",\"value\":false},\"displayFeatures\":{\"title\":\"Display Features\",\"instructions\":\"The display features plugin for analytics.js can be used to enable Advertising Features in Google Analytics, such as Remarketing, Demographics and Interest Reporting, and more. [Learn More](https://developers.google.com/analytics/devguides/collection/analyticsjs/display-features)\",\"type\":\"bool\",\"value\":false},\"ecommerce\":{\"title\":\"Ecommerce\",\"instructions\":\"Ecommerce tracking allows you to measure the number of transactions and revenue that your website generates. [Learn More](https://developers.google.com/analytics/devguides/collection/analyticsjs/ecommerce)\",\"type\":\"bool\",\"value\":false},\"enhancedEcommerce\":{\"title\":\"Enhanced Ecommerce\",\"instructions\":\"The enhanced ecommerce plug-in for analytics.js enables the measurement of user interactions with products on ecommerce websites across the user\'s shopping experience, including: product impressions, product clicks, viewing product details, adding a product to a shopping cart, initiating the checkout process, transactions, and refunds. [Learn More](https://developers.google.com/analytics/devguides/collection/analyticsjs/enhanced-ecommerce)\",\"type\":\"bool\",\"value\":false},\"enhancedLinkAttribution\":{\"title\":\"Enhanced Link Attribution\",\"instructions\":\"Enhanced Link Attribution improves the accuracy of your In-Page Analytics report by automatically differentiating between multiple links to the same URL on a single page by using link element IDs. [Learn More](https://developers.google.com/analytics/devguides/collection/analyticsjs/enhanced-link-attribution)\",\"type\":\"bool\",\"value\":false},\"linker\":{\"title\":\"Linker\",\"instructions\":\"The linker plugin simplifies the process of implementing cross-domain tracking as described in the Cross-domain Tracking guide for analytics.js. [Learn More](https://developers.google.com/analytics/devguides/collection/analyticsjs/linker)\",\"type\":\"bool\",\"value\":false},\"analyticsUrl\":{\"title\":\"Google Analytics Script URL\",\"instructions\":\"The URL to the Google Analytics tracking script. Normally this should not be changed, unless you locally cache it.\",\"type\":\"string\",\"value\":\"https://www.google-analytics.com/analytics.js\"}},\"dataLayer\":[],\"include\":false,\"key\":\"googleAnalytics\",\"environment\":{\"staging\":{\"include\":false},\"local\":{\"include\":false}},\"dependencies\":null},\"gtag\":{\"name\":\"Google gtag.js\",\"description\":\"The global site tag (gtag.js) is a JavaScript tagging framework and API that allows you to send event data to AdWords, DoubleClick, and Google Analytics. Instead of having to manage multiple tags for different products, you can use gtag.js and more easily benefit from the latest tracking features and integrations as they become available. [Learn More](https://developers.google.com/gtagjs/)\",\"templatePath\":\"_frontend/scripts/gtagHead.twig\",\"templateString\":\"{% set gtagProperty = googleAnalyticsId.value ?? googleAdWordsId.value ?? dcFloodlightId.value ?? null %}\\n{% if gtagProperty %}\\nwindow.dataLayer = window.dataLayer || [{% if dataLayer is defined and dataLayer %}{{ dataLayer |json_encode() |raw }}{% endif %}];\\nfunction gtag(){dataLayer.push(arguments)};\\ngtag(\'js\', new Date());\\n{% set pageView = (sendPageView.value and not craft.app.request.isLivePreview) %}\\n{% if googleAnalyticsId.value %}\\n{%- set gtagConfig = \\\"{\\\"\\n    ~ \\\"\'send_page_view\': #{pageView ? \'true\' : \'false\'},\\\"\\n    ~ \\\"\'anonymize_ip\': #{ipAnonymization.value ? \'true\' : \'false\'},\\\"\\n    ~ \\\"\'link_attribution\': #{enhancedLinkAttribution.value ? \'true\' : \'false\'},\\\"\\n    ~ \\\"\'allow_display_features\': #{displayFeatures.value ? \'true\' : \'false\'}\\\"\\n    ~ \\\"}\\\"\\n-%}\\ngtag(\'config\', \'{{ googleAnalyticsId.value }}\', {{ gtagConfig }});\\n{% endif %}\\n{% if googleAdWordsId.value %}\\n{%- set gtagConfig = \\\"{\\\"\\n    ~ \\\"\'send_page_view\': #{pageView ? \'true\' : \'false\'}\\\"\\n    ~ \\\"}\\\"\\n-%}\\ngtag(\'config\', \'{{ googleAdWordsId.value }}\', {{ gtagConfig }});\\n{% endif %}\\n{% if dcFloodlightId.value %}\\n{%- set gtagConfig = \\\"{\\\"\\n    ~ \\\"\'send_page_view\': #{pageView ? \'true\' : \'false\'}\\\"\\n    ~ \\\"}\\\"\\n-%}\\ngtag(\'config\', \'{{ dcFloodlightId.value }}\', {{ gtagConfig }});\\n{% endif %}\\n{% endif %}\\n\",\"position\":1,\"bodyTemplatePath\":\"_frontend/scripts/gtagBody.twig\",\"bodyTemplateString\":\"{% set gtagProperty = googleAnalyticsId.value ?? googleAdWordsId.value ?? dcFloodlightId.value ?? null %}\\n{% if gtagProperty %}\\n<script async src=\\\"{{ gtagScriptUrl.value }}?id={{ gtagProperty }}\\\"></script>\\n{% endif %}\\n\",\"bodyPosition\":2,\"vars\":{\"googleAnalyticsId\":{\"title\":\"Google Analytics Tracking ID\",\"instructions\":\"Only enter the ID, e.g.: `UA-XXXXXX-XX`, not the entire script code. [Learn More](https://support.google.com/analytics/answer/1032385?hl=e)\",\"type\":\"string\",\"value\":\"\"},\"googleAdWordsId\":{\"title\":\"AdWords Conversion ID\",\"instructions\":\"Only enter the ID, e.g.: `AW-XXXXXXXX`, not the entire script code. [Learn More](https://developers.google.com/adwords-remarketing-tag/)\",\"type\":\"string\",\"value\":\"\"},\"dcFloodlightId\":{\"title\":\"DoubleClick Floodlight ID\",\"instructions\":\"Only enter the ID, e.g.: `DC-XXXXXXXX`, not the entire script code. [Learn More](https://support.google.com/dcm/partner/answer/7568534)\",\"type\":\"string\",\"value\":\"\"},\"sendPageView\":{\"title\":\"Automatically send PageView\",\"instructions\":\"Controls whether the `gtag.js` script automatically sends a PageView to Google Analytics, AdWords, and DoubleClick Floodlight when your pages are loaded.\",\"type\":\"bool\",\"value\":true},\"ipAnonymization\":{\"title\":\"Google Analytics IP Anonymization\",\"instructions\":\"In some cases, you might need to anonymize the IP addresses of hits sent to Google Analytics. [Learn More](https://developers.google.com/analytics/devguides/collection/gtagjs/ip-anonymization)\",\"type\":\"bool\",\"value\":false},\"displayFeatures\":{\"title\":\"Google Analytics Display Features\",\"instructions\":\"The display features plugin for gtag.js can be used to enable Advertising Features in Google Analytics, such as Remarketing, Demographics and Interest Reporting, and more. [Learn More](https://developers.google.com/analytics/devguides/collection/gtagjs/display-features)\",\"type\":\"bool\",\"value\":false},\"enhancedLinkAttribution\":{\"title\":\"Google Analytics Enhanced Link Attribution\",\"instructions\":\"Enhanced link attribution improves click track reporting by automatically differentiating between multiple link clicks that have the same URL on a given page. [Learn More](https://developers.google.com/analytics/devguides/collection/gtagjs/enhanced-link-attribution)\",\"type\":\"bool\",\"value\":false},\"gtagScriptUrl\":{\"title\":\"Google gtag.js Script URL\",\"instructions\":\"The URL to the Google gtag.js tracking script. Normally this should not be changed, unless you locally cache it. The JavaScript `dataLayer` will automatically be set to the `dataLayer` Twig template variable.\",\"type\":\"string\",\"value\":\"https://www.googletagmanager.com/gtag/js\"}},\"dataLayer\":[],\"include\":false,\"key\":\"gtag\",\"environment\":{\"staging\":{\"include\":false},\"local\":{\"include\":false}},\"dependencies\":null},\"googleTagManager\":{\"name\":\"Google Tag Manager\",\"description\":\"Google Tag Manager is a tag management system that allows you to quickly and easily update tags and code snippets on your website. Once the Tag Manager snippet has been added to your website or mobile app, you can configure tags via a web-based user interface without having to alter and deploy additional code. [Learn More](https://support.google.com/tagmanager/answer/6102821?hl=en)\",\"templatePath\":\"_frontend/scripts/googleTagManagerHead.twig\",\"templateString\":\"{% if googleTagManagerId.value is defined and googleTagManagerId.value %}\\n{{ dataLayerVariableName.value }} = [{% if dataLayer is defined and dataLayer %}{{ dataLayer |json_encode() |raw }}{% endif %}];\\n(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':\\nnew Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],\\nj=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=\\n\'{{ googleTagManagerUrl.value }}?id=\'+i+dl;f.parentNode.insertBefore(j,f);\\n})(window,document,\'script\',\'{{ dataLayerVariableName.value }}\',\'{{ googleTagManagerId.value }}\');\\n{% endif %}\\n\",\"position\":1,\"bodyTemplatePath\":\"_frontend/scripts/googleTagManagerBody.twig\",\"bodyTemplateString\":\"{% if googleTagManagerId.value is defined and googleTagManagerId.value %}\\n<noscript><iframe src=\\\"{{ googleTagManagerNoScriptUrl.value }}?id={{ googleTagManagerId.value }}\\\"\\nheight=\\\"0\\\" width=\\\"0\\\" style=\\\"display:none;visibility:hidden\\\"></iframe></noscript>\\n{% endif %}\\n\",\"bodyPosition\":2,\"vars\":{\"googleTagManagerId\":{\"title\":\"Google Tag Manager ID\",\"instructions\":\"Only enter the ID, e.g.: `GTM-XXXXXX`, not the entire script code. [Learn More](https://developers.google.com/tag-manager/quickstart)\",\"type\":\"string\",\"value\":\"\"},\"dataLayerVariableName\":{\"title\":\"DataLayer Variable Name\",\"instructions\":\"The name to use for the JavaScript DataLayer variable. The value of this variable will be set to the `dataLayer` Twig template variable.\",\"type\":\"string\",\"value\":\"dl\"},\"googleTagManagerUrl\":{\"title\":\"Google Tag Manager Script URL\",\"instructions\":\"The URL to the Google Tag Manager script. Normally this should not be changed, unless you locally cache it.\",\"type\":\"string\",\"value\":\"https://www.googletagmanager.com/gtm.js\"},\"googleTagManagerNoScriptUrl\":{\"title\":\"Google Tag Manager Script &lt;noscript&gt; URL\",\"instructions\":\"The URL to the Google Tag Manager `&lt;noscript&gt;`. Normally this should not be changed, unless you locally cache it.\",\"type\":\"string\",\"value\":\"https://www.googletagmanager.com/ns.html\"}},\"dataLayer\":[],\"include\":false,\"key\":\"googleTagManager\",\"environment\":{\"staging\":{\"include\":false},\"local\":{\"include\":false}},\"dependencies\":null},\"facebookPixel\":{\"name\":\"Facebook Pixel\",\"description\":\"The Facebook pixel is an analytics tool that helps you measure the effectiveness of your advertising. You can use the Facebook pixel to understand the actions people are taking on your website and reach audiences you care about. [Learn More](https://www.facebook.com/business/help/651294705016616)\",\"templatePath\":\"_frontend/scripts/facebookPixelHead.twig\",\"templateString\":\"{% if facebookPixelId.value is defined and facebookPixelId.value %}\\n!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?\\nn.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;\\nn.push=n;n.loaded=!0;n.version=\'2.0\';n.queue=[];t=b.createElement(e);t.async=!0;\\nt.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,\\ndocument,\'script\',\'{{ facebookPixelUrl.value }}\');\\nfbq(\'init\', \'{{ facebookPixelId.value }}\');\\n{% set pageView = (sendPageView.value and not craft.app.request.isLivePreview) %}\\n{% if pageView %}\\nfbq(\'track\', \'PageView\');\\n{% endif %}\\n{% endif %}\\n\",\"position\":1,\"bodyTemplatePath\":\"_frontend/scripts/facebookPixelBody.twig\",\"bodyTemplateString\":\"{% if facebookPixelId.value is defined and facebookPixelId.value %}\\n<noscript><img height=\\\"1\\\" width=\\\"1\\\" style=\\\"display:none\\\"\\nsrc=\\\"{{ facebookPixelNoScriptUrl.value }}?id={{ facebookPixelId.value }}&ev=PageView&noscript=1\\\" /></noscript>\\n{% endif %}\\n\",\"bodyPosition\":2,\"vars\":{\"facebookPixelId\":{\"title\":\"Facebook Pixel ID\",\"instructions\":\"Only enter the ID, e.g.: `XXXXXXXXXX`, not the entire script code. [Learn More](https://developers.facebook.com/docs/facebook-pixel/api-reference)\",\"type\":\"string\",\"value\":\"\"},\"sendPageView\":{\"title\":\"Automatically send Facebook Pixel PageView\",\"instructions\":\"Controls whether the Facebook Pixel script automatically sends a PageView to Facebook Analytics when your pages are loaded.\",\"type\":\"bool\",\"value\":true},\"facebookPixelUrl\":{\"title\":\"Facebook Pixel Script URL\",\"instructions\":\"The URL to the Facebook Pixel script. Normally this should not be changed, unless you locally cache it.\",\"type\":\"string\",\"value\":\"https://connect.facebook.net/en_US/fbevents.js\"},\"facebookPixelNoScriptUrl\":{\"title\":\"Facebook Pixel Script &lt;noscript&gt; URL\",\"instructions\":\"The URL to the Facebook Pixel `&lt;noscript&gt;`. Normally this should not be changed, unless you locally cache it.\",\"type\":\"string\",\"value\":\"https://www.facebook.com/tr\"}},\"dataLayer\":[],\"include\":false,\"key\":\"facebookPixel\",\"environment\":{\"staging\":{\"include\":false},\"local\":{\"include\":false}},\"dependencies\":null},\"linkedInInsight\":{\"name\":\"LinkedIn Insight\",\"description\":\"The LinkedIn Insight Tag is a lightweight JavaScript tag that powers conversion tracking, retargeting, and web analytics for LinkedIn ad campaigns.\",\"templatePath\":\"_frontend/scripts/linkedInInsightHead.twig\",\"templateString\":\"{% if dataPartnerId.value is defined and dataPartnerId.value %}\\n_linkedin_data_partner_id = \\\"{{ dataPartnerId.value }}\\\";\\n{% endif %}\\n\",\"position\":1,\"bodyTemplatePath\":\"_frontend/scripts/linkedInInsightBody.twig\",\"bodyTemplateString\":\"{% if dataPartnerId.value is defined and dataPartnerId.value %}\\n<script type=\\\"text/javascript\\\">\\n(function(){var s = document.getElementsByTagName(\\\"script\\\")[0];\\n    var b = document.createElement(\\\"script\\\");\\n    b.type = \\\"text/javascript\\\";b.async = true;\\n    b.src = \\\"{{ linkedInInsightUrl.value }}\\\";\\n    s.parentNode.insertBefore(b, s);})();\\n</script>\\n<noscript>\\n<img height=\\\"1\\\" width=\\\"1\\\" style=\\\"display:none;\\\" alt=\\\"\\\" src=\\\"{{ linkedInInsightNoScriptUrl.value }}?pid={{ dataPartnerId.value }}&fmt=gif\\\" />\\n</noscript>\\n{% endif %}\\n\",\"bodyPosition\":3,\"vars\":{\"dataPartnerId\":{\"title\":\"LinkedIn Data Partner ID\",\"instructions\":\"Only enter the ID, e.g.: `XXXXXXXXXX`, not the entire script code. [Learn More](https://www.linkedin.com/help/lms/answer/65513/adding-the-linkedin-insight-tag-to-your-website?lang=en)\",\"type\":\"string\",\"value\":\"\"},\"linkedInInsightUrl\":{\"title\":\"LinkedIn Insight Script URL\",\"instructions\":\"The URL to the LinkedIn Insight script. Normally this should not be changed, unless you locally cache it.\",\"type\":\"string\",\"value\":\"https://snap.licdn.com/li.lms-analytics/insight.min.js\"},\"linkedInInsightNoScriptUrl\":{\"title\":\"LinkedIn Insight &lt;noscript&gt; URL\",\"instructions\":\"The URL to the LinkedIn Insight `&lt;noscript&gt;`. Normally this should not be changed, unless you locally cache it.\",\"type\":\"string\",\"value\":\"https://dc.ads.linkedin.com/collect/\"}},\"dataLayer\":[],\"include\":false,\"key\":\"linkedInInsight\",\"environment\":{\"staging\":{\"include\":false},\"local\":{\"include\":false}},\"dependencies\":null},\"hubSpot\":{\"name\":\"HubSpot\",\"description\":\"If you\'re not hosting your entire website on HubSpot, or have pages on your website that are not hosted on HubSpot, you\'ll need to install the HubSpot tracking code on your non-HubSpot pages in order to capture those analytics.\",\"templatePath\":null,\"templateString\":null,\"position\":1,\"bodyTemplatePath\":\"_frontend/scripts/hubSpotBody.twig\",\"bodyTemplateString\":\"{% if hubSpotId.value is defined and hubSpotId.value %}\\n<script type=\\\"text/javascript\\\" id=\\\"hs-script-loader\\\" async defer src=\\\"{{ hubSpotUrl.value }}{{ hubSpotId.value }}.js\\\"></script>\\n{% endif %}\\n\",\"bodyPosition\":3,\"vars\":{\"hubSpotId\":{\"title\":\"HubSpot ID\",\"instructions\":\"Only enter the ID, e.g.: `XXXXXXXXXX`, not the entire script code. [Learn More](https://knowledge.hubspot.com/articles/kcs_article/reports/install-the-hubspot-tracking-code)\",\"type\":\"string\",\"value\":\"\"},\"hubSpotUrl\":{\"title\":\"HubSpot Script URL\",\"instructions\":\"The URL to the HubSpot script. Normally this should not be changed, unless you locally cache it.\",\"type\":\"string\",\"value\":\"//js.hs-scripts.com/\"}},\"dataLayer\":[],\"include\":false,\"key\":\"hubSpot\",\"environment\":{\"staging\":{\"include\":false},\"local\":{\"include\":false}},\"dependencies\":null}},\"position\":1,\"name\":\"General\",\"description\":\"Script Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaScriptContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaJsonLdContainergeneral\":{\"data\":{\"mainEntityOfPage\":{\"issn\":null,\"about\":null,\"accessMode\":null,\"accessModeSufficient\":null,\"accessibilityAPI\":null,\"accessibilityControl\":null,\"accessibilityFeature\":null,\"accessibilityHazard\":null,\"accessibilitySummary\":null,\"accountablePerson\":null,\"aggregateRating\":null,\"alternativeHeadline\":null,\"associatedMedia\":null,\"audience\":null,\"audio\":null,\"author\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"award\":null,\"character\":null,\"citation\":null,\"comment\":null,\"commentCount\":null,\"contentLocation\":null,\"contentRating\":null,\"contentReferenceTime\":null,\"contributor\":null,\"copyrightHolder\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"copyrightYear\":null,\"correction\":null,\"creator\":{\"id\":\"{seomatic.site.creator.genericUrl}#creator\"},\"dateCreated\":null,\"dateModified\":null,\"datePublished\":null,\"discussionUrl\":null,\"editor\":null,\"educationalAlignment\":null,\"educationalUse\":null,\"encoding\":null,\"encodingFormat\":null,\"exampleOfWork\":null,\"expires\":null,\"funder\":null,\"genre\":null,\"hasPart\":null,\"headline\":null,\"inLanguage\":\"{seomatic.meta.language}\",\"interactionStatistic\":null,\"interactivityType\":null,\"isAccessibleForFree\":null,\"isBasedOn\":null,\"isFamilyFriendly\":null,\"isPartOf\":null,\"keywords\":null,\"learningResourceType\":null,\"license\":null,\"locationCreated\":null,\"mainEntity\":null,\"material\":null,\"materialExtent\":null,\"mentions\":null,\"offers\":null,\"position\":null,\"producer\":null,\"provider\":null,\"publication\":null,\"publisher\":null,\"publisherImprint\":null,\"publishingPrinciples\":null,\"recordedAt\":null,\"releasedEvent\":null,\"review\":null,\"schemaVersion\":null,\"sdDatePublished\":null,\"sdLicense\":null,\"sdPublisher\":null,\"sourceOrganization\":null,\"spatial\":null,\"spatialCoverage\":null,\"sponsor\":null,\"temporal\":null,\"temporalCoverage\":null,\"text\":null,\"thumbnailUrl\":null,\"timeRequired\":null,\"translationOfWork\":null,\"translator\":null,\"typicalAgeRange\":null,\"version\":null,\"video\":null,\"workExample\":null,\"workTranslation\":null,\"additionalType\":null,\"alternateName\":null,\"description\":\"{seomatic.meta.seoDescription}\",\"disambiguatingDescription\":null,\"identifier\":null,\"image\":{\"type\":\"ImageObject\",\"url\":\"{seomatic.meta.seoImage}\"},\"mainEntityOfPage\":\"{seomatic.meta.canonicalUrl}\",\"name\":\"{seomatic.meta.seoTitle}\",\"potentialAction\":{\"type\":\"SearchAction\",\"target\":\"{seomatic.site.siteLinksSearchTarget}\",\"query-input\":\"{seomatic.helper.siteLinksQueryInput()}\"},\"sameAs\":null,\"subjectOf\":null,\"url\":\"{seomatic.meta.canonicalUrl}\",\"context\":\"http://schema.org\",\"type\":\"{seomatic.meta.mainEntityOfPage}\",\"id\":null,\"graph\":null,\"include\":true,\"key\":\"mainEntityOfPage\",\"environment\":null,\"dependencies\":null},\"identity\":{\"currenciesAccepted\":null,\"openingHours\":[],\"paymentAccepted\":null,\"priceRange\":\"{seomatic.site.identity.localBusinessPriceRange}\",\"additionalProperty\":null,\"branchCode\":null,\"containedInPlace\":null,\"containsPlace\":null,\"geo\":{\"type\":\"GeoCoordinates\",\"latitude\":\"{seomatic.site.identity.genericGeoLatitude}\",\"longitude\":\"{seomatic.site.identity.genericGeoLongitude}\"},\"hasMap\":null,\"maximumAttendeeCapacity\":null,\"openingHoursSpecification\":null,\"photo\":null,\"publicAccess\":null,\"smokingAllowed\":null,\"specialOpeningHoursSpecification\":null,\"actionableFeedbackPolicy\":null,\"address\":{\"type\":\"PostalAddress\",\"streetAddress\":\"{seomatic.site.identity.genericStreetAddress}\",\"addressLocality\":\"{seomatic.site.identity.genericAddressLocality}\",\"addressRegion\":\"{seomatic.site.identity.genericAddressRegion}\",\"postalCode\":\"{seomatic.site.identity.genericPostalCode}\",\"addressCountry\":\"{seomatic.site.identity.genericAddressCountry}\"},\"aggregateRating\":null,\"alumni\":null,\"areaServed\":null,\"award\":null,\"brand\":null,\"contactPoint\":null,\"correctionsPolicy\":null,\"department\":null,\"dissolutionDate\":null,\"diversityPolicy\":null,\"diversityStaffingReport\":null,\"duns\":\"{seomatic.site.identity.organizationDuns}\",\"email\":\"{seomatic.site.identity.genericEmail}\",\"employee\":null,\"ethicsPolicy\":null,\"event\":null,\"faxNumber\":null,\"founder\":\"{seomatic.site.identity.organizationFounder}\",\"foundingDate\":\"{seomatic.site.identity.organizationFoundingDate}\",\"foundingLocation\":\"{seomatic.site.identity.organizationFoundingLocation}\",\"funder\":null,\"globalLocationNumber\":null,\"hasOfferCatalog\":null,\"hasPOS\":null,\"isicV4\":null,\"knowsAbout\":null,\"knowsLanguage\":null,\"legalName\":null,\"leiCode\":null,\"location\":null,\"logo\":{\"type\":\"ImageObject\",\"url\":\"{seomatic.helper.socialTransform(seomatic.site.identity.genericImageIds[0], \\\"schema-logo\\\")}\",\"width\":\"{seomatic.helper.socialTransformWidth(seomatic.site.identity.genericImageIds[0], \\\"schema-logo\\\")}\",\"height\":\"{seomatic.helper.socialTransformHeight(seomatic.site.identity.genericImageIds[0], \\\"schema-logo\\\")}\"},\"makesOffer\":null,\"member\":null,\"memberOf\":null,\"naics\":null,\"numberOfEmployees\":null,\"ownershipFundingInfo\":null,\"owns\":null,\"parentOrganization\":null,\"publishingPrinciples\":null,\"review\":null,\"seeks\":null,\"slogan\":null,\"sponsor\":null,\"subOrganization\":null,\"taxID\":null,\"telephone\":\"{seomatic.site.identity.genericTelephone}\",\"unnamedSourcesPolicy\":null,\"vatID\":null,\"additionalType\":null,\"alternateName\":\"{seomatic.site.identity.genericAlternateName}\",\"description\":\"{seomatic.site.identity.genericDescription}\",\"disambiguatingDescription\":null,\"identifier\":null,\"image\":{\"type\":\"ImageObject\",\"url\":\"{seomatic.site.identity.genericImage}\",\"width\":\"{seomatic.site.identity.genericImageWidth}\",\"height\":\"{seomatic.site.identity.genericImageHeight}\"},\"mainEntityOfPage\":null,\"name\":\"{seomatic.site.identity.genericName}\",\"potentialAction\":null,\"sameAs\":null,\"subjectOf\":null,\"url\":\"{seomatic.site.identity.genericUrl}\",\"context\":\"http://schema.org\",\"type\":\"{seomatic.site.identity.computedType}\",\"id\":\"{seomatic.site.identity.genericUrl}#identity\",\"graph\":null,\"include\":true,\"key\":\"identity\",\"environment\":null,\"dependencies\":null},\"creator\":{\"actionableFeedbackPolicy\":null,\"address\":{\"type\":\"PostalAddress\",\"streetAddress\":\"{seomatic.site.creator.genericStreetAddress}\",\"addressLocality\":\"{seomatic.site.creator.genericAddressLocality}\",\"addressRegion\":\"{seomatic.site.creator.genericAddressRegion}\",\"postalCode\":\"{seomatic.site.creator.genericPostalCode}\",\"addressCountry\":\"{seomatic.site.creator.genericAddressCountry}\"},\"aggregateRating\":null,\"alumni\":null,\"areaServed\":null,\"award\":null,\"brand\":null,\"contactPoint\":null,\"correctionsPolicy\":null,\"department\":null,\"dissolutionDate\":null,\"diversityPolicy\":null,\"diversityStaffingReport\":null,\"duns\":\"{seomatic.site.creator.organizationDuns}\",\"email\":\"{seomatic.site.creator.genericEmail}\",\"employee\":null,\"ethicsPolicy\":null,\"event\":null,\"faxNumber\":null,\"founder\":\"{seomatic.site.creator.organizationFounder}\",\"foundingDate\":\"{seomatic.site.creator.organizationFoundingDate}\",\"foundingLocation\":\"{seomatic.site.creator.organizationFoundingLocation}\",\"funder\":null,\"globalLocationNumber\":null,\"hasOfferCatalog\":null,\"hasPOS\":null,\"isicV4\":null,\"knowsAbout\":null,\"knowsLanguage\":null,\"legalName\":null,\"leiCode\":null,\"location\":null,\"logo\":{\"type\":\"ImageObject\",\"url\":\"{seomatic.helper.socialTransform(seomatic.site.creator.genericImageIds[0], \\\"schema-logo\\\")}\",\"width\":\"{seomatic.helper.socialTransformWidth(seomatic.site.creator.genericImageIds[0], \\\"schema-logo\\\")}\",\"height\":\"{seomatic.helper.socialTransformHeight(seomatic.site.creator.genericImageIds[0], \\\"schema-logo\\\")}\"},\"makesOffer\":null,\"member\":null,\"memberOf\":null,\"naics\":null,\"numberOfEmployees\":null,\"ownershipFundingInfo\":null,\"owns\":null,\"parentOrganization\":null,\"publishingPrinciples\":null,\"review\":null,\"seeks\":null,\"slogan\":null,\"sponsor\":null,\"subOrganization\":null,\"taxID\":null,\"telephone\":\"{seomatic.site.creator.genericTelephone}\",\"unnamedSourcesPolicy\":null,\"vatID\":null,\"additionalType\":null,\"alternateName\":\"{seomatic.site.creator.genericAlternateName}\",\"description\":\"{seomatic.site.creator.genericDescription}\",\"disambiguatingDescription\":null,\"identifier\":null,\"image\":{\"type\":\"ImageObject\",\"url\":\"{seomatic.site.creator.genericImage}\",\"width\":\"{seomatic.site.creator.genericImageWidth}\",\"height\":\"{seomatic.site.creator.genericImageHeight}\"},\"mainEntityOfPage\":null,\"name\":\"{seomatic.site.creator.genericName}\",\"potentialAction\":null,\"sameAs\":null,\"subjectOf\":null,\"url\":\"{seomatic.site.creator.genericUrl}\",\"context\":\"http://schema.org\",\"type\":\"{seomatic.site.creator.computedType}\",\"id\":\"{seomatic.site.creator.genericUrl}#creator\",\"graph\":null,\"include\":true,\"key\":\"creator\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"JsonLd Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaJsonLdContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTitleContainergeneral\":{\"data\":{\"title\":{\"title\":\"{seomatic.meta.seoTitle}\",\"siteName\":\"{seomatic.site.siteName}\",\"siteNamePosition\":\"{seomatic.meta.siteNamePosition}\",\"separatorChar\":\"{seomatic.config.separatorChar}\",\"include\":true,\"key\":\"title\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"Meta Title Tag\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTitleContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false}}','[]','{\"data\":{\"humans\":{\"templateVersion\":\"1.0.0\",\"templateString\":\"/* TEAM */\\n\\nCreator: {{ seomatic.site.creator.genericName ?? \\\"n/a\\\" }}\\nURL: {{ seomatic.site.creator.genericUrl ?? \\\"n/a\\\" }}\\nDescription: {{ seomatic.site.creator.genericDescription ?? \\\"n/a\\\" }}\\n\\n/* THANKS */\\n\\nCraftCMS - https://craftcms.com\\nPixel & Tonic - https://pixelandtonic.com\\n\\n/* SITE */\\n\\nStandards: HTML5, CSS3\\nComponents: Craft CMS 3, Yii2, PHP, JavaScript, SEOmatic\\n\",\"siteId\":null,\"include\":true,\"handle\":\"humans\",\"path\":\"humans.txt\",\"template\":\"_frontend/pages/humans.twig\",\"controller\":\"frontend-template\",\"action\":\"humans\"},\"robots\":{\"templateVersion\":\"1.0.0\",\"templateString\":\"# robots.txt for {{ siteUrl }}\\n\\nSitemap: {{ seomatic.helper.sitemapIndexForSiteId() }}\\n{% switch seomatic.config.environment %}\\n\\n{% case \\\"live\\\" %}\\n\\n# live - don\'t allow web crawlers to index cpresources/ or vendor/\\n\\nUser-agent: *\\nDisallow: /cpresources/\\nDisallow: /vendor/\\nDisallow: /.env\\n\\n{% case \\\"staging\\\" %}\\n\\n# staging - disallow all\\n\\nUser-agent: *\\nDisallow: /\\n\\n{% case \\\"local\\\" %}\\n\\n# local - disallow all\\n\\nUser-agent: *\\nDisallow: /\\n\\n{% default %}\\n\\n# default - don\'t allow web crawlers to index cpresources/ or vendor/\\n\\nUser-agent: *\\nDisallow: /cpresources/\\nDisallow: /vendor/\\nDisallow: /.env\\n\\n{% endswitch %}\\n\",\"siteId\":null,\"include\":true,\"handle\":\"robots\",\"path\":\"robots.txt\",\"template\":\"_frontend/pages/robots.twig\",\"controller\":\"frontend-template\",\"action\":\"robots\"},\"ads\":{\"templateVersion\":\"1.0.0\",\"templateString\":\"# ads.txt file for {{ siteUrl }}\\n# More info: https://support.google.com/admanager/answer/7441288?hl=en\\n{{ siteUrl }},123,DIRECT\\n\",\"siteId\":null,\"include\":true,\"handle\":\"ads\",\"path\":\"ads.txt\",\"template\":\"_frontend/pages/ads.twig\",\"controller\":\"frontend-template\",\"action\":\"ads\"}},\"name\":\"Frontend Templates\",\"description\":\"Templates that are rendered on the frontend\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\FrontendTemplateContainer\",\"handle\":\"SeomaticEditableTemplate\",\"include\":true,\"dependencies\":null,\"clearCache\":false}','{\"siteType\":\"CreativeWork\",\"siteSubType\":\"WebSite\",\"siteSpecificType\":\"\",\"seoTitleSource\":\"fromCustom\",\"seoTitleField\":\"\",\"siteNamePositionSource\":\"fromCustom\",\"seoDescriptionSource\":\"fromCustom\",\"seoDescriptionField\":\"\",\"seoKeywordsSource\":\"fromCustom\",\"seoKeywordsField\":\"\",\"seoImageIds\":[],\"seoImageSource\":\"fromAsset\",\"seoImageField\":\"\",\"seoImageTransform\":true,\"seoImageTransformMode\":\"crop\",\"seoImageDescriptionSource\":\"fromCustom\",\"seoImageDescriptionField\":\"\",\"twitterCreatorSource\":\"sameAsSite\",\"twitterCreatorField\":\"\",\"twitterTitleSource\":\"sameAsSeo\",\"twitterTitleField\":\"\",\"twitterSiteNamePositionSource\":\"fromCustom\",\"twitterDescriptionSource\":\"sameAsSeo\",\"twitterDescriptionField\":\"\",\"twitterImageIds\":[],\"twitterImageSource\":\"sameAsSeo\",\"twitterImageField\":\"\",\"twitterImageTransform\":true,\"twitterImageTransformMode\":\"crop\",\"twitterImageDescriptionSource\":\"sameAsSeo\",\"twitterImageDescriptionField\":\"\",\"ogTitleSource\":\"sameAsSeo\",\"ogTitleField\":\"\",\"ogSiteNamePositionSource\":\"fromCustom\",\"ogDescriptionSource\":\"sameAsSeo\",\"ogDescriptionField\":\"\",\"ogImageIds\":[],\"ogImageSource\":\"sameAsSeo\",\"ogImageField\":\"\",\"ogImageTransform\":true,\"ogImageTransformMode\":\"crop\",\"ogImageDescriptionSource\":\"sameAsSeo\",\"ogImageDescriptionField\":\"\"}'),
	(2,'2019-01-30 21:13:52','2019-02-05 20:30:09','5be7a644-b2d5-4392-9bf8-4e7da568fc9b','1.0.28','section',1,'Blog','blog','channel','blog/_entry',1,'{\"1\":{\"id\":\"1\",\"sectionId\":\"1\",\"siteId\":\"1\",\"enabledByDefault\":\"1\",\"hasUrls\":\"1\",\"uriFormat\":\"blog/{slug}\",\"template\":\"blog/_entry\",\"language\":\"en-us\"}}','2019-02-05 20:30:08','{\"language\":null,\"mainEntityOfPage\":\"WebPage\",\"seoTitle\":\"{entry.title}\",\"siteNamePosition\":\"\",\"seoDescription\":\"\",\"seoKeywords\":\"\",\"seoImage\":\"\",\"seoImageWidth\":\"\",\"seoImageHeight\":\"\",\"seoImageDescription\":\"\",\"canonicalUrl\":\"{entry.url}\",\"robots\":\"all\",\"ogType\":\"website\",\"ogTitle\":\"{seomatic.meta.seoTitle}\",\"ogSiteNamePosition\":\"\",\"ogDescription\":\"{seomatic.meta.seoDescription}\",\"ogImage\":\"{seomatic.meta.seoImage}\",\"ogImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"ogImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"ogImageDescription\":\"{seomatic.meta.seoImageDescription}\",\"twitterCard\":\"summary_large_image\",\"twitterCreator\":\"{seomatic.site.twitterHandle}\",\"twitterTitle\":\"{seomatic.meta.seoTitle}\",\"twitterSiteNamePosition\":\"\",\"twitterDescription\":\"{seomatic.meta.seoDescription}\",\"twitterImage\":\"{seomatic.meta.seoImage}\",\"twitterImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"twitterImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"twitterImageDescription\":\"{seomatic.meta.seoImageDescription}\"}','{\"siteName\":\"Project Heart\",\"identity\":null,\"creator\":null,\"twitterHandle\":\"\",\"facebookProfileId\":\"\",\"facebookAppId\":\"\",\"googleSiteVerification\":\"\",\"bingSiteVerification\":\"\",\"pinterestSiteVerification\":\"\",\"sameAsLinks\":[],\"siteLinksSearchTarget\":\"\",\"siteLinksQueryInput\":\"\",\"additionalSitemapUrls\":[],\"additionalSitemapUrlsDateUpdated\":null,\"additionalSitemaps\":[]}','{\"sitemapUrls\":true,\"sitemapAssets\":true,\"sitemapFiles\":true,\"sitemapAltLinks\":true,\"sitemapChangeFreq\":\"weekly\",\"sitemapPriority\":0.5,\"sitemapLimit\":null,\"structureDepth\":null,\"sitemapImageFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"caption\",\"field\":\"\"},{\"property\":\"geo_location\",\"field\":\"\"},{\"property\":\"license\",\"field\":\"\"}],\"sitemapVideoFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"description\",\"field\":\"\"},{\"property\":\"thumbnailLoc\",\"field\":\"\"},{\"property\":\"duration\",\"field\":\"\"},{\"property\":\"category\",\"field\":\"\"}]}','{\"MetaTagContainergeneral\":{\"data\":[],\"name\":\"General\",\"description\":\"General Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContaineropengraph\":{\"data\":[],\"name\":\"Facebook\",\"description\":\"Facebook OpenGraph Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"opengraph\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContainertwitter\":{\"data\":[],\"name\":\"Twitter\",\"description\":\"Twitter Card Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"twitter\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContainermiscellaneous\":{\"data\":[],\"name\":\"Miscellaneous\",\"description\":\"Miscellaneous Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"miscellaneous\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaLinkContainergeneral\":{\"data\":[],\"name\":\"General\",\"description\":\"Link Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaLinkContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaScriptContainergeneral\":{\"data\":[],\"position\":1,\"name\":\"General\",\"description\":\"Script Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaScriptContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaJsonLdContainergeneral\":{\"data\":{\"mainEntityOfPage\":{\"breadcrumb\":null,\"lastReviewed\":null,\"mainContentOfPage\":null,\"primaryImageOfPage\":null,\"relatedLink\":null,\"reviewedBy\":null,\"significantLink\":null,\"speakable\":null,\"specialty\":null,\"about\":null,\"accessMode\":null,\"accessModeSufficient\":null,\"accessibilityAPI\":null,\"accessibilityControl\":null,\"accessibilityFeature\":null,\"accessibilityHazard\":null,\"accessibilitySummary\":null,\"accountablePerson\":null,\"aggregateRating\":null,\"alternativeHeadline\":null,\"associatedMedia\":null,\"audience\":null,\"audio\":null,\"author\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"award\":null,\"character\":null,\"citation\":null,\"comment\":null,\"commentCount\":null,\"contentLocation\":null,\"contentRating\":null,\"contentReferenceTime\":null,\"contributor\":null,\"copyrightHolder\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"copyrightYear\":\"{entry.postDate |atom}\",\"creator\":{\"id\":\"{seomatic.site.identity.genericUrl}#creator\"},\"dateCreated\":false,\"dateModified\":\"{entry.dateUpdated |atom}\",\"datePublished\":\"{entry.postDate |atom}\",\"discussionUrl\":null,\"editor\":null,\"educationalAlignment\":null,\"educationalUse\":null,\"encoding\":null,\"exampleOfWork\":null,\"expires\":null,\"fileFormat\":null,\"funder\":null,\"genre\":null,\"hasPart\":null,\"headline\":\"{seomatic.meta.seoTitle}\",\"inLanguage\":\"{seomatic.meta.language}\",\"interactionStatistic\":null,\"interactivityType\":null,\"isAccessibleForFree\":null,\"isBasedOn\":null,\"isFamilyFriendly\":null,\"isPartOf\":null,\"keywords\":null,\"learningResourceType\":null,\"license\":null,\"locationCreated\":null,\"mainEntity\":null,\"material\":null,\"mentions\":null,\"offers\":null,\"position\":null,\"producer\":null,\"provider\":null,\"publication\":null,\"publisher\":{\"id\":\"{seomatic.site.identity.genericUrl}#creator\"},\"publisherImprint\":null,\"publishingPrinciples\":null,\"recordedAt\":null,\"releasedEvent\":null,\"review\":null,\"schemaVersion\":null,\"sourceOrganization\":null,\"spatialCoverage\":null,\"sponsor\":null,\"temporalCoverage\":null,\"text\":null,\"thumbnailUrl\":null,\"timeRequired\":null,\"translationOfWork\":null,\"translator\":null,\"typicalAgeRange\":null,\"version\":null,\"video\":null,\"workExample\":null,\"workTranslation\":null,\"additionalType\":null,\"alternateName\":null,\"description\":\"{seomatic.meta.seoDescription}\",\"disambiguatingDescription\":null,\"identifier\":null,\"image\":{\"type\":\"ImageObject\",\"url\":\"{seomatic.meta.seoImage}\"},\"mainEntityOfPage\":\"{seomatic.meta.canonicalUrl}\",\"name\":\"{seomatic.meta.seoTitle}\",\"potentialAction\":{\"type\":\"SearchAction\",\"target\":\"{seomatic.site.siteLinksSearchTarget}\",\"query-input\":\"{seomatic.helper.siteLinksQueryInput()}\"},\"sameAs\":null,\"url\":\"{seomatic.meta.canonicalUrl}\",\"context\":\"http://schema.org\",\"type\":\"{seomatic.meta.mainEntityOfPage}\",\"id\":null,\"include\":true,\"key\":\"mainEntityOfPage\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"JsonLd Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaJsonLdContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTitleContainergeneral\":{\"data\":{\"title\":{\"title\":\"{seomatic.meta.seoTitle}\",\"siteName\":\"{seomatic.site.siteName}\",\"siteNamePosition\":\"{seomatic.meta.siteNamePosition}\",\"separatorChar\":\"{seomatic.config.separatorChar}\",\"include\":true,\"key\":\"title\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"Meta Title Tag\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTitleContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false}}','[]','{\"data\":[],\"name\":null,\"description\":null,\"class\":\"nystudio107\\\\seomatic\\\\models\\\\FrontendTemplateContainer\",\"handle\":null,\"include\":true,\"dependencies\":null,\"clearCache\":false}','{\"siteType\":\"CreativeWork\",\"siteSubType\":\"WebPage\",\"siteSpecificType\":\"\",\"seoTitleSource\":\"fromField\",\"seoTitleField\":\"title\",\"siteNamePositionSource\":\"sameAsGlobal\",\"seoDescriptionSource\":\"fromCustom\",\"seoDescriptionField\":\"\",\"seoKeywordsSource\":\"fromCustom\",\"seoKeywordsField\":\"\",\"seoImageIds\":[],\"seoImageSource\":\"fromAsset\",\"seoImageField\":\"\",\"seoImageTransform\":true,\"seoImageTransformMode\":\"crop\",\"seoImageDescriptionSource\":\"fromCustom\",\"seoImageDescriptionField\":\"\",\"twitterCreatorSource\":\"sameAsSite\",\"twitterCreatorField\":\"\",\"twitterTitleSource\":\"sameAsSeo\",\"twitterTitleField\":\"\",\"twitterSiteNamePositionSource\":\"sameAsGlobal\",\"twitterDescriptionSource\":\"sameAsSeo\",\"twitterDescriptionField\":\"\",\"twitterImageIds\":[],\"twitterImageSource\":\"sameAsSeo\",\"twitterImageField\":\"\",\"twitterImageTransform\":true,\"twitterImageTransformMode\":\"crop\",\"twitterImageDescriptionSource\":\"sameAsSeo\",\"twitterImageDescriptionField\":\"\",\"ogTitleSource\":\"sameAsSeo\",\"ogTitleField\":\"\",\"ogSiteNamePositionSource\":\"sameAsGlobal\",\"ogDescriptionSource\":\"sameAsSeo\",\"ogDescriptionField\":\"\",\"ogImageIds\":[],\"ogImageSource\":\"sameAsSeo\",\"ogImageField\":\"\",\"ogImageTransform\":true,\"ogImageTransformMode\":\"crop\",\"ogImageDescriptionSource\":\"sameAsSeo\",\"ogImageDescriptionField\":\"\"}'),
	(3,'2019-01-30 21:13:52','2019-02-05 18:47:10','00beef66-98d3-45cc-b348-2c0e7fe106af','1.0.28','section',2,'Call to Action','callToAction','channel','call-to-action/_entry',1,'{\"1\":{\"id\":\"2\",\"sectionId\":\"2\",\"siteId\":\"1\",\"enabledByDefault\":\"1\",\"hasUrls\":\"1\",\"uriFormat\":\"call-to-action/{slug}\",\"template\":\"call-to-action/_entry\",\"language\":\"en-us\"}}','2019-02-05 18:47:10','{\"language\":null,\"mainEntityOfPage\":\"WebPage\",\"seoTitle\":\"{entry.title}\",\"siteNamePosition\":\"\",\"seoDescription\":\"\",\"seoKeywords\":\"\",\"seoImage\":\"\",\"seoImageWidth\":\"\",\"seoImageHeight\":\"\",\"seoImageDescription\":\"\",\"canonicalUrl\":\"{entry.url}\",\"robots\":\"all\",\"ogType\":\"website\",\"ogTitle\":\"{seomatic.meta.seoTitle}\",\"ogSiteNamePosition\":\"\",\"ogDescription\":\"{seomatic.meta.seoDescription}\",\"ogImage\":\"{seomatic.meta.seoImage}\",\"ogImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"ogImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"ogImageDescription\":\"{seomatic.meta.seoImageDescription}\",\"twitterCard\":\"summary_large_image\",\"twitterCreator\":\"{seomatic.site.twitterHandle}\",\"twitterTitle\":\"{seomatic.meta.seoTitle}\",\"twitterSiteNamePosition\":\"\",\"twitterDescription\":\"{seomatic.meta.seoDescription}\",\"twitterImage\":\"{seomatic.meta.seoImage}\",\"twitterImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"twitterImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"twitterImageDescription\":\"{seomatic.meta.seoImageDescription}\"}','{\"siteName\":\"Project Heart\",\"identity\":null,\"creator\":null,\"twitterHandle\":\"\",\"facebookProfileId\":\"\",\"facebookAppId\":\"\",\"googleSiteVerification\":\"\",\"bingSiteVerification\":\"\",\"pinterestSiteVerification\":\"\",\"sameAsLinks\":[],\"siteLinksSearchTarget\":\"\",\"siteLinksQueryInput\":\"\",\"additionalSitemapUrls\":[],\"additionalSitemapUrlsDateUpdated\":null,\"additionalSitemaps\":[]}','{\"sitemapUrls\":true,\"sitemapAssets\":true,\"sitemapFiles\":true,\"sitemapAltLinks\":true,\"sitemapChangeFreq\":\"weekly\",\"sitemapPriority\":0.5,\"sitemapLimit\":null,\"structureDepth\":null,\"sitemapImageFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"caption\",\"field\":\"\"},{\"property\":\"geo_location\",\"field\":\"\"},{\"property\":\"license\",\"field\":\"\"}],\"sitemapVideoFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"description\",\"field\":\"\"},{\"property\":\"thumbnailLoc\",\"field\":\"\"},{\"property\":\"duration\",\"field\":\"\"},{\"property\":\"category\",\"field\":\"\"}]}','{\"MetaTagContainergeneral\":{\"data\":[],\"name\":\"General\",\"description\":\"General Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContaineropengraph\":{\"data\":[],\"name\":\"Facebook\",\"description\":\"Facebook OpenGraph Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"opengraph\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContainertwitter\":{\"data\":[],\"name\":\"Twitter\",\"description\":\"Twitter Card Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"twitter\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContainermiscellaneous\":{\"data\":[],\"name\":\"Miscellaneous\",\"description\":\"Miscellaneous Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"miscellaneous\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaLinkContainergeneral\":{\"data\":[],\"name\":\"General\",\"description\":\"Link Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaLinkContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaScriptContainergeneral\":{\"data\":[],\"position\":1,\"name\":\"General\",\"description\":\"Script Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaScriptContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaJsonLdContainergeneral\":{\"data\":{\"mainEntityOfPage\":{\"breadcrumb\":null,\"lastReviewed\":null,\"mainContentOfPage\":null,\"primaryImageOfPage\":null,\"relatedLink\":null,\"reviewedBy\":null,\"significantLink\":null,\"speakable\":null,\"specialty\":null,\"about\":null,\"accessMode\":null,\"accessModeSufficient\":null,\"accessibilityAPI\":null,\"accessibilityControl\":null,\"accessibilityFeature\":null,\"accessibilityHazard\":null,\"accessibilitySummary\":null,\"accountablePerson\":null,\"aggregateRating\":null,\"alternativeHeadline\":null,\"associatedMedia\":null,\"audience\":null,\"audio\":null,\"author\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"award\":null,\"character\":null,\"citation\":null,\"comment\":null,\"commentCount\":null,\"contentLocation\":null,\"contentRating\":null,\"contentReferenceTime\":null,\"contributor\":null,\"copyrightHolder\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"copyrightYear\":\"{entry.postDate |atom}\",\"creator\":{\"id\":\"{seomatic.site.identity.genericUrl}#creator\"},\"dateCreated\":false,\"dateModified\":\"{entry.dateUpdated |atom}\",\"datePublished\":\"{entry.postDate |atom}\",\"discussionUrl\":null,\"editor\":null,\"educationalAlignment\":null,\"educationalUse\":null,\"encoding\":null,\"exampleOfWork\":null,\"expires\":null,\"fileFormat\":null,\"funder\":null,\"genre\":null,\"hasPart\":null,\"headline\":\"{seomatic.meta.seoTitle}\",\"inLanguage\":\"{seomatic.meta.language}\",\"interactionStatistic\":null,\"interactivityType\":null,\"isAccessibleForFree\":null,\"isBasedOn\":null,\"isFamilyFriendly\":null,\"isPartOf\":null,\"keywords\":null,\"learningResourceType\":null,\"license\":null,\"locationCreated\":null,\"mainEntity\":null,\"material\":null,\"mentions\":null,\"offers\":null,\"position\":null,\"producer\":null,\"provider\":null,\"publication\":null,\"publisher\":{\"id\":\"{seomatic.site.identity.genericUrl}#creator\"},\"publisherImprint\":null,\"publishingPrinciples\":null,\"recordedAt\":null,\"releasedEvent\":null,\"review\":null,\"schemaVersion\":null,\"sourceOrganization\":null,\"spatialCoverage\":null,\"sponsor\":null,\"temporalCoverage\":null,\"text\":null,\"thumbnailUrl\":null,\"timeRequired\":null,\"translationOfWork\":null,\"translator\":null,\"typicalAgeRange\":null,\"version\":null,\"video\":null,\"workExample\":null,\"workTranslation\":null,\"additionalType\":null,\"alternateName\":null,\"description\":\"{seomatic.meta.seoDescription}\",\"disambiguatingDescription\":null,\"identifier\":null,\"image\":{\"type\":\"ImageObject\",\"url\":\"{seomatic.meta.seoImage}\"},\"mainEntityOfPage\":\"{seomatic.meta.canonicalUrl}\",\"name\":\"{seomatic.meta.seoTitle}\",\"potentialAction\":{\"type\":\"SearchAction\",\"target\":\"{seomatic.site.siteLinksSearchTarget}\",\"query-input\":\"{seomatic.helper.siteLinksQueryInput()}\"},\"sameAs\":null,\"url\":\"{seomatic.meta.canonicalUrl}\",\"context\":\"http://schema.org\",\"type\":\"{seomatic.meta.mainEntityOfPage}\",\"id\":null,\"include\":true,\"key\":\"mainEntityOfPage\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"JsonLd Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaJsonLdContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTitleContainergeneral\":{\"data\":{\"title\":{\"title\":\"{seomatic.meta.seoTitle}\",\"siteName\":\"{seomatic.site.siteName}\",\"siteNamePosition\":\"{seomatic.meta.siteNamePosition}\",\"separatorChar\":\"{seomatic.config.separatorChar}\",\"include\":true,\"key\":\"title\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"Meta Title Tag\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTitleContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false}}','[]','{\"data\":[],\"name\":null,\"description\":null,\"class\":\"nystudio107\\\\seomatic\\\\models\\\\FrontendTemplateContainer\",\"handle\":null,\"include\":true,\"dependencies\":null,\"clearCache\":false}','{\"siteType\":\"CreativeWork\",\"siteSubType\":\"WebPage\",\"siteSpecificType\":\"\",\"seoTitleSource\":\"fromField\",\"seoTitleField\":\"title\",\"siteNamePositionSource\":\"sameAsGlobal\",\"seoDescriptionSource\":\"fromCustom\",\"seoDescriptionField\":\"\",\"seoKeywordsSource\":\"fromCustom\",\"seoKeywordsField\":\"\",\"seoImageIds\":[],\"seoImageSource\":\"fromAsset\",\"seoImageField\":\"\",\"seoImageTransform\":true,\"seoImageTransformMode\":\"crop\",\"seoImageDescriptionSource\":\"fromCustom\",\"seoImageDescriptionField\":\"\",\"twitterCreatorSource\":\"sameAsSite\",\"twitterCreatorField\":\"\",\"twitterTitleSource\":\"sameAsSeo\",\"twitterTitleField\":\"\",\"twitterSiteNamePositionSource\":\"sameAsGlobal\",\"twitterDescriptionSource\":\"sameAsSeo\",\"twitterDescriptionField\":\"\",\"twitterImageIds\":[],\"twitterImageSource\":\"sameAsSeo\",\"twitterImageField\":\"\",\"twitterImageTransform\":true,\"twitterImageTransformMode\":\"crop\",\"twitterImageDescriptionSource\":\"sameAsSeo\",\"twitterImageDescriptionField\":\"\",\"ogTitleSource\":\"sameAsSeo\",\"ogTitleField\":\"\",\"ogSiteNamePositionSource\":\"sameAsGlobal\",\"ogDescriptionSource\":\"sameAsSeo\",\"ogDescriptionField\":\"\",\"ogImageIds\":[],\"ogImageSource\":\"sameAsSeo\",\"ogImageField\":\"\",\"ogImageTransform\":true,\"ogImageTransformMode\":\"crop\",\"ogImageDescriptionSource\":\"sameAsSeo\",\"ogImageDescriptionField\":\"\"}'),
	(4,'2019-01-30 21:13:52','2019-11-07 16:37:11','b0f3b89b-90aa-446c-a4df-3136f89ef16d','1.0.28','section',3,'Homepage','homepage','single','',1,'{\"1\":{\"id\":\"3\",\"sectionId\":\"3\",\"siteId\":\"1\",\"enabledByDefault\":\"1\",\"hasUrls\":\"1\",\"uriFormat\":\"__home__\",\"template\":\"\",\"language\":\"en-us\"}}','2019-11-07 16:37:10','{\"language\":null,\"mainEntityOfPage\":\"WebPage\",\"seoTitle\":\"{entry.title}\",\"siteNamePosition\":\"\",\"seoDescription\":\"\",\"seoKeywords\":\"\",\"seoImage\":\"\",\"seoImageWidth\":\"\",\"seoImageHeight\":\"\",\"seoImageDescription\":\"\",\"canonicalUrl\":\"{entry.url}\",\"robots\":\"all\",\"ogType\":\"website\",\"ogTitle\":\"{seomatic.meta.seoTitle}\",\"ogSiteNamePosition\":\"\",\"ogDescription\":\"{seomatic.meta.seoDescription}\",\"ogImage\":\"{seomatic.meta.seoImage}\",\"ogImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"ogImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"ogImageDescription\":\"{seomatic.meta.seoImageDescription}\",\"twitterCard\":\"summary_large_image\",\"twitterCreator\":\"{seomatic.site.twitterHandle}\",\"twitterTitle\":\"{seomatic.meta.seoTitle}\",\"twitterSiteNamePosition\":\"\",\"twitterDescription\":\"{seomatic.meta.seoDescription}\",\"twitterImage\":\"{seomatic.meta.seoImage}\",\"twitterImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"twitterImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"twitterImageDescription\":\"{seomatic.meta.seoImageDescription}\"}','{\"siteName\":\"Project Heart\",\"identity\":null,\"creator\":null,\"twitterHandle\":\"\",\"facebookProfileId\":\"\",\"facebookAppId\":\"\",\"googleSiteVerification\":\"\",\"bingSiteVerification\":\"\",\"pinterestSiteVerification\":\"\",\"sameAsLinks\":[],\"siteLinksSearchTarget\":\"\",\"siteLinksQueryInput\":\"\",\"additionalSitemapUrls\":[],\"additionalSitemapUrlsDateUpdated\":null,\"additionalSitemaps\":[]}','{\"sitemapUrls\":true,\"sitemapAssets\":true,\"sitemapFiles\":true,\"sitemapAltLinks\":true,\"sitemapChangeFreq\":\"weekly\",\"sitemapPriority\":0.5,\"sitemapLimit\":null,\"structureDepth\":null,\"sitemapImageFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"caption\",\"field\":\"\"},{\"property\":\"geo_location\",\"field\":\"\"},{\"property\":\"license\",\"field\":\"\"}],\"sitemapVideoFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"description\",\"field\":\"\"},{\"property\":\"thumbnailLoc\",\"field\":\"\"},{\"property\":\"duration\",\"field\":\"\"},{\"property\":\"category\",\"field\":\"\"}]}','{\"MetaTagContainergeneral\":{\"data\":[],\"name\":\"General\",\"description\":\"General Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContaineropengraph\":{\"data\":[],\"name\":\"Facebook\",\"description\":\"Facebook OpenGraph Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"opengraph\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContainertwitter\":{\"data\":[],\"name\":\"Twitter\",\"description\":\"Twitter Card Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"twitter\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContainermiscellaneous\":{\"data\":[],\"name\":\"Miscellaneous\",\"description\":\"Miscellaneous Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"miscellaneous\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaLinkContainergeneral\":{\"data\":[],\"name\":\"General\",\"description\":\"Link Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaLinkContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaScriptContainergeneral\":{\"data\":[],\"position\":1,\"name\":\"General\",\"description\":\"Script Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaScriptContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaJsonLdContainergeneral\":{\"data\":{\"mainEntityOfPage\":{\"breadcrumb\":null,\"lastReviewed\":null,\"mainContentOfPage\":null,\"primaryImageOfPage\":null,\"relatedLink\":null,\"reviewedBy\":null,\"significantLink\":null,\"speakable\":null,\"specialty\":null,\"about\":null,\"accessMode\":null,\"accessModeSufficient\":null,\"accessibilityAPI\":null,\"accessibilityControl\":null,\"accessibilityFeature\":null,\"accessibilityHazard\":null,\"accessibilitySummary\":null,\"accountablePerson\":null,\"aggregateRating\":null,\"alternativeHeadline\":null,\"associatedMedia\":null,\"audience\":null,\"audio\":null,\"author\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"award\":null,\"character\":null,\"citation\":null,\"comment\":null,\"commentCount\":null,\"contentLocation\":null,\"contentRating\":null,\"contentReferenceTime\":null,\"contributor\":null,\"copyrightHolder\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"copyrightYear\":\"{entry.postDate |atom}\",\"correction\":null,\"creator\":{\"id\":\"{seomatic.site.identity.genericUrl}#creator\"},\"dateCreated\":false,\"dateModified\":\"{entry.dateUpdated |atom}\",\"datePublished\":\"{entry.postDate |atom}\",\"discussionUrl\":null,\"editor\":null,\"educationalAlignment\":null,\"educationalUse\":null,\"encoding\":null,\"encodingFormat\":null,\"exampleOfWork\":null,\"expires\":null,\"funder\":null,\"genre\":null,\"hasPart\":null,\"headline\":\"{seomatic.meta.seoTitle}\",\"inLanguage\":\"{seomatic.meta.language}\",\"interactionStatistic\":null,\"interactivityType\":null,\"isAccessibleForFree\":null,\"isBasedOn\":null,\"isFamilyFriendly\":null,\"isPartOf\":null,\"keywords\":null,\"learningResourceType\":null,\"license\":null,\"locationCreated\":null,\"mainEntity\":null,\"material\":null,\"materialExtent\":null,\"mentions\":null,\"offers\":null,\"position\":null,\"producer\":null,\"provider\":null,\"publication\":null,\"publisher\":{\"id\":\"{seomatic.site.identity.genericUrl}#creator\"},\"publisherImprint\":null,\"publishingPrinciples\":null,\"recordedAt\":null,\"releasedEvent\":null,\"review\":null,\"schemaVersion\":null,\"sdDatePublished\":null,\"sdLicense\":null,\"sdPublisher\":null,\"sourceOrganization\":null,\"spatial\":null,\"spatialCoverage\":null,\"sponsor\":null,\"temporal\":null,\"temporalCoverage\":null,\"text\":null,\"thumbnailUrl\":null,\"timeRequired\":null,\"translationOfWork\":null,\"translator\":null,\"typicalAgeRange\":null,\"version\":null,\"video\":null,\"workExample\":null,\"workTranslation\":null,\"additionalType\":null,\"alternateName\":null,\"description\":\"{seomatic.meta.seoDescription}\",\"disambiguatingDescription\":null,\"identifier\":null,\"image\":{\"type\":\"ImageObject\",\"url\":\"{seomatic.meta.seoImage}\"},\"mainEntityOfPage\":\"{seomatic.meta.canonicalUrl}\",\"name\":\"{seomatic.meta.seoTitle}\",\"potentialAction\":{\"type\":\"SearchAction\",\"target\":\"{seomatic.site.siteLinksSearchTarget}\",\"query-input\":\"{seomatic.helper.siteLinksQueryInput()}\"},\"sameAs\":null,\"subjectOf\":null,\"url\":\"{seomatic.meta.canonicalUrl}\",\"context\":\"http://schema.org\",\"type\":\"{seomatic.meta.mainEntityOfPage}\",\"id\":null,\"graph\":null,\"include\":true,\"key\":\"mainEntityOfPage\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"JsonLd Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaJsonLdContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTitleContainergeneral\":{\"data\":{\"title\":{\"title\":\"{seomatic.meta.seoTitle}\",\"siteName\":\"{seomatic.site.siteName}\",\"siteNamePosition\":\"{seomatic.meta.siteNamePosition}\",\"separatorChar\":\"{seomatic.config.separatorChar}\",\"include\":true,\"key\":\"title\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"Meta Title Tag\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTitleContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false}}','[]','{\"data\":[],\"name\":null,\"description\":null,\"class\":\"nystudio107\\\\seomatic\\\\models\\\\FrontendTemplateContainer\",\"handle\":null,\"include\":true,\"dependencies\":null,\"clearCache\":false}','{\"siteType\":\"CreativeWork\",\"siteSubType\":\"WebPage\",\"siteSpecificType\":\"\",\"seoTitleSource\":\"fromField\",\"seoTitleField\":\"title\",\"siteNamePositionSource\":\"sameAsGlobal\",\"seoDescriptionSource\":\"fromCustom\",\"seoDescriptionField\":\"\",\"seoKeywordsSource\":\"fromCustom\",\"seoKeywordsField\":\"\",\"seoImageIds\":[],\"seoImageSource\":\"fromAsset\",\"seoImageField\":\"\",\"seoImageTransform\":true,\"seoImageTransformMode\":\"crop\",\"seoImageDescriptionSource\":\"fromCustom\",\"seoImageDescriptionField\":\"\",\"twitterCreatorSource\":\"sameAsSite\",\"twitterCreatorField\":\"\",\"twitterTitleSource\":\"sameAsSeo\",\"twitterTitleField\":\"\",\"twitterSiteNamePositionSource\":\"sameAsGlobal\",\"twitterDescriptionSource\":\"sameAsSeo\",\"twitterDescriptionField\":\"\",\"twitterImageIds\":[],\"twitterImageSource\":\"sameAsSeo\",\"twitterImageField\":\"\",\"twitterImageTransform\":true,\"twitterImageTransformMode\":\"crop\",\"twitterImageDescriptionSource\":\"sameAsSeo\",\"twitterImageDescriptionField\":\"\",\"ogTitleSource\":\"sameAsSeo\",\"ogTitleField\":\"\",\"ogSiteNamePositionSource\":\"sameAsGlobal\",\"ogDescriptionSource\":\"sameAsSeo\",\"ogDescriptionField\":\"\",\"ogImageIds\":[],\"ogImageSource\":\"sameAsSeo\",\"ogImageField\":\"\",\"ogImageTransform\":true,\"ogImageTransformMode\":\"crop\",\"ogImageDescriptionSource\":\"sameAsSeo\",\"ogImageDescriptionField\":\"\"}'),
	(5,'2019-01-30 21:13:52','2019-11-07 16:38:03','0ff38588-a299-4ff0-a84b-06283b1ff6a4','1.0.28','section',4,'Page','page','structure','page/_entry',1,'{\"1\":{\"id\":\"4\",\"sectionId\":\"4\",\"siteId\":\"1\",\"enabledByDefault\":\"1\",\"hasUrls\":\"1\",\"uriFormat\":\"{slug}\",\"template\":\"page/_entry\",\"language\":\"en-us\"}}','2019-11-07 16:38:02','{\"language\":null,\"mainEntityOfPage\":\"WebPage\",\"seoTitle\":\"{entry.title}\",\"siteNamePosition\":\"\",\"seoDescription\":\"\",\"seoKeywords\":\"\",\"seoImage\":\"\",\"seoImageWidth\":\"\",\"seoImageHeight\":\"\",\"seoImageDescription\":\"\",\"canonicalUrl\":\"{entry.url}\",\"robots\":\"all\",\"ogType\":\"website\",\"ogTitle\":\"{seomatic.meta.seoTitle}\",\"ogSiteNamePosition\":\"\",\"ogDescription\":\"{seomatic.meta.seoDescription}\",\"ogImage\":\"{seomatic.meta.seoImage}\",\"ogImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"ogImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"ogImageDescription\":\"{seomatic.meta.seoImageDescription}\",\"twitterCard\":\"summary_large_image\",\"twitterCreator\":\"{seomatic.site.twitterHandle}\",\"twitterTitle\":\"{seomatic.meta.seoTitle}\",\"twitterSiteNamePosition\":\"\",\"twitterDescription\":\"{seomatic.meta.seoDescription}\",\"twitterImage\":\"{seomatic.meta.seoImage}\",\"twitterImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"twitterImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"twitterImageDescription\":\"{seomatic.meta.seoImageDescription}\"}','{\"siteName\":\"Project Heart\",\"identity\":null,\"creator\":null,\"twitterHandle\":\"\",\"facebookProfileId\":\"\",\"facebookAppId\":\"\",\"googleSiteVerification\":\"\",\"bingSiteVerification\":\"\",\"pinterestSiteVerification\":\"\",\"sameAsLinks\":[],\"siteLinksSearchTarget\":\"\",\"siteLinksQueryInput\":\"\",\"additionalSitemapUrls\":[],\"additionalSitemapUrlsDateUpdated\":null,\"additionalSitemaps\":[]}','{\"sitemapUrls\":true,\"sitemapAssets\":true,\"sitemapFiles\":true,\"sitemapAltLinks\":true,\"sitemapChangeFreq\":\"weekly\",\"sitemapPriority\":0.5,\"sitemapLimit\":null,\"structureDepth\":null,\"sitemapImageFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"caption\",\"field\":\"\"},{\"property\":\"geo_location\",\"field\":\"\"},{\"property\":\"license\",\"field\":\"\"}],\"sitemapVideoFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"description\",\"field\":\"\"},{\"property\":\"thumbnailLoc\",\"field\":\"\"},{\"property\":\"duration\",\"field\":\"\"},{\"property\":\"category\",\"field\":\"\"}]}','{\"MetaTagContainergeneral\":{\"data\":[],\"name\":\"General\",\"description\":\"General Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContaineropengraph\":{\"data\":[],\"name\":\"Facebook\",\"description\":\"Facebook OpenGraph Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"opengraph\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContainertwitter\":{\"data\":[],\"name\":\"Twitter\",\"description\":\"Twitter Card Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"twitter\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContainermiscellaneous\":{\"data\":[],\"name\":\"Miscellaneous\",\"description\":\"Miscellaneous Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"miscellaneous\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaLinkContainergeneral\":{\"data\":[],\"name\":\"General\",\"description\":\"Link Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaLinkContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaScriptContainergeneral\":{\"data\":[],\"position\":1,\"name\":\"General\",\"description\":\"Script Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaScriptContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaJsonLdContainergeneral\":{\"data\":{\"mainEntityOfPage\":{\"breadcrumb\":null,\"lastReviewed\":null,\"mainContentOfPage\":null,\"primaryImageOfPage\":null,\"relatedLink\":null,\"reviewedBy\":null,\"significantLink\":null,\"speakable\":null,\"specialty\":null,\"about\":null,\"accessMode\":null,\"accessModeSufficient\":null,\"accessibilityAPI\":null,\"accessibilityControl\":null,\"accessibilityFeature\":null,\"accessibilityHazard\":null,\"accessibilitySummary\":null,\"accountablePerson\":null,\"aggregateRating\":null,\"alternativeHeadline\":null,\"associatedMedia\":null,\"audience\":null,\"audio\":null,\"author\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"award\":null,\"character\":null,\"citation\":null,\"comment\":null,\"commentCount\":null,\"contentLocation\":null,\"contentRating\":null,\"contentReferenceTime\":null,\"contributor\":null,\"copyrightHolder\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"copyrightYear\":\"{entry.postDate |atom}\",\"correction\":null,\"creator\":{\"id\":\"{seomatic.site.identity.genericUrl}#creator\"},\"dateCreated\":false,\"dateModified\":\"{entry.dateUpdated |atom}\",\"datePublished\":\"{entry.postDate |atom}\",\"discussionUrl\":null,\"editor\":null,\"educationalAlignment\":null,\"educationalUse\":null,\"encoding\":null,\"encodingFormat\":null,\"exampleOfWork\":null,\"expires\":null,\"funder\":null,\"genre\":null,\"hasPart\":null,\"headline\":\"{seomatic.meta.seoTitle}\",\"inLanguage\":\"{seomatic.meta.language}\",\"interactionStatistic\":null,\"interactivityType\":null,\"isAccessibleForFree\":null,\"isBasedOn\":null,\"isFamilyFriendly\":null,\"isPartOf\":null,\"keywords\":null,\"learningResourceType\":null,\"license\":null,\"locationCreated\":null,\"mainEntity\":null,\"material\":null,\"materialExtent\":null,\"mentions\":null,\"offers\":null,\"position\":null,\"producer\":null,\"provider\":null,\"publication\":null,\"publisher\":{\"id\":\"{seomatic.site.identity.genericUrl}#creator\"},\"publisherImprint\":null,\"publishingPrinciples\":null,\"recordedAt\":null,\"releasedEvent\":null,\"review\":null,\"schemaVersion\":null,\"sdDatePublished\":null,\"sdLicense\":null,\"sdPublisher\":null,\"sourceOrganization\":null,\"spatial\":null,\"spatialCoverage\":null,\"sponsor\":null,\"temporal\":null,\"temporalCoverage\":null,\"text\":null,\"thumbnailUrl\":null,\"timeRequired\":null,\"translationOfWork\":null,\"translator\":null,\"typicalAgeRange\":null,\"version\":null,\"video\":null,\"workExample\":null,\"workTranslation\":null,\"additionalType\":null,\"alternateName\":null,\"description\":\"{seomatic.meta.seoDescription}\",\"disambiguatingDescription\":null,\"identifier\":null,\"image\":{\"type\":\"ImageObject\",\"url\":\"{seomatic.meta.seoImage}\"},\"mainEntityOfPage\":\"{seomatic.meta.canonicalUrl}\",\"name\":\"{seomatic.meta.seoTitle}\",\"potentialAction\":{\"type\":\"SearchAction\",\"target\":\"{seomatic.site.siteLinksSearchTarget}\",\"query-input\":\"{seomatic.helper.siteLinksQueryInput()}\"},\"sameAs\":null,\"subjectOf\":null,\"url\":\"{seomatic.meta.canonicalUrl}\",\"context\":\"http://schema.org\",\"type\":\"{seomatic.meta.mainEntityOfPage}\",\"id\":null,\"graph\":null,\"include\":true,\"key\":\"mainEntityOfPage\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"JsonLd Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaJsonLdContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTitleContainergeneral\":{\"data\":{\"title\":{\"title\":\"{seomatic.meta.seoTitle}\",\"siteName\":\"{seomatic.site.siteName}\",\"siteNamePosition\":\"{seomatic.meta.siteNamePosition}\",\"separatorChar\":\"{seomatic.config.separatorChar}\",\"include\":true,\"key\":\"title\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"Meta Title Tag\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTitleContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false}}','[]','{\"data\":[],\"name\":null,\"description\":null,\"class\":\"nystudio107\\\\seomatic\\\\models\\\\FrontendTemplateContainer\",\"handle\":null,\"include\":true,\"dependencies\":null,\"clearCache\":false}','{\"siteType\":\"CreativeWork\",\"siteSubType\":\"WebPage\",\"siteSpecificType\":\"\",\"seoTitleSource\":\"fromField\",\"seoTitleField\":\"title\",\"siteNamePositionSource\":\"sameAsGlobal\",\"seoDescriptionSource\":\"fromCustom\",\"seoDescriptionField\":\"\",\"seoKeywordsSource\":\"fromCustom\",\"seoKeywordsField\":\"\",\"seoImageIds\":[],\"seoImageSource\":\"fromAsset\",\"seoImageField\":\"\",\"seoImageTransform\":true,\"seoImageTransformMode\":\"crop\",\"seoImageDescriptionSource\":\"fromCustom\",\"seoImageDescriptionField\":\"\",\"twitterCreatorSource\":\"sameAsSite\",\"twitterCreatorField\":\"\",\"twitterTitleSource\":\"sameAsSeo\",\"twitterTitleField\":\"\",\"twitterSiteNamePositionSource\":\"sameAsGlobal\",\"twitterDescriptionSource\":\"sameAsSeo\",\"twitterDescriptionField\":\"\",\"twitterImageIds\":[],\"twitterImageSource\":\"sameAsSeo\",\"twitterImageField\":\"\",\"twitterImageTransform\":true,\"twitterImageTransformMode\":\"crop\",\"twitterImageDescriptionSource\":\"sameAsSeo\",\"twitterImageDescriptionField\":\"\",\"ogTitleSource\":\"sameAsSeo\",\"ogTitleField\":\"\",\"ogSiteNamePositionSource\":\"sameAsGlobal\",\"ogDescriptionSource\":\"sameAsSeo\",\"ogDescriptionField\":\"\",\"ogImageIds\":[],\"ogImageSource\":\"sameAsSeo\",\"ogImageField\":\"\",\"ogImageTransform\":true,\"ogImageTransformMode\":\"crop\",\"ogImageDescriptionSource\":\"sameAsSeo\",\"ogImageDescriptionField\":\"\"}'),
	(6,'2019-01-30 21:13:52','2019-02-08 17:11:34','1e7a1179-ec6a-4c97-a26a-39eda2c730aa','1.0.28','section',5,'Quotes','quotes','channel','quotes/_entry.twig',1,'{\"1\":{\"id\":\"5\",\"sectionId\":\"5\",\"siteId\":\"1\",\"enabledByDefault\":\"1\",\"hasUrls\":\"1\",\"uriFormat\":\"quotes/{slug}\",\"template\":\"quotes/_entry.twig\",\"language\":\"en-us\"}}','2019-02-08 17:11:34','{\"language\":null,\"mainEntityOfPage\":\"WebPage\",\"seoTitle\":\"{entry.title}\",\"siteNamePosition\":\"\",\"seoDescription\":\"\",\"seoKeywords\":\"\",\"seoImage\":\"\",\"seoImageWidth\":\"\",\"seoImageHeight\":\"\",\"seoImageDescription\":\"\",\"canonicalUrl\":\"{entry.url}\",\"robots\":\"all\",\"ogType\":\"website\",\"ogTitle\":\"{seomatic.meta.seoTitle}\",\"ogSiteNamePosition\":\"\",\"ogDescription\":\"{seomatic.meta.seoDescription}\",\"ogImage\":\"{seomatic.meta.seoImage}\",\"ogImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"ogImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"ogImageDescription\":\"{seomatic.meta.seoImageDescription}\",\"twitterCard\":\"summary_large_image\",\"twitterCreator\":\"{seomatic.site.twitterHandle}\",\"twitterTitle\":\"{seomatic.meta.seoTitle}\",\"twitterSiteNamePosition\":\"\",\"twitterDescription\":\"{seomatic.meta.seoDescription}\",\"twitterImage\":\"{seomatic.meta.seoImage}\",\"twitterImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"twitterImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"twitterImageDescription\":\"{seomatic.meta.seoImageDescription}\"}','{\"siteName\":\"Project Heart\",\"identity\":null,\"creator\":null,\"twitterHandle\":\"\",\"facebookProfileId\":\"\",\"facebookAppId\":\"\",\"googleSiteVerification\":\"\",\"bingSiteVerification\":\"\",\"pinterestSiteVerification\":\"\",\"sameAsLinks\":[],\"siteLinksSearchTarget\":\"\",\"siteLinksQueryInput\":\"\",\"additionalSitemapUrls\":[],\"additionalSitemapUrlsDateUpdated\":null,\"additionalSitemaps\":[]}','{\"sitemapUrls\":true,\"sitemapAssets\":true,\"sitemapFiles\":true,\"sitemapAltLinks\":true,\"sitemapChangeFreq\":\"weekly\",\"sitemapPriority\":0.5,\"sitemapLimit\":null,\"structureDepth\":null,\"sitemapImageFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"caption\",\"field\":\"\"},{\"property\":\"geo_location\",\"field\":\"\"},{\"property\":\"license\",\"field\":\"\"}],\"sitemapVideoFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"description\",\"field\":\"\"},{\"property\":\"thumbnailLoc\",\"field\":\"\"},{\"property\":\"duration\",\"field\":\"\"},{\"property\":\"category\",\"field\":\"\"}]}','{\"MetaTagContainergeneral\":{\"data\":[],\"name\":\"General\",\"description\":\"General Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContaineropengraph\":{\"data\":[],\"name\":\"Facebook\",\"description\":\"Facebook OpenGraph Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"opengraph\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContainertwitter\":{\"data\":[],\"name\":\"Twitter\",\"description\":\"Twitter Card Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"twitter\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContainermiscellaneous\":{\"data\":[],\"name\":\"Miscellaneous\",\"description\":\"Miscellaneous Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"miscellaneous\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaLinkContainergeneral\":{\"data\":[],\"name\":\"General\",\"description\":\"Link Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaLinkContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaScriptContainergeneral\":{\"data\":[],\"position\":1,\"name\":\"General\",\"description\":\"Script Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaScriptContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaJsonLdContainergeneral\":{\"data\":{\"mainEntityOfPage\":{\"breadcrumb\":null,\"lastReviewed\":null,\"mainContentOfPage\":null,\"primaryImageOfPage\":null,\"relatedLink\":null,\"reviewedBy\":null,\"significantLink\":null,\"speakable\":null,\"specialty\":null,\"about\":null,\"accessMode\":null,\"accessModeSufficient\":null,\"accessibilityAPI\":null,\"accessibilityControl\":null,\"accessibilityFeature\":null,\"accessibilityHazard\":null,\"accessibilitySummary\":null,\"accountablePerson\":null,\"aggregateRating\":null,\"alternativeHeadline\":null,\"associatedMedia\":null,\"audience\":null,\"audio\":null,\"author\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"award\":null,\"character\":null,\"citation\":null,\"comment\":null,\"commentCount\":null,\"contentLocation\":null,\"contentRating\":null,\"contentReferenceTime\":null,\"contributor\":null,\"copyrightHolder\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"copyrightYear\":\"{entry.postDate |atom}\",\"creator\":{\"id\":\"{seomatic.site.identity.genericUrl}#creator\"},\"dateCreated\":false,\"dateModified\":\"{entry.dateUpdated |atom}\",\"datePublished\":\"{entry.postDate |atom}\",\"discussionUrl\":null,\"editor\":null,\"educationalAlignment\":null,\"educationalUse\":null,\"encoding\":null,\"exampleOfWork\":null,\"expires\":null,\"fileFormat\":null,\"funder\":null,\"genre\":null,\"hasPart\":null,\"headline\":\"{seomatic.meta.seoTitle}\",\"inLanguage\":\"{seomatic.meta.language}\",\"interactionStatistic\":null,\"interactivityType\":null,\"isAccessibleForFree\":null,\"isBasedOn\":null,\"isFamilyFriendly\":null,\"isPartOf\":null,\"keywords\":null,\"learningResourceType\":null,\"license\":null,\"locationCreated\":null,\"mainEntity\":null,\"material\":null,\"mentions\":null,\"offers\":null,\"position\":null,\"producer\":null,\"provider\":null,\"publication\":null,\"publisher\":{\"id\":\"{seomatic.site.identity.genericUrl}#creator\"},\"publisherImprint\":null,\"publishingPrinciples\":null,\"recordedAt\":null,\"releasedEvent\":null,\"review\":null,\"schemaVersion\":null,\"sourceOrganization\":null,\"spatialCoverage\":null,\"sponsor\":null,\"temporalCoverage\":null,\"text\":null,\"thumbnailUrl\":null,\"timeRequired\":null,\"translationOfWork\":null,\"translator\":null,\"typicalAgeRange\":null,\"version\":null,\"video\":null,\"workExample\":null,\"workTranslation\":null,\"additionalType\":null,\"alternateName\":null,\"description\":\"{seomatic.meta.seoDescription}\",\"disambiguatingDescription\":null,\"identifier\":null,\"image\":{\"type\":\"ImageObject\",\"url\":\"{seomatic.meta.seoImage}\"},\"mainEntityOfPage\":\"{seomatic.meta.canonicalUrl}\",\"name\":\"{seomatic.meta.seoTitle}\",\"potentialAction\":{\"type\":\"SearchAction\",\"target\":\"{seomatic.site.siteLinksSearchTarget}\",\"query-input\":\"{seomatic.helper.siteLinksQueryInput()}\"},\"sameAs\":null,\"url\":\"{seomatic.meta.canonicalUrl}\",\"context\":\"http://schema.org\",\"type\":\"{seomatic.meta.mainEntityOfPage}\",\"id\":null,\"include\":true,\"key\":\"mainEntityOfPage\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"JsonLd Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaJsonLdContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTitleContainergeneral\":{\"data\":{\"title\":{\"title\":\"{seomatic.meta.seoTitle}\",\"siteName\":\"{seomatic.site.siteName}\",\"siteNamePosition\":\"{seomatic.meta.siteNamePosition}\",\"separatorChar\":\"{seomatic.config.separatorChar}\",\"include\":true,\"key\":\"title\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"Meta Title Tag\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTitleContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false}}','[]','{\"data\":[],\"name\":null,\"description\":null,\"class\":\"nystudio107\\\\seomatic\\\\models\\\\FrontendTemplateContainer\",\"handle\":null,\"include\":true,\"dependencies\":null,\"clearCache\":false}','{\"siteType\":\"CreativeWork\",\"siteSubType\":\"WebPage\",\"siteSpecificType\":\"\",\"seoTitleSource\":\"fromField\",\"seoTitleField\":\"title\",\"siteNamePositionSource\":\"sameAsGlobal\",\"seoDescriptionSource\":\"fromCustom\",\"seoDescriptionField\":\"\",\"seoKeywordsSource\":\"fromCustom\",\"seoKeywordsField\":\"\",\"seoImageIds\":[],\"seoImageSource\":\"fromAsset\",\"seoImageField\":\"\",\"seoImageTransform\":true,\"seoImageTransformMode\":\"crop\",\"seoImageDescriptionSource\":\"fromCustom\",\"seoImageDescriptionField\":\"\",\"twitterCreatorSource\":\"sameAsSite\",\"twitterCreatorField\":\"\",\"twitterTitleSource\":\"sameAsSeo\",\"twitterTitleField\":\"\",\"twitterSiteNamePositionSource\":\"sameAsGlobal\",\"twitterDescriptionSource\":\"sameAsSeo\",\"twitterDescriptionField\":\"\",\"twitterImageIds\":[],\"twitterImageSource\":\"sameAsSeo\",\"twitterImageField\":\"\",\"twitterImageTransform\":true,\"twitterImageTransformMode\":\"crop\",\"twitterImageDescriptionSource\":\"sameAsSeo\",\"twitterImageDescriptionField\":\"\",\"ogTitleSource\":\"sameAsSeo\",\"ogTitleField\":\"\",\"ogSiteNamePositionSource\":\"sameAsGlobal\",\"ogDescriptionSource\":\"sameAsSeo\",\"ogDescriptionField\":\"\",\"ogImageIds\":[],\"ogImageSource\":\"sameAsSeo\",\"ogImageField\":\"\",\"ogImageTransform\":true,\"ogImageTransformMode\":\"crop\",\"ogImageDescriptionSource\":\"sameAsSeo\",\"ogImageDescriptionField\":\"\"}'),
	(7,'2019-01-30 21:25:15','2019-01-30 23:36:48','78375f7c-1207-431c-a18f-38a88391d6d2','1.0.25','categorygroup',1,'Blog','blog','category','blog/_categories.twig',1,'{\"1\":{\"id\":1,\"groupId\":1,\"siteId\":1,\"hasUrls\":1,\"uriFormat\":\"blog/{slug}\",\"template\":\"blog/_categories.twig\",\"language\":\"en-us\"}}','2019-01-30 23:36:48','{\"language\":null,\"mainEntityOfPage\":\"WebPage\",\"seoTitle\":\"{category.title}\",\"siteNamePosition\":\"\",\"seoDescription\":\"\",\"seoKeywords\":\"\",\"seoImage\":\"\",\"seoImageWidth\":\"\",\"seoImageHeight\":\"\",\"seoImageDescription\":\"\",\"canonicalUrl\":\"{category.url}\",\"robots\":\"all\",\"ogType\":\"website\",\"ogTitle\":\"{seomatic.meta.seoTitle}\",\"ogSiteNamePosition\":\"\",\"ogDescription\":\"{seomatic.meta.seoDescription}\",\"ogImage\":\"{seomatic.meta.seoImage}\",\"ogImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"ogImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"ogImageDescription\":\"{seomatic.meta.seoImageDescription}\",\"twitterCard\":\"summary_large_image\",\"twitterCreator\":\"{seomatic.site.twitterHandle}\",\"twitterTitle\":\"{seomatic.meta.seoTitle}\",\"twitterSiteNamePosition\":\"\",\"twitterDescription\":\"{seomatic.meta.seoDescription}\",\"twitterImage\":\"{seomatic.meta.seoImage}\",\"twitterImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"twitterImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"twitterImageDescription\":\"{seomatic.meta.seoImageDescription}\"}','{\"siteName\":\"Project Heart\",\"identity\":null,\"creator\":null,\"twitterHandle\":\"\",\"facebookProfileId\":\"\",\"facebookAppId\":\"\",\"googleSiteVerification\":\"\",\"bingSiteVerification\":\"\",\"pinterestSiteVerification\":\"\",\"sameAsLinks\":[],\"siteLinksSearchTarget\":\"\",\"siteLinksQueryInput\":\"\",\"additionalSitemapUrls\":[],\"additionalSitemapUrlsDateUpdated\":null,\"additionalSitemaps\":[]}','{\"sitemapUrls\":true,\"sitemapAssets\":true,\"sitemapFiles\":true,\"sitemapAltLinks\":true,\"sitemapChangeFreq\":\"weekly\",\"sitemapPriority\":0.5,\"sitemapLimit\":null,\"structureDepth\":null,\"sitemapImageFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"caption\",\"field\":\"\"},{\"property\":\"geo_location\",\"field\":\"\"},{\"property\":\"license\",\"field\":\"\"}],\"sitemapVideoFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"description\",\"field\":\"\"},{\"property\":\"thumbnailLoc\",\"field\":\"\"},{\"property\":\"duration\",\"field\":\"\"},{\"property\":\"category\",\"field\":\"\"}]}','{\"MetaTagContainergeneral\":{\"data\":[],\"name\":\"General\",\"description\":\"General Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContaineropengraph\":{\"data\":[],\"name\":\"Facebook\",\"description\":\"Facebook OpenGraph Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"opengraph\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContainertwitter\":{\"data\":[],\"name\":\"Twitter\",\"description\":\"Twitter Card Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"twitter\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContainermiscellaneous\":{\"data\":[],\"name\":\"Miscellaneous\",\"description\":\"Miscellaneous Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"miscellaneous\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaLinkContainergeneral\":{\"data\":[],\"name\":\"General\",\"description\":\"Link Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaLinkContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaScriptContainergeneral\":{\"data\":[],\"position\":1,\"name\":\"General\",\"description\":\"Script Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaScriptContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaJsonLdContainergeneral\":{\"data\":{\"mainEntityOfPage\":{\"breadcrumb\":null,\"lastReviewed\":null,\"mainContentOfPage\":null,\"primaryImageOfPage\":null,\"relatedLink\":null,\"reviewedBy\":null,\"significantLink\":null,\"speakable\":null,\"specialty\":null,\"about\":null,\"accessMode\":null,\"accessModeSufficient\":null,\"accessibilityAPI\":null,\"accessibilityControl\":null,\"accessibilityFeature\":null,\"accessibilityHazard\":null,\"accessibilitySummary\":null,\"accountablePerson\":null,\"aggregateRating\":null,\"alternativeHeadline\":null,\"associatedMedia\":null,\"audience\":null,\"audio\":null,\"author\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"award\":null,\"character\":null,\"citation\":null,\"comment\":null,\"commentCount\":null,\"contentLocation\":null,\"contentRating\":null,\"contentReferenceTime\":null,\"contributor\":null,\"copyrightHolder\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"copyrightYear\":\"{category.postDate |atom}\",\"creator\":{\"id\":\"{seomatic.site.identity.genericUrl}#creator\"},\"dateCreated\":false,\"dateModified\":\"{category.dateUpdated |atom}\",\"datePublished\":\"{category.postDate |atom}\",\"discussionUrl\":null,\"editor\":null,\"educationalAlignment\":null,\"educationalUse\":null,\"encoding\":null,\"exampleOfWork\":null,\"expires\":null,\"fileFormat\":null,\"funder\":null,\"genre\":null,\"hasPart\":null,\"headline\":\"{seomatic.meta.seoTitle}\",\"inLanguage\":\"{seomatic.meta.language}\",\"interactionStatistic\":null,\"interactivityType\":null,\"isAccessibleForFree\":null,\"isBasedOn\":null,\"isFamilyFriendly\":null,\"isPartOf\":null,\"keywords\":null,\"learningResourceType\":null,\"license\":null,\"locationCreated\":null,\"mainEntity\":null,\"material\":null,\"mentions\":null,\"offers\":null,\"position\":null,\"producer\":null,\"provider\":null,\"publication\":null,\"publisher\":{\"id\":\"{seomatic.site.identity.genericUrl}#creator\"},\"publisherImprint\":null,\"publishingPrinciples\":null,\"recordedAt\":null,\"releasedEvent\":null,\"review\":null,\"schemaVersion\":null,\"sourceOrganization\":null,\"spatialCoverage\":null,\"sponsor\":null,\"temporalCoverage\":null,\"text\":null,\"thumbnailUrl\":null,\"timeRequired\":null,\"translationOfWork\":null,\"translator\":null,\"typicalAgeRange\":null,\"version\":null,\"video\":null,\"workExample\":null,\"workTranslation\":null,\"additionalType\":null,\"alternateName\":null,\"description\":\"{seomatic.meta.seoDescription}\",\"disambiguatingDescription\":null,\"identifier\":null,\"image\":{\"type\":\"ImageObject\",\"url\":\"{seomatic.meta.seoImage}\"},\"mainEntityOfPage\":\"{seomatic.meta.canonicalUrl}\",\"name\":\"{seomatic.meta.seoTitle}\",\"potentialAction\":{\"type\":\"SearchAction\",\"target\":\"{seomatic.site.siteLinksSearchTarget}\",\"query-input\":\"{seomatic.helper.siteLinksQueryInput()}\"},\"sameAs\":null,\"url\":\"{seomatic.meta.canonicalUrl}\",\"context\":\"http://schema.org\",\"type\":\"{seomatic.meta.mainEntityOfPage}\",\"id\":null,\"include\":true,\"key\":\"mainEntityOfPage\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"JsonLd Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaJsonLdContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTitleContainergeneral\":{\"data\":{\"title\":{\"title\":\"{seomatic.meta.seoTitle}\",\"siteName\":\"{seomatic.site.siteName}\",\"siteNamePosition\":\"{seomatic.meta.siteNamePosition}\",\"separatorChar\":\"{seomatic.config.separatorChar}\",\"include\":true,\"key\":\"title\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"Meta Title Tag\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTitleContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false}}','[]','{\"data\":[],\"name\":null,\"description\":null,\"class\":\"nystudio107\\\\seomatic\\\\models\\\\FrontendTemplateContainer\",\"handle\":null,\"include\":true,\"dependencies\":null,\"clearCache\":false}','{\"siteType\":\"CreativeWork\",\"siteSubType\":\"WebPage\",\"siteSpecificType\":\"\",\"seoTitleSource\":\"fromField\",\"seoTitleField\":\"title\",\"siteNamePositionSource\":\"sameAsGlobal\",\"seoDescriptionSource\":\"fromCustom\",\"seoDescriptionField\":\"\",\"seoKeywordsSource\":\"fromCustom\",\"seoKeywordsField\":\"\",\"seoImageIds\":[],\"seoImageSource\":\"fromAsset\",\"seoImageField\":\"\",\"seoImageTransform\":true,\"seoImageTransformMode\":\"crop\",\"seoImageDescriptionSource\":\"fromCustom\",\"seoImageDescriptionField\":\"\",\"twitterCreatorSource\":\"sameAsSite\",\"twitterCreatorField\":\"\",\"twitterTitleSource\":\"sameAsSeo\",\"twitterTitleField\":\"\",\"twitterSiteNamePositionSource\":\"sameAsGlobal\",\"twitterDescriptionSource\":\"sameAsSeo\",\"twitterDescriptionField\":\"\",\"twitterImageIds\":[],\"twitterImageSource\":\"sameAsSeo\",\"twitterImageField\":\"\",\"twitterImageTransform\":true,\"twitterImageTransformMode\":\"crop\",\"twitterImageDescriptionSource\":\"sameAsSeo\",\"twitterImageDescriptionField\":\"\",\"ogTitleSource\":\"sameAsSeo\",\"ogTitleField\":\"\",\"ogSiteNamePositionSource\":\"sameAsGlobal\",\"ogDescriptionSource\":\"sameAsSeo\",\"ogDescriptionField\":\"\",\"ogImageIds\":[],\"ogImageSource\":\"sameAsSeo\",\"ogImageField\":\"\",\"ogImageTransform\":true,\"ogImageTransformMode\":\"crop\",\"ogImageDescriptionSource\":\"sameAsSeo\",\"ogImageDescriptionField\":\"\"}'),
	(8,'2019-01-30 23:34:45','2019-02-14 22:31:51','ffdcae3e-26af-4c17-825c-5bad10a13cc7','1.0.25','categorygroup',2,'Quote','quote','category','quote/categories.twig',1,'{\"1\":{\"id\":2,\"groupId\":2,\"siteId\":1,\"hasUrls\":1,\"uriFormat\":\"quote/{slug}\",\"template\":\"quote/categories.twig\",\"language\":\"en-us\"}}','2019-02-14 22:31:33','{\"language\":null,\"mainEntityOfPage\":\"WebPage\",\"seoTitle\":\"{category.title}\",\"siteNamePosition\":\"\",\"seoDescription\":\"\",\"seoKeywords\":\"\",\"seoImage\":\"\",\"seoImageWidth\":\"\",\"seoImageHeight\":\"\",\"seoImageDescription\":\"\",\"canonicalUrl\":\"{category.url}\",\"robots\":\"all\",\"ogType\":\"website\",\"ogTitle\":\"{seomatic.meta.seoTitle}\",\"ogSiteNamePosition\":\"\",\"ogDescription\":\"{seomatic.meta.seoDescription}\",\"ogImage\":\"{seomatic.meta.seoImage}\",\"ogImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"ogImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"ogImageDescription\":\"{seomatic.meta.seoImageDescription}\",\"twitterCard\":\"summary_large_image\",\"twitterCreator\":\"{seomatic.site.twitterHandle}\",\"twitterTitle\":\"{seomatic.meta.seoTitle}\",\"twitterSiteNamePosition\":\"\",\"twitterDescription\":\"{seomatic.meta.seoDescription}\",\"twitterImage\":\"{seomatic.meta.seoImage}\",\"twitterImageWidth\":\"{seomatic.meta.seoImageWidth}\",\"twitterImageHeight\":\"{seomatic.meta.seoImageHeight}\",\"twitterImageDescription\":\"{seomatic.meta.seoImageDescription}\"}','{\"siteName\":\"Project Heart\",\"identity\":null,\"creator\":null,\"twitterHandle\":\"\",\"facebookProfileId\":\"\",\"facebookAppId\":\"\",\"googleSiteVerification\":\"\",\"bingSiteVerification\":\"\",\"pinterestSiteVerification\":\"\",\"sameAsLinks\":[],\"siteLinksSearchTarget\":\"\",\"siteLinksQueryInput\":\"\",\"additionalSitemapUrls\":[],\"additionalSitemapUrlsDateUpdated\":null,\"additionalSitemaps\":[]}','{\"sitemapUrls\":true,\"sitemapAssets\":true,\"sitemapFiles\":true,\"sitemapAltLinks\":true,\"sitemapChangeFreq\":\"weekly\",\"sitemapPriority\":0.5,\"sitemapLimit\":null,\"structureDepth\":null,\"sitemapImageFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"caption\",\"field\":\"\"},{\"property\":\"geo_location\",\"field\":\"\"},{\"property\":\"license\",\"field\":\"\"}],\"sitemapVideoFieldMap\":[{\"property\":\"title\",\"field\":\"title\"},{\"property\":\"description\",\"field\":\"\"},{\"property\":\"thumbnailLoc\",\"field\":\"\"},{\"property\":\"duration\",\"field\":\"\"},{\"property\":\"category\",\"field\":\"\"}]}','{\"MetaTagContainergeneral\":{\"data\":[],\"name\":\"General\",\"description\":\"General Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContaineropengraph\":{\"data\":[],\"name\":\"Facebook\",\"description\":\"Facebook OpenGraph Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"opengraph\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContainertwitter\":{\"data\":[],\"name\":\"Twitter\",\"description\":\"Twitter Card Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"twitter\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTagContainermiscellaneous\":{\"data\":[],\"name\":\"Miscellaneous\",\"description\":\"Miscellaneous Meta Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTagContainer\",\"handle\":\"miscellaneous\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaLinkContainergeneral\":{\"data\":[],\"name\":\"General\",\"description\":\"Link Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaLinkContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaScriptContainergeneral\":{\"data\":[],\"position\":1,\"name\":\"General\",\"description\":\"Script Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaScriptContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaJsonLdContainergeneral\":{\"data\":{\"mainEntityOfPage\":{\"breadcrumb\":null,\"lastReviewed\":null,\"mainContentOfPage\":null,\"primaryImageOfPage\":null,\"relatedLink\":null,\"reviewedBy\":null,\"significantLink\":null,\"speakable\":null,\"specialty\":null,\"about\":null,\"accessMode\":null,\"accessModeSufficient\":null,\"accessibilityAPI\":null,\"accessibilityControl\":null,\"accessibilityFeature\":null,\"accessibilityHazard\":null,\"accessibilitySummary\":null,\"accountablePerson\":null,\"aggregateRating\":null,\"alternativeHeadline\":null,\"associatedMedia\":null,\"audience\":null,\"audio\":null,\"author\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"award\":null,\"character\":null,\"citation\":null,\"comment\":null,\"commentCount\":null,\"contentLocation\":null,\"contentRating\":null,\"contentReferenceTime\":null,\"contributor\":null,\"copyrightHolder\":{\"id\":\"{seomatic.site.identity.genericUrl}#identity\"},\"copyrightYear\":\"{category.postDate |atom}\",\"creator\":{\"id\":\"{seomatic.site.identity.genericUrl}#creator\"},\"dateCreated\":false,\"dateModified\":\"{category.dateUpdated |atom}\",\"datePublished\":\"{category.postDate |atom}\",\"discussionUrl\":null,\"editor\":null,\"educationalAlignment\":null,\"educationalUse\":null,\"encoding\":null,\"exampleOfWork\":null,\"expires\":null,\"fileFormat\":null,\"funder\":null,\"genre\":null,\"hasPart\":null,\"headline\":\"{seomatic.meta.seoTitle}\",\"inLanguage\":\"{seomatic.meta.language}\",\"interactionStatistic\":null,\"interactivityType\":null,\"isAccessibleForFree\":null,\"isBasedOn\":null,\"isFamilyFriendly\":null,\"isPartOf\":null,\"keywords\":null,\"learningResourceType\":null,\"license\":null,\"locationCreated\":null,\"mainEntity\":null,\"material\":null,\"mentions\":null,\"offers\":null,\"position\":null,\"producer\":null,\"provider\":null,\"publication\":null,\"publisher\":{\"id\":\"{seomatic.site.identity.genericUrl}#creator\"},\"publisherImprint\":null,\"publishingPrinciples\":null,\"recordedAt\":null,\"releasedEvent\":null,\"review\":null,\"schemaVersion\":null,\"sourceOrganization\":null,\"spatialCoverage\":null,\"sponsor\":null,\"temporalCoverage\":null,\"text\":null,\"thumbnailUrl\":null,\"timeRequired\":null,\"translationOfWork\":null,\"translator\":null,\"typicalAgeRange\":null,\"version\":null,\"video\":null,\"workExample\":null,\"workTranslation\":null,\"additionalType\":null,\"alternateName\":null,\"description\":\"{seomatic.meta.seoDescription}\",\"disambiguatingDescription\":null,\"identifier\":null,\"image\":{\"type\":\"ImageObject\",\"url\":\"{seomatic.meta.seoImage}\"},\"mainEntityOfPage\":\"{seomatic.meta.canonicalUrl}\",\"name\":\"{seomatic.meta.seoTitle}\",\"potentialAction\":{\"type\":\"SearchAction\",\"target\":\"{seomatic.site.siteLinksSearchTarget}\",\"query-input\":\"{seomatic.helper.siteLinksQueryInput()}\"},\"sameAs\":null,\"url\":\"{seomatic.meta.canonicalUrl}\",\"context\":\"http://schema.org\",\"type\":\"{seomatic.meta.mainEntityOfPage}\",\"id\":null,\"include\":true,\"key\":\"mainEntityOfPage\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"JsonLd Tags\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaJsonLdContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false},\"MetaTitleContainergeneral\":{\"data\":{\"title\":{\"title\":\"{seomatic.meta.seoTitle}\",\"siteName\":\"{seomatic.site.siteName}\",\"siteNamePosition\":\"{seomatic.meta.siteNamePosition}\",\"separatorChar\":\"{seomatic.config.separatorChar}\",\"include\":true,\"key\":\"title\",\"environment\":null,\"dependencies\":null}},\"name\":\"General\",\"description\":\"Meta Title Tag\",\"class\":\"nystudio107\\\\seomatic\\\\models\\\\MetaTitleContainer\",\"handle\":\"general\",\"include\":true,\"dependencies\":[],\"clearCache\":false}}','[]','{\"data\":[],\"name\":null,\"description\":null,\"class\":\"nystudio107\\\\seomatic\\\\models\\\\FrontendTemplateContainer\",\"handle\":null,\"include\":true,\"dependencies\":null,\"clearCache\":false}','{\"siteType\":\"CreativeWork\",\"siteSubType\":\"WebPage\",\"siteSpecificType\":\"\",\"seoTitleSource\":\"fromField\",\"seoTitleField\":\"title\",\"siteNamePositionSource\":\"sameAsGlobal\",\"seoDescriptionSource\":\"fromCustom\",\"seoDescriptionField\":\"\",\"seoKeywordsSource\":\"fromCustom\",\"seoKeywordsField\":\"\",\"seoImageIds\":[],\"seoImageSource\":\"fromAsset\",\"seoImageField\":\"\",\"seoImageTransform\":true,\"seoImageTransformMode\":\"crop\",\"seoImageDescriptionSource\":\"fromCustom\",\"seoImageDescriptionField\":\"\",\"twitterCreatorSource\":\"sameAsSite\",\"twitterCreatorField\":\"\",\"twitterTitleSource\":\"sameAsSeo\",\"twitterTitleField\":\"\",\"twitterSiteNamePositionSource\":\"sameAsGlobal\",\"twitterDescriptionSource\":\"sameAsSeo\",\"twitterDescriptionField\":\"\",\"twitterImageIds\":[],\"twitterImageSource\":\"sameAsSeo\",\"twitterImageField\":\"\",\"twitterImageTransform\":true,\"twitterImageTransformMode\":\"crop\",\"twitterImageDescriptionSource\":\"sameAsSeo\",\"twitterImageDescriptionField\":\"\",\"ogTitleSource\":\"sameAsSeo\",\"ogTitleField\":\"\",\"ogSiteNamePositionSource\":\"sameAsGlobal\",\"ogDescriptionSource\":\"sameAsSeo\",\"ogDescriptionField\":\"\",\"ogImageIds\":[],\"ogImageSource\":\"sameAsSeo\",\"ogImageField\":\"\",\"ogImageTransform\":true,\"ogImageTransformMode\":\"crop\",\"ogImageDescriptionSource\":\"sameAsSeo\",\"ogImageDescriptionField\":\"\"}');

/*!40000 ALTER TABLE `craft_seomatic_metabundles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_sequences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sequences`;

CREATE TABLE `craft_sequences` (
  `name` varchar(255) NOT NULL,
  `next` int(11) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sessions`;

CREATE TABLE `craft_sessions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `token` char(100) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_sessions_uid_idx` (`uid`),
  KEY `craft_sessions_token_idx` (`token`),
  KEY `craft_sessions_dateUpdated_idx` (`dateUpdated`),
  KEY `craft_sessions_userId_idx` (`userId`),
  CONSTRAINT `craft_sessions_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_sessions` WRITE;
/*!40000 ALTER TABLE `craft_sessions` DISABLE KEYS */;

INSERT INTO `craft_sessions` (`id`, `userId`, `token`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'qJsWi-c9FRh5rznegV1w_n8Js6jimZOfPEo-oDUrg13KvnEHuwX2_RC7MSfF0v-NYxjGOH7oU0sgjeCfBWrnsHw11c47c48599un','2019-01-21 23:30:49','2019-01-21 23:48:52','b4bbe582-23db-49b5-8124-3a5c8441e424'),
	(2,1,'AZeucu0Ny3v2N4_BS4sjHNQzh522uj_qUkQV0P4MthD4QipZHAVYPYvSVCzWFdpqcw5CcFk-wgfDadre3powTUxecWA2K9Gm7Hak','2019-01-22 00:49:17','2019-01-22 00:49:17','e8c7bcb3-9875-40c0-a005-85ebf9ba0821'),
	(3,1,'_yZ7rGYcA_QUej1CJLtBpGalkDlcVKeLiqVe6b650OUeEq_8RuI6D9SkF8KKFBRORnvmbgUZdmsOHiFnDbix3iCC22iJDJ8XaTTZ','2019-01-22 01:50:09','2019-01-22 01:50:09','c3e7ee49-a2a4-4595-8f43-b0addce3e825'),
	(4,1,'KkU-XU6JltaF5FqqWV_BGbb2YpCGAWjov_DsjOyKpzLXchzBBIr4hydNOpUwfdaeIg7ZyybBE2C7-b6wslwb0n8H-7DdHFuOdKd-','2019-01-22 02:50:36','2019-01-22 02:50:36','f8635357-0370-46d3-85c0-8e25e57803c9'),
	(5,1,'gyBaPqGNkarEv2cDV13zeDFld5BbKGIz5W_2mxYlClJI3ecV46AlQ9iSZtx-0gbdAzZUM9Hg3pN8Dp95yx0MX0WLJqsKFZJL8tq9','2019-01-22 03:51:27','2019-01-22 03:51:27','27da3d68-f882-4ab0-8f49-ded81b0db4fe'),
	(6,1,'vy0q7BNUiBvM1UCqkt4EfXxaQIRiriuiP9u4EFszyu8UYnx-gzdWn96tw8Ng9rbO1fJ3Ce80ofqwWxauCw0K_j0aYbv_EtUAF-6D','2019-01-22 04:52:02','2019-01-22 04:52:02','2fca872b-3334-4013-8fd0-5e928b8f068c'),
	(7,1,'W9g02D2x44EtIINEjZjhDEDOM3ir2Iq9kiIg-nQ5WFIOMh-DDpoQMpNv8iixzPue-XIkdi0w-yFKwFeMcc0gUwsd_Vk1amt8MQQv','2019-01-22 05:52:46','2019-01-22 05:52:46','3f22d88b-20e6-4f3d-94b2-b9aada9b3da5'),
	(8,1,'84XwUN1oc4c-X_pm7XsFNqN_WPP87JzNMQwLXAQS7tUlW2Co2SO7Ag03lhVR_J2M7FB7HLA-o5NuZDrBDA4qxToPwKtvBckqgqte','2019-01-22 06:53:20','2019-01-22 06:53:20','7dd6d896-27c7-42de-a9f4-693bbf3a1956'),
	(9,1,'AalJD6WJLIWfYoS67mkCIhafOyzNGQjCvUjxR6nIuOW3p9bpoS1TbcHnsTxGO6CcODnJvS82q2LqA-eUIEMddCuPcA_c7RhRVAKL','2019-01-22 07:53:41','2019-01-22 07:53:41','51976da0-e98a-4581-ae7f-14b1a8a9a3eb'),
	(10,1,'wBZ1A6l3YwCEVKT-OV3ZsgkIGyTD9D2xkm42l02Kp4IwOK_4HVluNlCMzRt3XUoBVlN60FuGQy9mHo7k5Ny2vfzDYXRCWSeAtDXS','2019-01-22 08:54:34','2019-01-22 08:54:34','7a40d801-0398-4c2b-82df-70d1f52e1725'),
	(11,1,'ukniAzIv37P5phBqyKP4vNd2zgb-c3GQPmMgKIMGmrzwOX9j_XALKCaDNPTA2mQ2nkhqBzPg8nNJ9KPBgKTJ8qt4V3191u1cWkiG','2019-01-22 09:55:19','2019-01-22 09:55:19','81c3861f-433b-43ed-b54e-d78a2d45077c'),
	(12,1,'SyDOqQ-j86rDYqZgcUw9Ejh5Cg_YPwXusHZ1TkUt8w3UY0QuuVfGbPvt86mmUR2dysU390QN1nVjTpZIiTvrJMHAJSD7VDPTP7w5','2019-01-22 10:56:03','2019-01-22 10:56:03','5f8a155c-79b6-4328-a8b8-38b43eee7a17'),
	(13,1,'8np22NO5xFbt2G7NQaq0D16APR--0yQ5Z8JXyg-GkuSH2qOzkkiFppSHpa1rJxShNrP-UmgcXOxVtuIpXZGdWcTlV3xm77Ah9S8x','2019-01-22 11:56:29','2019-01-22 11:56:29','28de612a-b17a-4aa1-9f25-be97826cdf66'),
	(14,1,'GXvp_ehdoK7q2UdWxlIVhZh7GdE1k9YudPGYHyW8MLVtEZK4zTBTc5ZPxNqAI0gV43nQRZzcMfXZo48BkVOdZz8GWuiQmRtiZaom','2019-01-22 12:56:54','2019-01-22 12:56:54','586b8121-cbbf-451d-8001-2dcbb6498268'),
	(15,1,'q9VukN24As2lBa8hcXiVjAyM51QXItQKIqK3v7bErx-0lADRxPvGAxmFyNT9ZAb9XDfw8IUgF55Vt_hytAN27WaPLunI_EprBY0u','2019-01-22 13:57:06','2019-01-22 13:57:06','99f9375a-b2da-4b63-9646-606d718dbd3b'),
	(16,1,'6pzWNzsUDOioOrTXPGiLfm5aR69NFkc64Karaa3oQwUNo26U1YKvbcG6QtWgFHGN2-CuJ9ZDfVrHEwnF6l2xV8n5QNRZfzCrNrY1','2019-01-22 14:57:22','2019-01-22 16:12:13','54d350bb-a1ea-4692-9ee8-88d5b0fa7913'),
	(17,1,'hMWr7P4GG-wEW--OVrNMzEbwXW9XOqTSZ2LCG7mAluth0daeqMHtUGivSte68F6KXffFtgxO-XSxiVZwzaTsl151S8fRwGOxpPxi','2019-01-30 21:10:50','2019-01-30 23:51:12','29f6dc24-1a76-41d1-85bf-79fe38fea87f'),
	(18,1,'qj5ptlUWDKlZAhE5Cbb64hgDDYvh1Q8sHa_jDQJkGjsMn-UggUSZCuQ5xm6mValmTCBOvAX1VP6D-P3hHCJpZVI2_dgxr_-xfrPX','2019-01-31 15:15:16','2019-01-31 16:46:06','49f2e875-8364-47b1-8ac7-4f4588bab671'),
	(19,1,'nhdvhvw5BThUNlTrT7MaVkw2SWMwMTALnSj2DqZ_-dYEhHEoXg4q6qB3agXVgDhA0rvwXlmqUuSwfEkfQjeCDACTCx8j_2iI1PLc','2019-01-31 17:46:10','2019-01-31 17:46:10','3a6ef84d-7358-43f1-b92a-b27107f7a5bd'),
	(20,1,'I4aHiiwXaMXEzbmQkWyjGy2O7AW42r8J-px4AeN4xuAhOx-PoM-C31x1fYp7os63r4Smi4P6OQAGh6L5OxSgedXlp2AIDzQG-EgC','2019-01-31 18:46:14','2019-01-31 18:46:14','d44da1ce-1c1d-4fd5-ba8d-929051ce6c71'),
	(21,1,'ybRbjb7ctpBkkQBYZ2c5nPloWU53PKge1EG56Jn28of0cjXMsaq6eD9w5tpOuDiJIe_Z2zO1o8V-JRpsoz4bF4FwUEOdLR4wDW4O','2019-01-31 19:46:19','2019-01-31 19:46:19','0ec8141a-53c2-4dc7-add6-bf2ac0e32c1b'),
	(22,1,'4RmAKcElMnfek2zsEfD642u66919jTjC87MLkqZVrk2PgcwEkAB3CX0PqG2sI03id0aFPIlEWgwH5aHAZf_eyQTUQ5lUvUpJlJy8','2019-01-31 19:46:19','2019-01-31 23:40:49','e6e4d4aa-4b6a-4c44-b9a5-169c0be91f8c'),
	(23,1,'NcgwCo7Xg-L8AjJkFBgKg5BD43i8EOrnGXLeT0z9rYvOxiaC63QFPZxxrmWv8uFa-ueS-qmT1bIfRhJw8RCCBobc2quXkPyeCcpP','2019-02-01 00:40:53','2019-02-01 00:40:53','a82de254-ae0e-45de-b526-70aedaef3d21'),
	(24,1,'qXFhToLl_DMnmhsm-raajRzALwEULbngoYT7DYivgb8EKbrD4FXYgQvuy4NZmk63bpZDcSLcaf19C_6o_v3Q2l06u0TxhlIXeEMo','2019-02-01 00:40:54','2019-02-01 00:40:54','0445b310-c6d8-414d-b7b1-855ec336bf96'),
	(25,1,'I9ZMOf4eHPX18_pv7UYH55O1BvnPuYuUhsAHRYrbTivnPQSv1m1lqf2QvfHz09I1AsDfoJOE8wXdZtbMfV3WuuKSDVy9nYE5LuDX','2019-02-01 01:40:58','2019-02-01 01:40:58','1adf9186-1b04-48e6-ae85-ae61b85b56c3'),
	(26,1,'_8yxNaLfn-jdmcjahN_djzsrMWVrknv75oUqzt66ZyIp2fBMhdExBtGiCwyprypkIlrwKSh_quIvgGb9Xc6DdlHVgTkqyQuClEfq','2019-02-01 01:40:59','2019-02-01 01:40:59','5eb62669-36da-4fd9-a6c6-a59fedc97050'),
	(27,1,'tx_5IqLgJhVkDlpU7-zrvuAh9SKnGiup5bSX24-OOIKPdgyefAaoB32uyw9tlRHvWd6HsDYDkD6h3HDve0LxFGXRemx9lGpBfFdz','2019-02-01 02:41:02','2019-02-01 02:41:02','975c262f-a88a-4432-b8fa-358dad433254'),
	(28,1,'jVSdj-8GrPgElWmacd8SDH-1gKoNudgfH6I3r1niF5m-L1nmRFlltMyhVjXKQ_VLhHNapi-SfDdmw0qOAJcHazEWVI_1TrUH78Wh','2019-02-01 03:41:06','2019-02-01 03:41:06','7a70fc82-cb7d-4777-947a-a239f74c8f46'),
	(29,1,'deJ-TH9uM_ttAsvUyQLqpghAT8B3WLG6y1Oc62XCS3J12Uhn8L5iKCpXIu68lreIEc7Y98nrLDYTW0zL0dXKL4LX-BUPa9AHHfgV','2019-02-01 03:41:06','2019-02-01 03:41:06','544722a2-a302-4a35-8827-8a13b65c8c6d'),
	(30,1,'r0p3P7nsz05S32skw0kukGCXxqSeD5CqdzDoHSHIeXyxzX5iuSMBxObXSw9Y0a16gpnHUdXP3qk39Ohk9yHY0mI_6_QvnBfYlKRo','2019-02-01 04:41:10','2019-02-01 04:41:10','101bc370-06bb-4e71-aaee-8da82a91f897'),
	(31,1,'ws4egijQxWERZbtHzGHwpQ17XTHB3WNszHx0HjK4qhgDKTWWuJpkJ8LNSZwXdmRiI180hbVuDA8lU5g_NhY_FCc4jNTSkRhB-LNu','2019-02-01 04:41:10','2019-02-01 04:41:10','20dff440-95e7-4930-8150-a8c434fb6dde'),
	(32,1,'89TetVYIwAKVf1yITHUZAXkrw8gfjbSkiv_8yhn4kftML5Q2Vy0vcWIvxltKHJ_9esZO0jMjqQemDNHEU8ODYOrVmUl6XTXoDcbt','2019-02-01 05:41:14','2019-02-01 05:41:14','4593778f-c631-4ac9-8c07-a0c3a89789a2'),
	(33,1,'SrRvOdhds7M8n3eZMw2BEo_8CapvpwanKqfuaQJ1z98eBuaKXIWfd7i2cVsT2i1qbvrZr8Sf4LOERbnPXunuAUUMtzqcxNOUqTNM','2019-02-01 05:41:14','2019-02-01 05:41:14','879e0ae2-910c-466a-acc9-fdbca35ed19e'),
	(34,1,'dSiG1P4mwS5S-ap3UbAdj5o5khO0CBY7FqixWq5neHqqAmkDSpZ29Q6PXgb0sUhYC40RabAjzqI5SbGwsW1cea_0N1Q8RhOJmK7u','2019-02-01 06:41:18','2019-02-01 06:41:18','de2b2ff1-1317-4190-9183-f1aa96ba770b'),
	(35,1,'uQgstWJ5OWSZMwX1tzv3YwsgcT1iJhX_NGYeBpBDKsC_iWBTGR4z7q_786V7F8AQHTWEEINijhbP9NrXKsAoomltAp1oQ3PUfrka','2019-02-01 06:41:18','2019-02-01 06:41:18','48767b46-8cde-4855-b610-27805155f6d9'),
	(36,1,'OMViuZoW8I3a9HVL7458mN6tsxvAa8tgJA5ZTe7i1vsT-qOKNB_kxFR38c-3pbeMxB6sRf6B5vo13HjOTcxwwVLp9dhV7WzjFip0','2019-02-01 07:41:22','2019-02-01 07:41:22','ae9856da-5739-4908-9dfb-14bed9194a37'),
	(37,1,'lpBO1tdUBO25zR64nvWe8JEfYd-jRSCfHD-gBB_tLuo1zTzS3Be2ij5SuiDYOpXSN44NDXtsntReir5H5IRnnIig4GPn3H_LMyEH','2019-02-01 07:41:23','2019-02-01 07:41:23','be578e19-0365-4ec3-8907-6a6a699c68a4'),
	(38,1,'MmBTMaA8Z6PZ21Zm1kCINjN4DkOD3ZPYiFyezCMxyu0PH2jwsXrApQ_NsiQY62hPl7eSkZNELkrOwj3RoDRvphgXTpY7AQbH7epC','2019-02-01 08:41:27','2019-02-01 08:41:27','bd3ed50a-2ce4-44a4-a33a-10d657434a83'),
	(39,1,'8brQpKG__46jQZZ6IKa3CbQSJE6OWWJD0dQbLrKfZTzG06irR2hbc-YUmLO3GUSMnEVJ7PSUG_PnVcoeS_2Z_vTZsv54S-HYeFzp','2019-02-01 08:41:28','2019-02-01 08:41:28','49857756-49ed-420d-9ab4-e0786d25709d'),
	(40,1,'recV0C19esA6QmRSfOm4wNumGRlTHcFlhUgxF_4snQLrT7f4g64wFchI7z_Ox2hls9UeTtR0cpW4UKCXDhABj8Z9yHqmsZQCOvyx','2019-02-01 09:41:31','2019-02-01 09:41:31','93fd5c8f-a632-42dd-92fc-4973e2ef27c2'),
	(41,1,'G4JuCtH514-OCl_J2K0GsBoGooSCHSZi7dPnarCeBva2yEW8ZP88TUbrfxsO8TLcFZQuJMxKWNrO4NAkwdAweU5h7cdMR2n_u7Dn','2019-02-01 10:41:36','2019-02-01 10:41:36','81126cf0-bc6a-41d1-b69f-5dd0be2b77ca'),
	(42,1,'rPNf29oUxxnSEsROWsEHq9eycH77uSftAGxxNDzjSHo0Ro2mZLipon0uPeiJH3bgtp3qcjh5zdDY4RzHl_Km2ttksONyWUJXTk6k','2019-02-01 11:41:40','2019-02-01 11:41:40','6b3aea22-5fcf-4820-9d81-3eb3c58858f6'),
	(43,1,'-AJtIH_48DW-ng98FDoHtYJ6nHfWkATkntjfqgbawNSTTNigg-pN3_pb8RvWrUzKoGtra5aMDsAhJfy-prs8OkI6tRXo2TOQyQEt','2019-02-01 11:41:40','2019-02-01 11:41:40','b9a670da-0870-4531-a58c-f793f1668a73'),
	(44,1,'Qua8_rdtckYNrw7Mv6Gy1OTPhy6svdyTvdai76CwQYaHMZHGxrvvVq1vvpcb_wUxSbnIRgoIu84tg26wmtnE12E-8G-cS-z763kN','2019-02-01 12:41:44','2019-02-01 12:41:44','76667c8b-8193-4a9b-ae0e-b4e6da2a9d77'),
	(45,1,'EgU-OWjSf7iDqfSLb5yY8o-WdQwtZvo7IElkie9A6ecp-HvNOeSeXZJ_73xrfC4JUlTdc2vlr637p6KutdQIDJZCBwOhWXNd6yme','2019-02-01 12:41:44','2019-02-01 12:41:44','1ec9b9c7-11e9-44b6-b156-a8ee5e90da7c'),
	(46,1,'9ylJFIR68Ll9DgCxcdhrS04A5sXDkgyB_ZXNmaC6yyZciVo6ZsjSugxk9Fc9NMCyIPOu59cw5JP174dliylRb_hyJvVIh1LTZVzh','2019-02-01 13:41:48','2019-02-01 13:41:48','a47c267b-6784-4d90-86d3-7bfb2e8cf546'),
	(47,1,'_p9VV62UgONDibPim3gaDkTU_nFl4ruaugiC0aw9IzrLwQU3dkGhh3CQ-7QFZlwvrMkCnmE1O3vPHlCJgmAkHOUQRh3L0cpMv36f','2019-02-01 14:41:52','2019-02-01 14:41:52','db0bdfbe-5a82-4af0-bbdd-bf8a58b28528'),
	(48,1,'rflTv01YqfvN_-Q2UVigDUqTcjhsFg6JF9dFWSjOQMGZR-wr0WPkaKOselanvrmjbDGGairVPwJooof6YlIpdw1OsuDxgIytfOVe','2019-02-01 14:41:52','2019-02-01 14:41:52','d0ef61b1-fee8-41fa-8347-4a452d408989'),
	(49,1,'7pKLXigrTI1WubSqo60F3kVWh4IKCfIZtXjhpkVbj1vR3c7nx2-6gUAuDD2ZxKAwv7K_cw26UWRPQbUP3R1JgmnSANoyuyPF4jZf','2019-02-01 15:41:56','2019-02-01 15:41:56','c0af1331-1bfe-4f2e-81cb-ac7e2f5f9845'),
	(50,1,'QUV4zfB2O595MRSjj_iq5wpyYQWdlRi_JqtiIzbt__VvY3eBv6bxvuZt-MqfEEC0YLdCIQsAXrKtRDLMQCzaF_VyZgubuPgvksd-','2019-02-01 15:41:56','2019-02-01 15:41:56','8cf75c8f-cb62-41b4-9137-81e39912f5c9'),
	(51,1,'4nqcSQ8vVOmRpR8KR_0vpkxcX177kcfKIf8EgZ4wZZyMm84Oex6lTbD3aJX1xdIiaPk4zWC8kPBDfRZVHhuRYpy_0U3X2AMQ2dPf','2019-02-01 16:42:00','2019-02-01 16:42:00','885facfd-bb20-4ca3-8c42-e53d6ed4f622'),
	(52,1,'8sDRi6ODj24QvFpAdG66EFHqPc5_NGGxTtoszNvaH3cpR4OkKVBrxlwriRLj2RjjVzrpwNafRdH7XvYyEAchWd9CF01Fif4DqYJP','2019-02-01 16:42:01','2019-02-01 20:27:37','13a04101-53eb-41df-94cb-3f25cac765a1'),
	(53,1,'LwwcjmTaDYGJGOE3KFaoOizVnxdZpC1a_lDklEYGb3lFtVLYADm_DWob-lkHSQlIKNzp1YQkqFuHkeq8vFY6GaKjbBgtnDE6MjYc','2019-02-01 20:54:30','2019-02-01 20:57:47','5b6e1991-20c5-47e7-96b7-d1a613869cc0'),
	(54,1,'Ei_7fkZpjxl5MwMO4ytGkbGnCueI4fuUe3a15nwkrRmQ4YDy8iPxUHFGDZdh_rQzBuKoxtELHMmcSL59aOg_7D5biw5mqx2xEd6h','2019-02-01 21:57:52','2019-02-01 22:25:08','d809902f-06e0-4f59-a9fc-b94daa4ccb2f'),
	(55,1,'XfqE9SGHvpBW0J6Dchr2gV6zZfRO2LR-YzXsZiEkZT4Bm9EaTZIJdFRQZbbQfCt3DqMLAaeeM3tZkxwwtXdVpS5Yr074OixJAa6U','2019-02-01 23:25:12','2019-02-01 23:25:12','c9be4f34-7e6e-4a8e-b34c-c54a39d793ef'),
	(56,1,'JKg5gQ8ILZ7BSt1dzphP1q3qQg3w6YDEqHN4WWTFzrccdDevW1pUO4uTE7Z6f8lcEF7ZKVRy0TxYSXknFCwRy1GWX4k152hsJvZg','2019-02-02 00:25:16','2019-02-02 00:25:16','359b6fae-9be8-4e52-80c3-f6625c9755b7'),
	(57,1,'TO3sv8LuilfE3vRJ3alUyRMCs5qrMbSWKx7kpnej7PHB0p-Ek_SE7nBMskYSbd2XwralKblYwze-1DMzUlrqLTSb0PY2nZv8nAY7','2019-02-02 01:25:20','2019-02-02 01:25:20','cd16ff72-42c1-4628-93c7-12b794cb0e64'),
	(58,1,'CcuyK6xTInCuSiH50ZfSCDE74vM5namOdFos4ejsWHDKJsst7iqT72ZTtXV7Va-PdkrG6ki7ET7_56OJ5kEvXaq4q5MqQuJxc-8r','2019-02-02 02:25:24','2019-02-02 02:25:24','6e8737a6-456f-457f-bd0e-a82e33214a26'),
	(59,1,'TzXMGvj2d7rN6VApI2VjvtZ3aMleCDKlsS9qF2KoKH78qYBMcNdqpM5UTW33N5H8EOhv4l8ASpAfH2esUEjnsIg7Ur_Ni15BZeOs','2019-02-02 03:25:28','2019-02-02 03:25:28','0a527b9e-69df-4411-8ec2-8cc413962cd8'),
	(60,1,'Y0VJFznaWlrpse_7j8xEED2Tv9cjjuI5P3fCEP4-yuAC8dojb8ijdwKQWwrwy4-gfdoUXtfBYvIk2c21Bundf2QjPVCzClOQ5gPI','2019-02-02 04:25:32','2019-02-02 04:25:32','971af8da-39d1-494b-a61f-cbe447f818ab'),
	(61,1,'hNi1-O4LFkw2_k1MnRNkqU0xSZzStC_HfhpVm40oRHorpJcYZ2Bxb-nLzs9jLhzV8fDYTsIXHvGJndiR111Gcwstv57fzCKXp2gr','2019-02-02 05:25:36','2019-02-02 05:25:36','f98649cb-614d-4edd-a52f-b00da37ca4d3'),
	(62,1,'aqGxbUBHPlY-vEWqUcrjHHR3uIpkCyXKjXQiXhXWwGgnA2Vlv9dXbyIAUAwHYBLDHr5Iwyk9xTntAMkE3TyYncc7Ee68EUPDY7NX','2019-02-02 06:25:40','2019-02-02 06:25:40','f6cb52dd-66da-4ab1-8240-857578727e96'),
	(63,1,'7T34sr12jg88UowF2dViFKAXvqztRCt-h4KZTK039X2xzdv2n7VSQWiu15ey8aDdRHRPOybZHVNA-A_5rmUiXsF2vK1fZeDidPuj','2019-02-02 07:25:44','2019-02-02 07:25:44','01dc2fad-d116-4b99-ad52-53a972a42a02'),
	(64,1,'kp8vr1iyR5cYOjwcB_q3Es8lj_kZf5wVRAlQUqzPs5q6ElaANh5TGYvhU2y1IdAM2HqXPIRyASi5xSw-aJtl4BaKu6HSWnPRyCGM','2019-02-02 08:25:48','2019-02-02 08:25:48','56cde43e-7573-4aaf-ad84-32702f2e5da9'),
	(65,1,'ugRNZeii_Kq4R_89MlwFu0-EvFSH2TkBKbLIQW77wLOxRpAOZlsqq8oSE-wVYagQi4cY-D9JZ89XVyrP1xRGtptC2G5zVjoo-cfT','2019-02-02 09:25:52','2019-02-02 09:25:52','c1c3348a-968a-45d7-9db0-3bbd348221fc'),
	(66,1,'NalnoFNjxLQVxK4E7WIK1QkJsc9lxucqG5SXsewG3x_VkKkivrsRgTO1zeAlcEJw6z22Ra6j0G0pjsPMYeuxkpDTLL1W1jE9k3UB','2019-02-02 10:25:56','2019-02-02 10:25:56','c44056cd-a680-4754-8e73-4f117271216a'),
	(67,1,'HhfFv9tI7E7ZWytIBwk_OS2rAsw9sexUGSzUligvz3B9uxEnUETFjjf41kpdQxWVRk96PJUyVjWZ3TQ2j5VB1ByEwL620dfmtkQX','2019-02-02 11:26:01','2019-02-02 11:26:01','320fa2d4-4064-4257-af5d-d2f6f12bf74e'),
	(68,1,'a1P9hYq_3ccUcbomlranBRNwfeRNCv31xuIYyshkQ6oELmiYSI1RaTjugJxXrB9uL-uy8FPrR3PTwvv7lVjOm4SrwGBlbyP7LY2P','2019-02-02 12:26:05','2019-02-02 12:26:05','a88b4e9a-9672-4bc2-9531-04976a6dcc7c'),
	(69,1,'M1VgumadiIDKJsgPIdezjSP1rJRO8P31HoIuxhH-Sjaws2ck1blPU5mL4Rsi2Nm363J9tdwiOu9qNpt_p__YxwTQhQvwK6LmyhxR','2019-02-02 13:26:09','2019-02-02 13:26:09','3d92b1b7-3e07-48d0-ac45-65b8248288ad'),
	(70,1,'1gPlnfIAZXWZ781zl-zRJVBTWLY6PVDW_VjpPtKR650yPJT5lFPx6_xCVwh195CS7hrY8tnom1BZkdKnsckjzjufyhTeydn5hRJE','2019-02-02 14:26:13','2019-02-02 14:26:13','be4d3e86-e98a-462b-881f-ac0ae61cbf02'),
	(71,1,'oZSm9CHWPAhi8wf0Kx_X05ZNM3C4hMJIQbDQ10IDOjzTKPx3lth7ejkdTTOEnjiye8g98eprIeHqV-VVADHe2THuh_D8SEP99nTF','2019-02-02 15:26:17','2019-02-02 15:26:17','1a1dc6b2-73c2-4a10-94e5-459424fc1fb2'),
	(72,1,'FvDF6OKReVb47Kbie-UjrvLW8VK6sMzFrFu5OKD3sW7YIiVe1G8QyfqZKlaDDOcqYXqbNZbbPqaG2WvoXCBWoO-KitiKyVdJEGNP','2019-02-02 16:26:21','2019-02-02 16:26:21','28d73aee-360f-4b30-9d09-4608b374d839'),
	(73,1,'kx7xoZcgJq0Zsa5A_85Y9fFTbqPXc-14GO2YsKlxzzKfpmKpOXeijBYn9bGXoBufX1zY5f9JzDACijRXtDPG19FRNQNabeJJyqkI','2019-02-02 17:26:25','2019-02-02 17:26:25','7a2fe389-5131-4609-b391-2b554841bd21'),
	(74,1,'1byBjmD5_PRFNdK0K1L4fy5EFWoRNEezGQFcCorVyIVM9INT9iTqbjFbFaDHd8potON_Uhc8mshSEUqqWivcWgk7QCTXnJRtK4Lz','2019-02-02 18:26:28','2019-02-02 18:26:28','ea7f515e-7760-47b7-ab1c-1de08649f117'),
	(75,1,'m6G1oDQCMFf45C2bHLZOsP3uTZwiu6tC59f3G1dwe-Gn4QSARjyFq_9i_YWAM41KDe007GrhPfs_wi_xenT0gPiAra6wLU6Fd_gc','2019-02-02 19:26:31','2019-02-02 19:26:31','61b032cf-0e47-4988-a052-890c096d75ef'),
	(76,1,'e4R_URg8T6UyXXViiIQZdn6S3JvU6NH-TJy3uXb-iwlr0_u_0VkA31UZRhEDhGI_xUAkTgO-BDKHvphORLXlwBVxCGTD82YSHNnc','2019-02-02 20:26:35','2019-02-02 20:26:35','b65e2f96-014a-4a63-9d92-d4555b55c55a'),
	(77,1,'ew8fVzCbSRBfjhFIuyGhMdn9EYkpASV8bvnTdEma7Y79j-Wxu43m80cR_4XEplu0eR13l5I_b7Eh5RMxT2rJFFw5POtF1r3qFD2-','2019-02-02 21:26:39','2019-02-02 21:26:39','477b95c5-f29e-43c3-9e9d-35dd4f18a26e'),
	(78,1,'vnpwLGrmh3FTF_fL3hnXXNYmEUi01IddlS3dYSl84LrT2GWhwISwA5tURb_5qmuaMfqTnNdr8FVHGCOHYa5O3b7VU91wIgNNzlmJ','2019-02-02 22:26:43','2019-02-02 22:26:43','5703d8cb-ed3d-4235-983c-cd23b0f30a9b'),
	(79,1,'5fgMb2b18Tba62yodkwRcoby51dVRpv6MMpEVGZMysBhOdf9fopblqdjIs5I2e50QWiW_q3_fB0_4hRX5Cozi0NIb-yKGw6MzHGZ','2019-02-02 23:26:47','2019-02-02 23:26:47','633e698a-824f-4fe4-9937-0239534a43f8'),
	(80,1,'IAPtu3bxVkQQZwuOKcKkKXG8tUfq8cHtSpXHuLg73-La-rZnrr0Oys5wPXvKXBw6es0YzlwapgtjEh8vialLWUjP1CMBWr9iFNs-','2019-02-03 00:26:50','2019-02-03 00:26:50','b4965c36-c245-4101-8d91-5abb2d090d90'),
	(81,1,'T8K4mjGaFydSZ7jk2L_sSpUkoBqhHzP7CetthFDDsRsDpNOFYJEl9o4C5d8vyhQ3GT9xW8pdyLzkgDBsar4bEaKykRXDZ5ulzMy4','2019-02-03 01:26:54','2019-02-03 01:26:54','48b48162-b202-4c6c-988f-2d7d051774de'),
	(82,1,'MDby2Wu7VUxra7Ht5WtE3B5Hb9VOxryo3_YUZh96ijA42p1XZtZ6xw_l4IvOmo-eF6wQ7cggPGa9qiez43WtwAEvFzTCqaDN9C8i','2019-02-03 02:26:58','2019-02-03 02:26:58','3111edf4-1d3c-4c52-9d32-4e4a6ef709cf'),
	(83,1,'iJut_gMZj0pswS5R7_r8jbSTJtDX3X1gKs1eVYrIkufulVOtt9IvWRO7r8KuvSUuWMExEhWnikA7bC7UwDJ7U8KdIhx32wJ2nCM2','2019-02-03 03:27:02','2019-02-03 03:27:02','bd6e1767-3730-49e2-bed6-9abc66a5ed80'),
	(84,1,'k9U4WQZX-HR5Wc_B2Uo47a87g0FMHpY-xrdz0datOpvEVLBAyIoUlruSqKgmuvEhqg4ZtN80_C1HJ7T3sEiQHdglfkk7PT4v_pkV','2019-02-03 04:27:06','2019-02-03 04:27:06','6475af72-d261-4b2d-9eb7-0d44fa2a6688'),
	(85,1,'c3gVJxfOjjcbMDsbGpUVVaUQ2BJtGF_bu0fd_0i0GTObavMqCkWAwVbUqHfNOKuCRtVWpbDdPDIOj3-45mV1AcfzbqspmB1Vqr9b','2019-02-03 05:27:10','2019-02-03 05:27:10','95d1d099-6815-4648-b922-5cf6169f1847'),
	(86,1,'1JSdc93W1CZiVIA3iOuFlzE8SfkXg527m5TfYSACMBSvp2D4sFsh_b9fUPg1rna3zX2FKd7kB2FLrP_wIXVMGGVhNSsneLBt9_ZA','2019-02-03 06:27:14','2019-02-03 06:27:14','36d251b8-266d-4630-b3e3-8c7a29f422e4'),
	(87,1,'RIZqqkm15lGklvvVOBesToOpwueihrs-AsJoUydq3gjqfR-9wQUssuoVp_rxZcyyaZMYnBJfKOAMZvG4WSm-aJlclB6F_3v3U_iZ','2019-02-03 07:27:18','2019-02-03 07:27:18','be3414ce-7256-402c-81e7-52e01dbbcef8'),
	(88,1,'w2ZBe6IkMXriarXjqFEM2Ir1LvubZDr8aa425xqikaQG0f6rciiY9j3S8ds7IuTz8xd0S2H37uIfz9t5HzeghBtS6FB1zV9E9Rrq','2019-02-03 08:27:22','2019-02-03 08:27:22','27b56734-e7d5-4710-91bf-9a1dcb20fdc4'),
	(89,1,'9UNIf25pMo7JHAkYesMJGxRy4a_ReW_BUf2taUtGJHT3ZFkJlLhwpiSeYhbBNjayJHVxYbi8DazMQcOqE3GvWTD2DLnxLJSPWoOn','2019-02-03 09:27:26','2019-02-03 09:27:26','5491900b-b85e-4bf6-98a3-02e58a4365f6'),
	(90,1,'yy75cD9cw48gsvYU_P8l3dwZD-IhYLiKK_N-o23_nDb7hP_cCnrBCmJ6W4wvLWXbe_nDuHFldoFjBlLhGm6iFG_AOsHc_OqOMi0E','2019-02-03 10:27:30','2019-02-03 10:27:30','27a3709d-21ee-4e63-ac35-374121694321'),
	(91,1,'_4AAjg3jOYZxFE2N6vNSbAT88qzsJF2vj6AG0BUaqGhaBd0_fbKmU1VB4dyFNomoEyGv03Y8Z2uvetRF6H_byYnSGLbNq3UFkR5l','2019-02-03 11:27:34','2019-02-03 11:27:34','89a7748e-c3c7-4ac6-b61d-06211e88c99b'),
	(92,1,'0AdKmrFJb9pAymiY4INlH_L7ZUsXRdn4lZQ_6KrpY643iUDF_Ep8WOdRE8Fi4QA0C5eFMrpFiZFz5KTcH3U_1kFXRUhkr_VVSvxC','2019-02-03 12:27:38','2019-02-03 12:27:38','67f7190a-73fb-4b17-8aad-b7d35cc430c5'),
	(93,1,'ZvjE_V7zOHBjD9b1AEbhLWfRndevo_MfinBuTNZDaGfJEwQuYhi_evbf7avPw7fCBRCfVY9NJQnBPOfk5UgvzI_dy1PSAVZDIjKl','2019-02-03 13:27:42','2019-02-03 13:27:42','49e49379-6860-42ba-b6e9-c203d1540970'),
	(94,1,'V64qDa6n2qmU2EaRA-Y0Orc6mR6QyBmV4eeC2r-ekORhQazNzyi6Iirus70YCPVnvnJ_GMiuEL7UcgC7Xd5ZLzSGymudyjRSqZwG','2019-02-03 14:27:46','2019-02-03 14:27:46','2a7bfedc-ce4e-4583-a507-cd23ee2fa02c'),
	(95,1,'etm8srXtNyPG00UmSX4hYJyhpdCtWIvoRqpcE4r0Y1zvYxD5JP4rgsESbBjn6WAiiYi3DBBO7KpZsYg72g9DWOxSt37_-N6K4RAt','2019-02-03 15:27:50','2019-02-03 15:27:50','0e75f080-09a2-414e-8477-2ca5db4c51b7'),
	(96,1,'sFltrsVGw5ihJom_CFWolPGzaSCc2TuNj-TAv7Qz1oWWxH_yR5N4bL9uyRIPEdOuvubJilRwJyeyuEqWIqSWSvbtgEQ8jIsFmZLE','2019-02-03 16:27:54','2019-02-03 16:27:54','f0931833-5279-4e70-93ec-605f508f00ec'),
	(97,1,'wUVE94IvrZegeHGCOTxWwbo8-A9Z27IePDsdNPjDzWKHZHlFi6pClKx7PzVHAEorFeGOJydlmJ8PZd-X7HPKp25TaUsUyd5QfZI6','2019-02-03 17:27:58','2019-02-03 17:27:58','f66aca81-c3f2-4cb6-b108-3160d52f3e8f'),
	(98,1,'woNOiCi5n2xmsGeivP1Y1yzWcrP1B_Ba8-fXeg5cBbZ63_ATRJS9NrPmghQ5ACkGQ2ziKMX6G-WdYQepe5bPICMpn78exez-4Ebz','2019-02-03 18:28:03','2019-02-03 18:28:03','4af66bc4-0c80-4d0c-80db-8db8cfc3cc6d'),
	(99,1,'_rHVHjBsh5JaVsu7a2d9dRCc51V2Fg6XSnHorKGr7TATN4XJwgg-_4GWZB_lPjbHka3INPbFTRX2YpH1hsTrzTEcT-PqW-C4mfOH','2019-02-03 19:28:06','2019-02-03 19:28:06','b48ab272-5dc1-4711-8db4-219feb62119b'),
	(100,1,'7LQf2HPq-i8rqrv9rJqeWEFFritSCtAQ8xZnjdaGE_TzCu4DUK3Vdw-EacErut7erZGucFRacTfPSyt4u3i3f_0o4-W_Binp-sXg','2019-02-03 20:28:11','2019-02-03 20:28:11','0596a7c4-767c-46d0-b451-00be70fc573a'),
	(101,1,'jngIzqqRGjlgABzsNVSXQFkco6QXVtUy-ZfSDlaoIjwrfGaXsGCxubOiQ28kP97CxqQM8WjRPiGua7u3Bxg4u9vK6jbhvEn-Ae6L','2019-02-03 21:28:15','2019-02-03 21:28:15','82b2e6d7-19d1-442d-a2ef-379f486c3161'),
	(102,1,'a1n5OSppbVnsf3Wmsz5tmRxS_Lb-fTQQq-J9py3lxMGy8kZgC7eL3NKeZBGXFeU7WLSWee1hWyjacl9b5hqwpZcrvb-a_RIEKU5T','2019-02-03 22:28:19','2019-02-03 22:28:19','d57bc463-a463-4d6b-a92c-3d773334c1e3'),
	(103,1,'1uKSrFZCMdIWDg6NS0T4AtpgK8tIcThx1zMosO1yxxfqzWVHroCSZnOsRkQrTBxYAJ3R5p7sGXJgOeXhYtrztl0eiDMmcqd-2Ebf','2019-02-03 23:28:23','2019-02-03 23:28:23','6a4c5be3-f7aa-4bd1-a883-c91a0a4231b9'),
	(104,1,'QowHeV6K31VIQqIq3vnSMlLmyCNwVmsiVFsbmEnC7HYNzT_jVhEK_iinpW8qXyz3eew-x1ZUkjkbHD7d9NE5U70RN4Zjd0I1Om4P','2019-02-04 00:28:27','2019-02-04 00:28:27','6e8a790a-8582-4ab1-9f39-038cd5f5cc48'),
	(105,1,'bZmqD5tQG2EVe6F8g4NPQyZbjBj09ZqHdv_P0YYiU5SoSTrFAchzqYWh8E_C15h6wjYadKPw_-SBuu_5fLa0sLHdxzJQ0S10ykJL','2019-02-04 01:28:31','2019-02-04 01:28:31','4b76e0fc-c25c-40f0-8c67-913eb7a479e2'),
	(106,1,'8z_-tng-Jw_LfRYlNkHyinuS_d_NVFDLfBtCcaxOWMhX201gDYV_WschkPdVNv-hYShm2N_6wC6a8gxAstWHGDkzQH9BI6kds2SV','2019-02-04 02:28:35','2019-02-04 02:28:35','794b99a0-12b2-4189-ab4c-8bcb7ff67967'),
	(107,1,'_2rdisEC4qjj3BV5TdD7jZgbeizpYynYzU-lMNAJxz__a1CLVx9vKQad94Awz4m5aWiyUxjE4nY3jVzyRKF5NSZZgZgiKk1_cjL3','2019-02-04 03:28:40','2019-02-04 03:28:40','87dbeade-ff80-489b-84b2-e8160889a065'),
	(108,1,'GJJOEkUDTsmd5hC_v7pibriiZZGkhh6hlNj-bwq7gilJ_YpSah5iYUJPKigUAnx6wNRqhi37yJuw54wLoY1t6bI5Fam-qBOuSuap','2019-02-04 04:28:44','2019-02-04 04:28:44','769d6c9b-ec44-4b48-97ca-b31a22c8f82d'),
	(109,1,'mKNp1hSKovdyvUFo923CW5jMqmmJc72x_J963VJlkFSoq8fpFrq6FiG1WYj5hzCOnzb2c9CTG5MyGe61Bcbwk8RD4ks8SVzhNzEn','2019-02-04 05:28:48','2019-02-04 05:28:48','1580a13d-3f31-4d73-9100-12adfa53ad95'),
	(110,1,'y44v1LV92KyiUk_5SVIMlSpWHdEG1PZa3JZLgdf2duTiA90C7wPs0ZqKMq4H-9n9CFCgmz_c72R4otAn5RMYzFGECKWA7xo5D4lY','2019-02-04 06:28:52','2019-02-04 06:28:52','b2b35201-d203-489f-9f8b-815eb190fed7'),
	(111,1,'qWPNwdeMi-K7e-qYOVOhsLlGDY8vQNw1xLAVSSZ2e0rGY3ER6dKHcpxYqzj_9eHIQ1IRhCIiokkH2ytHaKKUzurQxjMDH4BWFIkm','2019-02-04 07:28:56','2019-02-04 07:28:56','f4b1e972-2b9c-4a97-8046-ba20cb0c240f'),
	(112,1,'Gy_IzqPduFSvEDg6BzJ6Lz2iiGDseDCU1C7_1nQpZWVO9Z5oOesrUwU-GtP54AgImAg9L5QQ45vlcEE9Byf3WeF4wn7hbWqVTCKZ','2019-02-04 08:29:00','2019-02-04 08:29:00','23f810d3-fed4-41ab-b04d-14d1ed0200c8'),
	(113,1,'Wsrp5JOaa2bBQvT9OxTqeBO4_5mcwvttM5IZYR6AC905IQ3pLGqf1m6Zr19XiZ-NN0SwaHI4uLH5mjaxNQwfcmPyl9s5UJfX3Q4P','2019-02-04 09:29:04','2019-02-04 09:29:04','97412c2a-ef1f-4186-ba3e-d4fcf627e0dc'),
	(114,1,'K-CIg02C_xZBhGLeMmo_RRltxPGQYIUwda4wXwXrXgVaKWKg-vYZfhtThKc0xRiSnK9a8LgNf9gAsl5Dg3x8dn5WxmwGyx5mPdPO','2019-02-04 10:29:08','2019-02-04 10:29:08','ba38420c-04d2-431d-9b59-402f52f7c918'),
	(115,1,'CGVSBbJo6mXNi7kNORcUqwk4eW3x9dv6YxK1nbqMRqkxHhAcVrVIMe_F8cSITapmmArnBUS7KwicQKjvoKLZbYDUDORLhj2fs7FZ','2019-02-04 11:29:12','2019-02-04 11:29:12','62815506-0136-471a-a937-6cfbb20f0f38'),
	(116,1,'z5BoEqfx2DFbFvzda78sMFodjm-rosZanKOGSwWE6R6sk04wFcL4ZK6RI3Civy_4uZrz0L0ANUHUI6XflMJhM-dtyHtG9gtIAJTf','2019-02-04 12:29:16','2019-02-04 12:29:16','a4f30596-449b-430a-82a1-430caeef943c'),
	(117,1,'7M2BYEegT2q_41Y9FVAsckeyBMbVPUgBHBl5Bv2rR4cyzvSTbIIRjQUblYg1yidbt-WLm9X9ieBC0kVleMI7VDU291Misabzq1tb','2019-02-04 13:29:19','2019-02-04 13:29:19','db21ac62-f0d5-42e1-b5da-2f0261cf1449'),
	(118,1,'jiiXxCVwcydrbwZpIsKT1CO9AK5Ie5GVovsc4bozfhwaymdIYz-HNhhbPierSTdH4j0k5Qw4jKkN4CTjBnxnSLwOiLusu5I2TZF5','2019-02-04 14:29:23','2019-02-04 14:29:23','aa8c9b41-8236-46ec-b62c-7425439ec127'),
	(119,1,'dDm4fSWVz2UZsJofG6t3Z6n50svZ82vr3CRPv6kTRfqM0hVg8U9b1pkxDj9vH3lhhNwX-v2hkYWC79z4IcYrKhE6RC1s0VCf-utj','2019-02-04 15:29:27','2019-02-04 15:29:27','aab7a4c8-9ea9-4c5c-bc3a-ac785d5f572d'),
	(120,1,'N3Q0ft5OeuV515xcOUWvwpOIFFGlS8TipzPHq9DUmlvlb3n0HgNzVJpSuzoakaEs4sAQvcA8LCPXfj0maheFsHEfRq7uES_ScISy','2019-02-05 17:45:27','2019-02-05 19:14:58','94e5f82f-a6df-41a9-87dd-64c4ad24a664'),
	(121,1,'A6LjgpEKmreobGGwXFx7lqCNjX865fKoCfS1cRDV86IqHCis3PEtbe48Pifh-ebIOv3VGiCZr3DTWmK1Rxwz_S8sHJv6i8Ar1iF4','2019-02-05 20:15:02','2019-02-05 21:34:48','f723937e-4479-45fe-93e8-cbcab58fecab'),
	(122,1,'VBqrwdVYABLYUGT7i1DUnfeXM32lxC3YRRFe5zB9DhISQM-3sLgqCDGaoNHtlsT94P8wLUUT1TQQ16_m36-nMZj3zLFtbHlmcZp9','2019-02-05 21:42:02','2019-02-05 22:37:32','bee5c9f0-3157-47e8-8e9c-2ff43110a3e8'),
	(123,1,'wB7I2vpvqeJVTIlEY_A30qPNzkrCMiPdl8Oe4gEnere4dhIo2QmY8Fkku6spoQZ6zHxLoMwCiojDEGJ2FwrwUAilowlYWp6xaa3Z','2019-02-05 23:30:04','2019-02-05 23:51:19','8c53c44a-56e6-411a-a50e-03a10eaed4da'),
	(124,1,'4K6-t9-PgdHAdK2OkICJjTUO3E_ddKczd5pEb7Kf8lIqNQycJy8AU5Rv4OHydk8-WazxoZ-PnjtBZ7w6GuwWtOcfGFi4CPe6U9-L','2019-02-07 20:17:01','2019-02-07 22:52:54','04241170-b728-4642-a1cb-9ff46fc724c3'),
	(125,1,'PNrcFegKBSyJtVPB1zEMJCMMLPuUn-bAvgAg3WTV9FYp807mED30FaUvznT_KRprzzKKxgSXU20q1ECIO9-NnaNIPZryz4IxdwwK','2019-02-07 20:44:15','2019-02-07 20:56:42','86a15a50-63d4-4604-a61e-3c8c21d13c7d'),
	(126,1,'4RNVfGpFg_IKmbr8yl8iPytVKeIfL15wDpZvOhwV3o5AyUxATAQVsOyeV6joxTPNlpVpGUZNIfwYEIBlJr5MCVu-Pi2hJ4dP6CTH','2019-02-07 23:48:59','2019-02-07 23:54:12','1fe929ea-eb7b-49f4-937c-a520915a34ae'),
	(127,1,'Y6cMNMaooBBPCZEtUf6IdB2_qSKnF4jcnk-870I1ViaL2svjcPDZqjNx4krq_OoppEU41hF_lKtGQ5k0MyvypwAsZfxa8Plrn1Uy','2019-02-07 23:54:23','2019-02-07 23:55:43','c7e8042a-6196-409e-9f7b-63214cbf4a08'),
	(128,1,'LKxfsJ0Egd6un9CRlH6sxW-z_Kx6CB2Rcs190iW_Ah-GL4o9RPe8bCCmQmEUr8rbFt6E57xARbqC0g1L3-hksk4nGnD2cnde0yI_','2019-02-08 15:41:50','2019-02-08 18:22:07','bc9639d7-b610-4221-9ee1-a348f2945fe0'),
	(129,1,'PCMw9OEovtYoe_zdbwPaXtdRetWG6OAmEXKeb63tpGzO-fowAeT7oBU3smQOx5ps7dHytc8uTNViIqlDZG5p-jOH1tSeNn9_jtF7','2019-02-08 17:29:46','2019-02-08 18:03:07','cc7cb869-9e30-4879-b744-ba67bf3a5495'),
	(130,1,'t_Y0ewzCnCJPWbg2MQdISwRpCfwlErH8nxfimV7QzEuRhjTGrWsNCqsHiCyVI7FaEyu8tJpGN2zZYNC_0vhJZtysnposEQp_j5YF','2019-02-08 20:00:37','2019-02-08 20:13:12','471fb2d3-3db9-495f-9f73-93c441cb1bad'),
	(131,1,'WJlSozcCYGOZmQqNsTZf6G5VzkygLmQnu4Ri1G3QyL6UX8oz75fACztfTnwkQykGKfPVA5cRW6luC3NwqIeve6UpLtq38Cct5Ntr','2019-02-11 15:07:30','2019-02-11 15:07:35','636e24e6-cb85-4970-823c-790b6c45275d'),
	(132,1,'ABqlNjYS6rBBE3ekQ4qUdrKMRuFSCDlzvK19A1GK7B87-NGi9c8LN_w42eXydCYCctfsPawNugCmQi89gMmu0Ncx87nBph3lwqrE','2019-02-11 16:06:03','2019-02-11 16:07:56','5562bee3-05b9-4b2b-b695-fbd41014ab22'),
	(133,1,'kGPLHLtOUjd5Wxcdw0oAPZemJOSfJ7_Vr-37ERxkOdfC4iZ3T3rMMsghlPRUg5GB2JV2nxPyiG25IcVGHz_a1rk0orhASzff_UvT','2019-02-12 20:33:29','2019-02-12 20:55:32','7fd5d680-aabc-4334-8716-2871a7a6c1f8'),
	(134,1,'u2oDhRbH8qwVD9KA0odux_k20UEzham19LsBYVwEJnOMh-uT-2lvhvpVJtYcmm19qbnsy90ZJYNlG10hfOZ8xmtV2maohEns0UHE','2019-02-14 21:29:14','2019-02-14 22:58:28','c57e9e67-b4ce-439f-a889-1ecbeaf425e1'),
	(135,179,'raT5iXTkiGn3BPuePBxEsMHnMZLmS-1BoNY5-1BiUHDT_4-FBJwewmTX9EWWvTCucRJ08Jykz0EsOwvRvJyQMbb3n3h3c4phW45i','2019-02-18 15:02:58','2019-02-18 15:03:05','ee3237df-41c9-47e7-a2d5-190ff69d02d4'),
	(136,179,'d3Sn5BbKuxA0LzV-eji-Q4n6QFk8S4MQah8Ca3BKLu1Hn8oH3M-adOpkd0X_APjHuPd_upEymQdLXKxmb4xDIlixRx9PVZhUGqgJ','2019-02-18 15:10:32','2019-02-18 15:31:01','25c21d93-6762-4c5e-b9a9-22177e3cfdc6'),
	(137,179,'9BP_MJuTMzI71YUEK9FsvvUGbWGFzUFJy7RAfp5YFq2nDhtFbG6Ra0c8mehFbNcOb3LdBjrYbkSNZgIRa88ZOsgeSi6yzZnFrQxr','2019-02-18 16:31:06','2019-02-18 16:31:06','fa468835-2b96-4317-a73c-91c7c8ab0a40'),
	(138,179,'7uZ5TuPQFGLYFQDT1duAWGjQMKHPDp1z5XdoWwIZp0RbbEIzA0lAcnvozkobOrj8FunA154L5BfeorUKSoe2SuqjWci61elAb2HL','2019-02-18 17:31:11','2019-02-18 17:31:11','86d677dd-5f74-4355-b0a7-a6ec0f074791'),
	(139,179,'LUFFpiogTevbLGsbMwZHD85b5QGawNzHAWCzAOJAahRhHTmaLobJuI7bwzQWxWH4n_9AHQueXmN-JaCf1XDBBXlREbUyUSXKBvft','2019-02-18 18:31:17','2019-02-18 19:24:13','d276d7c9-a2f3-4884-b49f-f90554844c60'),
	(140,179,'Gp3yneRRBecEeP75q4PTJwTjuXeBjQlzfajrba4breWZ3LmauMLlbaNPllfGriFQXIJ63Tc1SnZ7h1zVWwsFM9vpQMUXASOmJAcM','2019-02-18 20:24:18','2019-02-18 20:24:18','7cb6d692-dd05-4cfd-9214-84dcf37ea7d9'),
	(141,179,'tOQyHqv3k6TWQ7P-5tFbeS94_pdntRZKKJlKLihX3qVPWAVAI40aUoQIks3r52e_cMOWM7F_A98QgPDVzjJHjLgcMhDHq1-eCak3','2019-02-18 21:24:21','2019-02-18 21:24:21','291cb2b8-f418-4c18-8b40-286509fac503'),
	(142,179,'WtOPZrH6G_oLVui48hlhNk89psdWyUc48Rnu_fDKgp1wWRnpCkuDGLksa4bLOOz4JF1BfAEep8EtV643VoatwZTMpfF6cMc4JFpP','2019-02-18 22:24:25','2019-02-18 22:24:25','76e58540-fe15-40d6-8490-0b00d195b0b0'),
	(143,179,'-iq9pgsUuriSfnWe_eMksqHXaiKJwfNoU113cG54Yr6gxM8tg6x_WhnaA3lGKCdgMu6BFYavBaU8qZMhE-gIJaYkSk-9-iNcqp3p','2019-02-18 23:24:29','2019-02-18 23:24:29','37710cde-5917-45e6-a54b-14e0ac601e77'),
	(144,179,'bOW-bC4Zhi5KC_JTxt-_IB9yTwmu8WjEh0kOo-aXbyfn_284etp9E7V13BINOE-6OXE7TqJuiX3HD336eOHQyC2Sy2CJLAHzP2Fe','2019-02-19 00:24:33','2019-02-19 00:24:33','c9e92108-36ad-40cd-8294-f6e8f6b53022'),
	(145,179,'XW4fQNTdlIBb7bJcm8xPDQ9SMHlldmUx894JHpFwwm6YEJdAqkRFwOF2DXKVDOpcagwo01ctPwHi-YSIhdXNl8OZgqMnXkB5XeLL','2019-02-20 22:42:51','2019-02-20 22:43:01','ff370a72-a102-4583-b823-87ae805c8b6a'),
	(146,179,'aEnWWkr5SpFAWqzguOzz_mvRrIRB5Tff4DzSA0smW_2kIBw0LHUrz1ezm8574Boa8nwhu2q8uCn99Yxi1OKGR_ueLpe3p4f-IM0q','2019-02-20 23:43:05','2019-02-20 23:43:05','bd07786d-c4fe-4891-84e0-8989c8a433f6'),
	(147,1,'pIae6lFyqZ5d6NZzRci2BENciYXV-6VlEvrshWl48R3TuiCS-F8hAV0tTNrOTkjBF3bM7PoOccJKK22u-kIhkLhGxAdYrWhpfhzp','2019-02-21 23:32:05','2019-02-21 23:32:46','a9f3e3ef-0f78-42b5-ad85-4517ba8edb33'),
	(148,179,'BR4MlDQsvqa6YsjgwAo3xDP2w0rpmdLmX6JS3oZXoZ9HEqw2CH7Ryu3uyqK_RFydJAw8BGp2yEJciZNWIjKyta6OqH2q4Gg5NyMR','2019-02-25 21:53:03','2019-02-25 21:53:13','dd05fea4-1b59-4610-8553-5661c2aa177e'),
	(149,179,'qmM75en8UJ-Y5uYxczc246hMQcw2PwDT_kycPndTbbrenJADv3BxftONT1VuVGJ9j7CxyFT8pE55nmJnl0IjPHSMAhgU1qTiACV7','2019-02-25 22:53:18','2019-02-25 22:53:18','2dae9723-4550-4cdb-a518-a5610c8ffed1'),
	(150,1,'jIj3QM1BS1QZh5BiTXpf20yMxWZ5lz1I3ZUYrD3-VjBdzayQKugmePdhiXBvfmq1s_My6czFB5_7r0ExrDPGSoGYN2oSdfhtd6hv','2019-02-27 20:47:37','2019-02-27 21:50:06','ac71f5ae-a773-4fec-9aa8-e43bfddcfdf7'),
	(151,179,'0Omle4Izzdn-JpaKFw45YirkYc6Uu4jZ0Irtm8bgMJSLvEaRMTtgVAgR2NZEwb9E0aitSUaF47ZMcutHEyB9rgLgfuO4qum9RueP','2019-03-01 17:45:16','2019-03-01 17:45:23','cdf46fa8-18c2-46d2-9c1f-168450c830b5'),
	(152,179,'nIsPN6yCLIq-oJg3Teu2yR9CMMJ38dDj7lCmCrb11nSC2CsZ9ohAzJ8pOvlbSs9jeuigwrfkSs2SVh0fqobQ6fYRS4YVj6Nn4N6w','2019-03-01 18:45:27','2019-03-01 18:45:27','0fcf9dad-2a14-4ad5-8046-dbeec23f0d4f'),
	(153,179,'MQugpdCixsX78rGico8797wrhztza57zHhvFMDUcmPZ41mpumgLxhWZMsOBqrIdoEWnSl-XRWVIPgIwdtI__4gIasfCorCD4XkZE','2019-03-01 19:45:30','2019-03-01 19:45:30','f3d859ea-bb92-42d7-b5a6-a53c7ffa5ba1'),
	(154,179,'nUMgWjFt7yFhpdcM5mH8HIYDey9YbhYmgOHKweDCGS58j6yNZgKQt3uAtzxrhqlFA9cxW7osq4JSBlnAa0rnO4Ixu6hsRCExm-bk','2019-03-01 20:45:33','2019-03-01 20:45:33','8cc7a72b-f404-4d20-b849-30f270c90939'),
	(155,179,'EEqypXOnbT1XCsJCXEShTn8F9E3Xflf2ch_f1Hcef7p-j0CHi6T_K2kxduZ5ARn7ohhoqquJ6dHfoEQK478pkgnIhHIoDeVuOetA','2019-03-01 21:45:37','2019-03-01 21:45:37','eff1b1eb-4bb1-4862-b931-126f269fbbdd'),
	(156,179,'-aiYw71M1U36-DWl7bvJuVQCdKviKvYUgKemjePm17TfxfNnFRqQdB711mViZhSFpXm-oLAHZ82agZxJ0xhQOQol1rKNruTicnW3','2019-03-01 22:45:41','2019-03-01 22:45:41','aafc6ad3-f789-4ccf-beec-e2e350b141cd'),
	(157,179,'MLrXX5JpDpQtq9vycAsOQqT9saF48FGNuZq3Qhn6LwkBfDVw-KaaCLEFCJWwi2Ixtln5_VAnNvqlifnJVA2Tcd4BjjMyVnE-3HER','2019-03-01 23:45:45','2019-03-01 23:45:45','2c110e4c-c067-4a22-8cc4-77e182e867a6'),
	(158,179,'lgYqI48WYa3WZpFcv9WgKEv5w4EqVhS8p6DTw75G4YiEDAvzlLr3YBIK9S9IALlhmAVQoUqvpllJfSUx6gfE-VnlRsCzZLGtUG8P','2019-03-02 00:45:49','2019-03-02 00:45:49','d443cbde-a1f7-451a-bde3-6bdbcc8f9dba'),
	(159,179,'xz-BLVGdo5kRYPEMTNq2OD3h7zp5EG0ZOLLoSiHwZjCdtgAOa_n0jL1YXQfBC-Q-0RKxgxE64NiMv9_Q-8I_LR1FgIC_DOCMCNBC','2019-03-02 01:45:53','2019-03-02 01:45:53','05b52653-38d4-4803-b700-13ef554931c3'),
	(160,179,'bF9bdgyZqulgvjKDc3CKovqLM7fpu0GxRaFnA1tb4Iyt-LvUmeoqKJvqBgCJymEqkqzXSktrlh2KaiHKFPct4mAwVTDDx18GSYXx','2019-03-02 02:45:56','2019-03-02 02:45:56','4d89b09f-e7bd-46b3-8806-cbf5f856e176'),
	(161,179,'aeUUrFaaXZtjtKFLBeFdmt2KOzdvwn3O_H7trL0NhGAcvSreTAn_KyMMzQqeeBdhlyYv0ftrsYcuw90-LhvjemqlMvG-_U1KP0Rm','2019-03-02 03:46:00','2019-03-02 03:46:00','e46f0e6a-c67f-4b30-aa64-c7bc840f5cc7'),
	(162,179,'EAwDmUD3-2VX75aOZZV7wdkqzvRO74U2TLGv6vxt4M_2c6jBDbFnxF-wWU6AyHROJjytw__f72pIsCzUu-tlep9N75ORXEPdB5wf','2019-03-02 04:46:04','2019-03-02 04:46:04','950df009-b307-4891-bf4d-338691d338aa'),
	(163,179,'tA4uHGuhTSWlqGqJ0OY54rjBFVe2IJ4Mvs722nR6OPbGof6SCG8jA-JlGy_Mdyrd8ahg9MjdYVCt4ahNT8O7J3ATxIBAfz_vCPTt','2019-03-02 05:46:08','2019-03-02 05:46:08','daa8d67e-06b8-476c-9be8-ba4b4f1a0339'),
	(164,179,'huz6xqKnKPBHBnZzVibfLfWZH9x61Qvf8hfh4_hYyq8uxaXynxUgDS6IcyAVSFBc3rVk0nzqRO9RAzZuXy0KQTc8GsetLDH6tCdp','2019-03-02 06:46:12','2019-03-02 06:46:12','741e820c-49da-4845-8fd4-591f82675843'),
	(165,179,'o_liec_tlbWk9rQ-MVj4UplR-1iYTUHXBJy-mWLd-EUYRttdghWOkaXrmBcyqERAsOaTsKkfAEOH8aT-EIQLsq42iiV3zbvbJhQt','2019-03-02 07:46:16','2019-03-02 07:46:16','de8c10f8-795d-435c-adf1-af2998f307b6'),
	(166,179,'Rzf_yhHQE8v7B-RLmzwZymbRbqkyMMMi9bs3nPjFKkJQQYPpNodmoksKfXUTLPoGPt3PlYrdCGRgaAoYJxvc_LowXZ4KAkz0BRsy','2019-03-02 08:46:21','2019-03-02 08:46:21','6ef08898-65fb-47b4-8815-6a0393584272'),
	(167,179,'0dlv7ywAwNmqBYO1ZXwyA7V7X0eocz55bgcWlRw-HeYaxxSTWvoUsUmqyoufi0vUt9LbMyHlzd_4akzpm6uzm4qVVPmqkIGcAQyj','2019-03-02 09:46:25','2019-03-02 09:46:25','1fab8311-84bc-45f9-8129-8bc6ab35bffa'),
	(168,179,'JzgHXEaLPl3pq0Hz-V6lSRUkJWKCGUqL5TGrzN45gfXfod1mOv0i-PaER2Ro0mVDlVX5pKlC80rYXAKAoumWiTFwmF8FHcFTynTp','2019-03-02 10:46:28','2019-03-02 10:46:28','d1149489-ff14-46f3-a891-fb662630136a'),
	(169,179,'ReoyJplS62pjR1vh3ht-gMIcuppX0V08A31m8XllAzPtVprC7_o-ViUPhhsP9_XwwTaNAingozs_YTtpDy8EIbgT7G4DlaiBETEq','2019-03-02 11:46:33','2019-03-02 11:46:33','3e130a5d-900a-4ca7-8b50-81f4208562d1'),
	(170,179,'HkBL491wo3lNtW5ve-u9I1vg6PtX8V-5Ao46wzJml8QJKwT2xk9k2o6PVvHvN-cceDWO0ScKnt6XBga1Y0v_NzcpYrHgz04_WImO','2019-03-02 12:46:37','2019-03-02 12:46:37','9a7db2d7-055b-4a97-8341-511d66aa2882'),
	(171,179,'-nqGM1kzgwqr2yTWMvjFZ9G9YD-PELflYTB1X3h4NTVsp_4xtSKQRicPMsrcz94HR1TIjBL2vUBdw0qHVjvaGv67usxPdw81dqko','2019-03-02 13:46:41','2019-03-02 13:46:41','64e8b531-1765-4f24-9e47-36b135258ad0'),
	(172,179,'dlm1UmwPdGdnHp9Jn4CmG0_epMLP9N8_wiHVaJd9A1urAx2Hofy5qIHgDgpaXJX6deHyjjtLQAuFex8a7Ofyk8ZhwsgphT36ex_A','2019-03-02 14:46:46','2019-03-02 14:46:46','32b0372c-0843-40f0-b2b9-002477801e17'),
	(173,179,'47ryFso2rszPZiynBXjzbpZvpfisXNmsCREV9SzwoLPKWfzLwR4exhzv7Vg-OJowtFdZOZ4m07ztssvO8brqoptOJfx6-9FNfk4E','2019-03-02 15:46:52','2019-03-02 15:46:52','195a1eaf-31b3-411b-84c7-f1e72c9f9dad'),
	(174,179,'bFra9aTlLvv7LrE83QjdUpgsxx0tlOPFDqQ1E9ZnOST2WEPdYEw_Dc5hQRfd3J2I8_PVCkXeUZekU92-jzEnBjzisq8OYMEX2EPM','2019-03-02 16:46:56','2019-03-02 16:46:56','8c2da2bf-82a7-4131-bc14-fd4433411892'),
	(175,179,'LfvPJr1Lo4cmgzbhrpKmNtKo9wwUtSMQvbObV8kFiZihWMyJnioODwi68irWCKbaBBYEhXBe7qkovimT5Nqr3ldjU2lVOYHG_thq','2019-03-02 17:47:00','2019-03-02 17:47:00','a736ba88-9c69-4467-bf0e-8344923d9b41'),
	(176,179,'V_SlZ_CcMeWvPL1trYPJPmZpXOV0CsJiCom3_SzReKcD0SqCgI_Ffyh-rirTrmeUu0PanzwrIqwVdy8Omb_xvLXy7gwqd-NTu5ap','2019-03-02 18:47:04','2019-03-02 18:47:04','5f79e9e5-d362-4839-9c0c-86c54c84cb44'),
	(177,179,'YGGrtqlnLGMejnOFNIbcQhqcaRlSyWS0E5weCrVAlGVvKSktGd9fxH2cECSqbtkqbpBPxhcvv62OcTdENAQ6vc9Dp3s4I8wACBUd','2019-03-02 19:47:09','2019-03-02 19:47:09','904361c4-cb79-4e5a-85dc-8f7f63087818'),
	(178,179,'kF-6mDbpENtUvKbVkTiGmSlxzYBJHlLgNrm9wWfWZCckqzIZwVKVB8lzOi-psjKlrUWH2YZhlDIEMbWttGtspl9qNdhho-_ZssoT','2019-03-02 20:47:13','2019-03-02 20:47:13','00b1e468-07bf-424e-9376-70d42e5a193a'),
	(179,179,'Fa7hQmUD71PxzOYnVaS9vjFfk6gg-t7k6j6vy8lCLLTzEEFW2I_vMYgSu0JoC5ybxCMv_GpIs1dow6AzwcIWGyx2cXLM3DZ6mgF7','2019-03-02 21:47:17','2019-03-02 21:47:17','a7f5b4f4-08dd-444e-8f64-b3940665288f'),
	(180,179,'xqnMEbBzG_s06LG0B44kF9LObH0FIiS3-VJzOJZpXuI0dfDebMLEgFGpYR2Laio-TDmV71HzCIjb28sktjX1wDSfxz5Wh2UtjyLG','2019-03-02 22:47:21','2019-03-02 22:47:21','c74d5914-7394-4d2b-b3a3-67ccb89d5d8c'),
	(181,179,'ArGPZjOvCp3msWP4n_in664vLXjwVWTcKEl8dUihn5E7A6q4mKkmsLwFsVnJNGTZ3PrWOsqkAT0tJK7lppoob-kdv-67w7TtVMcp','2019-03-02 23:47:25','2019-03-02 23:47:25','2881cb66-bb17-4ff8-8492-b77e9ef3b6e1'),
	(182,179,'Mf7FRSG3QtqNmRgJXt-uX4SP_XY7c10GQkgIePRsWSQSF-VYXVh2iuE3v63PEKzKI_zZMtJ1C9s_-ANm1PTAN-Yc5FWGn6ezN6f9','2019-03-03 00:47:29','2019-03-03 00:47:29','8afa455d-646c-4fa4-8601-be94a93ab43d'),
	(183,179,'O0QXeLieTOYhK7iUGkN_yr9Nui_4pRK42Kr0M6FBCgFyzPwlUAOcwbsIzQf2vD_jg5OwbEUkxyme_jpDR46RvXsBWiEALMdtdlvD','2019-03-03 01:47:33','2019-03-03 01:47:33','bb7503f3-add3-48cc-b67e-512bed6d370d'),
	(184,179,'n8D3e7qZ_IHilKHKrn2AV6_Lg1_tYB0aVq_o--DJ_f7xDClCL6MoyhVqFkbwQKGxBXxxmvkq3M-qjc4GBFU7SXJ_ORPMLjnw-UmW','2019-03-03 02:47:37','2019-03-03 02:47:37','65a7057e-3112-4dc9-a8c4-db72c4613d66'),
	(185,179,'Gy5uIdPrSvuW7qyMxCoYS4kiU3LCPbfrwQMFPnzc1HJt8kCqGYW53BVaeRjeIdmCxT_McclTZmbqcdiiRSwKYmB2Mx4VKg5VF1Cx','2019-03-03 03:47:41','2019-03-03 03:47:41','41c5b065-389e-4de8-b8d2-ddbb5675a421'),
	(186,179,'PlTyJ8yI9j9vVfhRk7-xLb5zV0l651heFqmdMpbA_oQrg1OjHNluQzu6HrOXgimaDOE4ugJAog2BO7mdAbJO8ujl3xLmSyX85uEE','2019-03-03 04:47:45','2019-03-03 04:47:45','f7f018bf-64db-4a6b-9ddb-378983338811'),
	(187,179,'n2Gm_14_53BrFpv6ni_traXPCzGmVwXa2yB0JZVwdb5AJ53nxzToiJSNtzLAfYm-gCzv0YKHtVl3_UpbZcYP02fHfxZWn8yRpnRb','2019-03-03 05:47:49','2019-03-03 05:47:49','57036e8a-6660-4b20-a2ee-1df502d47bec'),
	(188,179,'f3bi6jD2vJsO72mGLh-U1O2jY9mF9CJdpdFa_UyYQmpWKu7sBvDmBSaf6jEJysCR6_63p3bIJT_DBfvaKRrI1RML3ANG2A5NUCTM','2019-03-03 06:47:53','2019-03-03 06:47:53','4ba15122-29e4-4090-bd7f-e828271779d3'),
	(189,179,'cvT9seASnvZplG94C5rDDZ9v8RjbyZYnCRaQji1JjPmPTBqDej3VOIXNXtRzznMue7dJfk1PIobWV00gABDkceeoC58Q1ZmRwy6q','2019-03-03 07:47:57','2019-03-03 07:47:57','3d222052-5d58-42b6-9d22-0bf7709c5369'),
	(190,179,'asx2ZKt22usske-4nb5aqz2kEPyboESF_MBUUpcJAQFA-HA4e6PzEHFnGusPE9lsrc6izNac5dYFji0RxY8C28rMQTipaQi67rGj','2019-03-03 08:48:01','2019-03-03 08:48:01','65f48d23-ff8e-4004-b8b6-2beb9f1b4486'),
	(191,179,'kBMiEkHR4gKTDfqCoU24oZAyXfPOC6vYuviaVmljTOXYCyMRmz32Ss_KVhD7qhaUH4Ri1ndbhTCHKcH3J3q20RXRfjdPJoEwk-4f','2019-03-03 09:48:05','2019-03-03 09:48:05','10072d93-3065-43a2-9913-6a92ff7229c0'),
	(192,179,'G7N0vbCCg7yS2EtMc9j97nnxhycA7rLLXLHoa9U8Ss1NzwWN6RH_qr20bJUykRW0duCwfOcOk2MoK0GjlCe0a7l5K-yxVEjKpcBl','2019-03-03 10:48:09','2019-03-03 10:48:09','a9e0d0c1-439c-4adc-a651-3f56b83571eb'),
	(193,179,'PKRecVFS1J81QEBfUXpdoqq3GYczQOEddIp6C-EsybTH6jEImcgcFcYP1Ddl_9d_KLYHV5hL4Af_e-dNePCivwDLhSVyfwrIClDH','2019-03-03 11:48:13','2019-03-03 11:48:13','c2fe059e-d45f-42e9-b7ce-e4ea5a6dbdd6'),
	(194,179,'9ur_ZfUkXQXxlI7w9MAwPoSz5qTBi__WORwEI6rQ1Z_W4mTiPhgdnJ7auQHg8CJjkC2AnusbONrg0s_gwU9C9SgO_E-fj8qrHBME','2019-03-03 12:48:17','2019-03-03 12:48:17','539476f2-4d2e-436f-aba2-76229c65bc21'),
	(195,179,'RLSoC1uV5yyg7GKQ5YZiAtlBiDRCkA-NnMkjgRbBW7ymWqG-a4gKih9GYIPXNdt_lp6EFKg2VzKG2Dl_13Y5ffGyozfgrxkWNwXR','2019-03-03 13:48:21','2019-03-03 13:48:21','52181822-9750-49d7-a29c-92c9459b99a4'),
	(196,179,'4TVxzrFLDUqeb0TtfM1TysCqnxffS-MwKrBUA5Kwq_HETvtbhsOYcc4YXHMIi6NiTuJmwcWA4YqFQ5GZoPnHeh4f7K-jtKMX5v1o','2019-03-03 14:48:26','2019-03-03 14:48:26','e2f23216-cc57-4c2c-a1c5-bd2bcf4ab101'),
	(197,179,'Doqw1Kc_3ysTwcdBI2J9Bg9zhR88M7E2qg1Z0plGj7DqnrUm2dQvohuV2Da5pj2eTXGDogJ6pVV9U2rtC0PYqjgOXb6QE67RM5DO','2019-03-03 15:48:31','2019-03-03 15:48:31','e89f05c3-44b3-4c17-8a03-33431aac6a6a'),
	(198,179,'a1KgfxAkqQzPiLk_UmhIqoAhN6yT-BD_hAXJSR6Sf3YuR2zw9qCwEkJ_QxBXHojli8s5Zg-wi2pY6IAy1svZtMFQr03126CeMIFE','2019-03-03 16:48:36','2019-03-03 16:48:36','751bba0a-80c6-4ec0-b77d-ec780d53bc61'),
	(199,179,'sd54CpE0v-VAuEnrXq6KYr5mYsspV-bbX7WEImB1StKpsBfVslFgEXOj4F5Z3bHBMS9YfW5xrjZ6L-fmO1Q0PLnqqZDo_fziwOUv','2019-03-03 17:48:40','2019-03-03 17:48:40','0bdc4ff2-690a-40ce-8407-6b8337c7ffc5'),
	(200,179,'bxi0UwjWTERyXRt_BPCeZ3gII5BDeaY7pCkqw-5rpOmi8FTSI5TJk-rpFR_D79_fYwAFiVykuqZtC9X5bQtFbQI6aCcaBjPnpIL_','2019-03-03 18:48:45','2019-03-03 18:48:45','d4ac4ea3-bdf2-4109-9857-c5dda30138c8'),
	(201,179,'-1tazpqN99_sPY0se7yNiObJPYDeHFPUAgUtirIKzvniHuebGFaCGVknSm4vggKZwsU_sqlPo4PLs9EJ3M6l3I_190yozCrZSGE_','2019-03-03 19:48:48','2019-03-03 19:48:48','a5439361-0333-4c5b-abcc-ec40d7cea3c7'),
	(202,179,'bWDZNF-fL_pQW9mowmAu6BrN-c0UXTzJU5iAwKE_t2TmFJ7UHzzmpXyb-AyZyQYb0OcYMKpDFvXkNpuw_4cPopDiUXWqNbyax8VB','2019-03-03 20:48:52','2019-03-03 20:48:52','8a96345b-e1be-43d5-b48a-ca52dbf57fff'),
	(203,179,'-DbBztxOA0uYy7gZ_yTdROZDsMw8hhwssGdR7bCjGSpoKgWC6j_-azh3h0LWlLMGsmKjDjm1tKsGWPlEqsAP7dtkF0b1AS2iS7by','2019-03-03 21:48:57','2019-03-03 21:48:57','90c27866-4a52-4625-a5e3-0b8c0b0a362a'),
	(204,179,'J2o9cTCL_hJiILhylA_QWUfkWMG1BASW7fekTEnkPOSiAuoJ1Q0GBWYbCXAvCbkddkH_ff5pAWtJNMdl6uCjuCVhB_lrbw7NYLtc','2019-03-03 22:49:01','2019-03-03 22:49:01','5978e599-11ea-460e-9dda-ffea1046b3c4'),
	(205,179,'-2a5DMgAt3ptnZa39bS3pIM0_ypENqrkGa-B60wOjcc5HNBXIbxr4OVpu2iqik-FQr2NHF9-ou2xrtIDgacZgQjxRQa9g6ybGhIL','2019-03-03 23:49:05','2019-03-03 23:49:05','27e9cf95-41de-4b79-974b-4ef16f5b57fc'),
	(206,179,'Uc_qJT-OvTBsBr4Wy5kgjsmr137rTD2mm2jZeeTAy-8ySFSNhLS4pQTdDt1z4i5al3S3Psaoar6uxhnhvhHg9vI4VQTBFHX9LF50','2019-03-04 00:49:10','2019-03-04 00:49:10','7fc79385-3011-4b04-92a6-d4ccf9048915'),
	(207,179,'Zrbir5TvuNYSoJTlEiRo9P13hQ1N2yh-PsDb11tcFJVewvrSTiTED52JQNmI8ssj1AxwyMkrjNFChnws-xoWYE9gA3vjT2fuy3f2','2019-03-04 01:49:14','2019-03-04 01:49:14','26d86353-1c5b-4222-bbee-0ef5fdf3be57'),
	(208,179,'hxvOEd8-CkbP4hN7UbQ0S95Kn0-lxA-GOBKa5qOiamnIbYWNLlDSYO_ji9Ab8EAx7ijbUzq0z8LZI5c87sdINWkcVu_8MoiomPXC','2019-03-04 02:49:17','2019-03-04 02:49:17','86703aae-09e2-4342-a4fb-fe554a405eba'),
	(209,179,'VcZd1oPzD08YtyEDUClH9yujCfs-Tpa9yewkU8zpvcFPj-BdyI1vX-oSw1Ov4IB7G9TxDBjfK9mbs1Av86kiEIBoZERZuuEgFObG','2019-03-04 03:49:21','2019-03-04 03:49:21','1ea7a2a8-b51b-46d2-b25a-c219c2b7ffa0'),
	(210,179,'A9JMRwPrQqTjFjxa1Rir6oRW6wO1yYeoG-MjMexEwiIpDl2_9-Kl4weKHUj7BGaanhVYDKcDdxg-mjaGgAEZSid-BkysHDb9wWiC','2019-03-04 04:49:25','2019-03-04 04:49:25','877ceebf-631d-4232-b144-74f295abcb26'),
	(211,179,'5dI0wb2vfiXsaxobpcbFXyA_NUoul99AMBq2DU16I2PQS24baVNq29pGsxxXYWTTdL7_Suh6k4ja23RC4sjE5DAneAzm2V_fQ0Pz','2019-03-04 05:49:29','2019-03-04 05:49:29','2e02893b-ad54-43c4-b48a-49d8fb14f76b'),
	(212,179,'X-2V2EUzIGfwLbiOmQnwnvixFXASJ09GX8On3DiN68esKuCPacTN7_Qpj0mifO87GPBUqfBABeGQ-bTdskOGxgcLs-h2uI83TgiW','2019-03-04 06:49:33','2019-03-04 06:49:33','70be5244-e3c2-40e6-b171-96c03042d9e1'),
	(213,179,'rKXcTycjtRC7xYIkzE3__KXTK_Q1LMLtlhqwIEAV7RpFubt41hEoN4Cym0ZPOsWQ7xNSq-GWkKHLOjpoRnLaeoo4Ie3he8c52jir','2019-03-04 07:49:37','2019-03-04 07:49:37','2659f19e-46e8-4e3e-bc19-d283dc4f763e'),
	(214,179,'bS3efP7Z6yYXW7Q1Dne-hd4u6ckcV1jVh8qzL9F1dQ4zTgUc0XXz_JMSDtTmfthFDXckas14cSrkTK9jPk8V0TXlkL58eLq64wyu','2019-03-04 08:49:41','2019-03-04 08:49:41','85fb2868-a5f5-40ba-9270-446fc6a92bfa'),
	(215,179,'eWow4Gl1Nfj4OLQlL9yDw7DqRfuQ6w33PmElDt39GZLBvhAt2kAAkk8AV1UuoGSt4-Z1NsSXU6YVUOJgL20OvxtVnsAMKWc3dlJW','2019-03-04 09:49:45','2019-03-04 09:49:45','e703448c-f300-40c6-a37b-78deb1dce488'),
	(216,179,'5k_83_LjPVr3QzcRsdemBOc8uuNlB-svCC0yD3C4hDxMZnQ1x4_kHMRdPgHtk3dlMcyXASKpa-zEyED-PtLGJM1T9DgkwMxqOkgy','2019-03-04 10:49:49','2019-03-04 10:49:49','9f7e9843-e958-434a-8005-bd741827402a'),
	(217,179,'CPN9nHpNKnLPqCUGXhsiVB-SvG1N4Z1IG3179MzlP5qfRELcB6UJaiElkYvae485ydF7mAENr_QKbefrxHIRuzPLWgbOTWK3K98C','2019-03-04 11:49:53','2019-03-04 11:49:53','a916606d-2d53-4e81-b97b-4d04e2961a61'),
	(218,179,'zEJemzFT1kpmnugUbeeYhpEl0LXLkfbnUgrMnqlvc75-eHSQtpYhNUOgDB0ImL_ruu-Bb6PAvk0jl-kloKJoxghFnVIhRAzWqKfZ','2019-03-04 12:49:57','2019-03-04 12:49:57','21be8047-5f91-4a6c-a484-e7b6e4202dfd'),
	(219,179,'nc0zJX269jdV7aP4gy-vMav7pgDmtwA0fXu-sVzBY4rE2tHYmh1EiJv6yNFedrgfV_mun5WD3tz4IWlrvfzFtG2RQL3I6pywd5L1','2019-03-04 13:50:01','2019-03-04 13:50:01','942c9b1d-f3ba-4428-99a0-e6b9d0f8dc2d'),
	(220,179,'uScccV1dYBuBJulwGKwXmIAMK7DfrNXT4mkyFGnk-p9EGdHoG0v5msLd80Y9UUrmZNVl8RaRJ0cSJMnp5LI9x57g8g1VALVHdWGd','2019-03-04 14:50:04','2019-03-04 14:50:04','f838d142-0e84-49b5-909c-bd779678db19'),
	(221,179,'CrUTqhkBG3FrolqP16mrn4xOJAk9sz8yM7EF1U3BOcvDo4KKHaNq0gOb8oUzz_buzpq6M41pNOGLhZKPP3J86G9a6H3V0QJU0igi','2019-03-04 15:50:08','2019-03-04 15:50:08','045b3846-a965-412f-8bb9-8c8c6d01eb3e'),
	(222,179,'Lyh8r7KkVmFkyYJX9PPPbbQ8h3tPNM-UB1BnEFaYHDVAZT9_rqA4XOLYPQ2wIP5o2eaGXNQvtriumHOGuPQTQaZo90hoWMrRAX9d','2019-03-04 16:50:12','2019-03-04 16:50:12','6e14731a-22f0-45a3-82a6-4d29e72ca0e9'),
	(223,179,'I_U9NF5Tqvz0jq_KtxJSWddrXWOvrNC0PmRVT0K-PRSoBpePTyXNhDYfCVWGo1VuEFVEc02wKVMKgFwcWS7-WMYdje02OyXkfDCQ','2019-03-04 17:50:16','2019-03-04 17:50:16','0713a6b5-8f67-4f4d-881d-f0d6d5a71577'),
	(224,179,'sCP5rOCxo1F06AdflAnmpLNIQnZYLJMMwGNDMc6rrpczz73yIudQmNKF889AL7ZGdGdeHzUr6QX0cYJnOFl97irzZIFinIylNl--','2019-03-04 18:51:23','2019-03-04 18:51:23','a974fef2-59dd-4486-a888-fb7ac250f348'),
	(225,179,'yBckFScRSPYhvVgi7OSLZ4W0Yp09KWZnnw2F0VHBRh4Q6mJ52Uvdb6f-IN-rpa8XMLALLBj8MxeF07cKfSFYhZ7F5q7DYSlyYAbZ','2019-03-04 19:51:27','2019-03-04 19:51:27','281e4653-9b91-41af-89f7-96c7832a0d3b'),
	(226,179,'dGfFdi7d9wkfDJOdcY-TnFZIQx6sybWM2Fj5lWczeUZhw14fbJa2BG1vGj1IDZbWyDJqoOF5gQCCj3XmmwHXGBlKRYOPbr_CfjFh','2019-03-08 17:41:36','2019-03-08 17:41:41','2e2b4326-5c01-453f-bb60-e58ae5b54c07'),
	(227,179,'XjxBLhiYrPNe5qFySzp4bAmR6aW8gVIDPjI5Vux1aDiFChLmIriIEGvMbhqLIWBeyL0d4nH842HfCVihQMuW6QOh6S0kRrkgMAzq','2019-03-08 18:41:44','2019-03-08 18:41:44','c1361a06-b0b7-471b-a4b5-c2f8c963d8a4'),
	(228,179,'behvnKWkfymiA_pC7ogszvQGx-4R21_vqY1VvvsTyjyARHyn_BzrVa6x1GyMQkj1a7_prPfcjj8F4E1ecV2N7hzE1s2t0juoZeEJ','2019-03-08 19:41:48','2019-03-08 19:41:48','4ecf3e7a-3cdd-4dfd-9c99-09a66b62d173'),
	(229,179,'An_VPtASViaGlDVZzw5kYpes20ZflkS7PdNy7gGIR8UOwJ3wDMZs7W7U9_YUakYNhwhMv9zhNXDn_A06YV6-PskRqqYKA5K6WVlL','2019-03-08 20:41:52','2019-03-08 20:41:52','c909f8ee-7506-466a-b4cf-e4de01314ccb'),
	(230,179,'vE3ORjBB0HQF2DNAwZtw8cCK5ienJw_xErnOIf0eRsIPbzinyH4AISWkpwqHk8zxQB2xKUsoRXaWaF0PxZeeLQyy9vbS5Bu3jc7i','2019-03-08 21:41:56','2019-03-08 21:41:56','d6c0ed35-318e-4428-8791-2a541febeae4'),
	(231,179,'C_w3WEl3TXtLLvHIHgM1qMaUbtViRbSGos4reZecD2OQ6BVaqB80Oa7X6UAZ3MmN34TOOdGEDTyR12AIj1-Nui1Ah3kfmwLqQBRf','2019-03-08 22:42:00','2019-03-08 22:42:00','35ce3a24-8cb5-425e-9f6a-22804586c1ee'),
	(232,179,'NuWvl4eCWNNR2GoNeqo1qMPc9ToD6CR8Z6XVrDwfcM4z5Ldhfap5I1E2s2SKxLnREbLJ3fqVdaWqJ3jXF1fPDJ7mIRus6RGU5jqz','2019-03-08 23:42:04','2019-03-08 23:42:04','5af1dc69-5cfc-4205-939b-a4711d189833'),
	(233,179,'1mFgaj1ZgxmFNFVydeOyGR-Z4XGQiNBFB2f83NquQk5srezMitUySoHj7BDZvLfZ_jwJyYfjqqU4JEqhcs2hUSUD-FdPDz85Cb6h','2019-03-09 00:42:08','2019-03-09 00:42:08','98edecfd-26ce-455b-a5b4-502a21eb7207'),
	(234,179,'1te397z-TuZa6B3UXOlgKp8F3oHjhU_jjVF94AMmz7ciS_dkN-XWWCNTo6wCDBJbm2hsQmNpfaJCB7ambxtEWNQIBVu2La1a1KHk','2019-03-09 01:42:12','2019-03-09 01:42:12','30afaf5d-eb80-4ed0-b954-abc7fa693ecf'),
	(235,179,'tp1M1jTvVkYe0kSE0DxWjlafD75i_CeeFMOmrz9ggMynBVNcDmrhVj_nH4Jyj35EfRkRT7yheqN5qM8ml8HHrKtyx_PdvAXl_InM','2019-03-09 02:42:16','2019-03-09 02:42:16','fd0a2131-bac4-475c-8a87-ea95867e1fe1'),
	(236,179,'hhu6WD8bEaCRYUdZk6kxBkuXSLgn0GSCxa2VJgnOcSnSa80ltNszaVMTIkpjHdOQURU8sMVHDjNvMgZdyyMwege9GnEPtTem1s1v','2019-03-09 03:42:20','2019-03-09 03:42:20','0141dc45-dc2e-4fcf-9cb9-07b551377dd5'),
	(237,179,'ErQo2GeMtSVtS72mxE5YY-yq_s8QK1BHUxWXM6ihzJ1R9CmufSx-sTczVXIlvhLE76l-rBUYlg3ljf0D-xm-MDSrbKzqVZWiffCG','2019-03-09 04:42:24','2019-03-09 04:42:24','c53b4213-24dd-4861-880c-d76134f20b4e'),
	(238,179,'z9LekRbIsw4D5v1yDZvl1o64Q8_w-RphwH60HqYoTZpBfixIQptHUJ52ThgNS4xTTQawmhhKDICrAkiVqahQFEh0jYN1eKf0MJ1l','2019-03-09 05:42:28','2019-03-09 05:42:28','67f5f015-a70b-47d6-8d3b-8fca2bd83073'),
	(239,179,'gOjz6k5fxLac6x5OJ1NZ4r6wTFLEgGDzQ2812MMhhSpzaI5OfOYiDnhessNtf-h_RmCnMlla0DcBQ7VhM3IButziJ_1K-FPz5wSi','2019-03-09 06:42:32','2019-03-09 06:42:32','a6c4face-1d0c-40d6-a083-5c2d04797126'),
	(240,179,'OT4kvcTgF2dEblwUfGx0vsO0RNRGZ65nYu_RuHmDCRWgolelnyGa97tH-2EsT1vyjuZSiiVtXv6vjV1Wa7eEO5iENLa9KD-0iVj9','2019-03-09 07:42:36','2019-03-09 07:42:36','659bd218-6bda-4593-84e6-65ce57983e21'),
	(241,179,'xi7WxUn7VCeKkOy7S2rYq6mFRYxUqrIxAdDuEGLSBbDauVWNfqzjP6pF5tQ6qJ2jhkok-AUSnAon80pAGsUIoi76ogztKZursL-D','2019-03-09 08:42:40','2019-03-09 08:42:40','6ac76f92-ae25-4b73-9786-81f92d66294d'),
	(242,179,'ovcgas0Wj6pbB_m9U5fsPQvYLh2kQa6j-ikdkq0PCJEZlToIqINEz7P3DUY_CE_rcOO_xAgA2SO8VUR-QFNtPlGk_83IMegq1xOD','2019-03-09 09:42:44','2019-03-09 09:42:44','6dbf1d31-7ef2-4a30-abab-1a9d819c5bbb'),
	(243,179,'N3g3svAIaGAjgYOZMYJtOhrVC3EQZdgyDQV43jbIKkFaUPbcVr3nlHKuuK3cJls3bL6X88YoB5Znl1elywLRNjw7KxOa_u3aTA0j','2019-03-09 10:42:48','2019-03-09 10:42:48','d50260c1-b730-4d28-af5d-e63fdeb8ed97'),
	(244,179,'wCU2Y1Y5yYhkN2bjnfkxYnbYldzCe0kcxGqYiCxUYC2kXScrkuBWqDzlqVTzG8FDYQCxRvicW39T6ewHTLxZLUW6j9u-W9Y6GHf3','2019-03-09 11:42:53','2019-03-09 11:42:53','bef0d6c7-1e72-40eb-9c48-927dba26f5ce'),
	(245,179,'kGGdhWAkSH2Okqp76jylZ0toL4Kr9D9pDLZdzy70bSd802-jtgdxMFeDA61H2Zz6pzMfWynOLJ7SNBQrcxkyNyvTBY5MfeM5Zl4y','2019-03-09 12:42:57','2019-03-09 12:42:57','bdb53db7-326e-4346-85f0-33ba8b2ffc44'),
	(246,179,'Ff6umOAXlCsmcWJ99YaM-WchhBwNEPk2_9FotUo1qo34lGAfQKMmU-BlFPrIPGRQlpncr8uOlG5BijQ62njlGsoLwaorw_yayut1','2019-03-09 13:43:02','2019-03-09 13:43:02','ce82f895-26bb-4bdf-8906-9ee2d22da809'),
	(247,179,'4XHpa7KDYBrZpX7TeHfPZjB9P6xG7L8NBQLJnXGMYWpJkkS8hdj1Q3ucV-lWUoQh1-BebT6CuCMUyWO84uZfzdg8vhayoVwHT_mT','2019-03-09 14:43:07','2019-03-09 14:43:07','39f31edd-8867-411d-9359-2548c0f77bab'),
	(248,179,'A74fT0m9e2Y-XA27T89_NicUJvwAW2vP6Ww9Z2UsPd9ju6RfizAZGZxsjBjY-RHBp14oGlbbMyjqq4An3pFieouC1eqOCQX3Wrd7','2019-03-09 15:43:13','2019-03-09 15:43:13','c6a44044-00c3-4ff0-a26b-bdb9454a4a4e'),
	(249,179,'0foq2OMN1RB58CmWdClZfJ_a7cfn7AUi1JMP1XXgK-DrTqZGVNXNyY2HbUIY0CHkILhTNmccGm-4_-YIsgHNsF55xfVES450OLSM','2019-03-09 16:43:17','2019-03-09 16:43:17','0dec7ce0-7a5d-484e-817f-07cf55cbf7ca'),
	(250,179,'iwVtcNShaEZQN0H20Q8NAC6tEdEEjAu01DS5UUuaUlLeiwcWwq3WHYvpKKKCT3wsToyQaAiWPiElnbIURY8VP1tSPadQQtYbyIqk','2019-03-09 17:43:22','2019-03-09 17:43:22','c526bd5f-4d56-49cc-a5d1-842ab6cf6d8c'),
	(251,179,'Bs-G72jRSvAldtYHlsGSRVBAG9k4nfET3b8ePVt--RxgIOfmlp2VF5f-FKniXyHcmB1f3KtuQIFhUnvYF3ukn9Zahm_s-68BMYkB','2019-03-09 18:43:26','2019-03-09 18:43:26','4b98e213-0661-41e2-a0e4-9966249ce1e8'),
	(252,179,'wrt8x2-ZVO7tqmiQCudsTEY7Cyc-spucSEYYcrpO80iZyaRPPW2Kl0lK0Op1Kndu3u2IgXLWSmsFe5353e5ym_rSXlxOYH4cjLxF','2019-03-09 19:43:30','2019-03-09 19:43:30','5fefa879-f388-4deb-b7b9-2bef5977d6ef'),
	(253,179,'mcc31etA3fOT6Goo27pArN5uBB4-cELgugg8mTVUjBLJuDeFPXYt_BYmfAJUSH1vWI3ETjvyQiEiAwTJ9s_UC1tPb8PLqy2PtgLD','2019-03-09 20:43:34','2019-03-09 20:43:34','218ca0d6-0795-4ea4-ab03-02b076b9cf9c'),
	(254,179,'AXWxwpvWrdnHEyFj5PQvGvgcDWEQjzv_XxCm0LohqKhX5OKX8qopVshltEfV_mtuqxhmbVGDS0g0FVV15UEfeSziqgTk7303D_8v','2019-03-09 21:43:38','2019-03-09 21:43:38','08b648ca-5b8a-4d24-a5c2-a69bc8ff5c59'),
	(255,179,'eGVli6Es7rlZMgyAI_VfqNYp1Q4vLTN6DB4Cv_5-hSkqSdMx0RK4h2xAlWIhS9BfA5jxCbcYVYtHLTL_qPJSEEnSLs5xr4OEAjDr','2019-03-09 22:43:42','2019-03-09 22:43:42','08d286fb-d5ee-49ca-9e52-31f5e381cfbf'),
	(256,179,'mIH912Sg3dvFDHEIVTTf9s6fYuFOX0uXIXGdlT4MGGnFZFbCNJkvADM-5w7dXTL-UFlzRssFuh-1c8MxgKkjnZaRVgF4t4lRlHyX','2019-03-09 23:43:46','2019-03-09 23:43:46','573bdca2-17a6-4cbf-a458-34b2de920843'),
	(257,179,'Bpr01yTBO_y-mlucTvsEvpZizAcALZ0khhaa_GQFPxSv5J4MUepqIfxsVAT2_8Pq4Pm7bfAOs8mf0srS6FK4lMc-VkZUvW5Af8l6','2019-03-10 00:43:50','2019-03-10 00:43:50','3e7fc5f3-f03c-4463-a1df-ee773dc4f8bc'),
	(258,179,'QBS4eHvXtgRB77OQqduF8Hn4exHp_WWVscY2N_HVA65sWqsQiQijYDAHfrYZGSTzn4_udnOzVH2oModz0vMZLhR6NWSW6xvUsD7j','2019-03-10 01:43:54','2019-03-10 01:43:54','cedfb8b9-2e56-48e3-bd5d-88c48b50a488'),
	(259,179,'JP5wGuHiNfcHR4efhQrXQUsKAOKYf74m2lJ_SPQHD2xMburrzMfOOiiXLGQmg_AVvnHD9wHDj2GsiGVw_jTrrQX29YpJxbeAU5I3','2019-03-10 02:43:58','2019-03-10 02:43:58','1db95d26-0bdf-4e26-a693-c2880b3a70b8'),
	(260,179,'pxBj_e8G-zdbF3eyoWZb2YGpQqsox-K3B8aR_nuitIz9wq_EG3ry1TS4p3XTJ-5SXvUoPksJg6ZOpCrPAh4voCMUC824wHUaalQK','2019-03-10 03:44:02','2019-03-10 03:44:02','9388b80c-02c6-475a-96db-9b944c78db8b'),
	(261,179,'cSn4yyBhUGWZCGrFMv_K0QN71bTUWlBnMd1BMIwMwFUmRJ5Tc11Ms6z3Tj7IJgrSAmfDDkqL3dIgOW80DoFWHLHKr6-9I7Rv7Gxd','2019-03-10 04:44:06','2019-03-10 04:44:06','5565dd4c-74dd-4c80-a0b9-66ee48f450c6'),
	(262,179,'IhbdDcUvgSrb0frtpulpf6qQzYBKCUWjxM6P3eBQpwxtgomRriOHC3LdEayh3zNJTOKoyLFeXGOInOCNgo0BvYKHhLiAgScvno0p','2019-03-10 05:44:10','2019-03-10 05:44:10','1e4fc8be-1724-4845-83ab-cef62cf5be31'),
	(263,179,'I_pJ3HXFGc86NBsNbRn05soOuXtIUEg_HGm3MGZlbaUcmquoY3v5G7uJ4OHEu9YnshmDKNIxg-FgMGZXdHO-ffdwkdPL14gzW5As','2019-03-10 06:44:14','2019-03-10 06:44:14','3cafa17a-99b6-41a5-98c0-78f8c2308369'),
	(264,179,'BoWwhQduqZZgT7LZ9T-btceGsjJwmR6DYqwwCnx3ClrX21bDKj3p5GJ72qEMMmFh4usR2jhWBdeHkEp-5OpGP9bzCB76qnnFMpji','2019-03-10 07:44:18','2019-03-10 07:44:18','7046623d-9040-44ad-acd1-4923cc05c456'),
	(265,179,'3AteV1EyTjmyiAJgeraTFizOt4qXhxBmTOTUWpuJ8g8fVqiS3m-wl5r62e9garj_3moicxwQynyORx3BaCQrieVKoNcQdDGuhZ60','2019-03-10 08:44:22','2019-03-10 08:44:22','3557d570-cdf9-4252-bcc4-fb841320cd4d'),
	(266,179,'o7qmgjE0awFge1H8huJMf72vENsTIeWrWQFW4bgblTVQ7ESYaghHD1lnmG2PUyHGtRC2v7IiC11tPqgIbHhpmDZxdvN2mMc8TjC6','2019-03-10 09:44:26','2019-03-10 09:44:26','e6c9e06f-4947-403c-acf9-c3fc45cd25de'),
	(267,179,'aBwNhFiyZIS-lxXeOWd0shpG4jSOuieMSrIRo8JXkJmPyrew_D4Y3PX5WW_xkBs8EuSOAlBrmLTkfZQcKdJRDxklegsBGi2yPf0p','2019-03-10 10:44:30','2019-03-10 10:44:30','86b6e264-6fd9-43b3-be69-29734d06b606'),
	(268,179,'7uVCVA0If3DCX-PSv5n86PcYmiNWheYhN81NWWzHrzdsuq7zXqd1RbsrF2DaRdyfirBDC1IOGB6XtBd0esrHlh636CClKDgBblMl','2019-03-10 11:44:34','2019-03-10 11:44:34','31747d51-2757-4123-b1da-beafadfdd1ad'),
	(269,179,'o80nO7Izt95MBoBqzTd7wSjOuPgu7L6WLWKcQ2bvSHLAwVlq5b8yPd2IJ5zuVgJkAGiL6Jsugx3RqaMdLyLqyvbcEBiuQj1a2rM1','2019-03-10 12:44:38','2019-03-10 12:44:38','0c4c11d6-48eb-4bb3-b3f2-5694fbe12982'),
	(270,179,'FiVLmgeWM3_nAHExVe3gBHHXjVddJ-APuBIRKo24VGMnNlnzVW1oc-ow9RnlPY3aHFWwb8OkTI0pNpAno1LdB31gtXewwsEqxMy_','2019-03-10 13:44:42','2019-03-10 13:44:42','9b867a81-ff95-4dbb-bbf2-59c755a75e0e'),
	(271,179,'9tVy0TAlLHiHtzshxh-ZaiX8ndBAkANVQo_NhAfF5OtknlU0IZOY7eG9IvAmlWA5dp7wj36pDgnuvc_pPU4x0jnjZSpEcCvV22W3','2019-03-10 14:44:48','2019-03-10 14:44:48','2e295f5e-6edc-44fe-bc7c-ab32abfab70f'),
	(272,179,'21EL_6TtVqt-xwThSzRGKOewvgg3JSBexJnkutRbIGi0Yj0LVjGchg85IQmMPyA1IrHWQMlIdXAf7-9yTcVJ5trQ6Az5eigMFCJR','2019-03-10 15:44:52','2019-03-10 15:44:52','ca322373-5e07-4b43-8e7b-025ad580509d'),
	(273,179,'Hk_0RhxtSsdTdm0Bvfj_I1o87jTbSHBMm5WFmWWEM0nlTMJbXWOLLrdDLv80R4-VIVjDfngLCyLNes8wXdn9idRRQyOYYJz6XXRC','2019-03-10 16:44:56','2019-03-10 16:44:56','bdfe3cf3-bb5b-4021-a891-936f1f213835'),
	(274,179,'4ZuaQMWW74NeoKG9tx5tn1kIztStbiItU7oeMlhcUiZuTfGkx3cn0YGi22GEznBPEt4i8UZpj2VKIpIw0a1Y4GNcYdwXiUwLTXzr','2019-03-10 17:45:00','2019-03-10 17:45:00','3f8e9f37-616d-4277-a376-e2a28cfc4715'),
	(275,179,'hsCtDiTVGddqGacvxJkqY6SHf2em7UMUh_lHtA3E1gyjUCcOp0KlPbU6qyq4PZZ3kdhgztjixEkD4_YSkOJUaweUvNDJmt5lnt5j','2019-03-10 18:45:04','2019-03-10 18:45:04','8a627930-0fdc-4a2b-9338-383a6e0c3b07'),
	(276,179,'NISSbZkF7gigygkgUq1FxtI05ltNY7v61Rtb2wyA3X30tNUcs2In0w3e_kTvjCRGJ_ZzAuIZ-C_x6ovi7OEAWgedl-R1MgHUZWKO','2019-03-10 19:45:08','2019-03-10 19:45:08','f3e18819-3a6a-43d7-ac62-1793b7424957'),
	(277,179,'QNBX4UVDmxQjdL4A3fcy9jxmOUXwQxSqjf-VGG6g5OrWA_0R_dTH3_svccBCB830atvnNBide78uvTdm5A9BBhNoJMO43Zc_lAgn','2019-03-10 20:45:12','2019-03-10 20:45:12','38bfbd8e-1595-4e9b-94be-951a04be7bc9'),
	(278,179,'HZV6qOmOeKTj7C9N-3nLJkoy_5Gt91rUC99esiZRZFWps2Ao-pi5rGXGQE41iljZl4VV8cPh_4ZIsDkitMA_LUYxJxB2IzAInvJo','2019-03-10 21:45:16','2019-03-10 21:45:16','80b3b0c3-9f43-47ad-86c3-e0407b568c6b'),
	(279,179,'-54fnzS90f5AMsLr5lDWPRG4cfTc-xLUj01XPnLdRwziGs977Vg45IxZ4CxhrNmmiMOcQl50pkVfpXqK5Vzq4IYkNRru7VuMjpEu','2019-03-10 22:45:20','2019-03-10 22:45:20','bc61cf6b-e5b3-47fc-be5c-23f178b689c4'),
	(280,179,'g9KIm_m4CIhDoVfjpeh754I8g1H5I-KctM-YUHKFBBkrr3S1yt1dqrpOjAAXF-LMNPbq14N9CbcU1yZULxEiLdCFcqLasV2Zedts','2019-03-10 23:45:24','2019-03-10 23:45:24','1f9bb500-d507-4211-97c0-d60548a6d29f'),
	(281,179,'S4b9P9yz7XLji6Q5p9bsMm7nwnQ5Nne0pPbQqTffReN-xMG83ZGwX_Ep25JKW31pMABOe9ka8E_7NTZBdkBdWHvSg9H5RItUFmXA','2019-03-11 00:45:28','2019-03-11 00:45:28','73996051-a09c-4e21-9f3f-04a5465c8294'),
	(282,179,'9WxiH2bYIcTKeWOFjUDg22-oJ00ylrBpHzAhSQPJSFpBWDDD0ZeUFn4pTyQypzubMCreTNczQbk7SA0cbIGfPqE-ecYIVeInbrrP','2019-03-11 01:45:32','2019-03-11 01:45:32','de2bef3e-5bcc-461c-a128-79f31ceb9ba9'),
	(283,179,'LQB5gbTYKwh6qzFfBYVLHdrWFHJh9WocDuRDDGecwA00SAf04QAKCucSjJDik6N-w9PeI4hr0w_s__bYNOLcppms9U3XEzYmEZS8','2019-03-11 02:45:36','2019-03-11 02:45:36','f46abb27-034c-45c7-9eb1-067c783f3fd1'),
	(284,179,'-a0BJ96ihXlNHfQokHn6teivDdp7DHfbqgZ8JLeAPPb_p26FrlWayDZpR_bu-Vuo1EAH-cfQXnwiTCTssQgkqpaFjtpIGJGzBaNJ','2019-03-11 03:45:40','2019-03-11 03:45:40','561e9942-26c6-42df-8f50-5f0c29f904be'),
	(285,179,'f0PzmdLKB3Qr68Wh-jayXuNs1D_MxxKTUIuc_zz_CvgrciM4I6KHFOxkTT_i8gTa8WXU4PWGB4ThnP2WXhCqmXPnOn10T6kKyjpB','2019-03-11 04:45:44','2019-03-11 04:45:44','d478139e-a467-4862-8e10-4f1e955595b8'),
	(286,179,'ueggCdYBlSKSc1K6bo7pYoF_ff_JEmlwADmvPmxrJ-t6JoB2ILWMW13lxId1LNtt7lM2j5RlU6iwzdz9AHWoLgZjOs1fkORnhw_j','2019-03-11 05:45:48','2019-03-11 05:45:48','c0697e99-38de-4b5f-bf12-b953b4c1ad30'),
	(287,179,'_oWbjxNlkfAp1vLlOG-gQBFlsTRCQY86-ShT2Jt0bWJjLno_5jlYJnr0yp3yPRcs7EK2y6gqW3W3XQLJHGNwMJz-xv7fpszsWQ1l','2019-03-11 06:45:52','2019-03-11 06:45:52','d243a9c7-a383-4be7-ae94-90f167da1bec'),
	(288,179,'AOXHNH7WRc1STL1HMO7y52S9iAS89VyW7wI2ymQCwi3zTQr_clQfAuPRY8vPNarhcvvYE0gXfePE8mDSxYTK2DJH3GeRI4lBn6F0','2019-03-11 07:45:56','2019-03-11 07:45:56','1b8f660a-f233-4c6f-8ef5-63272218f323'),
	(289,179,'8Ejxp0qQVhl4UIwVfYC3hyUivLpEi25m3HcJpBAKwNfTuYDp7lsPts-fQBVbrjGQdN4vDgXa6U4zTRnlmVyTOi6eHG1I1KNP2f_L','2019-03-11 08:46:00','2019-03-11 08:46:00','e3f1b4ab-f840-4235-89c4-2265acb1131d'),
	(290,179,'Z0l6g-Aikq0Q5z2qFrEbNgWVaAcR6erec-2_yyWg_hpLfLX6H5fCtGvkVHccwVZ8ZM-QZ8beL-ZA6ZIXI6HH2V4W9VeicHVJncKN','2019-03-11 09:46:04','2019-03-11 09:46:04','1efe9293-92f0-4f0f-b712-fd3400714ab8'),
	(291,179,'rJKqTnd2pzQjNfgN2H3TH8nhxkDO2M8fFed4j5HO41GmaCVDrQ_9TYy4TVjWoF2jvJ-LwnluJ0SlCbqe8Zch4gGSsKCSIkmL557M','2019-03-11 10:46:08','2019-03-11 10:46:08','6c2aeac0-20a4-4f88-86dc-ac2d5d2b8e87'),
	(292,179,'PBlJVneYulaZjenVDAJLQzQfhZ16A-WLf0jR0Fh6CeNQNv8nbJJaW1U3_wWaI7gKaZFlYuaaeRz8TVezF1iebZEvmCLV9Kn_ectY','2019-03-11 11:46:12','2019-03-11 11:46:12','421142bf-dfbe-4e55-8ad1-82961e4c43cb'),
	(293,179,'LrF0IG2xYF_ZsMp7yN8SRIVNl9CRLNvlwIfD0Yq7s0fOJoa4XQVaMsYQ6d5F0msRujOaFR4cQzRO2ul7Ph52UBoPRxm1Lc_TjFhV','2019-03-11 12:46:16','2019-03-11 12:46:16','adebc168-ce61-4ce5-b0fb-234cb4dc17d2'),
	(294,179,'AoNPfx7jXD6PofW35SE_dPOAqi8xaSb5ZpYtKN6UyesFmSTr8voshD-HA6z8YqwFQ32n45Wzmm1V8xw0m-NFpJveP50lDQ5pe2tA','2019-03-11 13:46:26','2019-03-11 13:46:26','55155f9f-247d-4125-b56d-ef45882544bf'),
	(295,179,'4tQkIQmUo73HVfrhTsk-lhK72HlOlLBicD1dbnS1Z3e4mOffxVY7uqobSYoJp7KYjlT2TEAiqW8-UobG3W4mltHxm7qfTFVMaZ2U','2019-03-11 14:46:30','2019-03-11 14:46:30','62cbdfbe-07ab-4035-9b8a-33eb40eeb0fa'),
	(296,179,'whtQBuOiaMbqiPoHzL0i3LarSLouf9hALfjSQZo26dBSWjD8ZvmOkWnk35a0M3Q2q5H-L2pf7mZUiZvDdBGZWxL68-P8FJbD-TW4','2019-03-11 15:46:34','2019-03-11 15:46:34','6b0f7289-a08f-4169-a535-aa52819d6278'),
	(297,179,'aRxnRpL7hPTn5IVwKZHipxxl4n4_qeqWU-3IhjMgIBczoDUVwtTFiV3sKV7wmRjGRIZJNb5LBKMbjzFABlI3zuQC_BaQcejzD3Jo','2019-03-11 16:46:38','2019-03-11 16:46:38','7661ffe5-f91a-4cc5-bbac-e1d629b16a6b'),
	(298,1,'MIV-uL6kjyle4siKpxasDs4G4d7kanCBFdEo2FEjcAz7GZAWkNlSG-euCfRXWspD_il44bALKoKJlVy1rXJO6fMXy1itY2DqhtAR','2019-03-14 15:48:35','2019-03-14 15:49:17','680c1e32-3a07-4145-b0f6-761ae8ed82ce'),
	(299,1,'JXMbLALAU4DFly9nrmsSQKw58mtMkZQpDqt3S-5iQs8Mhv89d-npf1EH3z9Y3fOaR0v4BTkChJ1tFGJ3bqFpQJ0JBACAotA0iTar','2019-03-14 19:21:56','2019-03-14 19:22:28','e4f8cdb6-42f6-419c-9ac1-806b79e4dbf5'),
	(300,179,'mHO19GXy-9zvkqZbOg5bycCbAmevBufFf-7l0Uu5FuZq4qLB83352MdE9gDgfaY3HISgRUILSl4qIzImUK9df2Vmv8g7M3Xq3Izm','2019-03-27 14:54:58','2019-03-27 14:58:19','e9fdd078-f637-4456-bacb-df8e9c4fb928'),
	(301,179,'bLpx57vrGDYkNQEsq7wR2RoqS2CBxOgYoUyo3KAcMd2Y7w2POGEo8_U3tm2jLgfH0uSUNJZUq6OJa_CF2Ik0bbaesaH17eoIwIp_','2019-03-27 15:58:23','2019-03-27 15:58:23','9d530d6f-99f3-48bc-8fc8-b366c1827a98'),
	(302,179,'o4SPn0B6o-IWLy2VuopcwWDI1EyBcn-sC95rLq8is5IppPGI1mWwvAG6P5eixOAry-7lG7KlhGcwpf9WaI15F5AjmSG1iE4cUZEW','2019-03-27 16:58:27','2019-03-27 16:58:27','2c3bb1b1-abea-4492-a7ae-32ed3db9e37d'),
	(303,179,'uymINu9epdngZt1NCZ7xtvziSDoFssb2PUXiCLpGTI9_eJzF8VV5TLeUt7-hgGh3YX0bAw1xNUGJLBcgJFiyd-CfK3eIJqAGvf4W','2019-03-27 17:58:31','2019-03-27 17:58:31','68b27b8e-9f30-4680-a05b-7dbfe3a18e8e'),
	(304,179,'lqMcOQXnRdLtEsBnsE4LVj9OnmId_mFkSNXCuffsHn1zuLbJG4UfF9Z3secprO7YD1IjxHosLYVUlMjIp5yhBOuy-va-WvVm3i4t','2019-03-27 18:58:35','2019-03-27 18:58:35','929c9e9f-fa64-4af8-a35b-dd35843c9631'),
	(305,179,'6PhOp-Z-eEgKC3mNZoWebX7hnLxvwsgkLzL9piEW2pJDjUoOrARH5jezINFpn4_8OKWGP43F30ogeWNJsB7AeUNs1qhMsXTCPelF','2019-03-27 19:58:39','2019-03-27 19:58:39','622f453d-d1ab-455e-9779-c8f425f4e8db'),
	(306,179,'p4XSQD9IYQyhI4zj78CVzSk7cQULSXvpgO--T2dyXCcKvEbKznzt3Q7pvX2U0NoCtbqDCqBTdknGkKpjUcCHqQxVLl1JnRB3HYUn','2019-03-27 20:58:43','2019-03-27 20:58:43','d55af06f-2860-4b7a-a27b-925078bad48c'),
	(307,179,'X6aIbhq1Rjf-VVuSBG8vIfmTzdE6zL6hWLvSF51Qvoi0IBvteJv1hYxfTS4ODwhkIAIPCsVXmaL4Jr2_putNIE7oSgOHv2eYME57','2019-03-27 21:58:47','2019-03-27 21:58:47','acb72779-4931-4de7-b002-76afd60dbd0a'),
	(308,179,'OuK_KTAMndOxwzAWaeJsQIP8fQHF4JFTU_7zFHwsGb9Sl6OTRkOe4WAxSrWqMdBmIq3v9iQBMSjTdf7nlLLpBZXPkSguYUlv6tA6','2019-03-27 22:58:51','2019-03-27 22:58:51','cd676966-f34c-4ffc-85b9-80fd79a909f6'),
	(309,179,'MwxgYnIMLazk1o5kxsD4Kc8OHV3HfmvAEPTP232lnrF2uOi2b3Z0q4u9h-ar8dM1t3JtE1fDkXfNg2A-9ANyzO652uejRrGFWxaC','2019-03-27 23:58:55','2019-03-27 23:58:55','38876643-72d2-4297-baff-553b78484dec'),
	(310,179,'7X_ilFTn7zJ0z7uSx_ApYirNxkmBYFlYOxwplJc8YRM3o2jwe8Sxb0WiP1r1T_oF2LnVOZ4n_YSHUEBnogslZJNeRu_2FOthaQ3C','2019-03-28 00:58:59','2019-03-28 00:58:59','b3d21e34-96b3-44fd-99f5-ea06ddb369fe'),
	(311,179,'1lcJUmOiqwPN2xCxeTb4HOrd7JAQExA_ILo8aGoiBi0qBkAFDHoU7bwLONzQmHOV7SXsnhsZ7k7SDRhvMpo8OBhiJPMqadHW9xDm','2019-03-28 01:59:03','2019-03-28 01:59:03','f0166071-6294-4325-bd57-a53ff8e0ce20'),
	(312,179,'4pM3fp7YDpalzVqbG4kkH1B3Dg1r74dZQNtmM8IsXK73YbUHrdp0h5HLtfhvCpOufa7KPfAWnHZzRbDOi4qfZoCY7rtYwb26uEfM','2019-03-28 02:59:07','2019-03-28 02:59:07','0d69bbb8-c5da-4204-94fe-1c8382cb6f48'),
	(313,179,'hUdslnkrfgPxItArNrb7oDn60C_OLR8E9TNRzrefc_ng3k7Luvs5Jia4luA3iecdI20fx-l-x3FNuF5vKs79Aq6Tsx8rCv10M0kb','2019-03-28 03:59:11','2019-03-28 03:59:11','7768a5e2-904e-415c-841d-c594da007c02'),
	(314,179,'2XVPdyLlILFaRplruwPXh-DhWtnUfb9636qNGHLjbXq-BoCSJzAl1AhGF6qe4sTin_mjKH_dVRsEuYEVFcyaKSP2FqYj7rfeHaT3','2019-03-28 04:59:15','2019-03-28 04:59:15','ee43b104-9571-43a5-8e29-e13ebcd95caa'),
	(315,179,'cYpsDWDX8cucaPnMoTaMG0XT8Ft4-VKnqnWX35ShdvPPRjpQ0Mhou_kAXXWtt7ao6OQ2FM08Kj6n804jM5JeR6oW3UvvyUSbkfML','2019-03-28 05:59:19','2019-03-28 05:59:19','37df0972-21fe-4beb-a273-cb49e96c7ff2'),
	(316,179,'KZguaXB0Mogv5b3DrJt7yzbcNOU40hAIosreJE_XDXBsjpF1ExfXzUXBEAg1YS6SOfOPz5cAZrpt0b8xzACuU8gg1GUcjt7gE7lh','2019-03-28 06:59:23','2019-03-28 06:59:23','df3ff174-ad5e-42fc-af9f-d3670cda61d4'),
	(317,179,'txY8Vi4di9jVZUqy82b1_8mf0_u5tnqjWHh6b8wn4LiPNlBKdNW5aJQBR-wkv8FRkezzpmfPyZHWT_CFgd-SlkZuc0sY7jEAZR-R','2019-03-28 07:59:27','2019-03-28 07:59:27','40f18588-a0d8-4a75-9141-4cdd9de5bb34'),
	(318,179,'_jNK8OOtAjI70i_LgiOajcx58XYnFVHoRvfrFeLAlz8f07MD7SsCu-4he-Y6qFpfpZij1n6a03f2UuDoJgB_9WqzO8X1XMq6vqfJ','2019-03-28 08:59:31','2019-03-28 08:59:31','be5c705c-84b0-4d37-9682-c03b19e562e2'),
	(319,179,'0d8HC4Xs-4PRTPWsa9rdf0BcxYbHQupzBgGQSc6d9gXQfIMttmD3mx6Y4quoObpn45z0WbynZugLQ458EEBQHJ6PXRK0AF0rGbd-','2019-03-28 09:59:36','2019-03-28 09:59:36','7aae7bba-d435-4159-ae97-731fab1e2986'),
	(320,179,'A0_yvFDXvyfmvOQdD20v9csx7vRlach6QwJ8qgUbPBhBBal054l9nZwvvpCDluEKtARq1aR4JjnfLTcufXYxqo5Zb3sa-KD6D6Hb','2019-03-28 10:59:40','2019-03-28 10:59:40','ebb7eaa8-964e-401a-94c1-f6897f22344e'),
	(321,179,'82t08UDujJuxu_Ap6laP6R3bgo7x-VmXN8-Eb_wqkq9gukODjfSZIJtNRfl9uaeLhInwbVSshDlZkB2nZToSq4BfIXDJNj5Eqfmb','2019-03-28 11:59:44','2019-03-28 11:59:44','6342e15c-e333-41a3-8820-917dd941a1c8'),
	(322,179,'CRbrwf5VUatUDDB41dEK6EjOR4hrTUr_LlEA9M3hA0_ktg_ockTBlBL3iWt3lUthEEsGvX7k5dxvwktMzRfdrCiMj3HuRTSoMlyp','2019-03-28 12:59:48','2019-03-28 12:59:48','22a0aac1-1d00-41a1-b62b-fed295e1ce9a'),
	(323,179,'yDDh3ok27X_4cTDf061dAItU17EerBtbr-k6pMX6WbmzA8SrHcg1eFaYimWmhqaRG60OVmryXSqOseBUvfYzufOVikivdYyfWAUJ','2019-03-28 13:59:52','2019-03-28 13:59:52','682cefe3-211c-4ca7-841f-48de2459291c'),
	(324,179,'L6bdhIUyC7QAEQQiHvALWS6OXiF08nWYp3rk-TmPFUcZLPpr1rYLv0eIszrCAJ0oB6_TA5T0fHrJlA6tIT9agpbigMAzFXlEg6JT','2019-03-28 14:59:57','2019-03-28 14:59:57','d120f371-f7ec-412f-a755-1a0265b80cb0'),
	(325,179,'QrH8exvfY3qbfPk6oCY9y2o9VHR_tJ1XnvJxHKtj5Mk8eosygmS6hf8g6vy_O5HzoHHPycidkqMpUHwiOEtxoBedCmupy5mjd4ic','2019-03-28 16:00:01','2019-03-28 16:00:01','8b7d2194-e779-46b6-a4dc-56749f3dda57'),
	(326,179,'dDbyxZkz-VyBo0noO_P-Oyhpvev06U0ler3H_0n-8ulMBwDh18cff6OcA_9bWMiqR02gP9hEt8eRuDFb4nZYkEyl8q0FCtzwZ6co','2019-03-28 17:00:06','2019-03-28 17:00:06','006ff174-ccd2-49b8-9cb2-cf5bfaa2a5ef'),
	(327,179,'1N5mqQbe2tfRB6MUUOq0PbtZp982MKs3tUY8UUz0utWbiz1-rVtUJod_OIRCxPTjdKEOdUWitsA8uRHA6Cs8jl8nHqIQ4dIm_ypp','2019-03-28 18:00:10','2019-03-28 18:00:10','8353d01a-4b88-4d41-b77b-109ea50e848c'),
	(328,179,'LQuogGb0g6GnXT3F9OEjlHPcvVrE1ow7qwvkr6buGAoABNcfQYpd0EL25M0SoWDBFFWi1_lCi4JBjimv7nvtHiHrS9_OGkEGmyHh','2019-03-28 19:00:16','2019-03-28 19:00:16','56e0497f-78a8-4a1e-9fbe-3940c8b0706e'),
	(329,179,'y706xdjNkWrR6tg-U4pQP7vmgY6bUZGPS0np8wbtH8iykEl42Dq8TI3BRW2qUYZPwrqjQVjxNyzEQ4HsXCCA9PqEMCIAUhfFdwAs','2019-03-28 20:00:20','2019-03-28 20:00:20','10defaff-04b1-4ecf-bc6c-5c885da8961d'),
	(330,179,'vl6omDNP_SMWB36oMHc72y5tujjka1TWmIB5nb0d3TDj4MCVNxxobjDBs5_GhwQ0SqzeHFkSvSYQ7FDVWXB7hc5FHxT_RZ87P53U','2019-03-28 21:00:24','2019-03-28 21:00:24','d995ec1d-2c29-4a11-a323-125042134f77'),
	(331,179,'gnRFQg-uEh4yj1h2yDcgjSRaW0NTNM4t8KHp_ouMx2ygs5epKWIMX0_ob9ZkKf4fVBOQUVjcnENl_D_THZsi-I3XmiC7HYiUl-kG','2019-03-28 22:00:28','2019-03-28 22:00:28','ab4c58c8-c7ad-4e95-a28e-7ef76674f621'),
	(332,179,'bgB03bvRz9xkr1WoiUQDcTi0UGJY1l0H49DaM25mtqVYx_HTlUL34EHr1XwRwN361wwuTUtZkN3xz0SQxYbEpYZcPUTyMn_Ro1RB','2019-03-28 23:00:32','2019-03-28 23:00:32','e369fb56-f7c6-458f-8d57-2ba14e031fae'),
	(333,179,'ZgruJmBapjKS7jBwcYuhNocshodKq-ur139Uxig1xco7UviM_thk8qUrjQcGPnesIYembIpR1rvMbwiUOSzbdVBD5TjpSawB7mc4','2019-03-29 00:00:37','2019-03-29 00:00:37','bb57e9cb-db69-442c-9786-66fc8099137f'),
	(334,179,'VD32gedSuj1xM0IqKW8WrZXvfIMTzZC8f26LRh2DXNSkegF0orwGNH87fgrWa5nsBdvtHpRicdv0eFwNhj6c8BzeVCRUp7_Pq2J2','2019-03-29 01:00:41','2019-03-29 01:00:41','c0e608ee-7ac1-4b36-95e2-fccfeb2e8be6'),
	(335,179,'b29xbAZDZX_Mnd4ZwI2y_bHanH1QhxFju8D88KKRLjCg9Bzc1a1O1OHBAjrwzJw07rAOgUMZfd6uwuMyJLIcUGeHVgkaSkRpWzFq','2019-03-29 02:00:48','2019-03-29 02:00:48','36f3c652-cf29-49b9-a9ac-8e375b54aaea'),
	(336,179,'rnOUi3ItsZL3vNC78whzmB6dpQ0HuX65Ij6nnYXobZSfs085hkA3BfL4CdtRMBSF1KPnI1lUz_mtFgxq9lt91A-K7jxQ4exmAr4M','2019-03-29 03:00:52','2019-03-29 03:00:52','9cd17ed7-d154-4b62-a659-a0586d7644d0'),
	(337,179,'Xi9y2tgbvfEo2pby4J0QNmIcVwikwnjclVD_Fc1t-pJM846AjJiARGb9j9-xsb5UX3SCIr5xPu62n3HKKpdaFKTgm5Fx_fdJs0uQ','2019-03-29 04:00:56','2019-03-29 04:00:56','c666be51-bb3d-4354-892e-553d4b1c003e'),
	(338,179,'iPlzA4dJfMlxwFROCXE1EWwGt5wE4l-x1odBKyXR2kBcOxTBiN7OGnHCyQIJJf3hKKWYJURs23LZryUetCNPLkMyvo9NAaKDsjjH','2019-03-29 05:01:00','2019-03-29 05:01:00','bf3ebd82-bd04-42fe-a898-8f79930cec78'),
	(339,179,'MyTvqGk13U77B6dbsyze2KBTw3uG_1txSw5xUlkNJM70W8-aiY9Q1YEfx76a3g_4Vk4sDWvdLi8ZzN1bTFKsnG89RPtd7PjWdmOT','2019-03-29 06:01:05','2019-03-29 06:01:05','6b3da7d8-6268-4358-8533-8285f2c36ea7'),
	(340,179,'bhrS7JT_lXs8EqDZJ6mVa7YDSVUw7JiFq6RFw4RiknwHjcuV2XaBs227dzjzHA6t73uOz1QXPL7oU6RI8ic3sls4CKif1P-Bk916','2019-03-29 07:01:08','2019-03-29 07:01:08','72e4aba3-583d-4a36-84ce-61eef8d32135'),
	(341,179,'-ON-sgvx5wDLKqzHd7LHLp6nyD7TwiMyG8CFLdrwXfSHryjdalpwIv6m-EjRrM9CTwVDuXSm2P5NVDGzjO4frpgFrpCU_ZYw99s3','2019-03-29 08:01:11','2019-03-29 08:01:11','38f85683-8a08-4afa-87c9-64a5d8b651a8'),
	(342,179,'1Aeh__XoSMYv9-v7njMAuFIQo4B5TlGfEHpt6ZnJSlP2kLWRgNpAogpKgYcGzGXx-lThNDeIggdd8L6_bE86Mf90SJBPAibceAuB','2019-03-29 09:01:15','2019-03-29 09:01:15','518ae5cc-2a9c-4cf9-ae6b-c5b11576f59f'),
	(343,179,'Mnq4nFQegFml7IW8Yw50FF-gIr5IZmUPTFujA0XcZvCthsee0R3NDHlT5tTArg9FUupHZDqvVpy6OLyZ374SWrP678qK3tD2FjBy','2019-03-29 10:01:20','2019-03-29 10:01:20','a2e5a40d-2bb1-4c04-8b3f-ae1892d469ee'),
	(344,179,'9vDbSFrX_MwmgO-qZzVbwWrF4Ua8YqFZBBFeW4N5TJTZkLlKg1VRCiJN5vFaiaMVluCXIbA-RL9kkTKEmnmsSU4SaSMQ8Rd9zKfZ','2019-03-29 11:01:23','2019-03-29 11:01:23','a35bfcc7-5d84-4189-a896-624f409ba30b'),
	(345,179,'VO36aIkR9GX49mHDSnnXk_ttb_DY5WraNXPnA7GTmVEXhbywAZQRkYFWmn7OLz3kcnMlzMuKx53XGXdGriM7mlG6opu54jeCXsCd','2019-03-29 12:01:27','2019-03-29 12:01:27','3670135f-ea16-4afa-b0cb-b0bd717f666b'),
	(346,179,'wbgLNj6vaE0VM-IP-W4pOYQ3kc8VsX1-tHQpmTJn_awxpo1hPnsf7ya73beu84jocsAHMeKA9Xbdsxi9888SF-bb_S8MB4ytpm6r','2019-03-29 13:01:32','2019-03-29 13:01:32','7f30d428-3381-4a78-9178-9fe7ed076fa5'),
	(347,179,'uq2TSbBqXTL0dlOZnD5bf2tBd46KaBRI_RlZdb0NpUaIBgLwyN0QhAHpP56q7TY0sJze0HlM47xTX1MBEWYS557VLLWhKUCzj8-C','2019-03-29 14:37:33','2019-03-29 14:37:33','647f881e-6bee-4334-ad78-0dd6ae0f24be'),
	(348,179,'6fQixGmkRiT6UKr6O-9eaXNFAl-Li_iEgUasvvNYi1NNlGqGwe6bN7CAy4aBtQ05N_SldDz6oXwoZXu1r5or-G6MG5paoN6kJSAc','2019-03-29 15:37:37','2019-03-29 15:37:37','23358610-105e-47c4-9c23-f9b7d0a792a0'),
	(349,179,'5mSkerfe7cb5pocrMk3BY3GDfJB1U_wAcnEfKns50XMJa69B5ZWhFzk4KGC1HZXCaoeEHLft5p8WDQgog-6cxdt-6wwIwUJq1_Rs','2019-03-29 16:37:41','2019-03-29 16:37:41','f5708a3c-f046-4655-8615-050d2255488f'),
	(350,179,'29F1jqMgvtnqOBTJZZZ8CeBeunKxuFtipOxR2NCoKN_h-gpybJVNQmT6KPFnlqlRHj3g_Bek4Edu0OHLAKTOmul6QOARLXSlyFiY','2019-03-29 17:37:45','2019-03-29 17:37:45','ae4b4dbd-0abc-48d2-9d23-9dde3e788119'),
	(351,179,'QRDvuXRHbJXm6P4Yk31yUTrGu-oi7fN8ijzCSAjdWqp108hQ__fTfIhTFfesGcT2qeiMg68Tb3QyPoWXxXWzA0D4Tyj13-1uqyBj','2019-03-29 18:37:49','2019-03-29 18:37:49','104f0693-b71b-4eaf-aa1e-2296f999d120'),
	(352,179,'cFXHfn5-es4pvndI_dYkprLtDM3lrioa6ZoggCfV-A-GmQmZ0pqp4ABx9riUkQjxPbRN-RGhcLhNpROpPbvGsrbYXOLjE52ZiAug','2019-03-29 19:37:53','2019-03-29 19:37:53','57ef69b6-f6da-4bda-bfd5-38cc8755f36e'),
	(353,179,'d07O8AcpE0sl_5eAKejKWyGJchTY7GDAcuIKSJOO07HUPOhU1ohBmol54bJCJTL-l80fPRh3HSEJ_xtl_Ewov65M0ntGW88B6knA','2019-03-29 20:42:20','2019-03-29 20:42:20','b85a011e-4915-4d23-91fc-dc3182a52660'),
	(354,179,'tLyqBNB73iOq00vUa6si2VEslGiDhE-jND76Ih1EUAb4_QGC1PhU0LWX_0W6U5XCjXXACm_oooqsUxPzGcWmaiKPvqL186gATVc7','2019-03-29 21:42:25','2019-03-29 21:42:25','4931700f-4384-4f3a-b6a6-8f7bec2f5339'),
	(355,179,'UHxzWtb57EEBHQN5qH60WYU67041e46yvo7hsjAHdXBPHQlI9hCvX5rHE4lcpByOCSVkYg-sQXFpVK7orU2sFRYC_iJni1uc1bwn','2019-03-29 22:42:28','2019-03-29 22:42:28','5077432d-dc36-4c5c-be21-2955dd3ac226'),
	(356,179,'MsSR_86AU-jXn2LedzAiyIl6Qg8FKtx6kbBywFOPx-kfdVlL4qLL1snyuwmgm4krJi5PFmEPS9Dlwo33Y5WsHAm6ECySMCyXleSc','2019-03-29 23:42:32','2019-03-29 23:42:32','30a9166b-d2aa-4507-9d70-ab9cfe684cd7'),
	(357,179,'UvUcU3rWzJwwjU0tgGjKLB6wAXi8hhe4ch-JTfTkvNS2oVXaOBBsrU0yYFdzwPT6IcT4oLWCakvLy7MVzau2CLFjAgocM5_IE4eg','2019-03-30 00:42:36','2019-03-30 00:42:36','40d72a55-7574-4847-afce-09b866ee4c18'),
	(358,179,'yxSFq_4R016DexhCR1zjdgX-V6Uip7b_PLpIOzPBuukLBjSBK3eGdMMVwi7dbPBBZ7T824iwfgrR5LskOFu-pc3moMQi4m1x1CDG','2019-03-30 01:42:40','2019-03-30 01:42:40','6c026334-04b8-4271-b5b6-8626214b2471'),
	(359,179,'-GVZWYEa9jx7McYXKY6QesZx0Vxmw8BXb1bunnGatf_4pY72TnoJvMLyvI0fy0F5ZSvr3m4l-f5w6G8zLh9IsjQ5WGa8IYkz1aoH','2019-03-30 02:42:44','2019-03-30 02:42:44','d2be6fd6-ab8f-4328-9900-f4d8ced24fde'),
	(360,179,'Cn8-7oWCGEZ2ywGLYr6wg6uCYmV1rYCXwa0YhdT15neDkOL1tQwaFoQxxSIFrgHYxNwN0_gcwidBNwLQA_3hnaWZEf0fmb_w5Edw','2019-03-30 03:42:48','2019-03-30 03:42:48','32081396-736f-4f2b-bfe7-0aff5648147c'),
	(361,179,'16euNoYxXMcKS-lV932CXc25_C9RlJE0GOhlLRBjOlmhoGm484CFfAJnDBhk7r_8W0sqXLh2ipOmKXjzef0vnJ1Bd5eBWvW2PYww','2019-03-30 04:42:52','2019-03-30 04:42:52','71d8eff2-7ffe-470c-94c5-1a6eb238c1fc'),
	(362,179,'MHfplx4oU9yewi_RXS6vo3U7ga8IRkOXvCYBw4u_gImWdxWEpGLFeQnUN1BrL7-HElyndJFQ8We3FUUgr6aUJdkhX-3GlK9YHFXy','2019-03-30 05:42:56','2019-03-30 05:42:56','0807cf4e-6345-4582-9a04-26de87628bce'),
	(363,179,'MTuV160Iaz9sud9J-MtJBpG4lLWkpRg3MBy9my-ulz_zA8_N-XLMeITjfCEUEMqh69bteEuFXVkFWKWqP8Qr230Lferh7b8d3F1_','2019-03-30 06:43:00','2019-03-30 06:43:00','3f4ecff4-fd37-4c54-91ea-d2bc8a6250bb'),
	(364,179,'p6LQsrTX6AvXg4muXBQW4Gtn8OyTSjYHWHJgC0GovGM0EUGwHSnrzcic7uxku4grFs3P5LZvXbwIm00lVi2yLwuVRDcby-pw6HBU','2019-03-30 07:43:04','2019-03-30 07:43:04','de17a30e-b52e-452a-aae6-1cafc36582d3'),
	(365,179,'56k-yTxH99wu2WOq2Tx-IT2gj7_ctUWsOxgO7Db-mS9HPL9GcGhhBRZcFchB4TKdcO7n_ygryjiJr1MFodA3QrdKZDCW7pcd1onx','2019-03-30 08:43:08','2019-03-30 08:43:08','8040c752-b012-42aa-8b1a-3daa974e1efd'),
	(366,179,'3em7KQI4y7wFTUChWvV8OH5X3Tgw-pIMuBFAOp81k9W-5jb9NnC3YJFFwcYZj4dKSUt5RGZ7a1mIR8PaXILCHdybEC0XEfDdp05l','2019-03-30 09:43:12','2019-03-30 09:43:12','476d199c-7e7d-481f-8026-7574dc1ddd3a'),
	(367,179,'2EH-OXYC_X7ABfSrXeComvDRWJTi2KPPIhmco5HC-jAY_-XvYUQ0RNo29-2RvWUu36c67b7Jk3b0JT0_Zx49RKrX1VKlgXhMYDo4','2019-03-30 10:43:16','2019-03-30 10:43:16','23abcb11-d583-4aa1-b46c-7ff389ecfee7'),
	(368,179,'g4Mnm2kuquQ5yIBCX_m7OoQjElWxzC87J8XnrapXLNGO5cAVhAPPeiuOILpnziLL3JyR6rkTITirwOAKSu8yja9SdTJKPIH6-PwX','2019-03-30 11:43:20','2019-03-30 11:43:20','881c7400-9af6-4a2a-907f-182aa6853347'),
	(369,179,'u54XV1UOlDmcjPdeKOhtEpRex035f5DlgfWK3YIT4cwV_mEVa-syrBilGtWt_8N2-yW32ejgiuqDTXL2AvH97elkMySSBcZ54FWS','2019-03-30 12:43:24','2019-03-30 12:43:24','083cc90d-6c24-4b9d-844e-af3e0a989e9f'),
	(370,179,'fLeQ52g7HLG-3UenVRKgFueJ2VYjavhb7yg2CrPHn5bNqNFYDv0NkRaIIYMwveltN4SmjsSuAcfRR8hxZ3Oic2_WjUJlsorjrxa4','2019-03-30 13:43:29','2019-03-30 13:43:29','a619c556-6938-43c7-b0e5-b8fa263904e9'),
	(371,179,'7fN7wiE0j6TA0DqvTHtq42Pp6Rx0vXNir4dDboNVRWXgLsiKrIPcGPuVfiHztmcx1OlRyaathTkR2pOSpkByBFBxwjpKSxE7PWU1','2019-03-30 14:43:35','2019-03-30 14:43:35','10a379be-31c2-44be-b0db-a09db8fb9e83'),
	(372,179,'9-ACQ2yRm4zMazRJ4-mnQrd7Mh0ErhIMomiVnWdSM_aOsTWP3-1k3iBdBrjaQgEMshgZJ_1eur9hl8P_1evPK_pHB_sB6GdcYSOr','2019-03-30 15:43:39','2019-03-30 15:43:39','a19d52fb-42fd-442e-b08a-bee68b49f618'),
	(373,179,'nb48nK1v0rkwvGi6NWImjg3zl17s562uPvXvR1yQAqEUljGw4SUSt6w2RQDZoS5McBopZIfW7dXjIrULcee-Ar7jM0pZ1cAR6s00','2019-03-30 16:43:43','2019-03-30 16:43:43','fcbddcf5-a9da-41df-b425-d966d7774d86'),
	(374,179,'KVsFLwbEAMwadX6yBYi5sySNfKprzKE3OAhwBK5u2nav94q-dDFi8-zUZMMiSqXWwVyS0_qHLKxzs-EY78tTB8Lglo6gqPgQkg-w','2019-03-30 17:43:47','2019-03-30 17:43:47','1841763d-64a1-4233-b26b-fbd1bffa14ea'),
	(375,179,'XWYmz4w0rMk7UYT5JqOEnlapTxIKteV1-gxbaLD_5I1B8DPVNfCw6WTma37rGv3XFWqNHMXa5z8Pjp2qtsg4ZTU0rfdbbUCW2oYo','2019-03-30 18:43:51','2019-03-30 18:43:51','fb9117bb-8d34-4004-a96e-8b6a0daae796'),
	(376,179,'la1zSIfDDnHR_taPFbOY3QXV-CLCV4hQE9fsEtf1OtrR5OgESLnSE5kCuD63xTYWx-iMoN9yPq9B9sXm2fQqK3C7vRs1fXcIn1m4','2019-03-30 19:43:55','2019-03-30 19:43:55','2b2eb08d-30a6-44f6-b60a-eef301cb292b'),
	(377,179,'hwNyH53gPBoBWyv5UnHExJNuSufL4T1DJL7PzFmoeBHetJREazCqjLC_I3l0tlWTUTM5uItKKcpF0xUNWiwxwtyzP4Ce3DZEnqi4','2019-03-30 20:43:59','2019-03-30 20:43:59','d35742a6-71f9-4d06-b5ae-fc1fa44c8678'),
	(378,179,'vl-eKhLq0yGFAB3hyu8cU_5-TnDMCxswn7xOzqq18buXPhIPH4of9SGobhI36sYLWZbIS0KElsP6-Nbo4tzs8b1s73ehVlmteCjm','2019-03-30 21:44:03','2019-03-30 21:44:03','73d6d88b-8c39-4fdc-a52a-3d3b54712d04'),
	(379,179,'XT-iXEzPv0go_MD2maJSHiqHy9eEz4SR0AnlsETBlvA29D3VApdUiLj7h30zrlhTjE9dS2fK5js70GIZcCF77LqSzb689nbxcAci','2019-03-30 22:44:06','2019-03-30 22:44:06','80e9c906-b336-439c-a0f5-4aa28a8ba413'),
	(380,179,'GM2brqF2mFyHfs33kf53VWqjXxgzLlcHWRCBxEUYoC3tgPDc6WqW2Q4U3Cb4Jho3AF0KKVQFXo545m8Yk-l2zXx_kuIU06mhbgcT','2019-03-30 23:44:10','2019-03-30 23:44:10','1be6caf8-31cc-4534-8dd7-35948f059785'),
	(381,179,'uzE9vgzWYRO-I-reSkOHol94S234wgL3Mn3M_7ccqUtRTHXAsHPUnJj5gUh6SNKa60O1HJUrAx3VCqiTgt9jo631zFlpUHhfqvI_','2019-03-31 00:44:14','2019-03-31 00:44:14','a9928593-99d0-4400-bae5-72f4efdadbd1'),
	(382,179,'kG6gJBZTzKGOu7s2SHc01U7-Ys9iLyhpr0wHowpIxJE_3qvJj58Jhc4-Axk6eQLqt-X7rMjSxGj1j4ElmV4aYjOvP_EU9sL5uZs8','2019-03-31 01:44:18','2019-03-31 01:44:18','3d313d5c-872c-4aa8-98bd-85b6ace33c88'),
	(383,179,'6nxVX-RTI3OBXkJjwvqaC7QAWRjMZxHBkILfISpvuCgXom8Aq9FYOGXIvyI_eumXGPDYnMhQe018rAsfeTWgICKlzk0THZtqHJ5a','2019-03-31 02:44:21','2019-03-31 02:44:21','5db68ddf-f674-47b6-8036-a385f2087685'),
	(384,179,'XuMuyegLjjPk3S0JqUgu1LkjuPBSyh3-KoI431IVVjPFrqTOJh1hPuOiY2nyBYuFY0BX1e4KioGzvbUp5HcPMHTYQRi0pF2xD256','2019-03-31 03:44:24','2019-03-31 03:44:24','9a176e72-b401-4c1e-9270-04cae1f4b160'),
	(385,179,'Ka7YYKWYScmkqYZeygJw_TLFsJXfSKeVyGfyX3fJYO-BSQgXPKiWHZ-YalfB9tZ3RW4s0zwgLMjvc3HWv0sHFvmZ_DMuPiDP5rwQ','2019-03-31 04:44:28','2019-03-31 04:44:28','09297853-5ad9-4233-9766-8b1c00150afd'),
	(386,179,'ehwkv4CNxEAZoGQhZB333xqQptorCqzroEwbXsbWFzauPMhMB_FWG8vTF39SHf5qGNi7QSzejdUTBgE6YZs7jD36vpq0Qwv1lxqx','2019-03-31 05:44:32','2019-03-31 05:44:32','dc2e1e52-0283-457f-8c6d-b7447c2f233b'),
	(387,179,'_9yQhldADRFdjXpWf2XOuh8QavWJHkmsbEDfkUK279b4LkmlSenmCbbmuiND2aTvOFumOZg_VKYOecFxHL4cYWIXs4mICvWJE7ma','2019-03-31 06:44:36','2019-03-31 06:44:36','eefdda47-6fd1-4f67-bf7e-f6a50bacb447'),
	(388,179,'okOz4eBeGXEyAyDRlXZIgTourgMXeU4ZiXOGBYmRNV0pXmrLE71rd7dnNTNdd7yJxLqMD60LdnoEPaQnhOxgStMC5Xrj9kVzigqD','2019-03-31 07:44:40','2019-03-31 07:44:40','6b65ca03-3e48-4041-9eba-9cd0f9fc74b4'),
	(389,179,'vfZjKDe7G2_2K_lZneZcZ_RYHWEn-awDx6V_EeCPGox_bbOUHQy1mhKVeB7DK2wKYwVbPd-6EiCPCHFMNOhS-dkE32KRBq40HV4z','2019-03-31 08:44:44','2019-03-31 08:44:44','f68686a9-b1f4-4366-8774-3958eb19c66f'),
	(390,179,'5GJ9wL0HqiJIyFEvPfQ9jZr3pH7qL-0dNk27xvgblwv_20Ny6xUwJF5yzaPaFM9TuA7O285KMK4cYJNNATqlvgO3lLBvdo8mGVQP','2019-03-31 09:44:48','2019-03-31 09:44:48','82091b54-0c6e-422d-865e-45bc46d3991a'),
	(391,179,'fQLZ4ygU7rEdtRYf14jCRu25VHN-DvCLp1iCHZn41HO883LwnIanpkuqqrQDEwe21qLDzYPNfWjfZ9MAoWBibloZdhAvh2syqcc-','2019-03-31 10:44:52','2019-03-31 10:44:52','24c63db0-21cb-4f7b-9ece-d177a5cf5a71'),
	(392,179,'cnB7LxiMXZpyl1h7ehX0Y-eXNxyi5795Pmkmdnodn1kYfFqHdlVgmYJ25b_xV_WgEvDF1_k8-8xt7TTEAA8oZcbs91Tnm-aO-ort','2019-03-31 11:44:56','2019-03-31 11:44:56','26a60524-0d2b-4c19-82a2-2d932a9b3c11'),
	(393,179,'uTjmyYfGU1CN9WlEXxP7fnOfO00tOvMNcDRPZzHHB9aIicPA1a1rkS9hAOnoAMzS2NzTdJAOuzntA2-uSJvMSkw0p-BKuAvQRO3d','2019-03-31 12:45:01','2019-03-31 12:45:01','b0d1c516-24dd-46de-9628-e918def76367'),
	(394,179,'iyH2jcOBEl0P6OEHhYLqQ_-4qpfQhAZetjQytDMjxKWcItFW4_hsEvkhRkP3E1_55I7AU9xG2yM_ZiVrsmYFyI_Hlpwqq1eNYBPE','2019-03-31 13:45:07','2019-03-31 13:45:07','65d9e431-8b9b-4a23-a8f0-e7ea11cf5750'),
	(395,179,'m6zwUSHW_tMHEzVpYWM3ksMEMtJ25HB1GcwK-3kM3ZTigwMprNBBYBv5wzgP8BILGK_lLGffAUTphQM0P0N3xdCS_c-oBBdUzlaj','2019-03-31 14:45:11','2019-03-31 14:45:11','3c660d22-4b5e-4e7a-aa75-39dc4d29455c'),
	(396,179,'naZhCfxBWtbQi9gg9cBx3_YT_T0y9JiC-EZ_bwzGXydRcqjsGvQh6gVyP8lRGHjB0-6KBmPPJG8Ci6fgW0LXIQ_W7G8wqGYvkZZL','2019-03-31 15:45:15','2019-03-31 15:45:15','5d32f6d6-085e-4983-a17c-251df3fdf331'),
	(397,179,'5uNEaaOs0wXn2mO3aLI50Zp25AGf5ZNwkI-vT9uJwUKMEw63pto-XtHAfRhXO6HtdGr6eXP2TGyhc_J7vhQSnfQBczarUmPr5RLr','2019-03-31 16:45:19','2019-03-31 16:45:19','dd1a2963-9682-4c27-8261-717763640b92'),
	(398,179,'4V--rIhXIvW_OLhi7glNGPa9aGRzQdsqjXEiftQeDcGuArioGA3V2LJGBXfWBFYBGnW0MrOi4YmX1tX-iK6zxKIduvM6sUo8X7yJ','2019-03-31 17:45:23','2019-03-31 17:45:23','277126fe-53f1-4d81-a296-99e3740ba8e3'),
	(399,179,'ShvrIv2zQnWwz9q64OFBAXdy6p-h56iQb63-Yr3FynqMJOka41Y_IkDBmWXSRKEygEa7_rkxJ1lD9lKIno2o_5KekthTbEigTe2P','2019-03-31 18:45:27','2019-03-31 18:45:27','a573549d-ce51-48b1-92b1-1a0c2a0579f2'),
	(400,179,'ebY4Tw0CkxQm7uXK8Aqa9TX8pmhZ7kTReka_jDtflm76hobhL7TNAOnrx-ml46W6l0G1NTjtHaYJ2axduwA7J4LKhQZj8KyD3Obc','2019-03-31 19:45:31','2019-03-31 19:45:31','e70d45ba-a466-493d-8886-20c72647f532'),
	(401,179,'hqLpRzTb6PGaXpyfFDQvwiZWP7iKSPal9qI3qc08Q3kKpCt8yiHJ0mmrymw055vthGOQCff8gQ-KOIYpaju5riPzkPiHlsZp0rpI','2019-03-31 20:45:35','2019-03-31 20:45:35','8d8616bb-7a55-481a-b083-cba7e3f61453'),
	(402,179,'acM-gttmx7m7gDypkXxhWzyiADKz5lc6g-kzucfuBn-Hq9fzrqff7aWJXOJtq-9sScPQVMNpYerP3D9lRo3f6yC-QlzkTj3N1iOY','2019-03-31 21:45:39','2019-03-31 21:45:39','b24bb7a3-c7e9-49f3-b839-00ad3983a7e9'),
	(403,179,'3_d30mp58BUhZxAIMhzgP2uNwKfU3TSE-5Eov2xPqZ5kQkeC_tGqrpZFqUAVvOmtRUVejJktyO9PBC0IwES0akb2TNHN79kLHJnC','2019-03-31 22:45:43','2019-03-31 22:45:43','f36397ff-bbd0-40dd-99f3-f741712e738c'),
	(404,179,'e7kfYx4sSEYA63PYLkokOQ-P5-8Lq4BJdIDkCQiqwWNikfC8cGibYJZ_0Wxjc2iX-VNdBgiiZgks1ClNDrpyuhAPpeBal6p17hr6','2019-03-31 23:45:47','2019-03-31 23:45:47','056ea680-9059-4f76-ad7f-fb21c55d4bef'),
	(405,179,'Zt2Pvm48KFMiDrIBCbFsjXXlttbEhURnjEtcPWuW6u7io76-hp5DwZbLGAH6X2F6KSynvvuwG-SG1wKaLbIP3_E2WlpBSDCLkN5T','2019-04-01 00:45:51','2019-04-01 00:45:51','41dc40f6-5180-4c13-96de-5f2aa5e0e38a'),
	(406,179,'fwnTTjTPskNBoljHljByuNsmiiIcXw6xWU-YqN_vcw6mMqO_J3VUZlAo0xGJFi3HPrsqHmSRxFLUmNVdsvn42V0273ma2aF7Iy07','2019-04-01 01:45:55','2019-04-01 01:45:55','e5b76e65-57ed-4b4f-bc15-81989a7a04ad'),
	(407,179,'yCkcCGz1k-lNMNKh_eOkuwxgh42Sk2pQCtHlA08MoaNYSNxa0xzG7xv7FxJXfaxSxnuPlrOJKAbF4sNAGIMfE3UJnLBmZDJ60f3i','2019-04-01 02:45:59','2019-04-01 02:45:59','eb06048a-4f30-449e-83d1-ba3a1fde26d0'),
	(408,179,'E4XQFLWBmdX9yBb17JmpgZYMstv_3_oYpjSkFnPOcInVQozCzhaUizIeqlXExmeY1ab71Z9zfnICse9J7oQc2pNipOqOqRfZ0L93','2019-04-01 03:46:04','2019-04-01 03:46:04','eb460bbc-3d08-4310-be21-21dd3f2375f2'),
	(409,179,'CPGfz4B59Q_NuTcE_xtYUMjN6Rb1IhCWK2b1Nk3eN2ansOSw1lWNKd97BmA-JuXEDdhwzSaU-rqR87eVFdurQIS7iVydhJhtw67P','2019-04-01 04:46:08','2019-04-01 04:46:08','e80b0d35-9acd-490b-bf0b-27b650b45425'),
	(410,179,'XW_nyurNcDYWLhmOrLRZE1MCmYayx0Tcd7oPWX3WwSub5Am3adw75VCS5Jc247SW1fF5ZloQHWDkY-bO7I_diORF4jBwvOoGNbh3','2019-04-01 05:46:11','2019-04-01 05:46:11','e4ae81d5-b59f-436c-abdf-a74736357034'),
	(411,179,'1cFZLrGNJ8X5jeNlZCFAGLbZGQ-ZV55CPrT0CuvGsmqWEb7F7b43Zt2ZqIcCP-FuasIS1vLv7XHWOjjckqpRWWN9W7PGrrtUbeQv','2019-04-01 06:46:15','2019-04-01 06:46:15','60d2fd3d-bbe3-42d3-b1ea-22882f5c9b16'),
	(412,179,'L0X4VvuHIIwTVJtsTy5xVNAUkJ-HjddcIYz5UiWBCHVuOC1i-rsqNFr-21Yoc0MBv-2vRDclG_0fAcB8GTqzMLEdYYOmHuvh23No','2019-04-01 07:46:19','2019-04-01 07:46:19','0c2974bb-d4fe-4981-b568-7ee436d6aa56'),
	(413,179,'AD6xaBfBeJKM04Jb5pIf6neG-31IDY7DaJ07BZGX97wp0RUH3veQJz543P2hM4UxMbnsCgOENxkMB3QYmenQehCQkMC81I8-MbxB','2019-04-01 08:46:23','2019-04-01 08:46:23','96d7869e-55bc-42c2-923c-8fe9fedeef34'),
	(414,179,'yG2SFu5EkhlFq5hS-zTOeGNTQLhNlQk7YoO3rZntdV6TRzzHHdOncd8U81thncZl9vHPkG0zOjJ9lHuexmjRiIfiA_s0hiCqj5_D','2019-04-01 09:46:27','2019-04-01 09:46:27','660460c2-cf81-4578-b560-1fac7f7e959b'),
	(415,179,'zlj1IYrwIzX9SUCc44FXUFffBYd6VhIx9ZvuykRdGujcEh5IK3d9yefvWIXzi2k4T_dcnbx5UsFEU5nqcCNnryGioz_T7A1ddvo3','2019-04-01 10:46:31','2019-04-01 10:46:31','1c04c32b-5291-423b-b03b-ea6ae67155e4'),
	(416,179,'n6RnZv0tkljhKSKSBxbQp7Ne6GwmtYnfEPkO-yhMpREUOQVlpyd9lNdS-5dtjHQknoEo0P8KuBKeVOu6dBvbISJ-Tw5ljwOrNa5R','2019-04-01 11:46:35','2019-04-01 11:46:35','2263af94-abf0-4ec3-b416-c3dad9db4a18'),
	(417,179,'aRMoh0bQQ3NvkMaED-sKRDdO5OvT2bhFdxebDoJwi0MUrCNUwtUAKW6H4jVMVLnVrHUfWXLIKQey7oqQV4C79pqmEqTdqTDFFPMo','2019-04-01 12:46:39','2019-04-01 12:46:39','8f41e1da-4e18-4bbf-92c9-97d38e3550db'),
	(418,179,'uJA-axL_kXuCzpH1-h6kx74p8vcrzOYlubk2MghOczzSFgJftUQKoKaYjoSap3JuseTx0-X4-WUkSlq6p7aYFCYZ6GpGFuBOIy_0','2019-04-01 13:46:45','2019-04-01 13:46:45','3e3fce16-951f-4b84-926e-4a01209232d9'),
	(419,179,'o0HQf2GOO_VwtHjrvKDVmFeP7uJvyapZMosQUXz5Vg2crXR7h2lQWAq7S41kuP9h8qAG9v_vwqQkxhelFHAUQD0JWj51mV7GAoHw','2019-04-01 14:46:53','2019-04-01 14:46:53','9c5cd842-f223-456c-b8ad-f1797f17cc2c'),
	(420,179,'iwCr-kLaphcaL9rqCT56kPK0AgcSf4CYqkvAgvG3pm2w88tosq6zGmfSQm6SZaJt3tQHnWwTH3ku_8S9EglJ4Af9XlbAlxhPRnZS','2019-04-01 16:05:59','2019-04-01 16:05:59','1253821a-00c3-4b9d-a1d8-86e25fdf26bf'),
	(421,179,'aEuSPE3rl1zlSglnLVOWkNmz_o7K0oHf7cjqFkfjV3tagmVzQ8uMUcJw5kn8z8zLushrMtgRctU70M-78oZFjHr_tA8YJcQb-3LS','2019-04-01 17:06:04','2019-04-01 17:06:04','05a486fa-a613-49b2-883b-4f57a8800ff7'),
	(422,179,'URWhiJ8otP1F0vURqr6l_g_COoACovM04t9wDD3JdckQ54LKcxChw0RguRlCoo8M9GcTe6q2h5p4_GkOjsjOil7Nj4mnpFA-mLk2','2019-04-01 18:06:09','2019-04-01 18:06:09','7d2c0d52-58bd-4487-801a-b9b1962d1a7c'),
	(423,179,'OWm5a9V8sXXAjCWdDU7qlUfcUM2at9-truDhJyAR3GuRfMIoYHyPXJ9EZDAPem8zqDIQC2ZCI5NUfLyrlzie8x9behG2I5nXeIjV','2019-04-01 19:06:13','2019-04-01 19:06:13','332ea936-23c6-466a-a5a0-7300b9bbc405'),
	(424,179,'m0CvStutb23rmOIalavtI9z2dX6y48ZECcYx1k0gI2cF5NRZ3NLoice-DVW0ADVtxk8Vqk7s7QU0H6cfx3HHurta76zeRIkIJudi','2019-04-01 20:06:20','2019-04-01 20:06:20','ea4406f3-40e4-4953-b9cb-347b04cc44f7'),
	(425,179,'EP3ITfzpD9OPUFatszwgwgfoNSSZU5y4ghYy6KSgszTdtOvd8RxRYesY5DlCIWWkX4xUTF_5xlvC1QSS9mj8ZiDXT8zHNoI3B9yS','2019-04-01 21:39:03','2019-04-01 21:39:03','09126e88-a4a0-4816-a1f3-9ceeb14f5395'),
	(426,179,'wffhgmCAhnHTjIUFKn9mnJEF7yf2Fl_HfH4eppR67RLJEL4mNm4e8E0IOcj9t6MXCcqMMakf8C_K2KfYjKxNg5O0XEReox-_K7lH','2019-04-01 22:39:07','2019-04-01 22:39:07','bca10728-932a-4fe5-bc05-810edbbf250f'),
	(427,179,'4wq2TRF4pS1pVaoP6izETy7XWw6RZFfvkNIZivmTRcS3mQUhURgqMAB72giTu5FZF10jEFJDDJjndjY5cK_uEK3aC-TZHH6ef8pv','2019-04-01 23:39:11','2019-04-01 23:39:11','1ec6b9b9-c62b-4f0a-a04a-e89ae540edb9'),
	(428,179,'bY9x-6kAboczrzpkqywgkez184CbSXVx2ZsDFmUsrdpVzdYKk2hRSuwrVIUOm9Ii9ug04izRtCxA_k9JmT5h_wLZd4qF0geQK447','2019-04-02 00:39:15','2019-04-02 00:39:15','21673630-494d-4c41-af25-3d391f1c11bd'),
	(429,179,'tP7mGmEI9b41bbVP42J1zJBd91xGUHJ6WrQxv0iCd9MKS79xM_ic9K0KkYcgI8NRUQXKSxQr-yVDe043TB6fMTMTAxoEshl3XiFO','2019-04-02 01:39:19','2019-04-02 01:39:19','d82189bf-658c-44ee-944e-d21eaf81fb2f'),
	(430,179,'RzE9rwzg5wb5XnqjSmI0sZGOjXJJ1lcAARwvnIDW8by5C-EYC9Ef2hMO6bFLIPQvk7Nr31k26nk8QJu6t3gbxvUYnUhR4N1TE8PC','2019-04-02 02:39:23','2019-04-02 02:39:23','996943b9-90d5-49fe-9d65-326b46e959d1'),
	(431,179,'m7xEZA-IheGehS00fkBMwW2bbCn29t1y2LPoGuRCxk0G9LY9dbqpekNDknDo2FSzjP9ZbEzmHzSXNeeNyxNR0XrZOS5yo65O_6vU','2019-04-02 03:39:27','2019-04-02 03:39:27','d03e9ad6-7f11-469a-a429-b5c0c0b0154b'),
	(432,179,'boS9H378Nn3bR5ihGOlaqtqa_1XQNZxe6QiAwY1ajycmL3bhSZVSa9h-ZG3JErlZwZs9Sw_ImiPfove5kfwMeVKRu_JEbzhq36Ek','2019-04-02 04:39:31','2019-04-02 04:39:31','55270640-2204-4910-9641-d1cea7b745aa'),
	(433,179,'uLQqOLi7LkFU-vSEVMhBNPbfqijGwYmRvTtscrCIwy3mjyV-NVpwvFMZ6t10XK-FB7S7KJ7AIhRVEfGvvGpGEgAHzEMHbMMVzLHp','2019-04-02 05:39:35','2019-04-02 05:39:35','1ae52a4a-5b94-43b8-b6e4-a038028c7874'),
	(434,179,'VaeflNW-wHTrskWzPLMLdixyDUEoBw9QY4JNncMnh6sWSQei7v8UX6CZFZBMoqCq4DKrOYK0adYqcnrYN91mAI6N3SeqrTE9zRf3','2019-04-02 06:39:39','2019-04-02 06:39:39','f691bb61-ae4d-414c-a4fc-429c5319dc8a'),
	(435,179,'pEi1UahyCaSuwN1CljA-vtMq-a70k6r308hFdbwBRymdsRVqGmn-WsVL5_ZyLqqI8myUXQKnZytKNg0O7mZbX7cLnyXk_xVadX3R','2019-04-02 07:39:43','2019-04-02 07:39:43','6561a0e0-1cc5-4788-a812-a0b9f85ad77a'),
	(436,179,'5H-wLktuIFpjxHBYZgpiA6U4t1Ztbo1uChKYXBSDTIeWJ7anH_rHNC9pGbAKvn67s4yr2y8-KiQBBr7irSuX9shQBW08R4--Zi0Y','2019-04-02 08:39:46','2019-04-02 08:39:46','88d16422-7c7c-4c6b-a700-d995017ccf2b'),
	(437,179,'nj65Ln1A39Uz8i828Avw2In16YTh0ol6NWxdt-Fm85zDFe9jEBz9dJuWZvBeTq02jjo1fBQpJhTDiSf_nyj_L6e64XE5cb0yCWeg','2019-04-02 09:39:50','2019-04-02 09:39:50','b51eebdf-0eed-4f7f-857a-4a87a9f8de81'),
	(438,179,'vhmFeXhwx4jtOXpa5C-wui2_UTaFuTnGybt79hC-letdy9bSVoy0YtlMC0fMtr1D_sHjA0-lYv0ZRNCD8-ceGwqJbwl6l49eD7DP','2019-04-02 10:39:54','2019-04-02 10:39:54','a27f3997-9c39-4b0b-a3b2-bae3e3eb18ff'),
	(439,179,'Uu3WTRwhW1mAUTD0qfh6MqXugfvENY14tS5c9ze5XokGcZzmsKomOKmYMp5XZ6iKFrf-5mtsGSO5xEIctR0rIjNWpbESxPUuirdw','2019-04-02 11:39:58','2019-04-02 11:39:58','7d6e081f-26e8-4c75-98fa-3a779789ae0b'),
	(440,179,'VKqFPNgR7kJHRChVrq5RUJa5IAHC3nlqFDP1_25qivT9BIAyu5Z6yWKZUrhF-iiBBTPFIcrQBahZ9sO1r7I17aHyuxRVXXH8Lkzh','2019-04-02 12:40:03','2019-04-02 12:40:03','daa62b5e-0b29-4e17-bd16-0208552e7da6'),
	(441,179,'FJpNpB1Yh_tSRtxl4-ud01B-s4nQNN-2uJhZ283yAgcUi02PyezjHPTuQJLXs49g5vJCWtQCxcPgHaXHi_R6V7bGhRkTKT987mAV','2019-04-02 13:40:09','2019-04-02 13:40:09','93ab1bdc-402d-4394-a9b5-f0a3b9f1b1af'),
	(442,179,'Ap2Hud1cH0menqij-yNcpLXqs7IKBYe7SGa-S-udyDrw5yx6wrBWJr3x_vGLxGfZLyuLtDtSE9JIysCU0RUNaOazMmNZFTiwepxK','2019-04-03 14:26:18','2019-04-03 14:26:18','894d766e-cac1-4a01-a023-5612e95564ce'),
	(443,179,'NmLxEINsSA5EU4MP1fkHQp5fMLNCcQFAo6U_14A1guH1hYymdDbcWSlYYZlAQ-uHa8iZCCJS2fgI4umht-LJ5GdHb9eSAjetefrE','2019-04-03 15:26:22','2019-04-03 15:26:22','442c3304-7f1d-43b9-bb32-99064f72b0ff'),
	(444,179,'2DXwx3oeuezQIi2tGieKh6KIsy9qfKxNAHgfSaf48k2fXX7DK9_rLieN9PqVbkzXTTxZGVWVW3xSVUzqlUeGOaW6jyLqYTCjoxuQ','2019-04-03 16:26:26','2019-04-03 16:26:26','04cbcea3-2a23-40db-a653-67e05e602e5d'),
	(445,179,'PCuaQ-vC4W7EqYtzjxvIzFJB9MAxOUQQXCS9vPb2u8UXpPu-7F9hxpWIB_jOqPqxy-onX48SLoIhciWW6Q2uwNA79P4z818jyMxs','2019-04-03 17:41:29','2019-04-03 17:41:29','0336f38c-1918-4989-8b80-c7c07528def7'),
	(446,179,'CvT-9dSEfQfzon2aTOXZOnR2umRIG-_kTjWDCl7mGH5lXjfamuCx8EyqT0cq_Y1fhwDApWlAk-YGxbbMay47f7bDxsMrTWRLqRDw','2019-04-03 18:41:33','2019-04-03 18:41:33','644d9159-acd8-4592-8ad2-d28fb342b1f7'),
	(447,179,'NvMNbLh9fwp4kObQeadJlVbd3pyEkKkhixeHCodmQZqh5DkzLAFXUdEpT6Sx3kvnagCDdf9rvkIvQI6crpHPwYRhiUhU7FTfwCHh','2019-04-03 19:41:37','2019-04-03 19:41:37','5eb8b654-f2ce-4099-a432-1e7f9de361f7'),
	(448,179,'nEZSE8qSVjo4CgyWLqD0yjK5qvoJxKLJiLgHqO-8hq0ZYcc3bwghhC2n4pbrnCMPcWFSRhjmxv7jKhP_qi4KkQZ0_o_CBb5O2_xg','2019-04-03 20:41:41','2019-04-03 20:41:41','1e332263-750a-4b05-9cf2-6cd366ae5c62'),
	(449,179,'ZNrL44ACSPO0QpnPrUgfC9cMXqv9YAd63w83kluqrZYiatZAmdjTkpSMb1QbCS76Jwwd5KqHwWdN1hIT7KkZJBAtsO48QYZCEWiq','2019-04-03 21:41:45','2019-04-03 21:41:45','a2d6455d-f7b5-473f-a20d-b9fa218a64f0'),
	(450,179,'1oVHGTHwvNhY84crpW7SKNg59Xr4Xisp5naIggiiSjGHKaE1VK63TWKitIOBDJywOOKGS_4kIDkTP-Yn10M19uzR2QZepn5_lt2R','2019-04-03 22:41:49','2019-04-03 22:41:49','9ad25ad6-482b-40b0-ae7e-2d4e3a410e9f'),
	(451,179,'i_Lg9NneUD0RT705_B8j3D_yK7tfRxbQPOU2MSF5T_YLMjKV-YWQE36-HNrU5Enm8Qk16dLTXxVyDyIrKsq5q7JqD_qKuZQV6P5G','2019-04-03 23:41:53','2019-04-03 23:41:53','d3cd13bf-48f9-4d2a-b23c-f0bf294a4322'),
	(452,179,'uN_DMjEQ0kwfrum0vjPHx9lJq8iHMEJCVwQ7V1c_BLk-BZXOIAEiI9vUQW0zkO0K-CvGc-RNtOm-0pmSG22kbHm1o2D-F1-wVKwf','2019-04-04 00:41:57','2019-04-04 00:41:57','c2f94744-db41-4ced-8c8f-79c6136192d0'),
	(453,179,'UmU_IOzC5gVMmH_n3ZFRNDNc4szi22qYpmx5PzGdvGMmQfQnu3U2sIgu3RXBWjx77oJrmMYR2RqN3XUhAneHiS2-H3eR74P_ckH0','2019-04-04 01:42:01','2019-04-04 01:42:01','30f1cae6-c134-4907-8ba8-903ef4bbad45'),
	(454,179,'rqMw2M6SP-xnJbSQGBlzV94TvGGI4OQk1XDGWdI1uDmoZGrH-4kixx6ZSY_zHVMUf5lLjW75Y2FLqw1D196wxSono_2M5h-2pyfI','2019-04-04 02:42:05','2019-04-04 02:42:05','b8a891bf-b671-4836-b98e-ad9d6f6f104e'),
	(455,179,'jAcaQyjVNKzzQvAU5VWGIwi-gDHzqfyp5z8HcXOwktGjF48Oouz0v83ncB63MBBrhueImCzb7DZATL2BX6E6aBgCPdcK7G_c7z-R','2019-04-04 03:42:09','2019-04-04 03:42:09','9847f2b8-800e-4be6-86d9-8a19b9d8f942'),
	(456,179,'YCxjmCpdZxoYhxhU-7Fy2V8eyumx3SaLn-8ot2g5JOqxCeBetQQzFKRiv47m5xazPT5ZG1sLUwyFsHxY-iqzysAypo6JABxwgldV','2019-04-04 04:42:13','2019-04-04 04:42:13','64113aba-7a68-4e9c-b349-ec33edbce735'),
	(457,179,'fB7uQleKt4lYTHvJ6vNfEO2_8BXXVzXJuqRLnBWEp6E9SQq2or3KJmYhjwkp0et1PhuPiMzK_5t2eIJJxdLhMU_6mHxLqEK2R-sk','2019-04-04 05:42:17','2019-04-04 05:42:17','0ae80679-3647-4797-9ba6-f737982c1731'),
	(458,179,'abMvoy9WYI_TxjnyMg_jYvzxCRkEd960LaW6RUE8NUASiv6P_YOCi0RqfvLwYBQ5M2sdHqJ5PuBf1bJ2zs7ZBlKlAEIZ2zFli9sI','2019-04-04 06:42:21','2019-04-04 06:42:21','67ac94c3-ebde-4997-99e1-6339c8246de1'),
	(459,179,'WQRn-9g0tdwvcQHKl_hRB4JScHAByt6jky29uUY1_jlviQ6YLiOqh1PsTJufiBBhek4mo0i28U34Qmz2C0Q6AbSDLMOeovKXngwj','2019-04-04 07:42:25','2019-04-04 07:42:25','22e5dd75-ff8a-427e-8ae6-11548811f9f7'),
	(460,179,'D4IjYLdMZqTehvKqNVQwLGLO4wtOuPxq0tiEf10En2pdaPtS91nPq090439nFRyo90hXUbIMzEhOAAe1T-a3P1mmQkWNURnHrvXN','2019-04-04 08:42:29','2019-04-04 08:42:29','f7f0eb36-4d07-49e6-a173-1f4f6b84b2f0'),
	(461,179,'YVb6hkohZ6NTkikKYAylB-q9gy2Hmfz7yHx49n2BAS0iEdmqul0FNwrqbr0Icp3Rg-QmBpdQVykFZb7DSGCKDaSdiojQp0BFHiYV','2019-04-04 09:42:33','2019-04-04 09:42:33','6e97932e-683d-4cb1-be4d-8492f65c5112'),
	(462,179,'akQZnTIHRfUQCxAy_pESJd4UiX7oe4F4Km-6Kg1MfUrsUjNx6QC3pjJKg-Wuu_PYAB6NDnAsWNax0awv2tk-gxrk7qp5vwAEZrPA','2019-04-04 10:42:38','2019-04-04 10:42:38','c11cb661-5da0-4248-8b9a-3943b7e73815'),
	(463,179,'AWq3X2M-0ac0VdUPzKKKPlKbhWdns3_EcYtgv8GehNsHjn9x8vbeo9otIxvHWSgGUr08L7FNw66kkDpwu4s7MUgcYV9K2pnBBOtU','2019-04-04 11:42:41','2019-04-04 11:42:41','50f335b8-f249-49e7-8a55-6b707c35f0d3'),
	(464,179,'rMjGF3jcSjNts282hq-jHTCLhpR4mEzMusoeEQBDHw-VeDaygTMphx09mqcLBpn1AV7KhgJ_eYDXjLW-6fVXJhilGxcegxxI9Gu8','2019-04-04 12:42:45','2019-04-04 12:42:45','b4b487a4-8fd4-4834-8636-aaa457092bce'),
	(465,179,'0e5BxnQR5xXsy_PwB7tXxkxn2JcazRDrkePjXWVjpnWS1UQAqz8DUZpTvC4Ym2ebp8N6aJSZpVCb28n0be1lP1zeqeAP9LfE7-k0','2019-04-04 13:42:50','2019-04-04 13:42:50','2fba5009-3b3f-486c-8f01-446da170fb9b'),
	(466,179,'HNwBrvCFd-lZLHRyvZghNpff_jgog9R54K7w1aKcKNQsU5Tylp1F62xVM_0ssRYwzaYknFXjeJfj2KspSC3XMYjRdc1poiqM6DxS','2019-04-04 14:42:54','2019-04-04 14:42:54','679b4fa9-6a5c-49b6-b7c3-ec94564e7655'),
	(467,179,'VUcq18II_ugzWaJ77ak8EsxSBCAKlBqw29Hz7CN2WrFwKorNvh-rmE_O0elbAMgjDWhjP46HvVN06DiHvdT6z9nxDtgAMisLST9h','2019-04-04 15:42:58','2019-04-04 15:42:58','155284ca-0c8b-43d5-9d5d-6686a83b63c9'),
	(468,179,'EJhNvZ5lLLT_VgWVOv-fqCyQMAUVvsdqPTdBV0zVIK7iOSS-GG4iZdNy8MfWjjUyu3EzJ5X-Cx4Ce5CHiOoVQVucTxPXZE8s2_hw','2019-04-04 16:43:02','2019-04-04 16:43:02','f1aef658-49c0-463c-b07e-c3f7f8d99a07'),
	(469,179,'Cc7lwu6Nq24aqeO9rjlPJ2sjawufc0KyVu0fouGrktKhR2MW_Sglz6pCKyapREKALjIEkG5QHHtaH7WWDgmMT7zqMiBKVHQi-MAr','2019-04-04 17:43:06','2019-04-04 17:43:06','56a1469d-cb2b-4fc1-b944-142c0c2e92b5'),
	(470,179,'8V7ShD_J1vYe7BC_Za3T6vdO8k0AsIUnULBkaJzPtIqjF_h9d_Hh-UD2W5Z5R9-_kwrKJtipjewRAFNxq1fgYbYHtP5hzhp8Sb9k','2019-04-04 18:43:12','2019-04-04 18:43:12','ffd2cf74-b321-441d-9cc8-e01952987686'),
	(471,179,'6UnLMMr_iNB3A8lx2CkzBJ0_cmD6-qQDic42xJurbt2QiLn9EYuJj-iYr8o8lHDTV-I5CYuhvXFDcLkEl_c3R_SI10tOPnHkPi63','2019-04-04 19:43:16','2019-04-04 19:43:16','5962f4c2-9b02-4ab7-9f04-a9626595cddf'),
	(472,179,'znGqoPXfXTqCWzaItd_6pqdDfgwL7AvC5R7RHBHPL5XZ7WQLlufcvoPGXNlGxyopUVi2RInPxzUcTlfctaIHyn7lQmr4zr-VBuf8','2019-04-04 20:46:50','2019-04-04 20:46:50','8fc2d1eb-a9cd-4544-9f2b-59f5b78a6c66'),
	(473,179,'gX4tjhmADAEO3mXRuCMgG12Zi62gjtmjcCNQ1u4-JkY0C0yyen6oVxT7H8d1BgAgUS5xeDhS0Bp_0T3in2ZNsfHbNmsnrwN9qvmv','2019-04-08 22:53:19','2019-04-08 22:53:19','d1ce3f3d-5519-4f79-8de6-7fa6086ff6b9'),
	(474,179,'RQ-ErLAxyYvLIaH9p2-jFxq4IEypT3vcFtO7EavtvHWet0JsCzK0EpxwT18dJbO6tbeznZf3OStiYWFRuV1_3QlRCopt3MR8HSIu','2019-04-08 23:53:22','2019-04-08 23:53:22','def8ef9b-9d90-47f8-8898-839327dd9c02'),
	(475,179,'tIcDF1-0uh0Oo9r9bqk5JRb9mJifZqV0qwe6KNBRtc7bWw7pMHm6EDROpiujAAzMuRn6ISSXjOby1qatuW8BS0a1rzDjJIXGk9Xh','2019-04-09 00:53:25','2019-04-09 00:53:25','1dbfb0ff-906d-44c8-89b3-7e02c4ffe663'),
	(476,179,'vBVbvbeYcUDeYNKmQnit34JZIUoohDde0Ltl1U_6LIMtd9-x-QAzfFsGDPUtyFlfD-GS0aCZ98m6F6LF_9FZS4nSCXzJnay_avSX','2019-04-09 01:53:28','2019-04-09 01:53:28','552ce5e9-cc1d-42c1-9273-db9a4691c3cf'),
	(477,179,'ya1phQUpHMwDciQJjOew9ioLCJbBybpkqpEOmJBwVfTbydPME-b83RiXqCESFxLMXGSFQW6qbdKprPC1vEpp9Zd5oDyjGk7dULym','2019-04-09 02:53:31','2019-04-09 02:53:31','0c107cc8-0975-4913-8e17-8d574f715061'),
	(478,179,'SAgPOK1qB9yrXdnQbr41xe-GH3htvg1UzFg1eQYauzx4Hzq0MpcGanKWi0s2Lqkkuo9zE8WhdkKma_5K0W7fR3fWjGgGaJAkkSIU','2019-04-09 03:53:33','2019-04-09 03:53:33','cb95bf57-dd9a-41d7-961e-29b3f30fff63'),
	(479,179,'JHLbVO2L30eyCw7zxsn5Svf9dHk4XDj_MgiwRuBkITFzF-PywbeTXQLLjzqCNxfniG8SPIKACY0zViEo-LD2K00bHSOFiEidMbgy','2019-04-09 04:53:36','2019-04-09 04:53:36','fdc19e31-2100-4a46-bfcd-8c57756b0a50'),
	(480,179,'lcDjtbjDuFiYoA6DAw3KAk7lzF7sO1tpI3ApyN1tuI3Fu_awE8xheqe1FcTToVb4baTNPRZIlAiyg16gJ6a02Xe8v79O-1BeJBca','2019-04-09 05:53:38','2019-04-09 05:53:38','29a460a7-23e5-4f7a-8a98-aeaa23a7a209'),
	(481,179,'AfcgZNOr-XOZkII4WDuQF5a3L42Eh7iZGFb7oc8pvLrVe4dopigVcxaF6vcv-Ef40piArPgpvKVMv7kdUORDaMuI-dK6jQwz2vDl','2019-04-09 06:53:40','2019-04-09 06:53:40','7464c860-3a46-47be-a92e-dd8802f8dcb4'),
	(482,179,'2yMmn2ppXB91EZLmFdMFw3mbOXGk5nY9dtU4Nu3_Gx-xSIaDa-bhXjXFG4QelKY50lImo_dmtB2O4EYFiFhMscPExStlmd4CyYql','2019-04-09 07:53:42','2019-04-09 07:53:42','24f01d46-b711-4527-8fda-4db4cfccd6de'),
	(483,179,'q1VGsUcyZvI3PN7vceYU1vxDZmM5ycROoRBL2i-bEgUOP24VUbFYxNb-_vfsgzRZ4FDLe9NKQjnA92cjehI4pjbWdiWHkI9zrbZb','2019-04-09 08:53:45','2019-04-09 08:53:45','bbf21e41-fa36-4c83-a98a-7c6e3f58d16c'),
	(484,179,'ex5gCtRs0eIk9j2Namuzmv6bgvDjH95RPMjW9u-KUreMQUm1Pnemrr6_S7jJfDEfC-4nB88iUUdRrS3umLp36su6c_s54vtPJZqm','2019-04-09 09:53:48','2019-04-09 09:53:48','12b62f4d-bf16-4bb1-92b9-d4cb15e32f29'),
	(485,179,'Tqyql3oLD3K-GCh4U51TlAzcFIBZOUxhZCOVxQc1HKuqdQw6T-Wv6-_pruK4oWGOXysBh0X083EzU8c7EsYAg1ixdM3TGX5aXpdc','2019-04-09 10:53:51','2019-04-09 10:53:51','5110240d-3b2d-4d7c-ac3f-867ec67931bc'),
	(486,179,'xBd2t3z7jmAlmENQHnOV6ewvEb73e6PxVk1579nz3gJt2zkR3IRDrdc9gxLsOOkSCRQGf77FCZoCPkw61TQvT7X13RW4KLKpv3Uq','2019-04-09 11:53:54','2019-04-09 11:53:54','37ac6130-87f7-46b6-ac64-f4d64528e94a'),
	(487,179,'lVbfkEa2ePFBAFdH_aut2GdF0vxKmyI5rhoqZp-yNOMIqcfgc4LQhIUYEQ5bepV6b6Pv4ec-kvqTERItAez5ndPviAPFqJSbcGog','2019-04-09 12:53:57','2019-04-09 12:53:57','792bd333-dc8d-416e-a29a-ecbc95057170'),
	(488,179,'V82fvo-foFs8j4lwbcjTDBzqRS8oK1CLU6lZhjcORQN_wGyVI5CElQnuL-LljAwh-g6kZr95s_b7ChBon-HUt5KMi9-7AvftWNcT','2019-04-09 13:54:01','2019-04-09 13:54:01','42cdb234-06a7-4f3a-8335-109e995f5973'),
	(489,179,'OiczqWeZC-ssL_qUZkm99XivtxWRLWG9yhvkMoTswMwAB-uJND0S25q4Ls4f_BDMmpC-v0C8viC6q-wwAoky54vL1-ed6CupbI4o','2019-04-09 14:58:32','2019-04-09 14:58:32','2ae45acf-7db4-4c0b-9978-33cf7608b4bb'),
	(490,179,'Anwua15029oE4s4d91BAr9ejTIoWGwHTimWPDwA8wVHdCc3sY--59iFzPO7N4Z9tfl1FRy7_xRcQQOKrVWdW8l77mZgO8FBpjV1Q','2019-04-09 15:58:40','2019-04-09 15:58:40','50837dfd-afc2-462a-b863-3d6e171b4c9a'),
	(491,179,'7bVRO5Ml5vFk8OVCiaN5F8WjTj9rghUCHXqCTUUnymW8JRAyFu02jhcPNO558ljvdP09sU1Y-JBjNz2NYxlmS-F38biTazIbQdyn','2019-04-09 16:58:45','2019-04-09 16:58:45','3c8e1dd3-86d7-493d-9250-39600e938838'),
	(492,179,'WsovWrGvTdphTDZe0My1DKhB2KVCgIQu7qQasSlABhDWbP8lc4Luy7EnLlZqyTrTDCJOS_0YXceZN1tCxZ9Bm22ULhzLrqKyupU0','2019-04-09 17:58:49','2019-04-09 17:58:49','753f7fac-c908-4ccb-a6b0-aaf51aa623a2'),
	(493,179,'6aGR0Dzs8m2M-loc8hdbcgDN_t5iSAtrKEashaDOz-SRvcEzBIXbqNl8BKOWp00Xp03yFjCrUiMDSG20CBZKQyEXeNqtHg-pTJqN','2019-04-09 18:58:53','2019-04-09 18:58:53','891e7733-69dd-4139-aab3-d558077debd7'),
	(494,179,'A9TSBDj7qsICt_xEQ6G6r9q6WSKv9-pxzmyQ6p7xZ_njatBPdN02RRocJJwjg-sP_Bkw7POdWHROtnTca44vZJu0FltehlUSd4g6','2019-04-09 19:58:57','2019-04-09 19:58:57','e0d5fc40-fc00-446d-a1c3-17d25cb99dc3'),
	(495,179,'fknP--9mqjU7IxkzgdPlf6-8_UfnBsYvwR2ieOWmrSa7JF6wMa6ddA56ATDTAdmiQHpWk2MixdDEL3fwhq9DiAguazgmkUGyAtip','2019-04-09 20:59:01','2019-04-09 20:59:01','02fecdf4-f3cc-4829-af25-95f4ae902c4e'),
	(496,179,'ar37cJ5v3g4sM2SmY7p1PIDjVHx_Mo1IZh_6SO-lnrgbXP9tjnQPsJqBrDRpQny94zc4nG3nfQeyEJ2wXnB2H58NDMunaW7viOuL','2019-04-09 21:59:05','2019-04-09 21:59:05','d8331c82-ca7c-4871-9712-bbcf558a74a0'),
	(497,179,'Invfa-cfk3Ec89ADfZni9gOHbJd0fXLOx07OSLVP0u4hTDkVxIM1DTPDhf47nJaZwMjtpoXrisyPl8EfzJrqp0HG8SZyGTj6uN2Z','2019-04-09 23:11:44','2019-04-09 23:11:44','99d3f8da-d01a-471a-ae90-129057154326'),
	(498,179,'IQ9ktDMniycQ1SNMIvQHL7QiB48BIzHx3BGPxzJKX2ynX2WLIBDZ6dPWdgVXyMJ7Bft95NihRvYkswik-drueiMRVHxjWnmncXDb','2019-04-10 00:39:02','2019-04-10 00:39:02','f14b67e3-9e29-42ce-9236-2977ac3b8749'),
	(499,179,'xOpKJhiOE_I9KsjfMEVR3lM02kSCyHHwjPgzdEpTvQM9EPR7u-UO2wGg9-Qvd--HOPkVHvAiFLQ5WtUoAvaYAhigggliYR6lunZc','2019-04-10 01:59:20','2019-04-10 01:59:20','fe0e8fe1-bcc8-4f69-8fb4-038235f3bb19'),
	(500,179,'-Lk_b-5PHW2RLK-Gu6Ap-DOukAxUjvpbofQOBLA42bHAqXUaJQSZH1o0V38ECme0x4c171dnFMZ47GewsDv5U6yW9B4C4wbsyHEd','2019-04-10 02:55:32','2019-04-10 02:55:32','963f25a5-9972-411d-a561-9a2c2a916e78'),
	(501,179,'dr26c-X0hLAhTBs_4z_1CyS10lApuL52KXNNtfPOONHDew8kZ3VZHO9cfK5MST0vjcYhJ-ube7KflQv-e2tfan5uKQQHtVmvHpp_','2019-04-10 04:20:44','2019-04-10 04:20:44','8123cdc6-6d93-4abe-b450-823a525ae5dc'),
	(502,179,'NVHGcrRxRKXjCz_T1CHVUoWieOMLx7NE8S0DGCFpqWYNIvJX0fn7GDGI-K5_ziSnddPI6wTfwgSZVIn1Icw9CELB6xnNW8WPKi0D','2019-04-10 05:36:14','2019-04-10 05:36:14','c5667ee5-0122-4e0b-b7b7-b9d19153755e'),
	(503,179,'xrk8IVdJbyf6SoXEnB1CiisU0lMBaW6NjP4KL_fzrIFazgzt3z7q7JdUOWtujSg3UMoBJCfG7iqSHpwC77YTI2bWDToC17bE1S2O','2019-04-10 06:50:51','2019-04-10 06:50:51','e755d7c0-b663-42fc-863c-bfd51b139719'),
	(504,179,'V6Sxj_bo-4P8va4Z36ZBEouUGVBA3jkJ-ouHstd76-hVPsjAMnsPsU26zD78FFVU_bxghqaNsdlzlk1Mo1v92U1_husyeFkAlHcz','2019-04-10 08:40:23','2019-04-10 08:40:23','75887e7a-c1ee-4600-8b49-b35beabc6183'),
	(505,179,'BDdAXSXvAAhGQLWLJZqyIMR3QQUEf2Zh7gcJQQFI7yw5xmuprSzCu7huIoeWxd-iSGXQ3lUXz-LP6xU-xDW4lM8isHyuMelB8Zen','2019-04-10 09:52:09','2019-04-10 09:52:09','25e00638-f332-4515-9e9d-dd12492cd597'),
	(506,179,'3y6TFje5jYfW85k77X6veiUGUdVGGIoGoUgQnXut3RQUXdxNHXlQKTXkmIrZqlL-6WMwY1eiI_W-7GV8-4LjWfcE113VRf7fpssH','2019-04-10 11:17:12','2019-04-10 11:17:12','354d0e04-b94e-4b7b-a73d-5c4980323802'),
	(507,179,'PXPkB6QTcWrY3Ef5EPIP2Ze7UgkL4zBs7FzeM3ApZJmZ6PCF-y7NYhchO9tTLcJM1nESyHWbkv8jLIy-VMf9AEBPv230-MAfHDXb','2019-06-25 19:19:09','2019-06-25 19:26:58','5c0776a4-d475-4c67-b46e-1aa952c9241b'),
	(508,179,'PMKSTQSCGM053t2pNW3JYueIFI_TCA0Sl1LiDicVqXE2mJo47--Fg7CuRpSFsbGQX31LaMsj42oa4ehV9AORg5bvXSmZ8WvyBNJt','2019-06-25 20:27:02','2019-06-25 20:27:02','56ed55fc-79c1-4fa2-b4ed-9da3555abed7'),
	(509,179,'sZTvusDtdB6v5eJo3kROe1EyKQs__omPW9oP8We5GZA9MP4tWODY_6gmft-O3t3OB5Xvo2ZRaqkELX9kN64bbKKmDkNLZVionuQP','2019-06-25 21:27:05','2019-06-25 21:27:05','538acfdf-0a6c-4308-b096-1bac1cae2ae3'),
	(510,179,'huBXQ5klJl5QEaHzCl7Rs5kEdv1rWh-iaPsIjQHi1nuOLmNdD6cyJzUE4D0_opNAsTvLVWOQBDr726BizOj7snDj0SPqIxzF3i8U','2019-06-25 22:27:09','2019-06-25 22:27:09','66fd8e98-b1f6-4ce5-80e8-dbe3a5d79cfd'),
	(511,179,'Qn-T2k8Gy5wX1qQ1N2t0z1jymo6Qm8QHxRxPJBG60O7-f1AnXIanK2-a1r1Z9rKwxnhXdclonERrT1XrgXwn1VBMM7I6Pau7P0Zw','2019-06-25 23:27:13','2019-06-25 23:27:13','557615a4-84af-4d4a-aafa-81a4958306f7'),
	(512,179,'uFxeyn8DVwr8jZbqM7O0puxyLQQBquaIDahuuVy2eVgTFS3oYc8cqoQVwfsbvVmPfgp2n-7mFB0df_MzY5IqUxDC9YFKxMB4bqM0','2019-06-26 00:27:17','2019-06-26 00:27:17','b1f39967-b75d-4f6f-9d2e-07df3d9dbc7c'),
	(513,179,'TTQLRdgfik0VF61zAYpD3-za0-v4oa6O4BOAGfqIcYaooDK3PKI1X5lVrewZSMGKvMERC0pEunEFSYtWMx_DlUHRxKpCDSY0bUVJ','2019-06-26 01:27:21','2019-06-26 01:27:21','9483ba5e-8002-4f48-897a-a6f34bfe8b32'),
	(514,179,'Lq1nMl6u5CMLZM3ZtWCN7SbMM0IkccqyaJTRV8A3mA3d6ALI-pZ8crojMjzFtylVJDCBYJP2t33fADM0k8ZQLoHcA2-LtNUM0VUE','2019-06-26 02:27:25','2019-06-26 02:27:25','d7c8e75a-fc18-4331-b68d-412530e9e981'),
	(515,179,'jbRJaY1t-NxO9e3DVF0KO3MosU9MG-3hyTA_NhIPNpfqlMnNWucrw3tI0CsP1hf1A5_ImyIozCddv3xzXOMBYr4R6qgqYNMDZEih','2019-06-26 03:27:29','2019-06-26 03:27:29','6b37810c-9a00-4439-b67e-7d67a4f41a72'),
	(516,179,'Qmt4jRFvpwQ3zMX9_XNyowP9zxL03JOlSo8SF98T1nyJN7WDnpZtNlOaoZz_sISmvlS_D958VesUbfcEIy13zOjJrXWr6Y0PEDU3','2019-06-26 04:27:33','2019-06-26 04:27:33','d890b57c-b406-4e59-8228-a507168cf2fe'),
	(517,179,'6vkbrSOJpizhRgoWYUVN4W6or7wThqWh8T54HcnexC0lRXTLhWs77ZDDgL0-lnVvn2YfFPoUgx8J8fijUJ_E0ddlb01xEbpAtpRw','2019-06-26 05:27:37','2019-06-26 05:27:37','6a12522a-a066-4d6a-9558-d71b09d5e99b'),
	(518,179,'ztgvTMlh7MktDNVl2Qz-iVIMmDR8SNL0N0QCcO52CFv2-T--dOaTke2KkuQrjcNLdAS8_h3rOtw9uac7UHAJkp3HyvF_QEPcDxT7','2019-06-26 06:27:41','2019-06-26 06:27:41','ef4b3106-3082-421d-aee6-73af0de4678d'),
	(519,179,'senMBTWw4ot23fod9pX-GJhw4Hk7XjrtoFsTFKBEOuvm_gt3h4d7ZY9wgVYeGYTWkIl20To63782874NtShCBrwB3M-I3LIfEHrF','2019-06-26 07:27:45','2019-06-26 07:27:45','7c65e8a8-2bd7-4c6e-901b-30d10fea4f78'),
	(520,179,'DV8VHXzjNz_64YOTOY7erOQbstXdSOM05oVk7YcoYsK31r1ht5pB1_lfL1vzinKu6WEXwrAcEnpGgVfThkq5qOsKUKmL54KhkOoz','2019-06-26 08:27:49','2019-06-26 08:27:49','f2ea9b99-7ec5-4991-a136-66a8fae95ff2'),
	(521,179,'cVKgv42vJmENVorClJwwBBchQ-LXpvm2Biq7GplNPl1ycU_8rd_QqP-cPGeh6B6xS8vI-tClGd-P0hLUsFmVJvTlGVpNMeTh5r68','2019-06-26 09:27:54','2019-06-26 09:27:54','1ca8fcd6-c2d5-42e3-ab52-69a500fa0800'),
	(522,179,'gm1EeQ3Aok1LYG2Wmb_Wkex9ZUNku5b8Hc_C1rYu7w45JU2HphEtokb-bUrzOfdgAO47RGyJEkPBjeO_U2V9YtjHj0IlF1YKyU79','2019-06-26 10:27:59','2019-06-26 10:27:59','9d792253-6fca-4729-bbd3-b39fb82d9079'),
	(523,179,'X_eMKtlrJLvc87mg3ZcApHoGRKHUfmBfTT8YZBZkJfG9zJQ-NHSN2AXwbG3utqSD3yryebFkrt8H-HQ0zVvo5zBV30Pci4dVDQUN','2019-06-26 11:28:03','2019-06-26 11:28:03','8ee4cc84-0bbe-478a-976a-fd04df3149e1'),
	(524,179,'368-sIUNDB9zuUIInWbkEiMalf-AT2Y6tavXth45uClOowzP_JiSrAlGgYQqNNc_8ODcCv8pvsOcSoKyY1CGC_u22ktc-NHcuzZL','2019-06-26 12:28:08','2019-06-26 12:28:08','02150d56-3e6e-4af6-bc10-36b7a0b4764e'),
	(525,179,'ZUK2XNMyDmwVnY_jeNPeqN3Fd51fNxWucWIrHR3aRTOIIxjCYMgqLvoY3PhsTE63scIPjF2QIImOZhSDKOM1at6NOMZXVPHzN7mu','2019-06-26 13:28:14','2019-06-26 13:28:14','b5c25475-2496-4425-b62f-51250b8c3e0a'),
	(526,179,'caDZDjsQGm2wb-mU3hruvGESWKDuhjS3WVGVNjAJbWj4BBagrj0aSavTQTYRL7LXcLY7aa8Rj5VBAA4KrvPq9KBDaMbUBN2IQbFd','2019-06-26 14:28:51','2019-06-26 14:28:51','127b6501-fb45-4987-8121-a9508becb41c'),
	(527,179,'z0dnTH-m4hqXXLhGJGqrRIBs9xSx53f_J3qoWGp2gT5_hxNzuyuaHiJjGj3bSeRa8XhEBcvqzBUcnLAXLUjg1Cb6pIHyHDUubhc8','2019-06-26 15:28:56','2019-06-26 15:28:56','f28da11c-232e-465e-a33c-285288596d5e'),
	(528,179,'0TFYEBz-amd1YIE4tidWF7SfXytUip0FW5q6x66Rf3-FE6GbFrBEOx7iMn2EnWpL5Vs-e4slAPwhsjAm1ab2IJhM0hqKgu_9ANZe','2019-06-26 16:29:01','2019-06-26 16:29:01','e6bbd43b-4d35-481e-825b-629be807fcc8'),
	(529,179,'_fk-Y2BpaB7t3Bqjoy7RoJHdgcMW46plqtZmILtG5xBhTTRTi7wM8nwGeolFoOtfON6vtMjW-JxVtcxhSxynqTMZMsrRdV3aBBY-','2019-06-26 17:29:07','2019-06-26 17:29:07','1768c9d6-297d-47a2-ae05-81f4d92ce31c'),
	(530,179,'aiyWZUwOJ2lk9NkSS025O-oiOndtJjvwg-6jSY7sVUfBxTG17mdwazDZSr81UeF-_f2l7HymewOQlfcFnaxHU7-Bm2EPBTKGGY-4','2019-06-26 18:29:11','2019-06-26 18:29:11','6ad33c79-1946-4456-8815-e6e65b5b3b61'),
	(531,179,'sEXcqUqIx9VC8e976aOTmKeacOjcDLwUB3fg-dHJKM4uI9lmcl9Cj-AAz1opku92gwnroiN28S-4-J65gvcLR3_CcrPAa-PLnWuX','2019-06-26 19:29:15','2019-06-26 19:29:15','9d3e88ee-a669-4192-9a35-621624c59ecf'),
	(532,179,'U8kxiwntiHza2UnGoBZFerKjOITK1MxNvxKo2XZhPFLlcf014jZY6xs5E29f9eP0XI7L9GHlz0qYRa82VJwT4OyThdDdf63IPHPC','2019-06-26 20:29:19','2019-06-26 20:29:19','7a5eb6f5-224c-4b40-bc25-b0f97035c115'),
	(533,179,'KfINvtHhKhmCpXFHQmAsBCTA-Kedcb7IgwPJDO2wdW6g1hDR5x25PPpmyMecKfovZqbyIY1AgM9Bk6CYwVgdQb2cBWxUUi7AoZeT','2019-06-26 21:29:23','2019-06-26 21:29:23','00398bdc-11bd-49df-a95a-a5d2db7a8f5f'),
	(534,179,'lsrvgx2_1bZXwMwT3kd097V0Q3Iz6SL-qknS-K4G4RtxBcgK8an6mwWGZdDTTWZFOZRXI-696Ap-_xBE-2OVmNAQ2qT8O0P8hLBz','2019-06-26 22:29:27','2019-06-26 22:29:27','592c33cd-4671-448d-80fe-dec225314af7'),
	(535,179,'IZDvEAIbaq_fr_XdlujXLrGF7eIiC5bpt8z2DpMhZ9fkn2dJOuMrReKpVbXtdmdLOFUAWyx8ZtEa7Gm31eIl2Ot4YJmTf5SF4cUt','2019-06-26 23:29:31','2019-06-26 23:29:31','7c56de06-b3b5-4059-a040-4546cc816f0e'),
	(536,179,'U9JUF88gHNMIyZokkA1y01j_LWF2rAtg1uC66dp8Ehxp4Ofuoc5c4W3D8heVbVb7mGiwCsI9jW5aApkuv7g7NeqUPOfatqmdTYJn','2019-06-27 00:29:35','2019-06-27 00:29:35','ce2770ff-7b86-4559-b8f9-7205c0d0d48e'),
	(537,179,'aMO4yq0LqoIk4QW6VXE23eVU0t_kFux11Mjt-gmLbc9xd9qBtXLE3OiYFIUeDw7IrMOY74W7I8K-8xMMQc8oQez0G8eZYbpO0LnP','2019-06-27 01:29:39','2019-06-27 01:29:39','ed348955-b8f2-4acb-8f8b-f850ca2b81fa'),
	(538,179,'MuNnf6zQw5NnJSPBQOxxaoCC_eq3MLVtQ0BSdA8ljhKJ74Ug5EZccl-avsZNMgWpyqJbQKeczawUEVhLtqAn9epXcNY7Gn0m4kKr','2019-06-27 02:29:43','2019-06-27 02:29:43','7349e13e-6143-4988-ba91-1bcec108181f'),
	(539,179,'R2uTiXACPRlRFfO8a_zT8oejoR9sVA3aRD-by8d5kHmKoAfht-81-F2KARKlR3GjTlOM2T8nIoU3f-d0LwWihpAqQahOJ5BOHY0h','2019-06-27 03:29:47','2019-06-27 03:29:47','f643ea82-8e58-4aca-ba8c-936c77cfbe13'),
	(540,179,'iy3nw-0BcGDbNJI4AxTLhCQ6nXIDvl363IymSw1BVfF8TmZdvt4255XpnGxMUxBo1qt2UvWGIb-0NiiSL9AORnHXNL0ptXotyEMC','2019-06-27 04:29:51','2019-06-27 04:29:51','977ed5ab-cfd3-4608-b286-380e7756ef5d'),
	(541,179,'S7MobK4KPKH1zyioNZzog8m6eOv98qbbEWRhGJQsXA386pGskX3nDJYZaEec1eExWxS6GbpUzofyd_bVYxPNsS4AbumqMPc8DrVp','2019-06-27 05:29:55','2019-06-27 05:29:55','92eb5562-f468-4189-9e87-939887708df0'),
	(542,179,'hX_V2lPmoVekxlRZJEljK40-pgQviWc7pkBvk6KQsUsT_Yr9TNPS6m4e_QmDd0t2DKWFVjgwm7rL8wpjvuK0w1J_4C_mhJtHhJpR','2019-06-27 06:29:59','2019-06-27 06:29:59','0f1aaedd-2a51-4503-83ac-16f7897cbe5b'),
	(543,179,'9433wtuzyXLYjcymo5kgmmr4LkNlOndPbgXi-0qrEOzYnxzW7KRHpdaIMcRGKwOPNa3-uui5wtLqLcSP-vDkWUThSWm-Y3O_FW9E','2019-06-27 07:30:03','2019-06-27 07:30:03','79adb013-bb25-4c10-a461-03d1f2bd8407'),
	(544,179,'acfC964DhZuYaLziuAiP1WOEGhUrwCiss_QV6oCddTGvlF1z4O4o70-ugOf6FgKmTWq8IfoQTMhlVOoPjHi8Wz2RNLatS_MZ9ZTw','2019-06-27 08:30:07','2019-06-27 08:30:07','b3eef2f5-0399-464a-8968-91edc197eba8'),
	(545,179,'_1R-UcZSm6jLe8CdacB_W0YdU_N7Dcu5XZI6QHxJeeyq_I3A9L2qrbgkoOCNTYGSA-q20FA3wqyZ8k4R8CP3lfJikC99gVHHUacP','2019-06-27 09:30:11','2019-06-27 09:30:11','ead9c989-2abc-4564-b8df-bf57911442ba'),
	(546,179,'INUc9a712PG_yXJ1gwOApbUhBlNSeF4X3jL3OdPRNyzgTmKk_8YTdUkY9b5HQ-Iw72jT1Vs1UkkqV8NokXPnF3UFEH6Vdw50Mhgt','2019-06-27 10:30:15','2019-06-27 10:30:15','e4ccc8c4-a613-465a-afbc-bc877f4bea7d'),
	(547,179,'HMRcUcQUNkVkxN_ujrM4gui1V7mqb9EnseO7FYK6tsDKnQzIxeXmiIX52_jwjtZamXpcJKk-fltp-DWo4LibeCsrHWvanTgZTQVE','2019-06-27 11:30:19','2019-06-27 11:30:19','edb75709-dc04-46b7-ba50-b8f2f42d367d'),
	(548,179,'GNHTKmKpPYBQLrACQeNhd7D9VYsGXQRaUfHK5aSF9RWoemNvhPbyqjEMMkMfKf-lFIbqyhi9Awj5oHlg1QPdxApUnej_4_KaOSLm','2019-06-27 12:30:23','2019-06-27 12:30:23','75fb840e-515c-4e6c-a045-da21eff1eb18'),
	(549,179,'04hd2YRRsFczjyZ8pawFIyjUaS705c75MjWlBwTQIrSZVZUiYyICOxPsPAWqFDnrfT-w3z1vU0SE1elAC4fXuowAMA0pJ2A7ADyE','2019-06-27 13:30:28','2019-06-27 13:30:28','9f4afab8-9872-4fa6-8373-4dc8ec922f94'),
	(550,179,'g_xUFvfhVWw8yfzncYHl1tPVdDXEQZULLvcIQgdRRKl4t2-rg39vE5mjkAStPjVHXGkWUODqN9aRADNIV_EbqTVlYYxquCmvx79q','2019-06-27 14:30:35','2019-06-27 14:30:35','1748e746-8b2b-41a4-b4a2-567393280c45'),
	(551,179,'anBi4uLcyLgFHJWYZavI05zmjP4o-cjY1IROR-vxknvCOXKkLybntayVOMvub-bLN92r29-7En4awVz3mBmq9rbhAGRNs78-733e','2019-06-27 16:10:12','2019-06-27 16:10:12','8216d556-e732-49e3-a023-312ce2a92e64'),
	(552,179,'Xw7zWsOq7CWPnNxSC1RTM2gC98HzAd_x2Amz-xVmLzKLFa6ZrRdQ1ui7rmzJ22581FtXhG8z8mQGGJRgX-ijUKyyXl2cYOUidre3','2019-06-27 17:48:37','2019-06-27 17:48:37','9e7fa0a1-fc1e-4beb-b316-97c113aa5984'),
	(553,179,'3m__C4dnIVKiKKuOnX-_AkeJTWMe8Rrt-XFQc7U_g671kZUNMQcckLk4yRzYomWn7Bc1v0qa3Sh_514sWfmgNAHbTZ2KHnbGweyg','2019-06-27 18:48:41','2019-06-27 18:48:41','a9f0e82a-ceb1-42fb-babe-6a9ef6796ece'),
	(554,179,'HWjxBLROuEK5MWO71RrVtCqPLPcOuPfmKwK3T7Dzx6NxJ1YrKb13rGX8PytnkGPxZqUlyOqiNI-uZMFoEkZhiEtThDOSP5PG0h-P','2019-06-27 19:49:28','2019-06-27 19:49:28','1645a5a0-5783-4e0e-b33d-0998e5245c5e'),
	(555,179,'JwnEGW6FoZmN4C-HJcCP0Kx_HPSInzKdK4OVR1JWqDMXBgn5Vr-VvTvB3zKFLF4Vie4UjxTvcySP2-5XIGRdkCBLR96z9fxnmGIS','2019-06-27 20:49:32','2019-06-27 20:49:32','2e1eba64-7934-4464-ab5f-5edec9536aad'),
	(556,179,'-FdOp7wS-fMmMG_aeasMUBUclOwjKIlkJCNn6xRvmKsKek7Om3Rs89tWQToSR7bj7jMZFwgCNDDkkSCkmv_j2dmx5NOFjaywLAlj','2019-06-27 21:49:37','2019-06-27 21:49:37','1dbaa21f-13bc-4124-bac1-553814670b05'),
	(557,179,'KLVjDmZoKg19Xxl6KgXvSQSVc4VB6ZMPTQb9WzfZtRZGHgEQ1TL3znLcIg9EV4nDvHRCO4y2vQMT7BrKLLxPj9bWlD0BrBmrLsqP','2019-06-27 22:49:41','2019-06-27 22:49:41','b7704a68-e44f-441b-acb3-725296f60b29'),
	(558,179,'mTCT4fQS0AtqkCQ8KSU2Tsf0motF3EHNT3uNEarsTMpGob89WERkcmcIJoGA0AlsA5bv3sA3NzKTbokhAVihGauEUv7LaqfB0WqR','2019-06-27 23:49:45','2019-06-27 23:49:45','9fc41ef6-3750-4a66-a5e1-09868de5ec53'),
	(559,179,'niG11eSxdk7zyCir9EaXxgnG2ouf9E7mA88p0EOlNB3ZfMpQ4HXeicRHBMNej5AAUtgNrnQ2zYsNBDSUsZ4gFbmuIw-3q8rK0g_x','2019-06-28 00:49:49','2019-06-28 00:49:49','58e74685-ce8d-46cd-b6cf-da9bff64a245'),
	(560,179,'sS_OdfMMV3k1cvjYF4ASrLyJnBJJ9D6wxHymcTwUSVp04kzkD4K5hfbrNklYewDGI9BTLzqba-Ll_Y3cHFvOJWgngRZCSl_sqtw1','2019-06-28 01:49:53','2019-06-28 01:49:53','3cc3d561-20a6-4938-be4b-68e819679bb8'),
	(561,179,'ZFRAyOnCWWgimFYVZ3_8iLbgNptwq3V_sX87cfD9RhctECBU_S36RdAI_5YlPjC3RG-_VgqQOgiQ3hEsrvN8BDJRi254BXd2whmr','2019-06-28 02:49:58','2019-06-28 02:49:58','bf8dc879-a5a4-4df8-b541-5f574a8420c3'),
	(562,179,'wHKcExVjHBSPSG_8eNZ74p0PJgsSAnf2wgJICzCsN35thUoawtmCJ9djyHy4TWYQ8y7pcP9tZrae7qikP_yoyYzS5WuRNN18-9EX','2019-06-28 03:50:03','2019-06-28 03:50:03','45b8b4be-769f-401a-9db7-32df756253a3'),
	(563,179,'peFnQw2FXlhghRAavvXGE7txDY6Tbb0A8gtJI_WhR6QcDhU1iz42ZyRrb1Cu74ikR_Bq4ugII2uDqbxX-C2_gjUP2uTFmc04af9A','2019-06-28 04:50:08','2019-06-28 04:50:08','74a62724-fe43-4639-8d28-a10078eabbf0'),
	(564,179,'HIMLw2VSOXM50Qaa9mIoK9dObd-AvDm0uCgn8mefIWUE9yLOXz4Qca0qIERkvByYC4opfEYxvpY8TW7BxiUdL2qgiFxuLNoAYniM','2019-06-28 05:50:12','2019-06-28 05:50:12','eec93fc9-3f8b-4593-996e-2bd612803aad'),
	(565,179,'d6kvNgfngURYbWmIDsGgbphgs2Vt-q_yw1EubnsFlrfqLtHKox9peaJE33nOzMDbIkE4ph222T0VIKdUf1dhV9PPxAQnEqzXatqI','2019-06-28 06:50:16','2019-06-28 06:50:16','322279cd-8a94-45c2-94a8-5ea2a0f08179'),
	(566,179,'JZCRGQf3G7K0OeOzGnp1LmAToiaViammINt2aLIHhQKKqhmROBZ1HdTIt5hSfvdEoxnKK2TCHIjrh4FycuBSwljtfxPPq_hO12Xa','2019-06-28 07:50:20','2019-06-28 07:50:20','3e2ec06a-8cd6-4ea7-a0c7-24948d8b6cb2'),
	(567,179,'8NXwQRn2-PDgtlYyvphsqMoTy2ctG0Bp2pJrNqpoMw4XfPQA06FWauIBAfiw22xSlw6a29ECEF8dH_2Ahy7J3tdt0GcUkc0tAfUO','2019-06-28 08:50:24','2019-06-28 08:50:24','68e19a16-cc40-47f8-8c4d-add0219d36b7'),
	(568,179,'O2a5JcSMeGJftWBqcjxcj-q2-QtM5Qq9kgZ5-AYSwtcHP86Xhy4jAuovQ2tFAnzVPLslT_8CM5eUVYBzymPm1hIweo4eqZAWgBEw','2019-06-28 09:50:29','2019-06-28 09:50:29','adcd914e-2cbb-4d1c-813b-4f4db4238bd9'),
	(569,179,'AzPOOQtAV8icWIsYG725PPr28TcmUDIl6lqaIyVWJcNDqcin4FWgq57OTryv_3adR6jqTUlDTg0ACYDlHMhhSU0_yBjV9Vc_JmyI','2019-06-28 10:50:33','2019-06-28 10:50:33','b756734d-78f4-408e-90c1-4c711725f6b5'),
	(570,179,'kzACoDMh8K-8ayVdA0JLz7cRt5Ih1pGUiGLrYSeMynM_gmouD_D47-hBck2-nbzQf1iRSKUhSgce2-QpZf2rMgsBpDN15QDkGKL3','2019-06-28 11:50:37','2019-06-28 11:50:37','dc805561-42a0-44b5-b43a-179ce49d4ac5'),
	(571,179,'NLiJQqgeNcwz546ZZwziZikZjqH441Z7ixPRiW51xnYPzzXGGuiecTRqOogi56A7y0bJKO6sFgzQ_MiHqgHPx1DMMRJuY99NuDOn','2019-06-28 12:50:41','2019-06-28 12:50:41','3fa89176-8887-47c9-b954-1dabd4b68065'),
	(572,179,'r-KZnCoPVfTMdIniRkndqvkaE9Fnx5QMGbaDR3J8otNDxoujguAKES6D1HXjVrSZs7UKAz7CBBPKnSaO8v5WT0cOCLwj3kJ9IZdW','2019-06-28 13:50:47','2019-06-28 13:50:47','a75ed709-cc9c-4c96-80a7-c4d4be65f38d'),
	(573,179,'BLBdNvMPeTE5HmQfP9Y8dgDf5bfSAKls67CReTT3igfs4K9pML9Jx8gF9UVusLCWGv3dkD7cyI84d35hADa9awfHf-E3AqdsEqAg','2019-06-28 15:30:59','2019-06-28 15:30:59','c7ce5d72-c64e-4fa9-bd8d-97611082338e'),
	(574,179,'tMs9v8H2LiYDWGm3vLodkAlChotfwZHs6u2WpPtQEKE1ML6kBSp6L4r7AWFwUqYA_ocOEQ-1og5OWoAQGrbHnfbR3UtKhq27hvN-','2019-06-28 16:31:04','2019-06-28 16:31:04','9895e13a-df66-4762-a6c9-8b2548bdb5b8'),
	(575,179,'K3UcGhOV3bKQf6g9a4dckqpNvyskonw4U4_MW8h378gY3nCwk1KmjlD3kN2MuqK6edI1xwkY2y9E3GK-wbL0T0e-2nBcNWriPkmS','2019-06-28 17:31:08','2019-06-28 17:31:08','efcc0365-1d49-4094-8ec3-5599c6f6310c'),
	(576,179,'-NIKx2eP4oKW-q-Vs7NZPaxs9nEqYADTzEAevR9116c4HScYD8snZd1UU4FZAC-sUmVHNXOd2xgfa8P2_dH2Xfs_WNxkPVpy3veb','2019-06-28 18:31:12','2019-06-28 18:31:12','5074fc48-59fd-4b9a-ad50-c293fa799193'),
	(577,179,'G7uEBizh4TRM_GKLRTqerAGOoR9hqDHyDZHRpfGKOHk9YdCBKE6pPOpaqMv0IbTrQoRMVOqqpvq8lDXau35aC6643tH6fOnfU6nd','2019-06-28 19:31:19','2019-06-28 19:31:19','f4ddf067-25fb-4045-91c9-1a29b7fa23b3'),
	(578,179,'dXSNKhG9j6wMT-TxG0qjVPROPDsqVgWyfVrcl3eL05FjByPAx9XrWpCySFTfmESbdahFr4EJioVjqrcDEIFtnU-RtGWnlFxhJIUV','2019-06-28 20:31:23','2019-06-28 20:31:23','0688a41c-7abf-4c31-ab0d-de5971eda741'),
	(579,179,'mdI-amoUDiJusvpZtpmoCPitwwEVhmO0ulXNOfGoXjoLSM3oM6e1Iy1s9tj8-RNwwWNM34lEGRvWV_Qz9wkm-wLV_MpIan-gFWf8','2019-06-28 21:31:32','2019-06-28 21:31:32','a1b199ef-5abd-4da3-acf6-989c203d9ac9'),
	(580,179,'2wXqRiFIlTSnE46NXEeDGbn1A-QLrIxBEJpgEvtiv9Xg13kXxceLeLJFHYre60iUAiCilWfFqVD4zkVax4iR2QGAtzTU1dTk-jJg','2019-06-28 22:31:36','2019-06-28 22:31:36','21c67637-d79d-4344-b512-bb61dd8e69bd'),
	(581,179,'rgz6lZK4GpuW2DTMlMnt1UQUo7OUGL-UNzNwXz-pghWT7SY-pJlGWrSEKNhM3D0gi8WzcIB7t7hcqh8zisgLLWj8TptlJWTC9NSb','2019-06-28 23:32:17','2019-06-28 23:32:17','e5e3ecde-44a3-4ed3-9a00-90563b47b6cd'),
	(582,179,'J1e2gf_K9C3TbCweJfdnPGmhjiBBqZ6klUJwFyT0-A3BUAVoCQ0OGxgSdvYhHryLdzeTPreZ-1L7Uy8kKdcuOcRJQzZdgQefITsZ','2019-06-29 01:09:52','2019-06-29 01:09:52','cd3e5f72-6ec8-4a71-a12c-507a768a6bce'),
	(583,179,'zWTsuAJ0V3BV2BLhHEiUtRBE7lxZyEuWMsw6r9wqjNa6sIDx76AL2Zs77ALOqdttsn5FLwS6r3MYzXmqOdVt5fvyaUuTxXD7-5e8','2019-06-29 02:26:48','2019-06-29 02:26:48','c2c5fe3e-5bdf-4f4b-a9ae-b568faa79e28'),
	(584,1,'STxoWWBsw-bh1eORCtRyWJsELSu48rBgyQuwIn4Ln4XY-T660G3Ei7dppmydcgKY4ROhrGrSkV7GJLpFsTQb0bCy098EwiE3Ok-S','2019-11-07 15:48:51','2019-11-07 16:44:05','f71518fd-f689-486b-b2c3-92d9d488839c');

/*!40000 ALTER TABLE `craft_sessions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_shunnedmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_shunnedmessages`;

CREATE TABLE `craft_shunnedmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `expiryDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_shunnedmessages_userId_message_unq_idx` (`userId`,`message`),
  CONSTRAINT `craft_shunnedmessages_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_sitegroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sitegroups`;

CREATE TABLE `craft_sitegroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_sitegroups_name_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_sitegroups` WRITE;
/*!40000 ALTER TABLE `craft_sitegroups` DISABLE KEYS */;

INSERT INTO `craft_sitegroups` (`id`, `name`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,'Craft CMS','2019-01-21 23:30:43','2019-11-07 16:42:56',NULL,'81be598e-e609-4e22-a752-1250e799527a');

/*!40000 ALTER TABLE `craft_sitegroups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_sites`;

CREATE TABLE `craft_sites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `primary` tinyint(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `language` varchar(12) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '0',
  `baseUrl` varchar(255) DEFAULT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_sites_dateDeleted_idx` (`dateDeleted`),
  KEY `craft_sites_handle_idx` (`handle`),
  KEY `craft_sites_sortOrder_idx` (`sortOrder`),
  KEY `craft_sites_groupId_fk` (`groupId`),
  CONSTRAINT `craft_sites_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_sitegroups` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_sites` WRITE;
/*!40000 ALTER TABLE `craft_sites` DISABLE KEYS */;

INSERT INTO `craft_sites` (`id`, `groupId`, `primary`, `name`, `handle`, `language`, `hasUrls`, `baseUrl`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,1,1,'Site Name','default','en-US',1,'$DEFAULT_SITE_URL',1,'2019-01-21 23:30:44','2019-11-07 16:43:40',NULL,'4e0fc612-e53b-4ed7-8b23-d723a0c987e8');

/*!40000 ALTER TABLE `craft_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_stc_columns
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_stc_columns`;

CREATE TABLE `craft_stc_columns` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_columnWidth` varchar(255) DEFAULT NULL,
  `field_columnContent` text,
  `field_form` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_stc_columns_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `craft_stc_columns_siteId_idx` (`siteId`),
  CONSTRAINT `craft_stc_columns_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_stc_columns_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_stc_columns` WRITE;
/*!40000 ALTER TABLE `craft_stc_columns` DISABLE KEYS */;

INSERT INTO `craft_stc_columns` (`id`, `elementId`, `siteId`, `dateCreated`, `dateUpdated`, `uid`, `field_columnWidth`, `field_columnContent`, `field_form`)
VALUES
	(1,69,1,'2019-01-31 16:04:34','2019-02-27 21:20:40','6d99bf53-df04-4b36-9022-0ca382ccce9f','6','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',NULL),
	(2,70,1,'2019-01-31 16:04:34','2019-02-27 21:20:40','acccac1a-4c6d-4027-b788-5964749a3843','6','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',NULL),
	(3,75,1,'2019-02-05 17:49:55','2019-02-27 21:20:40','4679d93e-8095-4b29-b66c-42a9cb6a8d98','12','<ul><li>Item</li><li>Lorem ipsum dolor</li><li>Lorem ipsum dolor sit amet</li><li>Lorem ipsum dolor</li><li>Lorem ipsum dolor sit amet</li><li>Incididunt ut labore et dolore magna aliqua</li><li>Lorem ipsum dolor</li><li>Lorem ipsum dolor sit amet</li><li>Lorem ipsum dolor</li><li>Lorem ipsum dolor sit amet</li><li>Incididunt ut labore et dolore magna aliqua</li><li>Item</li></ul>',NULL),
	(4,77,1,'2019-02-05 18:52:38','2019-02-05 20:28:28','b121c214-f20e-4355-87e8-62ffa786cf61','6','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',NULL),
	(5,78,1,'2019-02-05 18:52:38','2019-02-05 20:28:28','c8222535-1304-451c-bcfd-00ba7fc22a80','6','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',NULL),
	(6,81,1,'2019-02-05 20:29:19','2019-02-05 20:29:19','badc378f-d535-4038-87e4-4da1d63a08cd','12','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>',NULL),
	(7,84,1,'2019-02-05 20:30:08','2019-02-05 20:30:08','991dcdc9-145c-4604-95f9-ff09b2ccc861','12','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</p>',NULL),
	(8,99,1,'2019-02-07 21:02:36','2019-02-08 17:36:56','cd046c63-0dca-4f91-8f74-64b911043f1e','6','<p>We know that true transformation in our world begins with education. That is why we created Project Heart, our philanthropy education curriculum that teaches young people that they have the power and ability to create real, lasting change in their communities and the world.<br /></p>\n<p>Project Heart, our flagship educational curriculum, teaches 4th -12th grade students and students the foundational principles of philanthropy while challenging young people to give their time, talents, and treasure to someone else in need.</p>\n<p>Designed by teachers, for teachers, the curriculum follows the Understanding by Design (UbD) framework and aligns with Common Core for easy adoption into the classroom all while focusing on the 3 T’s of philanthropy - time, talent and treasure. Students learn that no matter their age, background or ability, when you use your 3 T’s, anyone can be a philanthropists and make a difference!</p>\n<p>Project Heart is one way that we are investing in the future, creating lasting and impactful change in our world through educating and inspiring young people to identify needs and work to solve the root of the issues in their communities. We believe that one inspired mind can change the world and, because of this, we are striving to foster an entire generation of young philanthropists!</p>',NULL),
	(9,100,1,'2019-02-07 21:02:36','2019-02-08 17:36:56','80148479-8aa6-43bc-935e-2d16d8557a4f','6','<figure><img src=\"{asset:15:url}\" alt=\"\" /></figure>',NULL),
	(10,103,1,'2019-02-07 21:16:26','2019-02-08 17:16:37','913377eb-c46b-4b7b-9981-0800c45212e3','6','<p>Project Heart for 4th &amp; 5th grade challenges your students to find the philanthropist inside and address the needs they see in the world around them as a class. In these 9 lessons, students are identifying personal passions, writing mission statements, learning about philanthropists, and completing a class service project. Each lesson averages 35 minutes, aligns with the common core state standards, and utilizes multiple best-practice teaching strategies. Students learn that by using their time, talent, and/or treasure they can be change agents who make a huge philanthropic impact.</p>',NULL),
	(11,107,1,'2019-02-07 21:18:38','2019-02-08 17:15:21','39cc5799-4150-4429-8024-890ae7e6a559','6','<p>Project Heart for Middle School engages your students to think about how they can personally make a difference with their time, talents, and/ or treasure.</p>\n<p>In these 10 lessons, students create measurable goals for executing their plans for philanthropic impact. Each lesson averages 20-25 minutes, aligns with the common core state standards, and utilizes multiple best-practice teaching strategies. Discovering what philanthropy means to them, students learn how their personal passions will drive their philanthropic mission. Students work in small groups or individually for their student-led projects while implementing ways to affect change on a local, national, and global level.</p>',NULL),
	(12,110,1,'2019-02-07 21:20:38','2019-02-08 17:44:24','0a6b016d-b50b-4ccc-ab26-5d04c7f85135','6','<p>Project Heart for High School provides a platform for students to explore how their passions and career goals can align with their philanthropic mission.</p>\n<p>In these 10 lessons, students create measurable goals for executing their plans for philanthropic impact. Each lesson averages 20-25 minutes, aligns with the common core state standards, and utilizes multiple best-practice teaching strategies. Discovering what philanthropy means to them, students learn how their personal passions will drive their philanthropic mission. Students work in small groups or individually for their student-led projects while implementing ways to affect change on a local, national, and global level.</p>',NULL),
	(13,111,1,'2019-02-07 21:20:38','2019-02-08 17:44:24','a335d728-9869-4df8-b386-d2db7d34d877','6','<figure><img src=\"{asset:97:url}\" alt=\"\" /></figure>\n<div class=\"_1BN1N Kzi1t MoE_1 _2DJZN\"><div class=\"_1HjH7\"><div title=\"Protected by Grammarly\" class=\"_3qe6h\"> </div></div></div>',NULL),
	(14,114,1,'2019-02-07 21:30:45','2019-02-08 18:05:31','81c9e54b-701a-4d10-9d7b-6d6b5b04965d','12','<h2 style=\"text-align:center;\"><span style=\"color:rgb(0,153,168);\">Choose Your</span> <span style=\"color:rgb(255,255,255);\">Grade</span></h2>',NULL),
	(15,121,1,'2019-02-07 21:44:10','2019-02-08 18:05:31','55861069-a2a8-4d76-a48d-75f63950fe54','6','<figure><img src=\"{asset:14:url}\" alt=\"\" /></figure>',NULL),
	(16,122,1,'2019-02-07 21:44:10','2019-02-08 18:05:32','4c0f761d-2e07-459c-9ff1-5a6fa210d15f','6','<h3><br /></h3>\n<h3><span style=\"color:rgb(255,255,255);\">Project Heart at a Glance</span></h3>\n<p><span style=\"color:rgb(255,255,255);\">Learn more about what we do at Project Heart.</span></p>\n<p><span style=\"color:rgb(255,255,255);\"><br /></span></p>\n<p><a href=\"b\" class=\"btn\">DOWNLOAD</a></p>',NULL),
	(17,124,1,'2019-02-07 21:44:10','2019-02-08 18:05:32','85b1f09b-1e4d-4535-98ff-a7b5ce87e271','6','<figure><img src=\"{asset:16:url}\" alt=\"\" /></figure>',NULL),
	(18,125,1,'2019-02-07 21:44:10','2019-02-08 18:05:32','acde904b-1dbc-45eb-8371-f014cad36e94','6','<p>We make speak different languages or have different cultures, but one thing that unites us is our desire to create a better world. From California to Maine to Vietnam to Australia, Project Heart is igniting the flame of philanthropy in students around the world, inspiring them to be change-makers, today.</p>',NULL),
	(19,139,1,'2019-02-07 22:19:35','2019-02-08 17:15:21','d85f9918-eb60-4d74-b5f7-d7d07a51eac0','6','<figure><img src=\"{asset:134:url}\" alt=\"\" /></figure>',NULL),
	(20,141,1,'2019-02-07 22:22:58','2019-02-08 17:16:37','177394e4-2ca7-4f3f-bf33-cee3cefd6656','6','<figure><img src=\"{asset:96:url}\" alt=\"\" /></figure>',NULL),
	(21,153,1,'2019-02-08 15:47:39','2019-02-08 18:05:31','1ab22ede-6a1d-4de0-9b83-2d9eaa652462','12','<h2 style=\"text-align:center;\"><span style=\"color:rgb(255,255,255);\">Project Heart</span> <span style=\"color:rgb(0,153,168);\">In Action</span></h2>',NULL),
	(22,155,1,'2019-02-08 15:52:54','2019-02-08 18:05:31','c11a5ce0-f356-4513-b5f7-ed2ab83a16eb','12','<h2 style=\"text-align:center;\"><span style=\"color:rgb(0,153,168);\">Download the</span> <span style=\"color:rgb(255,255,255);\">Project Heart Brochure</span></h2>',NULL),
	(23,157,1,'2019-02-08 15:52:54','2019-02-08 18:05:32','deadc62d-b035-4ce1-abcb-7e8f402ffa12','12','<h2 style=\"text-align:center;\"><span style=\"color:rgb(255,255,255);\">WORLD-CHANGERS</span> <span style=\"color:rgb(0,153,168);\">IN TRAINING</span></h2>',NULL),
	(24,159,1,'2019-02-08 15:52:55','2019-02-08 18:05:32','906ae425-beaa-4516-abb0-95c41c049cc7','12','<h2 style=\"text-align:center;\"><span style=\"color:rgb(0,153,168);\">Our</span> <span style=\"color:rgb(255,255,255);\">Stories</span></h2>',NULL),
	(25,161,1,'2019-02-08 15:54:32','2019-02-08 18:05:32','278eb491-d839-4ea9-9c88-6ee6dc5fd0c5','12','<h2 style=\"text-align:center;\"><span style=\"color:rgb(0,153,168);\">Connect With</span> <span style=\"color:rgb(255,255,255);\">OtterCares</span></h2>',NULL),
	(26,165,1,'2019-02-08 16:45:20','2019-02-08 18:05:31','52ee5e99-8c34-4354-bf5c-fdd097589b6d','6','<figure><img src=\"{asset:12:url}\" alt=\"\" /></figure>',NULL),
	(27,166,1,'2019-02-08 16:45:20','2019-02-08 18:05:31','4ec4f5aa-23ed-4dd4-82c3-bcaffd66881c','6','<h3><span style=\"color:rgb(205,45,45);\">We’re on a mission to change the world, one classroom at a time.</span></h3>\n<p>Changing the world is a big goal and we can’t do it alone. We want to ignite an entire generation of thinkers, makers, doers, and givers who have passion and resource to tackle the world’s biggest problems. That is why we created Project Heart, our philanthropy education curriculum. Project Heart taps into every child’s innate desire to give, while providing them with the tools and framework they need to be tomorrow’s world-changers, today. </p>',NULL);

/*!40000 ALTER TABLE `craft_stc_columns` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_stc_columns_1
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_stc_columns_1`;

CREATE TABLE `craft_stc_columns_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_columnWidth` varchar(255) DEFAULT NULL,
  `field_columnContent` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_stc_columns_1_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `craft_stc_columns_1_siteId_fk` (`siteId`),
  CONSTRAINT `craft_stc_columns_1_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_stc_columns_1_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_stc_forwardpathgrid
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_stc_forwardpathgrid`;

CREATE TABLE `craft_stc_forwardpathgrid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_fpTitle` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_stc_forwardpathgrid_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `craft_stc_forwardpathgrid_siteId_fk` (`siteId`),
  CONSTRAINT `craft_stc_forwardpathgrid_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_stc_forwardpathgrid_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_stc_forwardpathgrid` WRITE;
/*!40000 ALTER TABLE `craft_stc_forwardpathgrid` DISABLE KEYS */;

INSERT INTO `craft_stc_forwardpathgrid` (`id`, `elementId`, `siteId`, `dateCreated`, `dateUpdated`, `uid`, `field_fpTitle`)
VALUES
	(1,48,1,'2019-01-30 23:18:12','2019-02-27 21:20:40','96d61289-2d57-4a9a-a613-1dac0c61f85c','Headline here with will wrap maybe'),
	(2,49,1,'2019-01-30 23:18:12','2019-02-27 21:20:40','b036e5ba-90a4-4b93-93bc-211f5eb3d1ff','Headline here with will wrap maybe'),
	(3,50,1,'2019-01-30 23:18:13','2019-02-27 21:20:40','4d24e02f-858e-40c1-a6d6-21c935fd0f98','Headline here with will wrap maybe'),
	(4,116,1,'2019-02-07 21:37:48','2019-02-08 18:05:31','2e1f4b6c-26fa-4556-aee6-dc57c95658d6','Elementary School Curriculum'),
	(5,117,1,'2019-02-07 21:37:48','2019-02-08 18:05:31','65fd084f-9eb8-4a77-9515-347f0a408053','Middle School Curriculum'),
	(6,118,1,'2019-02-07 21:37:48','2019-02-08 18:05:31','d2555cce-0f2c-4453-94e6-af29b8f1a665','High School Curriculum');

/*!40000 ALTER TABLE `craft_stc_forwardpathgrid` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_stc_socialnetworks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_stc_socialnetworks`;

CREATE TABLE `craft_stc_socialnetworks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `elementId` int(11) NOT NULL,
  `siteId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  `field_socialurl` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_stc_socialnetworks_elementId_siteId_unq_idx` (`elementId`,`siteId`),
  KEY `craft_stc_socialnetworks_siteId_fk` (`siteId`),
  CONSTRAINT `craft_stc_socialnetworks_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_stc_socialnetworks_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_structureelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_structureelements`;

CREATE TABLE `craft_structureelements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `structureId` int(11) NOT NULL,
  `elementId` int(11) DEFAULT NULL,
  `root` int(11) unsigned DEFAULT NULL,
  `lft` int(11) unsigned NOT NULL,
  `rgt` int(11) unsigned NOT NULL,
  `level` smallint(6) unsigned NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_structureelements_structureId_elementId_unq_idx` (`structureId`,`elementId`),
  KEY `craft_structureelements_root_idx` (`root`),
  KEY `craft_structureelements_lft_idx` (`lft`),
  KEY `craft_structureelements_rgt_idx` (`rgt`),
  KEY `craft_structureelements_level_idx` (`level`),
  KEY `craft_structureelements_elementId_idx` (`elementId`),
  CONSTRAINT `craft_structureelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_structureelements_structureId_fk` FOREIGN KEY (`structureId`) REFERENCES `craft_structures` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_structureelements` WRITE;
/*!40000 ALTER TABLE `craft_structureelements` DISABLE KEYS */;

INSERT INTO `craft_structureelements` (`id`, `structureId`, `elementId`, `root`, `lft`, `rgt`, `level`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,NULL,1,1,110,0,'2019-01-30 21:22:51','2019-11-07 16:38:03','46d4fcff-ab20-4f6b-812c-3206c96db729'),
	(2,1,6,1,2,3,1,'2019-01-30 21:22:51','2019-01-30 21:22:51','88ce0e55-ca15-4175-ba07-5296b204ebe6'),
	(4,2,NULL,4,1,8,0,'2019-01-30 21:25:57','2019-01-30 23:36:48','cadee5a9-61ac-444d-b7d3-eb225d23eab0'),
	(11,3,NULL,11,1,4,0,'2019-01-30 21:51:59','2019-11-07 16:39:23','de0d005d-7139-490a-afb1-8562fe9dfafd'),
	(12,3,30,11,2,3,1,'2019-01-30 21:51:59','2019-01-30 21:51:59','6aa54906-265e-45cc-8a0e-17596edef1a0'),
	(18,4,NULL,18,1,4,0,'2019-01-30 21:54:06','2019-11-07 16:39:30','8d0e7e54-9e6d-4f63-8b99-9ea258e66d0f'),
	(19,4,36,18,2,3,1,'2019-01-30 21:54:06','2019-01-30 21:54:06','f685540e-d3e6-436f-8469-3f3288fb11b4'),
	(23,5,NULL,23,1,2,0,'2019-01-30 23:11:18','2019-11-07 16:38:03','5eb748a8-901b-4ab7-a3a4-0e9521f60510'),
	(27,6,NULL,27,1,2,0,'2019-01-30 23:12:53','2019-11-07 16:38:03','e91e042e-7950-4cdd-ab39-ea1245110116'),
	(31,7,NULL,31,1,2,0,'2019-01-30 23:18:13','2019-11-07 16:38:03','b6eb0494-a734-42e0-b9c8-bb77dada6f8e'),
	(36,8,NULL,36,1,10,0,'2019-01-30 23:35:14','2019-02-14 22:31:51','367d7a96-8a32-461f-827b-a4ee0ede695d'),
	(37,8,51,36,2,3,1,'2019-01-30 23:35:14','2019-01-30 23:35:14','31a6e452-a562-4d48-864f-0d344eb47920'),
	(38,8,52,36,4,5,1,'2019-01-30 23:35:24','2019-01-30 23:35:24','35268f98-d3a7-4033-8a1d-075043cd7d11'),
	(39,2,53,4,2,3,1,'2019-01-30 23:35:41','2019-01-30 23:35:41','d8be7d75-453a-4940-948c-0e9034be2a9d'),
	(40,2,54,4,4,5,1,'2019-01-30 23:36:36','2019-01-30 23:36:36','e76e3aad-d1cf-450d-946a-9c88fc96bfc6'),
	(41,2,55,4,6,7,1,'2019-01-30 23:36:48','2019-01-30 23:36:48','64ed6672-4810-4ac7-8e17-b48131730135'),
	(42,9,NULL,42,1,2,0,'2019-01-30 23:50:26','2019-11-07 16:38:03','266d02c4-eef7-43e2-bf13-b37de3c9c77c'),
	(51,10,NULL,51,1,2,0,'2019-01-31 15:16:54','2019-11-07 16:38:03','1dce1009-fbc4-4b49-9bb1-8c6c9b34f7ae'),
	(60,11,NULL,60,1,2,0,'2019-01-31 15:27:19','2019-11-07 16:38:03','c3bfec67-5900-4449-9eba-178acd951fa3'),
	(69,12,NULL,69,1,2,0,'2019-01-31 16:03:16','2019-11-07 16:38:03','d83deb43-8754-4291-aa84-e0c4c21e8bd0'),
	(78,13,NULL,78,1,2,0,'2019-01-31 16:04:36','2019-11-07 16:38:03','eb308be1-80c1-472a-b972-f18d7d1a8b63'),
	(88,14,NULL,88,1,2,0,'2019-01-31 16:06:10','2019-11-07 16:38:03','0ae30474-55df-420e-9b8e-9ff57780bb9f'),
	(99,15,NULL,99,1,2,0,'2019-01-31 20:27:08','2019-11-07 16:38:03','48da5c70-759f-4fd1-a013-8a86ecbe2a23'),
	(110,16,NULL,110,1,2,0,'2019-01-31 20:57:39','2019-11-07 16:37:11','af9dc3fd-8163-4628-be70-14b79bfd6fb3'),
	(112,17,NULL,112,1,2,0,'2019-01-31 22:50:31','2019-11-07 16:38:03','e98846e9-1c03-4286-ae51-a3b6ee2ca941'),
	(123,18,NULL,123,1,2,0,'2019-01-31 23:26:57','2019-11-07 16:38:03','745d1ad0-4866-41b3-9341-ed096cbba558'),
	(134,19,NULL,134,1,2,0,'2019-01-31 23:40:41','2019-11-07 16:38:03','85acc278-4fd6-4197-864e-35d2df4360a8'),
	(145,20,NULL,145,1,2,0,'2019-02-05 17:49:55','2019-11-07 16:38:03','94b67624-21b1-40e8-8db8-b8dce0c6ca4a'),
	(157,21,NULL,157,1,4,0,'2019-02-05 18:52:38','2019-02-05 18:52:38','cc3c596e-498e-4f8f-ab41-02aef5a126e2'),
	(158,21,76,157,2,3,1,'2019-02-05 18:52:38','2019-02-05 18:52:38','8330113c-15ac-4a99-8bc3-d77cc09c17c7'),
	(159,22,NULL,159,1,4,0,'2019-02-05 20:28:29','2019-02-05 20:28:29','190812ca-fe5a-478a-bad4-b36ded2f9ea1'),
	(160,22,76,159,2,3,1,'2019-02-05 20:28:29','2019-02-05 20:28:29','7049cbb5-054f-419c-8dd5-619b5fc7d5be'),
	(161,23,NULL,161,1,4,0,'2019-02-05 20:29:19','2019-02-05 20:29:19','8750528a-9e04-44ac-8b42-d5ce40d1c98f'),
	(162,23,80,161,2,3,1,'2019-02-05 20:29:19','2019-02-05 20:29:19','913e5355-1c75-4262-8ee1-4b0a68975275'),
	(163,24,NULL,163,1,4,0,'2019-02-05 20:30:08','2019-02-05 20:30:08','f9638f9a-19d7-4351-834b-aeaebcf38e3f'),
	(164,24,83,163,2,3,1,'2019-02-05 20:30:08','2019-02-05 20:30:08','27fc8512-e4f6-4b38-941c-f2e775a84a67'),
	(165,25,NULL,165,1,2,0,'2019-02-07 20:26:26','2019-11-07 16:37:11','09146d80-a5d5-4038-bad0-2316241dccd4'),
	(168,26,NULL,168,1,2,0,'2019-02-07 20:27:16','2019-11-07 16:37:11','835d33f9-0068-4e1c-ad94-8d3c8763e38a'),
	(171,27,NULL,171,1,2,0,'2019-02-07 20:33:23','2019-11-07 16:37:11','fc4c15ac-f33d-4d1c-8383-def45dbac668'),
	(174,28,NULL,174,1,2,0,'2019-02-07 20:37:50','2019-11-07 16:37:11','ae68b4b1-d034-4e00-896c-22a19b333c95'),
	(176,29,NULL,176,1,6,0,'2019-02-07 21:02:36','2019-02-07 21:02:36','96df28a7-687d-4623-b906-ba7e1e36a9ec'),
	(177,29,98,176,2,3,1,'2019-02-07 21:02:36','2019-02-07 21:02:36','97290312-222a-403d-b88a-cc6ed4c73e5b'),
	(178,29,101,176,4,5,1,'2019-02-07 21:02:36','2019-02-07 21:02:36','0646b03f-ab4d-40cc-b55f-85feba4fd8a3'),
	(179,30,NULL,179,1,6,0,'2019-02-07 21:03:22','2019-02-07 21:03:22','9fd9d7cb-b4cf-43f8-863e-e71c9c75d071'),
	(180,30,98,179,2,3,1,'2019-02-07 21:03:22','2019-02-07 21:03:22','d340b688-38bc-4151-8873-b71a4e388bd6'),
	(181,30,101,179,4,5,1,'2019-02-07 21:03:22','2019-02-07 21:03:22','3798ea25-c34c-43a7-b10c-da60127ab120'),
	(182,31,NULL,182,1,6,0,'2019-02-07 21:06:44','2019-02-07 21:06:44','6086614e-a154-4ab8-833d-02204954a85d'),
	(183,31,98,182,2,3,1,'2019-02-07 21:06:44','2019-02-07 21:06:44','b43ecd92-6d9f-4d01-86c7-d4a11113ecd9'),
	(184,31,101,182,4,5,1,'2019-02-07 21:06:44','2019-02-07 21:06:44','beee25a3-a4e2-4fcb-b30d-8d98a8db5cb7'),
	(185,32,NULL,185,1,2,0,'2019-02-07 21:16:26','2019-02-12 20:39:11','045ea3b7-6bd3-446b-aa62-0175263beaaa'),
	(188,33,NULL,188,1,2,0,'2019-02-07 21:17:08','2019-02-12 20:50:49','afe2eeee-4271-4be5-bb8c-6d33c3c8d963'),
	(191,34,NULL,191,1,2,0,'2019-02-07 21:18:38','2019-02-12 20:52:21','ae5dba1e-97cf-4f6d-b65b-1af6a7df3c79'),
	(194,35,NULL,194,1,2,0,'2019-02-07 21:20:38','2019-02-12 20:49:00','c70d4b4d-2ae2-4c37-b524-67f7217e8140'),
	(196,36,NULL,196,1,2,0,'2019-02-07 21:23:41','2019-02-12 20:53:06','086ec8af-031c-428d-b85f-d5864695f3fa'),
	(199,37,NULL,199,1,2,0,'2019-02-07 21:30:45','2019-11-07 16:37:11','087aecdb-72ce-4f54-8b13-56f0e85618aa'),
	(202,38,NULL,202,1,2,0,'2019-02-07 21:35:36','2019-11-07 16:37:11','a0281fc7-75f7-4433-8ce6-cc0a608f856b'),
	(205,39,NULL,205,1,2,0,'2019-02-07 21:37:48','2019-11-07 16:37:11','955dc3c5-fc00-4b82-b855-6937e0616c07'),
	(209,40,NULL,209,1,2,0,'2019-02-07 21:44:10','2019-11-07 16:37:11','cb42d881-a955-45b4-a132-5956f6c1a161'),
	(216,41,NULL,216,1,2,0,'2019-02-07 22:07:10','2019-11-07 16:37:11','74d5850a-d09d-417f-9386-1cbbd3e1b788'),
	(226,42,NULL,226,1,4,0,'2019-02-07 22:19:35','2019-02-12 20:52:21','d73f2531-77d9-43a3-b5ac-00582548a29c'),
	(228,42,140,226,2,3,1,'2019-02-07 22:19:35','2019-02-12 20:43:47','5f03a926-5c18-4ccb-bc4e-7c8e4e6eca7c'),
	(230,43,NULL,230,1,4,0,'2019-02-07 22:22:58','2019-02-12 20:50:49','dc508252-fb30-4930-9136-82160fbf9058'),
	(232,43,142,230,2,3,1,'2019-02-07 22:22:58','2019-02-12 20:39:11','de14da15-f752-48d9-95b1-8f21570b14af'),
	(234,44,NULL,234,1,4,0,'2019-02-07 22:24:54','2019-02-12 20:53:06','41ffb9c6-a272-453d-a95a-8fc53e17c6a8'),
	(236,44,143,234,2,3,1,'2019-02-07 22:24:54','2019-02-12 20:49:00','d3452bef-ab64-4515-a7fa-6aad49aa0829'),
	(238,45,NULL,238,1,4,0,'2019-02-07 22:28:23','2019-02-12 20:52:21','ac044659-2fbe-42cf-a09b-d1d0ee9e8a3d'),
	(240,45,140,238,2,3,1,'2019-02-07 22:28:23','2019-02-12 20:43:47','7f9053b9-6ef9-40be-bc5d-e732bcb8ac35'),
	(242,46,NULL,242,1,2,0,'2019-02-07 22:31:37','2019-11-07 16:37:11','c059bb12-e7b2-4427-9ff9-d13bcd373ce7'),
	(252,47,NULL,252,1,4,0,'2019-02-07 22:33:25','2019-02-12 20:53:06','545f5317-93ab-4e65-9b94-b8cab08adbd2'),
	(254,47,143,252,2,3,1,'2019-02-07 22:33:25','2019-02-12 20:49:00','2c9e63ac-d928-4791-895b-3636c8fc7cfe'),
	(256,48,NULL,256,1,4,0,'2019-02-07 22:36:32','2019-02-12 20:53:06','eeecaf2b-7972-4945-a007-880322d7026f'),
	(258,48,143,256,2,3,1,'2019-02-07 22:36:32','2019-02-12 20:49:00','dfa07d87-a1b4-4f52-89d7-3b46660f616f'),
	(260,8,145,36,6,7,1,'2019-02-07 22:41:16','2019-02-07 22:41:16','350a5cb1-c6bd-42bd-b5f7-2abae692740f'),
	(261,8,148,36,8,9,1,'2019-02-07 22:44:08','2019-02-07 22:44:08','1fd9e2e4-a3af-4ee1-80ad-171c138040a5'),
	(262,49,NULL,262,1,4,0,'2019-02-07 22:51:47','2019-02-12 20:52:21','54feddd7-4f6c-4cef-9b11-ef74967613eb'),
	(264,49,140,262,2,3,1,'2019-02-07 22:51:47','2019-02-12 20:43:47','ef764a57-99e1-4424-8d50-03d816428af7'),
	(266,50,NULL,266,1,2,0,'2019-02-07 22:52:52','2019-11-07 16:37:11','7ea3a6c3-38bf-41c1-87c6-571b95e95040'),
	(276,51,NULL,276,1,6,0,'2019-02-07 23:52:59','2019-02-07 23:52:59','ce3de028-69df-4706-92fb-87f72e915c15'),
	(277,51,98,276,2,3,1,'2019-02-07 23:52:59','2019-02-07 23:52:59','2a85307c-db6f-4e02-9d90-70981424a678'),
	(278,51,101,276,4,5,1,'2019-02-07 23:52:59','2019-02-07 23:52:59','1a14d348-9c35-4601-b0b9-a9ab830ebc61'),
	(279,52,NULL,279,1,2,0,'2019-02-07 23:55:39','2019-11-07 16:38:03','16f5807c-77c2-4b57-8ab5-c6a15b8aebb9'),
	(291,53,NULL,291,1,2,0,'2019-02-08 15:45:05','2019-11-07 16:37:11','b2ec42ba-2a54-493c-bfec-2c4344f6de9e'),
	(301,54,NULL,301,1,2,0,'2019-02-08 15:47:40','2019-11-07 16:37:11','e647fdb8-9aff-45e5-987d-fbc5b1a0d08f'),
	(312,55,NULL,312,1,2,0,'2019-02-08 15:52:55','2019-11-07 16:37:11','ed57b5d3-238a-4dfd-a8c0-0d2d6ea7921b'),
	(326,56,NULL,326,1,2,0,'2019-02-08 15:54:32','2019-11-07 16:37:11','4f23536f-0fc1-4080-b078-55d3c9fb77cf'),
	(341,57,NULL,341,1,2,0,'2019-02-08 15:57:41','2019-11-07 16:37:11','d236c209-2858-466b-a637-dca942f51187'),
	(356,58,NULL,356,1,2,0,'2019-02-08 15:59:57','2019-11-07 16:37:11','fb8a4185-3bce-414e-8783-a6f61ebeba8d'),
	(371,59,NULL,371,1,2,0,'2019-02-08 16:42:16','2019-11-07 16:37:11','f852f441-53a1-4e43-abb2-cd594ca3c8ca'),
	(387,60,NULL,387,1,2,0,'2019-02-08 16:45:21','2019-11-07 16:37:11','ba5f0761-11d0-4129-998f-f6adad1ace06'),
	(403,61,NULL,403,1,2,0,'2019-02-08 16:47:00','2019-11-07 16:37:11','91ab69c3-fe2c-4dac-9151-bf8dab4ca4f8'),
	(419,62,NULL,419,1,2,0,'2019-02-08 16:48:46','2019-11-07 16:37:11','dce91fdf-d96e-405b-89e6-6665ac74090a'),
	(435,63,NULL,435,1,2,0,'2019-02-08 16:53:48','2019-11-07 16:37:11','2f5a5023-5333-42a5-93ec-c179b66f2e4c'),
	(451,64,NULL,451,1,2,0,'2019-02-08 16:58:06','2019-11-07 16:37:11','7f0c6536-2fa3-4bbe-946b-b721a96f51eb'),
	(467,65,NULL,467,1,8,0,'2019-02-08 17:05:33','2019-02-08 17:05:33','5d4dc6d6-e6ad-474c-ae6c-0b2bfd00bcbd'),
	(468,65,98,467,2,3,1,'2019-02-08 17:05:33','2019-02-08 17:05:33','818c89ad-932a-4052-9e2c-9b2acb0ad405'),
	(469,65,101,467,4,5,1,'2019-02-08 17:05:33','2019-02-08 17:05:33','09a3f989-3f42-4032-b1af-b4b34988c24b'),
	(470,65,169,467,6,7,1,'2019-02-08 17:05:33','2019-02-08 17:05:33','dbb10387-5a68-430c-86bf-206fef0d0f54'),
	(471,66,NULL,471,1,6,0,'2019-02-08 17:07:08','2019-02-12 20:50:49','776cb98d-fad5-4bad-bfd3-c8c34c560ae7'),
	(473,66,170,471,2,3,1,'2019-02-08 17:07:08','2019-02-12 20:39:11','e0be353e-ccac-4b85-806c-2a5160f67a88'),
	(474,66,142,471,4,5,1,'2019-02-08 17:07:08','2019-02-12 20:39:11','008bb4a5-9289-4f4c-9e26-6cb6452d3b57'),
	(476,67,NULL,476,1,6,0,'2019-02-08 17:08:39','2019-02-12 20:50:49','b6507229-f645-4c13-8bb8-de1f269759bb'),
	(478,67,170,476,2,3,1,'2019-02-08 17:08:39','2019-02-12 20:39:11','dadfceef-006f-4b94-90f9-67eee51fb919'),
	(479,67,142,476,4,5,1,'2019-02-08 17:08:39','2019-02-12 20:39:11','9dcbdc32-250d-4697-9efd-011b228eaf40'),
	(481,68,NULL,481,1,6,0,'2019-02-08 17:11:44','2019-02-12 20:52:21','3a60a555-46b5-4c33-9c12-9356f5697602'),
	(483,68,171,481,2,3,1,'2019-02-08 17:11:44','2019-02-12 20:43:47','6f65ca49-3118-44a2-8f01-a357812523ca'),
	(484,68,140,481,4,5,1,'2019-02-08 17:11:44','2019-02-12 20:43:47','9ca4ac5a-a5ac-49b0-bcb7-10a2dd43d114'),
	(486,69,NULL,486,1,6,0,'2019-02-08 17:15:21','2019-02-12 20:52:21','f0c33869-2a7f-4802-bbf3-01ce9f699a29'),
	(488,69,171,486,2,3,1,'2019-02-08 17:15:21','2019-02-12 20:43:47','3deddbbb-822c-4cb7-aaf3-33cbbae0d238'),
	(489,69,140,486,4,5,1,'2019-02-08 17:15:21','2019-02-12 20:43:47','085e479d-0c5b-4c11-a297-4e8950349bb3'),
	(491,70,NULL,491,1,6,0,'2019-02-08 17:16:37','2019-02-12 20:50:49','cd62fe39-49ee-4071-97c5-89ab13f44b1d'),
	(493,70,170,491,2,3,1,'2019-02-08 17:16:37','2019-02-12 20:39:11','e3b836d8-21cf-413a-9263-a97760ab1de1'),
	(494,70,142,491,4,5,1,'2019-02-08 17:16:37','2019-02-12 20:39:11','e23ac302-5821-4159-aab3-421830222f34'),
	(496,71,NULL,496,1,4,0,'2019-02-08 17:17:43','2019-02-12 20:53:06','30e28c52-0604-4ab6-b77f-4210cfc4f965'),
	(498,71,143,496,2,3,1,'2019-02-08 17:17:43','2019-02-12 20:49:00','3f882378-324e-49ae-a95c-63056ddf7d62'),
	(500,72,NULL,500,1,8,0,'2019-02-08 17:36:28','2019-02-08 17:36:28','b4e58fe4-4a39-465c-80d5-c4d468e06dda'),
	(501,72,98,500,2,3,1,'2019-02-08 17:36:28','2019-02-08 17:36:28','8f7aa1ac-cd3b-4aaa-b3f1-4e268e32cf68'),
	(502,72,101,500,4,5,1,'2019-02-08 17:36:28','2019-02-08 17:36:28','9924f043-d786-4015-94eb-b0135be7c7d9'),
	(503,72,169,500,6,7,1,'2019-02-08 17:36:28','2019-02-08 17:36:28','d58dcbe0-f02c-46ae-bc31-717b74a2b1df'),
	(504,73,NULL,504,1,8,0,'2019-02-08 17:36:56','2019-02-08 17:36:56','ba15d5e3-29b2-4ce0-868d-11d7b5cd1772'),
	(505,73,98,504,2,3,1,'2019-02-08 17:36:56','2019-02-08 17:36:56','11e12749-7d68-42b2-b9e7-1a535abbb232'),
	(506,73,101,504,4,5,1,'2019-02-08 17:36:56','2019-02-08 17:36:56','615a51fc-b763-43ff-b1f9-55ab1d459f03'),
	(507,73,169,504,6,7,1,'2019-02-08 17:36:56','2019-02-08 17:36:56','9b769d81-2509-4fe0-8fd3-f934e2e0b3a2'),
	(508,74,NULL,508,1,6,0,'2019-02-08 17:41:19','2019-02-12 20:53:06','cdc52aef-2767-4ebe-b2fc-46ebe6d49d90'),
	(510,74,172,508,2,3,1,'2019-02-08 17:41:19','2019-02-12 20:49:00','ec68ea7a-0961-4972-abb5-89304a37cd64'),
	(511,74,143,508,4,5,1,'2019-02-08 17:41:19','2019-02-12 20:49:00','5f5992e9-54e9-476f-894a-b96c9c910ae1'),
	(513,75,NULL,513,1,6,0,'2019-02-08 17:44:24','2019-02-12 20:53:06','55b8eb0f-80d7-40b5-acf5-9dc7bbb41ad7'),
	(515,75,172,513,2,3,1,'2019-02-08 17:44:24','2019-02-12 20:49:00','ff79ffc5-3cf0-4425-ae13-93361ca166a4'),
	(516,75,143,513,4,5,1,'2019-02-08 17:44:24','2019-02-12 20:49:00','7cdcc184-51a9-44f6-ab24-108e6a4996aa'),
	(518,76,NULL,518,1,2,0,'2019-02-08 18:05:32','2019-11-07 16:37:11','9287d604-e165-4a93-bc63-4e40df70f648'),
	(534,77,NULL,534,1,2,0,'2019-02-08 20:04:40','2019-11-07 16:38:03','c7b3ae32-9631-4944-aca5-4cf07556f6c1'),
	(547,78,NULL,547,1,2,0,'2019-02-08 20:13:05','2019-11-07 16:38:03','f7cc46a2-8f50-4240-94eb-a085955d7df8'),
	(560,79,NULL,560,1,8,0,'2019-02-12 20:39:11','2019-02-12 20:50:49','5d54d64a-d293-43e5-82df-472e0e62e14c'),
	(561,79,174,560,2,3,1,'2019-02-12 20:39:11','2019-02-12 20:39:11','d5339649-9d1a-494b-a9ad-f9a2d8e38445'),
	(562,79,170,560,4,5,1,'2019-02-12 20:39:11','2019-02-12 20:39:11','181ee71b-34b8-4a3f-9908-e62965d568af'),
	(563,79,142,560,6,7,1,'2019-02-12 20:39:11','2019-02-12 20:39:11','93faf8f4-b65d-49e3-b4a5-b4d3f887d71b'),
	(565,80,NULL,565,1,8,0,'2019-02-12 20:43:47','2019-02-12 20:52:21','07327d95-15ca-443a-9551-fcd3d5eb6ae9'),
	(566,80,175,565,2,3,1,'2019-02-12 20:43:47','2019-02-12 20:43:47','de221fc9-8cc3-4fe1-807e-4ef04b392e66'),
	(567,80,171,565,4,5,1,'2019-02-12 20:43:47','2019-02-12 20:43:47','fbbad736-da24-4d39-b017-4385726bacd9'),
	(568,80,140,565,6,7,1,'2019-02-12 20:43:47','2019-02-12 20:43:47','53e243ba-0ed4-41eb-8bea-f31b74a49b44'),
	(570,81,NULL,570,1,8,0,'2019-02-12 20:49:00','2019-02-12 20:53:06','bb4d399b-71f5-4acf-ab75-85ace1ccbdf2'),
	(571,81,176,570,2,3,1,'2019-02-12 20:49:00','2019-02-12 20:49:00','d9bcc83b-80bc-4358-a610-05761b1166c8'),
	(572,81,172,570,4,5,1,'2019-02-12 20:49:00','2019-02-12 20:49:00','e56f82b6-a07d-4bfb-b380-75e643f402f3'),
	(573,81,143,570,6,7,1,'2019-02-12 20:49:00','2019-02-12 20:49:00','19c6caaf-414d-4f8d-8957-142bf30779c1'),
	(575,82,NULL,575,1,8,0,'2019-02-12 20:50:49','2019-02-12 20:50:49','7d8681f0-0fac-4d02-a002-ec036da9d334'),
	(576,82,174,575,2,3,1,'2019-02-12 20:50:49','2019-02-12 20:50:49','42535fb4-ec0e-4f8a-91ed-b54bab6cb5cc'),
	(577,82,170,575,4,5,1,'2019-02-12 20:50:49','2019-02-12 20:50:49','42b1f292-6346-4b3a-b61a-2ade97fe8071'),
	(578,82,142,575,6,7,1,'2019-02-12 20:50:49','2019-02-12 20:50:49','d4aee765-f6c4-409a-988f-62c2622a4134'),
	(579,83,NULL,579,1,8,0,'2019-02-12 20:52:21','2019-02-12 20:52:21','cfff3621-11ab-4d16-9223-8a0015210f58'),
	(580,83,175,579,2,3,1,'2019-02-12 20:52:21','2019-02-12 20:52:21','609cfe98-70ca-48ba-82c8-184045bbb9ca'),
	(581,83,171,579,4,5,1,'2019-02-12 20:52:21','2019-02-12 20:52:21','9d59b252-c980-4114-a392-b0beb9532f9f'),
	(582,83,140,579,6,7,1,'2019-02-12 20:52:21','2019-02-12 20:52:21','650dbd44-ebe1-46c6-ab6e-e9ced6dff92a'),
	(583,84,NULL,583,1,8,0,'2019-02-12 20:53:06','2019-02-12 20:53:06','57be370a-793b-4711-817b-833aecc613fe'),
	(584,84,176,583,2,3,1,'2019-02-12 20:53:06','2019-02-12 20:53:06','0e609f82-fa9d-436b-a625-ff50a9d90a53'),
	(585,84,172,583,4,5,1,'2019-02-12 20:53:06','2019-02-12 20:53:06','dedbdfdb-b809-466a-9a90-2eb90333afb0'),
	(586,84,143,583,6,7,1,'2019-02-12 20:53:06','2019-02-12 20:53:06','2e1783f1-f0cf-464c-a6d0-f722f4e9d56f'),
	(587,85,NULL,587,1,2,0,'2019-02-14 22:28:47','2019-11-07 16:38:03','0dab96fb-7d26-4dde-b410-82146c0d3a8d'),
	(602,86,NULL,602,1,2,0,'2019-02-27 21:20:40','2019-11-07 16:38:03','65d1f274-d133-4d60-8364-729283848030'),
	(616,1,181,1,4,5,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','a5b1a2b9-8f9f-451e-ba37-390b72ae47d7'),
	(617,1,182,1,6,7,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','3716b9ae-e8cc-4c3b-a9f1-fb85faedba22'),
	(618,1,183,1,8,9,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','3d114913-9bae-4b99-9ed0-bf338ebea294'),
	(619,1,184,1,10,11,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','182f5cbb-68a6-4963-b00c-704be982f336'),
	(620,1,185,1,12,13,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','5997cb93-cce3-45f2-8b06-dd74daf627d8'),
	(621,1,186,1,14,15,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','cd6da82e-a82c-4e0d-82c5-d4c32f2909aa'),
	(622,1,187,1,16,17,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','6817c4ab-1569-4982-908f-1a056555dfef'),
	(623,1,188,1,18,19,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','59ef9e73-1198-4f50-9742-680ad4c54175'),
	(624,1,189,1,20,21,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','859b97b3-769a-45d0-a0a4-8aabc2149eef'),
	(625,1,190,1,22,23,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','f8150116-b323-41a4-8ee1-efa63a84757c'),
	(626,1,192,1,24,25,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','acda260e-c83f-4d1f-8d1e-2f0b8a3bf886'),
	(627,1,193,1,26,27,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','2f42dfcd-6ee4-42e5-ae19-8a541cdd4857'),
	(628,1,194,1,28,29,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','afb5225e-2f85-48d5-ae7d-390691d5e037'),
	(629,1,195,1,30,31,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','07306add-b657-4988-87f6-7d8114c70a62'),
	(630,1,196,1,32,33,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','ce5f90b9-7234-40e2-808a-1c7765a4c0db'),
	(631,1,197,1,34,35,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','7e0b3548-d142-44e5-8af7-9b1939882603'),
	(632,1,198,1,36,37,1,'2019-11-07 15:48:53','2019-11-07 15:49:26','8a29f3e2-0de1-40a5-b62d-e0e8ceaacae9'),
	(633,1,199,1,38,39,1,'2019-11-07 15:48:54','2019-11-07 15:49:26','e74f82ee-75a0-4b9c-8cdc-54c7cff8a6a4'),
	(634,1,205,1,40,41,1,'2019-11-07 15:48:54','2019-11-07 15:49:26','a7729397-b256-444a-842d-2e37df4b5f69'),
	(635,1,206,1,42,43,1,'2019-11-07 15:48:54','2019-11-07 15:49:26','35498dee-968f-4f74-ae92-fa19885a9682'),
	(636,1,219,1,44,45,1,'2019-11-07 15:48:54','2019-11-07 15:49:26','dd3f229c-e2f3-4529-a952-8dfa88cfdcfb'),
	(637,1,221,1,46,47,1,'2019-11-07 15:48:54','2019-11-07 15:49:26','a234441f-74a4-41c4-807b-8df3cbd78806'),
	(638,1,238,1,48,49,1,'2019-11-07 15:48:55','2019-11-07 15:49:26','855f818c-2604-4167-b589-2248b6dfb067'),
	(639,1,239,1,50,51,1,'2019-11-07 15:48:55','2019-11-07 15:49:26','2fd37cc8-8436-4b0e-8df8-db654c644b90'),
	(640,1,240,1,52,53,1,'2019-11-07 15:48:55','2019-11-07 15:49:26','5f0d20e9-100e-4369-9609-5a0af903d8af'),
	(641,1,246,1,54,55,1,'2019-11-07 15:48:55','2019-11-07 15:49:26','a1092ca2-afb6-4072-9ede-e2cf0bd54ba1'),
	(642,1,247,1,56,57,1,'2019-11-07 15:48:55','2019-11-07 15:49:26','d9e33adc-714a-48a6-b4ab-753340b8f1ab'),
	(643,1,248,1,58,59,1,'2019-11-07 15:48:55','2019-11-07 15:49:26','b8623423-483d-4a77-92a7-d96678662e54'),
	(644,1,249,1,60,61,1,'2019-11-07 15:48:55','2019-11-07 15:49:26','8a22d67e-5226-4aa8-9adc-184bc70b3d9d'),
	(645,1,250,1,62,63,1,'2019-11-07 15:48:55','2019-11-07 15:49:26','c5ce9fad-ee32-4861-8258-86a95e3a239d'),
	(646,1,251,1,64,65,1,'2019-11-07 15:48:55','2019-11-07 15:49:26','4a1894b7-2628-4f11-9124-f0fda98e39d5'),
	(647,1,252,1,66,67,1,'2019-11-07 15:48:55','2019-11-07 15:49:26','fa50d45b-a4e2-43a6-972f-b2428332253f'),
	(648,1,253,1,68,69,1,'2019-11-07 15:48:55','2019-11-07 15:49:26','02a72e06-e981-4cee-b6ad-cb60a65a3de6'),
	(649,1,261,1,70,71,1,'2019-11-07 15:48:55','2019-11-07 15:49:26','07226e37-155a-4890-ac0e-9957e10ed913'),
	(650,1,263,1,72,73,1,'2019-11-07 15:48:55','2019-11-07 15:49:26','5cd6c013-75ad-40d9-9817-f3ae745fb683'),
	(651,1,264,1,74,75,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','9d4bf3e2-cf7b-4bc6-8cad-1bc1e88d9cd3'),
	(652,1,265,1,76,77,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','0a1f2fa4-786e-4e54-92a6-fce3bc4fdfaa'),
	(653,1,268,1,78,79,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','ed3e0232-0bbc-46ac-b184-04844d820bd6'),
	(654,1,273,1,80,81,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','0e6fa984-3c98-4b5c-a51a-49b0253a672c'),
	(655,1,274,1,82,83,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','11542b30-c56f-4915-8018-391a1a99790d'),
	(656,1,276,1,84,85,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','414730b2-50f5-4464-9ee7-fbed44084071'),
	(657,1,278,1,86,87,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','62f2b7b2-712c-4a9e-b460-c49ae4e66ba4'),
	(658,1,279,1,88,89,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','a26fa35c-2642-4126-9c0f-d720b88350d3'),
	(659,1,280,1,90,91,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','40066e78-45b6-4979-91d8-b9faad09c999'),
	(660,1,281,1,92,93,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','8a0e3e3d-6f98-4939-b02e-6eae3ad189cc'),
	(661,1,282,1,94,95,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','8596b755-d732-46d3-8d87-5e99fcc8eb65'),
	(662,1,283,1,96,97,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','af55bb47-a284-42b7-a661-21aa9a395079'),
	(663,1,284,1,98,99,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','b1ca8a5f-55d3-4421-bfa1-cb0e53ad2451'),
	(664,1,285,1,100,101,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','21165f44-e88f-456d-aa45-37b21c234928'),
	(665,1,286,1,102,103,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','2633235b-39a3-4b69-bbea-67aaff20d979'),
	(666,1,288,1,104,105,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','72495f40-8f59-4277-bb2b-53f4e156d512'),
	(667,1,289,1,106,107,1,'2019-11-07 15:48:56','2019-11-07 15:49:26','f555da45-87d2-4503-84c7-c87b88c1f596'),
	(668,1,292,1,108,109,1,'2019-11-07 16:38:03','2019-11-07 16:38:03','e087ee8a-1edc-4e25-93ca-e7f5e0abb6a8');

/*!40000 ALTER TABLE `craft_structureelements` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_structures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_structures`;

CREATE TABLE `craft_structures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maxLevels` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_structures_dateDeleted_idx` (`dateDeleted`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_structures` WRITE;
/*!40000 ALTER TABLE `craft_structures` DISABLE KEYS */;

INSERT INTO `craft_structures` (`id`, `maxLevels`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,NULL,'2019-01-21 23:39:46','2019-01-22 16:11:58',NULL,'28148cbe-8f04-486c-9acf-57a4379ee6f9'),
	(2,NULL,'2019-01-30 21:25:15','2019-01-30 21:25:15',NULL,'489bb1cd-412e-41f1-aa73-12797a2e91ee'),
	(3,NULL,'2019-01-30 21:51:30','2019-01-30 21:51:30',NULL,'fc7dcf6c-fc5f-465c-a7a1-10795c56177b'),
	(4,1,'2019-01-30 21:51:45','2019-01-30 21:54:38',NULL,'375c37d4-e557-4435-98d7-b167d24d81fe'),
	(5,NULL,'2019-01-30 23:11:18','2019-01-30 23:11:18','2019-01-30 23:12:53','2b71eeba-e08b-4b94-b320-c6e51c1faa39'),
	(6,NULL,'2019-01-30 23:12:53','2019-01-30 23:12:53','2019-01-30 23:18:13','8366d6e2-733f-41e9-8997-8f18025b1032'),
	(7,NULL,'2019-01-30 23:18:13','2019-01-30 23:18:13','2019-01-30 23:50:26','6bb97aad-27fd-4429-a4f3-96914a208d69'),
	(8,NULL,'2019-01-30 23:34:45','2019-01-30 23:34:45',NULL,'15e92219-8361-44e4-8d09-0a3b849060bb'),
	(9,NULL,'2019-01-30 23:50:26','2019-01-30 23:50:26','2019-01-31 15:16:54','61f38788-4f97-4a5e-9247-6600ecb1a7ea'),
	(10,NULL,'2019-01-31 15:16:54','2019-01-31 15:16:54','2019-01-31 15:27:18','a2e60188-5990-47a6-a89e-d232581b49ac'),
	(11,NULL,'2019-01-31 15:27:18','2019-01-31 15:27:18','2019-01-31 16:03:16','0133bb3c-8003-4617-88a7-ea5229735f95'),
	(12,NULL,'2019-01-31 16:03:16','2019-01-31 16:03:16','2019-01-31 16:04:36','09c4cc3b-4690-409f-abae-dc5844846f9a'),
	(13,NULL,'2019-01-31 16:04:36','2019-01-31 16:04:36','2019-01-31 16:06:10','176ba5ee-1b59-472d-9b72-e76122b5e07f'),
	(14,NULL,'2019-01-31 16:06:10','2019-01-31 16:06:10','2019-01-31 20:27:08','4369f341-cb9f-41d0-9469-6aa7bcf8aa37'),
	(15,NULL,'2019-01-31 20:27:08','2019-01-31 20:27:08','2019-01-31 22:50:31','41bfe745-e609-40d2-9042-3b34e3bdbcb7'),
	(16,NULL,'2019-01-31 20:57:39','2019-01-31 20:57:39','2019-02-07 20:26:26','28714459-bbe6-49bb-b293-d2664f48c913'),
	(17,NULL,'2019-01-31 22:50:31','2019-01-31 22:50:31','2019-01-31 23:26:57','a60974a7-046c-4401-bd6b-23cf58f47ee5'),
	(18,NULL,'2019-01-31 23:26:57','2019-01-31 23:26:57','2019-01-31 23:40:41','76dc59ad-76ff-4d2b-9c7a-2048e85a6f97'),
	(19,NULL,'2019-01-31 23:40:41','2019-01-31 23:40:41','2019-02-05 17:49:55','b927b632-25b8-426b-a7b8-3c64d141b3eb'),
	(20,NULL,'2019-02-05 17:49:55','2019-02-05 17:49:55','2019-02-07 23:55:39','3b9e5e29-b078-4444-bbe0-907047150017'),
	(21,NULL,'2019-02-05 18:52:38','2019-02-05 18:52:38','2019-02-05 20:28:29','7b865526-3646-444b-8a55-f635a933d5d7'),
	(22,NULL,'2019-02-05 20:28:29','2019-02-05 20:28:29',NULL,'19506cc6-0b16-4c5f-8712-080efee280b1'),
	(23,NULL,'2019-02-05 20:29:19','2019-02-05 20:29:19',NULL,'3e2f3cce-477c-4a12-abf1-fddb9b183c20'),
	(24,NULL,'2019-02-05 20:30:08','2019-02-05 20:30:08',NULL,'c4e356c7-7100-4afb-8ca6-3ce9eb30af4a'),
	(25,NULL,'2019-02-07 20:26:26','2019-02-07 20:26:26','2019-02-07 20:27:16','83fdb0e6-4f1c-43b8-b69b-c65098dd86c8'),
	(26,NULL,'2019-02-07 20:27:16','2019-02-07 20:27:16','2019-02-07 20:33:23','7cd5091b-a36f-44fa-ae70-d046e2e72314'),
	(27,NULL,'2019-02-07 20:33:23','2019-02-07 20:33:23','2019-02-07 20:37:50','9a879639-5138-4af0-ba2a-3651b5b23abd'),
	(28,NULL,'2019-02-07 20:37:50','2019-02-07 20:37:50','2019-02-07 21:30:45','1257a876-386f-4b14-abba-2089f51da15d'),
	(29,NULL,'2019-02-07 21:02:36','2019-02-07 21:02:36','2019-02-07 21:03:22','3e6b4e17-a92f-4e06-91d2-5e1b910e2bc0'),
	(30,NULL,'2019-02-07 21:03:22','2019-02-07 21:03:22','2019-02-07 21:06:44','fabc63e6-f538-40f7-a53d-e1b9151d1980'),
	(31,NULL,'2019-02-07 21:06:44','2019-02-07 21:06:44','2019-02-07 23:52:59','aba1b4d5-8f02-40af-a505-29b322d23bc6'),
	(32,NULL,'2019-02-07 21:16:26','2019-02-07 21:16:26','2019-02-07 21:17:08','80fb3f40-1541-429a-a032-4309388d988a'),
	(33,NULL,'2019-02-07 21:17:08','2019-02-07 21:17:08','2019-02-07 22:22:58','1051285b-7ad4-4769-b4a9-30753d6efd4c'),
	(34,NULL,'2019-02-07 21:18:38','2019-02-07 21:18:38','2019-02-07 22:19:35','2bec753b-c255-4e2f-a036-9728b7263070'),
	(35,NULL,'2019-02-07 21:20:38','2019-02-07 21:20:38','2019-02-07 21:23:41','64c65504-19c8-4d80-8204-bcde5ba7bccb'),
	(36,NULL,'2019-02-07 21:23:41','2019-02-07 21:23:41','2019-02-07 22:24:54','c36ca304-112b-4ed5-bb28-eae00e970e1a'),
	(37,NULL,'2019-02-07 21:30:45','2019-02-07 21:30:45','2019-02-07 21:35:36','b99fd592-577a-400e-b392-98a16fd317ff'),
	(38,NULL,'2019-02-07 21:35:36','2019-02-07 21:35:36','2019-02-07 21:37:48','5a56766d-975c-4782-b0ab-71ebf64731e9'),
	(39,NULL,'2019-02-07 21:37:48','2019-02-07 21:37:48','2019-02-07 21:44:10','378fca0f-3dc2-41a0-816e-72d035254d80'),
	(40,NULL,'2019-02-07 21:44:10','2019-02-07 21:44:10','2019-02-07 22:07:10','5f823653-a5bb-4b76-a618-136bac61af35'),
	(41,NULL,'2019-02-07 22:07:10','2019-02-07 22:07:10','2019-02-07 22:31:37','7aba4f65-a1ef-4c78-8b7f-ad4b1fd824ac'),
	(42,NULL,'2019-02-07 22:19:35','2019-02-07 22:19:35','2019-02-07 22:28:23','c82fcdc7-bdca-4400-9fbc-7508f0ae5589'),
	(43,NULL,'2019-02-07 22:22:58','2019-02-07 22:22:58','2019-02-08 17:07:08','45075f0b-28eb-46aa-b10b-bf9a813f0080'),
	(44,NULL,'2019-02-07 22:24:54','2019-02-07 22:24:54','2019-02-07 22:33:25','adc48fce-ba4a-431a-b99f-7539e9d17eb5'),
	(45,NULL,'2019-02-07 22:28:23','2019-02-07 22:28:23','2019-02-07 22:51:47','d85c5814-3b79-409b-8988-202f221de940'),
	(46,NULL,'2019-02-07 22:31:37','2019-02-07 22:31:37','2019-02-07 22:52:52','4299ef37-e006-4168-afbb-5f0a318896bd'),
	(47,NULL,'2019-02-07 22:33:25','2019-02-07 22:33:25','2019-02-07 22:36:32','260086ee-fabc-4060-96b7-26d8d9f4623e'),
	(48,NULL,'2019-02-07 22:36:32','2019-02-07 22:36:32','2019-02-08 17:17:43','d8bf17c1-02ea-4d21-ac6f-dcaaa5fc083d'),
	(49,NULL,'2019-02-07 22:51:47','2019-02-07 22:51:47','2019-02-08 17:11:44','b67f08f5-9a3b-4fee-95c5-d868222b039f'),
	(50,NULL,'2019-02-07 22:52:52','2019-02-07 22:52:52','2019-02-08 15:45:05','8204d060-9d5e-41ab-80a7-7cd5213cb52b'),
	(51,NULL,'2019-02-07 23:52:59','2019-02-07 23:52:59','2019-02-08 17:05:33','ea5d0b4e-bc0b-416d-9e98-706ea3280a07'),
	(52,NULL,'2019-02-07 23:55:39','2019-02-07 23:55:39','2019-02-08 20:04:40','7c931fd1-58a9-41f4-9b6c-6f3fc682a0cb'),
	(53,NULL,'2019-02-08 15:45:05','2019-02-08 15:45:05','2019-02-08 15:47:40','bcebd580-35d9-405c-9656-c8239b6da4fc'),
	(54,NULL,'2019-02-08 15:47:40','2019-02-08 15:47:40','2019-02-08 15:52:55','d1b6ce0e-6973-4c6b-a0a9-18d2485ce569'),
	(55,NULL,'2019-02-08 15:52:55','2019-02-08 15:52:55','2019-02-08 15:54:32','488d9600-70c7-4d4a-b33a-2b175cb56a37'),
	(56,NULL,'2019-02-08 15:54:32','2019-02-08 15:54:32','2019-02-08 15:57:41','7a78d083-7997-4817-ab43-fab78c4ffb89'),
	(57,NULL,'2019-02-08 15:57:41','2019-02-08 15:57:41','2019-02-08 15:59:57','8e3256e3-2963-4edb-ae37-c7a03222e946'),
	(58,NULL,'2019-02-08 15:59:57','2019-02-08 15:59:57','2019-02-08 16:42:16','ba30602e-d1bc-4612-b81a-f3dfcdf82220'),
	(59,NULL,'2019-02-08 16:42:16','2019-02-08 16:42:16','2019-02-08 16:45:21','88a64396-bbf2-470c-8608-30894a012a6b'),
	(60,NULL,'2019-02-08 16:45:21','2019-02-08 16:45:21','2019-02-08 16:47:00','5106c52e-8d64-4044-a2c3-f0543fdf888a'),
	(61,NULL,'2019-02-08 16:47:00','2019-02-08 16:47:00','2019-02-08 16:48:46','a531809e-a7f2-4165-8728-dbcc7bf0e236'),
	(62,NULL,'2019-02-08 16:48:46','2019-02-08 16:48:46','2019-02-08 16:53:48','9e03c27d-0174-40d1-a5e7-4870e09e7459'),
	(63,NULL,'2019-02-08 16:53:48','2019-02-08 16:53:48','2019-02-08 16:58:06','476d03d7-4b8f-484f-a65f-151e2cde0a65'),
	(64,NULL,'2019-02-08 16:58:06','2019-02-08 16:58:06','2019-02-08 18:05:32','98d4d028-7ff3-4bfa-92d6-04fc2335b014'),
	(65,NULL,'2019-02-08 17:05:33','2019-02-08 17:05:33','2019-02-08 17:36:27','baeb13ed-faf5-43a1-94ef-782d1192567d'),
	(66,NULL,'2019-02-08 17:07:08','2019-02-08 17:07:08','2019-02-08 17:08:39','b79a8dc1-7998-4227-8926-7353c4635e51'),
	(67,NULL,'2019-02-08 17:08:39','2019-02-08 17:08:39','2019-02-08 17:16:37','2e9d0a3b-63e6-4113-901f-98a1c5a90302'),
	(68,NULL,'2019-02-08 17:11:44','2019-02-08 17:11:44','2019-02-08 17:15:21','ef52fd53-4f17-4978-9d76-2464c3d3fb25'),
	(69,NULL,'2019-02-08 17:15:21','2019-02-08 17:15:21','2019-02-12 20:43:47','fdbc8d36-8be0-4c22-8389-678fe541f059'),
	(70,NULL,'2019-02-08 17:16:37','2019-02-08 17:16:37','2019-02-12 20:39:11','53f7bd71-0bc2-416b-9466-a41b4c12dfe2'),
	(71,NULL,'2019-02-08 17:17:43','2019-02-08 17:17:43','2019-02-08 17:41:19','93591162-045c-45e7-b778-581846678f94'),
	(72,NULL,'2019-02-08 17:36:27','2019-02-08 17:36:27','2019-02-08 17:36:56','5fda120c-bea0-4300-a234-d5a413e27ded'),
	(73,NULL,'2019-02-08 17:36:56','2019-02-08 17:36:56',NULL,'6752b063-fada-42c7-849c-f13100bcdcb1'),
	(74,NULL,'2019-02-08 17:41:19','2019-02-08 17:41:19','2019-02-08 17:44:24','b9b08b94-d77e-4467-9bb7-76f61d5d38c6'),
	(75,NULL,'2019-02-08 17:44:24','2019-02-08 17:44:24','2019-02-12 20:49:00','1ad0a627-d084-4886-b81d-6c08bb5f7073'),
	(76,NULL,'2019-02-08 18:05:32','2019-02-08 18:05:32','2019-11-07 16:37:11','2b87e134-b2fa-44bd-b36c-ae2ec27d2cc9'),
	(77,NULL,'2019-02-08 20:04:40','2019-02-08 20:04:40','2019-02-08 20:13:05','47963d62-3426-45d5-b748-6ee13579c02b'),
	(78,NULL,'2019-02-08 20:13:05','2019-02-08 20:13:05','2019-02-14 22:28:47','23160eac-0b0c-4356-bb08-9481ef4dff37'),
	(79,NULL,'2019-02-12 20:39:11','2019-02-12 20:39:11','2019-02-12 20:50:49','e5c4d3e1-a006-44d1-ae4e-48092b1c7cfd'),
	(80,NULL,'2019-02-12 20:43:47','2019-02-12 20:43:47','2019-02-12 20:52:21','fb4aa118-e9bb-4a36-a174-1bf5d65a4a7a'),
	(81,NULL,'2019-02-12 20:49:00','2019-02-12 20:49:00','2019-02-12 20:53:06','e6323260-efb5-43af-8209-387175ff37c2'),
	(82,NULL,'2019-02-12 20:50:49','2019-02-12 20:50:49',NULL,'5b263781-43f6-4727-b8ba-5a2ddd2dbb7d'),
	(83,NULL,'2019-02-12 20:52:21','2019-02-12 20:52:21',NULL,'3bff0d01-2093-4563-b990-c8aa511c5df8'),
	(84,NULL,'2019-02-12 20:53:06','2019-02-12 20:53:06',NULL,'950fdfc7-0938-4312-9f35-f65cca8fc8f0'),
	(85,NULL,'2019-02-14 22:28:47','2019-02-14 22:28:47','2019-02-27 21:20:40','3630e091-a212-42ff-ba74-e49aa3b5e222'),
	(86,NULL,'2019-02-27 21:20:40','2019-02-27 21:20:40','2019-11-07 16:38:03','c16629ea-5352-4507-bf53-b298e9449270');

/*!40000 ALTER TABLE `craft_structures` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_supertableblocks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_supertableblocks`;

CREATE TABLE `craft_supertableblocks` (
  `id` int(11) NOT NULL,
  `ownerId` int(11) NOT NULL,
  `ownerSiteId` int(11) DEFAULT NULL,
  `fieldId` int(11) NOT NULL,
  `typeId` int(11) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `deletedWithOwner` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_supertableblocks_ownerId_idx` (`ownerId`),
  KEY `craft_supertableblocks_fieldId_idx` (`fieldId`),
  KEY `craft_supertableblocks_typeId_idx` (`typeId`),
  KEY `craft_supertableblocks_sortOrder_idx` (`sortOrder`),
  KEY `craft_supertableblocks_ownerSiteId_idx` (`ownerSiteId`),
  CONSTRAINT `craft_supertableblocks_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_supertableblocks_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_supertableblocks_ownerId_fk` FOREIGN KEY (`ownerId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_supertableblocks_ownerSiteId_fk` FOREIGN KEY (`ownerSiteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `craft_supertableblocks_typeId_fk` FOREIGN KEY (`typeId`) REFERENCES `craft_supertableblocktypes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_supertableblocks` WRITE;
/*!40000 ALTER TABLE `craft_supertableblocks` DISABLE KEYS */;

INSERT INTO `craft_supertableblocks` (`id`, `ownerId`, `ownerSiteId`, `fieldId`, `typeId`, `sortOrder`, `deletedWithOwner`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(48,47,NULL,41,4,1,1,'2019-01-30 23:18:12','2019-02-27 21:20:40','f54d8eb7-8568-4209-9fb5-83e3a955fef4'),
	(49,47,NULL,41,4,2,1,'2019-01-30 23:18:12','2019-02-27 21:20:40','0877fcec-7f49-4835-9e41-0e9be80f06a1'),
	(50,47,NULL,41,4,3,1,'2019-01-30 23:18:13','2019-02-27 21:20:40','de51a0b5-04d5-4a7e-af83-e1b9cdd8bb63'),
	(69,68,NULL,38,3,1,1,'2019-01-31 16:04:34','2019-02-27 21:20:40','af791203-cbc0-4fe8-8f77-3e0d38d78959'),
	(70,68,NULL,38,3,2,1,'2019-01-31 16:04:34','2019-02-27 21:20:40','f503f3ae-c427-4ace-bc68-314fe052ff24'),
	(75,74,NULL,38,3,1,1,'2019-02-05 17:49:55','2019-02-27 21:20:40','c0382ee0-89ba-4944-84e7-961c97ca6922'),
	(77,76,NULL,38,3,1,NULL,'2019-02-05 18:52:38','2019-02-05 20:28:28','01a6ca1f-22d4-4d42-bc5d-104e9a9ff806'),
	(78,76,NULL,38,3,2,NULL,'2019-02-05 18:52:38','2019-02-05 20:28:28','667a206c-7006-4a76-8034-abcd950cf380'),
	(81,80,NULL,38,3,1,NULL,'2019-02-05 20:29:19','2019-02-05 20:29:19','edb3cb28-bdbe-4a45-8fe8-5cd2b9492c07'),
	(84,83,NULL,38,3,1,NULL,'2019-02-05 20:30:08','2019-02-05 20:30:08','bf01cba3-eb5d-4749-9691-1d9a834ae31f'),
	(99,98,NULL,38,3,2,NULL,'2019-02-07 21:02:36','2019-02-08 17:36:56','1340775a-9361-4f9a-ae79-85183eace13f'),
	(100,98,NULL,38,3,1,NULL,'2019-02-07 21:02:36','2019-02-08 17:36:56','2ffc8ec0-c636-4b9f-95fc-557a34cdf5f4'),
	(103,102,NULL,38,3,2,1,'2019-02-07 21:16:26','2019-02-08 17:16:37','0667b039-4cf4-4dd1-a4b3-b490e20179d3'),
	(107,106,NULL,38,3,2,1,'2019-02-07 21:18:38','2019-02-08 17:15:21','ab229bfc-c3fd-45a5-81db-443315b059fa'),
	(110,109,NULL,38,3,2,1,'2019-02-07 21:20:38','2019-02-08 17:44:24','a3f5c233-dafd-48c4-b3e6-8eb529becce9'),
	(111,109,NULL,38,3,1,1,'2019-02-07 21:20:38','2019-02-08 17:44:24','4a0a734f-8126-42fe-8c42-87623e8244da'),
	(114,113,NULL,38,3,1,1,'2019-02-07 21:30:45','2019-02-08 18:05:31','dbbf18ef-b5a5-4fa9-932e-957087e586c1'),
	(116,115,NULL,41,4,1,1,'2019-02-07 21:37:48','2019-02-08 18:05:31','314be3f2-0882-4cb4-a782-f417a7c18b3a'),
	(117,115,NULL,41,4,2,1,'2019-02-07 21:37:48','2019-02-08 18:05:31','8fb6c941-a4e2-40dd-a344-d30800e01119'),
	(118,115,NULL,41,4,3,1,'2019-02-07 21:37:48','2019-02-08 18:05:31','8c8d4e9b-79a9-43e0-8a9d-f9ebc0a43610'),
	(121,120,NULL,38,3,1,1,'2019-02-07 21:44:10','2019-02-08 18:05:31','b6ea8b1f-c7bd-4168-9681-99b6b7c5fb75'),
	(122,120,NULL,38,3,2,1,'2019-02-07 21:44:10','2019-02-08 18:05:32','311a58b3-f00c-454f-bbf1-6721d1b2dc3a'),
	(124,123,NULL,38,3,1,1,'2019-02-07 21:44:10','2019-02-08 18:05:32','9968c3d0-25ee-4632-924d-3cbe6e9d9d48'),
	(125,123,NULL,38,3,2,1,'2019-02-07 21:44:10','2019-02-08 18:05:32','d9d680f2-12b3-4d41-8f0e-a1e805fce019'),
	(139,106,NULL,38,3,1,1,'2019-02-07 22:19:35','2019-02-08 17:15:21','22adf49c-e682-4623-b81c-0dbbfdf3bae6'),
	(141,102,NULL,38,3,1,1,'2019-02-07 22:22:58','2019-02-08 17:16:37','ebce7e81-a77b-480a-aadd-9b7e5201a66a'),
	(153,152,NULL,38,3,1,1,'2019-02-08 15:47:39','2019-02-08 18:05:31','9bf8e16b-3740-4c0c-b0cd-b5a7773bf589'),
	(155,154,NULL,38,3,1,1,'2019-02-08 15:52:54','2019-02-08 18:05:31','71286566-1e3e-4a25-963a-c665e5554986'),
	(157,156,NULL,38,3,1,1,'2019-02-08 15:52:54','2019-02-08 18:05:32','db1625a4-ff72-413c-8558-792890fb39d5'),
	(159,158,NULL,38,3,1,1,'2019-02-08 15:52:55','2019-02-08 18:05:32','db02e233-dde0-48d7-9e32-5db42974647f'),
	(161,160,NULL,38,3,1,1,'2019-02-08 15:54:32','2019-02-08 18:05:32','a47cbaae-1b94-4749-8602-65495af1b6e0'),
	(165,164,NULL,38,3,1,1,'2019-02-08 16:45:20','2019-02-08 18:05:31','fe2c8774-37f7-4341-b047-833fd62bc7de'),
	(166,164,NULL,38,3,2,1,'2019-02-08 16:45:20','2019-02-08 18:05:31','7369d287-c57b-4e76-b89b-dd097737be08');

/*!40000 ALTER TABLE `craft_supertableblocks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_supertableblocktypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_supertableblocktypes`;

CREATE TABLE `craft_supertableblocktypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldId` int(11) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_supertableblocktypes_fieldId_idx` (`fieldId`),
  KEY `craft_supertableblocktypes_fieldLayoutId_idx` (`fieldLayoutId`),
  CONSTRAINT `craft_supertableblocktypes_fieldId_fk` FOREIGN KEY (`fieldId`) REFERENCES `craft_fields` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_supertableblocktypes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_supertableblocktypes` WRITE;
/*!40000 ALTER TABLE `craft_supertableblocktypes` DISABLE KEYS */;

INSERT INTO `craft_supertableblocktypes` (`id`, `fieldId`, `fieldLayoutId`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(3,38,10,'2019-01-21 23:49:08','2019-01-31 16:04:10','6201b01a-ad18-4b80-b177-da1103bc45f6'),
	(4,41,11,'2019-01-21 23:49:10','2019-01-30 23:31:54','9f710e28-f3c7-45c0-b11c-ae72aa1d379b');

/*!40000 ALTER TABLE `craft_supertableblocktypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_systemmessages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_systemmessages`;

CREATE TABLE `craft_systemmessages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(255) NOT NULL,
  `key` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `body` text NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_systemmessages_key_language_unq_idx` (`key`,`language`),
  KEY `craft_systemmessages_language_idx` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_taggroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_taggroups`;

CREATE TABLE `craft_taggroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_taggroups_name_idx` (`name`),
  KEY `craft_taggroups_handle_idx` (`handle`),
  KEY `craft_taggroups_dateDeleted_idx` (`dateDeleted`),
  KEY `craft_taggroups_fieldLayoutId_fk` (`fieldLayoutId`),
  CONSTRAINT `craft_taggroups_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_tags
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_tags`;

CREATE TABLE `craft_tags` (
  `id` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `deletedWithGroup` tinyint(1) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_tags_groupId_idx` (`groupId`),
  CONSTRAINT `craft_tags_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_taggroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_tags_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_templatecacheelements
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_templatecacheelements`;

CREATE TABLE `craft_templatecacheelements` (
  `cacheId` int(11) NOT NULL,
  `elementId` int(11) NOT NULL,
  KEY `craft_templatecacheelements_cacheId_idx` (`cacheId`),
  KEY `craft_templatecacheelements_elementId_idx` (`elementId`),
  CONSTRAINT `craft_templatecacheelements_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `craft_templatecaches` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_templatecacheelements_elementId_fk` FOREIGN KEY (`elementId`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_templatecachequeries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_templatecachequeries`;

CREATE TABLE `craft_templatecachequeries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cacheId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `query` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `craft_templatecachequeries_cacheId_idx` (`cacheId`),
  KEY `craft_templatecachequeries_type_idx` (`type`),
  CONSTRAINT `craft_templatecachequeries_cacheId_fk` FOREIGN KEY (`cacheId`) REFERENCES `craft_templatecaches` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_templatecaches
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_templatecaches`;

CREATE TABLE `craft_templatecaches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `siteId` int(11) NOT NULL,
  `cacheKey` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `body` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `craft_templatecaches_cacheKey_siteId_expiryDate_path_idx` (`cacheKey`,`siteId`,`expiryDate`,`path`),
  KEY `craft_templatecaches_cacheKey_siteId_expiryDate_idx` (`cacheKey`,`siteId`,`expiryDate`),
  KEY `craft_templatecaches_siteId_idx` (`siteId`),
  CONSTRAINT `craft_templatecaches_siteId_fk` FOREIGN KEY (`siteId`) REFERENCES `craft_sites` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_tokens`;

CREATE TABLE `craft_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(32) NOT NULL,
  `route` text,
  `usageLimit` tinyint(3) unsigned DEFAULT NULL,
  `usageCount` tinyint(3) unsigned DEFAULT NULL,
  `expiryDate` datetime NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_tokens_token_unq_idx` (`token`),
  KEY `craft_tokens_expiryDate_idx` (`expiryDate`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_tokens` WRITE;
/*!40000 ALTER TABLE `craft_tokens` DISABLE KEYS */;

INSERT INTO `craft_tokens` (`id`, `token`, `route`, `usageLimit`, `usageCount`, `expiryDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(56,'V9jW_lFxadkbBnmQ-c4IIKq46a6HrliV','[\"live-preview/preview\",{\"previewAction\":\"entries/preview-entry\",\"userId\":\"1\"}]',NULL,NULL,'2019-02-28 21:13:45','2019-02-27 21:13:45','2019-02-27 21:13:45','8b8d1ab0-3b84-4f24-89f6-9f227da20872'),
	(57,'AWucRm4BOCCC9hXLbWS-QSG4sBrfZgGW','[\"live-preview/preview\",{\"previewAction\":\"entries/preview-entry\",\"userId\":\"1\"}]',NULL,NULL,'2019-02-28 21:22:31','2019-02-27 21:22:31','2019-02-27 21:22:31','ba20851a-e01e-41bd-82a2-b8b1f860fce5');

/*!40000 ALTER TABLE `craft_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_usergroups`;

CREATE TABLE `craft_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_usergroups_handle_unq_idx` (`handle`),
  UNIQUE KEY `craft_usergroups_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_usergroups_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_usergroups_users`;

CREATE TABLE `craft_usergroups_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_usergroups_users_groupId_userId_unq_idx` (`groupId`,`userId`),
  KEY `craft_usergroups_users_userId_idx` (`userId`),
  CONSTRAINT `craft_usergroups_users_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_usergroups_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_userpermissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpermissions`;

CREATE TABLE `craft_userpermissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_userpermissions_name_unq_idx` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_userpermissions` WRITE;
/*!40000 ALTER TABLE `craft_userpermissions` DISABLE KEYS */;

INSERT INTO `craft_userpermissions` (`id`, `name`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'editimagesinvolume:4af88935-0c38-4783-9283-a0fa900cc2ce','2019-11-07 15:47:13','2019-11-07 15:47:13','7764f6c3-73cc-4508-92da-74d9a6aab0f1');

/*!40000 ALTER TABLE `craft_userpermissions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_userpermissions_usergroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpermissions_usergroups`;

CREATE TABLE `craft_userpermissions_usergroups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_userpermissions_usergroups_permissionId_groupId_unq_idx` (`permissionId`,`groupId`),
  KEY `craft_userpermissions_usergroups_groupId_idx` (`groupId`),
  CONSTRAINT `craft_userpermissions_usergroups_groupId_fk` FOREIGN KEY (`groupId`) REFERENCES `craft_usergroups` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_userpermissions_usergroups_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `craft_userpermissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_userpermissions_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpermissions_users`;

CREATE TABLE `craft_userpermissions_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `permissionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_userpermissions_users_permissionId_userId_unq_idx` (`permissionId`,`userId`),
  KEY `craft_userpermissions_users_userId_idx` (`userId`),
  CONSTRAINT `craft_userpermissions_users_permissionId_fk` FOREIGN KEY (`permissionId`) REFERENCES `craft_userpermissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_userpermissions_users_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table craft_userpreferences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_userpreferences`;

CREATE TABLE `craft_userpreferences` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `preferences` text,
  PRIMARY KEY (`userId`),
  CONSTRAINT `craft_userpreferences_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_userpreferences` WRITE;
/*!40000 ALTER TABLE `craft_userpreferences` DISABLE KEYS */;

INSERT INTO `craft_userpreferences` (`userId`, `preferences`)
VALUES
	(1,'{\"language\":\"en-US\"}'),
	(179,'{\"language\":null,\"weekStartDay\":null,\"enableDebugToolbarForSite\":false,\"enableDebugToolbarForCp\":false}');

/*!40000 ALTER TABLE `craft_userpreferences` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_users`;

CREATE TABLE `craft_users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `photoId` int(11) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `locked` tinyint(1) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `pending` tinyint(1) NOT NULL DEFAULT '0',
  `lastLoginDate` datetime DEFAULT NULL,
  `lastLoginAttemptIp` varchar(45) DEFAULT NULL,
  `invalidLoginWindowStart` datetime DEFAULT NULL,
  `invalidLoginCount` tinyint(3) unsigned DEFAULT NULL,
  `lastInvalidLoginDate` datetime DEFAULT NULL,
  `lockoutDate` datetime DEFAULT NULL,
  `hasDashboard` tinyint(1) NOT NULL DEFAULT '0',
  `verificationCode` varchar(255) DEFAULT NULL,
  `verificationCodeIssuedDate` datetime DEFAULT NULL,
  `unverifiedEmail` varchar(255) DEFAULT NULL,
  `passwordResetRequired` tinyint(1) NOT NULL DEFAULT '0',
  `lastPasswordChangeDate` datetime DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_users_uid_idx` (`uid`),
  KEY `craft_users_verificationCode_idx` (`verificationCode`),
  KEY `craft_users_email_idx` (`email`),
  KEY `craft_users_username_idx` (`username`),
  KEY `craft_users_photoId_fk` (`photoId`),
  CONSTRAINT `craft_users_id_fk` FOREIGN KEY (`id`) REFERENCES `craft_elements` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_users_photoId_fk` FOREIGN KEY (`photoId`) REFERENCES `craft_assets` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_users` WRITE;
/*!40000 ALTER TABLE `craft_users` DISABLE KEYS */;

INSERT INTO `craft_users` (`id`, `username`, `photoId`, `firstName`, `lastName`, `email`, `password`, `admin`, `locked`, `suspended`, `pending`, `lastLoginDate`, `lastLoginAttemptIp`, `invalidLoginWindowStart`, `invalidLoginCount`, `lastInvalidLoginDate`, `lockoutDate`, `hasDashboard`, `verificationCode`, `verificationCodeIssuedDate`, `unverifiedEmail`, `passwordResetRequired`, `lastPasswordChangeDate`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,'nerdymind',NULL,NULL,NULL,'dev@nerdymind.com','$2y$13$K8hOqXXXOswqu9Me8YvJieveNemqUa2uhFP8EfGgryf6tEkbr2cve',1,0,0,0,'2019-11-07 15:48:51',NULL,NULL,NULL,'2019-03-14 19:21:50',NULL,1,NULL,NULL,NULL,0,'2019-01-21 23:30:48','2019-01-21 23:30:48','2019-11-07 15:48:51','9bb09368-11bd-4c4e-b9c2-8b954a532ee2'),
	(179,'jessicadieken',NULL,'','','jessica.dieken@ottercares.org','$2y$13$deMt112F90mZNUS41rFIweHDRPeTuGYhP.iM8kUja.1HRuFnbrz06',1,0,0,0,'2019-06-29 02:26:48',NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,0,'2019-02-18 15:02:37','2019-02-14 22:43:46','2019-06-29 02:26:48','655e82d5-55b8-42d0-886c-addbef46bcee');

/*!40000 ALTER TABLE `craft_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_volumefolders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_volumefolders`;

CREATE TABLE `craft_volumefolders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parentId` int(11) DEFAULT NULL,
  `volumeId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `craft_volumefolders_name_parentId_volumeId_unq_idx` (`name`,`parentId`,`volumeId`),
  KEY `craft_volumefolders_parentId_idx` (`parentId`),
  KEY `craft_volumefolders_volumeId_idx` (`volumeId`),
  CONSTRAINT `craft_volumefolders_parentId_fk` FOREIGN KEY (`parentId`) REFERENCES `craft_volumefolders` (`id`) ON DELETE CASCADE,
  CONSTRAINT `craft_volumefolders_volumeId_fk` FOREIGN KEY (`volumeId`) REFERENCES `craft_volumes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_volumefolders` WRITE;
/*!40000 ALTER TABLE `craft_volumefolders` DISABLE KEYS */;

INSERT INTO `craft_volumefolders` (`id`, `parentId`, `volumeId`, `name`, `path`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,NULL,1,'Default','','2019-01-21 23:42:29','2019-01-21 23:42:29','31bf46cc-b86c-4e7b-9887-6ff632658051'),
	(2,NULL,NULL,'Temporary source',NULL,'2019-01-30 21:34:19','2019-01-30 21:34:19','a54d2307-90cf-4c74-9e4b-657445905baa'),
	(3,2,NULL,'user_1','user_1/','2019-01-30 21:34:19','2019-01-30 21:34:19','c82f2a02-87a4-4d98-a37a-e2a3bbd53ffb');

/*!40000 ALTER TABLE `craft_volumefolders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_volumes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_volumes`;

CREATE TABLE `craft_volumes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fieldLayoutId` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `handle` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `hasUrls` tinyint(1) NOT NULL DEFAULT '1',
  `url` varchar(255) DEFAULT NULL,
  `settings` text,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `dateDeleted` datetime DEFAULT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_volumes_name_idx` (`name`),
  KEY `craft_volumes_handle_idx` (`handle`),
  KEY `craft_volumes_fieldLayoutId_idx` (`fieldLayoutId`),
  KEY `craft_volumes_dateDeleted_idx` (`dateDeleted`),
  CONSTRAINT `craft_volumes_fieldLayoutId_fk` FOREIGN KEY (`fieldLayoutId`) REFERENCES `craft_fieldlayouts` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_volumes` WRITE;
/*!40000 ALTER TABLE `craft_volumes` DISABLE KEYS */;

INSERT INTO `craft_volumes` (`id`, `fieldLayoutId`, `name`, `handle`, `type`, `hasUrls`, `url`, `settings`, `sortOrder`, `dateCreated`, `dateUpdated`, `dateDeleted`, `uid`)
VALUES
	(1,NULL,'Default','default','craft\\volumes\\Local',1,'/images/default','{\"path\":\"@webroot/images/default\"}',NULL,'2019-01-21 23:42:29','2019-01-21 23:42:29',NULL,'4af88935-0c38-4783-9283-a0fa900cc2ce');

/*!40000 ALTER TABLE `craft_volumes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table craft_widgets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `craft_widgets`;

CREATE TABLE `craft_widgets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `sortOrder` smallint(6) unsigned DEFAULT NULL,
  `colspan` tinyint(3) DEFAULT NULL,
  `settings` text,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `dateCreated` datetime NOT NULL,
  `dateUpdated` datetime NOT NULL,
  `uid` char(36) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `craft_widgets_userId_idx` (`userId`),
  CONSTRAINT `craft_widgets_userId_fk` FOREIGN KEY (`userId`) REFERENCES `craft_users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `craft_widgets` WRITE;
/*!40000 ALTER TABLE `craft_widgets` DISABLE KEYS */;

INSERT INTO `craft_widgets` (`id`, `userId`, `type`, `sortOrder`, `colspan`, `settings`, `enabled`, `dateCreated`, `dateUpdated`, `uid`)
VALUES
	(1,1,'craft\\widgets\\RecentEntries',1,NULL,'{\"section\":\"*\",\"siteId\":\"1\",\"limit\":10}',1,'2019-01-21 23:30:53','2019-01-21 23:30:53','53752758-518a-409b-adf1-a267fccde9a2'),
	(2,1,'craft\\widgets\\CraftSupport',2,NULL,'[]',1,'2019-01-21 23:30:53','2019-01-21 23:30:53','c513a0ea-3af8-4a28-84ec-b2e55ef1f1f6'),
	(3,1,'craft\\widgets\\Updates',3,NULL,'[]',1,'2019-01-21 23:30:53','2019-01-21 23:30:53','a04647a2-98d8-4745-90f2-9207c9d37b07'),
	(4,1,'craft\\widgets\\Feed',4,NULL,'{\"url\":\"https://craftcms.com/news.rss\",\"title\":\"Craft News\",\"limit\":5}',1,'2019-01-21 23:30:54','2019-01-21 23:30:54','aa940b1a-3715-4152-b58e-605de5fe0230'),
	(5,179,'craft\\widgets\\RecentEntries',1,NULL,'{\"section\":\"*\",\"siteId\":\"1\",\"limit\":10}',1,'2019-02-18 15:02:59','2019-02-18 15:02:59','ed94be73-91f7-45a3-8168-83aa574c5337'),
	(6,179,'craft\\widgets\\CraftSupport',2,NULL,'[]',1,'2019-02-18 15:02:59','2019-02-18 15:02:59','718330ce-24ca-467c-b799-513feea30cea'),
	(7,179,'craft\\widgets\\Updates',3,NULL,'[]',1,'2019-02-18 15:02:59','2019-02-18 15:02:59','9218908d-0501-4548-b81a-9dd3fdb88851'),
	(8,179,'craft\\widgets\\Feed',4,NULL,'{\"url\":\"https://craftcms.com/news.rss\",\"title\":\"Craft News\",\"limit\":5}',1,'2019-02-18 15:02:59','2019-02-18 15:02:59','75cb6451-a05d-433a-93b9-fcfe97c661c2');

/*!40000 ALTER TABLE `craft_widgets` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
