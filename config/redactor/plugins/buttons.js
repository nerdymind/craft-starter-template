(function($R)
{
    $R.add('plugin', 'buttons', {
        translations: {
            en: {
                "button": "Button"
            }
        },
        init: function(app)
        {
            this.app = app;
            this.lang = app.lang;
            this.toolbar = app.toolbar;
        },
        start: function()
        {
            // create dropdown object
            var dropdownData = {
                item1: {
                    title: 'Default',
                    api: 'plugin.buttons.setButtonDefault'
                },
                item2: {
                    title: 'Primary',
                    api: 'plugin.buttons.setButtonPrimary'
                }
            };

            // create the button data
            var buttonData = {
                title: this.lang.get('button'),
            };

            // create the button
            var $button = this.toolbar.addButton('buttons', buttonData);


            // set dropdown
            var dropdown = $button.setDropdown(dropdownData);
        },

        setButtonDefault: function()
        {
            this.app.api('module.inline.clearclass');
            var args = {
                class: 'btn',
            };
            this.app.inline.set(args);
        },

        setButtonPrimary: function()
        {
            this.app.api('module.inline.clearclass');
            var args = {
                class: 'btn btn-primary',
            };
            this.app.inline.set(args);
        },



    });
})(Redactor);