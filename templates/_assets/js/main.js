

/**
 * We'll load Vue and the Bootstrap Vue plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    require ("@fortawesome/fontawesome-free/js/all.js"); //pull font awesome
    require('./includes/app'); // Vue
} catch (e) {
    console.log('error requiring Bootstrap Vue deps: ', e.toString())
}

// ..
