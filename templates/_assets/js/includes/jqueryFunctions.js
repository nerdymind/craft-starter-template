/**
 * jQuery functions
 */
(function($) {
    "use strict";

    // jQuery loaded
    $(function() {

        // Craft Admin bar spacing
        if ($('#adminbar').length !== 0) {
            $('body').addClass('has-adminbar');
        }


        //featherlightGallery
        $('.photoGallery a, .gallery a').featherlightGallery({
            openSpeed: 300,
            previousIcon: '&#8249;',
            nextIcon: '&#8250;',
        });

        function stickyNav(){
            var page_scroll = $(document).scrollTop();
            var nav = $('.fixed-header');
            //var topbar = $('.top-nav');
            var header_scroll = '170';
            var header_wrapper = '300';

            $('.header-space').css('height', nav.outerHeight());

            $(window).scroll(function(){

                if($(window).scrollTop() >= header_wrapper){
                    nav.addClass('sticky');
                }
                if($(window).scrollTop() < header_wrapper){
                    nav.removeClass('sticky');
                }

                if($(window).scrollTop() >= header_scroll){
                    nav.addClass('scroll');
                }
                if($(window).scrollTop() < header_scroll){
                    nav.removeClass('scroll');
                }
            })
        }
        stickyNav();

    }); // End of DOM Ready
})(jQuery); // Fully reference jQuery
