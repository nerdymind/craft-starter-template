// app.js
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import VueCarousel from 'vue-carousel';

Vue.use(VueCarousel);
Vue.use(BootstrapVue);

//importing bootstrap
import VuePictureSwipe from 'vue-picture-swipe';
Vue.component('vue-picture-swipe', VuePictureSwipe);

import { ModalPlugin } from 'bootstrap-vue'
Vue.use(ModalPlugin)

//use vue for nav
var header = new Vue({
    el: '#header',
    delimiters: ['${', '}'],
});


const vues = document.querySelectorAll(".modal-block");
const each = Array.prototype.forEach;
each.call(vues, (el, index) => new Vue({el}));


const photoGalleryEl = document.querySelectorAll(".photoGallery");
const photoGalleryEach = Array.prototype.forEach;
photoGalleryEach.call(photoGalleryEl, (el, index) => new Vue({el}));


const accordionEl = document.querySelectorAll(".accordion");
const accordionEach = Array.prototype.forEach;
accordionEach.call(accordionEl, (el, index) => new Vue({el}));
